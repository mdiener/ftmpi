/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 
 			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#ifdef IMA_ALPHA
#include <sys/resource.h>
#endif
#include "ensemble.h"
#include "parsing.h"
#include "services.h"
#include "param.h"
#include "utils.h"
#include "msg.h"
#include "snipe_lite.h"
#include "ns_lib.h"
#include "envs.h"
#include "proc.h"
#include "internal_state_sys.h"
#include "ft-mpi-modes.h"
#include "notifier.h"
#include "libstartup.h"
#include "libcio.h"

#define TIMEOUT 500


#ifdef IMA_ALPHA
typedef long time64_t;
#endif


/**
resetSystem - kill all processes
@return Error Number
*/

int resetSystem(void)
{
    int mid;
    int rc;
    int err;

    /** Assume initilizeNS is called before calling this function */

    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    startup_finder_vm(vmname);
    rc = startup_reset(mid,&err);
    if(rc<0) {
        printf("Connection problem while connect to hostID %d\n",err);
    } else {
        printf("System reset\n");
    }
    return CONS_SUCCESS;
}

/**
haltSystem - kill all processes, daemon
@return Error Number
*/

int haltSystem(void)
{
    int mid;
    int rc;
    int err;

    /** Assume initilizeNS is called before calling this function */

    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    startup_finder_vm(vmname);
    rc = startup_halt(mid,&err);
    if(rc<0) {
        printf("Connection problem while connect to hostID %d\n",err);
    } else {
        printf("System halted\n");
    }
    return CONS_SUCCESS;
}

/**
killProcess - send signal to process process
@param gid global process ID
@param signal signal number
@return Error Number
*/

int killProcess(int gid, int signal,int flag)
{
    int mid;
    int rc;
    int err;
    char sgid[MAXBUF];
    char serr[MAXBUF];

    /** Assume initilizeNS is called before calling this function */

    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    startup_finder_vm(vmname);
    rc = startup_kill(gid,signal,mid,flag,&err);
    switch(rc) {
       case -1 : if(flag==1) { 
		    sprintf(sgid,"GID %d",gid); 
		 } else { 
		    sprintf(sgid,"RunID %d",gid); 
		 } 
		 printError(CONS_INVLP, sgid); 
		 return CONS_INVLP;
       case -2 : printError(CONS_INVLKL, strerror(err)); return CONS_INVLKL;
       case -3 : sprintf(sgid,"%d",gid); printError(CONS_INVLST, sgid); return CONS_INVLST;
       case -4 : sprintf(serr,"%d",err); printError(CONS_CONNPROB, serr); return CONS_CONNPROB;
    };
    return CONS_SUCCESS;
}

/**
spawnProcess - Spawn Process
@param spawn_cmd spawn command
@param argc argument count
@param argv argument value
@return Error number
*/
int spawnProcess(char *spawn_cmd,int argc, char **argv)
{
    int flag;
    int np;                     /* np = num of procs requested */
    int exe;
    int exeargs;                /* exeargs = # of exe args */
    int next;
    int needoutput;
    int mpiapp;
    int cma, mma;
    int cm, mm;
    int cwdir; 
    int ret;


    needoutput=0;
    mpiapp=0;
    cma=0;
    mma=0;
    cm=0;
    mm=0;
    cwdir=0;
    np=0;



    if (argc < 4) {
        printError(CONS_CMDARG, spawn_cmd);
        printHelp(spawn_cmd);
        return CONS_CMDARG;
    }

    if(!strcmp(spawn_cmd,"mpispawn")) {
       cm = FT_MODE_REBUILD;   /* REBULD communicators */
       mm = FT_MODE_NOP;       /* do not do comms do NOPs */
       mpiapp=1;
    }

    silent=0;

    exe = 1;                    /* were we think the exe name is */
    next = 1;

    while (next < argc) {
       if (!strcmp(argv[next],"-o")) {
            needoutput=1;
            exe = next+1;
            next+=1;
       } else if (!strcmp(argv[next],"-s")) {
            silent=1;
            exe = next+1;
            next+=1;
       } else if (!strcmp(argv[next],"-mpi")) {
            /* set default only -mpi is set */
            cm = FT_MODE_REBUILD;   /* REBULD communicators */
            mm = FT_MODE_NOP;       /* do not do comms do NOPs */
            mpiapp=1;
            exe = next+1;
            next+=1;
       } else if (!strcmp(argv[next],"-com_mode")) {
	    cma = next+1;
	    exe = next+2;
	    next+=2;
       } else if (!strcmp(argv[next],"-msg_mode")) {
	    mma = next+1;
	    exe = next+2;
	    next+=2;
       } else if (!strcmp(argv[next],"-cwd")) {
            if(argv[next+1]==NULL) {
                printError(CONS_INVLCWD, spawn_cmd);
                printHelp(spawn_cmd);
                return CONS_INVLCWD;
	    }
            cwdir = next+1;
	    exe = next+2; 
	    next+=2;
       } else if (!strcmp(argv[next],"-np")) {
            if(argv[next+1]==NULL) {
                printError(CONS_INVLNP, spawn_cmd);
                printHelp(spawn_cmd);
                return CONS_INVLNP;
	    }
	    np=atoi(argv[next+1]);
            if ((np <= 0) || (np > 2048)) {
                printError(CONS_INVLNP, spawn_cmd);
                printHelp(spawn_cmd);
                return CONS_INVLNP;
            }
	    exe = next+2;
	    next+=2;
       } else {
            next++;
       }
    }


    /* The exe is at argv[exe] */
    exeargs = argc - exe;       /* calculate the number of exe args */

    if (cma) {
        if(argv[cma]==NULL) {
            printError(CONS_INVLCMA, spawn_cmd);
            printHelp(spawn_cmd);
            return CONS_INVLCMA;
	}
        if(!strcmp(argv[cma],"BLANK")) cm = FT_MODE_BLANK;
	else if(!strcmp(argv[cma],"REBUILD")) cm = FT_MODE_REBUILD;
	else if(!strcmp(argv[cma],"SHRINK")) cm = FT_MODE_SHRINK;
	else if(!strcmp(argv[cma],"ABORT")) cm = FT_MODE_ABORT;
	else { printError(CONS_INVLCMA, spawn_cmd); printHelp(spawn_cmd); return CONS_INVLCMA; }
    }

    if (mma) {
        if(argv[mma]==NULL) {
            printError(CONS_INVLMMA, spawn_cmd);
            printHelp(spawn_cmd);
            return CONS_INVLMMA;
	}
        if(!strcmp(argv[mma],"CONT")) mm = FT_MODE_CONT;
	else if(!strcmp(argv[mma],"NOP")) mm = FT_MODE_NOP;
	else { printError(CONS_INVLMMA, spawn_cmd); printHelp(spawn_cmd); return CONS_INVLMMA; }
    }

    if(mpiapp==1) {
        flag=RUN_FTMPI;
    } else {
	if(mma!=0) {
           printError(CONS_INVLWMPI, "-msg_mode");
           printHelp(spawn_cmd);
	   return CONS_INVLWMPI;
	} 
	if(cma!=0) {
           printError(CONS_INVLWMPI, "-com_mode");
           printHelp(spawn_cmd);
	   return CONS_INVLWMPI;
	}
        flag=RUN_HARNESS;
    }

    if(argv[exe]==NULL) {
        printError(CONS_INVLEXE, spawn_cmd);
        printHelp(spawn_cmd);
        return CONS_INVLEXE;
    }

    if(needoutput==1) {
        flag=flag | RUN_NEEDOUTPUT;
    }
    if(cwdir!=0) {
       ret=startup_run(flag,vmname,np,HOSTAUTO,NULL,cm,mm,exeargs,&argv[exe],argv[cwdir]);
    } else {
       ret=startup_run(flag,vmname,np,HOSTAUTO,NULL,cm,mm,exeargs,&argv[exe],hn_cwdir);
    }
    if(ret<0) {
       switch(ret) {
          case CONS_NOHOST : printError(CONS_NOHOST, vmname); printHelp("add"); break;
          default :
          printError(CONS_CMDARG, spawn_cmd);
          printHelp(spawn_cmd);
       };
    }
    return 0;

}


/*************************************************************************/




/**
processStatus - Display Process Status
@return Error number
*/
int processStatus(void)
{
    struct startup_procstat *item, *itemHead;
    int mid;
    int hostID;
    int i;
    int nsud;
    int nItem;
    int totalItem;
    int sud;
    int first;
    int mytime;
    int day, hour, minute, second;
    char strstatus[MAXBUF];

    /** Assume initilizeNS is called before calling this function */

    ns_open();
    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    nsud = startup_finder_vm(vmname);

    if (nsud < 0) {
        printError(CONS_NOHOST, vmname);
	printHelp("add");
        return CONS_NOHOST;
    }

    first = 1;
    totalItem = 0;

    for (sud = 0; sud < nsud; sud++) {
        nItem = startup_ps(sud, mid, 1, &itemHead, &hostID); /* 1 mean - expire it */
        if (nItem > 0) {
            totalItem += nItem;
            if (first == 1) {
/*
                printf
                    ("01234567890123456789012345678901234567890123456789012345678901234567890123456789\n");
*/
                printf
                    ("ProcID   RunID   HostID   Command                   Status           ElapsedTime\n");
                first = 0;
            }
            item = itemHead;
            for (; item; item = item->next) {
                printf("%6d  %6d  %7d   %-24.24s",
                       item->gid, item->caller, hostID, item->execName);

                if (item->status == STATUS_FAILED_ON_EXEC) {
                    sprintf(strstatus, "failed(err:%d)",
                            WEXITSTATUS(item->exitValue));
                } else {
                    if (item->status == STATUS_EXITED) {
                        if (WIFEXITED(item->exitValue)) {
                            sprintf(strstatus, "exited(val:%d)",
                                    WEXITSTATUS(item->exitValue));
                        } else if (WIFSIGNALED(item->exitValue)) {
                            sprintf(strstatus, "exited(sig:%d)",
                                    WTERMSIG(item->exitValue));
                        }
                    } else {
                        strcpy(strstatus,
                               (char *) statusToString(item->status));
                    }
                }
                printf("  %-14.14s", strstatus);
	        mytime=item->elapsedTime;
		day=mytime/86400;
		hour=(mytime%86400)/3600;
		minute=((mytime%86400)%3600)/60;
		printf("  ");

		/** Format is either NdNNhNNmNNs or NNNNdNNhNNm **/

		if(day>999) { 
		    printf("%dd",day);
		} else if(day>99) {
		    printf(" %dd",day);
		} else if(day>9) {
		    printf("  %dd",day);
		} else if(day>0) {
		    printf("%dd",day);
		} else {
		    printf("   ");
		}	
		if(hour>0) { if(hour<10) {printf("0");} printf("%dh",hour); } else { printf("   "); }
		if(minute>0) { if(minute<10) {printf("0");} printf("%dm",minute); } else { printf("   "); }
		if(day<10) { 
		    second=((mytime%86400)%3600)%60;
		    if(second<10) {printf("0");} printf("%ds",second); 
		}
                puts("");

                if (item->callbackListLength > 0) {
                    printf("callbacks: ");
                    for (i = 0; i < item->callbackListLength; i++) {
                        if (item->callbackList[i] != -1) {
                            printf("%d ", item->callbackList[i]);
                        }
                    }
                    printf("\n");

                }
            }
        }
    }
    if (totalItem == 0) {
        printf("%sNo processes found%s\n",error_begin,error_end);
    }
    return CONS_SUCCESS;
}


/**
expireProcess - expire process information
@param age age
@return Error Number
*/

int expireProcess(int age)
{
    int mid;
    int rc;
    int err;
    char serr[MAXBUF];

    /** Assume initilizeNS is called before calling this function */

    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    startup_finder_vm(vmname);
    rc = startup_expire(age,mid,&err);
    if(rc >= 0) {
        printf("%d process(es) have been expired.\n",rc);
    } else {
        sprintf(serr,"%d",err); printError(CONS_CONNPROB, serr); return CONS_CONNPROB;
    }
    return CONS_SUCCESS;
}


/**
execCommands - execute command simultaneously
@param cmd commmandx
@param option option
*/

int execCommands(char **param, int cmd, int option)
{
    int i, j, pid, status;
    int rc;
    int hostID;
    int age;
    int gid;
    int runid;
    char *result;

    if(cmd==CONS_SIG || cmd==CONS_SIGALL) {
       i=2;
    } else if(cmd==CONS_ADD && option==1) { /* add from file */
       i = 0;
    } else {
       i = 1;
    }

    while (param[i] != NULL) {
        pid = fork();
        if (pid < 0) {
            perror("fork");
        } else if (pid == 0) {
            switch (cmd) {
            case CONS_ADD:
                add(param[i]);
                break;
            case CONS_DELETE:
		hostID=strtol(param[i], &result, 10);
		if(strcmp(result,"")) {
                    printError(CONS_INVLHID, param[i]);
		} else {
                    rc=deleteHost(hostID);
		    if(rc==-1) {
                        printError(CONS_INVLHID, param[i]);
		    }
		}
                break;
            case CONS_EXPIRE:
		age=strtol(param[i], &result, 10);
		if(strcmp(result,"")) {
                    printError(CONS_INVLAGE, param[i]);
		} else {
                    expireProcess(age);
		}
                break;
	    case CONS_KILL:
            case CONS_SIG:
		gid=strtol(param[i], &result, 10);
		if(strcmp(result,"")) {
                    printError(CONS_INVLGID, param[i]);
		} else {
                    killProcess(gid,option,1); /* 1 = kill by GID */ 
		}
                break;
	    case CONS_KILLALL:
	    case CONS_SIGALL:
		runid=strtol(param[i], &result, 10);
		if(strcmp(result,"")) {
                    printError(CONS_INVLRID, param[i]);
		} else {
                    killProcess(runid,option,2); /* 2 = kill by RunID */ 
		}
                break;
            };
	    exit(CONS_SUCCESS);
        }
        i++;
    }
    for (j = 0; j < i; j++) {
        wait3(&status, 0, NULL);
    }
    return CONS_SUCCESS;
}




/**
execCommand - Execute Command
@param firstPos First position of process in input.
@param input Input
@return Error number
*/
int execCommand(int firstPos, IS input)
{
    int i, paramIndex;
    char tmpbuf[MAXBUF];
    char *param[MAXBUF];
    int signal;
    int ret;

    paramIndex = 0;
    for (i = firstPos; i < input->NF; i++) {
        if (!strcmp(input->fields[i], ";")) {
            i++;
            break;
        } else {
            param[paramIndex] = (char *) malloc(strlen(input->fields[i]) + 1);
            strcpy(param[paramIndex], input->fields[i]);
            paramIndex++;
        }
    }
    param[paramIndex] = NULL;

    if (param != NULL) {
        if ((!strcmp(param[0], "help")) || (!strcmp(param[0], "?"))) {
            printHelp(param[1]);
        } else if (!strcmp(param[0], "spawn")) {
            spawnProcess("spawn",paramIndex, param);
        } else if (!strcmp(param[0], "mpispawn")) {
            spawnProcess("mpispawn",paramIndex, param);
        } else if (!strcmp(param[0], "kill")) {
            if (param[1] == NULL) {
                printError(CONS_CMDARG, "kill");
                printHelp("kill");
            } else {
                signal = -1;       /* kill with HUP and QUIT */
                execCommands(param, CONS_KILL, signal);
            }
        } else if (!strcmp(param[0], "killall")) {
            if (param[1] == NULL) {
                printError(CONS_CMDARG, "killall");
                printHelp("killall");
            } else {
                signal = -1;       /* kill with HUP and QUIT */
                execCommands(param, CONS_KILLALL, signal);
            }
        } else if (!strcmp(param[0], "expire")) {
            if (paramIndex != 2) {
                printError(CONS_CMDARG, "expire");
                printHelp("expire");
            } else {
                execCommands(param, CONS_EXPIRE, 0);
            }
        } else if (!strcmp(param[0], "service")) {
            if (paramIndex != 3) {
                printError(CONS_CMDARG, "service");
                printHelp("service");
            } else {
                if(!strcmp(param[1], "all")) {
                    ret=service_all(param[2]);
		    if(ret!=CONS_SUCCESS) {
                       switch(ret) {
                           case CONS_INVLOPR: printError(ret, param[2]); printHelp("service"); break;
		       }
		    }
		} else  {
                    ret=service_run(service_getID(param[1]),param[2],0);
		    if(ret!=CONS_SUCCESS) {
                       switch(ret) {
                           case CONS_INVLSID: printError(ret, param[1]); printHelp("service"); break;
                           case CONS_INVLOPR: printError(ret, param[2]); printHelp("service"); break;
		       }
		    }
	        }
            }
        } else if (!strcmp(param[0], "sig")) {
            if (paramIndex < 3) {
                printError(CONS_CMDARG, "sig");
                printHelp("sig");
            } else {
                signal=atoi(param[1]);
                execCommands(param, CONS_SIG, signal);
            }
        } else if (!strcmp(param[0], "sigall")) {
            if (paramIndex < 3) {
                printError(CONS_CMDARG, "sigall");
                printHelp("sigall");
            } else {
                signal=atoi(param[1]);
                execCommands(param, CONS_SIGALL, signal);
            }
        } else if (!strcmp(param[0], "conf")) {
            if (param[1] != NULL) {
                if(!strcmp(param[1],"-a")) {
                    conf(ALLINFO); 
		} else {
		    printError(CONS_CMDARG, "conf");
                    printHelp("conf");
		}
            } else {
                conf(DEFAULT); 
            }
	} else if (!strcmp(param[0], "ps")) {
	    if (param[1] != NULL) {
	        printError(CONS_CMDARG, "ps");
	        printHelp("ps");
	     } else {
	        processStatus(); 
	     }
        } else if (!strcmp(param[0], "halt")) {
            haltSystem();
        } else if (!strcmp(param[0], "haltall")) {
            haltSystem();
	    service_all("stop");
	    exit(0);
#ifdef HAVE_READLINE
        } else if (!strcmp(param[0], "history")) {
	    if (param[1] != NULL) {
	        printError(CONS_CMDARG, "history");
	        printHelp("history");
	     } else {
                listHistory();
	     }
#endif
        } else if (!strcmp(param[0], "reset")) {
            resetSystem();
        } else if (!strcmp(param[0], "add")) {
            if (param[1] == NULL) {
                printError(CONS_CMDARG, "add");
                printHelp("add");
            } else {
                if(!strcmp(param[1],"-f")) {
                    ret=addFromFile(param[2]);  
                    if(ret!=CONS_SUCCESS) {
                        printError(CONS_CMDARG, "add");
                        printHelp("add");
		    }
		} else {
                    /* check FTMPI notifier first */
		    ret = notifier_find ();
		    if (ret<0) {
		        printError(CONS_NONOTIFY,NULL);
		        return CONS_NONOTIFY;
		    }
                    execCommands(param, CONS_ADD, 0);
		}
            }
        } else if (!strcmp(param[0], "alias")) {
            if(paramIndex==1) {
               print_data(table_alias);
	    } else if(paramIndex==2) {
               printError(CONS_CMDARG, "alias");
               printHelp("alias");
	    } else {
               memset(&tmpbuf,0,MAXBUF);
               strcpy(tmpbuf,param[2]);
               for(i=3;i<paramIndex;i++) {
                  strcat(tmpbuf," ");
		  strcat(tmpbuf,param[i]);
	       }
	       add_data(table_alias,param[1],tmpbuf);
	    }
        } else if (!strcmp(param[0], "unalias")) {
            if(paramIndex!=2) {
                printError(CONS_CMDARG, "unalias");
                printHelp("unalias");
            } else {
                ret=del_data(table_alias,param[1]);
	        if(ret<0) {
                   printError(CONS_INVLALN, param[1]);
                   printHelp("unalias");
	        }
            }
        } else if (!strcmp(param[0], "delete")) {
            if (param[1] == NULL) {
                printError(CONS_CMDARG, "delete");
                printHelp("delete");
            } else {
                execCommands(param, CONS_DELETE, 0);
            }
        } else if (!strcmp(param[0], "vmname")) {
            if (paramIndex > 2) {
                printError(CONS_CMDARG, "vmname");
                printHelp("vmname");
            } else {
                if(paramIndex==1) {
                    puts(vmname);
		} else {
                    strcpy(vmname,param[1]);
		}
            }
        } else if (!strcmp(param[0], "cwd")) {
            if (paramIndex > 2) {
                printError(CONS_CMDARG, "cwd");
                printHelp("cwd");
            } else {
                if(paramIndex==1) {
                    puts(hn_cwdir);
		} else {
                    strcpy(hn_cwdir,param[1]);
		}
            }
        } else {
            printError(CONS_INVLCMD, param[0]);
        }
    }
    return 0;
}

/**
waitProcess - Wait Child Process
@param nChild number of child process
@return Error number
*/
int waitProcess(int *nChild, int pid)
{
    int dead;
    int i, er;
    int status;
    int termSig;
    int first = 1;
    int max = (*nChild);

    for (i = 0; i < max; i++) {
        dead = 0;
        if (first == 1) {
            while (waitpid(pid, &status, WUNTRACED) < 0);
            first = 0;
            er = pid;
        } else {
            er = wait3(&status, WNOHANG, NULL);
        }
        if (er > 0) {
            if (WIFEXITED(status)) {    /* Normal Exit */
                dead = 1;
            } else if (WIFSTOPPED(status)) {    /* Process STOP (Traced) */
#ifndef IMA_SP2 /* AIX doesn't support WCOREDUMP... Let it be ;p */
            } else if (WCOREDUMP(status)) {     /* Process core dump */
                printError(CONS_INVLSEG, NULL);
                dead = 1;
#endif
            } else if (WIFSIGNALED(status)) {   /* Process Terminate */
                dead = 1;
                termSig = WTERMSIG(status);
                if (termSig == SIGSEGV) {
                    printError(CONS_INVLSEG, NULL);
                }
            } else {            /* Unknown */
                dead = 1;
            }
        }
        if (dead == 1) {
            (*nChild)--;
        }
    }
    return 0;
}

