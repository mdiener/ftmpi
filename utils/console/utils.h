/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/
#ifndef UTILS_H
#define UTILS_H

#include "parsing.h"
#include "startup.h"

#define CONS_SUCCESS      0    /* Success */
#define CONS_PROGARG     -1    /* Invalid Arguments */ 
#define CONS_INVLSYN     -2    /* Invalid Console Syntax */
#define CONS_INVLCMD     -3    /* Invalid Command */
#define CONS_INVLSEG     -4    /* Segmentation Fault */
#define CONS_INVLHLP     -5    /* Invalid Help Option */
#define CONS_NOHOST      -6    /* No host availabie */
#define CONS_NONS        -7    /* Can not contact name service */
#define CONS_CMDARG      -8    /* Invalid command argument */
#define CONS_INVLNP      -9    /* Invalid number of process */
#define CONS_INVLP       -10   /* Invalid process number(process not found) */
#define CONS_INVLKL      -11   /* Invalid kill */
#define CONS_INVLST      -12   /* Invalid status */
#define CONS_INVLHID     -13   /* Invalid host ID */
#define CONS_INVLGID     -14   /* Invalid global process ID format */
#define CONS_INVLAGE     -15   /* Invalid age format */
#define CONS_AMBICMD     -16   /* Ambiguous command */
#define CONS_INVLWMPI    -17   /* Use -com_mode/-msg_mode without -mpi */
#define CONS_INVLCMA     -18   /* Invalid communicator mode */
#define CONS_INVLMMA     -19   /* Invalid message mode */
#define CONS_INVLANS     -20   /* Invalid answer [y/n] */
#define CONS_CONNPROB    -21   /* Connection Problem */
#define CONS_INVLRID     -22   /* Invalid Run ID */
#define CONS_INVLEXE     -23   /* Invalid execution file */
#define CONS_INVLCWD     -24   /* Invalid current working directory */
#define CONS_INVLENV     -25   /* Environment was not specified */
#define CONS_NONOTIFY    -26   /* Can not contact ftmpi_notifier */
#define CONS_INVLSID     -27   /* Invalid service identifier */
#define CONS_INVLOPR     -28   /* Invalid operation */
#define CONS_INVLDEP     -29   /* Invalid dependency */
#define CONS_GETCWDIR    -30   /* Cannot get current working directory */
#define CONS_INVLALN    -31    /* alias name not found */

#define MAX_HOLIDAY 3


typedef struct {
   int day;
   int month;
   int year;
   char message[MAXBUF];
} holiday_t;

typedef struct table {
   char key[MAXBUF];
   char value[MAXBUF];
   struct table *next;
} DATATAB;

extern DATATAB *table_alias;

int printFatal(char *filename,int line, char *message);
int printError(int err, char *param);
int printHelp(char *command);
int findDelim(int firstPos, IS input);
int findLanguage(void);
int checkTerm(void);
int checkFields(IS input);
int printWelcome(void);
int printHoliday(void);
char *cons_getHostname(int addr);

int init_data(DATATAB **table);
int add_data(DATATAB *table,char *key,char *value);
int del_data(DATATAB *table,char *key);
int print_data(DATATAB *table);
char *get_data(DATATAB *table,char *key);

#ifdef HAVE_READLINE
int initialize_readline(void);
int listHistory(void);
#endif

/* The stuff below is for glumbling compiler.*/
extern char vmname[MAXBUF];
extern char hn_cwdir[MAXBUF];
extern char *prompt_begin;
extern char *prompt_end;
extern char *welcome_cs;
extern char *welcome_de;
extern char *welcome_en;
extern char *welcome_fr;
extern char *welcome_th;
extern char *myLanguage;
extern char *welcome_begin;
extern char *welcome_end;
extern char *error_begin;
extern char *error_end;
extern char *help_begin;
extern char *help_end;
extern int myColor;
extern char *help[]; 

extern holiday_t holiday [MAX_HOLIDAY];

extern int console_init(void);

extern int isEaster(int day,int month,int year);
extern int confirmCommand(IS input,char *command);

char ** auto_completion (char *text, int start);
char * command_generator (char *text, int state);




#endif
