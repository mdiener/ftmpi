/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef PARAM_H
#define PARAM_H


/* Default console command pompt */
#define DEFAULT_PROMPT "con> "

/* Default virtual machine name */
#define DEFAULT_VIRTUAL_MACHINE "dvm"

/* Remote shell method */
#define RSHCOMMAND "@sshcommand@"

/* Console run command file */
#define CONSOLERC ".harness/conrc"

/* Color stuff ;-) */

/* Please make sure that your terminal are colorizable */
/* Known terminal that colorizable :                   */
/* linux, ansi, console, con132x25, con132x30          */
/* con132x43, con132x60, con80x25, con80x28            */
/* con80x30, con80x43, con80x50,  con80x60             */
/* cons25,  xterm, rxvt, xterm-color                   */
/* color-xterm, vt100, dtterm, color_xterm             */

#define SHOWCOLOR 0   /* 1 = show , 0 = don't show */

/* Font attribute :
 * 00=none, 01=bold, 04=underscore, 05=blink, 07=reverse, 08=concealed
 * Text color :
 * 30=black, 31=red, 32=green, 33=yellow, 34=blue, 35=magenta, 36=cyan, 37=white
 * Background color :
 * 40=black, 41=red, 42=green, 43=yellow, 44=blue, 45=magenta, 46=cyan, 47=white
 */

/* Welcome message font */
#define WELCOME_FONT "01;33"
/* Error message font */
#define ERROR_FONT "01;31"
/* Prompt font */
#define PROMPT_FONT "01;34"
/* Help title font */
#define HELP_FONT "00;36"


/****************************************************************************/
/* Don't change anything below this line unless you know what you are doing */
/****************************************************************************/

/* Maximum internal buffer */
#define MAXBUF 256

/* Default startup name */
#define DEFAULT_STARTUP_NAME "ftmpi:services:startup_notify_service"

/* Default attempt to startup service */
#define SERVICE_ATTEMPT 2

/* Default time out to startup service */
#define SERVICE_TIMEOUT 10


#endif
