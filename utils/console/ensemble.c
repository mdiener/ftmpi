/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "param.h"
#include "utils.h"
#include "ns_lib.h"
#include "envs.h"
#include "proc.h"
#include "ensemble.h"
#include "internal_state_sys.h"
#include "libstartup.h"
#include "notifier.h"

/**
 conf - lists host in virtual machine 
 @return error number
*/
int conf(int flag)
{
    int group, nMember, rc, i;
    int *startupAddrs, *startupPorts, *entries;
    char startupName[MAXBUF];
    char tmpbuf[MAXBUF];
    struct startup_info sinfo;
    int mid,nsud;

    /** Assume initilizeNS is called before calling this function */

    sprintf(startupName,"%s:%s",vmname, DEFAULT_STARTUP_NAME);
    ns_open();
    rc = ns_info(startupName, 0, &group, &nMember, NULL, NULL, NULL);
    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    if(flag==ALLINFO) {
       nsud=startup_finder_vm(vmname);
    }

    if (nMember < 1) {
        printError(CONS_NOHOST, vmname);
	printHelp("add");
        return CONS_NOHOST;
    } else {
        startupAddrs = (int *) malloc((sizeof(int) * nMember));
        startupPorts = (int *) malloc((sizeof(int) * nMember));
        entries = (int *) malloc((sizeof(int) * nMember));

        ns_open();
        rc = ns_info(startupName, nMember, &group, &nMember, entries,
                     startupAddrs, startupPorts);
        ns_close();

        if (rc < 1) {
            free(startupAddrs);
            startupAddrs = NULL;
            free(startupPorts);
            startupPorts = NULL;
        } else {
            if(flag==ALLINFO && nsud!=rc) {
                sprintf(tmpbuf,"rc is %d nsud is %d\n",rc,nsud); 
                printFatal(__FILE__,__LINE__,tmpbuf);
	    }

            if (rc > 1) {
                printf("Found %d hosts\n", rc);
            } else {
                printf("Found %d host\n", rc);
            }
            printf("  HostID                          HOST    PORT");
	    if(flag==ALLINFO) {
                printf("           ARCH");
	    }
	    printf("\n");

            for (i = 0; i < rc; i++) {
                printf("%8d%30.29s%8d", entries[i],
                       cons_getHostname(startupAddrs[i]), startupPorts[i]);
		if(flag==ALLINFO) {
                    specific_startup_info(mid,i,&sinfo);
                    printf("%15.15s" , sinfo.arch);
		}
		printf("\n");
            }
        }
    }
    return CONS_SUCCESS;
}

/**
 add - add hosts to virtual machine
 @param hostname hostname
 @return error number
*/
int add(char *hostname)
{
    int er;
    char port[20];
    char *param[12];

    if (hostname == NULL) {
        printError(CONS_CMDARG, "add");
        printHelp("add");
        return CONS_CMDARG;
    }

    sprintf( port, "%d", HARNESS_NS_HOST_PORT );

    param[0] = strdup(RSHCOMMAND);
    param[1] = strdup("-n");
    param[2] = strdup(hostname);
    param[3] = strdup("startup_d");
    param[4] = strdup("-d");
    param[5] = strdup("-hn");
    param[6] = strdup(HARNESS_NS_HOST);
    param[7] = strdup("-hp");
    param[8] = strdup(port);
    param[9] = strdup(vmname);
    param[10] = strdup("&");
    param[11] = NULL;

    er = execvp(param[0], param);
    if (er < 1) {
        perror("add");
    }

    exit(CONS_SUCCESS);
    return CONS_SUCCESS;
}

/**
 deleteHost - delete hosts from virtual machine
 @param hostID hostID
 @return error number
*/
int deleteHost(int hostID)
{
    int mid;
    int rc;

    /** Assume initilizeNS is called before calling this function */

    ns_gid(NS_ID_OTHER,1,&mid);
    ns_close();

    startup_finder_vm(vmname);
    rc=startup_delete(hostID,mid);
    return rc;
}

/**
 addFromFile - add hosts to virtual machine from specific file
 @param filename filename
 @return error number
*/
int addFromFile(char *filename)
{
    FILE *fp;
    char buf[MAXBUF];
    char *cp;
    char *hostlist[MAXBUF];
    int i;
    int er;

    er = notifier_find ();
    if (er<0) {
        printError(CONS_NONOTIFY,NULL);
        return CONS_NONOTIFY;
    }

    if((fp=fopen(filename,"r"))==NULL) {
        perror(filename);
        return CONS_CMDARG;
    } 
    i=0;
    fgets(buf,MAXBUF,fp);
    while(!feof(fp)) {
        cp=strtok(buf,"\n");
	if(cp!=NULL) {
	    hostlist[i]=malloc(sizeof(char)*MAXBUF);
            strcpy(hostlist[i],cp);
	    i++;
	}
        fgets(buf,MAXBUF,fp);
    }
    fclose(fp);
    hostlist[i]=NULL;
    execCommands(hostlist,CONS_ADD,1);
    return CONS_SUCCESS;
}
