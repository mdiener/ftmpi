/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
			Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <locale.h>
#include <ctype.h>
#include <time.h>
#ifdef HAVE_READLINE
#include <readline/history.h>
#include <readline/readline.h>
#endif
#include "parsing.h"
#include "param.h"
#include "utils.h"
#include "envs.h"
#include "ns_lib.h"

/* Please make sure that MAX_HOLIDAY match number of item */
holiday_t holiday [MAX_HOLIDAY] = {
/* Day , Month, Year(4 digits), Message (Note: -1=any) */
	{1, 1,-1,"Happy New Year"},
	{4, 7,-1,"Happy the fourth of July"},
	{25,12,-1,"Merry Christmas :-)"},
};

/* Welcome message */
char *welcome_cs = "";
char *welcome_de = "Willkommen zur Harness/FT-MPI Konsole";
char *welcome_en = "Welcome to Harness/FT-MPI console.";
char *welcome_fr = "Bienvenue dans la console Harness/FT-MPI.";
char *welcome_th = "�Թ�յ�͹�Ѻ������ Harness/FT-MPI console";

/*
 * Command & Description
 + Syntax
*/
char *help[] = {
    "*add       Add hosts to virtual machine",
    "+add       add <hostname> [hostname1 hostname2 ...] \n\tor  add -f <filename>",
    "*alias     Alias command",
    "+alias     alias [<name> <cmd> <arg>]",
    "*conf      List hosts in virtual machine ",
    "+conf      conf [-a]",
    "+conf      -a is show all information",
    "*cwd       Set or print the current working directory",
    "+cwd       cwd [name]",
    "*delete    Delete hosts from virtual machine",
    "+delete    delete <hostid> [hostid1 hostid2 ...]",
    "*exit      Exit from console",
    "+exit      exit",
    "*expire    Expire process information",
    "+expire    expire <age (seconds)>",
    "*halt      Halt virtual machine",
    "+halt      halt",
    "*haltall   Halt virtual machine and stop all services",
    "+haltall   haltall",
    "*help      Display help message",
    "+help      help [command]",
#ifdef HAVE_READLINE
    "*history   Display list of command history",
    "+history   history",
#endif
    "*kill      Terminate process(es)",
    "+kill      kill <GID> [GID1 GID2 ...]",
    "*killall   Terminate all process(es) in runID",
    "+killall   killall <RunID> [RunID1 RunID2 ...]",
    "*mpispawn  Spawn MPI process(es)",
    "+mpispawn  mpispawn -np <number of process> [ <-option name> [option argument] ] <process name>",
    "+mpispawn  Option include :",
    "+mpispawn  -com_mode:\tBLANK, SHRINK, REBUILD, ABORT",
    "+mpispawn  \t\tBLANK: Communictor just marks lost nodes and continues",
    "+mpispawn  \t\tSHRINK: Communicator removes lost nodes and shrinks",
    "+mpispawn  \t\tREBUILD: Communicator is rebuilt with new nodes",
    "+mpispawn  \t\tABORT: Communicator errors lead to aborted application, ala MPI-1.2",
    "+mpispawn  -cwd <DIR>\tSet current working directory to DIR",
    "+mpispawn  -msg_mode:\tCONT or NOP",
    "+mpispawn  \t\tCONT: Message communication continues to all surviving nodes",
    "+mpispawn  \t\tNOP: Message communication does nothing so application can recover quicker",
    "+mpispawn  -o\t\tRedirect output",
    "+mpispawn  -s\t\tSilent startup mode",
    "*ps        Display process(es) status",
    "+ps        ps",
    "*quit      Quit from console",
    "+quit      quit",
    "*reset     Kill all tasks",
    "+reset     reset",
    "*service   Service management",
    "+service   service [service_name] [operation]",
    "+service   Service_name",
    "+service   \t\tall: all services",
    "+service   \t\tname_service: Naming service",
    "+service   \t\tftmpi_notifier: ftmpi notifier",
    "+service   Operation",
    "+service   \t\trestart: restart service",
    "+service   \t\tstart: start service",
    "+service   \t\tstatus: query service status",
    "+service   \t\tstop: stop service",
    "*sig       Send signal to process(es)",
    "+sig       sig <signum> <GID> [GID1 GID2 ...]",
    "*sigall    Send signal to all process(es) in runID",
    "+sigall    sigall <signum> <RunID> [RunID1 RunID2 ...]",
    "*spawn     Spawn process(es)",
    "+spawn     spawn -np <number of process> [ <-option name> [option argument] ] <process name>",
    "+spawn     Option include :",
    "+spawn     -com_mode:\tBLANK, SHRINK, REBUILD, ABORT",
    "+spawn     \t\tBLANK: Communictor just marks lost nodes and continues",
    "+spawn     \t\tSHRINK: Communicator removes lost nodes and shrinks",
    "+spawn     \t\tREBUILD: Communicator is rebuilt with new nodes",
    "+spawn     \t\tABORT: Communicator errors lead to aborted application, ala MPI-1.2",
    "+spawn     -cwd <DIR>\tSet current working directory to DIR",
    "+spawn     -mpi\t\tSpawn MPI job",
    "+spawn     -msg_mode:\tCONT or NOP",
    "+spawn     \t\tCONT: Message communication continues to all surviving nodes",
    "+spawn     \t\tNOP: Message communication does nothing so application can recover quicker",
    "+spawn     -o\t\tRedirect output",
    "+spawn     -s\t\tSilent startup mode",
    "*unalias   unalias command",
    "+unalias   unalias <name>",
    "*vmname    Set or print the current virtual machine name",
    "+vmname    vmname [name]",
    0
};

char *myLanguage;

/* Variable for color stuff */
char *welcome_begin;
char *welcome_end;
char *error_begin;
char *error_end;
char *help_begin;
char *help_end;
int myColor; /* 1= show color , 0 = don't show or non-support terminal */

/**
isEaster - Is today is Easter?  caculated by Gregorian Calendar (i.e. valid after 1752)
@param day [INPUT]
@param month [INPUT]
@param year [INPUT]
*/
int isEaster(int day,int month,int year)
{
   int golden, sunday, pfm;
   int easter, tmp;
   int solar, lunar;

   if(month != 3 && month !=4) { /* Easter should be in March or April only */
       return 0;
   }

   golden = (year % 19) + 1;
   /* Finding a Sunday */
   sunday = (year + (year/4) - (year/100) + (year/400)) % 7;
   if (sunday < 0) sunday += 7;

   /* The solar and lunar corrections */
   solar = (year - 1600)/100 - (year-1600)/400;
   lunar = (((year-1400)/100) * 8) / 25;

   /* The Paschal full moon */
   pfm = (3-(11*golden) + solar -lunar) % 30;
   if(pfm < 0) pfm += 30;
   if((pfm==29) || (pfm == 28 && golden > 11)) pfm--;

   tmp = (4-pfm-sunday) % 7;
   if(tmp < 0) tmp += 7;
   easter = pfm + tmp + 1;

   /* OK, Finally we got it :) */
   if(  ((easter< 11) && (month==3) && (day == (easter + 21))) ||
        ((easter>=11) && (month==4) && (day == (easter - 10)))  ) {
      return 1;
   }
   return 0;
}

/**
findLanguage - find our language
*/

int findLanguage(void)
{
  char *tmp, *pos;
  tmp = setlocale(LC_MESSAGES,"");
  /* tmp is keep in form of language[_territory][.codeset][@modifier]
   * see ISO 639 for the language code except when the locale is set to C.
   * Note: the result of setlocale is an opaque string.
   */
  if( tmp == NULL ) myLanguage = "en";
  else {
    pos = index(tmp, '_');
    if( pos == NULL ) myLanguage ="en";
    else {
      myLanguage = (char*)malloc( (pos - tmp + 1) * sizeof(char) );
      strncpy( myLanguage, tmp, (pos - tmp) );
    }
  }
  return 0;
}

/**
checkTerm - Is this terminal support color?
*/
int checkTerm(void)
{
   char *myterm;
   char tmp[MAXBUF];

   myColor=SHOWCOLOR;
   welcome_begin=strdup("");
   welcome_end=strdup("");
   error_begin=strdup("");
   error_end=strdup("");
   prompt_begin=strdup("");
   prompt_end=strdup("");
   help_begin=strdup("");
   help_end=strdup("");


   myterm=getenv("TERM");
   if(myterm!=NULL && myColor==1) {
       if(!strcmp(myterm,"console") ||
          !strcmp(myterm,"xterm") ||
          !strcmp(myterm,"ansi") ||
          !strcmp(myterm,"linux")) {

          sprintf(tmp,"[%sm",WELCOME_FONT);
	  welcome_begin=strdup(tmp);
          sprintf(tmp,"[%sm",ERROR_FONT);
	  error_begin=strdup(tmp);
          sprintf(tmp,"[%sm",PROMPT_FONT);
	  prompt_begin=strdup(tmp);
          sprintf(tmp,"[%sm",HELP_FONT);
	  help_begin=strdup(tmp);

          sprintf(tmp,"[0m");
	  welcome_end=strdup(tmp);
	  prompt_end=strdup(tmp);
	  error_end=strdup(tmp);
	  help_end=strdup(tmp);

          return 0;
       }
   }

   /* sorry your terminal doesn't support color ;p */
   myColor=0;
   return 0;
}

/**
printWelcome - Print welcome message
*/
int printWelcome(void)
{

   if(!strcmp(myLanguage,"cs")) { /* Czech */
       printf("\n%s%s%s\n",welcome_begin,welcome_cs,welcome_end);
   } else if(!strcmp(myLanguage,"de")) { /* German */
       printf("\n%s%s%s\n",welcome_begin,welcome_de,welcome_end);
   } else if(!strcmp(myLanguage,"fr")) { /* France */
       printf("\n%s%s%s\n",welcome_begin,welcome_fr,welcome_end);
   } else if(!strcmp(myLanguage,"th")) { /* Thai */
       printf("\n%s%s%s\n",welcome_begin,welcome_th,welcome_end);
   } else { /* default is English */
       printf("\n%s%s%s\n",welcome_begin,welcome_en,welcome_end);
   }
   return 0;
}

/**
printHoliday - Print holiday message
*/
int printHoliday(void)
{
   struct tm *today;
   time_t tp;
   int i;

   time(&tp);
   today=gmtime(&tp);
   today->tm_year = today->tm_year + 1900;
   today->tm_mon = today->tm_mon + 1;

   /*
   printf("Today : %d/%d/%d\n",today->tm_mon,today->tm_mday,today->tm_year);
   */

   if(isEaster(today->tm_mday,today->tm_mon,today->tm_year)) {
      printf("%s- Happy Easter day%s\n",welcome_begin,welcome_end);
   }
   for(i=0;i<MAX_HOLIDAY;i++) {
      if(  (holiday[i].day == -1 || holiday[i].day == today->tm_mday) &&
           (holiday[i].month == -1 || holiday[i].month == today->tm_mon) &&
           (holiday[i].year == -1 || holiday[i].year == today->tm_year) ) {
           printf("%s- %s%s\n",welcome_begin,holiday[i].message,welcome_end);
      }

   }
   printf("\n");
   return 0;
}

/**
printFatal - print fatal bug message
@param filename filename
@param line line
@param message
*/

int printFatal(char *filename, int line, char *message)
{
    printf("\n\n*************************************************\n");
    printf("* FATAL ERROR!! \n");
    printf("* Please e-mail to angskun@cs.utk.edu. Thank you\n" );
    printf("*************************************************\n");
    printf("\nFILE : %s\nLINE : %d\nERROR: %s\n", filename,line,message);
    printf("*************************************************\n\n");
    return 0;
}


/**
printError - Print Error Message
@param err Error
@param param Parameter of error
*/
int printError(int err,char *param)
{
    switch(err) {
        case CONS_INVLSYN : printf("%sSyntax error: `%s' unexpected.%s\n",error_begin,param,error_end); break;
	case CONS_INVLSEG : printf("%sInternal error: Segmentation Fault%s\n",error_begin,error_end); break;
        case CONS_INVLCMD : printf("%s%s: Command not found. Please see help.%s\n",error_begin,param,error_end); break;
        case CONS_PROGARG : printf("%s [-f <hostlist file>] [-x <cmds file>] [virtual_machine]\n",param); break;
        case CONS_INVLHLP : printf("%sHelp: Unknown command \"%s\"%s\n",error_begin,param,error_end); break;
        case CONS_NOHOST  : printf("%sNo host available in \"%s\". Please see \"add\" command.%s\n",error_begin,param,error_end); break;
        case CONS_NONS    : printf("%sCan not contact name services. Please also make sure that ftmpi_notifier is running%s\n",error_begin,error_end); break;
        case CONS_CMDARG  : printf("%s%s: Invalid argument.%s\n",error_begin,param,error_end); break;
        case CONS_INVLNP  : printf("%s%s: Invalid number of process.%s\n",error_begin,param,error_end); break;
	case CONS_INVLP   : printf("%sProcess %s not found%s\n",error_begin,param,error_end); break;
	case CONS_INVLKL  : printf("%s%s%s\n",error_begin,param,error_end); break;
	case CONS_INVLST  : printf("%sProcess %s is not running%s\n",error_begin,param,error_end); break;
        case CONS_INVLHID : printf("%sInvalid host ID %s.%s\n",error_begin,param,error_end); break;
        case CONS_INVLGID : printf("%sInvalid process ID %s.%s\n",error_begin,param,error_end); break;
        case CONS_INVLAGE : printf("%sInvalid age %s.%s\n",error_begin,param,error_end); break;
        case CONS_AMBICMD : printf("%sAmbiguous command %s.%s\n",error_begin,param,error_end); break;
        case CONS_INVLWMPI: printf("%sCannot use option %s without -mpi.%s\n",error_begin,param,error_end); break;
	case CONS_INVLCMA : printf("%s%s: Invalid communicator mode (com_mode)%s\n",error_begin,param,error_end); break;
	case CONS_INVLMMA : printf("%s%s: Invalid message mode (msg_mode)%s\n",error_begin,param,error_end); break;
	case CONS_INVLANS : printf("%sPlease answer 'y' or 'n'%s\n",error_begin,error_end); break;
	case CONS_CONNPROB: printf("%sConnection problem with host ID %s%s\n",error_begin,param,error_end); break;
        case CONS_INVLRID : printf("%sInvalid Run ID %s.%s\n",error_begin,param,error_end); break;
	case CONS_INVLEXE : printf("%s%s: Invalid executable file%s\n",error_begin,param,error_end); break;
	case CONS_INVLCWD : printf("%s%s: Invalid current working directory%s\n",error_begin,param,error_end); break;
	case CONS_INVLENV : printf("%sNo %s specified%s\n",error_begin,param,error_end); break;
        case CONS_NONOTIFY: printf("%sCan not contact ftmpi_notifier. Please make sure that ftmpi_notifier is running before add new hosts.%s\n",error_begin,error_end); break;
	case CONS_INVLSID : printf("%sUnknown services %s%s\n",error_begin,param,error_end); break;
	case CONS_INVLOPR : printf("%sInvalid operation %s%s\n",error_begin,param,error_end); break;
	case CONS_INVLDEP : printf("%sCannot stop/restart the service while %s is still running%s\n",error_begin,param,error_end); break;
	case CONS_GETCWDIR: printf("%sCannot get current working directory%s\n",error_begin,error_end); break;
	case CONS_INVLALN : printf("%s%s: alias name not found%s\n",error_begin,param,error_end); break;
        default: printf("%sUnknown Error !!%s\n",error_begin,error_end); break;
    };
    return 0;
}


/**
printHelp - Print Help Message
@param command command
*/
int printHelp(char *command)
{
    size_t len;
    int found, first;
    char **p, *tmp;

    found=0;
    first=0;

    if(command!=NULL) {
        len=strlen(command);
        for(p = help; *p; p++) {
            if(!strncmp(command,*p + 1,len)) {
                if(**p == '*') {
                    tmp = (*p) + len + 1;
		    if(*tmp!=' ') {
                        continue;
		    }
                    while(*tmp == ' ') {
                        tmp=tmp+1;
                    }
                    printf("Command: %s\n",command);
                    printf("Description: %s\n",tmp);
                    found=1;
                } else if(**p == '+') {
                    tmp=*p + len + 1;
		    if(*tmp!=' ') {
                        continue;
		    }
                    while(*tmp == ' ') {
                        tmp=tmp+1;
                    }
		    if(first==0) {
                        printf("Syntax: %s\n",tmp);
			first=1;
		    } else {
                        printf("        %s\n",tmp);
		    }
                    found=1;
                }
            }
        }
        if(found==0) {
            printError(CONS_INVLHLP,command);
        }
    } else {

        printf("%sCommand   Description%s\n",help_begin,help_end);
        printf("%s=====================%s\n",help_begin,help_end);

        for(p = help; *p; p++) {
            if(!strncmp("*",*p,1)) {
                printf("%s\n",(*p) + 1);
            }
        }
    }

    return 0;
}

/**
confirmCommand - confirm important command
@param command command
@retval
*/
int confirmCommand(IS input,char *command)
{
    char tmpbuf[MAXBUF];
    char *cp;

    if(!strcmp(input->fields[0],command)) {
       if(input->NF!=1) {
           printError(CONS_CMDARG,command);
	   printHelp(command);
	   return -1;
       } else {
           for(;;) { /* some compiler don't like while(1)...try intel */
              printf("Do you want to %s system? [y/n] ",command);
              fgets(tmpbuf,MAXBUF,input->f);
	      cp=strtok(tmpbuf," \t\n");
	      if(cp!=NULL) {
	          if(!strcmp(cp,"y")) {
		      return 0;
	          } else if(!strcmp(cp,"n")) {
		      return -1;
		  }
	      }
	      printError(CONS_INVLANS,NULL);
	   }
       }
    }
    return 0;
}

/**
checkFields - Check error of input from parsing and manage abbreviate command.
@param input Input
@retval -1 Invalid input
@retval 0 Valid input
@retval 1 Exit command
*/
int checkFields(IS input)
{

    /* Warning: Please keep this function away from your kids. It contains a lot of pointers. */

    char **p;
    int match;
    char *cp;
    char orgbuf[MAXBUF];
    char tmpbuf[MAXBUF];
    char newtext2[MAXLEN];
    char *line;
    char lastchar;
    size_t cplen;
    size_t orgbuflen;
    size_t i,j;
    int rc;
    int done;
#if HAVE_READLINE
    char *expansion;
    int result;
    static char prev[MAXBUF]="";
#endif

    match=0;
    if(!strcmp(input->fields[0],";")) {
        printError(CONS_INVLSYN,";");
        return -1;
    }


#ifdef HAVE_READLINE
		   using_history ();
		   result = history_expand (input->text1, &expansion);
		   if (result) {
                       if(result < 0) {
		          printf ("%s%s%s\n", error_begin, expansion , error_end );
		       } else {
		          printf ("%s\n", expansion );
		       }
		   }

		   if (result < 0 || result == 2) {
		       free (expansion);
		       return -1;
                   } else {
                         if(strcmp(expansion,prev)) {
                             if(!strncmp(input->name,"stdin",5)) { /* don't add history from script or rc file */
		                add_history (expansion);
				strncpy(prev,expansion,strlen(expansion));
				prev[strlen(expansion)]='\0';
			     }
			 }
		         strncpy (input->text1, expansion, sizeof (input->text1) - 1);
		         strncpy (orgbuf, expansion, sizeof (orgbuf) - 1);
		         free(expansion);
		  }

                   j=0;
                   line=orgbuf;
		   lastchar = ' ';
		   for (i = 0; line[i] != '\0' && i < MAXLEN-1; i++) {
		       if (isspace((int)line[i])) {
		           lastchar = line[i];
			   line[i] = '\0';
			} else {
			   if (isspace((int)lastchar)) {
			        input->fields[j] = line+i;
				j++;
			   }
	                   lastchar = line[i];
		        }
		    }
		    input->NF=j;
#endif

		    strcpy(orgbuf,input->fields[0]);


    for(p = help; *p; p++) {
       if(!strncmp("*",*p,1)) {


           if(!strncmp(orgbuf,(*p)+1,strlen(orgbuf))) {
               if(match==0) {
                   strcpy(tmpbuf,(*p)+1);
	           cp=strtok(tmpbuf," ");
		   memset(newtext2,0,MAXLEN);
                   strcpy(newtext2,cp);
		   orgbuflen=strlen(orgbuf);
		   cplen=strlen(newtext2);
		   j=orgbuflen;
                   orgbuflen=strlen(input->text1);
		   for(i=cplen; j<orgbuflen;i++) {
                      newtext2[i]=input->text1[j];
		      j++;
		   }
		   memcpy(input->text2,newtext2,MAXLEN);
                   match++;
	       } else {
                  /* give them second chance in case of perfectly match */
                  done=0;
                  for(p = help; *p; p++) {
                     if( (!strncmp(orgbuf,(*p)+1,strlen(orgbuf))) && (*((*p)+1+strlen(orgbuf))==' ')) {
                         done=1;
			 break;
	             }
		  }
		  if(done==1) {
                      break;
		  }

                  printError(CONS_AMBICMD,orgbuf);
                  printf("%sCommand   Description%s\n",help_begin,help_end);
                  printf("%s=====================%s\n",help_begin,help_end);
                  for(p = help; *p; p++) {
                      if(!strncmp(orgbuf,(*p)+1,strlen(orgbuf)) && !(strncmp("*",*p,1))) {
                          printf("%s\n",(*p) + 1);
		      }
		  }
                  return -1;
	       }
	   }
       }
    }

                   j=0;
                   line=input->text2;
		   lastchar = ' ';
		   for (i = 0; line[i] != '\0' && i < MAXLEN-1; i++) {
		       if (isspace((int)line[i])) {
		           lastchar = line[i];
			   line[i] = '\0';
			} else {
			   if (isspace((int)lastchar)) {
			        input->fields[j] = line+i;
				j++;
			   }
	                   lastchar = line[i];
		        }
		    }
		    input->NF=j;



    if(!strcmp(input->fields[0],"exit") || !strcmp(input->fields[0],"quit")) {
        return 1;
    }

    /* Bypass confirmation if we get input from script */

    if(input->script==0) {
       rc=confirmCommand(input,"haltall");
       if(rc!=0) {
          return rc;
       }
       rc=confirmCommand(input,"halt");
       if(rc!=0) {
          return rc;
       }
       rc=confirmCommand(input,"reset");
       if(rc!=0) {
          return rc;
       }
    }

    return 0;
}

/**
findDelim - Find delimeter position of each process.
@param firstPos First position of process in input
@param input Input
@return position of delimeter
*/
int findDelim(int firstPos,IS input)
{
    int i;
    for(i=firstPos;i<input->NF;i++) {
        if(!strcmp(input->fields[i],";")) {
            i++;
            break;
        }
    }
    return i;
}



/**
 cons_getHostname - getHostname from IP(v4)
 @param addr address
 @return hostname
*/
char * cons_getHostname(int addr)
{
    struct hostent *hp;
    struct in_addr saddr;
    saddr.s_addr=addr;

    hp = gethostbyaddr((char *) &addr, sizeof(addr), AF_INET);
    if (hp==NULL) {
        return inet_ntoa(saddr);
    } else {

#ifdef ENABLE_ALIASES    /* Enable alias hostname */
        if(hp->h_aliases[0]!=NULL) {
            return hp->h_aliases[0];
        }
#endif

        if(hp->h_name !=NULL) {
            return hp->h_name;
        } else {
            return inet_ntoa(saddr);
        }
    }
}

/**
init_data - intialize data structure
@param tab table
#return error number
*/
int init_data(DATATAB **tab)
{
   (*tab)=(DATATAB *)malloc(sizeof(DATATAB));
   strcpy((*tab)->key,"");
   strcpy((*tab)->value,"");
   (*tab)->next=NULL;
   return 0;
}

/**
add_data - add key and value pair to data structure
@param table table
@param key key (null terminate string)
@param value value (null terminate string)
@return error number
*/
int add_data(DATATAB *table, char *key,char *value)
{
   DATATAB *tab;
   DATATAB *tmp;
   int result;

   if( (strlen(key) > MAXBUF) || (strlen(value) >= MAXBUF) ) {
      return -1;
   }
   tab=table;

   /* FIXME: Please change the link-listed to faster data structure (e.g. red-black tree). */

   while(tab->next!=NULL) {
      result=strcmp(key,tab->next->key);
      if(result==0) {
	 /* Duplicate entry, replace it */
         strncpy(tab->next->key,key,MAXBUF);
         strncpy(tab->next->value,value,MAXBUF);
	 return 0;
      } else if(result<0) {
         tmp=tab->next;
         tab->next=(DATATAB *)malloc(sizeof(DATATAB));
         strncpy(tab->next->key,key,MAXBUF);
         strncpy(tab->next->value,value,MAXBUF);
         tab->next->next=tmp;
	 return 0;
      } else {
         tab=tab->next;
      }
   }
   /* Insert first or last entry */
   tab->next=(DATATAB *)malloc(sizeof(DATATAB));
   strncpy(tab->next->key,key,MAXBUF);
   strncpy(tab->next->value,value,MAXBUF);
   tab->next->next=NULL;
   return 0;
}

/**
del_data - delete key from the data structure
@param table table
@param key key (null terminate string)
@retval 0 success
@retval <0 fail
*/

int del_data(DATATAB *table, char *key)
{
   DATATAB *tab;
   DATATAB *tmp;
   int result;

   tab=table;

   /* FIXME: Please change the link-listed to faster data structure (e.g. red-black tree). */

   while(tab->next!=NULL) {
      result=strcmp(key,tab->next->key);
      if(result==0) {
         tmp=tab->next->next;
         free(tab->next);
         tab->next=tmp;
	 return 0;
      } else if(result<0) {
	 return -1;
      } else {
         tab=tab->next;
      }
   }
   return -1;
}

/**
print_data - print the data structure
@param table table
@return error number
*/
int print_data(DATATAB *table)
{
   DATATAB *tab;

   tab=table;

   if(tab==NULL) return 0;
   if(tab->next==NULL) return 0;

   tab=tab->next; /* skip first node */

   while(tab->next!=NULL) {
      printf("[%s] = [%s]\n",tab->key,tab->value);
      tab=tab->next;
   }
   printf("[%s] = [%s]\n",tab->key,tab->value);
   return 0;
}

/**
get_data - get value from the key (also match partial command)
@param table table
@param key key
@return value (or NULL, if not found);
*/
char *get_data(DATATAB *table, char *key)
{
   DATATAB *tab;
   int result;
   char *retresult;
   int len;
   int retlen;
   int keylen;

   tab=table;

   while(tab->next!=NULL) {
      len=strlen(tab->next->key);
      result=strncmp(key,tab->next->key,len);
      if(result==0) {
          retlen=strlen(tab->next->value);
          retresult=(char *)malloc(retlen+1);
	  strncpy(retresult,tab->next->value,retlen);
          retresult[retlen]='\0';
	  keylen=strlen(key);
	  if((keylen-len) > 0) {
             retresult=(char *)realloc(retresult,(retlen+(keylen-len)+1));
	     memcpy(&retresult[retlen],&key[len],(keylen-len));
             retresult[retlen+(keylen-len)]='\0';
	  }
	  return retresult;
      } else if(result <0) { /* not found */
          return NULL;
      } else {
          tab=tab->next;
      }
   }
   return NULL;
}


#ifdef HAVE_READLINE
/**
listHistory - List command histories.
@return error number
*/
int listHistory(void)
{
   HIST_ENTRY **hist_list;
   int i;

   hist_list = history_list();
   if(hist_list) {
       for(i=0; hist_list[i]; i++) {
            printf("%5d  %s\n",(i + history_base), hist_list[i]->line);
       }
   }
   return 0;
}


/* Generator function for command completion.  STATE lets us know whether
 *    to start from scratch; without any state (i.e. STATE == 0), then we
 *       start at the top of the list. */
char * command_generator (char *text, int state)
{
   static int list_index;
   static size_t len;
   char tmpbuf[MAXBUF];
   char **p;
   char *cp;
   int i;

   /* If this is a new word to complete, initialize now.  This includes
    *      saving the length of TEXT for efficiency, and initializing the index
    *           variable to 0. */
   if (!state) {
	list_index = 0;
	len = strlen (text);
   }

   i=0;
   /* Return the next name which partially matches from the command list. */
   for(p = help; *p; p++) {
        if(**p == '*') {
            if(!strncmp(text,*p + 1,len)) {
                if(i<list_index) {
		   i++ ;
		} else {
                   list_index++;
                   strcpy(tmpbuf,(*p)+1);
		   cp=strtok(tmpbuf," ");
		   return strdup(cp);
		}
	    }
        }
        /* If no names matched, then return NULL. */
   }
   return ((char *)NULL);
}


char ** auto_completion (char *text, int start)
{
   char **matches;
   matches = (char **)NULL;
   rl_attempted_completion_over = 1;

   /* If this word is at the start of the line, then it is a command
    *      to complete.  Otherwise it is the name of a file in the current
    *           directory. */
   if (start == 0) matches = (char **) rl_completion_matches (text, (rl_compentry_func_t *) command_generator);
   return (matches);
}

/**
initialize_readline - initialize_readline
*/
int initialize_readline (void)
{
    rl_readline_name = "Harness";
    rl_attempted_completion_function = (rl_completion_func_t *)auto_completion;
    return 0;
}


#endif

/**
statusToString - get string status.
@return error string
*/
char *statusToString(int status) {
  switch (status) {
      case STATUS_INVALID: return "invalid";
      case STATUS_RUNNING: return "running";
      case STATUS_EXITED: return "exited";
      case STATUS_FAILED_ON_EXEC: return "failed on exec";
      default: return "unknown status code";
  }
}
