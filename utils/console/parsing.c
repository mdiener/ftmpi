/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "param.h"
#include "parsing.h"
#include "utils.h"

#ifdef HAVE_READLINE
#include <readline/readline.h>

#endif

/**
parsing_init - Initialize structure for parsing
@param filename Input Filename (NULL for STDIN)
@return input_structure
*/

IS parsing_init(char *filename)
{
  IS is;
  
  is = (struct input *)malloc(sizeof(struct input));

  is->text1[MAXLEN-1] = '\0';
  is->NF = 0;
  is->line = 0;
  if (filename == NULL) {
    is->name = "stdin";
    is->f = stdin;
    is->script = 0;
  } else {
    is->script = 1;
    is->name = filename;
    is->f = fopen(filename, "r");
    if (is->f == NULL) {
      free(is);
      return NULL;
    }
  }
  return is;
}

/**
parsing_getLine - get a line.
@param is intialized input structure.
@return >=0 number of fields
@return -1 error
*/
long parsing_getLine(IS is)
{
  size_t i;
  size_t len;
  
  char lastchar;
  char *line;
  char *temp2;
#ifdef HAVE_READLINE
  char prompt[256];
  char *temp;
#endif

#ifdef HAVE_READLINE
  if(!strcmp(is->name,"stdin") && isatty(fileno(is->f)) ) {
     /* check for real tty to prevent warning from readline */
     sprintf(prompt,"%s%s%s",prompt_begin,DEFAULT_PROMPT,prompt_end);
     temp=readline(prompt);  
     if(!temp) {
        return -1; 
     }
     strcpy(is->text1,temp);
  } else {
     if (fgets(is->text1, MAXLEN-1, is->f) == NULL) {
         is->NF = -1;
         return -1;
     }
  }
#else
  if (fgets(is->text1, MAXLEN-1, is->f) == NULL) {
    is->NF = -1;
    return -1;
  }
#endif

  /* 
   Before do something else. Let's get rid of white space in front of each line. 
   */
  
  len=strlen(is->text1);
  for(i=0;i<len;i++) {
     if(is->text1[i]!=' ' && is->text1[i]!='\t') {
        break;
     }
  }
  if(i!=0) { /* we have some white space */
     memmove(is->text1,&is->text1[i],len-i);
     is->text1[len-i]='\0';
  }


  /* alias command */

  (void)strtok(is->text1,"\n");
  temp2=get_data(table_alias,is->text1);
  if(temp2!=NULL) {
     strcpy(is->text1,temp2);
     free(temp2);
  }

  is->NF = 0;
  is->line++;
  strcpy(is->text2, is->text1);

  line = is->text2;
  lastchar = ' ';
  for (i = 0; line[i] != '\0' && i < MAXLEN-1; i++) {
    if (isspace((int)line[i])) {
      lastchar = line[i];
      line[i] = '\0';
    } else {
      if (isspace((int)lastchar)) {
        is->fields[is->NF] = line+i;
        is->NF++;
      }
      lastchar = line[i];
    }
  }

  return is->NF;
}

/**
parsing_finalize - destroy input structure.
@param is input_structure.
*/
void parsing_finalize(IS is)
{
  if (is->f != stdin) {
      fclose(is->f);
  }
  free(is);
  return;
} 
