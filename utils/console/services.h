/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>

#include "notifier.h"
#include "param.h"
#include "utils.h"
#include "envs.h"
#include "ns_lib.h"

/* service ID */
enum {
SERVICE_NOTIFIER, 
SERVICE_NS,
MAX_SERVICE
};

typedef int (*pt2Function)(void);

typedef struct {
    char name[MAXBUF];
    pt2Function status_f;
    pt2Function shutdown_f;
} services_t;

int service_notifier_status(void);
int service_NS_status(void);
int service_notifier_shutdown(void);
int service_NS_shutdown(void);
int service_autostart(int service_id,char *host,int force);
int service_getID(char *service_name);
int service_run(int service_id,char *operation,int force);
int service_all(char *operation);
int service_start(char *service_name, char *host);
int service_notifier_clean(void);

extern services_t services[MAX_SERVICE];
extern int init_ns_called;
