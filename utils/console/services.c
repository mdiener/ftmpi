/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <netdb.h>
#include "snipe_lite.h"
#include "services.h"

/* List of Known service */
/* Please also add service ID in service header file */

services_t services[MAX_SERVICE] = {
	{ "ftmpi_notifier", service_notifier_status, service_notifier_shutdown } ,
	{ "name_service",  service_NS_status, service_NS_shutdown } ,
};

int init_ns_called=0;

/**
service_start - start service (The service must be in form of <service_name> -d)
@return status
*/
int service_start(char *service_name, char *host)
{
    struct hostent *hp;
    struct in_addr in;
    char *command[6];
    char **p;
    int status;
    int pid;
    int er;
    int remoteIP;

    hp = gethostbyname (host);
    if(hp==NULL) {
       return -999;
    }

    p = hp->h_addr_list;
    (void) memcpy(&in.s_addr, *p, sizeof (in.s_addr));
    remoteIP =(int) in.s_addr;

    if(remoteIP != get_my_addr()) {
       command[0]=strdup(RSHCOMMAND);
       command[1]=strdup("-n");
       command[2]=strdup(host);
       command[3]=strdup(service_name);
       command[4]=strdup("-d");
       command[5]=NULL;
    } else {
       command[0]=strdup(service_name);
       command[1]=strdup("-d");
       command[2]=NULL;
    }

    pid=fork();
    if(pid==0) {
        er=execvp(command[0],command);
        if(er<1) {
            perror(RSHCOMMAND);
        }
    } else {
        waitpid(pid,&status,0);
	if( WIFEXITED(status) )
	  if( WEXITSTATUS(status) == 0 ) return 0;
    }
    return -1;
}


/** 
service_NS_status - query status of the name service
@return status
*/
int service_NS_status(void)
{
       if(init_ns_called) return 0;

       if (!HARNESS_NS_HOST) {
            printError(CONS_INVLENV,"HARNESS_NS_HOST");
            return CONS_INVLENV;
       }

       if (!HARNESS_NS_HOST_PORT) {
           printError(CONS_INVLENV,"HARNESS_NS_HOST_PORT");
	   return CONS_INVLENV;
       }

       if( ns_init(HARNESS_NS_HOST,HARNESS_NS_HOST_PORT) != 0 )
	 return -1;
       init_ns_called=1;
       return 0;
}

/** 
service_notifier_cleanup - clean up notifier record in the NS
@return error number 
*/
int service_notifier_clean(void)
{
      char notifyname[256];
      int t0, t1;
      int e, a, p;
      int rc;

      sprintf(notifyname, "ftmpi:services:notifier");
      ns_open ();
      rc = ns_info (notifyname, 1, &t0, &t1, &e, &a, &p);
      if(rc >=1) {
          ns_del(notifyname,t0,0);
      }
      ns_close ();
      return 0;
}

/** 
service_notifier_status - query status of the ftmpi_notifier
@return status
*/
int service_notifier_status(void)
{
     int ret;
     ret=service_NS_status();
     if(ret >= 0) {
        return notifier_check(0);
     }
     return -1;
}

/** 
service_notifier_shutdown - shutdown ftmpi_notifier
@return error number
*/
int service_notifier_shutdown(void)
{
      return notifier_shutdown(1111);
}

/**
service_notifier_shutdown - shutdown ftmpi_notifier
*/
int service_NS_shutdown(void)
{
     ns_open();
     ns_halt();
     return 0;
}

/**
service_getID - query service ID
@param service_name service name
@retval >=0 service ID 
@retval <0 error 
*/
int service_getID(char *service_name)
{
    int i;
    for(i=0;i<MAX_SERVICE;i++) {
        if(!strcmp(service_name,services[i].name)) {
            return i;
	}
    }
    return CONS_INVLSID ;
}

/**
service_autostart - auto start service
@param service_id service identifier
@param host host
*/ 
int service_autostart(int service_id,char *host,int force)
{
    int i,j, ret;

    if(service_id < 0 || service_id >= MAX_SERVICE) {
       return CONS_INVLSID;
    }

    ret=-1;
    if(force!=2) {
       ret=services[service_id].status_f();
       if(ret==CONS_INVLENV || ret==CONS_SUCCESS) { 
          return ret;
       }
    }

    if(host==NULL) {
        return CONS_INVLHID;
    }

    if(service_id==SERVICE_NOTIFIER) {
       service_notifier_clean();
    }

    printf("Starting %s  ",services[service_id].name);
    fflush(stdout);

    i=0; j=0;

    while(ret < 0 && i < SERVICE_ATTEMPT) {
        ret = service_start(services[service_id].name,host);
	printf("."); fflush(stdout);
        if( ret != 0 ) break;
        ret=services[service_id].status_f();
	while(ret <0 && j < SERVICE_TIMEOUT) {
	   sleep(1);
           ret=services[service_id].status_f();
	   j++;
	}
	i++;
    }
    if(ret<0) {
        printf("Failed\n");
        fflush(stdout);
    } else {
        printf("Success\n");
        fflush(stdout);
    }
    return ret;
}

/**
service_run - run service depend on opration
@param service_id service identifier
@param operation operation
@param force force flag
*/
int service_run(int service_id, char *operation, int force)
{
   int ret;

   if(service_id < 0 || service_id >= MAX_SERVICE) {
       return CONS_INVLSID;
   }

   if(operation==NULL) {
       printError(CONS_INVLOPR,operation);
       printHelp("service");
       return CONS_INVLOPR;
   }

   if(!strcmp(operation,"start")) {
       return service_autostart(service_id,HARNESS_NS_HOST,force); 
   } else if(!strcmp(operation,"stop")) {
      if(!force) {
          /* check dependecy */
          if(service_id==SERVICE_NS) {
             ret=services[SERVICE_NOTIFIER].status_f();
	     if(ret >= 0) {
                 printError(CONS_INVLDEP,"ftmpi_notifier");
                 return CONS_INVLDEP;
	     }
	  }
      }
      ret=services[service_id].status_f();
      if(ret >=0) {
         return services[service_id].shutdown_f();
      } 
      return CONS_SUCCESS;
   } else if(!strcmp(operation,"status")) {
      printf("%s is ",services[service_id].name);
      ret=services[service_id].status_f();
      if(ret<0) {
         puts("stopped");
         if(service_id==SERVICE_NOTIFIER) {
             service_notifier_clean();
         }
      } else {
         puts("running");
      }
      return CONS_SUCCESS;
   } else if(!strcmp(operation,"restart")) {
      ret=service_run(service_id,"stop",0);
      if(ret>=0) {
          service_run(service_id,"start",2);
      }
      return CONS_SUCCESS;
   }

   return CONS_INVLOPR;
}


/**
service_all - all service management
@param operation operation
*/
int service_all(char *operation)
{
   int i, ret;
   if(!strcmp(operation,"restart")) {
      service_all("stop");
      sleep(2); 
      service_all("start");
   } else if(!strcmp(operation,"start")) {
      for(i=MAX_SERVICE-1;i>=0;i--) {
         ret=service_run(i,operation,1);
         if(ret!=CONS_SUCCESS) {
	    return ret;
         }
      }
   } else {
      for(i=0;i<MAX_SERVICE;i++) {
         ret=service_run(i,operation,1);
         if(ret!=CONS_SUCCESS) {
	    return ret;
         }
      }
   }
   return 0;
}
