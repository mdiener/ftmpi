/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/
#ifndef PROC_H
#define PROC_H

#define CONS_ADD 1
#define CONS_KILL 2
#define CONS_DELETE 3
#define CONS_SIG 4
#define CONS_EXPIRE 5
#define CONS_KILLALL 6
#define CONS_SIGALL 7

int execCommand(int firstPos, IS input);
int waitProcess(int *nChild, int pid);
int execCommands(char **param,int command,int option);
int resetSystem(void);
int haltSystem(void);
int killProcess(int gid, int signal,int flag);
int spawnProcess(char *spawn_cmd, int argc, char **argv);
int processStatus(void);
int expireProcess(int age);
int execCommands(char **param, int cmd, int option);
int execCommand(int firstPos, IS input);
int waitProcess(int *nChild, int pid);

#endif
