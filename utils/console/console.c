/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun <angskun@cs.utk.edu> 

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "parsing.h"
#include "param.h"
#include "ensemble.h"
#include "utils.h"
#include "proc.h"
#include "services.h"

#ifdef HAVE_READLINE
#include <readline/history.h>
#endif

char vmname[MAXBUF];
char hn_cwdir[MAXBUF];
char *prompt_begin;
char *prompt_end;

DATATAB *table_alias = NULL;

/**
console_init - initialize console
*/
int console_init(void)
{
   int err;

   init_envs();
   get_envs();
   findLanguage();
   checkTerm();
#ifdef HAVE_READLINE
   initialize_readline();
#endif

   if(!HARNESS_NS_HOST) {
       printError(CONS_INVLENV,"HARNESS_NS_HOST");
       return CONS_INVLENV;
   }

   err=service_autostart(SERVICE_NS,HARNESS_NS_HOST,0);
   if(err!=CONS_SUCCESS) {
	exit(err);
   }

   err=service_autostart(SERVICE_NOTIFIER,HARNESS_NS_HOST,0);
   if(err!=CONS_SUCCESS) {
	exit(err);
   }

   return 0;
}

/** 
main - Main function of console
@param argc argument count
@param argv argument value
*/
int main(int argc,char **argv)
{
    IS input;                       
    int err = 0;
    int firstPos;        /* first position of input in input stream */
    int nextPos;
    int dvm;
    int next;
    int state;
    char* machinefile = NULL, *scriptname = NULL;
    char* scname=NULL;
    char rcfile[MAXBUF];
    struct stat statbuf;
    char prompt[MAXBUF];
    char* end_prompt;

    signal(SIGPIPE,SIG_IGN); /* just in case */

    init_data(&table_alias);

    dvm=1;
    next=1;
    state=0;

    if(getenv("HOME")!=NULL) {
       sprintf(rcfile,"%s/%s",getenv("HOME"),CONSOLERC);
       if(stat(rcfile,&statbuf)==0) {
          state=2;
	  scriptname= rcfile;
       }
    }


    while(next<argc) {
        if(!strcmp(argv[next],"-f")) {
	   if(argv[next+1]==NULL) {
               printError(CONS_PROGARG,basename(argv[0]));
	       exit(CONS_PROGARG);
	   } else {
	       machinefile = argv[next+1];
               next=next+2;
	       dvm=dvm+2;
	   }
	} else if(!strcmp(argv[next],"-x")) {
	   if(argv[next+1]==NULL) {
               printError(CONS_PROGARG,basename(argv[0]));
	       exit(CONS_PROGARG);
	   } else {
               if(state==2) {
	          scname = argv[next+1];
	       } else {
                  state=1;
	          scriptname = argv[next+1];
	       }
	   }
           next=next+2;
	   dvm=dvm+2;
	} else {
           next++;
	}
    } 

    console_init();
    printWelcome();
    printHoliday();
    
    /* have a nice line output for console running in a script */
    end_prompt = prompt_end;
    if( ((int)fileno(stdin)) >= 0 ) {
      if( !isatty(fileno(stdin)) ) {
	if( scriptname == NULL ) {
	  end_prompt = malloc( strlen(prompt_end) + 2 );
	  sprintf( end_prompt, "%s\n", prompt_end );
	}
      }
    }

    if(argv[dvm]!=NULL) {
        strcpy(vmname,argv[dvm]);
    } else { 
        strcpy(vmname,DEFAULT_VIRTUAL_MACHINE);
    }

    if(getcwd(hn_cwdir,MAXBUF)==NULL) { /* call 911 please...  */
       printError(CONS_GETCWDIR,NULL);
       exit(CONS_GETCWDIR);
    }

    if( machinefile != NULL ) addFromFile(machinefile);

    while( err == 0 ) {
        input = parsing_init(scriptname);
	if (input == NULL) {
	    if( scriptname == NULL ) perror("stdin");
	    else perror(scriptname);
	    exit(1);
	}

#ifndef HAVE_READLINE
	if(state==0) {
            strcpy(prompt,DEFAULT_PROMPT);
            printf("%s%s%s",prompt_begin,prompt,prompt_end);
            fflush(stdout);
	}
#else
        if(fileno(stdin) >= 0 ) {
           if(!isatty(fileno(stdin)) ) {
	       if(state==0) {
                  strcpy(prompt,DEFAULT_PROMPT);
                  printf("%s%s%s",prompt_begin,prompt,prompt_end);
                  fflush(stdout);
	       }
	   }
	}
#endif

	while(parsing_getLine(input) >= 0) {
            if(input->NF!=0) {
                err=checkFields(input);
		if(err>0) {
		  break;     
		} else if(err==0) {
		  firstPos=0;  
		  nextPos=0;  
		  while(nextPos < input->NF) {
		    nextPos=findDelim(firstPos,input);
		    execCommand(firstPos,input);
		    firstPos=nextPos;
		  }
		}
	    }

#ifndef HAVE_READLINE
	  if(state==0) {
              printf("%s%s%s",prompt_begin,prompt,end_prompt);
              fflush(stdout);
	  }
#else
        if(fileno(stdin) >= 0 ) {
           if(!isatty(fileno(stdin)) ) {
	       if(state==0) {
                  strcpy(prompt,DEFAULT_PROMPT);
                  printf("%s%s%s",prompt_begin,prompt,prompt_end);
                  fflush(stdout);
	       }
	   }
	}
#endif

	} /* while(parsing_getLine(input) >= 0)  */
	parsing_finalize(input);
	switch(state) {
           case 2 : if(scname==NULL) { state=0; scriptname=NULL; } else { scriptname=scname; state=1; } break;
           case 1 : state=0; scriptname=NULL; break;
           case 0 : err=1; break; /* exit on EOF */
	}
	
    } /* while(use_script >=0) */

    puts("");

    return 0;
}
