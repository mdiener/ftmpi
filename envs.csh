
setenv HARNESS_ROOT		/home/**user**/Harness/hcore

setenv HARNESS_LIB_DIR          $HARNESS_ROOT/lib
setenv HARNESS_BIN_DIR          $HARNESS_ROOT/bin
setenv HARNESS_CACHE_DIR        $HARNESS_ROOT/tmp

setenv HARNESS_SVC_DIR          $HARNESS_ROOT/svc

setenv HARNESS_ARCH `${HARNESS_ROOT}/conf/pvmgetarch`

# The default port used by G_HCORE_D
setenv HARNESS_GHCORED_PORT 1945

# The default Harness top level NAME SERVICE
setenv HARNESS_NS_HOST	**HOST**
setenv HARNESS_NS_HOST_PORT	1973

# The above service will point to the lower service

# The default HARNESS REGISTRATION metadata-ring SERVICE 
setenv HARNESS_RS_HOST	**set to same host as above**
setenv HARNESS_RS_HOST_PORT	1970

set path=($path $HARNESS_ROOT/bin/$HARNESS_ARCH)
rehash

