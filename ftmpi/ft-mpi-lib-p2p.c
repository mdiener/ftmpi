/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg    <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gabriel    <egabriel@cs.utk.edu>
 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*

	MPI API point 2 point user library level calls

*/


#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-lib-p2p.h"
#include "ft-mpi-lib-nb.h"
#include "ft-mpi-p2p.h"
#include "ft-mpi-nb.h"
#include "ft-mpi-com.h"
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-procinfo.h"
#include "debug.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Iprobe               = PMPI_Iprobe
#    pragma weak  MPI_Probe                = PMPI_Probe
#    pragma weak  MPI_Recv                 = PMPI_Recv
#    pragma weak  MPI_Rsend                = PMPI_Rsend
#    pragma weak  MPI_Send                 = PMPI_Send
#    pragma weak  MPI_Sendrecv             = PMPI_Sendrecv
#    pragma weak  MPI_Sendrecv_replace     = PMPI_Sendrecv_replace
#    pragma weak  MPI_Ssend                = PMPI_Ssend

#endif

#    define  MPI_Iprobe                PMPI_Iprobe
#    define  MPI_Probe                 PMPI_Probe
#    define  MPI_Recv                  PMPI_Recv
#    define  MPI_Rsend                 PMPI_Rsend
#    define  MPI_Send                  PMPI_Send
#    define  MPI_Sendrecv              PMPI_Sendrecv
#    define  MPI_Sendrecv_replace      PMPI_Sendrecv_replace
#    define  MPI_Ssend                 PMPI_Ssend




/* these calls are in two groups 
* the MPI_* routines are the full calls that do full error checking etc 
* the ftmpi_mpi_* routines are the ones that do the same with less error
* checking */

int  MPI_Send( void *buf, int count, MPI_Datatype datatype, int dest, 
	       int tag, MPI_Comm comm )
{
  int rc;
  int size;

  /* temp */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);
  
  CHECK_TAG_S (comm,tag);
  
  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );
  if( dest == MPI_PROC_NULL ) return MPI_SUCCESS;
  if ( (dest < 0) || (dest >= size) ) RETURNERR (comm, MPI_ERR_RANK);
  
  CHECK_DDT(comm,datatype,1);
  CHECK_BUF(buf,datatype,count);
  
#ifdef SEND_VERBOSE
  /*   printf("Send from rank [%d] to [%d] com mode = %d stat %d remote stat %d\n", coms[0].my_rank, dest, comm_mode, comm_state, remote_state); */
#endif
  
  /* check for failure */
  CHECK_FT(comm);
  
  rc = ftmpi_mpi_send( buf, count, datatype, dest, tag, comm );
  
  if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
  RETURNERR(comm, rc);
}

int  MPI_Ssend( void *buf, int count, MPI_Datatype datatype, int dest, 
		int tag, MPI_Comm comm )
{
  return ( PMPI_Send (buf, count, datatype, dest, tag, comm ));
}

int  MPI_Rsend( void *buf, int count, MPI_Datatype datatype, int dest, 
		int tag, MPI_Comm comm )
{
  return ( PMPI_Send (buf, count, datatype, dest, tag, comm ));
}

int  MPI_Recv( void *buf, int count, MPI_Datatype datatype, int source, 
	       int tag, MPI_Comm comm, MPI_Status *status )
{
  int rc;
  int size;

  CHECK_MPIINIT;
  CHECK_COM ( comm );
  GET_PPT_SIZE(comm,size);

  CHECK_TAG_R (comm, tag);

  if ( count < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if( source == MPI_PROC_NULL ) {
    if( status != (MPI_Status*)MPI_STATUS_IGNORE ) {
      status->MPI_SOURCE = MPI_PROC_NULL;
      status->MPI_ERROR = MPI_SUCCESS;
      status->MPI_TAG = MPI_ANY_TAG;
      status->msglength = 0;
    }
    return MPI_SUCCESS;
  }

  if( source != MPI_ANY_SOURCE )
    if( (source < 0) || (source >= size) )
	RETURNERR (comm, MPI_ERR_RANK);

  CHECK_DDT(comm,datatype,1);
  CHECK_BUF(buf,datatype,count);

#ifdef RECV_VERBOSE
/*   printf("recv rank [%d] from [%d] com mode = %d stat %d remote stat %d\n", coms[0].my_rank, source, comm_mode, comm_state, remote_state); */
#endif 
		
/* check for failure */
  CHECK_FT (comm);

  rc = ftmpi_mpi_recv( buf, count, datatype, source, tag, comm, status);

  if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
  else
    RETURNERR (comm, rc);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Sendrecv(void* sbuf,int scnt,MPI_Datatype sddt, int dest,int stag,
		 void *rbuf,int rcnt,MPI_Datatype rddt, int source,int rtag,
		 MPI_Comm comm, MPI_Status *status)
{
  int size;
  int destgid=-1, sourcegid=-1, mygid;
  int dosend = 0;
  int dorecv = 0;
  int ret=MPI_SUCCESS;
  int dsize;

  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);

  CHECK_TAG_R (comm, rtag);
  CHECK_TAG_S (comm, stag);

  if ( (dest < 0) && (dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR ( comm, MPI_ERR_RANK);

  if ( (source < 0) && (source != MPI_PROC_NULL) && (source!=MPI_ANY_SOURCE) ) 
    RETURNERR (comm, MPI_ERR_RANK);
  if ( source >= size ) RETURNERR ( comm, MPI_ERR_RANK);

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );

  CHECK_DDT(comm,rddt,1);
  CHECK_DDT(comm,sddt,1);
  CHECK_FT (comm);

  mygid     = ftmpi_sys_get_gid();
  if ( dest != MPI_PROC_NULL )
    {
      destgid   = ftmpi_com_gid (comm,dest);
      dosend    = 1;
    }

  if ( source != MPI_PROC_NULL )
    {
      sourcegid = ftmpi_com_gid (comm,source);
      dorecv    = 1;
    }


  if ( (destgid == sourcegid ) && ( destgid == mygid ) )
    {
      /* Special case: send and receive from myself. Done with a copy */
      ret = ftmpi_ddt_copy_ddt_to_ddt(sbuf,sddt,scnt,rbuf,rddt,rcnt);
      if ( ret < 0 ) RETURNERR (comm, ret );
      
      ftmpi_mpi_type_size(sddt, &dsize );
      status->MPI_SOURCE = dest;
      status->MPI_TAG    = rtag;
      status->MPI_ERROR  = MPI_SUCCESS;
      status->msglength  = scnt*dsize;
      status->dt         = sddt;
      status->elements   = scnt;
      status->flags      = FTMPI_REQ_DONE | FTMPI_REQ_FREED;	  
    }

  if ( dosend && dorecv )
    {
      /* We are sending and receiving a message */

      MPI_Request req;

      ret = ftmpi_mpi_irecv ( rbuf, rcnt, rddt, source, rtag, comm, &req );
      if ( ret != MPI_SUCCESS ) goto remove_request;
      ret = ftmpi_mpi_send ( sbuf, scnt, sddt, dest, stag, comm );
      if ( ret != MPI_SUCCESS ) goto remove_request;
      ret = ftmpi_mpi_wait (&req, status );
      
    remove_request:
      if ( (ret != MPI_SUCCESS ) && (req != MPI_REQUEST_NULL) ) 
	{
	  if( ftmpi_nb_req_decref( req ) >= 0 ) 
	    { 
	      /* set the request position to NULL TODO check return value */
	      ftmpi_nb_free_req_handle( req );
	    } 
	  else 
	    printf( "WARNING there is a problem with the request !!!\n" );
	}
    }

  else
    {
      if ( dosend ) 
	{
	  /* Just send but not receiving. Have to set status to nullstatus */
	  ret = ftmpi_mpi_send ( sbuf, scnt, sddt, dest, stag, comm );
	  
	}
      if ( dorecv ) 
	{
	  /* Just receiving a message */
	  ret = ftmpi_mpi_recv ( rbuf, rcnt, rddt, source, rtag, 
				 comm, status);
	}
    } 

  if ( !(dorecv) && (status != (MPI_Status*)MPI_STATUS_IGNORE) )
    {
      status->MPI_SOURCE = MPI_PROC_NULL;
      status->MPI_TAG    = MPI_ANY_TAG;
      status->MPI_ERROR  = MPI_SUCCESS;
      status->msglength  = 0;
      status->dt         = rddt;
      status->elements   = 0;
      status->flags      = FTMPI_REQ_DONE | FTMPI_REQ_FREED;
    }
  
  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Sendrecv_replace(void * buf,int cnt,MPI_Datatype ddt, int dest,
			 int stag,int source, int rtag,MPI_Comm comm, 
			 MPI_Status *status)
{
  int packsize;
  char *tmpbuf=NULL;
  int dosend=0;
  int dorecv=0;
  int ret=MPI_SUCCESS;
  int mygid, destgid=-1, sourcegid=-1;
  int size;
  int dsize;


  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);

  CHECK_TAG_R (comm, rtag);
  CHECK_TAG_S (comm, stag);

  if ( (dest < 0) && (dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR (comm, MPI_ERR_RANK );

  if ( (source < 0) && (source != MPI_PROC_NULL) && (source!=MPI_ANY_SOURCE) ) 
    RETURNERR (comm, MPI_ERR_RANK);
  if ( source >= size ) RETURNERR (comm, MPI_ERR_RANK);

  if ( cnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );

  CHECK_DDT(comm,ddt,1);
  CHECK_FT (comm);

  mygid = ftmpi_sys_get_gid();
  if ( dest != MPI_PROC_NULL )
    {
      destgid   = ftmpi_com_gid (comm,dest);
      dosend    = 1;
    }

  if ( source != MPI_PROC_NULL )
    {
      sourcegid = ftmpi_com_gid (comm,source);
      dorecv    = 1;
    }

  
  if ( ( destgid == sourcegid ) && ( destgid == mygid ) )
    {
      /* Special case: send and receive from myself. \
	 Don't have to do anything, data is in the right
	 place already */
      
      ftmpi_mpi_type_size(ddt, &dsize );
      status->MPI_SOURCE = dest;
      status->MPI_TAG    = rtag;
      status->MPI_ERROR  = MPI_SUCCESS;
      status->msglength  = cnt*dsize;
      status->dt         = ddt;
      status->elements   = cnt;
      status->flags      = FTMPI_REQ_DONE | FTMPI_REQ_FREED;	  
    }
  else
    if ( dosend && dorecv )
      {
	/* We are sending and receiving at the same time */
	FTMPI_DDT_CODE_INFO* code_info;
	MPI_Request req;

	/* Allocate memory and copy data into buffer for the send op */
	code_info = ftmpi_procinfo_get_ddt_code_info(destgid);
	ret = ftmpi_ddt_encode_size_det(ddt, cnt, &packsize, code_info);
	if ( packsize > 0 )
	  {
	    tmpbuf = (char *)_MALLOC( packsize );
	    if ( tmpbuf == NULL ) RETURNERR (comm, MPI_ERR_INTERN);
	    ftmpi_ddt_xdr_encode_dt_to_buffer(buf, tmpbuf, NULL, packsize, 
					      cnt, ddt,code_info);
	  }

	/* Send and recv the data */
	ret = ftmpi_mpi_irecv ( buf, cnt, ddt, source, rtag, comm, &req);
	if ( ret != MPI_SUCCESS ) goto remove_request;
	ret = ftmpi_mpi_send ( tmpbuf, packsize, MPI_BYTE, dest, stag, comm);
	if ( ret != MPI_SUCCESS) goto remove_request;
	ret = ftmpi_mpi_wait ( &req, status );
	
	if (tmpbuf != NULL ) 
	  _FREE ( tmpbuf );

      remove_request:
	if ( (ret != MPI_SUCCESS) && (req != MPI_REQUEST_NULL) ) {
	  if( ftmpi_nb_req_decref( req ) >= 0 ) 
	    { 
	      /* set the request position to NULL TODO check return value */
	      ftmpi_nb_free_req_handle( req );
	    } 
	  else 
	    printf( "WARNING there is a problem with the request !!!\n" );
	}
      }
    else
      {
	if ( dosend ) 
	  {
	    /* Just send data and set null-status */
	    ret = ftmpi_mpi_send ( buf, cnt, ddt, dest, stag, comm );
	  }
	if ( dorecv ) 
	  {
	    /* Just receive a message */
	    ret = ftmpi_mpi_recv (buf, cnt, ddt, source, rtag, comm, status);
	  }

      }

  if ( !(dorecv) && (status != (MPI_Status*)MPI_STATUS_IGNORE) )
    {
      status->MPI_SOURCE = MPI_PROC_NULL;
      status->MPI_TAG    = MPI_ANY_TAG;
      status->MPI_ERROR  = MPI_SUCCESS;
      status->msglength  = 0;
      status->dt         = ddt;
      status->elements   = 0;
      status->flags      = FTMPI_REQ_DONE | FTMPI_REQ_FREED;
    }
  
  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return ( MPI_SUCCESS ); 
}




/* ********************************************************************* */
/*                            Probe operations                            */
/* ********************************************************************* */



int MPI_Probe(int source,int tag,MPI_Comm comm,MPI_Status *status)
{
  int flag;
  int rc;
  int size; 

  CHECK_MPIINIT;
  CHECK_COM (comm);
  GET_PPT_SIZE(comm,size);
  
  CHECK_TAG_R (comm, tag);
  if ( (source < 0) && (source != MPI_PROC_NULL) && (source!=MPI_ANY_SOURCE) ) 
    RETURNERR (comm, MPI_ERR_RANK);
  if ( source >= size ) RETURNERR (comm, MPI_ERR_RANK);
  
  /* check for failure */
  CHECK_FT (comm);
  
  if (source==MPI_PROC_NULL) {
    ftmpi_pp_set_nullstatus (status);
    return (MPI_SUCCESS);
	}
  
  flag = 0;
  
  while (!flag) {	/* loop awaiting for the message */
    
    rc = ftmpi_pp_probe (source, tag, comm, &flag, status, 1);	/* note the & */
    /* 1 = blocking probe call (just like a blocking recv */
    
    if (rc<0)  RETURNERR (comm, rc) /* on error quite though */
		 
    else {	/* we don't have the answer we need so make progress on recvs */
      /* until we get the data we are waiting on */
      /* progress call is actually part of the lower level probe call itself */
      if (rc<0) RETURNERR (comm, rc);
    }

  } /* while */

  if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
  RETURNERR (comm, rc)
}

int MPI_Iprobe(int source,int tag,MPI_Comm comm,int *flag, MPI_Status *status)
{
int rc;
 int size; 

 CHECK_MPIINIT;
 CHECK_COM(comm);
 GET_PPT_SIZE(comm,size);

 CHECK_TAG_R (comm, tag);

 if ( (source < 0) && (source != MPI_PROC_NULL) && (source!=MPI_ANY_SOURCE) ) 
   RETURNERR (comm, MPI_ERR_RANK);
 if ( source >= size ) RETURNERR (comm, MPI_ERR_RANK);
 
/* check for failure */
CHECK_FT (comm);

if (source==MPI_PROC_NULL) {
	ftmpi_pp_set_nullstatus (status);
	return (MPI_SUCCESS);
	}


rc = ftmpi_pp_probe (source, tag, comm, flag, status, 0);
/* 0 = nonblocking, but the call still makes a progress call if no message aready available */

/* non blocking / immediate so no need to check flag :) */
/* just pass it back */

 if (rc<0) RETURNERR (comm, rc)
 else
   return(rc);
}

/*
 * here we are have defuncted the ftmpi_mpi_send and replaced it via a 
 * isend/wait combo.
 *
 * This is the first phase of a move to purely NB coms
 * GEF Sep03.
 */


int  ftmpi_mpi_send( void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm )
{
   int rc;
   MPI_Request request;
   MPI_Status status;

   rc = ftmpi_mpi_isend (buf,count,datatype,dest,tag,comm,&request);
   if ( rc != MPI_SUCCESS ) goto remove_request;

   if( (rc = ftmpi_mpi_wait(&request,&status)) == MPI_SUCCESS ) return MPI_SUCCESS;

 remove_request:
   if( request == MPI_REQUEST_NULL ) return rc; /* not yet allocated */
   /* Now let's remove the request */
   if( ftmpi_nb_req_decref( request ) >= 0 ) {  /* attempt to free */
     /* set the request position to NULL TODO check return value */
     ftmpi_nb_free_req_handle( request );
   } else {
     printf( "WARNING there is a problem with the request !!!\n" );
   }
   
   return (rc);
}


/* Actual routines that do something other than just check the arg codes */

int  ftmpi_mpi_recv( void *buf, int count, MPI_Datatype datatype, int source, 
	       int tag, MPI_Comm comm, MPI_Status *status )
{
   int rc;
   MPI_Request request;

   rc = ftmpi_mpi_irecv (buf,count,datatype,source,tag,comm,&request);
   /* Here we have the following problem: In the case of error, this request should be
    * removed from the queues or the next message will match this "lost" request.
    */
   if(rc != MPI_SUCCESS) goto remove_request;
   
   if( (rc = ftmpi_mpi_wait(&request,status)) == MPI_SUCCESS ) return MPI_SUCCESS;

 remove_request:
   if( request == MPI_REQUEST_NULL ) return rc; /* not yet allocated */
   /* Now let's remove the request */
   if( ftmpi_nb_req_decref( request ) >= 0 ) {  /* attempt to free */
     /* set the request position to NULL TODO check return value */
     ftmpi_nb_free_req_handle( request );
   } else {
     printf( "WARNING there is a problem with the request !!!\n" );
   }
   
   return (rc);
}


