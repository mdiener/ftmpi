/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>
			George Bosilca <bosilca@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 We handle all the communicator operations here such as init, copy, dup 
 and copying in and out of the different DBs (via the ft-mpi-common.c routines)

 Graham HLRS, 2001.
*/


#include "mpi.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-sys.h"
#include "stdio.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-cart.h"
#include "ft-mpi-graph.h"
#include "ft-mpi-group.h"
#include "ft-mpi-com.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-attr.h"
#include "ft-mpi-err.h"
#include "ft-mpi-p2p.h"
#include "debug.h"

comm_info_t _ftmpi_comm[MAXCOMS];	/* just for now a fixed array */
int _ftmpi_comfree = MAXCOMS;

extern int MAXGROUPS;

#define DO_STANDARD_COMM_CHECKS(COM, RETVAL) \
do { \
  if( (COM) == MPI_COMM_NULL ) return (RETVAL); \
  if( ((COM) < 0) || ((COM) >= MAXCOMS) ) return (RETVAL); \
} while(0)

/* get the com structure by handle and return ptr to it */

comm_info_t * ftmpi_com_get_ptr (MPI_Comm com)
{
  DO_STANDARD_COMM_CHECKS(com, NULL);
  if( _ftmpi_comm[com].comm == MPI_COMM_NULL ) return (NULL);
  return (&_ftmpi_comm[com]);
}


/* 
 used by mpi-lib API calls on a communicator
 i.e. fails if it is freed etc etc
 */

int	ftmpi_com_ok (int com)
{
  DO_STANDARD_COMM_CHECKS(com, MPI_ERR_COMM);
  if( _ftmpi_comm[com].comm == MPI_COMM_NULL ) return (MPI_ERR_COMM);
  return (MPI_SUCCESS);
}

/* 
 This routine associates a communicator with a group
 	It updates both the Group and the Comm structures and ref counts
 */

int	ftmpi_com_set_group (MPI_Comm com, MPI_Group grp)
{
  DO_STANDARD_COMM_CHECKS( com, -1 );

  /*
    These tests taken out as someone might want to set a NULL group... oneday?
    if (grp==MPI_GROUP_NULL) return (-1);
    if ((grp<0)||(grp>=MAXGROUPS)) return (-1);
  */
  
  /* ok, set it */
  _ftmpi_comm[com].group = grp;
  ftmpi_group_refinc (grp);
  return (MPI_SUCCESS);
}


/*
 here we get the group from the communicator
 */

MPI_Group ftmpi_com_get_group (MPI_Comm com)
{
  DO_STANDARD_COMM_CHECKS( com, -1 );
  /* ok, get it */
  return (_ftmpi_comm[com].group);
}


#ifdef DEFUNCT
/* get send and recv counts */
/* usually this routine is not used as the nb routines get it directly via */
/* a com info ptr */

int	ftmpi_com_get_send_count (MPI_Comm com)
{
  DO_STANDARD_COMM_CHECKS( com, -1 );
  /* ok, get it */
  return (_ftmpi_comm[com].req_list_send_count);
}
#endif /* DEFUNCT */


#ifdef DEFUNCT
int	ftmpi_com_get_recv_count (MPI_Comm com)
{
  DO_STANDARD_COMM_CHECKS( com, -1 );
  /* ok, get it */
  return (_ftmpi_comm[com].req_list_recv_count);
}
#endif /* DEFUNCT */



/* clear all coms */

int ftmpi_com_init ()
{
  int i;
  for(i=0;i<MAXCOMS;i++) {
    _ftmpi_comm[i].msg_list_head = NULL;	/* this is needed to stop com_clr */
    /* clearing a non initised pointer */
    /* we do it here to make sure */
    _ftmpi_comm[i].msg_list_count = 0;
    
    _ftmpi_comm[i].topo_type = MPI_UNDEFINED;
    _ftmpi_comm[i].cart_ptr  = NULL;
    _ftmpi_comm[i].graph_ptr = NULL;
    
    _ftmpi_comm[i].group = MPI_GROUP_NULL;
    _ftmpi_comm[i].remote_group = MPI_GROUP_NULL;
    _ftmpi_comm[i].local_comm = MPI_COMM_NULL;
    _ftmpi_comm[i].inter = 0;
    
    _ftmpi_comm[i].nattr   = 0;
    _ftmpi_comm[i].maxattr = 0;
    _ftmpi_comm[i].attrs   = NULL;
    
    _ftmpi_comm[i].errhandler = 0;
    _ftmpi_comm[i].errfn      = NULL;
    
    ftmpi_com_clr (i);
  }
  _ftmpi_comfree = MAXCOMS;
  
  return ( MPI_SUCCESS ); 
}

int ftmpi_com_clr_all_but_world ()
{
  int i;

  /* printf("ftmpi_com_clr_all_but_world () called\n"); */
  
  for(i=2;i<MAXCOMS;i++) 
    if (_ftmpi_comm[i].comm!= MPI_COMM_NULL) ftmpi_com_clr (i);
  
  return ( MPI_SUCCESS ); 
}


/* clear a single com */
int ftmpi_com_clr (int i) 
{
  int j;
  int grp;
  graph_info_t *graph;
  cart_info_t *cart;

	/* if it was in use and is now freed inc free count */
	if (_ftmpi_comm[i].comm!= MPI_COMM_NULL) _ftmpi_comfree++;

	/* if it had been in use, was then freed and now can be released inc cnt */
	if ((_ftmpi_comm[i].comm==MPI_COMM_NULL)&&(_ftmpi_comm[i].freed==1)) _ftmpi_comfree++; 
	
	/* free non matching receives / unread data */
	if (_ftmpi_comm[i].msg_list_head) {
		msg_list_destroy (_ftmpi_comm[i].msg_list_head);
		_ftmpi_comm[i].msg_list_head= NULL;
		_ftmpi_comm[i].msg_list_count = 0;
		}

	/* free non blocking data structures if they exist */
	/* TODO */

	/* free topology information: has to be done before
	   invalidatinf the communicator else the comm_get_cart/graph
	   functions won't work properly */
	cart = (cart_info_t *) ftmpi_comm_get_cart_ptr ( (MPI_Comm) i );
	if(cart != NULL)
	  {
	    ftmpi_cart_free ( cart );
	    if(_ftmpi_comm[i].cart_ptr != NULL)
		_ftmpi_comm[i].cart_ptr = NULL;
	  }

	graph = (graph_info_t *) ftmpi_comm_get_graph_ptr ( (MPI_Comm) i);
	if(graph != NULL)
	  {
	    ftmpi_graph_free ( graph );
	    if(_ftmpi_comm[i].graph_ptr != NULL)
		_ftmpi_comm[i].graph_ptr = NULL;
	  }
	_ftmpi_comm[i].topo_type = MPI_UNDEFINED;


	/* handle groups */
	grp = _ftmpi_comm[i].group;
	if (grp!=MPI_GROUP_NULL) {
		ftmpi_group_refdec (grp);
		ftmpi_group_free (&grp);
	}

	_ftmpi_comm[i].nbref = 0;

	_ftmpi_comm[i].comm = MPI_COMM_NULL;
	_ftmpi_comm[i].group = MPI_GROUP_NULL;
	_ftmpi_comm[i].derived_from = MPI_COMM_NULL;
	_ftmpi_comm[i].freed = 0;	
	_ftmpi_comm[i].maxsize =0;
	_ftmpi_comm[i].currentsize = 0;
	_ftmpi_comm[i].nprocs = 0;
	_ftmpi_comm[i].my_rank = MPI_UNDEFINED;
	_ftmpi_comm[i].my_gid = 0;
	_ftmpi_comm[i].ft_com_mode = 0;
	_ftmpi_comm[i].ft_msg_mode = 0;
	_ftmpi_comm[i].ft_com_state = -1;
	_ftmpi_comm[i].ft_epoch = 0;
	for (j=0;j<MAXPERAPP;j++) {
	  _ftmpi_comm[i].gids[j] = 0;
	  _ftmpi_comm[i].conn_entry[j] = 0;
	}

	/* handle remote groups */
	grp = _ftmpi_comm[i].remote_group;
	if (grp!=MPI_GROUP_NULL) {
		ftmpi_group_refdec (grp);
		ftmpi_group_free (&grp);
	}

	_ftmpi_comm[i].remote_group = MPI_GROUP_NULL;
	if ( (_ftmpi_comm[i].inter) && (!_ftmpi_comm[i].am_shadow) )
	    MPI_Comm_free ( &(_ftmpi_comm[i].local_comm) );
	_ftmpi_comm[i].inter       = 0;
	_ftmpi_comm[i].lleader     = 0;
	_ftmpi_comm[i].rleader     = 0;
	_ftmpi_comm[i].lleader_gid = 0;
	_ftmpi_comm[i].rleader_gid = 0;


	/* Free all the attribute related stuff. */
	/* NOTE: do not reset maxattr and do not free attrs, they
	   might be reused in the next comm. Since they are usually
	   very short (e.g. 5), this shouldn't be a memory problem */

	_ftmpi_comm[i].nattr       = 0;
	if ( _ftmpi_comm[i].maxattr > 0 )
	  for ( j = 0; j < _ftmpi_comm[i].maxattr; j ++ )
	    _ftmpi_comm[i].attrs[j] = MPI_KEYVAL_INVALID;
	

	/* Reset error-handler */
	_ftmpi_comm[i].errhandler = MPI_ERRORS_RETURN;
	_ftmpi_comm[i].errfn = (MPI_Comm_errhandler_fn *) (ftmpi_errors_return );


	/* Reset shadow communicator */
	_ftmpi_comm[i].shadow     = MPI_COMM_NULL;
	_ftmpi_comm[i].am_shadow  = 0;
	_ftmpi_comm[i].parent     = MPI_COMM_NULL;	

	/* Reset topology information */
	_ftmpi_comm[i].bmtree_root     = MPI_UNDEFINED;
	_ftmpi_comm[i].bmtree_prev     = MPI_UNDEFINED;
	_ftmpi_comm[i].bmtree_nextsize = 0;
	_ftmpi_comm[i].tree_prev       = MPI_UNDEFINED;
	_ftmpi_comm[i].tree_nextsize   = 0;
	_ftmpi_comm[i].ring_prev       = MPI_UNDEFINED;
	_ftmpi_comm[i].ring_next       = MPI_UNDEFINED;
	for ( j = 0; j < MAXTREEFANOUT; j++ )
	  {
	    _ftmpi_comm[i].bmtree_next[j] = MPI_UNDEFINED;
	    _ftmpi_comm[i].tree_next[j]  = MPI_UNDEFINED;
	  }

	return (0);
}

/* 
 * In here we (re)build a particular com
 */
int ftmpi_com_build (int com, int gid, int cm, int mm, int epoch,
		     int *ids, int ext, int procs, int reset_msg_lists)
{
  int i,j,r;
  int m;
  int cnt;

#ifdef VERBOSE
  printf("ftmpi_com_build com %d gid %d cm %d mm %d ep %d ext %d np %d reset msglists %d\n",
	 com, gid, cm, mm, epoch, ext, procs, reset_msg_lists); 
  fflush(stdout);
#endif

  _ftmpi_comm[com].comm = com;
  if (_ftmpi_comm[com].derived_from==MPI_COMM_NULL) {
    /* if not before, it is now */
    _ftmpi_comm[com].derived_from = MPI_COMM_WORLD;
  }
  _ftmpi_comm[com].freed =0;
  _ftmpi_comm[com].nbref =0;
  _ftmpi_comm[com].maxsize =ext;
  _ftmpi_comm[com].currentsize = ext;
  _ftmpi_comm[com].nprocs = procs;
  _ftmpi_comm[com].my_gid = gid;
  _ftmpi_comm[com].ft_com_mode = cm;
  _ftmpi_comm[com].ft_msg_mode = mm;
  _ftmpi_comm[com].ft_com_state = FT_OK;
  _ftmpi_comm[com].ft_epoch = epoch;	

  /* if this is either a new communicator or its being complete gutted */
  /* if its MPI_COMM_WORLD and being recovered do NOT reset the lists */
  if (reset_msg_lists) {
	 /* if the message list existed before, free it */
	 if (_ftmpi_comm[com].msg_list_head) 
	   msg_list_destroy (_ftmpi_comm[com].msg_list_head);
  
	 /* alocate new list */
	 _ftmpi_comm[com].msg_list_head = msg_list_init();
	 _ftmpi_comm[com].msg_list_count = 0;
	 
	 /* alocate new req sending list */
/* 	 _ftmpi_comm[com].req_list_send_head = req_list_init(); */
/* 	 _ftmpi_comm[com].req_list_send_count = 0; */
  
	 /* alocate new req recving list */
/* 	 _ftmpi_comm[com].req_list_recv_head = req_list_init(); */
/* 	 _ftmpi_comm[com].req_list_recv_count = 0; */
  
	 /* alocate new req completed list */
/* 	 _ftmpi_comm[com].req_list_done_head = req_list_init(); */
/* 	 _ftmpi_comm[com].req_list_done_count = 0; */
  }
  
  /* TODO: check if it's really work in case of recovery after a fault */
  /* NOW if we just rebuilt the MCW, we remove conn entries for unknown gids */
  /* As they cannot be in any other coms if they are not in MCW !   GEF */

  /* XXX except maybe in an intercom under MPI_2 spawn! */
  
  if (com==MPI_COMM_WORLD) remove_conn_entry_not_in_list (ids, ext);

  /* now we check for our rank */
  r = MPI_UNDEFINED;
  for (i=0;i<ext;i++) if (ids[i]==gid) { r = i; break; }
  _ftmpi_comm[com].my_rank = r;
  
  /* copy the usefull data into it */
  if (MAXPERAPP>(ext)) m=ext;
  else m = MAXPERAPP;
  for (j=0;j<m;j++) {
    if ( ids[j] == 0 ) {
      /* Failed processes in the blank mode */
      _ftmpi_comm[com].gids[j] = MPI_PROC_NULL;
      _ftmpi_comm[com].conn_entry[j] = MPI_UNDEFINED;
    }
    else {
      _ftmpi_comm[com].gids[j] = ids[j];
      r = find_conn_entry_by_gid (ids[j]);
      if (r) { 
	_ftmpi_comm[com].conn_entry[j] = r;
      }
    }
  }
  /* and clear the rest */
  for( ; j < MAXPERAPP; j++ ) {
    /* note conn entry 0 is a special blank */
    _ftmpi_comm[com].gids[j] = 0;
    _ftmpi_comm[com].conn_entry[j] = 0;
  }
  /*   _ftmpi_comm[com].cart_ptr = NULL; */

  /* ok, at the end we should move any premature delivered messages for this */
  /* comm. any early messages should be on the system queue */
  cnt = ftmpi_move_sys_msgs_to_unexp_msg_list (com);

#ifdef VERBOSERECOVERY
  if (cnt)
    fprintf(stderr, "com_build: moved %d messages to com %d unexpected msgQ\n", cnt, com);
#endif /* VERBOSERECOVERY */
  
  return (0);
}

int	ftmpi_com_rank (int com)
{
  /* printf("com rank com%d rank0 %d\n", com, _ftmpi_comm[com].my_rank); */
  DO_STANDARD_COMM_CHECKS(com, MPI_ERR_COMM);
  if( _ftmpi_comm[com].comm == MPI_UNDEFINED ) return (MPI_ERR_COMM);
  return (_ftmpi_comm[com].my_rank);
}

int	ftmpi_com_size (int com)
{
  /* printf("com size com%d size %d\n", com, _ftmpi_comm[com].currentsize); */
  DO_STANDARD_COMM_CHECKS(com, MPI_ERR_COMM);
  if( _ftmpi_comm[com].comm == MPI_UNDEFINED ) return (MPI_ERR_COMM);
  return (_ftmpi_comm[com].currentsize);
}


/* returns the global ID of the MPI process */

int ftmpi_com_gid (int com, int rank)
{
  DO_STANDARD_COMM_CHECKS( com, MPI_ERR_COMM );

  if ( _ftmpi_comm[com].inter )
   {
     /* Inter communicator */
     group_t * grp_ptr;

     grp_ptr = ftmpi_com_get_remote_group_ptr ( com);
     if ((rank<0) || (rank>grp_ptr->maxsize))
       return ( MPI_ERR_RANK );
     return ( grp_ptr->gids[rank]);
   }
 else
   {
     /* Intra-communicator */
     if ((rank<0)||(rank>=_ftmpi_comm[com].maxsize)) 
       return (MPI_ERR_RANK);
     return (_ftmpi_comm[com].gids[rank]);
   }
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* inverse function of ftmpi_com_gid: input is a gid and it returns
   its rank in the given communicator . */
int ftmpi_com_map_rank  ( MPI_Comm com, int gid)
{
  int i=MPI_PROC_NULL;

  if ( _ftmpi_comm[com].inter )
    {
      /* Inter communicator */
      group_t * grp_ptr;

      grp_ptr = ftmpi_com_get_remote_group_ptr ( com );
      for ( i = 0; i < grp_ptr->maxsize; i ++ )
	if ( grp_ptr->gids[i] == gid )
	  return ( i );
    }
  else
    {
      /* Intra-communicators */
      for ( i = 0; i < _ftmpi_comm[com].maxsize ; i ++ )
	if ( _ftmpi_comm[com].gids[i] == gid )
	  return ( i );
    }
	     
  return ( i );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

/* returns MY global ID in the world com.. */
/* make sure it exists before you call it! */
int ftmpi_com_my_gid ()
{
  return (_ftmpi_comm[MPI_COMM_WORLD].my_gid);
}

/* returns com mode for com */
/* make sure its a valid com first! */
int ftmpi_com_get_com_mode (int com)
{
  return (_ftmpi_comm[com].ft_com_mode);
}

/* returns msg mode for com */
/* make sure its a valid com first! */
int ftmpi_com_get_msg_mode (int com)
{
  return (_ftmpi_comm[com].ft_msg_mode);
}


/* returns the numeric floating point type for the MPI process
 */
int ftmpi_com_fptype (int com, int rank)
{
  /* right now just complain */
  fprintf( stderr, "ftmpi_com_fptype: what the hell could i return here !!!\n" );
  return -1;
}


/*
 This routine copies one com data set completely into another 
 if the target com value is zero/0 then the routine allocates a 
 com value (finds a free slot) itself else it uses what it is told
 */
int ftmpi_com_copy (int com, int *newcom)
{
  int topo_type;

  if (!newcom) return (MPI_ERR_ARG);	/* bad args */

  if (com==MPI_COMM_NULL) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }
  if ((com<0)||(com>=MAXCOMS)) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }
  
  if (_ftmpi_comm[com].comm==MPI_UNDEFINED) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }

  
  /* ok, now find empty slot */
  /* that is free to be allocated */
  /* unless already indicated */
  
  if( (*newcom) == MPI_COMM_NULL ) {
    if( ((*newcom) = ftmpi_com_get_free_comm()) == MPI_COMM_NULL )
      return (MPI_ERR_INTERN);
  }	/* if no slot preallocated */

  /* ok have a slot so fill it using a previous function :) */
  /* noting that it is an identical copy */
  ftmpi_com_build( *newcom, _ftmpi_comm[com].my_gid,
		   _ftmpi_comm[com].ft_com_mode, 
		   _ftmpi_comm[com].ft_msg_mode, _ftmpi_comm[com].ft_epoch, 
		   &(_ftmpi_comm[com].gids[0]), _ftmpi_comm[com].currentsize, 
		   _ftmpi_comm[com].nprocs, 1);
  
  /*Copy topology information if required */
  topo_type = ftmpi_com_get_topo ( com );
  if ( topo_type == MPI_CART ) {
    cart_info_t *cart, *newcart;
      
    cart = ( cart_info_t *) ftmpi_comm_get_cart_ptr ( com );
    newcart = ftmpi_cart_copy ( cart );
    ftmpi_comm_attach_cart ( *newcom, newcart );
  } else if ( topo_type == MPI_GRAPH ) {
    graph_info_t *graph, *newgraph;
    
    graph = ( graph_info_t *) ftmpi_comm_get_graph_ptr ( com );
    newgraph = ftmpi_graph_copy ( graph );
    ftmpi_comm_attach_graph ( *newcom, newgraph );
  }

  return (MPI_SUCCESS);
}

/* Return the first free communicator.
 *
 * !! NO: it should return the first free comm where
 * above every slot is free! 
 * This is one of the reasons why I want to split the comm-id
 * from the comm-handle!
 *
 * Note: this communicator should have nbref set to zero or else
 * it could be a freed communicator with still pending coms on it
 */
int ftmpi_com_get_free_comm( void )
{
  int i;
  int h = -1, n;

  for( i = 1; i < MAXCOMS; i++ )
    if( (_ftmpi_comm[i].comm != MPI_COMM_NULL)
	|| (_ftmpi_comm[i].nbref) ) 
      h=i;
  /* did we find some space for the communicator ?? */
  if( h == -1 ) return MPI_COMM_NULL;
  n = h + 1;

  if ((_ftmpi_comm[n].comm==MPI_COMM_NULL)&&(!_ftmpi_comm[n].nbref) )
    return ( n );
  else {
    /* ops no slots! */
    fprintf(stderr,"FT-MPI:%s:ftmpi_get_free_comm cannot find memory "
	    "for a new communicator descriptor!\n", __FILE__ );
    fprintf(stderr,"This may be caused by not freeing communicators\n");
    fprintf(stderr,"If you need a larger lookup table change the "
	    "MAXCOMS entry in ft-mpi-sys.h\n");
  fprintf(stderr,"comments to GEF\n");
  }

  return MPI_COMM_NULL;
}

/*
 This is the same as the com_copy except you can specify a new set of IDs
 */

int	ftmpi_com_copy_subset (int com, int *newcom, int *gids, 
			       int extent, int nprocs)
{
  if (com==MPI_COMM_NULL) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }
  if ((com<0)||(com>=MAXCOMS)) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }
  
  if (_ftmpi_comm[com].comm==MPI_UNDEFINED) {
    *newcom = MPI_COMM_NULL;
    return (MPI_ERR_COMM);
  }
  
  if (!newcom) return (MPI_ERR_ARG);	/* bad args */
  
  /* ok, now find empty slot */
  /* that is free to be allocated */
  
  if( *newcom == 0 ) {
    if( (*newcom = ftmpi_com_get_free_comm()) == MPI_COMM_NULL )
      return (MPI_ERR_INTERN);
    
  } /* no pre-allocated com slot */

  /* ok have a slot so fill it using a previous function :) */
  /* noting that it is an identical copy */
  
  ftmpi_com_build( *newcom, _ftmpi_comm[com].my_gid,
		   _ftmpi_comm[com].ft_com_mode, 
		   _ftmpi_comm[com].ft_msg_mode,
		   _ftmpi_comm[com].ft_epoch, 
		   gids, extent, nprocs, 1);
  return (MPI_SUCCESS);
}


int ftmpi_com_free (MPI_Comm *com) 

{
  int i, handle;
  MPI_Errhandler errh; 
  int nvalues;
  int ret;

  if (com==NULL) return (MPI_ERR_ARG);
  if (*com==MPI_COMM_WORLD) return (MPI_ERR_COMM);	/* can not do that */
  if (*com==MPI_COMM_SELF) return (MPI_ERR_COMM);	/* can not do that */
  
  DO_STANDARD_COMM_CHECKS( *com, MPI_ERR_COMM );

  /* if already freed this would be true for example */
  if (_ftmpi_comm[*com].comm==MPI_COMM_NULL) {
    return (MPI_ERR_COMM);
  }

  /* ok, we JUST mark it free here unless it really is free */

  if (_ftmpi_comm[*com].nbref) { /* has pending nb ops on it */
    
    _ftmpi_comm[*com].comm=MPI_COMM_NULL; /* means we can no longer look up on it */
    _ftmpi_comm[*com].freed = 1;
    
    *com = MPI_COMM_NULL;	/* return null handle so they can't use it :) */
    /* NOTE, the nb calls now have to clear this com when the ref count */
    /* reaches zero for us! */
    
    return (MPI_SUCCESS);
  }
  
  /* ok, there are no pending MPI communications on this so we can free it */

  /* remove this communicator from the key-list, in case it had some attributes
     attached */
  if (_ftmpi_comm[*com].nattr > 0 ) {
    for ( i = 0 ; i < _ftmpi_comm[*com].nattr; i++ ) {
      ret = ftmpi_keyval_delete ( _ftmpi_comm[*com].attrs[i], *com, &nvalues );
      if ( ret != MPI_SUCCESS ) return ( ret );
    }
  }
  
  /* unmark the communicator in the errhandler list */
  handle = ftmpi_com_get_errhandler ( *com);
  errh   = ftmpi_errh_get_err ( handle );
  if ( (errh != MPI_ERRORS_ARE_FATAL ) && ( errh!= MPI_ERRORS_RETURN ))
    ftmpi_errhval_unmark ( handle, *com );

  ftmpi_com_clr (*com);
  *com = MPI_COMM_NULL;
  return (MPI_SUCCESS);
}


/*
This is a shorthand routine that checks to see if a communicator has
been marked as freed but isn't yet as it has pending ops on it 
if it doesn't anymore then free it!

This is usualy called from the NB routines when they dec a ref counter down 
*/

int ftmpi_com_check_freed (MPI_Comm com)
{
  if (com==MPI_COMM_WORLD) return (0);
  if ((com<0)||(com>=MAXCOMS)) return (0);

  if ((_ftmpi_comm[com].freed)&&(!_ftmpi_comm[com].nbref)) return ftmpi_com_clr (com);
  return (0);
}



/*
 debugging routines
*/

int ftmpi_com_display (MPI_Comm com)
{
  int i;

  printf("Comm id %d internal info\n", com); fflush(stdout);
  printf("comm %d derived_from %d group %d\n", _ftmpi_comm[com].comm, 
	 _ftmpi_comm[com].derived_from, _ftmpi_comm[com].group); fflush(stdout);
  printf("freed %d nbref %d maxsize %d csize %d nprocs %d\n", 
	 _ftmpi_comm[com].freed, _ftmpi_comm[com].nbref, _ftmpi_comm[com].maxsize, 
	 _ftmpi_comm[com].currentsize,_ftmpi_comm[com].nprocs);fflush(stdout);

  printf("my_rank %d my_gid 0x%x [%d] com_mode %d msg_mode %d com_state %d epoch %d\n", 
	 _ftmpi_comm[com].my_rank, _ftmpi_comm[com].my_gid, _ftmpi_comm[com].my_gid, _ftmpi_comm[com].ft_com_mode, 
	 _ftmpi_comm[com].ft_msg_mode, _ftmpi_comm[com].ft_com_state, _ftmpi_comm[com].ft_epoch);
  fflush(stdout);
  
  for (i=0;i<_ftmpi_comm[com].nprocs;i++) {
    printf( "entry [%i] GIDs 0x%x [%d], conn_entry %d\n", i, 
	   _ftmpi_comm[com].gids[i], _ftmpi_comm[com].gids[i],
	   _ftmpi_comm[com].conn_entry[i] );
  }
  printf("\n\n"); fflush(stdout);
  
  if ( _ftmpi_comm[com].inter ) {
    group_t *grp;
    printf("Communicator is an inter-communicator \n");fflush(stdout);
    printf("_ftmpi_comm[com].remote_group %d, _ftmpi_comm[com].local_comm %d\n", 
	   _ftmpi_comm[com].remote_group, _ftmpi_comm[com].local_comm );fflush(stdout);
    printf("lleder %d, rleader %d, lleader_gid %d, rleader_gid %d\n",
	   _ftmpi_comm[com].lleader, _ftmpi_comm[com].rleader, _ftmpi_comm[com].lleader_gid, 
	   _ftmpi_comm[com].rleader_gid );fflush(stdout);


    grp = ftmpi_group_get_ptr ( _ftmpi_comm[com].remote_group );
    printf("Displaying the remote group %d, maxsize %d\n", 
	   _ftmpi_comm[com].remote_group,  grp->maxsize);
    fflush(stdout); 
    
    for (i=0;i<grp->maxsize;i++) 
      printf("grp->GIDs[%d] %d \n", i, grp->gids[i]);
    printf ("\n\n"); fflush(stdout);
  }
  else
    {
      printf("Communicator is an intra-communicator\n");

      /* Here we should output the used topology! */
    }
  fflush(stdout);

  return ( MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_comm_dump_compact (MPI_Comm com)
{
   char tname[256];

   
   if (_ftmpi_comm[com].comm != MPI_COMM_NULL) {
	  printf("Com %2d ", _ftmpi_comm[com].comm);
	  printf ("procs %2d ", _ftmpi_comm[com].nprocs);
	  if (_ftmpi_comm[com].am_shadow) printf("Shdw ");
	  if (_ftmpi_comm[com].inter) printf("Inter "); else printf("Intra ");
	  if (_ftmpi_comm[com].freed) printf("Freed ");
	  if (_ftmpi_comm[com].nbref) printf("Refc %1d ", _ftmpi_comm[com].nbref);
	  if (_ftmpi_comm[com].errhandler) printf("Errh %1d ", _ftmpi_comm[com].errhandler);
	  printf("\n");

	  if (_ftmpi_comm[com].msg_list_head) {
		 sprintf(tname, "Com %2d msg_list: ", _ftmpi_comm[com].comm);
		 msg_list_dump_compact (tname, _ftmpi_comm[com].msg_list_head,
			   _ftmpi_comm[com].msg_list_count);
	  }

	  else 
		 printf("Com %d no msg list active\n", _ftmpi_comm[com].comm);

	return (1);
   }
return (0);
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_comm_dump_all_compact ()
{
   int cnt=0;
   int i;

   printf("Gid %d dumping all the communicators in compact format with msg lists\n", my_gid);
   for (i=0;i<MAXCOMS;i++) {
	 cnt += ftmpi_comm_dump_compact ((MPI_Comm)i);
   }
   printf("Gid %d dumped info on %d active communicators\n\n", my_gid, cnt);

   return (0);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_com_reset_all_msg_lists ()
{
   int i;

   for (i=0;i<MAXCOMS;i++) {
	  if (_ftmpi_comm[i].comm!=MPI_COMM_NULL) {
		 	if (!_ftmpi_comm[i].msg_list_head) {
			   fprintf(stderr,"ftmpi_com_reset_all_msg_lists: null msg head?\n");
			   return (MPI_ERR_INTERN);
			}

		  ftmpi_nb_free_all_msgs (_ftmpi_comm[i].msg_list_head);
		  _ftmpi_comm[i].msg_list_count = 0;
	  }
   }

return (0);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_com_reset_shadow_msg_lists ()
{
   int i;

   for (i=1;i<MAXCOMS;i++) {	/* note the 1 */
	   if ((_ftmpi_comm[i].comm!=MPI_COMM_NULL)&&(_ftmpi_comm[i].am_shadow)) 
	   {
		if (!_ftmpi_comm[i].msg_list_head) {
		   fprintf(stderr,"ftmpi_com_reset_shadow_msg_lists: null msg head?\n");
		   return (MPI_ERR_INTERN);
		}
		  msg_list_free_all (_ftmpi_comm[i].msg_list_head);
		  _ftmpi_comm[i].msg_list_count = 0;
	   }
	}
return (0);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_com_move_all_lists_to_system_list (int startcom)
{
   int i;

   for (i=startcom;i<MAXCOMS;i++) {
	  if (_ftmpi_comm[i].comm!=MPI_COMM_NULL) {
		 	if (!_ftmpi_comm[i].msg_list_head) {
			   fprintf(stderr,"ftmpi_com_move_all_lists_to_system_list: null msg head?\n");
			   return (MPI_ERR_INTERN);
			}

		  ftmpi_nb_move_msg_list_to_system_list (_ftmpi_comm[i].msg_list_head);
		  _ftmpi_comm[i].msg_list_count = 0;
	  }
   }

return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_comm_attach_cart(MPI_Comm com, cart_info_t * cart)
{
  DO_STANDARD_COMM_CHECKS( com, 0 );
  
  /*  if(_ftmpi_comm[com].cart_ptr != NULL)
    _FREE(_ftmpi_comm[com].cart_ptr);
  */
  
  if ( _ftmpi_comm[com].topo_type != MPI_UNDEFINED )
    {
      printf("Internal error: trying to attach a cartesian structure"
	     " to a communicator, which has already a topology set\n");
      return (MPI_ERR_INTERN);
    }
  _ftmpi_comm[com].topo_type = MPI_CART;
  _ftmpi_comm[com].cart_ptr = cart;

  return 1;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

cart_info_t * ftmpi_comm_get_cart_ptr(MPI_Comm com)
{
  if((com<0)||(com>=MAXCOMS))
    return(NULL);
  if ( _ftmpi_comm[com].topo_type != MPI_CART)
    return (NULL);
  return((cart_info_t*)_ftmpi_comm[com].cart_ptr);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int ftmpi_comm_attach_graph(MPI_Comm com, graph_info_t * graph)
{
  if((com<0)||(com>=MAXCOMS)) 
    return(0);
  
  if ( _ftmpi_comm[com].topo_type != MPI_UNDEFINED )
    {
      printf("Internal error: trying to attach a graph topology"
	     " to a communicator, which has already a topology set\n");
      return (MPI_ERR_INTERN);
    }
  _ftmpi_comm[com].topo_type = MPI_GRAPH;
  _ftmpi_comm[com].graph_ptr = graph;
  return  ( 1 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
graph_info_t * ftmpi_comm_get_graph_ptr(MPI_Comm com)
{
  if((com<0)||(com>=MAXCOMS))
    return(NULL);
  if ( _ftmpi_comm[com].topo_type != MPI_GRAPH)
    return (NULL);
  return((graph_info_t*)_ftmpi_comm[com].graph_ptr);
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_topo ( MPI_Comm com )
{
  return (_ftmpi_comm[com].topo_type );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_nattr ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].nattr );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_copy_attr ( MPI_Comm corg, MPI_Comm cnew )
{
  int i;
  int ret;

    /* call now for each attribute the user defined copy-operation 
     and set depending on the result the attribute */

  for ( i = 0; i < _ftmpi_comm[corg].nattr ; i ++ ) {
    ret = ftmpi_keyval_copy ( _ftmpi_comm[corg].attrs[i], (int) corg, 
			      (int ) cnew );
    if ( ret != MPI_SUCCESS ) return ( ret );
  }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_set_attr ( MPI_Comm com, int key )
{
  int tmp;
  /* allocate memory if required for the list of
     attributes for the new communicator */

  tmp = _ftmpi_comm[com].nattr;

  if ( tmp > (_ftmpi_comm[com].maxattr-1) ) {
    if ( _ftmpi_comm[com].attrs != NULL ) {
      /* there are already some elements set, we need to enlarge the buffer */
      int *tmpArray, newSize =_ftmpi_comm[com].maxattr;
      /* allocate a bigger array */
      newSize = _ftmpi_comm[com].maxattr + FTMPI_COMM_ATTR_BLOCK_SIZE;
      tmpArray = (int *)_MALLOC( newSize * sizeof(int));
      if( tmpArray == NULL ) return ( MPI_ERR_INTERN );
      
      /* restore the old data into the new array and free the temporary buffer */
      memcpy ( tmpArray, _ftmpi_comm[com].attrs,
	       (size_t)( _ftmpi_comm[com].maxattr * sizeof (int)));
      _FREE( _ftmpi_comm[com].attrs );
      _ftmpi_comm[com].attrs = tmpArray;
      _ftmpi_comm[com].maxattr = newSize;
    } else {
      /* first element to be set, array is not yet allocated */
/*       assert( _ftmpi_comm[com].maxattr == 0 ); */
      _ftmpi_comm[com].maxattr += FTMPI_COMM_ATTR_BLOCK_SIZE;
      _ftmpi_comm[com].attrs = (int *)_MALLOC( _ftmpi_comm[com].maxattr * sizeof(int));
      if ( _ftmpi_comm[com].attrs == NULL ) return ( MPI_ERR_INTERN );
    }
  }
  
  /* set now the element */
  _ftmpi_comm[com].attrs[tmp] = key;
  _ftmpi_comm[com].nattr++;
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_del_attr ( MPI_Comm com, int key )
{
  int i;

  /* find first the according element */
  for ( i = 0; i <_ftmpi_comm[com].nattr; i++ )
    if ( _ftmpi_comm[com].attrs[i] == key ) break;

  /* the impossible error: at this level we shouldn't have an invalid key */
  if ( i >= _ftmpi_comm[com].nattr ) return ( MPI_ERR_INTERN );

  _ftmpi_comm[com].nattr--;

  /* now we have to shrink the array. I hope memmove doesn't have a problem to move
     0 bytes, because this might happen....*/
  if ( _ftmpi_comm[com].nattr > 0 )
    memmove ( &(_ftmpi_comm[com].attrs[i]), &(_ftmpi_comm[com].attrs[i+1]),
	      ((_ftmpi_comm[com].nattr-i) * sizeof(int)));

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_errhandler ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].errhandler);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_set_errhandler ( MPI_Comm com, int err_handle)
{
  _ftmpi_comm[com].errhandler = err_handle;
  _ftmpi_comm[com].errfn = (MPI_Comm_errhandler_fn *) ftmpi_errh_get_function ( err_handle );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_copy_errhandler ( MPI_Comm cold, MPI_Comm cnew )
{
  int handle;

  handle = ftmpi_com_get_errhandler ( cold );
  ftmpi_com_set_errhandler ( cnew, handle );
  ftmpi_errhval_mark ( handle, cnew );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_create_shadow ( MPI_Comm comm, MPI_Comm shadow )
{
  int rc;
  int inter;
  MPI_Group org_grp, new_grp;

  inter = ftmpi_com_test_inter ( comm );

  rc = ftmpi_com_copy (comm, &shadow); /* This routine copies the topology */
                                       /* stuff aswell.   EG Feb. 5 2003 */
  
  if (rc<0) return ( rc); /* ftmpi_com_copy sets the correct MPI rc */
                          /* take note Tone, rc's get passed through */
	         	  /* multiple layers */

  /* we need to also update the com to group relationships... */
  org_grp = ftmpi_com_get_group (comm);	/* get original group */
  
  /* ftmpi_com_set_group (nc, org_grp);	*/
  /* make new com link to same group */
  /* note this increments grp ref cnts */
  /* we originally set both coms to point to the same group */
  /* but now they have their own groups */
  
  rc = ftmpi_group_copy (org_grp, &new_grp);
  if (rc<0) return ( rc);
  
  rc = ftmpi_com_set_group (shadow, new_grp);
  if (rc<0) return ( rc);


  _ftmpi_comm[shadow].am_shadow = 1;
  _ftmpi_comm[shadow].parent    = comm;
  _ftmpi_comm[comm].shadow      = shadow;

  if ( inter )
    ftmpi_com_copy_intercom ( comm, shadow ); 
    
  /* Copy error - handler */
  ftmpi_com_copy_errhandler ( comm, shadow );


  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
MPI_Comm ftmpi_com_get_shadow ( MPI_Comm com )
{
  return (_ftmpi_comm[com].shadow );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_test_shadow ( MPI_Comm com )
{
  return (_ftmpi_comm[com].am_shadow );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
MPI_Comm ftmpi_com_get_parent ( MPI_Comm com )
{
  return (_ftmpi_comm[com].parent );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_init_errh_toabort ()
{
  int handle, tmperrh;
  MPI_Errhandler errhandler=MPI_ERRORS_ARE_FATAL;

  handle = ftmpi_errh_get_handle ( errhandler );

  /* since we have to unmark the 'old' errhandler, we have
     to recover it first ...*/
  tmperrh = ftmpi_com_get_errhandler ( MPI_COMM_WORLD );
  ftmpi_errhval_unmark (tmperrh, MPI_COMM_WORLD );
	  
  /* Set now the new errhandler. Register it at the errhandler list
     and at the communicator */
  ftmpi_errhval_mark ( handle, MPI_COMM_WORLD );
  ftmpi_com_set_errhandler ( MPI_COMM_WORLD, handle );


  tmperrh = ftmpi_com_get_errhandler ( MPI_COMM_SELF );
  ftmpi_errhval_unmark (tmperrh, MPI_COMM_SELF );
	  
  /* Set now the new errhandler. Register it at the errhandler list
     and at the communicator */
  ftmpi_errhval_mark ( handle, MPI_COMM_SELF );
  ftmpi_com_set_errhandler ( MPI_COMM_SELF, handle );

  return ( MPI_SUCCESS);
}
