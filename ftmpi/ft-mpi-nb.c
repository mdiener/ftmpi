/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <assert.h>

#include <string.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-nb.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-fifo.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-p2p.h"
#include "ft-mpi-conn.h" /* as we use make_hdr conn_send_data etc */
#include "ft-mpi-procinfo.h"
#include "ft-mpi-buffer.h"
#include "debug.h"


/* FTMPI_HDR_PAYLOAD is not really what it should be so here we use */
/* a different flag */
#define SINGLEWRITEBUFFERSIZE (4096)

/* point at which we switch from a tight loop to the memcopy call */
/* used in post_send call when sending a combined hdr&data/payload */
/* #define USEMEMCOPYLIMIT	(SINGLEWRITEBUFFERSIZE) */
#define USEMEMCOPYLIMIT	(0)
/* if SINGLEWRITEBUFFERSIZE means never use */
/* if 0 means always use */

extern int my_gid;	/* usually imported via lib.h */


#define MAXREQ	2050	/* mpich tester needs 2048 to pass..... grr */

#define MAXFREEDREQ	50 /* how many we leave without freeing */
#define INITREQS	40  /* how many request structs we initially create */

struct req_list	* reqtable[MAXREQ];

long	nextreqid;	/* the next request id (not same as user level handle) */
					/* this starts always with '1' */
					/* cannot be 0 as msgid of 0 means no matching req id */

struct req_list * req_list_send_head=NULL;
struct req_list * req_list_recv_head=NULL;
struct req_list * req_list_done_head=NULL;
struct req_list * req_list_recvs_inprogress_head=NULL;

/* some globals needed to keep count of total MPI nb operations */
/* if these are both zero then blocking ops should go faster without attempting to access the */
/* progress engine etc */

long req_list_send_count=0;		/* how many sends are there to complete */
long req_list_recv_count=0;		/* how many recvs are on the queue */
long req_list_done_count=0;		/* how many requests on the done queue */
long req_list_recvs_inprogress_count=0;	/* how many recvs are in progress */

/* unexpected messages are stored completed in their com owned Qs */
/* incompleted unexpected messages are stored here so we can match NB recvs */
/* to them to prevent races and simply handling on completition of unexp recv */
struct msg_list * incomplete_msg_list_head = NULL;
long incomplete_msg_list_count;

/* messages recvd from non existing communicators are stored here */
/* this can occur during recovery or a race during communicator creation */
struct msg_list * system_msg_list_head = NULL;
long system_msg_list_count;


/* main progress loop state flags */
/* these are used so that we know what happened and avoid recursive calls */

fifo_t*	progress_queue;

/* call back protocols */

/* the recv is split and in conn. George will move this into here for OVM */
/* int conn_snipe2_recvhdr_callback (SNIPE2CALLBACKPROTO); */
int ftmpi_nb_snipe2_sentdata_callback (SNIPE2CALLBACKPROTO);

/* temp, for two part send operations only */
int ftmpi_nb_snipe2_sentjustdatahdr_callback (SNIPE2CALLBACKPROTO);


/* very temp check flag */
int ftmpi_nb_check_unexpected_queue_again=0;
/* this must be replaces real soon. TODO GEF Oct03 */



/* 
 * this routine creates all the NB req lists in one go
 * (unexpected msg queues are handled by com still)
 * But we also now also create the SNIPE2 pending event queue here as well 
*/
void ftmpi_nb_init_req_and_msg_lists()
{
  /* alocate new req free list */
  req_list_init_freelist(INITREQS, MAXFREEDREQ);

  /* alocate new req sending list */
  req_list_send_head = req_list_init();
  req_list_send_count = 0;
			  
  /* alocate new req recving list */
  req_list_recv_head = req_list_init();
  req_list_recv_count = 0; 

  /* alocate new req completed list */
  req_list_done_head = req_list_init();
  req_list_done_count = 0;

  /* alocate new req for partly completed recvs (inprogress) list */
  req_list_recvs_inprogress_head = req_list_init();
  req_list_recvs_inprogress_count = 0;

  /* incomplete message queues */
  /* i.e. unexpected messages that are not fully recvd yet */
  incomplete_msg_list_head = msg_list_init ();
  incomplete_msg_list_count = 0;

  /* stsyem lists */
  system_msg_list_head = msg_list_init ();
  system_msg_list_count = 0;

  /* progress queue */
  progress_queue = fifo_queue_init ();
}

/*
 * this function clears all nb_request & msg lists 
 */
void ftmpi_nb_reset_req_and_msg_lists()
{
  /* reset req sending list */
  ftmpi_nb_free_all_reqs (req_list_send_head);
  req_list_send_count = 0;
			  
  /* reset req recving list */
  ftmpi_nb_free_all_reqs (req_list_recv_head);
  req_list_recv_count = 0; 

  /* reset req completed list */
  ftmpi_nb_free_all_reqs (req_list_done_head);
  req_list_done_count = 0;

  /* alocate new req for partly completed recvs (inprogress) list */
  ftmpi_nb_free_all_reqs (req_list_recvs_inprogress_head);
  req_list_recvs_inprogress_count = 0;

  /* incomplete message queues */
  /* i.e. unexpected messages that are not fully recvd yet */
  ftmpi_nb_free_all_msgs (incomplete_msg_list_head);
  incomplete_msg_list_count = 0;

  /* stsyem lists */
  ftmpi_nb_free_all_msgs (system_msg_list_head);
  system_msg_list_count = 0;

}

void ftmpi_nb_reset_unexpected_and_inprogress_msg_lists()
{
  /* alocate new req for partly completed recvs (inprogress) list */
  ftmpi_nb_free_all_reqs (req_list_recvs_inprogress_head);
  req_list_recvs_inprogress_count = 0;

  /* incomplete message queues */
  /* i.e. unexpected messages that are not fully recvd yet */
  ftmpi_nb_free_all_msgs (incomplete_msg_list_head);
  incomplete_msg_list_count = 0;
}

void ftmpi_nb_free_all_reqs (struct req_list* rl_head)
{
   struct req_list* rlp;
   int done=0;

   if (!rl_head) return; /* if bad list return rather than deadlock */

   while (!done) {
	  rlp = req_list_head (rl_head);
	  if (rlp) ftmpi_nb_req_force_free (rlp);
	  else done=1;
   }
return;
}

void ftmpi_nb_free_all_msgs (struct msg_list* ml_head)
{
   struct msg_list* mlp;
   int done=0;

   if (!ml_head) return; /* if bad list return rather than deadlock */

   while (!done) {
	  mlp = msg_list_head (ml_head);
	  if (mlp) ftmpi_nb_msg_force_free (mlp);
	  else done=1;
   }
return;
}


/* 
 clears/init all handles 
*/
void ftmpi_nb_clearall_reqhandles ()
{
  int i;

  for(i=0;i<MAXREQ;i++) {
    reqtable[i] = NULL;
  }
  
  nextreqid = 1; /* must always start with above '0' value */
  
  
  /* hope this is the right place to rest these */
/*   req_list_send_count = 0; */
/*   req_list_recv_count = 0; */
/*   req_list_done_count = 0; */
}

/* -- misc debug routines -- */
/* used to debug HPL/BLACS/the usual suspects */
/* GEF */

int ftmpi_nb_dump_req_table (int verbose)
{
  int i;
  int tableinuse=0;
  int markeddone=0;
  int markedfreed=0;
  int markedcancelled=0;
  int disconnected=0;
  struct req_list *rlp;
  int send, recv;
  
  
  /* first do a count */
  for (i=0;i<MAXREQ;i++) {
    if (reqtable[i]) { /* its in use */
      tableinuse++;
      rlp = reqtable[i];
      /* now individual counts */
      if(rlp->rl_status.flags & FTMPI_REQ_DONE) markeddone++;
      if(rlp->rl_status.flags & FTMPI_REQ_FREED) markedfreed++;
      if(rlp->rl_status.flags & FTMPI_REQ_CANCELLED) markedcancelled++;
      if (!rlp->rl_rlink) disconnected++;
    } /* if in use */
  } /* for each req */
  
  printf("----------------------------------------------------------------------------------\n");
  printf("ftmpi_nb_dump_req_table: table size %d inuse %d \n", MAXREQ, tableinuse);
  printf("Marked: done %d freed %d cancelled %d disconnected %d\n",
	 markeddone, markedfreed, markedcancelled, disconnected);
  printf("Next low level request ID %ld\n", nextreqid);
  printf("----------------------------------------------------------------------------------\n");
  
  /* if any entries in use, display them in compacted form if verbose */
  if ((tableinuse)&&(verbose)) {
    printf("index\thandle\tid\ts/r\ttrank\tdone\tcancel\tfreed\tdiscon\trefcnt\tmlen\n");
    for (i=0;i<MAXREQ;i++) {
      if (reqtable[i]) { /* its in use */
	rlp = reqtable[i];
	printf("%d\t%d\t%ld\t", i, rlp->rl_req_handle, rlp->rl_req_id);
	
	/* now more complex info */
	send = ftmpi_nb_is_send (i);
	recv = ftmpi_nb_is_recv (i);
	if (send) printf("S\t");
	if (recv) printf("R\t");
	if (!(send||recv)) printf("??\t");
	
	printf("%d\t", rlp->rl_target_rank);
	
	if (rlp->rl_status.flags & FTMPI_REQ_DONE) printf("Y\t");
	else printf("N\t");
	if (rlp->rl_status.flags & FTMPI_REQ_FREED) printf("Y\t");
	else printf("N\t");
	if (rlp->rl_status.flags & FTMPI_REQ_CANCELLED) printf("Y\t");
	else printf("N\t");
	if (!rlp->rl_rlink) printf("Y\t");
	else printf("N\t");
	
	printf("%d\t", rlp->rl_req_refcnt);
	
	printf( "%d\n", rlp->rl_status.msglength );
      } /* if in use */
    } /* for each req */
    printf("\n");
  }
  
  printf("----------------------------------------------------------------------------------\n");
  fflush(stdout);
  return 0;
}


/* ---------------------------------------------------------------------------------- */
/* Routines to handle request HANDLES themselves */
/* ---------------------------------------------------------------------------------- */

/*
 get next free request handle from handle table 
 */

int ftmpi_nb_get_freehandle ()
{
  int i;

  for(i=0;i<MAXREQ;i++) 
    if (reqtable[i]==NULL) return (i);
  
  return (MPI_REQUEST_NULL);
}

/*
 verify request handle is valid 
 should check if its freed etc etc 
 */

/* inline int ftmpi_nb_req_ok (MPI_Request rh) */
int ftmpi_nb_req_ok (MPI_Request rh)
{
  if ((rh<0)||(rh>=MAXREQ)) return (0);
  if (!reqtable[rh]) return (0);
  return (1);
}

/* get ptr to request object itself */
/* inline struct req_list * ftmpi_nb_get_reqlist_ptr_by_handle (MPI_Request rh) */
struct req_list * ftmpi_nb_get_reqlist_ptr_by_handle (MPI_Request rh)
{
  if (ftmpi_nb_req_ok(rh)) return (reqtable[rh]);
  return (NULL);
}

/* 
 has the request been done yet?
 */
int ftmpi_nb_req_isdone (MPI_Request rh)
{
  struct req_list *rlptr;

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( rh );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_status.flags & FTMPI_REQ_DONE);
}


/* 
 has the request been cancelled?
 */

int ftmpi_nb_req_iscancelled (MPI_Request rh)
{
  struct req_list *rlptr;

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( rh );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_status.flags & FTMPI_REQ_CANCELLED);
}

/* 
 Cancel a request
 Or mark it as cancelled and move it to the completed list
 */

int ftmpi_nb_req_cancel (MPI_Request rh)
{
  struct req_list *rlptr;
  int send=0;
  int recv=0;
  int rc=0;
  int lowlevelcancel=0;
  
  
  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( rh );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  
  /* if it's already cancelled or completed just return MPI_SUCCESS */
  if( rlptr->rl_status.flags & (FTMPI_REQ_CANCELLED|FTMPI_REQ_DONE) ) {
    return (MPI_SUCCESS);
  }
  
  /* depending if its a send or recv move it from that list to done list */
  send = ftmpi_nb_is_send (rh);
  recv = ftmpi_nb_is_recv (rh);
  
  if (send)
    rc = ftmpi_nb_move_send_2_done (rlptr);
  	/* should check to see if its really on the inprogress Q as well!! GGGG */
  if (recv)
    rc =  ftmpi_nb_move_recv_2_done (rlptr);
  
  /* mark it as cancelled */
  /* but only if we have cancelled it */
  rlptr->rl_status.flags |= FTMPI_REQ_CANCELLED;
  
  /* as we want it marked as cancelled but not done. we have to reset the done */
  /* lets not mark it just in case a test else where checks for a done flag */
  /* BUT does look which queue its on but instead if just the flag is set. */
  /* rlptr->rl_req_done = 0; */
  /* GEF I hope that is right :) */

  /* if it is possible to cancel at the device level */
  /* GEORGE take note */
  if (rlptr->rl_snipe2_reqptr) {
  		lowlevelcancel = snipe2_cancel_msg (rlptr->rl_snipe2_reqptr);
		if (lowlevelcancel>0) rlptr->rl_snipe2_reqptr = NULL;
  }

  return (rc);
}

/*
 *
 * this is just like a normal cancel, except....
 * if the request is already done it now gets marked as both done & cancelled 
 * also we use a rlp rather than a handle as it is used to cancel the failed
 * processes communications on the send/recv queues
 *
 */
int ftmpi_nb_req_force_cancel (struct req_list *rlp)
{
  int send=0;
  int recv=0;
  int rc=0;
  int lowlevelcancel=0;
  
  
  if( rlp == NULL ) return MPI_ERR_REQUEST;
  
  /* if it's already cancelled return 1 */
  if( rlp->rl_status.flags & FTMPI_REQ_CANCELLED ) {
    return (1);
  }

  /* if it's already completed just return 1 and mark cancelled as well!! */
  if( rlp->rl_status.flags & FTMPI_REQ_DONE) {
	 rlp->rl_status.flags |= FTMPI_REQ_CANCELLED;
    return (1);
  }

  /* ok, its on either the send or recv queues */
  
  /* depending if its a send or recv move it from that list to done list */
  if( rlp->rl_req_type & REQ_SEND_MASK ) send=1;
  if( rlp->rl_req_type & REQ_RECV_MASK ) recv=1;
  
  if (send)
    rc = ftmpi_nb_move_send_2_done (rlp);
  if (recv)
    rc =  ftmpi_nb_move_recv_2_done (rlp);
  
  /* mark it as cancelled */
  /* but only if we have cancelled it */
  rlp->rl_status.flags |= FTMPI_REQ_CANCELLED;
  
  /* as we want it marked as cancelled but not done. we have to reset the done */
  /* lets not mark it just in case a test else where checks for a done flag */
  /* BUT does look which queue its on but instead if just the flag is set. */
  /* rlptr->rl_req_done = 0; */
  /* GEF I hope that is right :) */

  /* if it is possible to cancel at the device level */
  /* GEORGE take note */
  if (rlp->rl_snipe2_reqptr) {
  		lowlevelcancel = snipe2_cancel_msg (rlp->rl_snipe2_reqptr);
		if (lowlevelcancel>0) rlp->rl_snipe2_reqptr = NULL;
  }

  return (rc);
}

/* Generic routine to handle communications with MPI_PROC_NULL.
 * We should always return a request for all non blocking communications
 * as the user could ask for the status later. So the idea is to create
 * fake requests, and insert them directly in the done queue. Later all of them
 * could use the same static request as the status field should always contain
 * same information:
 *   source = MPI_PROC_NULL , tag = MPI_ANY_TAG and count = 0.
 */
int ftmpi_nb_handle_proc_null( MPI_Comm comm, MPI_Request* request )
{
  FTMPI_Request_t* pRequest;

  *request = ftmpi_nb_get_freehandle();
  if( *request == MPI_REQUEST_NULL ) return MPI_ERR_INTERN;

  /* first lets allocate the request */
  reqtable[*request] = pRequest = req_list_new_element_only();
  pRequest->rl_req_com = comm;
  pRequest->rl_req_refcnt = 1;

  req_list_add_2tail (req_list_done_head, pRequest);	/* add to done list */
  
  /* update done counter */
  req_list_done_count++;
  
  /* and finally one last important update */
  pRequest->rl_status.flags = FTMPI_REQ_DONE;
  /* and dont forget to set the status correctly */
  pRequest->rl_status.MPI_ERROR = MPI_SUCCESS;
  pRequest->rl_status.MPI_SOURCE = MPI_PROC_NULL;
  pRequest->rl_status.MPI_TAG = MPI_ANY_TAG;
  pRequest->rl_status.msglength = 0;
  return MPI_SUCCESS;
}



/* routines to handle lists */

/* send list first */

/* this routine adds the request to the send list. */
/* all it assumes is that the req handle is chosen for it first! */
/* and that the comm is valid etc */

int ftmpi_nb_add2_send_list (void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request req)
{
  struct req_list *rlp;		/* actual element list */

  rlp = req_list_new (req_list_send_head);	
  /* get a new element and add it to the list */
  
  if (!rlp) return (MPI_ERR_INTERN);
  
  rlp->rl_req_refcnt = 1;	/* after new we know refcnt should be 1 */
  
  rlp->rl_req_id = nextreqid++;		/* a new request gets a new id each time */
  
  /* fill in request details */

  rlp->rl_req_handle = req;
  rlp->rl_req_type = REQ_ISEND;
  rlp->rl_req_com = comm;
  rlp->rl_posters_rank = ftmpi_com_rank (comm);
  rlp->rl_target_rank = dest;
  rlp->rl_mpi_tag = tag;
  rlp->rl_sgid = my_gid;	/* sending so I am sender */
  rlp->rl_rgid = ftmpi_com_gid (comm, dest);
  rlp->rl_req_com_ver=0;	/* should be epoch value */
  rlp->rl_req_cnt = count;
  rlp->rl_req_dt = datatype;
  rlp->rl_req_usrbuf = (char*) buf;
  rlp->rl_req_rawbuf = (char*) NULL;
  rlp->rl_req_rawbufid = -1;
  rlp->rl_status.MPI_SOURCE = MPI_ANY_SOURCE; /* it's a send */
  rlp->rl_status.MPI_TAG = tag;       /* not known yet */
  rlp->rl_status.MPI_ERROR = MPI_SUCCESS;     /* still correct :) */
  rlp->rl_status.msglength = 0;	              /* not know yet */
  rlp->rl_status.flags = FTMPI_REQ_EMPTY;     /* clear the flags */
  
  rlp->rl_req_rts = 0;	/* have not sent a RTS */
  rlp->rl_req_cts = 0;	/* have not revcd a CTS */
  rlp->rl_req_msgid = 0;	/* do not have a matching send-recv pair */

  /* SNIPE2 stuff */
  rlp->rl_snipe2_reqptr = NULL; /* must be null until posted */
  rlp->rl_snipe2_conn = 0;
  rlp->rl_snipe2_rc   = 0;
  
  /* here we do a calculation based on the datatype etc to see if it needs to be */
  /* sent directly or buffered */
  
  rlp->code_info = 
	 (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(rlp->rl_rgid);

  rlp->rl_req_dt_op = ftmpi_ddt_encode_size_det(datatype,count,
						&(rlp->rl_status.msglength),
						rlp->code_info);

  req_list_send_count++;		/* inc count of sends pending */

  /* as we know the request handle and we don't want to pass back the request element ptr */
  /* we do all the req table updates here */
  /* GEF jan03 */
  
  reqtable[req] = rlp;		/* link request handle to request element */
  
  return (MPI_SUCCESS);
}






/* matching calls */


/* this routine takes a requestion handle and a ptr to a status object */
/* checks the complete list for that req */
/* if a match is found it copies the status object across */

/* allowfree flag indicates if we should free the request if found */
/* if we free the request, we return '2' */
/* if we just find a match we return '1' */
/* if no match we return '0' */

int ftmpi_nb_find_completed (MPI_Request rh, MPI_Status *status, 
								int allowfree)
{
  struct req_list *rlp;		/* actual element list */
  /* int i, j; */
  int rc;

  rlp = reqtable[rh];	/* get request element */
  
  /* now we can check if its done without more work */
  
  if( !(rlp->rl_status.flags & FTMPI_REQ_DONE) ) return (0); /* its not done yet mate */
 
  /* else its done/completed so set the status if possible and then */
  /* free it if required/allowed by ref counts */

  /* if MPI_Status is not an ignore and it is either a */
  /* READ request or READ type request then we can copy over the status info */
  
  if (status!=MPI_STATUS_IGNORE) {
    /* simply copy the status from the request in all cases.
     * That should handle everything fine
     */
    *status = rlp->rl_status;
  }

  /* The buffer used in buffered-sends has to be marked as free */
  if ( (rlp->rl_req_type == REQ_PBSEND) || (rlp->rl_req_type == REQ_IBSEND) )
    ftmpi_buffer_used_free ( rh );
  
  
  if (allowfree) { /* i.e. a probe is not, but a wait is */
    /* we dec the ref count to the request, which also decs the dt refcnt */
    rc = ftmpi_nb_req_decref (rh);
    
    /* before we go, we update the count of done/completed requests */
    req_list_done_count--;
    
    if (rc==1) return (2);	/* match found, AND req removed */
    
    /* we are allowed to reset done flag inside request here */
    /* this is incase we continue to use the request (persistant) */
    rlp->rl_status.flags ^= FTMPI_REQ_DONE;
  }
  
  return (1);	/* match found, but not deleted */
}

/* 
 This calls progress until a request completes, unless there is an error
 But it does not set an error code
 */

int	ftmpi_nb_complete_upto_req (MPI_Request req)
{
    int rc=0;
    int done =0;
    
    if ((req<0)||(req>=MAXREQ)) return (MPI_ERR_REQUEST);
    if (!reqtable[req]) return (MPI_ERR_REQUEST);
    
    /* is it done already ?? */
    rc = ftmpi_nb_req_isdone(req);
    if (rc) return (rc);
    
#ifdef VERBOSES2
    printf ("ftmpi_nb_progress_recv_req called on req %d\n", req);
#endif

    while (!done) {
#ifdef VERBOSES2
	printf("pre: complete_upto_req: req %d done %d\n", req, done);
#endif
	rc = ftmpi_nb_progress(1); /* 1 = non busy wait */
	if (rc<0) return (rc);
	done = ftmpi_nb_req_isdone(req);
#ifdef VERBOSES2
        printf("post: complete_upto_req: req %d done %d\n", req, done);
#endif
	if (done<0) return (done);
    }
  
    return (done);
}


/*
 These functions support higher level request operations like
 persistant requests (i.e. as needed by Edgar)
 GEF
 */

/* this return the target rank or error */

int ftmpi_nb_get_req_target_rank (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_target_rank);
}


/* this returns the nb operation type or error */

int ftmpi_nb_get_req_type (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_req_type);
}


/* this returns the nb rawlen (i.e. amount of data to be passed on the wire!) */

int ftmpi_nb_get_req_rawlen (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_status.msglength);
}

/* this returns the nb datatype (i.e. so we can inc or dec ref counts) */

int ftmpi_nb_get_req_datatype (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  return (rlptr->rl_req_dt);
}


/* this sets the nb operation type or error */
int ftmpi_nb_put_req_type (int type, MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  rlptr->rl_req_type = type;
  return (0);
}


/* set data values in the request elements for buf/cnt/dt  */
/* ** this should only be used when doing a bsend persistant operation ** */

int ftmpi_nb_set_req_buf_cnt_dt( void* buf, int count, MPI_Datatype datatype,
				 MPI_Request req )
{
  struct req_list *rlptr;       /* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;

  rlptr->rl_req_cnt = count;
  rlptr->rl_req_dt = datatype;
  rlptr->rl_req_usrbuf = (char*) buf;
  
  return (0);
}


/* set/get data values in the request elements for bsendinit  */
int ftmpi_nb_set_req_bsendinit (void* buf, int count, MPI_Datatype datatype,
				MPI_Request req)
{
  struct req_list *rlptr;       /* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
 
  rlptr->rl_req_per_cnt = count;
  rlptr->rl_req_per_dt = datatype;
  rlptr->rl_req_per_usrbuf = (char*) buf;
  
  return (0);
}

int ftmpi_nb_get_req_bsendinit (void** buf, int* count, MPI_Datatype* datatype,
				MPI_Request req)
{
  struct req_list *rlptr;       /* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;

  if (count) *count = rlptr->rl_req_per_cnt;
  if (datatype) *datatype = rlptr->rl_req_per_dt;
  if (buf) *buf = rlptr->rl_req_per_usrbuf;
  
  return (0);
}


/* this returns the nb request refcnt */

int ftmpi_nb_get_req_refcnt (MPI_Request req)
{
  struct req_list *rlptr;       /* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  
  return (rlptr->rl_req_refcnt);
}

/* this routine return 1 if its a send of some kind */

/* this routine return 1 if its a send of some kind */
/* get req type does almost the same but you then need to compare against a list */
/* unlike other tests if we error we return 0 */
/* the fault is found when the req is neither a send/recv/done or cancelled.. */

int ftmpi_nb_is_send (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;

  if( rlptr->rl_req_type & REQ_SEND_MASK ) return 1;
  return 0;
}

/* this routine is like above, it returns 1 if the req is a recv op of any type */
/* unlike other tests if we error we return 0 */
/* the fault is found when the req is neither a send/recv/done or cancelled.. */

int ftmpi_nb_is_recv (MPI_Request req)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;

  if( rlptr->rl_req_type & REQ_RECV_MASK ) return 1;
  return 0;
}


/*
 This function allocates a request
 and fills in the data structure req_list without adding it to a 
 particular queue

 caller MUST later add this to a list or FREE it to avoid memory problems
 */

int ftmpi_nb_set_req (void* buf,int count,MPI_Datatype datatype,int
them, int tag, MPI_Comm comm, int type, MPI_Request req)
{
  struct req_list *rlp;		/* actual element list */

  /* using new req_list call :) */
  rlp = req_list_new_element_only (); /* note no head of list needed */
  if (!rlp) return (MPI_ERR_INTERN);
  
  rlp->rl_req_refcnt = 1;
  rlp->rl_req_id = nextreqid++;		/* a new request gets a new id each time */
  
  /* make sure User level Requests point to this element */
  /* this makes sure we don't lose it and thus we can find/free it again */
  reqtable[req] = rlp;
  
  /* now set the structs contents */
  rlp->rl_req_handle = req;
  rlp->rl_req_type = type;
  rlp->rl_req_com = comm;
  rlp->rl_posters_rank = ftmpi_com_rank (comm);
  rlp->rl_target_rank = them;
  rlp->rl_mpi_tag = tag;   /* TODO we already have the tag in the rl_status ?? */
  rlp->rl_sgid = my_gid;
  rlp->rl_req_com_ver=0;	/* should be epoch value */
  rlp->rl_req_cnt = count;
  rlp->rl_req_dt = datatype;
  rlp->rl_req_usrbuf = (char*) buf;
  rlp->rl_req_rawbuf = (char*) NULL;
  rlp->rl_req_rawbufid = -1;

  
  rlp->rl_status.MPI_SOURCE = MPI_ANY_SOURCE;
  rlp->rl_status.MPI_TAG = tag;
  rlp->rl_status.MPI_ERROR = MPI_SUCCESS;
  rlp->rl_status.msglength = 0;	/* not know yet */
  rlp->rl_status.flags = FTMPI_REQ_EMPTY; /* clear the flags */

  rlp->rl_req_rts = 0;	/* have not sent a RTS */
  rlp->rl_req_cts = 0;	/* have not revcd a CTS */
  rlp->rl_req_msgid = 0;	/* do not have a matching send-recv pair */
  
  /* here we do a calculation based on the datatype etc to see if it needs to be */
  /* accessed directly from the users buffer or buffered via the rawbuffer */

  if ( !(type & REQ_RECV_MASK) ) { /* send operation */
    /* if this is a SEND op */
    rlp->rl_rgid = ftmpi_com_gid (comm, them);
    rlp->code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(rlp->rl_rgid);
    rlp->rl_req_dt_op = ftmpi_ddt_encode_size_det(datatype,count,&(rlp->rl_status.msglength),rlp->code_info);
  } else { /* its a RECV op */
    rlp->rl_rgid = my_gid;
    if( them != MPI_ANY_SOURCE) rlp->rl_sgid = ftmpi_com_gid (comm, them);
    else rlp->rl_sgid = 0; /* not known yet */
    rlp->code_info = NULL;
    rlp->rl_req_dt_op = -1;
  } 

  /* SNIPE2 stuff */
  rlp->rl_snipe2_reqptr = NULL;
  rlp->rl_snipe2_conn = 0;
  rlp->rl_snipe2_rc   = 0;

  return (0);
}


/*
 This routine ftmpi_nb_req_add2_send_list is almost the same as
 ftmpi_nb_add2_send_list except here we pass in a req handle to an 
 req element that is all ready filled in using the ftmpi_nb_set_req
 call. i.e. isends do it in one call and persisant sends do it in two...
*/

int ftmpi_nb_req_add2_send_list (MPI_Request req, int updaterefcnt)
{
  struct req_list *rlptr;	/* actual element list */

  /* here we have to figure out the comm from the req itself */
  /* so first get the request */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( req );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;

  /* adds this to the send queue */
  req_list_add_2tail (req_list_send_head, rlptr);	
  
  req_list_send_count++;		/* inc count of sends pending */
  
  if(updaterefcnt) rlptr->rl_req_refcnt++; 		/* inc ref count for REQUEST */
  
  return (MPI_SUCCESS);
}



/*
 This routine ftmpi_nb_req_add2_recv_list is almost the same as
 ftmpi_nb_add2_recv_list except here we pass in a req handle to an 
 req element that is all ready filled in using the ftmpi_nb_set_req
 call. i.e. irecv does it in one call and persistant recvs do it in two...
*/

int ftmpi_nb_req_add2_recv_list (MPI_Request rh, int updaterefcnt)
{
  struct req_list *rlptr;		/* actual element list */

  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( rh );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  
  /* adds this to the recv queue */
  req_list_add_2tail (req_list_recv_head, rlptr);	

  if(updaterefcnt) rlptr->rl_req_refcnt++; 	/* inc ref count for REQUEST */
  
  req_list_recv_count++;	/* inc count of sends pending */
  
  return (MPI_SUCCESS);
}


/*
 adds completed requests to the done list queue
 DOES not assume it was any list (hense its a ADD not a MOVE)
 */

/* static inline int ftmpi_nb_add_req_2_done (struct req_list* rlptr) */
static int ftmpi_nb_add_req_2_done (struct req_list* rlptr)
{
  
  req_list_add_2tail (req_list_done_head, rlptr);	/* add to done list */
  
  /* update done counters */
  req_list_done_count++;
  
  /* and finally one last important update */
  rlptr->rl_status.flags |= FTMPI_REQ_DONE;
  
  return(0);
}

int ftmpi_nb_move_recv_2_done (struct req_list* rlptr)
{
  /* update recv counter */
  req_list_recv_count--;
  req_list_detach (rlptr);	/* remove request from the recv list */
  return ftmpi_nb_add_req_2_done( rlptr );
}

int ftmpi_nb_move_recv_inprogress_2_done (struct req_list* rlptr)
{
  /* update recv counter */
  req_list_recvs_inprogress_count--;
  req_list_detach (rlptr);	/* remove request from the recv list */
  return ftmpi_nb_add_req_2_done( rlptr );
}

int ftmpi_nb_move_recv_2_inprogress (struct req_list* rlptr)
{
  /* update recv counter */
  req_list_recv_count--;
  req_list_detach (rlptr);	/* remove request from the recv list */

  /* do the move */
  req_list_add_2tail (req_list_recvs_inprogress_head, rlptr);	
  /* add to inprogress list */
  
  /* update inprogress counters */
  req_list_recvs_inprogress_count++;

  return (0);
}


int ftmpi_nb_move_send_2_done (struct req_list* rlptr)
{
  /* update send counters */
  req_list_send_count--;
  req_list_detach (rlptr);	/* remove request from the send list */
  return ftmpi_nb_add_req_2_done( rlptr );
}


/* high level version using the request handle */
/* used by the lib-nb lib-perms level */
int ftmpi_nb_move_send_request_2_done (MPI_Request sreq)
{
   struct req_list *rlp;       /* request list queue */
   int ret;


   /* here we have to figure out the comm from the req itself */
   /* so first get the request */

   rlp = reqtable[sreq];   /* get request element */
   if (!rlp) return (MPI_ERR_REQUEST);

   ret =  ftmpi_nb_move_send_2_done (rlp);
   return (ret);

}

/*
 This routine ftmpi_nb_req_add2_done_list is almost the same as
 all other ftmpi_nb_add2_XXX_list calls except here we pass in a req 
 handle to an req element that is all ready filled in using the 
 ftmpi_nb_set_req call.
 NOTE this request is NOT expected to be on either a send or recv list yet!
 i.e. used if a match a recv to an unexpected message immediately
*/
int ftmpi_nb_req_add2_done_list (MPI_Request rh, int updaterefcnt)
{
  struct req_list *rlptr;		/* actual element list */
  
  rlptr = ftmpi_nb_get_reqlist_ptr_by_handle( rh );
  if( rlptr == NULL ) return MPI_ERR_REQUEST;
  
  if(updaterefcnt) rlptr->rl_req_refcnt++; /* inc ref count for REQUEST */

  return ftmpi_nb_add_req_2_done( rlptr );
}

/*
 This routine takes a message stored in the tmp buffer of a req
  and passes it back to the user space buffer 
  (if it wasn't already done that way)

  control of what to do is via the rl_req_dt_op field of the req element 
    0 = no buffering/conversion data should have been put into userspace already
  1 = data is in the rawbuffer and needs to be decoded into the users buffer
  2 = data is in the rawbuffer and needs to be decoded and xdrd into the usr buf
	  this mode requires that we allocate an extra buffer for the XDR set
*/ 

int ftmpi_nb_decode_buf (struct req_list *rlp)
{
  char * tbuf = NULL;
  int ret;
  /*  int bufid,len; */

  if (rlp->rl_req_dt_op==0) return (0);	/* data already in user space buffer, why are we here again? */

  if (rlp->rl_req_dt_op==1){
    ret = ftmpi_ddt_xdr_decode_dt_to_buffer (rlp->rl_req_rawbuf, rlp->rl_req_usrbuf, NULL, rlp->rl_status.msglength, rlp->rl_req_cnt, rlp->rl_req_dt, rlp->code_info);
#ifdef USE_MALLOC
    _FREE(rlp->rl_req_rawbuf); 
    rlp->rl_req_rawbuf=(char*)NULL;
#else
    free_msg_buf (rlp->rl_req_rawbufid);
	rlp->rl_req_rawbufid = -1;
    rlp->rl_req_rawbuf=(char*)NULL;
#endif
    return (0);
  }

  if (rlp->rl_req_dt_op==2) {
    /* for XDR we need to supply a temp buffer */
#ifdef USE_MALLOC
    tbuf = _MALLOC ( rlp->rl_status.msglength );
    if ( tbuf == NULL ) {
      fprintf(stderr,"ftmpi_decode_buf: could not allocate memory");
      MPI_Abort ( MPI_COMM_WORLD, MPI_ERR_INTERN);
    }
#else
    /* here we use the msgbuf stuff as its quicker for short term alloc/free cycles */
    bufid = get_msg_buf_of_size(rlp->rl_status.msglength,1,0);
    if (bufid==-1) {
      MPI_Abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
      return (MPI_ERR_INTERN);  /* not that I will ever return */
    }
    get_msg_buf_info (bufid, &tbuf, &len);
#endif

    ret = ftmpi_ddt_xdr_decode_dt_to_buffer (rlp->rl_req_rawbuf, rlp->rl_req_usrbuf, tbuf, rlp->rl_status.msglength, rlp->rl_req_cnt, rlp->rl_req_dt, rlp->code_info);

#ifdef USE_MALLOC
    if ( tbuf != NULL )
      _FREE ( tbuf );

    _FREE(rlp->rl_req_rawbuf);
    rlp->rl_req_rawbuf=(char*)NULL;
#else
	if (tbuf != NULL)
    	free_msg_buf (bufid);
	bufid = -1;

	free_msg_buf (rlp->rl_req_rawbufid);
	rlp->rl_req_rawbufid = -1;
	rlp->rl_req_rawbuf = (char*)NULL;
#endif
    return (0);

  } /* if op==2 */
  
  /* should never ever get here, check */
  assert(0);
  return MPI_ERR_INTERN;
}


/*
 This routine takes a message stored in a users buffer
  and if needed encodes it into a temp buffer for sending 
  (if it wasn't already done that way)

  control of what to do is via the rl_req_dt_op field of the req element 
  0 = no buffering/conversion data should be sent from userspace 
  1 = data is in the users buffer and needs to be encoded into the tmp buffer
  2 = data is in the users buffer and needs to be encoded and xdrd into the tmp buf
	this mode requires that we allocate an extra buffer for the XDR set

	If conversion is needed, then the final buffers are offset by hdroffset
	they are also allocated by this larger size as well :)
	GEF Sep03
*/ 

int ftmpi_nb_encode_buf2 (struct req_list *rlp, long hdroffset)
{
  char * tbuf = NULL;
  int ret;
  /*  int bufid,len; */

  if (rlp->rl_req_dt_op==0) return (0);	/* data already in user space buffer, why are we here again? */


  /* its not a non conversion type of operation so.. */
  /* get a rawbuffer for this operation */
  /* We have to use here _MALLOC instead of get_msg_buf_of_size(), since in Wait/Test we are
     executing a _FREE and not a free_msg_buf() on it 
  */

#ifdef USE_MALLOC
  rlp->rl_req_rawbuf = (char*) _MALLOC((rlp->rl_status.msglength)+hdroffset);
  if (!rlp->rl_req_rawbuf) return (MPI_ERR_INTERN);
#else
  rlp->rl_req_rawbufid = get_msg_buf_of_size(rlp->rl_status.msglength+hdroffset,1,0);
  if (rlp->rl_req_rawbufid==-1) return (MPI_ERR_INTERN);
  get_msg_buf_info (rlp->rl_req_rawbufid, &rlp->rl_req_rawbuf, &len);
#endif


  if (rlp->rl_req_dt_op==1) {
    ret = ftmpi_ddt_xdr_encode_dt_to_buffer (  rlp->rl_req_usrbuf, 
		  			(rlp->rl_req_rawbuf)+hdroffset, NULL, 
					rlp->rl_status.msglength, rlp->rl_req_cnt, 
					rlp->rl_req_dt, rlp->code_info);
    return (0);
  }
  
  if (rlp->rl_req_dt_op==2) {
    /* for XDR we need to supply a temp buffer */
#ifdef USE_MALLOC
    tbuf = _MALLOC ( rlp->rl_status.msglength );
    if ( tbuf == NULL ) {
      fprintf(stderr,"ftmpi_encode_buf2: could not allocate memory\n");
      MPI_Abort ( MPI_COMM_WORLD, MPI_ERR_INTERN);
	  return (MPI_ERR_INTERN);
    }
#else
    /* here we use the msgbuf stuff as its quicker for short term alloc/free cycles */
    bufid = get_msg_buf_of_size(rlp->rl_status.msglength,1,0);
    if (bufid==-1) {
      MPI_Abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
      return (MPI_ERR_INTERN);  /* not that I will ever return */
    }
    get_msg_buf_info (bufid, &tbuf, &len);
#endif

    ret = ftmpi_ddt_xdr_encode_dt_to_buffer (rlp->rl_req_usrbuf, (rlp->rl_req_rawbuf)+hdroffset, tbuf,
					     rlp->rl_status.msglength, rlp->rl_req_cnt, rlp->rl_req_dt,
					     rlp->code_info);
#ifdef USE_MALLOC
    if ( tbuf != NULL ){
      _FREE( tbuf );
      tbuf = NULL;
    }
#else
    free_msg_buf (bufid);
	tbuf = NULL;
	bufid = -1;
#endif

    /* calling routine must one day free the rawbuf after sending.. */
    return (0);
  }
  
  /* should never ever get here, check */
  assert(0);
  return MPI_ERR_INTERN;
}


/*
 Other calls to test 
*/

int ftmpi_nb_req_incref (MPI_Request rh)
{
  struct req_list *rlp;
  int dt;

  rlp = reqtable[rh];
  if (!rlp) return (MPI_ERR_REQUEST);

  rlp->rl_req_refcnt++;
  /* each time we increment we increase the count (ok classic comment GEF) */
  dt = rlp->rl_req_dt;
  if (dt>0) ftmpi_ddt_increment_dt_uses (dt);

  return ( 1 ); 
}

int	ftmpi_nb_req_decref (MPI_Request rh)
{
  struct req_list *rlp;
  int dt;

  rlp = reqtable[rh];
  if (!rlp) return (MPI_ERR_REQUEST);

  rlp->rl_req_refcnt--;
  if (rlp->rl_req_refcnt<0) {
    fprintf(stderr,"ftmpi_nb_req_decref on req handle %d negative?\n", rh);
    return (MPI_ERR_INTERN);
  }
  
  /* each time we decrement we decrease the count (ok classic comment GEF) */
  dt = rlp->rl_req_dt;
  if (dt>0) ftmpi_ddt_decrement_dt_uses (dt);
  
  /* if ref count is zero and we want to free the request... we nuke it */
  if (!rlp->rl_req_refcnt) {
    
    if ( (rlp->rl_req_type == REQ_IBSEND ) || (rlp->rl_req_type == REQ_PBSEND))
      ftmpi_buffer_used_free ( rh );
    
    
    /* check whether request is "floating in space" e.g. pers. req. */
    if ( (rlp->rl_link == NULL) && ( rlp->rl_rlink == NULL ))
      _FREE( rlp );
    else {
      /* we can now free the req list element up itself! */
      req_list_free (rlp);
    }
    
    return (1);
  }

  /* ok if we are not freed and we are marked as done, we might assume! that */
  /* we are on the done queue, so detach us... */
  /* used by persistant requests */
  if (rlp->rl_status.flags & FTMPI_REQ_DONE) req_list_detach (rlp);
  
  return (0);
}

/* this is like the ftmpi_nb_req_decref routine but doesn't care about */
/* ref cnts */
int	ftmpi_nb_req_force_free (struct req_list *rlp)
{
  int dt;

  if (!rlp) return (MPI_ERR_REQUEST);

  /* each time we decrement we decrease the count (ok classic comment GEF) */
  dt = rlp->rl_req_dt;
  if (dt>0) ftmpi_ddt_decrement_dt_uses (dt);
  
  if ( (rlp->rl_req_type == REQ_IBSEND ) || (rlp->rl_req_type == REQ_PBSEND))
      ftmpi_buffer_used_free ( rlp->rl_req_handle );

  /* force unlike a refdec has to be able to handle the raw buffer being */
  /* still allocated */

#ifdef USE_MALLOC
  	if (rlp->rl_req_rawbuf) {
	   _FREE (rlp->rl_req_rawbuf);
	   rlp->rl_req_rawbuf = (char*) NULL;
	}
#else
	if ((rlp->rl_req_rawbuf)&&(rlp->rl_req_rawbufid!=-1)) {
		free_msg_buf (rlp->rl_req_rawbufid);
		rlp->rl_req_rawbufid=-1;
		rlp->rl_req_rawbuf = (char*) NULL;
	}
#endif
    
    
  /* check whether request is "floating in space" e.g. pers. req. */
  if ( (rlp->rl_link == NULL) && ( rlp->rl_rlink == NULL ))
      _FREE( rlp );
  else {
      /* we can now free the req list element up itself! */
      req_list_free (rlp);
  }
    
  /* ok if we are not freed and we are marked as done, we might assume! that */
  /* we are on the done queue, so detach us... */
  /* used by persistant requests */
  if (rlp->rl_status.flags & FTMPI_REQ_DONE) req_list_detach (rlp);
  
  return (0);
}


int	ftmpi_nb_msg_force_free (struct msg_list *mlp)
{
  if (!mlp) return (MPI_ERR_INTERN);

#ifdef USE_MALLOC
  	if (mlp->ml_msg_buf) {
	   _FREE (mlp->ml_msg_buf);
	   mlp->ml_msg_buf = (char*) NULL;
	}
#else
	/* oneday will need to check for msgbufid when used */
  	if (mlp->ml_msg_buf) {
	   _FREE (mlp->ml_msg_buf);
	   mlp->ml_msg_buf = (char*) NULL;
	}
#endif
    
  msg_list_free (mlp);
    
  return (0);
}


/*
This routine takes a request and resets the reqtable contents for that request
*/

int	ftmpi_nb_free_req_handle (MPI_Request rh)
{
  if (reqtable[rh]) {
    reqtable[rh]=NULL;
    return(1);
  }
  return(0); /* already NULL... ? */
}


/* 
 These functions handle the unexpected message list for nb recv calls

 i.e. if you post a irecv or pir and the message is already on the 
 unexpected message list then this routine does all its needs too

 NOTE this is like the start of the p2p recv call which does the same
 for matching blocking recv calls 
 (TODO might be to merge the two together)

	GEF
*/ 


/* 
	This function takes the request and if an unexpected message matches it then puts it on the 
	done/completed list
	NOTE the request is expected to NOT be on the recv list already

	this funct is used instead of ftmpi_pp_get_from_msg_list as it does
	one memory copy less... and it also puts onto the done list.
*/

int ftmpi_nb_check_msglist_for_matching_req_and_get (int req)
{
  /* int comm; */
  struct msg_list *mlp, *mlh;  	/* ptrs to message list elements */
  comm_info_t  *cptr;             /* ptr to the comm data structure */
  struct req_list *rlp;			/* ptr to the request */
  
  rlp = reqtable[req];	/* get request element */
  if (!rlp) return (MPI_ERR_REQUEST);
  
  cptr = ftmpi_com_get_ptr (rlp->rl_req_com);
  if (!cptr) return (MPI_ERR_COMM);
  
  mlh = cptr->msg_list_head;  /* get head of the list! */
  
  if (!(cptr->msg_list_count)) /* if no messages queued then no check todo ! */
    return (0);				/* no match */
  
  mlp = msg_list_find_by_header (mlh, rlp->rl_target_rank, rlp->rl_mpi_tag);
  
  if (!mlp) /* no match so no game */
    return (0);				/* no match */
  
  /* ok we have a match so that means that we can complete this nb recv straight away and move the req to the done list */
  
#ifdef NB_VERBOSE
  printf("ftmpi_nb_check_msglist_for_matching_req_and_get: nb recv immediately matched by unexpected message\n");
#endif /* NB_VERBOSE */
  
  /* fill in recv op */
  rlp->rl_status.MPI_SOURCE = mlp->ml_senders_rank;
  rlp->rl_status.MPI_TAG = mlp->ml_mpi_tag;
  rlp->rl_sgid = mlp->ml_sgid;
  rlp->rl_req_com_ver = 0;        /* not implemented ? */
  
  /* Now we have match a header with a request so we are able to compute
   * the ddt stuff. So now the rlp->ls_status.msglength contains the size
   * that the user expect to have (ie we suppose here that the size of the
   * user buffer is enought to hold that).
   */
/*   assert( rlp->code_info == NULL ); */
  rlp->code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(rlp->rl_sgid);
  rlp->rl_req_dt_op = ftmpi_ddt_decode_size_det(rlp->rl_req_dt, rlp->rl_req_cnt,
						&(rlp->rl_status.msglength), rlp->code_info);

  /* Check for truncation. The rl_status.msglength should always contain the 
   * size of usefull data even on a heterogeneous environment.
   */
  if( rlp->rl_status.msglength != mlp->ml_msg_length ) {
    if( rlp->rl_status.msglength > mlp->ml_msg_length ) {
      int dt_size;

      ftmpi_mpi_type_size ( rlp->rl_req_dt, &dt_size );
      /* there is no trunctation of data as we are not copying */
      /* the decode does what it needs using rl_status.msglength */
      rlp->rl_status.msglength = mlp->ml_msg_length;
      rlp->rl_req_cnt = mlp->ml_msg_length/dt_size;
    } else {
      printf( "%s:%d truncation for message %d(%p) [source %d: tag %d] from %d to %ld bytes\n",
	      __FILE__, __LINE__, rlp->rl_req_handle, rlp,
	      mlp->ml_senders_rank, mlp->ml_mpi_tag, 
	      rlp->rl_status.msglength, mlp->ml_msg_length );
      rlp->rl_status.MPI_ERROR = MPI_ERR_TRUNCATE;
    }
  }

  if (rlp->rl_req_dt_op ==0) { /* if no conversion needed */
    /* we must add an offset for the start of the DT? */
    /* blocking calls get this done for them in the p2p calls */
	 /* only do the memcopy if non zero length message :) */
	 if (rlp->rl_status.msglength) 
    	memcpy( ((char*)rlp->rl_req_usrbuf + ftmpi_ddt_get_first_offset(rlp->rl_req_dt)),
	    (void*)mlp->ml_msg_buf, rlp->rl_status.msglength);
  } else {	
    /* copy data from the msg list to the req_raw data ptr */
    /* this allows us to decode the requests data into user space */
#ifdef USE_MALLOC
    rlp->rl_req_rawbuf = mlp->ml_msg_buf;
	rlp->rl_req_rawbufid = -1; /* as no msg buf for mlps yet? */
#else
    rlp->rl_req_rawbuf = mlp->ml_msg_buf;
	rlp->rl_req_rawbufid = -1; /* as no msg buf for mlps yet? */
	/* there should be a msg buf id here so assert */
	MPI_Abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
	assert (0);
#endif
    
    /* decode the data into the users address space if possible */
    ftmpi_nb_decode_buf (rlp);
  }
  
  /* now we can free the msg_list */
  /* note the decode freed the rlps raw data depending on the encoding */
  
  if (rlp->rl_req_dt_op ==0) { /* if no conversion needed */
	/* we allocated the ml_msg_buf so free it */
	 /* only it really has been allocated */
	 if (mlp->ml_msg_buf) _FREE(mlp->ml_msg_buf);
	/* mark ml_msg_buf freed */
    mlp->ml_msg_buf = (void*)NULL; 
  } else {
    /* free message buffer in message queue */
	 /* cannot be done as it was also refd by the req_raw_buf and that was */
	 /* freed by the decode_buf () */
	/* mark ml_msg_buf freed */
    mlp->ml_msg_buf = (void*)NULL; 
  }
  
  /* free entry from message list (recv queue) */
  msg_list_free (mlp);
  
  /* update message count in recv queue for this communicator */
  (cptr->msg_list_count)--;
  
  /* now we move the request straight to the done list :) */
  
  req_list_add_2tail (req_list_done_head, rlp);   /* add to done list */
  
  /* update done counters */
  req_list_done_count++;
  
  /* and finally one last important update */
  rlp->rl_status.flags |= FTMPI_REQ_DONE;
  
  return (1);	/* match, set,  game */
}


/* 
 * this routine ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink
 * is a to be used with ftmpi_nb_complete_msg_to_req
 *
 */

int ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink (int req)
{
  /* int comm; */
  struct msg_list *mlp, *mlh;  	/* ptrs to message list elements */
  struct req_list *rlp;			/* ptr to the request */
  
  if (!incomplete_msg_list_count) 
	 /* if no messages queued then no check todo ! */
    return (0);				/* no match */

  rlp = reqtable[req];	/* get request element */
  if (!rlp) return (MPI_ERR_REQUEST);
  
  mlh = incomplete_msg_list_head;  /* get head of the list! */
  
  mlp = msg_list_find_by_header (mlh, rlp->rl_target_rank, rlp->rl_mpi_tag);
  
  if (!mlp) /* no match so no game */
    return (0);				/* no match */
  
  /* ok we have a match so that means that we can complete this nb recv straight away and move the req to the done list */


#ifdef NB_VERBOSE
  printf("ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink: nb recv  matched by incomplete unexpected message\n");
#endif /* NB_VERBOSE */
 
  mlp->ml_matched_nb_rlp = rlp;	/* remember we have matched to someone! */

  /* to stop any confusion we then remove the mlp and rlps from */
  /* any lists to prevent any further accidental matching */
  /* actually I updated this so that req gets put on the inprogress list */
  /* a ref to the mlp still exists in the conn layer */
  /* which also points to the rlp........ I hope GEF */

  ftmpi_nb_move_recv_2_inprogress (rlp);

  msg_list_detach (mlp);
  incomplete_msg_list_count--;

  return (0);	
}

/*
 * this routine takes a complete msg (not on any queue)
 * and transfers the contents to an already matched NB request 
 * 
 * This is done for the case involving a nb req being set inbetween
 * the recving of a long unexpecetd matching message
 *
 * As this was discovered when posting the nb recv it is not on the
 * recv q anymore but is now on the inprogress list 
 * this req is then moved to the doneQ
 *
 * GEF Oct03 I wish it was snowing outside
 */

int ftmpi_nb_complete_msg_to_req (struct req_list *rlp, struct msg_list *mlp)
{
    /* ok we have a match so that means that we can complete this nb recv straight away and move the req to the done list */
    
#ifdef NB_VERBOSE
    printf("ftmpi_nb_complete_msg_to_req: nb recv post matched by unexpected message\n");
#endif /* NB_VERBOSE */
    
    /* fill in recv op */
    rlp->rl_status.MPI_SOURCE = mlp->ml_senders_rank;
    rlp->rl_status.MPI_TAG = mlp->ml_mpi_tag;
    rlp->rl_sgid = mlp->ml_sgid;
    rlp->rl_req_com_ver = 0;        /* not implemented ? */
    
    /* Now we have match a header with a request so we are able to compute
     * the ddt stuff. So now the rlp->ls_status.msglength contains the size
     * that the user expect to have (ie we suppose here that the size of the
     * user buffer is enought to hold that).
     */
    /*   assert( rlp->code_info == NULL ); */
    rlp->code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(rlp->rl_sgid);
    rlp->rl_req_dt_op = ftmpi_ddt_decode_size_det(rlp->rl_req_dt, rlp->rl_req_cnt,
						  &(rlp->rl_status.msglength), rlp->code_info);
    
    /* Check for truncation. The rl_status.msglength should always contain the 
     * size of usefull data even on a heterogeneous environment.
     */
    if( rlp->rl_status.msglength != mlp->ml_msg_length ) {
	if( rlp->rl_status.msglength > mlp->ml_msg_length ) {
	    /* there is no trunctation of data as we are not copying */
	    /* the decode does what it needs using rl_status.msglength */
	    rlp->rl_status.msglength = mlp->ml_msg_length;
	} else {
	    printf( "%s:%d truncation for message %d(%p) [source %d: tag %d] from %d to %ld bytes\n",
		    __FILE__, __LINE__, rlp->rl_req_handle, rlp,
		    mlp->ml_senders_rank, mlp->ml_mpi_tag, 
		    rlp->rl_status.msglength, mlp->ml_msg_length );
	    rlp->rl_status.MPI_ERROR = MPI_ERR_TRUNCATE;
	}
    }
    
    if (rlp->rl_req_dt_op ==0) { /* if no conversion needed */
	/* we must add an offset for the start of the DT? */
	/* blocking calls get this done for them in the p2p calls */
	if (rlp->rl_status.msglength) 
	    memcpy( ((char*)rlp->rl_req_usrbuf + ftmpi_ddt_get_first_offset(rlp->rl_req_dt)),
		    (void*)mlp->ml_msg_buf, rlp->rl_status.msglength);
    } else {	
	/* copy data from the msg list to the req_raw data ptr */
	/* this allows us to decode the requests data into user space */
#ifdef USE_MALLOC
	rlp->rl_req_rawbuf = mlp->ml_msg_buf;
	rlp->rl_req_rawbufid = -1; /* as no msg buf for mlps yet */
#else
    rlp->rl_req_rawbuf = mlp->ml_msg_buf;
	rlp->rl_req_rawbufid = -1; /* as no msg buf for mlps yet? */
	/* there should be a msg buf id here so assert */
	MPI_Abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
	assert (0);
#endif
	
	/* decode the data into the users address space if possible */
	ftmpi_nb_decode_buf (rlp);
    }
    
    /* now we can free the msg_list */
    /* note the decode freed the rlps raw data depending on the encoding */
    
    if (rlp->rl_req_dt_op ==0) { /* if conversion needed */
	/* we allocated the ml_msg_buf, so free it */
	/* only it really has been allocated */
	if (mlp->ml_msg_buf) _FREE(mlp->ml_msg_buf);
	mlp->ml_msg_buf = (void*)NULL; 
    } else {
	/* we do not free the ml_msg_buf as the memory was refd by req_raw_buf */
	/* and that was freed in the decode_buf routine */
	/* so just make it free */
	/* free message buffer in message queue */
	mlp->ml_msg_buf = (void*)NULL; 
    }
    
    /* free entry (its floating in space anyway :) */
    msg_list_free_unattached (mlp);
    
    /* update message count in recv queue for this communicator */
    /*   cptr = ftmpi_com_get_ptr (rlp->rl_req_com); */
    /*   if (!cptr) return (MPI_ERR_COMM); */
    /*   (cptr->msg_list_count)--; */
    /* not on a list so we cannot do this */
    
    /* now we can the request to the done list :) */
    ftmpi_nb_move_recv_inprogress_2_done (rlp);
    
    return (1);	/* match, set,  game */
}


/*
 These are send operations that take a req or rlp and force that req to
 be completed
 */
int	ftmpi_nb_req_do_send (MPI_Request request)
{
   int done=0;

   done = ftmpi_nb_req_isdone (request);
   while (!done) {
   	ftmpi_nb_progress (1);
	done = ftmpi_nb_req_isdone (request);
   }
   return (0);
}


/*
 count and schedule routines

 These routines make up counts and lists of things to be done to
 complete nb operations.
 This is as close to building a schedule as we will get for now
 for example, if we need to wait on a send to GID for a request REQ
 we need to know how many other sends on that comm are also pending
 for the GID in front of the REQ etc..
 We also need to know how many sends and recvs there are in a 
 REQ list when handling a waitall on a list of REQs
 There may be 2 sends and two recvs for that WAITALL but that might
 equal 20 operations to preserve message ordering.

 This should change when we implement a proper ticket CTS/RTS FLCTRL proto later

 GEF Feb03
*/

/* how many sends do we need to do to complete this send Request */

int ftmpi_nb_total_sends (MPI_Request req)
{
  struct req_list *rlp;			/* ptr to the request */
  int cnt=0;
  
  rlp = reqtable[req];	/* get request element */
  if (!rlp) return (MPI_ERR_REQUEST);
  
  /* if its already marked done don't do it again! */
  if (rlp->rl_status.flags & FTMPI_REQ_DONE) return (0);
  
  /* also check cancelled */
  if (rlp->rl_status.flags & FTMPI_REQ_CANCELLED) return (MPI_ERR_INTERN);
  
  if (!req_list_send_count) return (0);
  
  rlp = req_list_head (req_list_send_head);
  if (!rlp) return (0);
  
  cnt = req_list_howmany_send_to_gid (req_list_send_head, rlp->rl_rgid, req);
  return (cnt);
}



/* how many recvs do we have to do 'potentially' before this Req can complete */
/* its not exact as we don't know about an ANYSOURCE/ANYTAG effects they may count or */
/* may not! :) */

int ftmpi_nb_total_recvs (MPI_Request req)
{
  struct req_list *rlp;			/* ptr to the request */
  int cnt=0;
  
  rlp = reqtable[req];	/* get request element */
  if (!rlp) return (MPI_ERR_REQUEST);
  
  /* if its already marked done don't do it again! */
  if (rlp->rl_status.flags & FTMPI_REQ_DONE) return (0);
  
  /* also check cancelled */
  if (rlp->rl_status.flags & FTMPI_REQ_CANCELLED) return (MPI_ERR_INTERN);
  
  if (!req_list_recv_count) return (0);
  
  rlp = req_list_head (req_list_recv_head);
  if (!rlp) return (0);
  
  if (!req_list_recv_count) return (0);
  
  cnt = req_list_howmany_recv_from_gid (req_list_recv_head, rlp->rl_sgid, req);	
  /* note this routine has to handle ANYSOURCE! */
  return (cnt);
}



/*
 check to send routines

 These poll a connection to see if a write would block...

 */

/* see XCTS / XRTS proto message in conn instead */

/* 
 do a recv routine
 */

/* currently handled by poll ops in p2p.c */

/* but most of these routines use GID lists etc */
/* the following is a short hand routine to get the RECV ids from a request */

/* this routine recvs all requests upto the request given */
/* not explicitly the hdr matching of the p2p routine does that for us */
/* it must handle ANY Source as awell */
/* if a message comes in that matches the current request without */
/* matching all before, this routine will return */

int ftmpi_nb_recv_upto_req (MPI_Request request)
{
    int rc=0;
    int done=0;
    struct req_list *rlp;
    
    rlp = reqtable[request];    /* get request element */
    if (!rlp) return (MPI_ERR_REQUEST);
    
    /* if its already marked done don't do it again! */
    if (rlp->rl_status.flags & FTMPI_REQ_DONE) return (0);
    
    /* also check cancelled */
    if (rlp->rl_status.flags & FTMPI_REQ_CANCELLED) return (MPI_ERR_REQUEST);
    
    while (!done) {
	
	rc = ftmpi_nb_req_isdone (request);
#ifdef VERBOSES2
	printf ("ftmpi_nb_recv_upto_req: req %d rlp 0x%x isdone %x\n", request, rlp, rc);
#endif
	if( rc != 0 ) return rc;
	
	rc = ftmpi_nb_progress (1);
	if (rc<0) return (rc);
    }
    
    return (rc);
}



/*
 *  This call is for handling Isends to oneself
 *  The request is expeceted to already be on the send queue
 *  The args are passing in to reduce testing time (Isend has them already)
 *
 *  The order of ops are
 *
 *  (1) check for a self send, if false return 0
 *  (2) check for a matching nb recv, if found do a ddtcopy etc and 
 *  	move both requests to the DONE queues
 *  (3)	if not a nb match then pack data into a small buffer and put this
 *  	on the unexpected message queue. Then put the send req on the done-q.
 *
 */

int ftmpi_nb_send2_self_test_and_do (MPI_Request req)
{
  int tgid, rc, ret, mygid;
  struct req_list *rrlp;	/* recv request list pointer */
  struct req_list *srlp;	/* send request list pointer */
  comm_info_t  *cptr;		/* comm list pointer used to find head of mqueue */
  struct msg_list *mlp, *mlh; /* message list elements & heads */
  int msize; 				/* message size if sending locally */
  char *buf; 
  int count, datatype, target, tag; 
  MPI_Comm comm;
  FTMPI_DDT_CODE_INFO * code_info = NULL;

  /* check if it's a send to self */
  srlp = ftmpi_nb_get_reqlist_ptr_by_handle(req);
  comm = srlp->rl_req_com;
  target = srlp->rl_target_rank;
  mygid = ftmpi_sys_get_gid();

  tgid = ftmpi_com_gid (comm, target);
  if (tgid<0) return (tgid);
  
  if (tgid!=mygid) return (0);	/* not a send to self, return */
  
  /* get the send ops request handle as we will need it to mark done */
  /* and get contact info about it */
  /* this is done here so that persistant calls get use this routine by */
  /* request handle only */
  /* original version has all the args passed in by ref */
  /* GEF */
  buf = srlp->rl_req_usrbuf;
  count = srlp->rl_req_cnt;
  datatype = srlp->rl_req_dt;
  tag = srlp->rl_mpi_tag;
  
  code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(mygid);
  rc = ftmpi_ddt_encode_size_det(datatype,count,&msize,code_info);

  /* figure out the size of the data to be moved */
  /* ftmpi_ddt_write_read_size_det(datatype, count, &msize, srlp->rl_req_dt_op);	 */
  /* encoding was 0 for a hopefully very local operation */
  
  /* ok now we need to see if we match a NB operation */
  
  cptr=NULL;   /* reset ptr each time */
  cptr = ftmpi_com_get_ptr (comm);
  if (!cptr) return (MPI_ERR_COMM);
  
  if (req_list_recv_count) {
    /*    printf("nb recvs exist so check for a nb match\n"); */
    rrlp = req_list_find_req_matching_recvhdr (req_list_recv_head,
					       target, tag, comm);
    if (rrlp) { /* if a match on a nonblocking request */
      /* 		printf("Isend to self found a matching nb recv\n"); */
      
      /* transfer the data from one system to the other */
      ret = ftmpi_ddt_copy_ddt_to_ddt ( 
				       buf, datatype, count, 
				       rrlp->rl_req_usrbuf, rrlp->rl_req_dt, 
				       rrlp->rl_req_cnt);
      if (ret<0) return (ret); /* on problem let lib layer handle error */
      
      /* fill in the receive request */
      rrlp->rl_status.MPI_SOURCE = target; 
      rrlp->rl_status.MPI_TAG = tag;
      rrlp->rl_sgid = mygid;
      rrlp->rl_req_com_ver = 0;
      
      /* rl_status.msglength is set here as the two part _set_ and _add_ does not */
      /* set it  */
      /* this is needed so a status look up can check it */
      rrlp->rl_status.msglength = msize;
      
      
      /* status is set during a wait/test op */
      
      /* so, just set the request to DONE and get outa here */
      
      ftmpi_nb_move_recv_2_done (rrlp);
      
      /* move the send to the done list as well */
      ftmpi_nb_move_send_2_done (srlp);
      
      return (1); /* a nonzero positive RC */
      
    } /* nb matched */
    /* 	else printf("Isend to self no matching nb recv found\n"); */
  } /* if any nb recvs exist */
  
  
  /* ok we did not match a NB operation, thus do a unexpected message */
  
  /* note this is very much like ftmpi_pp_put_into_msg_list() */
  
  mlh = cptr->msg_list_head;
  
  /* now get a msg_list entry point i.e. a new place to put the message */
  
  mlp = msg_list_new(mlh); /* get me a newslot */
  
  mlp->ml_senders_rank = target;
  mlp->ml_mpi_tag = tag;
  mlp->ml_msg_type = 0;
  mlp->ml_prot_type = 0;      	/* not implemented ? */
  mlp->ml_msg_id = 0;
  mlp->ml_target_rank = target;
  mlp->ml_sgid = mygid;
  mlp->ml_rgid = mygid;
  mlp->ml_msg_length = msize;   	/* we recheck this in a sec.. */
  mlp->ml_msg_com_ver = 0;     	/* not implemented ? */

  mlp->ml_matched_nb_rlp = NULL;	/* not yet :) */
  
  if (srlp->rl_req_dt_op==0) {     /* no conv needed */
    /* get a temp buffer for the data */
    if ( msize ) {
      mlp->ml_msg_buf = (char*)_MALLOC(msize);
      if (!(mlp->ml_msg_buf)) {
	fprintf(stderr,"ftmpi_nb:ftmpi_nb_send2_self_test_and_do: Fatal error.\n"
		"Unable to allocate memory for out of order message on the\n"
		"current recv message queue.");
    
	/* free entry from message list (recv queue) */
	msg_list_free (mlp);
	return (MPI_ERR_INTERN);
      } /* if cannot allocate internal unexpected message buffer ! */
  
      memcpy( mlp->ml_msg_buf, 
	      ((char*)srlp->rl_req_usrbuf+ftmpi_ddt_get_first_offset(srlp->rl_req_dt)),
	      srlp->rl_status.msglength );
    }
    else
      mlp->ml_msg_buf = NULL;
  } else {
    /* encode data directly to the unexpected ml_buf */
    ftmpi_nb_encode_buf2 (srlp, 0);
#ifdef USE_MALLOC
    mlp->ml_msg_buf = srlp->rl_req_rawbuf;
    srlp->rl_req_rawbuf = NULL; /* or else we might free the ml_buf */
    /* during a test or wait !! */
#else
    mlp->ml_msg_buf = srlp->rl_req_rawbuf;
    srlp->rl_req_rawbuf = NULL; /* or else we might free the ml_buf */
    /* during a test or wait !! */
	if (srlp->rl_req_rawbufid!=-1) {
	   fprintf(stderr,"Warning send2self throwing msgbuf id %d to the wind!\n", srlp->rl_req_rawbufid);
	}
#endif
  }
  
  /* at this point the unexpected message is complete */
  /* so increment the msg list count! */
  (cptr->msg_list_count)++;
  
  
  /* so now move the Isend to the done list and get outa here */
  ftmpi_nb_move_send_2_done (srlp);
  
  return (1); /* a nonzero positive RC */
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_nb_clearmsglists_for_a_com ( MPI_Comm comm )
{
  comm_info_t *cptr;
  struct req_list *tmp, *rlp;
  struct msg_list *mtmp, *mhead, *mrlp;
  
  /* Get comm pointer */
  cptr = ftmpi_com_get_ptr ( comm );

  if ( cptr->msg_list_count > 0 ) {
    /* Get head of msg list */
    mhead = cptr->msg_list_head; 
    for (mrlp = mhead->ml_link; mrlp != mhead; mrlp = mtmp) {
      mtmp = mrlp->ml_link;
      msg_list_free ( mrlp );
    }
    cptr->msg_list_count = 0;
  }

  if ( req_list_send_count > 0 ) {
    for (rlp = req_list_send_head->rl_link; rlp != req_list_send_head; rlp = tmp) {
      tmp = rlp->rl_link;
      if (rlp->rl_req_com == comm) {
      	ftmpi_nb_free_req_handle ( rlp->rl_req_handle);
      	req_list_free ( rlp );
    	req_list_send_count--;
      }
    }
  }

  if ( req_list_recv_count > 0 ) {
    /* get head of done queue */
    for (rlp = req_list_recv_head->rl_link; rlp != req_list_recv_head; rlp = tmp) {
      tmp = rlp->rl_link;
	  if (rlp->rl_req_com == comm) {
      	ftmpi_nb_free_req_handle ( rlp->rl_req_handle);
      	req_list_free ( rlp );
    	req_list_recv_count--;
	  }
    }

  }

  if ( req_list_done_count > 0 ) {
    /* Get head of done queue */
    for (rlp = req_list_done_head->rl_link; rlp != req_list_done_head; rlp = tmp) {
      tmp = rlp->rl_link;
      if (rlp->rl_req_com == comm) {
      	ftmpi_nb_free_req_handle ( rlp->rl_req_handle);
      	req_list_free ( rlp );
    	req_list_done_count--;
      }
    }

  }

  return ( MPI_SUCCESS );
}


/* ********************************************************************** */
/* ********************************************************************** */

/*  The following routine are device dependant. Currently this is SNIPE2  */

/* ********************************************************************** */
/* ********************************************************************** */



int ftmpi_nb_post_send (MPI_Request req)
{
    struct req_list * rlp;
    msg_hdr_t*  hdrptr;
    int ftmpiconn;
    int snipe2chan;
    int rc;
    /*    int len; */
    char *p, *q;
    int i;
    int snipe2reqid;
    snipe2_msg_list_t *snipe2reqptr;
    int connected=0;
    int sendallowed=0;
    
#ifdef VERBOSES2
    printf("Posting request %d to SNIPE2 send queue\n", req);
#endif
    rlp = ftmpi_nb_get_reqlist_ptr_by_handle (req);
    if (!rlp) {
	return (MPI_ERR_INTERN);
    }
    
    /* get the target TID and look up the low level channel for this destination */
    ftmpiconn = find_conn_entry_by_gid (rlp->rl_rgid);
    
    if (ftmpiconn<0) { /* problem */
	return (ftmpiconn);
    }
    
    /* now check to see if the system is connected */
    /* and sending of user level data is allowed */
    /* if not connected in any way make a connection */
    /* if we are connected and not sendallowed, let callbacks handle the situation */
    conn_status (ftmpiconn, &connected, &sendallowed);

    if (!connected) {
	   /* not connected make sure all is reset */
	   rlp->rl_snipe2_conn = 0;
	   rlp->rl_snipe2_rc   = 0;

#ifdef VERBOSES2
	    printf("ftmpi_nb_post_send: no connection to 0x%x yet. Calling mkconn2\n", rlp->rl_rgid);
#endif 

	    rc = conn_mkconn2 (rlp->rl_rgid, 1);	/* 1 = allow snipe2 to handle it from now on */

		/* as rc ==1 if we connected ok... we don't pass this back unless its
		 * an error */
		if (rc<0) return (rc);
		else return (0);
    }

    if (!sendallowed) {
#ifdef VERBOSES2
	    printf("ftmpi_nb_post_send: connection to 0x%x not ready for userdata, delaying\n", rlp->rl_rgid);
#endif 
	    return (MPI_SUCCESS);		/* we cannot post yet, delay till later */
    }
    
    rc = conn_snipe2_getchan (ftmpiconn, &snipe2chan);
    if (rc<0) return (rc);
    
    
    /* here we add the high level request to the low level send queue */
    rlp->rl_snipe2_conn = snipe2chan;	/* cache connection info in req */
    rlp->rl_snipe2_rc   = 0;			/* reset snipe2 rc for req */
    
    /* */
    
    /* before we do anything, we have to convert the data as needed */
    ftmpi_nb_encode_buf2 (rlp, msg_hdr_size);
    
    
    /* we have 3 modes to consider here */
    
    /* first non conversion (op type 0) short message */
    /* these get compacted into a combined long header (hdr+payload) */
    /* and posted as a single low level send */
    
    /* second non conversion (op type 0) long message */
    /* these get posted as TWO separate low level sends */
    
    /* third, conversion needed but we can combine both high level header */
    /* and the converted data into a single buffer for a single */
    /* low level send */
    /* i.e. converted data goes into -> newbuf+msg_hdr_size... */
    
    
    /* 1st case */
    if(rlp->rl_req_dt_op==0){
	
	if ((rlp->rl_status.msglength+msg_hdr_size)<=SINGLEWRITEBUFFERSIZE) {	
	   														/* case 1 */
	    
	    
#ifdef VERBOSES2
	    printf("Req %d combined hdr&data send\n", req);
#endif
	    
	    /* header and data combined */
	    
	    /* we need to use the raw buf for the combined hdr & data */
#ifdef USE_MALLOC
	    rlp->rl_req_rawbuf = _MALLOC ( rlp->rl_status.msglength + msg_hdr_size);
	    if ( rlp->rl_req_rawbuf == NULL ) {
		fprintf(stderr, "ftmpi_nb_post_send: could not allocate memory\n");
		MPI_Abort ( MPI_COMM_WORLD, MPI_ERR_INTERN);
	    }
		rlp->rl_req_rawbufid = -1; /* be safe */
#else
	    rlp->rl_req_rawbufid = get_msg_buf_of_size(
						       (rlp->rl_status.msglength)+msg_hdr_size, 1, 0);
	    
	    if (rlp->rl_req_rawbufid==-1) {
		ftmpi_mpi_abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
		return (MPI_ERR_INTERN);  /* not that I will ever return */
	    }
	    get_msg_buf_info (rlp->rl_req_rawbufid, &(rlp->rl_req_rawbuf), &len);
#endif
	    
	    hdrptr = (msg_hdr_t*) (rlp->rl_req_rawbuf);	/* get ptr to msghdr */
	    
	    /* the header can now be built */
	    make_hdr (*hdrptr, XHDR, my_gid, rlp->rl_rgid, rlp->rl_status.msglength, 
		      0, rlp->rl_req_com, rlp->rl_posters_rank, 
		      rlp->rl_target_rank, rlp->rl_mpi_tag);
	    
#ifdef VERBOSES2
	    conn_hexdump ((char*)hdrptr, msg_hdr_size);
	    debug_dump_header(hdrptr);
#endif

		if (rlp->rl_status.msglength) { /* if above zero bytes! */
	    
	    /* user data can now be copied into the second half of the buffer */
	    /* note the special offsets */
	    	p = (rlp->rl_req_rawbuf)+msg_hdr_size;

	    	q = (char*)rlp->rl_req_usrbuf
		   		+ftmpi_ddt_get_first_offset(rlp->rl_req_dt);

		   	if (rlp->rl_status.msglength<USEMEMCOPYLIMIT) {
	    		for(i=0;i<(rlp->rl_status.msglength);i++) *p++ = *q++;	
			} /* use loop to copy */
		   else 
			  memcpy (p,q,rlp->rl_status.msglength);
		} /* if above zero bytes to copy */
	    
	    
	    /* network byte order the header before posting */
	    trans_hdr_to_network ((*hdrptr)); /* translate it */
	    
#ifdef VERBOSES2
	    conn_hexdump ((char*)hdrptr, msg_hdr_size);
#endif
	    
	    /* combined data in the rawbuffer now */	
	    /* send both togther */
	    rlp->rl_snipe2_reqid = snipe2_post_send_msg (
							 snipe2chan, (char*)rlp->rl_req_rawbuf, 
							 msg_hdr_size+rlp->rl_status.msglength,
							 (intfuncptr)(ftmpi_nb_snipe2_sentdata_callback),
							 (long)rlp, 1, &(rlp->rl_snipe2_reqptr)
							 );
	    
	}
	
	else { /* 2nd case */
#ifdef VERBOSES2
	    printf("Req %d separate hdr and data sends\n", req);
#endif
	    
	    /* we need to use the raw buf for the hdr only */
#ifdef USE_MALLOC
	    rlp->rl_req_rawbuf = _MALLOC ( msg_hdr_size );
	    if ( rlp->rl_req_rawbuf == NULL ) {
		fprintf(stderr, "ftmpi_nb_post_send: could not allocate memory\n");
		MPI_Abort ( MPI_COMM_WORLD, MPI_ERR_INTERN);
	    }
		rlp->rl_req_rawbufid = -1;
#else
	    rlp->rl_req_rawbufid = get_msg_buf_of_size(
						       msg_hdr_size, 1, 0);
	    
	    if (rlp->rl_req_rawbufid==-1) {
		MPI_Abort (MPI_COMM_WORLD, MPI_ERR_INTERN);
		return (MPI_ERR_INTERN);  /* not that I will ever return */
	    }
	    get_msg_buf_info (rlp->rl_req_rawbufid, &(rlp->rl_req_rawbuf), &len);
#endif
	    hdrptr = (msg_hdr_t*) (rlp->rl_req_rawbuf);	/* get ptr to msghdr */
	    
	    /* the header can now be built */
	    make_hdr (*hdrptr, XHDR, my_gid, rlp->rl_rgid, rlp->rl_status.msglength, 
		      0, rlp->rl_req_com, rlp->rl_posters_rank, 
		      rlp->rl_target_rank, rlp->rl_mpi_tag);
	    
#ifdef VERBOSES2
	    debug_dump_header(hdrptr);
#endif
	    
	    /* network byte order the header before posting */
	    trans_hdr_to_network ((*hdrptr)); /* translate it */
	    
	    
	    /* the send can now be posted in two parts */
	    /* note the special offsets */
	    
	    /* first the header */
	    snipe2reqid = snipe2_post_send_msg (
						snipe2chan, (char*)rlp->rl_req_rawbuf, msg_hdr_size,
						(intfuncptr)(ftmpi_nb_snipe2_sentjustdatahdr_callback),
						(long)rlp, 1, &snipe2reqptr
						);
	    
	    /* second the real user data */
	    rlp->rl_snipe2_reqid = snipe2_post_send_msg ( snipe2chan, 
							  (char*)rlp->rl_req_usrbuf+ftmpi_ddt_get_first_offset(rlp->rl_req_dt), 
							  rlp->rl_status.msglength,
							  (intfuncptr)(ftmpi_nb_snipe2_sentdata_callback),
							  (long)rlp, 1, &(rlp->rl_snipe2_reqptr)
							  );
	    
	    
	}
	
    } /* cases 1 & 2 */
    
    /* 3rd case */
    
    if(rlp->rl_req_dt_op!=0) {
	/* the data is already well converted with a hdrsized gap at the start */
#ifdef VERBOSES2
	printf("Req %d combined converted data op:%1d hdr and data send\n", 
	       req, rlp->rl_req_dt_op );
#endif
	
	hdrptr = (msg_hdr_t*) (rlp->rl_req_rawbuf);	/* get ptr to msghdr */
	
	/* fill in the header */
	make_hdr (*hdrptr, XHDR, my_gid, rlp->rl_rgid, rlp->rl_status.msglength, 
		  0, rlp->rl_req_com, rlp->rl_posters_rank, 
		  rlp->rl_target_rank, rlp->rl_mpi_tag);
	
#ifdef VERBOSES2
	debug_dump_header(hdrptr);
#endif
	
	/* change the header into network byte order before posting */
	/* something George should do at a lower level */
	/* network byte order the header before posting */
	trans_hdr_to_network ((*hdrptr)); /* translate it */
	
	
	/* all packed into rawbuf, post the send at last */
	rlp->rl_snipe2_reqid = snipe2_post_send_msg (
						     snipe2chan, (char*)rlp->rl_req_rawbuf, 
						     (rlp->rl_status.msglength)+msg_hdr_size,
						     (intfuncptr)(ftmpi_nb_snipe2_sentdata_callback),
						     (long)rlp, 1, &(rlp->rl_snipe2_reqptr)
						     );
	
    } /* third case */
    
    /* note callback returns the rlp as the userval! */
    /* also '1' means send data is a autodelete on success function */
    
    return (MPI_SUCCESS);
}

/* George will change this here probably */
int ftmpi_nb_post_recv (MPI_Request req)
{
    struct req_list * rlp;
    
    printf("Posting request %d to SNIPE2 recv queue\n", req);
    rlp = ftmpi_nb_get_reqlist_ptr_by_handle (req);
    if (!rlp) {
	BANG;
	return (MPI_ERR_INTERN);
    }
    
    return (MPI_SUCCESS);
}





/* ********************************************************************** */
/* ********************************************************************** */


/* SNIPE 2 low level progress loop */

int	ftmpi_nb_progress (int testorwait)
{
    int rc;
    int done=0;
    int progresscount=0;
    int progresstype=0;
    void *v1, *v2, *v3;	/* progress callback return values */
    /* could be anything, int, long or pointer */
	int newcon, newchan, newtgid; /* new connection & channel info cached to avoid conn calls */
    
    while (!done) {
	/* reset previous events */
	/* these events are set by callbacks in conn.c */
	/* no due to immediate operations these can be set recursively.. so if they get incremenetde */
	/* decrement them one by one :) */
	/* this means routines handling them .. grr */
	/* must be a linked list of todo items... */
	
	progresscount = fifo_queue_count (progress_queue);

	/* if progress count is NOT ZERO then a callback or init routine */
	/* may have caused us to complete an event and then calling */
	/* snipe2 with a SNIPE2SOME may block!!!! */
	/* I think George spotted this, maybe. GEF */

	if ((testorwait==0)||(progresscount)||(ft_mpi_conn_any_error()))
	    rc = snipe2_progress (SNIPE2ALL, SNIPE2TEST); /* not blocking */
	else
	    rc = snipe2_progress (SNIPE2ALL, SNIPE2SOME); /* blocking */

	
	
	progresscount = fifo_queue_count (progress_queue);

	/* debug recovery */
	if (ft_mpi_conn_any_error()) {
/* 	   printf("progress count after progress on error %d\n",  */
/* 			 progresscount); */
/* 	   sleep (1); */
	}
	
	while (progresscount>0) {
	    
	    progresstype = fifo_queue_get (progress_queue, &v1, &v2, &v3);
	    
	    if (progresstype<0) {
			fprintf(stderr,"ftmpi_nb_progress:fifo_queue_get return %d\n",
				progresstype);
			return (MPI_ERR_INTERN);
	    }
	    
	    switch (progresstype) {
		
	    case (PROGRESS_NEWCONN):
			/* was there any new connection attempts? */
		   newcon = 0;
		   newtgid=0;
			rc = conn_handle_con_attempts (1, 1, &newcon, &newtgid);
			/* the first 1= one attempt to compelete. The second 1 means enable low level device (SNIPE2) */
			if (rc==1) { /* we had a successful connection */
			   conn_snipe2_getchan (newcon, &newchan);
#ifdef VERBOSES2
			   printf("ftmpi_nb_progress: NEWCONN activated conn %d chan %d to 0x%x. Posting pending sends if any\n", 
					 	newcon, newchan, newtgid);
#endif /* VERBOSES2 */
			   ftmpi_nb_post_allpending (newtgid, newchan);
			}
			if (rc==0) { /* non event or bad connection attempt or ALT event */
			}
			if (rc==-1) { /* something actually bad happened */
			}
		break;
		
	    case (PROGRESS_CONNERROR):
		break;
		
	    case (PROGRESS_CONNCLOSE):
		break;
		
	    case (PROGRESS_SEND_COMPLETED):
		break;
		
	    case (PROGRESS_RECV_COMPLETED):
			/* we finished receiving something */
			rc = ftmpi_nb_handle_recvd_data ((int)v2);
		break;
		
	    case (PROGRESS_TRUNC_COMPLETED):
		break;
		
	    case (PROGRESS_HDR_RECVD):
			/* we have recved a header (could be xon/xoff/xalt or xhdr) */
			/* this routine does all the real work */
			/* it needs to be passed the connection which is v2 */
			/* this also needs to be after recv_completed for when do */
			/* more than one recv per progress call in the low level device */
			rc = ftmpi_nb_handle_recvd_header ((int)v2);
		break;
		
	    case (PROGRESS_OTHER):
		break;
		
	    case (PROGRESS_EVENT):
		break;
		
	    case (PROGRESS_READYCONN):
#ifdef VERBOSES2
			printf("READYCONN: on chan %d from gid 0x%x on connection %d\n", v1, v2, v3);
#endif
			/* v2 is the target id */
			ftmpi_nb_post_allpending ((int)v2, (int)v1);
		break;

	    default: 
			fprintf(stderr,"ftmpi_nb_progress:invalid progress type %d\n",
			progresstype);
			return (MPI_ERR_INTERN);
		break;
		
	    } /* switch */
	    
	    
	    /* after switch recheck events to process */
	    progresscount = fifo_queue_count (progress_queue);
	    
	    /* this is where a recvd hdr forces us to close a connection */
	    /* i.e. when we send a xoff back to some and then need to clean up */
	    /* this check should be after the progress hdrs recvd check as */
	    /* that can trigger this being set */
	    
	} /* while queued progress items to work though */
	
	
	/* should be a set of ifs here to decide when we are done */
	done = 1;
	
    } /* while not done (main loop) */

	if (ft_mpi_conn_any_error()) return (MPI_ERR_OTHER);
    
    return (0);
}



int ftmpi_nb_post_allpending (int tgid, int chan)
{  
   int done=0;
   struct req_list *rlp;
   MPI_Request request;

   while (!done) {
	   rlp = req_list_find_by_rgid_and_notchan (req_list_send_head, tgid, chan);
	   if (!rlp) return (0);
	   else { /* a vaid request, so post it */
		  request = rlp->rl_req_handle;

#ifdef VERBOSES2
		  printf("ftmpi_nb_post_allpending: on 0x%x chan %d posting request %d\n", tgid, chan, request);
#endif

	  	ftmpi_nb_post_send (request);

	   } /* valid request */
   } /* searching for a valid request */

    return 0;
}

/* misc call back functions */

int ftmpi_nb_snipe2_sentdata_callback (SNIPE2CALLBACKPROTO)
{
    struct req_list * rlp;
    
    rlp = (struct req_list *) userval;
    
#ifdef VERBOSES2
    printf("ftmpi_nb_snipe2_sentdata_callback: on NB req %d SNIPE2 conn %d reqid %d\n",
	   rlp->rl_req_handle, onchan, req);
#endif
    
    /* once the data is sent, free any temp buffers if WE allocated them */
    if (rlp->rl_req_rawbuf) {
#ifdef USE_MALLOC
     _FREE( rlp->rl_req_rawbuf );
     rlp->rl_req_rawbuf = NULL;
#else
     free_msg_buf (rlp->rl_req_rawbufid);
	 rlp->rl_req_rawbufid = -1;
     rlp->rl_req_rawbuf = NULL;
#endif
    }
    
    /* move this request to the DONE list */
    ftmpi_nb_move_send_2_done (rlp);
    
    return (0);
}

/* this call back is for the case when the data is sent in two parts */
int ftmpi_nb_snipe2_sentjustdatahdr_callback (SNIPE2CALLBACKPROTO)
{
    struct req_list * rlp;

    rlp = (struct req_list *) userval;

#ifdef VERBOSES2
    printf("ftmpi_nb_snipe2_sentjustdatahdr_callback: on NB req %d SNIPE2 conn %d reqid %d\n",
	   rlp->rl_req_handle, onchan, req);
#endif
    /* no action needed. Final version does not need this call back */
    
    if (rlp->rl_req_rawbuf) {
#ifdef USE_MALLOC 
      _FREE ( rlp->rl_req_rawbuf ); 
      rlp->rl_req_rawbuf = NULL;
#else
      free_msg_buf (rlp->rl_req_rawbufid);
	  rlp->rl_req_rawbufid = -1;
      rlp->rl_req_rawbuf = NULL;
#endif
    }
    
    return (0);
}


/*
 * This routine takes the recvd header and matches it to either
 * a preposted recv request or set it up to be put on the unexp queue.
 *
 * No mater what happens (unless a zero length message) the routines
 * called from here post additional recv requests to get the actual msg data
 *
 * This routine is needed under SNIPE2 but NOT FAMP/OVM
 */

int ftmpi_nb_handle_recvd_header (int recvconn)
{
    int rc;
    msg_hdr_t* hdrptr;
    int intype, inmsgid, incomm, infrom, into, infromgid, intogid, intag, insize;
    struct req_list *rlp=NULL;    /* ptr to a match nb req */
    
    hdrptr = conn_get_recvhdr (recvconn);		/* get the pointer to the header */
    
#ifdef VERBOSES2
    conn_hexdump ((char*)hdrptr, msg_hdr_size);
    printf("recvd header  ");
    debug_dump_header (hdrptr);
#endif
    
    get_hdr ((*hdrptr), intype, infromgid, intogid, insize, inmsgid,
	     incomm, infrom, into, intag);
    
    /* ok we have a header now */
    /* at this point we can match to a NB req or put the data on the unexpq */
    /* if the data has no payload then we don't post an additional recv */
    
    if (req_list_recv_count) { /* i.e. MPI nb recvs pending */
	rlp = req_list_find_req_matching_recvhdr (req_list_recv_head, 
						  infrom, intag, incomm);
    }
    
    if (rlp) { /* header matches a MPI nb pending recv */
#ifdef VERBOSES2
	printf("Header matches a pending NB recv. Completing\n");
#endif
	
	/* we need to move it from the recv list to the inprogress list */
	ftmpi_nb_move_recv_2_inprogress (rlp);
	/* this prevents other headers from matching accidently */
	
	/* here we call int conn_snipe2_post_recvdata () */
	/* with the right target buffer */
	/* this is effectively a replacement of ftmpi_pp_put_into_req_list */
	
	/* the data size the NB request needs. used to detect TRUNC as we set status in here */
	/* 		 expsize = rlp->rl_status.msglength;	 */
	/* wrong until we do the ftmpi_ddt_decode_size_det() call we don't know the actual read size! */
	
	rc = ftmpi_pp_post_recv_into_req_list (rlp, recvconn, intype, inmsgid, incomm, infrom, into, 
					       infromgid, intogid, intag, insize);
	
	/* if we have a trunc situation we post a trunc recv to flush the channel */
	if (insize>(rlp->rl_status.msglength)) rc = conn_snipe2_post_recvtrunc (recvconn,(insize-rlp->rl_status.msglength));
	
	
    }
    
    if (!rlp) { /* no match or no MPI nb recvs pending */
#ifdef VERBOSES2
	printf("Header does not match a pending NB recv: queuing on unexpected msgQ\n");
#endif		 
	rc = ftmpi_pp_post_recv_into_msg_list (recvconn, intype, inmsgid, incomm, infrom, into, 
					       infromgid, intogid, intag, insize);
	
	/* trunctaion is handled when we match an unexpected recvd message to a NB recv */
	/* until then how do we know we have trunction? */
    } 
    
    /* ok, all recvs to get data posted */
    /* this means we can NOW post the next recv header on this connection */
    
    /* meaning we might have the following pending ops */
    /* recv_data, recv_trunc, recv_hdr */
    
    rc =  conn_snipe2_post_recvhdr (recvconn);
    
    return (0);
}



int ftmpi_nb_handle_recvd_data (int recvconn)
{
    int chan=0;
    struct req_list *rlp=NULL;    /* ptr to a match nb req */
    struct msg_list *mlp=NULL;	 /* ptr to msg list element */
    
    chan = conn_get_recvptrs (recvconn, &rlp, &mlp);
    
#ifdef VERBOSES2
    printf("ftmpi_nb_handle_recvd_data: recv completed on conn %d chan %d for MPI NB rlp 0x%x mlp 0x%x \n",
	   recvconn, chan, rlp, mlp);
    fflush(stdout);
#endif
    
    if (rlp) { /* we completed a nonblocking request */
	/* decode the incomming data */
	ftmpi_nb_decode_buf (rlp);
	
	/* mark as done */
	/* move off the inprogress list */
	ftmpi_nb_move_recv_inprogress_2_done (rlp);
    }
    
    if (mlp) { /* we completed a unexpected message recv */
	/* we do not need to decode this msg until we match it. */
	/* but we need to make sure that it is added to the correct unexpected message queue */
	/* if it did prematch a NB request */
	if (mlp->ml_matched_nb_rlp) {
	    /* sometimes its possible for a matching nb request to be posted */
	    /* that matches the unexp msg before it is fully completed */
	    /* if this happens the mlp is marked with the matching rlp */
	    /* now that the msg is complete we can complete the nb request */
	    ftmpi_nb_complete_msg_to_req (mlp->ml_matched_nb_rlp, mlp);
	}
	else {
	    if (mlp->ml_queue != systemQ)
		ftmpi_pp_add_msg_to_msg_list (mlp);
	    else {
#ifdef VERBOSE
		fprintf(stderr,"Note: unexpected system message completed.\n");
#endif
		/* no action to take until recovery or new com complete */
	    }
	}
    }
    return 0;
}




int debug_dump_header (msg_hdr_t * hptr)
{
    msg_hdr_t hdr;
    int intype, inmsgid, incomm, infrom, into, infromgid, intogid, intag, insize;
    
    
    hdr = *hptr;
    
    get_hdr (hdr, intype, infromgid, intogid, insize, inmsgid,
	     incomm, infrom, into, intag);
    
    printf( "hdr: incom %d infrom %d into %d infromgid 0x%x intogid 0x%x intag %d insize %d\n",
	    incomm, infrom, into, infromgid, intogid, intag, insize);
    return 0;
}


/* Why am I not using the handle_nullproc routine ? 
   For persistent requests we must no remove the old
   request ( that's why I cannot use the move_2_done routines
   aswell, but have to unroll everything manually.
*/
void ftmpi_nb_per_set_nullstatus_and_mark_done ( MPI_Request req)
{
    struct req_list *rlp;
    int dt;
    
    rlp = reqtable[req];    /* get request element */
    
    /* Increment counter for request and datatype */
    rlp->rl_req_refcnt++;
    
    dt = rlp->rl_req_dt;
    if (dt>0) ftmpi_ddt_increment_dt_uses (dt);
    
    /* Set NULL status */
    ftmpi_pp_set_nullstatus ( &rlp->rl_status );
    
    /* and finally one last important update */
    rlp->rl_status.flags |= FTMPI_REQ_DONE;
    
    /* move it to the done queue */
    req_list_add_2tail (req_list_done_head, rlp);
    /* update done counters */
    req_list_done_count++;
}




int ftmpi_nb_dump_all_queues ()
{
   printf("-------------------------------------------------------------\n");
   printf("ftmpi_nb_dump_all_queues for GID %d\n", my_gid);
   printf("-------------------------------------------------------------\n");
   /* request lists first */
   req_list_dump_compact ("send Q\0", req_list_send_head, req_list_send_count);
   req_list_dump_compact ("recv Q\0", req_list_recv_head, req_list_recv_count);
   req_list_dump_compact ("done Q\0", req_list_done_head, req_list_done_count);
   req_list_dump_compact ("inprogress recvQ\0", req_list_recvs_inprogress_head, req_list_recvs_inprogress_count);
	/* message lists queue */
   msg_list_dump_compact ("incomplete Q\0", incomplete_msg_list_head, incomplete_msg_list_count);
   msg_list_dump_compact ("system Q\0", system_msg_list_head, system_msg_list_count);
   printf("-------------------------------------------------------------\n");
   return MPI_SUCCESS;
}

/*
 * special recovery filtering and mapping calls 
 */
int ftmpi_nb_move_msg_list_to_system_list (struct msg_list* ml_head)
{
   struct msg_list* mlp;
   int done=0;

   if (!ml_head) return (MPI_ERR_INTERN); /* if bad list return rather than deadlock */

   while (!done) {
	  mlp = msg_list_head (ml_head);
	  if (mlp) {
		 msg_list_detach (mlp); /* as its still on the message list! */
		 msg_list_add_2tail (system_msg_list_head, mlp);
		 mlp->ml_queue=systemQ; /* yes it has changed queues */
		 system_msg_list_count++;
	  }
	  else done=1;
   }
return (0);
}


int ftmpi_nb_filter_system_list (int cnt, int* deadlist)
{
     struct msg_list *mlp;
     struct msg_list *ml_head;

	 struct msg_list **rm_mlist;
	 int realcnt=0;
	 int syslistcnt=0;

	 int i;


/* printf("ftmpi_nb_filter_system_list: cnt %d system_msg_list_count %d\n",  */
/* 	  cnt, system_msg_list_count); */

	 if (cnt<1) return (0);
	 if (!deadlist) return (MPI_ERR_INTERN);
/* for(i=0;i<cnt;i++) printf("dead %d = %d  ", i, deadlist[i]); */
/* printf("\n"); */

	 if (system_msg_list_count<1) return (0); /* nothing to check anyway! */

	 /* we are a 2 pass algorithm */

	 /* ok build the real list of failed requests */
	 /* we need at least as many as the system list is long currently */
	 rm_mlist = (struct msg_list**) _MALLOC (
		   	system_msg_list_count*sizeof (struct msg_list*));
	 
	 ml_head = system_msg_list_head;


     for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link) {
		for (i=0;i<cnt;i++) {
		   if ( (deadlist[i]==mlp->ml_sgid)||
			    (deadlist[i]==mlp->ml_rgid) ) {
			rm_mlist[realcnt] = mlp; /* remember this to delete later */
			realcnt++;
		   }
	 	}
		syslistcnt++;
	 }
/* printf("\n"); */

	 if (!realcnt) {
		_FREE (rm_mlist);
		return (0); /* if none found return sooner, i.e. now */
	 }

	 else {
		for (i=0;i<realcnt;i++) {
/* printf("freeing %d of %d 0x%x pre-syscnt %d\n", i, realcnt, rm_mlist[i], system_msg_list_count); */
		   ftmpi_nb_msg_force_free (rm_mlist[i]);
		   system_msg_list_count--;
		}
	 }

	_FREE (rm_mlist);	/* free tmp to remove list */

	 return (realcnt);
}


int ftmpi_nb_filter_req_lists_and_reqtable (int cnt, int* deadlist)
{
/* right now I do not alter the reqtable. Edgar may change this later. GEF03 */

/* I filter the done and then the send & recv request lists */
/* done must be filtered first as filtering the others can cause more reqs */ 
/* to be placed on it from the send & recv queues that are marked then */
/* as cancelled */

 /* cancel dead reqs on done list */
ftmpi_nb_filter_req_list (cnt, deadlist, req_list_done_head, &req_list_done_count);
 /* cancel dead reqs on sending list */
ftmpi_nb_filter_req_list (cnt, deadlist, req_list_send_head, &req_list_send_count);
 /* cancel dead reqs on recving list */
ftmpi_nb_filter_req_list (cnt, deadlist, req_list_recv_head, &req_list_recv_count);

return (0);
}

int ftmpi_nb_filter_req_list (int cnt, int* deadlist, struct req_list* rl_head, 								long *listcounter)
{
     struct req_list *rlp;

	 struct req_list **rm_rlist;
	 int realcnt=0;

	 int i;


	 if (cnt<1) return (0);
	 if (!deadlist) return (MPI_ERR_INTERN);

	 if (!listcounter)	return (MPI_ERR_INTERN); /* no lost counter value?? */
	 if (*listcounter<1) return (0); /* nothing to check anyway! */

	 /* we are a 2 pass algorithm */

	 /* ok build the real list of failed requests to CANCEL */
	 /* we need at least as many as the list is long currently */
	 rm_rlist = (struct req_list**) _MALLOC (
		   	(*listcounter)*sizeof (struct req_list*));
	 

     for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {
		for (i=0;i<cnt;i++) {
		   if ( (deadlist[i]==rlp->rl_sgid)||
			    (deadlist[i]==rlp->rl_rgid) ) {
			rm_rlist[realcnt] = rlp; /* remember this to cancel later */
			realcnt++;
		   }
	 	}
	 }

	 if (!realcnt) {
		_FREE (rm_rlist);
		return (0); /* if none found return sooner, i.e. now */
	 }

	 else {
		for (i=0;i<realcnt;i++) {
		   ftmpi_nb_req_force_cancel (rm_rlist[i]);
		   (*listcounter)--;
		}
	 }

	_FREE (rm_rlist);	/* free tmp to remove list */

	 return (realcnt);
}
