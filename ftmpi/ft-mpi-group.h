
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_GROUPS
#define _FT_MPI_H_GROUPS

/*
 MPI groups system header
 */

typedef struct {
  MPI_Group group;
  int freed;
  int refcnt;
  int in_use;

  int maxsize;
  int currentsize;
  int nprocs;
  int my_rank;
  int my_gid;
  int gids[MAXPERAPP];
} group_t;

/* the following is the handle for group that always exists.. :) */
#define MPI_GROUP_WORLD	1
#define MPI_GROUP_SELF	2


int * ftmpi_group_get_gids_ptr ( MPI_Group group );
group_t * ftmpi_group_get_ptr (int i);
int ftmpi_group_gid (MPI_Group grp, int rank);
int ftmpi_group_ok (MPI_Group grp);
group_t* ftmpi_group_init (group_t* grp);
int ftmpi_group_clr_all_but_world (void);
int ftmpi_group_clr (int i);
int ftmpi_group_get_free (int *newgrp);
int ftmpi_group_build (int grp, int gid, int *ids, int ext, int procs);
int ftmpi_group_rank (int grp);
int ftmpi_group_size (int grp);
int ftmpi_group_copy (int grp, int *newgrp);
int ftmpi_group_free (MPI_Group *grp) ;
int ftmpi_group_check_freed (MPI_Group grp);
int ftmpi_group_refinc (MPI_Group grp);
int ftmpi_group_refdec (MPI_Group grp);
int *ftmpi_group_get_gids_ptr ( MPI_Group grp );
int ftmpi_group_display (MPI_Group grp);
void ftmpi_group_relocate ( group_t *g1, group_t *g2 );
void ftmpi_group_arrfree ( group_t *g );

int ftmpi_mpi_group_incl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp);
int ftmpi_mpi_group_range_incl(MPI_Group group,int n, int ranges[][3],
			       MPI_Group * newgroup);
int ftmpi_mpi_group_intersection ( MPI_Group group1, MPI_Group group2,
				   MPI_Group *group_out );
int ftmpi_mpi_group_excl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp);




#endif /*  _FT_MPI_H_GROUPS */

