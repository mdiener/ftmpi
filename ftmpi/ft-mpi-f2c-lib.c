
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Antonin Bukovsky <tone@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include "ft-mpi-f2c-lib.h"
#include "ft-mpi-fprot.h"


int _ATB_MAX_CFLAG_LEN =0;
int * _ATB_MAX_CFLAG = NULL;

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
MPI_Datatype fdt2c (int indt)
{
  return ((MPI_Datatype) indt);
  if (indt==MPI_INTEGER) return (MPI_INT);
  if (indt==MPI_REAL) return (MPI_FLOAT);
  if (indt==MPI_DOUBLE_PRECISION) return (MPI_DOUBLE);
  if (indt==MPI_BYTE) return (MPI_CHAR);
  if (indt==MPI_CHARACTER) return (MPI_CHAR);

  return ((MPI_Datatype) indt);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int cdt2f (MPI_Datatype indt)
{
  if (indt==MPI_INT) return (MPI_INTEGER);
  if (indt==MPI_FLOAT) return (MPI_REAL);
  if (indt==MPI_DOUBLE) return (MPI_DOUBLE_PRECISION);
  if (indt==MPI_CHAR) return (MPI_BYTE);
  if (indt==MPI_CHAR) return (MPI_CHARACTER);
 
  return ((int)indt);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int fstr2c (char *in, char *out, int outmaxlen) 
{
  int i, j;
  j = strlen (in);
  if (j >= outmaxlen ) j = outmaxlen -1; /* truncate with space for null */
  
  for(i=0;i<j;i++) 
    if (in[i]<' ' || in[i]>122) break;
    
  strncpy (out, in, i); 
  out[i] = '\0';
  
  return (i);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

#ifdef MPIINITDONERIGHT /* i.e. the search for Spork (argc, argv) 
			   continues*/
void mpi_init_( ierr )
int *ierr;
{
  int  Argc, i, argsize = 40;
  char **Argv, *p;
  int  ArgcSave;
  char **ArgvSave;
 
  /* Recover the args with the Fortran routines iargc_ and getarg_ */
  ArgcSave = Argc = mpir_iargc_() + 1;
  ArgvSave = Argv = (char **)MALLOC( Argc * sizeof(char *) );
  if (!Argv) {
    *ierr = ERROR( (MPI_Comm)0, MPI_ERR_OTHER,"Out of space in MPI_INIT" );
    return;
  }
  for (i=0; i<Argc; i++) {
    ArgvSave[i] = Argv[i] = (char *)MALLOC( argsize + 1 );
    if (!Argv[i]) {
      *ierr = ERROR( (MPI_Comm)0, MPI_ERR_OTHER,"Out of space in MPI_INIT" );
      return;
    }
    mpir_getarg_( &i, Argv[i], argsize );
    /* Trim trailing blanks */
    p = Argv[i] + argsize - 1;
    while (p > Argv[i]) {
      if (*p != ' ') {
        p[1] = '\0';
        break;
      }
      p--;
    }
  }
 
  *ierr = MPI_Init( &Argc, &Argv );
 
  /* Recover space */
  for (i=0; i<ArgcSave; i++) {
    FREE( ArgvSave[i] );
  }
  FREE( ArgvSave );
}
#else /* MPIINITDONERIGHT */
void mpi_init_( ierr )
int *ierr;
{
#ifdef DEBUGNAME
  fprintf(stderr,"%sFunction MPI_Init.\n", HDR );
#endif
  *ierr = MPI_Init( NULL, NULL ); /* only if we can get away with it ;) */
}

#endif /* MPIINITDONERIGHT */

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Initialized(int *flag); */
void mpi_initialized_(int *flag,int * __ierr) 
{
  *__ierr = MPI_Initialized((int *)ToPtr(flag));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_finalize_(int * __ierr)
{
  *__ierr = MPI_Finalize();
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Abort(MPI_Comm comm,int errorcode); */
void mpi_abort_(int * comm,int * errorcode,int * __ierr)
{
 *__ierr = MPI_Abort((MPI_Comm)(*((int*)comm)),(*((int*)errorcode)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_get_processor_name_ ( char *name, int* len, int* __ierr)
{
  *__ierr = MPI_Get_processor_name ( name, len );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* double MPI_Wtime() */
double mpi_wtime_()
{
  return (MPI_Wtime ());
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
double mpi_wtick_ ()
{
  return ( MPI_Wtick() );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Pcontrol(const int level,...) */
void mpi_pcontrol_(int *level,...)
{
  MPI_Pcontrol ( *level );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/



