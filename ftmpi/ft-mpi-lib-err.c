
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@Cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
/*#include "ft-mpi-sys.h" */
#include "ft-mpi-com.h"
#include "ft-mpi-err.h"


/* Definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Error_class          = PMPI_Error_class
#    pragma weak  MPI_Error_string         = PMPI_Error_string
#    pragma weak  MPI_Errhandler_create    = PMPI_Errhandler_create
#    pragma weak  MPI_Errhandler_free      = PMPI_Errhandler_free
#    pragma weak  MPI_Errhandler_get       = PMPI_Errhandler_get
#    pragma weak  MPI_Errhandler_set       = PMPI_Errhandler_set
#    pragma weak MPI_Add_error_class		       = PMPI_Add_error_class 
#    pragma weak MPI_Add_error_code		       = PMPI_Add_error_code  
#    pragma weak MPI_Add_error_string		       = PMPI_Add_error_string
#    pragma weak MPI_Comm_create_errhandler	       = PMPI_Comm_create_errhandler 
#    pragma weak MPI_Comm_get_errhandler	       = PMPI_Comm_get_errhandler    
#    pragma weak MPI_Comm_set_errhandler	       = PMPI_Comm_set_errhandler    
#    pragma weak MPI_Comm_call_errhandler	       = PMPI_Comm_call_errhandler 

#endif

#    define MPI_Error_class           PMPI_Error_class
#    define MPI_Error_string          PMPI_Error_string
#    define MPI_Errhandler_create     PMPI_Errhandler_create
#    define MPI_Errhandler_free       PMPI_Errhandler_free
#    define MPI_Errhandler_get        PMPI_Errhandler_get
#    define MPI_Errhandler_set        PMPI_Errhandler_set
#    define MPI_Add_error_class			PMPI_Add_error_class 
#    define MPI_Add_error_code			PMPI_Add_error_code  
#    define MPI_Add_error_string		PMPI_Add_error_string
#    define MPI_Comm_create_errhandler		PMPI_Comm_create_errhandler 
#    define MPI_Comm_get_errhandler		PMPI_Comm_get_errhandler    
#    define MPI_Comm_set_errhandler		PMPI_Comm_set_errhandler    
#    define MPI_Comm_call_errhandler		PMPI_Comm_call_errhandler 



int MPI_Errhandler_create ( MPI_Handler_function *fn, MPI_Errhandler *errhandler )
{
  int ret;

  ret = ftmpi_errh_get_next_free ( FTMPI_ERR_COMM1 );
  if ( ret == MPI_ERR_INTERN )
    RETURNERR ( MPI_COMM_WORLD, ret );

  if ( fn != NULL )
    {
      ftmpi_errh_set ( ret, (void*) fn );
      *errhandler = ftmpi_errh_get_err ( ret );
    }
  else 
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG) ;


  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Errhandler_set  ( MPI_Comm comm, MPI_Errhandler errhandler )
{
  int tmperrh;
  int handle;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  handle = ftmpi_errh_get_handle ( errhandler );
  if (handle < 0 ) 
    RETURNERR ( comm, MPI_ERR_ARG ); /* must be an invalid errhandler */


  /* since we have to unmark the 'old' errhandler, we have
     to recover it first ...*/
  tmperrh = ftmpi_com_get_errhandler ( comm );
  ftmpi_errhval_unmark (tmperrh, comm );
	  
  /* Set now the new errhandler. Register it at the errhandler list
     and at the communicator */
  ftmpi_errhval_mark ( handle, comm );
  ftmpi_com_set_errhandler ( comm, handle );

  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Errhandler_get  ( MPI_Comm comm, MPI_Errhandler *errhandler )
{
  int handle;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  handle = ftmpi_com_get_errhandler ( comm );
  *errhandler = ftmpi_errh_get_err ( handle );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Errhandler_free ( MPI_Errhandler *errhandler )
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_errh_get_handle ( *errhandler );
  if ( ret < 0 ) RETURNERR ( MPI_COMM_WORLD, ret );
    
  *errhandler = ftmpi_errh_release ( ret );
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Error_string    ( int errorcode, char *string, int *resultlen )
{
  int ret;
  int handle;

  CHECK_MPIINIT;

  ret = ftmpi_errcode_test ( errorcode );
  if ( ret != MPI_SUCCESS )
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG );

  handle = ftmpi_errcode_get_handle ( errorcode );
  ftmpi_errcode_get_string ( handle, string, resultlen );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Error_class     ( int errorcode, int *errorclass )
{
  int ret;
  int classhandle;
  int handle;
  
  CHECK_MPIINIT;

  ret = ftmpi_errcode_test ( errorcode );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG );

  handle = ftmpi_errcode_get_handle ( errorcode );
  classhandle = ftmpi_errcode_get_class ( handle );
  *errorclass = ftmpi_errclass_get_value ( classhandle );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_create_errhandler ( MPI_Comm_errhandler_fn *fn, 
				 MPI_Errhandler *errhandler )
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_errh_get_next_free ( FTMPI_ERR_COMM2 );
  if ( ret == MPI_ERR_INTERN )
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_INTERN );

  if ( fn == NULL ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG );

  ftmpi_errh_set ( ret, (void *)fn );
  *errhandler = ftmpi_errh_get_err ( ret );

  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_set_errhandler ( MPI_Comm comm, MPI_Errhandler errhandler )
{
  int tmperrh;
  int handle;

  CHECK_MPIINIT;
  CHECK_COM(comm);
    
  handle = ftmpi_errh_get_handle ( errhandler );
  if (handle < 0 ) RETURNERR ( comm, MPI_ERR_ARG );

  /* since we have to unmark the 'old' errhandler, we have
     to recover it first ...*/
  tmperrh = ftmpi_com_get_errhandler ( comm );
  ftmpi_errhval_unmark (tmperrh, comm );
	  
  /* Set now the new errhandler. Register it at the errhandler list
     and at the communicator */
  ftmpi_errhval_mark ( handle, comm );
  ftmpi_com_set_errhandler ( comm, handle );

  return ( MPI_SUCCESS  );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_get_errhandler ( MPI_Comm comm, MPI_Errhandler *errhandler )
{
  int handle;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  handle = ftmpi_com_get_errhandler ( comm );
  *errhandler = ftmpi_errh_get_err ( handle );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Add_error_class  ( int *errorclass )
{
  int handle;
  int class;

  CHECK_MPIINIT;
  
  handle = ftmpi_errclass_get_next_free (&class);
  if ( handle == MPI_ERR_INTERN )
    RETURNERR ( MPI_COMM_WORLD, handle );

  *errorclass = class;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Add_error_code   ( int errorclass, int *errorcode )
{
  int classhandle;
  int codehandle;
  int code;
  int ret;

  CHECK_MPIINIT;

  classhandle = ftmpi_errclass_get_handle ( errorclass );
  if ( classhandle == MPI_ERR_ARG )
    RETURNERR (MPI_COMM_WORLD,  classhandle ); /* invalid error class */

  codehandle = ftmpi_errcode_get_next_free ( classhandle, &code );
  ret = ftmpi_errclass_add_errcode ( classhandle, codehandle );
  if ( ret == MPI_ERR_INTERN ) 
    RETURNERR  (MPI_COMM_WORLD,  ret ); /* couldn't attach this errorcode to the class,
					since the list is already full */
  
  *errorcode = code;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Add_error_string ( int errorcode, char *string )
{
  int handle;
  int ret;

  CHECK_MPIINIT;

  handle = ftmpi_errcode_get_handle ( errorcode );
  if ( handle == MPI_ERR_ARG ) 
    RETURNERR (MPI_COMM_WORLD,  handle ); /* invalide error code */

  ret = ftmpi_errcode_set_string ( handle, string );
  if ( (ret == MPI_ERR_ARG ) || ( ret == MPI_ERR_INTERN ))
    RETURNERR (MPI_COMM_WORLD,  ret ); /* not allowed to set string for a predefined code 
				     or a malloc failed */

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_call_errhandler ( MPI_Comm comm, int errorcode )
{
  MPI_Comm_errhandler_fn *fn;
  int handle;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  handle = ftmpi_com_get_errhandler ( comm );
  fn = ( MPI_Comm_errhandler_fn *) ftmpi_errh_get_function ( handle );

  fn ( &comm, &errorcode );
  return ( MPI_SUCCESS );
}



