/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors of this file:	
           Edgar Gabriel <egabriel@cs.utk.edu>
           Graham E Fagg <fagg@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/


#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-buffer.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-group.h"
#include "ft-mpi-com.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-procinfo.h"
#include "ft-mpi-nb.h"

/* Definitions for the profiling interface */

#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Buffer_attach        = PMPI_Buffer_attach
#    pragma weak  MPI_Buffer_detach        = PMPI_Buffer_detach
#    pragma weak  MPI_Bsend                = PMPI_Bsend
#    pragma weak  MPI_Ibsend               = PMPI_Ibsend

#endif /* HAVE_PRAGMA_WEAK */

#    define  MPI_Buffer_attach         PMPI_Buffer_attach
#    define  MPI_Buffer_detach         PMPI_Buffer_detach
#    define  MPI_Bsend                 PMPI_Bsend
#    define  MPI_Ibsend                PMPI_Ibsend


int MPI_Buffer_attach ( void *buffer, int size )
{
  int flag;

  CHECK_MPIINIT;
  CHECK_FT(MPI_COMM_WORLD);

  if ( size < 0 ) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if ( buffer == NULL ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);

  flag = ftmpi_buffer_isattached ();
  if ( flag ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );

  ftmpi_buffer_init ( buffer, size );
    
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Buffer_detach ( void *buffer, int *size )
{
  int flag;

  CHECK_MPIINIT;
  CHECK_FT(MPI_COMM_WORLD);

  if ( size == NULL ) RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);

  flag = ftmpi_buffer_isattached ();
  if ( !flag ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );
  
  flag = ftmpi_buffer_detach ( (void **) buffer, size );
  if ( flag != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_INTERN );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Bsend ( void *buf, int count, MPI_Datatype datatype, int dest,
		int tag, MPI_Comm comm )
{
  used_buffer *handle;
  int packsize;
  int ret;
  void *bufp;
  MPI_Request req;
  int flag;
  int size;
  FTMPI_DDT_CODE_INFO * code_info = NULL;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);
  GET_PPT_SIZE (comm,size);

  CHECK_TAG_S(comm, tag);
  
  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT);
  if ( (dest < 0) && ( dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR(comm, MPI_ERR_RANK);

  CHECK_DDT(comm, datatype, 1);

  if ( dest == MPI_PROC_NULL )
    return ( MPI_SUCCESS );

  flag = ftmpi_buffer_isattached ();
  if ( !flag ) RETURNERR ( comm, MPI_ERR_BUFFER );

  size = ftmpi_buffer_size ();
  code_info = (FTMPI_DDT_CODE_INFO*) ftmpi_procinfo_get_ddt_code_info(ftmpi_com_gid(comm, dest));

  ret = ftmpi_ddt_encode_size_det(datatype, count, &packsize,code_info);
  if ( packsize > size ) RETURNERR( comm, MPI_ERR_BUFFER);

  handle = ftmpi_buffer_getnextseg ( packsize, &bufp);
  if ( handle == NULL ) 
    {
      fprintf(stderr, "FTMPI Error in MPI_Bsend: user buffer exhausted\n");
      RETURNERR ( comm, MPI_ERR_BUFFER );
    }
    
  ret = ftmpi_ddt_xdr_encode_dt_to_buffer(buf, bufp, NULL, packsize,
					  count, datatype, code_info);

  ret = ftmpi_mpi_isend ( bufp, packsize, MPI_BYTE, dest, tag, comm, &req);
  if ( ret != MPI_SUCCESS ) return ( ret );

  ftmpi_buffer_used_setreq ( handle, req );
  ftmpi_buffer_used_setlcontrol ( handle );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Ibsend(void* buf,int count,MPI_Datatype datatype,int dest,
	       int tag,MPI_Comm comm,MPI_Request *request)
{
  used_buffer *handle;
  int packsize;
  int ret;
  void *bufp;
  MPI_Request req;
  int flag;
  int size;
  FTMPI_DDT_CODE_INFO * code_info = NULL;
  
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);
  GET_PPT_SIZE(comm,size);
  CHECK_TAG_S(comm, tag);

  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT);
  if ( (dest < 0) && ( dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR (comm, MPI_ERR_RANK);

  CHECK_DDT (comm, datatype, 1);

  if ( dest == MPI_PROC_NULL )
    return ( MPI_Isend ( buf, count, datatype, dest, tag, comm, request));

  flag = ftmpi_buffer_isattached ();
  if ( !flag ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );

  size = ftmpi_buffer_size ();

  code_info = (FTMPI_DDT_CODE_INFO*) ftmpi_procinfo_get_ddt_code_info(ftmpi_com_gid(comm, dest));
  ret = ftmpi_ddt_encode_size_det(datatype, count, &packsize,code_info);
  if ( packsize > size ) RETURNERR (comm, MPI_ERR_BUFFER);

  handle = ftmpi_buffer_getnextseg ( packsize, &bufp);
  if ( handle == NULL ) 
    {
      fprintf(stderr, "FTMPI Error in Ibsend: user buffer exhausted\n");
      RETURNERR ( comm, MPI_ERR_BUFFER );
    }
    
  ret = ftmpi_ddt_xdr_encode_dt_to_buffer(buf, bufp, NULL, packsize,
					  count, datatype, code_info);

  ret = ftmpi_mpi_isend ( bufp, packsize, MPI_BYTE, dest, tag, comm, &req);
  if ( ret != MPI_SUCCESS ) return ( ret );
  
  ftmpi_buffer_used_setreq ( handle, req);

  /*mark request as IBSEND request */
  ftmpi_nb_put_req_type ( REQ_IBSEND, req);

  *request = req;
  return ( MPI_SUCCESS );
}
