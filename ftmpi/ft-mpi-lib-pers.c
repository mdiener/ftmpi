/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-buffer.h"
#include "ft-mpi-nb.h"
#include "ft-mpi-group.h"
#include "ft-mpi-com.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-procinfo.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Bsend_init           = PMPI_Bsend_init
#    pragma weak  MPI_Recv_init            = PMPI_Recv_init
#    pragma weak  MPI_Rsend_init           = PMPI_Rsend_init
#    pragma weak  MPI_Send_init            = PMPI_Send_init
#    pragma weak  MPI_Ssend_init           = PMPI_Ssend_init
#    pragma weak  MPI_Start                = PMPI_Start
#    pragma weak  MPI_Startall             = PMPI_Startall

#endif

#    define  MPI_Bsend_init            PMPI_Bsend_init
#    define  MPI_Recv_init             PMPI_Recv_init
#    define  MPI_Rsend_init            PMPI_Rsend_init
#    define  MPI_Send_init             PMPI_Send_init
#    define  MPI_Ssend_init            PMPI_Ssend_init
#    define  MPI_Start                 PMPI_Start
#    define  MPI_Startall              PMPI_Startall




int MPI_Recv_init(void* buf,int count,MPI_Datatype datatype,int source,
		  int tag,MPI_Comm comm,MPI_Request *request)
{
  MPI_Request req;
  int ret;
  int size;

  /* Check for initialization, communicator and tag */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);

  CHECK_TAG_R(comm,tag);
  CHECK_REQ_P(comm, request);

  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );
  CHECK_DDT( comm, datatype, 1);

  if ( (source < 0) && (source != MPI_PROC_NULL) && (source!=MPI_ANY_SOURCE) ) 
    RETURNERR (comm, MPI_ERR_RANK);
  if ( source >= size ) RETURNERR ( comm, MPI_ERR_RANK );


#ifdef RECV_VERBOSE 
  printf("recv rank [%d] from [%d] com mode = %d stat %d "
	 "remote stat %d\n", coms[0].my_rank, source, comm_mode, 
	 comm_state, remote_state); 
#endif  

  /* check for failure */
  CHECK_FT(comm);

  /* First find free req handle */
  req = ftmpi_nb_get_freehandle ();

  if (req<0) {
    *request = MPI_REQUEST_NULL;
    RETURNERR (comm, MPI_ERR_INTERN);
  }

  /* set request value */
  *request = req;	

  ret = ftmpi_nb_set_req (buf, count, datatype, source, tag, 
			  comm, REQ_PRECV, req);
  if ( ret != MPI_SUCCESS )
    RETURNERR(comm, ret);

  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Send_init (void* buf, int count, MPI_Datatype datatype,
		   int dest, int tag, MPI_Comm comm, 
		   MPI_Request *request)
{
  int ret;
  MPI_Request req;
  int size;


  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);

  CHECK_REQ_P(comm,request);
  CHECK_TAG_S(comm,tag);

  if ( (dest < 0) && (dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR (comm, MPI_ERR_RANK );
  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );
  
  CHECK_DDT( comm, datatype, 1);

#ifdef SEND_VERBOSE
  printf("Send from rank [%d] to [%d] com mode = %d stat %d "
	 "remote stat %d\n", coms[0].my_rank,
	 dest, comm_mode, comm_state, remote_state); 
#endif
   
  /* check for failure */
  CHECK_FT(comm);

  /* NB stuff */

  /* First find free req handle */
  req = ftmpi_nb_get_freehandle ();
  if (req<0) {
    *request = MPI_REQUEST_NULL;
    RETURNERR (comm,MPI_ERR_INTERN);
  }

  /* set request value */
  *request = req;	
  
  ret = ftmpi_nb_set_req (buf, count, datatype, dest, tag, comm, 
			  REQ_PSEND, req);
  if ( ret != MPI_SUCCESS )
    RETURNERR(comm, ret );
  
  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* ATTENTION: in the switch statement, the cases for send, rsend
   and ssend are currently agglomerated. This might change in the
   future. EG Jan. 27 2003 */

int MPI_Start ( MPI_Request *request )
{
  int ret;
  MPI_Request req;
  int type;
  int tr;    /* target rank: source for a recv, dest for a send */
  int rc;
  int self; /* if a send to self */
  
 
  CHECK_MPIINIT;

  /* THe next statement is not completly correct. Theoretically
     we should get the communicator which was attached to this
     persistent request, and use its errorhandler!
     EG. March 11, 2003 
  */
  CHECK_FT(MPI_COMM_WORLD);
  CHECK_REQ_P(MPI_COMM_WORLD, request);
  req = *request; /* cache request */
  rc = ftmpi_nb_req_ok (req);
  if (!rc) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
  
  /* Has this request already bin used in Start without a Wait ? */
  rc = ftmpi_nb_get_req_refcnt ( req );
  if ( rc > 1 ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_REQUEST);

  type = ftmpi_nb_get_req_type ( req );
  if ( type < 0 ) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);

  tr = ftmpi_nb_get_req_target_rank ( req );
  if (tr == MPI_PROC_NULL ) {
    ftmpi_nb_per_set_nullstatus_and_mark_done ( req);
    return (MPI_SUCCESS);
  }

  switch ( type )
    {
    case ( REQ_PRECV ):
      {
	ret =  ftmpi_nb_check_msglist_for_matching_req_and_get(req); 
	if ( !ret )
	  {
	    /* must inc ref cnt each time, set by second arg. */
	    ret = ftmpi_nb_req_add2_recv_list (req, 1);
	    if ( ret < 0 ) return ( ret );

	    ret = ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink (req);
	  }
	else
	  {
	    /* increase counter manually */
	    ftmpi_nb_req_incref ( req );
	    ret = MPI_SUCCESS;
	  }
	    
	break;
      }
    case (REQ_PSEND):
    case (REQ_PSSEND):
    case (REQ_PRSEND):
      {
	/* Increment datatype ? */

	ret = ftmpi_nb_req_add2_send_list (req, 1);
	/* must inc ref cnt each time */

	/* send to self check.. */
	self = ftmpi_nb_send2_self_test_and_do (req);
	if (self<0) RETURNERR (MPI_COMM_WORLD, self); 
	if (self>0) return ( MPI_SUCCESS);
	/* if self = 0 which means not to self */

	ret = ftmpi_nb_post_send(req);
	ftmpi_nb_progress (0);
	ftmpi_nb_req_isdone (req);

	break;
      }
    case (REQ_PBSEND):
      {
	void *buf, *bufp;
	int count;
	MPI_Datatype ddt;
	int size, packsize;
	used_buffer *handle;	
	int flag;
	FTMPI_DDT_CODE_INFO * code_info = NULL;


	/* Get the original buf, count and ddt setting from the user */
	ret = ftmpi_nb_get_req_bsendinit ( &buf, &count, &ddt, req);

	/* Check whether MPI_Buffer_attach has been called */
	flag = ftmpi_buffer_isattached ();
	if ( !flag ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );

	/* Check overall size of the buffer */
	size = ftmpi_buffer_size ();
	/* Our message packed would take up to ... bytes. Does this fit ? */

	/* This might need to change, depending on whether I need to know how am
	   I sending to. Currently, for RSC, it works */
	code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(my_gid);
	ret = ftmpi_ddt_encode_size_det(ddt, count, &packsize,code_info);

	if ( packsize > size ) RETURNERR( MPI_COMM_WORLD, MPI_ERR_BUFFER);
	
	/* ok, find a memory segment in the buffer */
	handle = ftmpi_buffer_getnextseg ( packsize, &bufp);
	if ( handle == NULL ) 
	  {
	    fprintf(stderr, "FTMPI Error in MPI_Start: user buffer exhausted\n");
	    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );
	  }
	/* pack the data */
	ret = ftmpi_ddt_xdr_encode_dt_to_buffer(buf, bufp, NULL, packsize,
					  count, ddt, code_info);
	
	/* attach the req to this buffer-segment */
	ftmpi_buffer_used_setreq ( handle, req);

	/* correct the settings of buf, count, and ddt for the actual 
	   communication */
	ftmpi_nb_set_req_buf_cnt_dt ( bufp, packsize, MPI_PACKED, req );
	ret = ftmpi_nb_req_add2_send_list (req, 1);
			/* must inc ref cnt each time */

	/* send to self check.. */
	self = ftmpi_nb_send2_self_test_and_do (req);
	if (self<0) RETURNERR (MPI_COMM_WORLD, self); 
	if (self>0) return ( MPI_SUCCESS);
	/* if self = 0 which means not to self */

	ret = ftmpi_nb_post_send(req);
	ftmpi_nb_progress (0);
	ftmpi_nb_req_isdone (req);

	break;
      }
    default:
      {
	ret = MPI_ERR_INTERN;
      }
    }

  if ( ret != MPI_SUCCESS )
    RETURNERR(MPI_COMM_WORLD,ret);

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* ATTENTION: if one of the initiated communications goes wrong,
   we stop in the Start-loop and return immediatly. This behavior
   might change in the future. EG Jan. 27 2003. */

int MPI_Startall (int count, MPI_Request *arr_of_requests)
{
  int ret;
  MPI_Request req;
  int i;
  int rc;
  int tr;   /* target rank */
  int type;
  int self;

  CHECK_MPIINIT;
  CHECK_FT(MPI_COMM_WORLD);
  CHECK_REQ_P(MPI_COMM_WORLD,arr_of_requests);
  if (count == 0 ) return (MPI_SUCCESS);

  for ( i = 0; i < count; i ++ )
    {
      req = arr_of_requests[i]; /* cache request */
      rc = ftmpi_nb_req_ok (req);
      if (!rc) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);

      type = ftmpi_nb_get_req_type ( req );
      if ( type < 0 ) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);

      /* Has this request already bin used in Start without a Wait ? */
      rc = ftmpi_nb_get_req_refcnt ( req );
      if ( rc > 1 ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_REQUEST);
      
      tr = ftmpi_nb_get_req_target_rank ( req );
      if ( tr == MPI_PROC_NULL ) {
	ftmpi_nb_per_set_nullstatus_and_mark_done ( req);
	continue;
      }

      switch ( type )
	{
	case ( REQ_PRECV ):
	  {
	    ret =  ftmpi_nb_check_msglist_for_matching_req_and_get(req); 
	    if ( !ret )
	      {
		/* must inc ref cnt each time, set by second arg. */
		ret = ftmpi_nb_req_add2_recv_list (req, 1);
		if ( ret >= 0 )  
		  ret = ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink (req);
	      }
	    else
	      {
		/* increase counter manually */
		ftmpi_nb_req_incref ( req );
		ret = MPI_SUCCESS;
	      }
	    
	    if (ret<0) RETURNERR (MPI_COMM_WORLD,ret);
	    break;
	  }
	case (REQ_PSEND):
	case (REQ_PSSEND):
	case (REQ_PRSEND):
	  {
	    ret = ftmpi_nb_req_add2_send_list (req, 1);
			/* must inc ref cnt each time */
	    if (ret<0) RETURNERR (MPI_COMM_WORLD,ret);

	   /* send to self check.. */
	   self = ftmpi_nb_send2_self_test_and_do (req);
	   if (self<0) RETURNERR (MPI_COMM_WORLD, self); 
	   if (self>0) continue;
	   /* if self = 0 which means not to self */

	   ret = ftmpi_nb_post_send(req);
	   ftmpi_nb_progress (0);
	   ftmpi_nb_req_isdone (req);
	   
	    break;
	  }
	case (REQ_PBSEND):
	  {
	    void *buf, *bufp;
	    int count;
	    MPI_Datatype ddt;
	    int size, packsize;
	    used_buffer *handle;	
	    int flag;
	    FTMPI_DDT_CODE_INFO * code_info = NULL;
     

	    /* Get the original buf, count and ddt setting from the user */
	    ret = ftmpi_nb_get_req_bsendinit ( &buf, &count, &ddt, req);
	    
	    /* Check whether MPI_Buffer_attach has been called */
	    flag = ftmpi_buffer_isattached ();
	    if ( !flag ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );
	    
	    /* Check overall size of the buffer */
	    size = ftmpi_buffer_size ();
	    /* Our message packed would take up to ...bytes. Does this fit ? */


	    /* This might need to change, depending on whether I need to know how am
	       I sending to. Currently, for RSC, it works */
	    code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(my_gid);
	    ret = ftmpi_ddt_encode_size_det(ddt, count, &packsize,code_info);

	    if ( packsize > size ) RETURNERR( MPI_COMM_WORLD, MPI_ERR_BUFFER);
	    
	    /* ok, find a memory segment in the buffer */
	    handle = ftmpi_buffer_getnextseg ( packsize, &bufp);
	    if ( handle == NULL ) 
	      {
		fprintf(stderr, "FTMPI Error in MPI_Startall: user buffer exhausted\n");
		RETURNERR ( MPI_COMM_WORLD, MPI_ERR_BUFFER );
	      }
	    /* pack the data */
	    ret = ftmpi_ddt_xdr_encode_dt_to_buffer(buf, bufp, NULL, packsize,
						    count, ddt, code_info);
	    
	    /* attach the req to this buffer-segment */
	    ftmpi_buffer_used_setreq ( handle, req);
	    
	    /* correct the settings of buf, count, and ddt for the actual 
	       communication */
	    ftmpi_nb_set_req_buf_cnt_dt ( bufp, packsize, MPI_BYTE, req );
	    ret = ftmpi_nb_req_add2_send_list (req, 1);

	   /* send to self check.. */
	   self = ftmpi_nb_send2_self_test_and_do (req);
	   if (self<0) RETURNERR (MPI_COMM_WORLD, self); 
	   if (self>0) continue;
	   /* if self = 0 which means not to self */
	   
	   ret = ftmpi_nb_post_send(req);
	   ftmpi_nb_progress (0);
	   ftmpi_nb_req_isdone (req);
	   break;
	  }
	default:
	  {
	    RETURNERR(MPI_COMM_WORLD,MPI_ERR_INTERN);
	  }
	} /* switch */
    }/* for ( i = 0;...) */
      
  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

/* ATTENTION: the implementation of Ssend_init, Rsend_init and Bsend_init
   are currently just copies of Send_init. This might change in later 
   versions. EG, Jan. 27. 2003 */

int MPI_Ssend_init (void* buf, int count, MPI_Datatype datatype,
		   int dest, int tag, MPI_Comm comm, 
		   MPI_Request *request)
{
  return ( PMPI_Send_init ( buf, count, datatype, dest, tag, comm,
			    request ));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Rsend_init (void* buf, int count, MPI_Datatype datatype,
		   int dest, int tag, MPI_Comm comm, 
		   MPI_Request *request)
{
  return ( PMPI_Send_init ( buf, count, datatype, dest, tag, comm,
			    request ));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Bsend_init (void* buf, int count, MPI_Datatype datatype,
		   int dest, int tag, MPI_Comm comm, 
		   MPI_Request *request)
{
  int ret;
  MPI_Request req;
  int size;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);
  CHECK_FT(comm);

  CHECK_REQ_P(comm,request);
  CHECK_TAG_S(comm,tag);

  if ( (dest < 0) && (dest != MPI_PROC_NULL) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( dest >= size ) RETURNERR (comm, MPI_ERR_RANK );
  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );

  CHECK_DDT( comm, datatype, 1);

#ifdef SEND_VERBOSE
  /* printf("Bsend from rank [%d] to [%d] com mode = %d stat %d "
     "remote stat %d\n", coms[0].my_rank,
     dest, comm_mode, comm_state, remote_state); */
#endif
   
  
  /* NB stuff */

  /* First find free req handle */
  req = ftmpi_nb_get_freehandle ();
  if (req<0) {
    *request = MPI_REQUEST_NULL;
    RETURNERR (comm,MPI_ERR_INTERN);
  }

  /* set request value */
  *request = req;	

  ret = ftmpi_nb_set_req (buf, count, datatype, dest, tag, comm, 
			  REQ_PBSEND, req);
  if ( ret != MPI_SUCCESS ) RETURNERR(comm, ret );

  ret = ftmpi_nb_set_req_bsendinit ( buf, count, datatype, req );

  return (MPI_SUCCESS);
}












