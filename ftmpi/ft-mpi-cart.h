
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
                        Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_CART
#define _FT_MPI_H_CART

cart_info_t* ftmpi_cart_set ( int ndims, int num_ranks, int *dims, 
			      int *periods);
void ftmpi_cart_free (cart_info_t *cart);
cart_info_t* ftmpi_cart_copy (cart_info_t *cart);
int ftmpi_cart_compare (cart_info_t *cart1, cart_info_t *cart2);
int ftmpi_cart_get_ndims ( cart_info_t *cart );
int* ftmpi_cart_get_dims_ptr ( cart_info_t *cart );
int* ftmpi_cart_get_periods_ptr ( cart_info_t *cart );
int ftmpi_cart_calc_rank ( cart_info_t *cart, int *coords );
int ftmpi_cart_calc_coords ( int *coords, int *dims, int rank, int ndims );
int ftmpi_cart_calc_neighbors ( int direction, int displ, int *r_source,
				int *r_dest, cart_info_t *cart, int rank );


#endif /*  _FT_MPI_H_CART */

