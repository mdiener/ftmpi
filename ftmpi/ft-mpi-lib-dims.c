
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include <mpi.h>
#include "ft-mpi-lib.h"
#include <stdio.h>
#include "debug.h"

#ifdef HAVE_PRAGMA_WEAK
#    pragma weak  MPI_Dims_create   = PMPI_Dims_create
#endif

#    define  MPI_Dims_create        PMPI_Dims_create



#define mpi_guess(a,b)  mpi_root((a),(b))

int mpi_root( double, double);
static int getFirstBit ( int, int * );
static int factorAndCombine ( int, int, int * );

#define ROOT_ITERS 10



/******************************************************************/
/******************************************************************/
/******************************************************************/
int mpi_root(double x_in, double n_in)
{
  int n = (int)n_in;
  int x = (int)x_in;
  unsigned long i, j, r ;
  unsigned long guess, high, low ;
  
  if (n == 0 || x == 0)
    return (1);
  
  r = n ;
  for(i=1;i<(unsigned long)n;i++)
    r*=n ;
  r = x/r ;
  guess = 1<<(31/n) ;
  guess-- ;
  if(r<guess)
    guess = r ;
  high = guess ;
  low = 1 ;
  for(j=0;j<ROOT_ITERS;j++) {
    r = guess ;
    for(i=1;i<(unsigned long)n;i++)
      r *= guess ;
    if(r > (unsigned long)x) {
      high = guess ;
      guess = (guess - low)/2 + low ;
    } else {
      low = guess ;
      guess = (high-guess)/2 + guess ;
    }
  }
  
  if (guess > 0)
    return (int)(guess) ;
  else
    return (1);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static int getFirstBit(int inputInt, int *bitPositionPtr)
{
  int mask, saveMask, bitPosition;
  
  if (inputInt == 0){
    *bitPositionPtr = -1;
    return(0);
  }
  
  mask = 1;
  saveMask = 0;
  bitPosition = 0;
  while (saveMask == 0){
    if ((mask & inputInt) != 0){
      saveMask = mask;
    }
    mask <<= 1;
    bitPosition++;
  }
  
  *bitPositionPtr = bitPosition - 1;
  return(saveMask);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static int factorAndCombine(int factorMe,int numFactors,int *factors)
{
  typedef struct BranchInfo{
    int currentBranch;
    int nextBranch;
    int currentValue;
  }
  BranchInfo;
  
  
#define NUM_PRIMES 168
  static int primes[NUM_PRIMES]=
  {   2,3,5,7,11,13,17,19,23,29,
      31,37,41,43,47,53,59,61,67,71,
      73,79,83,89,97,101,103,107,109,113,
      127,131,137,139,149,151,157,163,167,173,
      179,181,191,193,197,199,211,223,227,229,
      233,239,241,251,257,263,269,271,277,281,
      283,293,307,311,313,317,331,337,347,349,
      353,359,367,373,379,383,389,397,401,409,
      419,421,431,433,439,443,449,457,461,463,
      467,479,487,491,499,503,509,521,523,541,
      547,557,563,569,571,577,587,593,599,601,
      607,613,617,619,631,641,643,647,653,659,
      661,673,677,683,691,701,709,719,727,733,
      739,743,751,757,761,769,773,787,797,809,
      811,821,823,827,829,839,853,857,859,863,
      877,881,883,887,907,911,919,929,937,941,
      947,953,967,971,977,983,991,997
  };
  
#define MAX_PRIME primes[NUM_PRIMES-1]
  
  int treeIndex,firstNonZeroBit,bitPosition,tmp,mask,remainingFactorMe;
  BranchInfo *searchTree,bestBranch;
  int *primeFactors;
  int status,i,j,maxNumFactors,t,k,n,q,r,testing;
  int numPrimeFactors,factorCount,insertIndex=0;
  int numPrimeLeft;
  double nthRoot,distance,minDistance=0.0;
  /*  int mpi_errno; */
  
  
  
  
  
  if((factorMe <=0)||(factorMe >=(MAX_PRIME * MAX_PRIME))||(numFactors <=0)){
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }
  
  
  if(numFactors==1){
    factors[0]=factorMe;
    status=MPI_SUCCESS;
    return(status);
  }
  
  for(i=0;i<numFactors;i++){
    factors[i]=1;
  }
  
  if(factorMe==1){
    status=MPI_SUCCESS;
    return(status);
  }
  
  
  tmp=factorMe;
  i=0;
  while(tmp > 0){
    i++;
    tmp >>=1;
  }
  maxNumFactors=i + 1;
  
  
  primeFactors=(int *)_MALLOC(maxNumFactors*sizeof(int));
  if(primeFactors==((int *)NULL)){
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_INTERN);
  }
  
  t=0;
  k=0;
  n=factorMe;
  
  while(n !=1){
    testing=1;
    while(testing==1){
      q=n/primes[k];
      r=n % primes[k];
      if(r==0){
        
        t++;
        primeFactors[t - 1]=primes[k];
        n=q;
        testing=0;
      }
      else if(q > primes[k]){
        k++;
      }
      else{
        t++;
        primeFactors[t - 1]=n;
        n=1;
        testing=0;
      }
    }
  }
  numPrimeFactors=t;
  if(numFactors >=numPrimeFactors){
    for(i=0;i<numPrimeFactors;i++){
      factors[i]=primeFactors[(numPrimeFactors - 1)- i];
    }
  }
  else{
    searchTree=(BranchInfo *)_MALLOC(maxNumFactors*sizeof(BranchInfo));
    if(searchTree==((BranchInfo *)NULL)){
      _FREE(primeFactors);
      RETURNERR (MPI_COMM_WORLD, MPI_ERR_INTERN);
    }
    remainingFactorMe=factorMe;
    factorCount=0;
    numPrimeLeft=numPrimeFactors;
    nthRoot=mpi_guess((double)remainingFactorMe,((double)(numFactors - factorCount)));
    tmp=0;
    i=numPrimeLeft - 1;
    while((i >=0)&&((numFactors - factorCount)> 1)){
      if(primeFactors[i] >=nthRoot){
        factors[factorCount]=primeFactors[i];
        remainingFactorMe /=primeFactors[i];
        factorCount++;
        if(numFactors > factorCount){
          nthRoot=mpi_guess((double)remainingFactorMe,((double)(numFactors - factorCount)));
        }
        else{
          nthRoot=0.0;
        }
        tmp++;
      }
      else{
        i=0;
      }
      i--;
    }
    numPrimeLeft -=tmp;
    while((numPrimeLeft >(numFactors - factorCount))&&((numFactors - factorCount)> 1))
    {
      treeIndex=0;
      searchTree[treeIndex].currentBranch=1 <<(numPrimeLeft - 1);
      searchTree[treeIndex].currentValue=primeFactors[numPrimeLeft - 1];
      if((searchTree[treeIndex].currentBranch & 3)!=0){
        searchTree[treeIndex].nextBranch=0;
      }
      else{
        searchTree[treeIndex].nextBranch=searchTree[treeIndex].currentBranch +(1 <<(numPrimeLeft - 3));
      }
      bestBranch.currentBranch=searchTree[treeIndex].currentBranch;
      bestBranch.currentValue=searchTree[treeIndex].currentValue;
      if((double)bestBranch.currentValue==nthRoot){
        searchTree[0].currentBranch=0;
      }
      else{
        minDistance=nthRoot -(double)bestBranch.currentValue;
        minDistance *=minDistance;
      }
      while(searchTree[0].currentBranch !=0){
        if((searchTree[treeIndex].currentBranch & 1)==1){
          while((searchTree[treeIndex].nextBranch==0)&&(treeIndex > 0)){
            treeIndex--;
          }
          if(searchTree[treeIndex].nextBranch==0){
            treeIndex--;
          }
          if(treeIndex==-1){
            searchTree[treeIndex + 1].currentBranch >>=1;
            if(searchTree[treeIndex + 1].currentBranch > 0){
              tmp=getFirstBit( searchTree[treeIndex + 1].currentBranch,&bitPosition);
              searchTree[treeIndex + 1].currentValue=primeFactors[bitPosition];
            }
          }
          else {
            searchTree[treeIndex + 1].currentBranch=searchTree[treeIndex].nextBranch;
            tmp=getFirstBit( searchTree[treeIndex + 1].currentBranch,&bitPosition);
            searchTree[treeIndex + 1].currentValue=searchTree[treeIndex].currentValue * primeFactors[bitPosition];
            if((searchTree[treeIndex].nextBranch & 1)==1){
              searchTree[treeIndex].nextBranch=0;
            }
            else{
              firstNonZeroBit=getFirstBit( searchTree[treeIndex].nextBranch,&bitPosition);
              searchTree[treeIndex].nextBranch &=~firstNonZeroBit;
              searchTree[treeIndex].nextBranch +=firstNonZeroBit >> 1;
            }
          }
          treeIndex++;
          if((searchTree[treeIndex].currentBranch & 3)!=0){
            searchTree[treeIndex].nextBranch=0;
          }
          else{
            firstNonZeroBit=getFirstBit( searchTree[treeIndex].currentBranch,&bitPosition);
            searchTree[treeIndex].nextBranch=searchTree[treeIndex].currentBranch +(firstNonZeroBit >> 2);
          }
        }
        else{
          firstNonZeroBit=getFirstBit( searchTree[treeIndex].currentBranch,&bitPosition);
          firstNonZeroBit >>=1;
          bitPosition -=1;
          searchTree[treeIndex + 1].currentBranch=searchTree[treeIndex].currentBranch + firstNonZeroBit;
          searchTree[treeIndex + 1].currentValue=searchTree[treeIndex].currentValue * primeFactors[bitPosition];
          treeIndex++;
          if((searchTree[treeIndex].currentBranch & 3)!=0){
            searchTree[treeIndex].nextBranch=0;
          }
          else{
            searchTree[treeIndex].nextBranch=searchTree[treeIndex].currentBranch +(firstNonZeroBit >> 2);
          }
        }
        distance=nthRoot -(double)searchTree[treeIndex].currentValue;
        if(distance < 0.0){
          searchTree[treeIndex].nextBranch=0;
        }
        distance *=distance;
        if(distance < minDistance){
          minDistance=distance;
          bestBranch.currentBranch=searchTree[treeIndex].currentBranch;
          bestBranch.currentValue=searchTree[treeIndex].currentValue;
          if(minDistance==0.0)searchTree[0].currentBranch=0;
        }
      }
      remainingFactorMe /=bestBranch.currentValue;
      mask=1;
      tmp=0;
      for(bitPosition=0;bitPosition<numPrimeLeft;bitPosition++){
        if((bestBranch.currentBranch & mask)!=0){
          primeFactors[bitPosition]=0;
          tmp++;
        }
        mask <<=1;
      }
      i=0;
      while(tmp > 0){
        if(primeFactors[i]==0){
          for(j=i;j<(numPrimeLeft - 1);j++){
            primeFactors[j]=primeFactors[j+1];
          }
          tmp--;
          numPrimeLeft--;
          i--;
        }
        i++;
      }
      i=0;
      insertIndex=factorCount;
      while(i < factorCount){
        if(bestBranch.currentValue > factors[i]){
          insertIndex=i;
          break;
        }
        i++;
      }
      for(i=factorCount;i>insertIndex;i--){
        factors[i]=factors[i-1];
      }
      factors[insertIndex]=bestBranch.currentValue;
      factorCount++;
      if(numFactors > factorCount){
        nthRoot=mpi_guess((double)remainingFactorMe,((double)(numFactors - factorCount)));
      }
      else{
        nthRoot=0.0;
      }
      tmp=0;
      i=numPrimeLeft;
      while((i >=0)&&((numFactors - factorCount)> 1)){
        if(primeFactors[i] >=nthRoot){
          factors[factorCount]=primeFactors[i];
          remainingFactorMe /=primeFactors[i];
          factorCount++;
          if(numFactors > factorCount){
            nthRoot=mpi_guess((double)remainingFactorMe,((double)(numFactors - factorCount)));
          }
          else{
            nthRoot=0.0;
          }
          tmp++;
        }
        else{
          i=0;
        }
        i--;
      }
      numPrimeLeft -=tmp;
    }
    if(factorCount==(numFactors - 1)){
      tmp=primeFactors[0];
      for(j=1;j<numPrimeLeft;j++){
        tmp *=primeFactors[j];
      }
      numPrimeLeft=0;
      i=0;
      while(i < factorCount){
        if(tmp > factors[i]){
          insertIndex=i;
          i=factorCount;
        }
        i++;
      }
      if(i==factorCount){
        insertIndex=i;
      }
      for(i=factorCount;i>insertIndex;i--){
        factors[i]=factors[i-1];
      }
      factors[insertIndex]=tmp;
      factorCount++;
    }
    _FREE(searchTree);
  }
  _FREE(primeFactors);
  return(MPI_SUCCESS);
}

/******************************************************************/
/******************************************************************/
/******************************************************************/
int MPI_Dims_create(int nnodes, int ndims, int *dims)
{
  int i, *newDims, newNdims;
  int testProduct, freeNodes, stat, ii;
  /*   int mpi_errno = MPI_SUCCESS; */
  /*   static char myname[] = "MPI_DIMS_CREATE"; */
  

  CHECK_MPIINIT;

  if (nnodes <= 0)
    return MPI_ERR_ARG;
  if (ndims <= 0)
    return MPI_ERR_ARG;
  
  newNdims = 0;
  for (i=0; i<ndims; i++) {
    if (dims[i]<0) {
      return MPI_ERR_DIMS;
    }
    if (dims[i]==0)
      newNdims++;
  }
  
  if (newNdims == 0)  {
    testProduct = 1;
    for (i=0; i<ndims; i++) {
      testProduct *= dims[i];
    }
    if (testProduct != nnodes) {
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_DIMS);
    }
    else
      return MPI_SUCCESS;
  }
  
  freeNodes = nnodes;
  for (i=0; i<ndims; i++) {
    if (dims[i]>0) {
      if (nnodes%dims[i] != 0) {
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_DIMS);
      }
      freeNodes /= dims[i];
    }
  }
  
  newDims = (int *)_MALLOC(newNdims*sizeof(int));
  if (newDims == ((int *)0)) {
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_INTERN);
  }
  
  stat = factorAndCombine(freeNodes, newNdims, newDims);
  if (stat != 0) {
    _FREE(newDims);
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_INTERN);
  }
  
  for (i=0, ii=0; i<ndims; i++) {
    if (dims[i]==0) {
      dims[i] = newDims[ii];
      ii++;
    }
  }
  
  _FREE(newDims);
  
  return(MPI_SUCCESS);
}
