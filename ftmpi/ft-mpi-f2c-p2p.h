/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_P2P
#define _FT_MPI_H_F2C_P2P

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define HDR     "FTMP:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak MPI_SEND              = mpi_send_
#    pragma weak PMPI_SEND             = mpi_send_ 
#    pragma weak MPI_RECV              = mpi_recv_
#    pragma weak PMPI_RECV             = mpi_recv_ 
#    pragma weak MPI_SENDRECV          = mpi_sendrecv_
#    pragma weak PMPI_SENDRECV         = mpi_sendrecv_ 
#    pragma weak MPI_BSEND             = mpi_bsend_
#    pragma weak PMPI_BSEND            = mpi_bsend_ 
#    pragma weak MPI_RSEND             = mpi_rsend_
#    pragma weak PMPI_RSEND            = mpi_rsend_ 
#    pragma weak MPI_SSEND             = mpi_ssend_
#    pragma weak PMPI_SSEND            = mpi_ssend_ 
#    pragma weak MPI_SENDRECV_REPLACE  = mpi_sendrecv_replace_	
#    pragma weak PMPI_SENDRECV_REPLACE = mpi_sendrecv_replace_
#    pragma weak MPI_PROBE             = mpi_probe_	
#    pragma weak PMPI_PROBE            = mpi_probe_
#    pragma weak MPI_BUFFER_ATTACH     = mpi_buffer_attach_	
#    pragma weak PMPI_BUFFER_ATTACH    = mpi_buffer_attach_
#    pragma weak MPI_BUFFER_DETACH     = mpi_buffer_detach_	
#    pragma weak PMPI_BUFFER_DETACH    = mpi_buffer_detach_

#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak mpi_send__              = mpi_send_
#    pragma weak pmpi_send__             = mpi_send_
#    pragma weak mpi_recv__              = mpi_recv_
#    pragma weak pmpi_recv__             = mpi_recv_
#    pragma weak mpi_sendrecv__          = mpi_sendrecv_
#    pragma weak pmpi_sendrecv__         = mpi_sendrecv_
#    pragma weak mpi_bsend__             = mpi_bsend_ 
#    pragma weak pmpi_bsend__            = mpi_bsend_
#    pragma weak mpi_rsend__             = mpi_rsend_
#    pragma weak pmpi_rsend__            = mpi_rsend_
#    pragma weak mpi_ssend__             = mpi_ssend_
#    pragma weak pmpi_ssend__            = mpi_ssend_
#    pragma weak mpi_sendrecv_replace__  = mpi_sendrecv_replace_
#    pragma weak pmpi_sendrecv_replace__ = mpi_sendrecv_replace_
#    pragma weak mpi_probe__             = mpi_probe_
#    pragma weak pmpi_probe__            = mpi_probe_
#    pragma weak mpi_buffer_attach__     = mpi_buffer_attach_
#    pragma weak pmpi_buffer_attach__    = mpi_buffer_attach_
#    pragma weak mpi_buffer_detach__     = mpi_buffer_detach_
#    pragma weak pmpi_buffer_detach__    = mpi_buffer_detach_


#  elif defined(FORTRANNOUNDERSCORE)


#    pragma weak mpi_send              = mpi_send_
#    pragma weak pmpi_send             = mpi_send_
#    pragma weak mpi_recv              = mpi_recv_
#    pragma weak pmpi_recv             = mpi_recv_
#    pragma weak mpi_sendrecv          = mpi_sendrecv_
#    pragma weak pmpi_sendrecv         = mpi_sendrecv_
#    pragma weak mpi_bsend             = mpi_bsend_ 
#    pragma weak pmpi_bsend            = mpi_bsend_
#    pragma weak mpi_rsend             = mpi_rsend_
#    pragma weak pmpi_rsend            = mpi_rsend_
#    pragma weak mpi_ssend             = mpi_ssend_
#    pragma weak pmpi_ssend            = mpi_ssend_
#    pragma weak mpi_sendrecv_replace  = mpi_sendrecv_replace_
#    pragma weak pmpi_sendrecv_replace = mpi_sendrecv_replace_
#    pragma weak mpi_probe             = mpi_probe_
#    pragma weak pmpi_probe            = mpi_probe_
#    pragma weak mpi_buffer_attach     = mpi_buffer_attach_
#    pragma weak pmpi_buffer_attach    = mpi_buffer_attach_
#    pragma weak mpi_buffer_detach     = mpi_buffer_detach_
#    pragma weak pmpi_buffer_detach    = mpi_buffer_detach_

#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak pmpi_send_             = mpi_send_
#    pragma weak pmpi_recv_             = mpi_recv_
#    pragma weak pmpi_sendrecv_         = mpi_sendrecv_
#    pragma weak pmpi_bsend_            = mpi_bsend_
#    pragma weak pmpi_rsend_            = mpi_rsend_
#    pragma weak pmpi_ssend_            = mpi_ssend_
#    pragma weak pmpi_sendrecv_replace_ = mpi_sendrecv_replace_
#    pragma weak pmpi_probe_            = mpi_probe_
#    pragma weak pmpi_buffer_attach_    = mpi_buffer_attach_
#    pragma weak pmpi_buffer_detach_    = mpi_buffer_detach_


#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define mpi_send_              PMPI_SEND 
#      define mpi_recv_              PMPI_RECV
#      define mpi_sendrecv_          PMPI_SENDRECV 
#      define mpi_bsend_             PMPI_BSEND 
#      define mpi_rsend_             PMPI_RSEND 
#      define mpi_ssend_             PMPI_SSEND 
#      define  mpi_sendrecv_replace_ PMPI_SENDRECV_REPLACE   
#      define  mpi_probe_            PMPI_PROBE    
#      define mpi_buffer_attach_     PMPI_BUFFER_ATTACH     
#      define mpi_buffer_detach_     PMPI_BUFFER_DETACH     

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_send_             pmpi_send__ 
#      define  mpi_recv_             pmpi_recv__ 
#      define  mpi_sendrecv_         pmpi_sendrecv__ 
#      define  mpi_rsend_            pmpi_rsend__ 
#      define  mpi_ssend_            pmpi_ssend__ 
#      define  mpi_bsend_            pmpi_bsend__ 
#      define  mpi_buffer_attach_    pmpi_buffer_attach__ 
#      define  mpi_buffer_detach_    pmpi_buffer_detach__ 
#      define  mpi_sendrecv_replace_ pmpi_sendrecv_replace__ 
#      define  mpi_probe_            pmpi_probe__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_send_             pmpi_send 
#      define  mpi_recv_             pmpi_recv 
#      define  mpi_sendrecv_         pmpi_sendrecv 
#      define  mpi_rsend_            pmpi_rsend 
#      define  mpi_ssend_            pmpi_ssend 
#      define  mpi_bsend_            pmpi_bsend 
#      define  mpi_buffer_attach_    pmpi_buffer_attach 
#      define  mpi_buffer_detach_    pmpi_buffer_detach 
#      define  mpi_sendrecv_replace_ pmpi_sendrecv_replace 
#      define  mpi_probe_            pmpi_probe 


#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define  mpi_send_             pmpi_send_ 
#      define  mpi_recv_             pmpi_recv_ 
#      define  mpi_sendrecv_         pmpi_sendrecv_ 
#      define  mpi_rsend_            pmpi_rsend_ 
#      define  mpi_ssend_            pmpi_ssend_ 
#      define  mpi_bsend_            pmpi_bsend_ 
#      define  mpi_buffer_attach_    pmpi_buffer_attach_ 
#      define  mpi_buffer_detach_    pmpi_buffer_detach_ 
#      define  mpi_sendrecv_replace_ pmpi_sendrecv_replace_ 
#      define  mpi_probe_            pmpi_probe_ 
#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#      define mpi_send_              MPI_SEND 
#      define mpi_recv_              MPI_RECV
#      define mpi_sendrecv_          MPI_SENDRECV 
#      define mpi_bsend_             MPI_BSEND 
#      define mpi_rsend_             MPI_RSEND 
#      define mpi_ssend_             MPI_SSEND 
#      define  mpi_sendrecv_replace_ MPI_SENDRECV_REPLACE   
#      define  mpi_probe_            MPI_PROBE    
#      define mpi_buffer_attach_     MPI_BUFFER_ATTACH     
#      define mpi_buffer_detach_     MPI_BUFFER_DETACH     


#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_send_             mpi_send__ 
#      define  mpi_recv_             mpi_recv__ 
#      define  mpi_sendrecv_         mpi_sendrecv__ 
#      define  mpi_rsend_            mpi_rsend__ 
#      define  mpi_ssend_            mpi_ssend__ 
#      define  mpi_bsend_            mpi_bsend__ 
#      define  mpi_buffer_attach_    mpi_buffer_attach__ 
#      define  mpi_buffer_detach_    mpi_buffer_detach__ 
#      define  mpi_sendrecv_replace_ mpi_sendrecv_replace__ 
#      define  mpi_probe_            mpi_probe__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_send_             mpi_send 
#      define  mpi_recv_             mpi_recv 
#      define  mpi_sendrecv_         mpi_sendrecv 
#      define  mpi_rsend_            mpi_rsend 
#      define  mpi_ssend_            mpi_ssend 
#      define  mpi_bsend_            mpi_bsend 
#      define  mpi_buffer_attach_    mpi_buffer_attach 
#      define  mpi_buffer_detach_    mpi_buffer_detach 
#      define  mpi_sendrecv_replace_ mpi_sendrecv_replace 
#      define  mpi_probe_            mpi_probe 


#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */
#endif
