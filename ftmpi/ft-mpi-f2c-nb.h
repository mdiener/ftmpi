/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_NB
#define _FT_MPI_H_F2C_NB

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define HDR     "FTMP:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak MPI_ISEND                 = mpi_isend_
#    pragma weak PMPI_ISEND                = mpi_isend_ 
#    pragma weak MPI_IRSEND                = mpi_irsend_
#    pragma weak PMPI_IRSEND               = mpi_irsend_ 
#    pragma weak MPI_IBSEND                = mpi_ibsend_	
#    pragma weak PMPI_IBSEND               = mpi_ibsend_
#    pragma weak MPI_ISSEND                = mpi_issend_	
#    pragma weak PMPI_ISSEND               = mpi_issend_
#    pragma weak MPI_IRECV                 = mpi_irecv_
#    pragma weak PMPI_IRECV                = mpi_irecv_ 

#    pragma weak MPI_BSEND_INIT            = mpi_bsend_init_	
#    pragma weak PMPI_BSEND_INIT           = mpi_bsend_init_
#    pragma weak MPI_RECV_INIT             = mpi_recv_init_	
#    pragma weak PMPI_RECV_INIT            = mpi_recv_init_
#    pragma weak MPI_RSEND_INIT            = mpi_rsend_init_	
#    pragma weak PMPI_RSEND_INIT           = mpi_rsend_init_
#    pragma weak MPI_SEND_INIT             = mpi_send_init_	
#    pragma weak PMPI_SEND_INIT            = mpi_send_init_
#    pragma weak MPI_SSEND_INIT            = mpi_ssend_init_	
#    pragma weak PMPI_SSEND_INIT           = mpi_ssend_init_

#    pragma weak MPI_STARTALL              = mpi_startall_	
#    pragma weak PMPI_STARTALL             = mpi_startall_
#    pragma weak MPI_START                 = mpi_start_	
#    pragma weak PMPI_START                = mpi_start_

#    pragma weak MPI_IPROBE                = mpi_iprobe_
#    pragma weak PMPI_IPROBE               = mpi_iprobe_ 
#    pragma weak MPI_REQUEST_FREE          = mpi_request_free_	
#    pragma weak PMPI_REQUEST_FREE         = mpi_request_free_
#    pragma weak MPI_CANCEL                = mpi_cancel_	        
#    pragma weak PMPI_CANCEL               = mpi_cancel_
#    pragma weak MPI_TEST_CANCELLED        = mpi_test_cancelled_	
#    pragma weak PMPI_TEST_CANCELLED       = mpi_test_cancelled_

#    pragma weak MPI_TESTALL               = mpi_testall_	
#    pragma weak PMPI_TESTALL              = mpi_testall_
#    pragma weak MPI_TESTANY               = mpi_testany_	
#    pragma weak PMPI_TESTANY              = mpi_testany_
#    pragma weak MPI_TEST                  = mpi_test_	
#    pragma weak PMPI_TEST                 = mpi_test_
#    pragma weak MPI_TESTSOME              = mpi_testsome_	
#    pragma weak PMPI_TESTSOME             = mpi_testsome_

#    pragma weak MPI_WAITALL               = mpi_waitall_	
#    pragma weak PMPI_WAITALL              = mpi_waitall_
#    pragma weak MPI_WAITANY               = mpi_waitany_	
#    pragma weak PMPI_WAITANY              = mpi_waitany_
#    pragma weak MPI_WAIT                  = mpi_wait_	
#    pragma weak PMPI_WAIT                 = mpi_wait_
#    pragma weak MPI_WAITSOME              = mpi_waitsome_	
#    pragma weak PMPI_WAITSOME             = mpi_waitsome_	 


#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak mpi_isend__               = mpi_isend_
#    pragma weak pmpi_isend__              = mpi_isend_ 
#    pragma weak mpi_irsend__              = mpi_irsend_
#    pragma weak pmpi_irsend__             = mpi_irsend_ 
#    pragma weak mpi_ibsend__              = mpi_ibsend_	
#    pragma weak pmpi_ibsend_              = mpi_ibsend_
#    pragma weak mpi_issend__              = mpi_issend_	
#    pragma weak pmpi_issend__             = mpi_issend_
#    pragma weak mpi_irecv__               = mpi_irecv_
#    pragma weak pmpi_irecv__              = mpi_irecv_ 

#    pragma weak mpi_bsend_init__          = mpi_bsend_init_	
#    pragma weak pmpi_bsend_init__         = mpi_bsend_init_
#    pragma weak mpi_recv_init__           = mpi_recv_init_	
#    pragma weak pmpi_recv_init__          = mpi_recv_init_
#    pragma weak mpi_rsend_init__          = mpi_rsend_init_	
#    pragma weak pmpi_rsend_init__         = mpi_rsend_init_
#    pragma weak mpi_send_init__           = mpi_send_init_	
#    pragma weak pmpi_send_init__          = mpi_send_init_
#    pragma weak mpi_ssend_init__          = mpi_ssend_init_	
#    pragma weak pmpi_ssend_init__         = mpi_ssend_init_

#    pragma weak mpi_startall__            = mpi_startall_	
#    pragma weak pmpi_startall__           = mpi_startall_
#    pragma weak mpi_start__               = mpi_start_	
#    pragma weak pmpi_start__              = mpi_start_

#    pragma weak mpi_iprobe__              = mpi_iprobe_
#    pragma weak pmpi_iprobe__             = mpi_iprobe_ 
#    pragma weak mpi_request_free__        = mpi_request_free_	
#    pragma weak pmpi_request_free__       = mpi_request_free_
#    pragma weak mpi_cancel__              = mpi_cancel_	        
#    pragma weak pmpi_cancel__             = mpi_cancel_
#    pragma weak mpi_test_cancelled__      = mpi_test_cancelled_	
#    pragma weak pmpi_test_cancelled__     = mpi_test_cancelled_

#    pragma weak mpi_testall__             = mpi_testall_	
#    pragma weak pmpi_testall__            = mpi_testall_
#    pragma weak mpi_testany__             = mpi_testany_	
#    pragma weak pmpi_testany__            = mpi_testany_
#    pragma weak mpi_test__                = mpi_test_	
#    pragma weak pmpi_test__               = mpi_test_
#    pragma weak mpi_testsome__            = mpi_testsome_	
#    pragma weak pmpi_testsome__           = mpi_testsome_

#    pragma weak mpi_waitall__             = mpi_waitall_	
#    pragma weak pmpi_waitall__            = mpi_waitall_
#    pragma weak mpi_waitany__             = mpi_waitany_	
#    pragma weak pmpi_waitany__            = mpi_waitany_
#    pragma weak mpi_wait__                = mpi_wait_	
#    pragma weak pmpi_wait__               = mpi_wait_
#    pragma weak mpi_waitsome__            = mpi_waitsome_	
#    pragma weak pmpi_waitsome__           = mpi_waitsome_	 


#  elif defined(FORTRANNOUNDERSCORE)

#    pragma weak mpi_isend               = mpi_isend_
#    pragma weak pmpi_isend              = mpi_isend_ 
#    pragma weak mpi_irsend              = mpi_irsend_
#    pragma weak pmpi_irsend             = mpi_irsend_ 
#    pragma weak mpi_ibsend              = mpi_ibsend_	
#    pragma weak pmpi_ibsend             = mpi_ibsend_
#    pragma weak mpi_issend              = mpi_issend_	
#    pragma weak pmpi_issend             = mpi_issend_
#    pragma weak mpi_irecv               = mpi_irecv_
#    pragma weak pmpi_irecv              = mpi_irecv_ 

#    pragma weak mpi_bsend_init          = mpi_bsend_init_	
#    pragma weak pmpi_bsend_init         = mpi_bsend_init_
#    pragma weak mpi_recv_init           = mpi_recv_init_	
#    pragma weak pmpi_recv_init          = mpi_recv_init_
#    pragma weak mpi_rsend_init          = mpi_rsend_init_	
#    pragma weak pmpi_rsend_init         = mpi_rsend_init_
#    pragma weak mpi_send_init           = mpi_send_init_	
#    pragma weak pmpi_send_init          = mpi_send_init_
#    pragma weak mpi_ssend_init          = mpi_ssend_init_	
#    pragma weak pmpi_ssend_init         = mpi_ssend_init_

#    pragma weak mpi_startall            = mpi_startall_	
#    pragma weak pmpi_startall           = mpi_startall_
#    pragma weak mpi_start               = mpi_start_	
#    pragma weak pmpi_start              = mpi_start_

#    pragma weak mpi_iprobe              = mpi_iprobe_
#    pragma weak pmpi_iprobe             = mpi_iprobe_ 
#    pragma weak mpi_request_free        = mpi_request_free_	
#    pragma weak pmpi_request_free       = mpi_request_free_
#    pragma weak mpi_cancel              = mpi_cancel_	        
#    pragma weak pmpi_cancel             = mpi_cancel_
#    pragma weak mpi_test_cancelled      = mpi_test_cancelled_	
#    pragma weak pmpi_test_cancelled     = mpi_test_cancelled_

#    pragma weak mpi_testall             = mpi_testall_	
#    pragma weak pmpi_testall            = mpi_testall_
#    pragma weak mpi_testany             = mpi_testany_	
#    pragma weak pmpi_testany            = mpi_testany_
#    pragma weak mpi_test                = mpi_test_	
#    pragma weak pmpi_test               = mpi_test_
#    pragma weak mpi_testsome            = mpi_testsome_	
#    pragma weak pmpi_testsome           = mpi_testsome_

#    pragma weak mpi_waitall             = mpi_waitall_	
#    pragma weak pmpi_waitall            = mpi_waitall_
#    pragma weak mpi_waitany             = mpi_waitany_	
#    pragma weak pmpi_waitany            = mpi_waitany_
#    pragma weak mpi_wait                = mpi_wait_	
#    pragma weak pmpi_wait               = mpi_wait_
#    pragma weak mpi_waitsome            = mpi_waitsome_	
#    pragma weak pmpi_waitsome           = mpi_waitsome_	 


#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak pmpi_isend_              = mpi_isend_ 
#    pragma weak pmpi_irsend_             = mpi_irsend_ 
#    pragma weak pmpi_ibsend_             = mpi_ibsend_
#    pragma weak pmpi_issend_             = mpi_issend_
#    pragma weak pmpi_irecv_              = mpi_irecv_ 

#    pragma weak pmpi_bsend_init_         = mpi_bsend_init_
#    pragma weak pmpi_recv_init_          = mpi_recv_init_
#    pragma weak pmpi_rsend_init_         = mpi_rsend_init_
#    pragma weak pmpi_send_init_          = mpi_send_init_
#    pragma weak pmpi_ssend_init_         = mpi_ssend_init_

#    pragma weak pmpi_startall_           = mpi_startall_
#    pragma weak pmpi_start_              = mpi_start_

#    pragma weak pmpi_iprobe_             = mpi_iprobe_ 
#    pragma weak pmpi_request_free_       = mpi_request_free_
#    pragma weak pmpi_cancel_             = mpi_cancel_
#    pragma weak pmpi_test_cancelled_     = mpi_test_cancelled_

#    pragma weak pmpi_testall_            = mpi_testall_
#    pragma weak pmpi_testany_            = mpi_testany_
#    pragma weak pmpi_test_               = mpi_test_
#    pragma weak pmpi_testsome_           = mpi_testsome_

#    pragma weak pmpi_waitall_            = mpi_waitall_
#    pragma weak pmpi_waitany_            = mpi_waitany_
#    pragma weak pmpi_wait_               = mpi_wait_
#    pragma weak pmpi_waitsome_           = mpi_waitsome_	 

#  endif /*FORTRANCAPS, etc. */

#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#    define mpi_isend_                  PMPI_ISEND
#    define mpi_irsend_                 PMPI_IRSEND
#    define mpi_ibsend_                 PMPI_IBSEND	
#    define mpi_issend_                 PMPI_ISSEND	
#    define mpi_irecv_                  PMPI_IRECV

#    define mpi_bsend_init_             PMPI_BSEND_INIT	
#    define mpi_recv_init_              PMPI_RECV_INIT	
#    define mpi_rsend_init_             PMPI_RSEND_INIT	
#    define mpi_send_init_              PMPI_SEND_INIT	
#    define mpi_ssend_init_             PMPI_SSEND_INIT	

#    define mpi_startall_               PMPI_STARTALL	
#    define mpi_start_                  PMPI_START	

#    define mpi_iprobe_                 PMPI_IPROBE
#    define mpi_request_free_           PMPI_REQUEST_FREE	
#    define mpi_cancel_                 PMPI_CANCEL	        
#    define mpi_test_cancelled_         PMPI_TEST_CANCELLED	

#    define mpi_testall_                PMPI_TESTALL	
#    define mpi_testany_                PMPI_TESTANY	
#    define mpi_test_                   PMPI_TEST	
#    define mpi_testsome_               PMPI_TESTSOME	

#    define mpi_waitall_                PMPI_WAITALL	
#    define mpi_waitany_                PMPI_WAITANY	
#    define mpi_wait_                   PMPI_WAIT	
#    define mpi_waitsome_               PMPI_WAITSOME	

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#    define mpi_isend_                  pmpi_isend__
#    define mpi_irsend_                 pmpi_irsend__
#    define mpi_ibsend_                 pmpi_ibsend__	
#    define mpi_issend_                 pmpi_issend__	
#    define mpi_irecv_                  pmpi_irecv__

#    define mpi_bsend_init_             pmpi_bsend_init__	
#    define mpi_recv_init_              pmpi_recv_init__	
#    define mpi_rsend_init_             pmpi_rsend_init__	
#    define mpi_send_init_              pmpi_send_init__	
#    define mpi_ssend_init_             pmpi_ssend_init__	

#    define mpi_startall_               pmpi_startall__	
#    define mpi_start_                  pmpi_start__	

#    define mpi_iprobe_                 pmpi_iprobe__
#    define mpi_request_free_           pmpi_request_free__	
#    define mpi_cancel_                 pmpi_cancel__	        
#    define mpi_test_cancelled_         pmpi_test_cancelled__	

#    define mpi_testall_                pmpi_testall__	
#    define mpi_testany_                pmpi_testany__	
#    define mpi_test_                   pmpi_test__	
#    define mpi_testsome_               pmpi_testsome__	

#    define mpi_waitall_                pmpi_waitall__	
#    define mpi_waitany_                pmpi_waitany__	
#    define mpi_wait_                   pmpi_wait__	
#    define mpi_waitsome_               pmpi_waitsome__	


#    elif defined(FORTRANNOUNDERSCORE)

#    define mpi_isend_                  pmpi_isend
#    define mpi_irsend_                 pmpi_irsend
#    define mpi_ibsend_                 pmpi_ibsend	
#    define mpi_issend_                 pmpi_issend	
#    define mpi_irecv_                  pmpi_irecv

#    define mpi_bsend_init_             pmpi_bsend_init	
#    define mpi_recv_init_              pmpi_recv_init	
#    define mpi_rsend_init_             pmpi_rsend_init	
#    define mpi_send_init_              pmpi_send_init	
#    define mpi_ssend_init_             pmpi_ssend_init	

#    define mpi_startall_               pmpi_startall	
#    define mpi_start_                  pmpi_start	

#    define mpi_iprobe_                 pmpi_iprobe
#    define mpi_request_free_           pmpi_request_free	
#    define mpi_cancel_                 pmpi_cancel	        
#    define mpi_test_cancelled_         pmpi_test_cancelled	

#    define mpi_testall_                pmpi_testall	
#    define mpi_testany_                pmpi_testany	
#    define mpi_test_                   pmpi_test	
#    define mpi_testsome_               pmpi_testsome	

#    define mpi_waitall_                pmpi_waitall	
#    define mpi_waitany_                pmpi_waitany	
#    define mpi_wait_                   pmpi_wait	
#    define mpi_waitsome_               pmpi_waitsome	

#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#    define mpi_isend_                  pmpi_isend_
#    define mpi_irsend_                 pmpi_irsend_
#    define mpi_ibsend_                 pmpi_ibsend_	
#    define mpi_issend_                 pmpi_issend_	
#    define mpi_irecv_                  pmpi_irecv_

#    define mpi_bsend_init_             pmpi_bsend_init_	
#    define mpi_recv_init_              pmpi_recv_init_	
#    define mpi_rsend_init_             pmpi_rsend_init_	
#    define mpi_send_init_              pmpi_send_init_	
#    define mpi_ssend_init_             pmpi_ssend_init_	

#    define mpi_startall_               pmpi_startall_	
#    define mpi_start_                  pmpi_start_	

#    define mpi_iprobe_                 pmpi_iprobe_
#    define mpi_request_free_           pmpi_request_free_	
#    define mpi_cancel_                 pmpi_cancel_	        
#    define mpi_test_cancelled_         pmpi_test_cancelled_	

#    define mpi_testall_                pmpi_testall_	
#    define mpi_testany_                pmpi_testany_	
#    define mpi_test_                   pmpi_test_	
#    define mpi_testsome_               pmpi_testsome_	

#    define mpi_waitall_                pmpi_waitall_	
#    define mpi_waitany_                pmpi_waitany_	
#    define mpi_wait_                   pmpi_wait_	
#    define mpi_waitsome_               pmpi_waitsome_	

#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#    define mpi_isend_                  MPI_ISEND
#    define mpi_irsend_                 MPI_IRSEND
#    define mpi_ibsend_                 MPI_IBSEND	
#    define mpi_issend_                 MPI_ISSEND	
#    define mpi_irecv_                  MPI_IRECV

#    define mpi_bsend_init_             MPI_BSEND_INIT	
#    define mpi_recv_init_              MPI_RECV_INIT	
#    define mpi_rsend_init_             MPI_RSEND_INIT	
#    define mpi_send_init_              MPI_SEND_INIT	
#    define mpi_ssend_init_             MPI_SSEND_INIT	

#    define mpi_startall_               MPI_STARTALL	
#    define mpi_start_                  MPI_START	

#    define mpi_iprobe_                 MPI_IPROBE
#    define mpi_request_free_           MPI_REQUEST_FREE	
#    define mpi_cancel_                 MPI_CANCEL	        
#    define mpi_test_cancelled_         MPI_TEST_CANCELLED	

#    define mpi_testall_                MPI_TESTALL	
#    define mpi_testany_                MPI_TESTANY	
#    define mpi_test_                   MPI_TEST	
#    define mpi_testsome_               MPI_TESTSOME	

#    define mpi_waitall_                MPI_WAITALL	
#    define mpi_waitany_                MPI_WAITANY	
#    define mpi_wait_                   MPI_WAIT	
#    define mpi_waitsome_               MPI_WAITSOME	

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#    define mpi_isend_                  mpi_isend__
#    define mpi_irsend_                 mpi_irsend__
#    define mpi_ibsend_                 mpi_ibsend__	
#    define mpi_issend_                 mpi_issend__	
#    define mpi_irecv_                  mpi_irecv__

#    define mpi_bsend_init_             mpi_bsend_init__	
#    define mpi_recv_init_              mpi_recv_init__	
#    define mpi_rsend_init_             mpi_rsend_init__	
#    define mpi_send_init_              mpi_send_init__	
#    define mpi_ssend_init_             mpi_ssend_init__	

#    define mpi_startall_               mpi_startall__	
#    define mpi_start_                  mpi_start__	

#    define mpi_iprobe_                 mpi_iprobe__
#    define mpi_request_free_           mpi_request_free__	
#    define mpi_cancel_                 mpi_cancel__	        
#    define mpi_test_cancelled_         mpi_test_cancelled__	

#    define mpi_testall_                mpi_testall__	
#    define mpi_testany_                mpi_testany__	
#    define mpi_test_                   mpi_test__	
#    define mpi_testsome_               mpi_testsome__	

#    define mpi_waitall_                mpi_waitall__	
#    define mpi_waitany_                mpi_waitany__	
#    define mpi_wait_                   mpi_wait__	
#    define mpi_waitsome_               mpi_waitsome__	

#    elif defined(FORTRANNOUNDERSCORE)

#    define mpi_isend_                  mpi_isend
#    define mpi_irsend_                 mpi_irsend
#    define mpi_ibsend_                 mpi_ibsend	
#    define mpi_issend_                 mpi_issend	
#    define mpi_irecv_                  mpi_irecv

#    define mpi_bsend_init_             mpi_bsend_init	
#    define mpi_recv_init_              mpi_recv_init	
#    define mpi_rsend_init_             mpi_rsend_init	
#    define mpi_send_init_              mpi_send_init	
#    define mpi_ssend_init_             mpi_ssend_init	

#    define mpi_startall_               mpi_startall	
#    define mpi_start_                  mpi_start	

#    define mpi_iprobe_                 mpi_iprobe
#    define mpi_request_free_           mpi_request_free	
#    define mpi_cancel_                 mpi_cancel	        
#    define mpi_test_cancelled_         mpi_test_cancelled	

#    define mpi_testall_                mpi_testall	
#    define mpi_testany_                mpi_testany	
#    define mpi_test_                   mpi_test	
#    define mpi_testsome_               mpi_testsome	

#    define mpi_waitall_                mpi_waitall	
#    define mpi_waitany_                mpi_waitany	
#    define mpi_wait_                   mpi_wait	
#    define mpi_waitsome_               mpi_waitsome	

#else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#endif /* HAVE_PRAGMA_WEAK */

#endif /* _FT_MPI_H_F2C_NB */
