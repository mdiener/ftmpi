
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* Sep 03 GEF */
/* changed this so that all callers can have access to a freelist */
/* this list is shared amoungst any number of user level/lib lists */
/* i.e. no matter how many inits are called, there is just one free list */


/* MPI includes */
#include <mpi.h>


/* local includes */
#ifndef LISTSH
#include "ft-mpi-list.h"
#endif

#ifndef REQLISTH
#include "ft-mpi-req-list.h"
#endif


/* Typical PVM setup */
#ifdef HASSTDLIB
#include <stdlib.h>
#endif

#if defined(SYSVBFUNC)
#include <memory.h>
#define BZERO(d,n)      memset(d,0,n)
#define BCMP(s,d,n)     memcmp(d,s,n)
#define BCOPY(s,d,n)    memcpy(d,s,n)

#else
#define BZERO(d,n)      bzero(d,n)
#define BCMP(s,d,n)     bcmp(s,d,n)
#define BCOPY(s,d,n)    bcopy(s,d,n)
#endif

#include <stdio.h>	/* just for the dump instructions */
#include <string.h> /* needed for strcmp when checking local,remote names */
#include "debug.h"

/* struct must be of this form */
/*
 * struct type {
 *		struct type *t_link, *t_rlink;
 *		int t_data0;
 *		int t_data1;
 *        .
 *		int t_dataN;
 * };
 */

/* start data lists off this way */
/* struct req_list *rl_head = 0; */
/* static int current_top_key = 0; */

/* the structures used here, contain an empty first element that links to */
/* itself. */


/* free list stuff */
static struct req_list * reqfreelist=NULL;
static long   reqfree=0;
static long   reqmaxfree=0;




/* we need to return this first element so that user can store the head of the
 * list 
 */

struct req_list *
req_list_init()
{
struct req_list *first;

	first = TALLOC(1, struct req_list, 0);
	first->rl_link = first->rl_rlink = first; /* all points to me */
	return (first);
}

/* 
 *  This creates a free list of nb requests of length INITREQS 
 *  */
void req_list_init_freelist(int initelements, int maxelements)
{ 
int i; 
	   struct req_list *rlp;
	     
	     reqfreelist = req_list_init();
		 reqfree = 0;
		 reqmaxfree = maxelements;

		 for(i=0;i<initelements;i++)  {
		    /* make a new entry */

			/* cannot use new here as it will get them off the free list DUD! */
			/* 		    rlp = req_list_new(reqfreelist); */

			if (!(rlp = TALLOC(1, struct req_list, 0))) {
			           fprintf(stderr, "req_list_init_freelist() can't get memory\n");
					           exit(-1);
							       }
			LISTPUTBEFORE(reqfreelist, rlp, rl_link, rl_rlink);


			/* make sure item knows it free */
		    rlp->rl_req_handle = MPI_REQUEST_NULL;        
			reqfree++;
		}
}


struct req_list *
req_list_tail (rl_head)
struct req_list *rl_head;
{
	if (rl_head->rl_rlink != rl_head) return (rl_head->rl_rlink);
	else return ((struct req_list *)0);
}


struct req_list *
req_list_head (rl_head)
struct req_list *rl_head;
{
	if (rl_head->rl_link != rl_head) return (rl_head->rl_link);
	else return ((struct req_list *)0);
}


/*	req_list_new()
*
*	Make a new req_list descriptor, adds to list at the tail.
* This makes sure that request order is preserved.
* Returns the pointer to the new item so that it can be updated/filled in 
* Gets it from the free list if possible
*
*/

struct req_list *
req_list_new (rl_head)
struct req_list *rl_head;
{
	struct req_list *rlp;

	if (reqfree) {
		rlp = req_list_tail (reqfreelist);  /* get of tail of free list */
		if (!rlp) return (rlp);

		LISTDELETE(rlp, rl_link, rl_rlink); /* detaches from free list */
		reqfree--;

		LISTPUTBEFORE(rl_head, rlp, rl_link, rl_rlink); /* adds to new tail */
		return (rlp);
    }
	/* else allocate it etc etc */
	if (!(rlp = TALLOC(1, struct req_list, 0))) {
		fprintf(stderr, "req_list_new() can't get memory\n");
		exit(1);
	}
	LISTPUTBEFORE(rl_head, rlp, rl_link, rl_rlink);

	return rlp;
}

/*	req_list_new_element_only()
*
*	Make a new req_list descriptor does not add to *any* list.
* Returns the pointer to the new item so that it can be updated/filled in 
*
* Caller must make sure they add to a list or free this later!
*
*/

struct req_list *
req_list_new_element_only ()
{
  struct req_list *rlp;

	if (reqfree) {
		rlp = req_list_tail (reqfreelist);  /* get of tail of free list */
		if (!rlp) return (rlp);

		LISTDELETE(rlp, rl_link, rl_rlink); /* detaches from free list */
		reqfree--;

		return (rlp);
	}

	/* else no spare on freelist, alloc a new element */

  if (!(rlp = TALLOC(1, struct req_list, 0))) {
    fprintf(stderr, "req_list_new_element_only() can't get memory\n");
    exit(1);
  }
  /* no adding to list */
  /* 	LISTPUTBEFORE(rl_head, rlp, rl_link, rl_rlink); */
  
  return rlp;
}

/* req_list_add_2tail (rl_head, rlp)
 * this routine adds the rlp to the tail of the list 
 * normally we don't do this as req_list_new does it for us
 * but req_list_new_element_only doesn't..
 * this is here for the persistant reqs calls
 * which don't know which list until the last second
 *
 */

void req_list_add_2tail (rl_head, rlp)
struct req_list *rl_head;
struct req_list *rlp;
{
	LISTPUTBEFORE(rl_head, rlp, rl_link, rl_rlink);
}


/*
* Note here we don't change the reqs in list counter. This has to be done by
* the calling routine as it will have access to this (being as its in the
* com_list structures anyway).
*
*/
void
req_list_free(rlp)
	struct req_list *rlp;
{
	LISTDELETE(rlp, rl_link, rl_rlink); /* detach it */

	if (reqfree<reqmaxfree) { /* we can add it back to the free list */
	       LISTPUTBEFORE(reqfreelist, rlp, rl_link, rl_rlink);
		   rlp->rl_req_handle = MPI_REQUEST_NULL;
		   reqfree++;
	}
	else 
		FREE(rlp);	/* can do nothing with it, so free it up */
}

/* call that frees all but the empty head element of the list */
void req_list_free_all (struct req_list* rl_head)
{
  struct req_list *rlp;

  if (!rl_head) return;	/* if bad list return rather than deadlock */

  /* loop taking the head and freeing it, when no head, return */
  while (1) {
    rlp = req_list_head (rl_head);
    if (rlp) req_list_free (rlp);
    else 
      return;
    	
  }
}


/*
* Note here we don't change the reqs in list counter. This has to be done by
* the calling routine as it will have access to this (being as its in the
* com_list structures anyway).
*
* this routine detaches the element from the list BUT does not free the memory
*
*/
void
req_list_detach(rlp)
	struct req_list *rlp;
{
	LISTDELETE(rlp, rl_link, rl_rlink);
	rlp->rl_link = rlp->rl_rlink = NULL;
}



void req_list_destroy (rl_head)
struct req_list *rl_head;
{
  struct req_list *rlp;

  if (!rl_head) return;	/* if bad list return rather than deadlock */

  /* loop taking the head and freeing it, when no head, throw empty */
  /* record place holder away as well and then return */
  while (1) {
    rlp = req_list_head (rl_head);
    if (rlp) req_list_free (rlp);
    else {
      _FREE((char *) rl_head);
      return;
    }	
  }
}



#ifdef DEFUNCT
/*	req_list_find_by_header (head, rank, tag, comm)
*
* This routine takes an incomming messages tag and senders rank 
* and matches it to a request if one exists!
*
* Handles wildcards correctly. (Although this should be done higher up
* to save on stack push/pulls and a few ifs + cache misses etc etc)
*
*
*/

struct req_list *
req_list_find_by_header (rl_head, in_rank, in_tag, in_comm)
struct req_list * rl_head;
	int in_rank;
	int in_tag;
	int in_comm;
{
	struct req_list *rlp;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {

      if (in_comm==rlp->rl_req_com) { /* then pattern match as usual */

		/* if any source and tag and we know the com matches return */
		if((in_tag==MPI_ANY_TAG)&&(in_rank==MPI_ANY_SOURCE))
			return(rlp);

		 /* then search by senders rank */
		if((in_tag==MPI_ANY_TAG)&&(rlp->rl_target_rank==in_rank))
			return rlp;

		/* then search by tag */
		if((in_rank==MPI_ANY_SOURCE)&&(rlp->rl_mpi_tag==in_tag)) 
			return rlp;

		/* else just pattern match on both.. this should be neater TODO[] */
		if ((rlp->rl_mpi_tag==in_tag)&&(rlp->rl_target_rank==in_rank))
			return rlp;

	} /* if matching comm */
	
	/* if none of them, then ops! */	
	return (struct req_list*)0;	/* no match, sorry */
}
#endif /* DEFUNCT */



/*	req_list_find_req_matching_recvhdr (head, inrank, intag, incomm)
*
* This routine takes an incomming messages tag and rank 
* and matches it to a request if one exists!
*
* Handles wildcards correctly. 
* Note check looks backwards as the requests have the ANY flags not
* the inrank and intag. These are real values and not wildcards
*
* GEF UTK Feb03
*/

struct req_list *
req_list_find_req_matching_recvhdr (rl_head, in_rank, in_tag, in_comm)
struct req_list * rl_head;
	int in_rank;
	int in_tag;
	MPI_Comm in_comm;
{
	struct req_list *rlp;

	/* we have to step through all the requests each time to find a match */

	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {

      if (in_comm==rlp->rl_req_com) { /* then pattern match as usual */

		/* this req matches all */ 
	if((rlp->rl_mpi_tag==MPI_ANY_TAG)&&(rlp->rl_target_rank==MPI_ANY_SOURCE)) 
		return(rlp);

	if((rlp->rl_mpi_tag==MPI_ANY_TAG)&&(rlp->rl_target_rank==in_rank))
		return(rlp);

	if((rlp->rl_mpi_tag==in_tag)&&(rlp->rl_target_rank==MPI_ANY_SOURCE))
		return(rlp);

	/* lastly if both tag and rank match */
	if((rlp->rl_mpi_tag==in_tag)&&(rlp->rl_target_rank==in_rank))
		return(rlp);

	  } /* if matching comm */

	} /* for each request */	

	/* if none of them, then ops! */	
	return (struct req_list*)0;	/* no match, sorry */
}

/*
* This routine is used to find requests by their req_id.
* Cancelling messages is an example of this routines use.
*/
struct req_list *
req_list_find_by_req_id (rl_head, req_id)
struct req_list * rl_head;
	int req_id;
{
	struct req_list *rlp;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link)
		if((req_id==rlp->rl_req_handle))
			return( rlp );

	/* if none of them, then ops! */	
	return (struct req_list*)0;	/* no match, sorry */
}

/*
* This routine is used to find requests by their recv gid 
*
*	I.e. I want to flush all messages to the receiver that match this id
*	i.e. a sender calls this routine
*
*/
struct req_list *
req_list_find_by_rgid (rl_head, rgid)
struct req_list * rl_head;
int rgid;
{
	struct req_list *rlp;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link)
		if((rgid==rlp->rl_rgid))
			return( rlp );

	/* if none of them, then ops! */	
	return (struct req_list*)0;	/* no match, sorry */
}

/*
* This routine is used to find requests by their recv gid 
* that do not have their low level connection set to the 'chan'
*
*	I.e. I want to flush all messages to the receiver that match this id
*	i.e. a sender calls this routine
*
*/
struct req_list *
req_list_find_by_rgid_and_notchan (rl_head, rgid, chan)
struct req_list * rl_head;
int rgid;
int chan;
{
	struct req_list *rlp;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link)
		if((rgid==rlp->rl_rgid)&&(rlp->rl_snipe2_conn!=chan))
			return( rlp );

	/* if none of them, then ops! */	
	return (struct req_list*)0;	/* no match, sorry */
}



/*
* This routine is used to find requests by their target/recv gid 
*
*	I.e. I want to flush all messages to the target that match this id
*	and I need to know how many in advance
*/
int 
req_list_howmany_send_to_gid (rl_head, rgid, stopatreq)
struct req_list * rl_head;
MPI_Request stopatreq;

	int rgid;
{
	struct req_list *rlp;
	int cnt=0;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {
		if((rgid==rlp->rl_rgid))
			cnt++;
		if (rlp->rl_req_handle==stopatreq) return(cnt);	
						/* we count all upto & including this request */
		}

	return (cnt);	/* return count */
}


/*
* This routine is used to find requests by their target/recv gid 
*
*	I.e. I want to flush all messages that might come from this target id
*	and I need to know how many in advance
*	as its a recv we might have to count the ANY_SOURCE ops as well
*	if we don't then we can mess up the ordering of messages
*/
int 
req_list_howmany_recv_from_gid (rl_head, sgid, stopatreq)
struct req_list * rl_head;
int sgid;
MPI_Request stopatreq;
{
	struct req_list *rlp;
	int cnt=0;
	
	for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {
		if((sgid==rlp->rl_sgid)||(rlp->rl_target_rank==MPI_ANY_SOURCE))
			cnt++;
		if (rlp->rl_req_handle==stopatreq) break;	/* we count all upto & including this request */
		}

	return (cnt);	/* return count */
}



void
req_list_dump(rl_head)
struct req_list * rl_head;
{
  struct req_list *rlp;

  for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {
    printf("Request Handle %d  ", rlp->rl_req_handle);
    printf("Request ID %ld  ", rlp->rl_req_id);
    printf("Type %d  ", rlp->rl_req_type);
    printf("refcnt %d  ", rlp->rl_req_refcnt);
    if( rlp->rl_status.flags & FTMPI_REQ_FREED ) printf( "freed" );
    printf("Comm %d  ", rlp->rl_req_com);
    printf("posters rank %d  ", rlp->rl_posters_rank);
    printf("target rank %d  ", rlp->rl_target_rank);
    printf("MPI tag %d  ", rlp->rl_mpi_tag);
    printf("MPI recvd tag %d  ", rlp->rl_status.MPI_TAG);
    printf("MPI recvd source %d  ", rlp->rl_status.MPI_SOURCE);
    
    printf("\n");
	printf("Req freelist available %ld max %ld\n", reqfree, reqmaxfree);
  }
}

void
req_list_dump_compact (comment, rl_head, expectedcount)
struct req_list * rl_head;
char *comment;
int expectedcount;
{
  struct req_list *rlp;
  int cnt=0;

  printf("req_list dump of [%s] expected count %d\n", comment, expectedcount);
  if (!rl_head) {
#include "assert.h"
	assert (0);
  }
  for (rlp = rl_head->rl_link; rlp != rl_head; rlp = rlp->rl_link) {
	printf("%3d H %3d T %d refc %d Com %2d rank %4d trank %4d Tag %d RTag %d RS %d\n",
		  cnt, rlp->rl_req_handle, rlp->rl_req_type,
		  rlp->rl_req_refcnt, rlp->rl_req_com, 
		  rlp->rl_posters_rank, rlp->rl_target_rank,
		  rlp->rl_mpi_tag, rlp->rl_status.MPI_TAG, rlp->rl_status.MPI_SOURCE);
	cnt++;
  }
  if (cnt!=expectedcount) 
	 printf("WARNING req_list dump of [%s] expected count %d NOT actual %d\n",
		   comment, expectedcount, cnt);
  
  fflush(stdout);

}

void
req_list_dump_freeinfo ()
{
  printf("Req freelist available %ld max %ld\n", reqfree, reqmaxfree);
}


/* fast function that just return true if it matches the header */

int req_hdr_match (req_rank, req_tag, sender, tag) 
int req_rank, req_tag, sender, tag;

{

	if((req_tag==MPI_ANY_TAG)&&(req_rank==MPI_ANY_SOURCE)) return(1);

	if((req_tag==MPI_ANY_TAG)&&(req_rank==sender)) return (1);

	if((req_rank==MPI_ANY_SOURCE)&&(req_tag==tag)) return (1);

	if((req_rank==sender)&&(req_tag==tag)) return (1);

	return (0);
}
	
