
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_LIB_GROUPS
#define _FT_MPI_LIB_GROUPS

int ftmpi_mpi_group_incl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp);
int ftmpi_mpi_group_range_incl(MPI_Group group,int n, int ranges[][3],
			       MPI_Group * newgroup);
int ftmpi_mpi_group_excl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp);
int ftmpi_mpi_group_intersection ( MPI_Group group1, MPI_Group group2,
				   MPI_Group *group_out );



#endif /*  _FT_MPI_LIB_GROUPS */

