
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* Sep 03 GEF */
/* changed this so that all callers can have access to a freequeue */
/* this queue is shared amoungst any number of user level/lib queues */
/* i.e. no matter how many inits are called, there is just one free queue */

#include <stdlib.h>

/* MPI includes */
#include "mpi.h"

/* local includes */
#ifndef LISTSH
#include "ft-mpi-list.h"
#endif

#ifndef FIFOQUEUE
#include "ft-mpi-fifo.h"
#endif

#if defined(SYSVBFUNC)
#include <memory.h>
#define BZERO(d,n)      memset(d,0,n)
#define BCMP(s,d,n)     memcmp(d,s,n)
#define BCOPY(s,d,n)    memcpy(d,s,n)

#else
#define BZERO(d,n)      bzero(d,n)
#define BCMP(s,d,n)     bcmp(s,d,n)
#define BCOPY(s,d,n)    bcopy(s,d,n)
#endif

#include <stdio.h>	/* just for the dump instructions */
#include <string.h> /* needed for strcmp when checking local,remote names */

#include "debug.h" /* Georges debugging memory routines */


/* struct must be of this form */
/*
 * struct type {
 *		struct type *t_link, *t_rlink;
 *		int t_data0;
 *		int t_data1;
 *        .
 *		int t_dataN;
 * };
 */

/* start data queues off this way */
/* fifo_t*fq_head = 0; */
/* static int current_top_key = 0; */

/* the structures used here, contain an empty first element that links to */
/* itself. */


/* free queue stuff */
static fifo_t* fifofreequeue=NULL;
static long   fifofree=0;
static long   fifomaxfree=0;
static int    fifofreelistinit=0;

/* local prototypes */
void fifo_queue_init_freequeue(int initelements, int maxelements);
fifo_t* fifo_queue_tail (fifo_t* fq_head);
fifo_t* fifo_queue_new (fifo_t* fq_head);
void fifo_queue_free(fifo_t* fq_head, fifo_t* fqp);






/* we need to return this first element so that user can store the head of the
 * queue 
 */

fifo_t *
fifo_queue_init()
{
fifo_t *first;

/* first make a free queue if one is not already existing */
if (!fifofreelistinit) {
   fifo_queue_init_freequeue (10,50);
}

	first = TALLOC(1, fifo_t, 0);
	first->fq_link = first->fq_rlink = first; /* all points to me */

	first->count = 0; /* zero count */
	return (first);
}

/* 
 *  This creates a free queue of fifo elements of length initelements 
 *  
 */
void fifo_queue_init_freequeue(int initelements, int maxelements)
{ 
int i; 
fifo_t*fqp;
	     
   fifofreelistinit=1; /* prevents inf recursion ;) */
   fifofreequeue = fifo_queue_init();
   fifofree = 0;
   fifomaxfree = maxelements;

		 for(i=0;i<initelements;i++)  {
		    /* make a new entry */

			/* cannot use new here as it will get them off the free queue DUD! */
			/* 		    fqp = fifo_queue_new(fifofreequeue); */

			if (!(fqp = TALLOC(1, fifo_t, 0))) {
			           fprintf(stderr, "fifo_queue_init_freequeue() can't get memory\n");
					           exit(-1);
							       }
			LISTPUTBEFORE(fifofreequeue, fqp, fq_link, fq_rlink);


			/* make sure item knows it free */
		    fqp->fq_type = 0;        
			fifofreequeue->count++;	/* not really needed */
			fifofree++;
		}
}


fifo_t* fifo_queue_tail (fifo_t* fq_head)
{
	if (fq_head->fq_rlink != fq_head) return (fq_head->fq_rlink);
	else return ((fifo_t*)0);
}


fifo_t*
fifo_queue_head (fq_head)
fifo_t*fq_head;
{
	if (fq_head->fq_link != fq_head) return (fq_head->fq_link);
	else return ((fifo_t*)0);
}


/*	fifo_queue_new()
*
*	Make a new fifo_queue descriptor, adds to queue at the tail.
* This makes sure that fifouest order is preserved.
* Returns the pointer to the new item so that it can be updated/filled in 
* Gets it from the free queue if possible
*
*/

fifo_t* fifo_queue_new (fifo_t* fq_head)
{
	fifo_t*fqp;

	if (fifofree) {
		fqp = fifo_queue_tail (fifofreequeue);  /* get of tail of free queue */
		if (!fqp) return (fqp);

		LISTDELETE(fqp, fq_link, fq_rlink); /* detaches from free queue */
		fifofreequeue->count--;
		fifofree--;

		LISTPUTBEFORE(fq_head, fqp, fq_link, fq_rlink); /* adds to new tail */
		fq_head->count++;
		return (fqp);
    }
	/* else allocate it etc etc */
	if (!(fqp = TALLOC(1, struct fifo_queue, 0))) {
		fprintf(stderr, "fifo_queue_new() can't get memory\n");
		exit(1);
	}
	LISTPUTBEFORE(fq_head, fqp, fq_link, fq_rlink);
	fq_head->count++;

	return fqp;
}

/*
*
* Free an item from a fifo
* counters are updated here 
*/
void fifo_queue_free(fifo_t* fhead, fifo_t* fqp)
{
	LISTDELETE(fqp, fq_link, fq_rlink); /* detach it */
	fhead->count--;
	

	if (fifofree<fifomaxfree) { /* we can add it back to the free queue */
	       LISTPUTBEFORE(fifofreequeue, fqp, fq_link, fq_rlink);
		   fqp->fq_type = 0;
		   fifofreequeue->count++;
		   fifofree++;
	}
	else  {
		_FREE(fqp);	/* can do nothing with it, so free it up */
	}
}

void fifo_queue_destroy (fq_head)
fifo_t*fq_head;
{
  fifo_t*fqp;

  if (!fq_head) return;	/* if bad queue return rather than deadlock */

  /* loop taking the head and freeing it, when no head, throw empty */
  /* record place holder away as well and then return */
  while (1) {
    fqp = fifo_queue_head (fq_head);
    if (fqp) fifo_queue_free (fq_head, fqp);
    else {
      _FREE ((char *) fq_head);
      return;
    }	
  }
}

/* user level routines
 * These are the only three routines users probably ever need to call 
 *
 * get from queue
 * add to queue
 * count of elements in the queue
 */

long	fifo_queue_count (fifo_t* fq_head)
{
   if (!fq_head) return (-1);	/* error */
   else
	  return (fq_head->count);
}

int	fifo_queue_get (fifo_t* fq_head, void** valp1, void** valp2, void** valp3)
{
   fifo_t *fqp;
   int rtype;

   fqp = fifo_queue_head (fq_head);
   if (!fqp) return (-1);

   /* copy data out and then delete element */
   rtype = fqp->fq_type;
   if (valp1) *valp1 = fqp->fq_val[0];
   if (valp2) *valp2 = fqp->fq_val[1];
   if (valp3) *valp3 = fqp->fq_val[2];

   /* delete element (free it up) */
   fifo_queue_free(fq_head, fqp);

   return (rtype);
}

int		fifo_queue_add (fifo_t* fq_head, int type, void* val1, void* val2, void *val3)
{
   fifo_t *fqp;
    fqp = fifo_queue_new (fq_head);
	if (!fqp) return (-1);
	fqp->fq_type = type;
	fqp->fq_val[0] = val1;
	fqp->fq_val[1] = val2;
	fqp->fq_val[2] = val3;
	fqp->count=0;
	return (0);
}



void
fifo_queue_dump(fq_head)
fifo_t* fq_head;
{
   int i;
  fifo_t*fqp;

  for (fqp = fq_head->fq_link; fqp != fq_head; fqp = fqp->fq_link) {
    printf("Type %2d vals ", fqp->fq_type);
	for (i=0;i<FIFODATAMAX;i++) 
	   printf("%ld:0x%lx ", (long)fqp->fq_val[i], (unsigned long)fqp->fq_val[i]);
    printf("\n");
	printf("Fifo freequeue available %ld max %ld\n", fifofree, fifomaxfree);
  }
}

void
fifo_queue_dump_freeinfo ()
{
	printf("Fifo freequeue available %ld max %ld\n", fifofree, fifomaxfree);
}


