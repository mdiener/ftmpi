
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg    <fagg@cs.utk.edu>	<project lead>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 These are the lower level non-blocking [support] operations
 */

#ifndef _FT_MPI_H_NB_LIB
#define _FT_MPI_H_NB_LIB

/* prototypes of NB ops that do not do additional error checking */

int ftmpi_mpi_irecv (void* buf,int count,MPI_Datatype datatype,int source,
	  					int tag,MPI_Comm comm,MPI_Request *request);

int ftmpi_mpi_isend (void* buf,int count,MPI_Datatype datatype,int dest,
	  					int tag,MPI_Comm comm,MPI_Request *request);

int ftmpi_mpi_test (MPI_Request *request,int *flag,MPI_Status *status);

int ftmpi_mpi_wait (MPI_Request *request,MPI_Status *status);


#endif /*  _FT_MPI_H_NB_LIB */

