#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-op.h"
#include "debug.h"


/*#define _ATB_ERROR -460917 */

#define HAVE_LONG_DOUBLE 1


/*typedef struct _atb_mpi_op {
  MPI_User_function * op;
  int commute;
  int permanent;
}_ATB_MPI_OP;
*/

extern int FTMPI_DDT_B_DATATYPE_TOTAL;


int _ATB_MPI_OP_INIT = 0;
int _ATB_MPI_OP_B_CNT = 0;
int _ATB_MPI_OP_CNT = 0;
int _ATB_MPI_OP_MAX = 16;
_ATB_MPI_OP * _ATB_MPI_OPS_ARRAY = NULL;


typedef struct {
  float re;
  float im;
} s_complex;

typedef struct {
  double re;
  double im;
} d_complex;

#ifdef HAVE_MPI_BYTE 
   byte * ab = (byte *)inoutvec,* bb = (byte  *)invec; 
#endif 

#define _ATB_PTRS \
  int * ai = (int *)inoutvec,* bi = (int  *)invec; \
  unsigned int * aui = (unsigned int *)inoutvec,* bui = (unsigned int *)invec; \
  long * al = (long *)inoutvec,* bl = (long *)invec;\
  unsigned long * aul = (unsigned long *)inoutvec,* bul = (unsigned long *)invec;\
  short * as = (short *)inoutvec,* bs = (short *)invec;\
  unsigned short * aus = (unsigned short *)inoutvec,* bus = (unsigned short *)invec;\
  char * ac = (char *)inoutvec,* bc = (char *)invec;\
  unsigned char * auc = (unsigned char *)inoutvec,* buc = (unsigned char *)invec;

#define _ATB_FL_PTRS \
  float * af = (float *)inoutvec,* bf = (float *)invec;\
  double * ad = (double *)inoutvec,* bd = (double *)invec;\
  long double * ald = (long double *)inoutvec,* bld = (long double *)invec;

#define _ATB_LL_PTRS \
  long long * all = (long long *)inoutvec,* bll = (long long *)invec;

#define _ATB_CPLX_PTRS \
  s_complex * asc = (s_complex *)inoutvec,* bsc = (s_complex *)invec;\
  d_complex * adc = (d_complex *)inoutvec,* bdc = (d_complex *)invec;




typedef struct {
  int  value;
  int  loc;
} _ATB_2int_loctype;

typedef struct {
  int  value;
  int  loc;
} _ATB_2real_loctype;

typedef struct {
  double  value;
  double  loc;
} _ATB_2dp_loctype;


typedef struct {
  float  value;
  int    loc;
} _ATB_floatint_loctype;

typedef struct {
  long  value;
  int    loc;
} _ATB_longint_loctype;

typedef struct {
  short  value;
  int    loc;
} _ATB_shortint_loctype;

typedef struct {
  double  value;
  int     loc;
} _ATB_doubleint_loctype;

/* #if defined(HAVE_LONG_DOUBLE) */
typedef struct {
  long double   value;
  int           loc;
} _ATB_longdoubleint_loctype;
/* #endif */


#define _ATB_SPTRS \
  _ATB_2int_loctype * sai = (_ATB_2int_loctype *)inoutvec,* sbi = (_ATB_2int_loctype  *)invec; \
  _ATB_2real_loctype * sar = (_ATB_2real_loctype *)inoutvec,* sbr = (_ATB_2real_loctype  *)invec; \
  _ATB_2dp_loctype * sadp = (_ATB_2dp_loctype *)inoutvec,* sbdp = (_ATB_2dp_loctype  *)invec; \
  _ATB_longint_loctype * sal = (_ATB_longint_loctype *)inoutvec,* sbl = (_ATB_longint_loctype *)invec;\
  _ATB_shortint_loctype * sas = (_ATB_shortint_loctype *)inoutvec,* sbs = (_ATB_shortint_loctype *)invec;\
  _ATB_floatint_loctype * saf = (_ATB_floatint_loctype *)inoutvec,* sbf = (_ATB_floatint_loctype *)invec;\
  _ATB_doubleint_loctype * sad = (_ATB_doubleint_loctype *)inoutvec,* sbd = (_ATB_doubleint_loctype *)invec;


#define _ATB_LL_SPTRS \
  _ATB_longlongint_loctype * sall = (_ATB_longlongint_loctype *)inoutvec,* sbll = (_ATB_longlongint_loctype *)invec;

#define _ATB_LD_SPTRS \
  _ATB_longdoubleint_loctype * sald = (_ATB_longdoubleint_loctype *)inoutvec,* sbld = (_ATB_longdoubleint_loctype *)invec;


void _atb_mpi_max(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_min(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_sum(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_prod(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_land(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_band(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_lor(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_bor(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_lxor(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_bxor(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_maxloc(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);
void _atb_mpi_minloc(void * invec,void * inoutvec,int * cnt,MPI_Datatype * ddt);


int _FTMPI_OP_ERRNO=MPI_SUCCESS;


void _atb_init_op()
{
  int i;

  if(_ATB_MPI_OP_INIT){
    printf("_ATB_INIT_OP has alrady been run ... returning\n");
    return;
  }
  _ATB_MPI_OPS_ARRAY = (_ATB_MPI_OP *)_MALLOC(sizeof(_ATB_MPI_OP)*_ATB_MPI_OP_MAX);

  if(_ATB_MPI_OPS_ARRAY == NULL){
    printf("ERROR -- COULD NOT ALLOCATE MEMORY FOR BASIC OPS\nEXITING!!!!!\n");
    exit(_ATB_ERROR);
  }

  for(i=0;i<_ATB_MPI_OP_MAX;i++){
    _ATB_MPI_OPS_ARRAY[i].op = NULL;
    _ATB_MPI_OPS_ARRAY[i].commute = -1;
    _ATB_MPI_OPS_ARRAY[i].permanent = 1;
  }


  _ATB_MPI_OPS_ARRAY[MPI_MAX].op = _atb_mpi_max;
  _ATB_MPI_OPS_ARRAY[MPI_MAX].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_MIN].op = _atb_mpi_min;
  _ATB_MPI_OPS_ARRAY[MPI_MIN].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_SUM].op = _atb_mpi_sum;
  _ATB_MPI_OPS_ARRAY[MPI_SUM].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_PROD].op = _atb_mpi_prod;
  _ATB_MPI_OPS_ARRAY[MPI_PROD].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_LAND].op = _atb_mpi_land;
  _ATB_MPI_OPS_ARRAY[MPI_LAND].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_BAND].op = _atb_mpi_band;
  _ATB_MPI_OPS_ARRAY[MPI_BAND].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_LOR].op = _atb_mpi_lor;
  _ATB_MPI_OPS_ARRAY[MPI_LOR].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_BOR].op = _atb_mpi_bor;
  _ATB_MPI_OPS_ARRAY[MPI_BOR].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_LXOR].op = _atb_mpi_lxor;
  _ATB_MPI_OPS_ARRAY[MPI_LXOR].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_BXOR].op = _atb_mpi_bxor;
  _ATB_MPI_OPS_ARRAY[MPI_BXOR].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_MAXLOC].op = _atb_mpi_maxloc;
  _ATB_MPI_OPS_ARRAY[MPI_MAXLOC].commute = -1;
  _ATB_MPI_OP_B_CNT++;

  _ATB_MPI_OPS_ARRAY[MPI_MINLOC].op = _atb_mpi_minloc;
  _ATB_MPI_OPS_ARRAY[MPI_MINLOC].commute = -1;
  _ATB_MPI_OP_B_CNT++;


  _ATB_MPI_OP_CNT =_ATB_MPI_OP_B_CNT;

  _ATB_MPI_OP_INIT = 1;
}


/****************************************************************/
int _atb_op_add(_ATB_MPI_OP * new_op)
{
  int i;

  _ATB_MPI_OP * tmp_array = NULL;
  int org_size = _ATB_MPI_OP_MAX;

  if(_ATB_MPI_OP_CNT == _ATB_MPI_OP_MAX){
    /* ARRRAY IS FULL NEED TO RE ALLOCATE ... */
    _ATB_MPI_OP_MAX *= 2;
    tmp_array = (_ATB_MPI_OP *)_MALLOC(sizeof(_ATB_MPI_OP)*_ATB_MPI_OP_MAX);
    if(tmp_array == NULL){
      printf("BUMMER -- no more space ... WHAT NOW GRAHAM????\n");
      exit(_ATB_ERROR);
    }
    memcpy(tmp_array,_ATB_MPI_OPS_ARRAY,sizeof(_ATB_MPI_OP)*org_size);
    _FREE(_ATB_MPI_OPS_ARRAY);
    _ATB_MPI_OPS_ARRAY = tmp_array;

    memcpy(&_ATB_MPI_OPS_ARRAY[_ATB_MPI_OP_CNT],new_op,sizeof(_ATB_MPI_OP));
    org_size = _ATB_MPI_OP_CNT;
    _ATB_MPI_OP_CNT++;
    return(org_size);
  }
  else { 
    for(i=_ATB_MPI_OP_B_CNT;i<org_size;i++){
      if(_ATB_MPI_OPS_ARRAY[i].op == NULL){
        memcpy(&_ATB_MPI_OPS_ARRAY[i],new_op,sizeof(_ATB_MPI_OP));
        _ATB_MPI_OP_CNT++;
        return(i);
      }
    }
  }
  return(-1);
}
/****************************************************************/
/* function to check whether an op is valid. 
   needed in the reduce-functions.
   EG. March 27 2003 
*/
int _atb_op_ok ( MPI_Op op)
{

  if(_ATB_MPI_OP_INIT != 1){
    _atb_init_op();
  }


  if ( op < 0 || op>= _ATB_MPI_OP_CNT )
    return (_ATB_ERROR);
  else
    return(0);
}
/****************************************************************/
MPI_User_function * _atb_op_get(MPI_Op op)
{

  if(_ATB_MPI_OP_INIT != 1){
    _atb_init_op();
  }

  /* I think _ATB_MPI_OP_CNT is more precise than _ATB_MPI_OP_MAX.
     EG March 27 2003 
  */
  if(op < 0 || op >= _ATB_MPI_OP_CNT){ 
    printf("MPI_OP is an invalid value [%d]\n",op);
    return(NULL);
  }

  /* Ok Now return the fucntion call if any */
  return((MPI_User_function *)_ATB_MPI_OPS_ARRAY[op].op);
}
/*******************************************************************/
int _atb_op_get_commute(MPI_Op op)
{

  if(_ATB_MPI_OP_INIT != 1){
    _atb_init_op();
  }

  /* I think _ATB_MPI_OP_CNT is more precise than _ATB_MPI_OP_MAX.
     EG March 27 2003 
  */
  if(op < 0 || op >= _ATB_MPI_OP_CNT){ 
    printf("MPI_OP is an invalid value [%d]\n",op);
    return(_ATB_ERROR);
  }

  /* Ok Now return the fucntion call if any */
  return(_ATB_MPI_OPS_ARRAY[op].commute);
}


/****************************************************************/
#define _ATB_MPI_MAX(a,b) (((b)>(a))?(b):(a))

void _atb_mpi_max(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
  _ATB_FL_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_MAX(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_MAX(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_MAX(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_MAX(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_MAX(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_MAX(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_MAX(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_MAX(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_MAX(auc[i],buc[i]);
      break;
    case MPI_REAL:
    case MPI_FLOAT:
      for ( i=0; i<len; i++ )
        af[i] = _ATB_MPI_MAX(af[i],bf[i]);
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      for ( i=0; i<len; i++ )
        ad[i] = _ATB_MPI_MAX(ad[i],bd[i]);
      break;
#ifdef HAVE_LONG_DOUBLE
    case MPI_LONG_DOUBLE:
      for ( i=0; i<len; i++ )
        ald[i] = _ATB_MPI_MAX(ald[i],bld[i]);
      break;
#endif
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_max\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_MIN(a,b) (((b)<(a))?(b):(a))

void _atb_mpi_min(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
  _ATB_FL_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_MIN(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_MIN(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_MIN(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_MIN(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_MIN(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_MIN(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_MIN(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_MIN(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_MIN(auc[i],buc[i]);
      break;
    case MPI_REAL:
    case MPI_FLOAT:
      for ( i=0; i<len; i++ )
        af[i] = _ATB_MPI_MIN(af[i],bf[i]);
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      for ( i=0; i<len; i++ )
        ad[i] = _ATB_MPI_MIN(ad[i],bd[i]);
      break;
#ifdef HAVE_LONG_DOUBLE
    case MPI_LONG_DOUBLE:
      for ( i=0; i<len; i++ )
        ald[i] = _ATB_MPI_MIN(ald[i],bld[i]);
      break;
#endif
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_min\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_SUM(a,b) ((a)+(b))

void _atb_mpi_sum(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
  _ATB_CPLX_PTRS
  _ATB_FL_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_SUM(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_SUM(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_SUM(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_SUM(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_SUM(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_SUM(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_SUM(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_SUM(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_SUM(auc[i],buc[i]);
      break;
    case MPI_REAL:
    case MPI_FLOAT:
      for ( i=0; i<len; i++ )
        af[i] = _ATB_MPI_SUM(af[i],bf[i]);
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      for ( i=0; i<len; i++ )
        ad[i] = _ATB_MPI_SUM(ad[i],bd[i]);
      break;
#if defined(HAVE_LONG_DOUBLE)
    case MPI_LONG_DOUBLE:
      for ( i=0; i<len; i++ )
        ald[i] = _ATB_MPI_SUM(ald[i],bld[i]);
      break;
#endif
    case MPI_COMPLEX:
      for ( i=0; i<len; i++ ){
        asc[i].re = _ATB_MPI_SUM(asc[i].re ,bsc[i].re);
        asc[i].im = _ATB_MPI_SUM(asc[i].im ,bsc[i].im);
      }
      break;
    case MPI_DOUBLE_COMPLEX:
      for ( i=0; i<len; i++ ){
        adc[i].re = _ATB_MPI_SUM(adc[i].re ,bdc[i].re);
        adc[i].im = _ATB_MPI_SUM(adc[i].im ,bdc[i].im);
      }
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_sum\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_PROD(a,b) ((a)*(b))

void _atb_mpi_prod(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
  _ATB_CPLX_PTRS
  _ATB_FL_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch(*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_PROD(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_PROD(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_PROD(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_PROD(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_PROD(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_PROD(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_PROD(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_PROD(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_PROD(auc[i],buc[i]);
        break;
    case MPI_REAL:
    case MPI_FLOAT:
      for ( i=0; i<len; i++ )
        af[i] = _ATB_MPI_PROD(af[i],bf[i]);
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      for ( i=0; i<len; i++ )
        ad[i] = _ATB_MPI_PROD(ad[i],bd[i]);
      break;
#ifdef HAVE_LONG_DOUBLE
    case MPI_LONG_DOUBLE:
      for ( i=0; i<len; i++ )
        ald[i] = _ATB_MPI_PROD(ald[i],bld[i]);
      break;
#endif
    case MPI_COMPLEX:
      for ( i=0; i<len; i++ ){
        s_complex c;
        c.re = asc[i].re; c.im = asc[i].im;
        asc[i].re = c.re*bsc[i].re - c.im*bsc[i].im;
        asc[i].im = c.im*bsc[i].re + c.re*bsc[i].im;
      }
      break;
    case MPI_DOUBLE_COMPLEX:
      for ( i=0; i<len; i++ ){
        d_complex c;
        c.re = adc[i].re; c.im = adc[i].im;
        adc[i].re = c.re*bdc[i].re - c.im*bdc[i].im;
        adc[i].im = c.im*bdc[i].re + c.re*bdc[i].im;
      }
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_prod\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_LAND(a,b) ((a)&&(b))

void _atb_mpi_land(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_LOGICAL:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_LAND(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_LAND(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_LAND(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_LAND(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_LAND(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_LAND(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_LAND(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_LAND(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_LAND(auc[i],buc[i]);
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_land\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_BAND(a,b) ((a)&(b))

void _atb_mpi_band(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

#ifdef HAVE_MPI_BYTE
   byte * ab = (byte *)inoutvec,* bb = (byte  *)invec; 
#endif

  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_BAND(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_BAND(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_BAND(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_BAND(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_BAND(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_BAND(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_BAND(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BAND(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_BAND(auc[i],buc[i]);
      break;
    case MPI_BYTE:
#ifdef HAVE_MPI_BYTE
      for ( i=0; i<len; i++ )
        ab[i] = _ATB_MPI_BAND(ab[i],bb[i]);
#else 
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BAND(ac[i],bc[i]);
#endif
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_band\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_LOR(a,b) ((a)||(b))

void _atb_mpi_lor(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

   _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_LOGICAL:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_LOR(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_LOR(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_LOR(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_LOR(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_LOR(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_LOR(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_LOR(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_LOR(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_LOR(auc[i],buc[i]);
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_lor\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_BOR(a,b) ((a)|(b))

void _atb_mpi_bor(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

  _FTMPI_OP_ERRNO=MPI_SUCCESS;

#ifdef HAVE_MPI_BYTE
   byte * ab = (byte *)inoutvec,* bb = (byte  *)invec; 
#endif

  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_BOR(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_BOR(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_BOR(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_BOR(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_BOR(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_BOR(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_BOR(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BOR(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_BOR(auc[i],buc[i]);
      break;
    case MPI_BYTE:
#ifdef HAVE_MPI_BYTE
      for ( i=0; i<len; i++ )
        ab[i] = _ATB_MPI_BOR(ab[i],bb[i]);
#else 
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BOR(ac[i],bc[i]);
#endif
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_bor\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_LXOR(a,b) (((a)&&(!b))||((!a)&&(b)))

void _atb_mpi_lxor(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif
  
  _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch (*ddt){
    case MPI_LOGICAL:
    case MPI_INT:
      for ( i=0; i<len; i++ ){
        ai[i] = _ATB_MPI_LXOR(ai[i],bi[i]);
      }
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_LXOR(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_LXOR(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_LXOR(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_LXOR(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_LXOR(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_LXOR(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_LXOR(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_LXOR(auc[i],buc[i]);
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_lxor\n",*ddt); */
      break;
  }
}


/****************************************************************/
#define _ATB_MPI_BXOR(a,b) ((a)^(b))

void _atb_mpi_bxor(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_PTRS
#ifdef HAVE_LONG_LONG
  _ATB_LL_PTRS
#endif

  _FTMPI_OP_ERRNO=MPI_SUCCESS;

#ifdef HAVE_MPI_BYTE
   byte * ab = (byte *)inoutvec,* bb = (byte  *)invec; 
#endif


  switch (*ddt){
    case MPI_INTEGER:
    case MPI_INT:
      for ( i=0; i<len; i++ )
        ai[i] = _ATB_MPI_BXOR(ai[i],bi[i]);
      break;
    case MPI_UNSIGNED:
      for ( i=0; i<len; i++ )
        aui[i] = _ATB_MPI_BXOR(aui[i],bui[i]);
      break;
    case MPI_LONG:
      for ( i=0; i<len; i++ )
        al[i] = _ATB_MPI_BXOR(al[i],bl[i]);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG_INT:
      for ( i=0; i<len; i++ )
        all[i] = _ATB_MPI_BXOR(all[i],bll[i]);
      break;
#endif
    case MPI_UNSIGNED_LONG:
      for ( i=0; i<len; i++ )
        aul[i] = _ATB_MPI_BXOR(aul[i],bul[i]);
      break;
    case MPI_SHORT:
      for ( i=0; i<len; i++ )
        as[i] = _ATB_MPI_BXOR(as[i],bs[i]);
      break;
    case MPI_UNSIGNED_SHORT:
      for ( i=0; i<len; i++ )
        aus[i] = _ATB_MPI_BXOR(aus[i],bus[i]);
      break;
    case MPI_CHAR:
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BXOR(ac[i],bc[i]);
      break;
    case MPI_UNSIGNED_CHAR:
      for ( i=0; i<len; i++ )
        auc[i] = _ATB_MPI_BXOR(auc[i],buc[i]);
      break;
    case MPI_BYTE:
#ifdef HAVE_MPI_BYTE
      for ( i=0; i<len; i++ )
        ab[i] = _ATB_MPI_BXOR(ab[i],bb[i]);
#else 
      for ( i=0; i<len; i++ )
        ac[i] = _ATB_MPI_BXOR(ac[i],bc[i]);
#endif
      break;
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_bxor\n",*ddt); */
      break;
  }
}


/****************************************************************/
void _atb_mpi_maxloc(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_SPTRS
#ifdef HAVE_LONG_DOUBLE
  _ATB_LD_SPTRS
#endif


  _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch(*ddt){
    case MPI_2INTEGER: 
    case MPI_2INT: 
      for (i=0; i<len; i++) {
        if (sai[i].value == sbi[i].value)
          sai[i].loc = _ATB_MPI_MIN(sai[i].loc,sbi[i].loc);
        else if (sai[i].value < sbi[i].value) {
          sai[i].value = sbi[i].value;
          sai[i].loc   = sbi[i].loc;
        }
      }
      break;
	  case MPI_2REAL:
      for (i=0; i<len; i++) {
        if (sar[i].value == sbr[i].value)
          sar[i].loc = _ATB_MPI_MIN(sar[i].loc,sbr[i].loc);
        else if (sar[i].value < sbr[i].value) {
          sar[i].value = sbr[i].value;
          sar[i].loc   = sbr[i].loc;
        }
      }
      break;
	  case MPI_2DOUBLE_PRECISION:
      for (i=0; i<len; i++) {
        if (sadp[i].value == sbdp[i].value)
          sadp[i].loc = _ATB_MPI_MIN(sadp[i].loc,sbdp[i].loc);
        else if (sadp[i].value < sbdp[i].value) {
          sadp[i].value = sbdp[i].value;
          sadp[i].loc   = sbdp[i].loc;
        }
      }
      break;
    case MPI_FLOAT_INT: 
      for (i=0; i<len; i++) {
        if (saf[i].value == sbf[i].value)
          saf[i].loc = _ATB_MPI_MIN(saf[i].loc,sbf[i].loc);
        else if (saf[i].value < sbf[i].value) {
          saf[i].value = sbf[i].value;
          saf[i].loc   = sbf[i].loc;
        }
      }
      break;
    case MPI_LONG_INT: 
      for (i=0; i<len; i++) {
        if (sal[i].value == sbl[i].value)
          sal[i].loc = _ATB_MPI_MIN(sal[i].loc,sbl[i].loc);
        else if (sal[i].value < sbl[i].value) {
          sal[i].value = sbl[i].value;
          sal[i].loc   = sbl[i].loc;
        }
      }
      break;
    case MPI_SHORT_INT: 
      for (i=0; i<len; i++) {
        if (sas[i].value == sbs[i].value)
          sas[i].loc = _ATB_MPI_MIN(sas[i].loc,sbs[i].loc);
        else if (sas[i].value < sbs[i].value) {
          sas[i].value = sbs[i].value;
          sas[i].loc   = sbs[i].loc;
        }
      }
      break;
    case MPI_DOUBLE_INT: 
      for (i=0; i<len; i++) {
        if (sad[i].value == sbd[i].value)
          sad[i].loc = _ATB_MPI_MIN(sad[i].loc,sbd[i].loc);
        else if (sad[i].value < sbd[i].value) {
          sad[i].value = sbd[i].value;
          sad[i].loc   = sbd[i].loc;
        }
      }
      break;
#if defined(HAVE_LONG_DOUBLE)
    case MPI_LONG_DOUBLE_INT: 
      for (i=0; i<len; i++) {
        if (sald[i].value == sbld[i].value)
          sald[i].loc = _ATB_MPI_MIN(sald[i].loc,sbld[i].loc);
        else if (sald[i].value < sbld[i].value) {
          sald[i].value = sbld[i].value;
          sald[i].loc   = sbld[i].loc;
        }
      }
      break;
#endif
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_maxloc\n",*ddt); */
      break;
  }
}


/****************************************************************/
void _atb_mpi_minloc(void * invec,void * inoutvec,int *cnt, MPI_Datatype * ddt)
{
  int i, len = *cnt;
  _ATB_SPTRS

#ifdef HAVE_LONG_DOUBLE
  _ATB_LD_SPTRS
#endif

  _FTMPI_OP_ERRNO=MPI_SUCCESS;

  switch(*ddt){
    case MPI_2INTEGER: 
    case MPI_2INT: 
      for (i=0; i<len; i++) {
        if (sai[i].value == sbi[i].value)
          sai[i].loc = _ATB_MPI_MIN(sai[i].loc,sbi[i].loc);
        else if (sai[i].value > sbi[i].value) {
          sai[i].value = sbi[i].value;
          sai[i].loc   = sbi[i].loc;
        }
      }
      break;
    case MPI_2REAL: 
      for (i=0; i<len; i++) {
        if (sar[i].value == sbr[i].value)
          sar[i].loc = _ATB_MPI_MIN(sar[i].loc,sbr[i].loc);
        else if (sar[i].value > sbr[i].value) {
          sar[i].value = sbr[i].value;
          sar[i].loc   = sbr[i].loc;
        }
      }
      break;
    case MPI_2DOUBLE_PRECISION: 
      for (i=0; i<len; i++) {
        if (sadp[i].value == sbdp[i].value)
          sadp[i].loc = _ATB_MPI_MIN(sadp[i].loc,sbdp[i].loc);
        else if (sadp[i].value > sbdp[i].value) {
          sadp[i].value = sbdp[i].value;
          sadp[i].loc   = sbdp[i].loc;
        }
      }
      break;
    case MPI_FLOAT_INT: 
      for (i=0; i<len; i++) {
        if (saf[i].value == sbf[i].value)
          saf[i].loc = _ATB_MPI_MIN(saf[i].loc,sbf[i].loc);
        else if (saf[i].value > sbf[i].value) {
          saf[i].value = sbf[i].value;
          saf[i].loc   = sbf[i].loc;
        }
      }
      break;
    case MPI_LONG_INT: 
      for (i=0; i<len; i++) {
        if (sal[i].value == sbl[i].value)
          sal[i].loc = _ATB_MPI_MIN(sal[i].loc,sbl[i].loc);
        else if (sal[i].value > sbl[i].value) {
          sal[i].value = sbl[i].value;
          sal[i].loc   = sbl[i].loc;
        }
      }
      break;
    case MPI_SHORT_INT: 
      for (i=0; i<len; i++) {
        if (sas[i].value == sbs[i].value)
          sas[i].loc = _ATB_MPI_MIN(sas[i].loc,sbs[i].loc);
        else if (sas[i].value > sbs[i].value) {
          sas[i].value = sbs[i].value;
          sas[i].loc   = sbs[i].loc;
        }
      }
      break;
    case MPI_DOUBLE_INT: 
      for (i=0; i<len; i++) {
        if (sad[i].value == sbd[i].value)
          sad[i].loc = _ATB_MPI_MIN(sad[i].loc,sbd[i].loc);
        else if (sad[i].value > sbd[i].value) {
          sad[i].value = sbd[i].value;
          sad[i].loc   = sbd[i].loc;
        }
      }
      break;
#if defined(HAVE_LONG_DOUBLE)
    case MPI_LONG_DOUBLE_INT: 
      for (i=0; i<len; i++) {
        if (sald[i].value == sbld[i].value)
          sald[i].loc = _ATB_MPI_MIN(sald[i].loc,sbld[i].loc);
        else if (sald[i].value > sbld[i].value) {
          sald[i].value = sbld[i].value;
          sald[i].loc   = sbld[i].loc;
        }
      }
      break;
#endif
    default:
      _FTMPI_OP_ERRNO=MPI_ERR_OP;
      /*      printf("\nINVALID DATATYPE %d for _atb_mpi_minloc\n",*ddt); */
      break;
  }
}

