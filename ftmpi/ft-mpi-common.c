
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include "mpi.h"
#include "ft-mpi-db-types.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-sys.h"

#include "ns_lib.h"		
#include "debug.h"
#include <stdio.h>
#include <unistd.h>
#include "snipe_lite.h"

/*
extern int ftmpi_silent;
*/

/* Db records now */


db_record_access_t	db_record_access [DB_FT_MPI_LAST_RECORD]	/* fixed size */

= {                  /* type, name, arg, interval, tag, sockfd, addr, port */ 
  { DB_FT_MPI_STATE,	"FTMPI:state",		1,	50000,     10, -1, 0, 11010 }, /* arg = RUNID */
  { DB_FT_MPI_STARTUP,	"FTMPI:startup",	1,	200000,    20, -1, 0, 12010 }, /* arg = RUNID */
  { DB_FT_MPI_CONN,		"FTMPI:conn",		2,	10000,     30, -1, 0, 13010 }, /* args = RUNID, RANK */
  { DB_FT_MPI_ONFAIL,	"FTMPI:onfail",		1,	500000,	   40, -1, 0, 14010 }, /* arg = RUNID */
  { DB_FT_MPI_RECOVER,	"FTMPI:recover",	2,	500000,	   50, -1, 0, 15010 }, /* args = RUNID, FCOUNT */
  { DB_FT_MPI_RECOVERED,	"FTMPI:recovered",	2,	50000,	   60, -1, 0, 16010 }, /* args = RUNID, FCOUNT */
  { DB_FT_MPI_LEADER,	"FTMPI:leader",	        1,	50000,	   70, -1, 0, 17010 }, /* arg = RUNID  */
  { DB_FT_MPI_HANDLER_INFO,	"FTMPI:handler",	2,	50000,     80, -1, 0, 18010 }, /* args = RUNID, FCOUNT */
};





/* New support functions that will help tidy the code */

/* NOTE DB is designed to handle NS3 protocol (and ring if tone finishes it) */

/* this one return a handle to a db record of type record_type */
/* the record may or maynot need the additional args to create its name */

/* it will try tryloops time with the default sleep values from the record_type list */
/* if it timesout it will return DB_NO_RECORD */

/* if breakonfailure is set and a failure occurs normal state changes will occur */
/* and this routine will return DB_FAILURE_DETECT */

/* Else if a record is found it returns BD_OK */
/* unless the args were wrong and then it would return DB_BAD_ARGS */

/* if handle->NULL we don't attempt to return any record data */


int	ftmpi_poll_for_db_record (handle, myid, db_type, index, record_type, 
	  						arg1, arg2, tryloops, breakonfailure_as)

int *handle;	/* access handle */
int myid;		/* used in case record is private 0=NS_ID_CLIENT_ANON */
int db_type;	/* type of DB, 0=Packed record NSv2, 1=addr:port style NSv1 */
int index;		/* DB_ANY = ANY, else >=0 mean that INDEX only */
int record_type; /* type of DB record */
int arg1, arg2;  /* args used to make certain kinds of records */
int tryloops;	 /* how many times we loop attempting to update the record */
		 /* if DB has no such options and can do a notify exchange, we wait loop*timeout sec */
int breakonfailure_as;	/* socket/or accept socket */
			/* if we detect a failure we stop attempting to do a DB access */
			/* can we call the update state routines before returning an error */

{
int	k, l=0;
int	finished=0;
int 	rc=0;
char	rnametmp[MAX_DB_NAME_LEN];		/* bad.. fixed length record names.. */
int	args;
int	h;					/* as in handle */
int i1, i2;
int r;
int mb;
int lastgen=0;



/* first check args */ 
if ((record_type<0)||(record_type>=DB_FT_MPI_LAST_RECORD)) return (DB_BAD_ARGS);

/* make record name */
args = db_record_access[record_type].db_arg_count;

if (!args) sprintf(rnametmp, "%s", db_record_access[record_type].db_access_name);
if (args==1) sprintf(rnametmp, "%s:%x", db_record_access[record_type].db_access_name, arg1);
if (args==2) sprintf(rnametmp, "%s:%x:%d", db_record_access[record_type].db_access_name, arg1, arg2);

#ifdef REALVERBOSE9
printf("rnametmp %s\n", rnametmp);
#endif
#ifdef REALVERBOSE
printf("db %d rt %d a1 %d a2 %d loops %d breakon %d\n",
		db_type, record_type, arg1, arg2, tryloops, breakonfailure_as );
#endif

/* Now loop until we find record, run out of loops (timeout) or detect a failure */
l = 0;

while (!finished) {	/* main loop */

	/* check for record */

	if (db_type==0) { /* Packed record type */
		ns_open ();
		r = ns_record_get (myid, rnametmp, RECORD_DB_INDEXED, index, &i1, &i2, &mb, &lastgen);
		ns_close ();
		if ((i1>=0)&&(r>=0)) {
			h = mb;
			}
		else {
			h = -1;
			/*
			if (!ftmpi_silent) printf("Record [%s][%d] not found yet!\n", rnametmp, index);
			*/
			}

	}

	else { /* traditional older NS v1 addr:port type */
		ns_open ();
	    r = ns_info (rnametmp, 1, &i1, &i2, NULL, NULL, NULL);
		ns_close ();
		if (r>=0) h = i1;
		else {
			h = -1;
			/*
			if (!ftmpi_silent) printf("!");
			*/
			}
	}


	if (h >=0 ) { /* record found */
		*handle = h;	
		return (DB_OK);
	}

	/* else we didn't find a record */

	/* so we sleep and then check stuff out */

	usleep (db_record_access[record_type].db_retry_interval);

	if (tryloops) 	{
		l++;
		if (l==tryloops) { finished=1; return (DB_NO_RECORD); }
	}

	if (breakonfailure_as) {
		k = pollconn (breakonfailure_as, 0, 1000);	/* check the socket */
												/* any movement and ops */

		if (k>0) { /* we have a failure */
				finished=1; 
				return (DB_FAILURE_DETECT); 
		}
	}	
	/*
			if (!ftmpi_silent) printf(".");
			*/

} /* while not finished */

return (rc);
}

/* ftmpi_unreg_wait_for_db_record - unregister new ftmpi_wait_for db record
@param record_type type of record
@param arg1 argument#1 (depend on type of record)
@param arg2 argument#2 (depend on type of record)
*/ 
int ftmpi_unreg_wait_for_db_record(int record_type, int arg1, int arg2)
{
   int addr,port,tag;
   char recordname[MAX_DB_NAME_LEN];	

   if(db_record_access[record_type].db_callback_sockfd==-1) { /* we not register */
      return -1;
   }
   addr=db_record_access[record_type].db_callback_addr;
   port=db_record_access[record_type].db_callback_port;
   tag=db_record_access[record_type].db_callback_tag;
   switch(db_record_access[record_type].db_arg_count) {
      case 0: sprintf(recordname, "%s", db_record_access[record_type].db_access_name); break;
      case 1: sprintf(recordname, "%s:%x", db_record_access[record_type].db_access_name, arg1); break;
      case 2: sprintf(recordname, "%s:%x:%d", db_record_access[record_type].db_access_name, arg1, arg2); break;
   }

   ns_open();
   ns_record_callback_unregister(addr,port,recordname,tag);
   ns_close();

   closeconn(db_record_access[record_type].db_callback_sockfd);
   db_record_access[record_type].db_callback_sockfd=-1;
   return 0;
}

/* ftmpi_wait_for_db_record - wait for DB record
@param recordtype type of record
@param arg1 argument#1 (depend on type of record)
@param arg2 argument#2 (depend on type of record)
@param lastgen last generation
@param breakonerror breakonerror socket
@param flag (0 = reg/unreg eachtime, 1= leave registered)
*/
int ftmpi_wait_for_db_record(int record_type, int arg1, int arg2, int lastgen, int breakonerror, int flag)
{
    char rnametmp[MAX_DB_NAME_LEN];		/* bad.. fixed length record names.. */
    int rc[5];
    int ret;
    int callback_sockfd;
    int callback_addr, callback_port;
    int tag;
    int mb;
    int ntag;
    int from;
    int new_sockfd;
    
    callback_port = db_record_access[record_type].db_callback_port;
    callback_sockfd = db_record_access[record_type].db_callback_sockfd ;
    tag = db_record_access[record_type].db_callback_tag;

    if ((record_type<0)||(record_type>=DB_FT_MPI_LAST_RECORD)) return (DB_BAD_ARGS);

#ifdef VERBOSE
       printf("[%s:%d] CALLBACK_SOCKFD is %d\n",__FILE__,__LINE__,callback_sockfd);
#endif

    if(callback_sockfd<0) { /* need register */
        callback_sockfd = init_lsocket2 (&callback_port);
	if(callback_sockfd < 0) {
            return (DB_BAD_CONN);
	}
	db_record_access[record_type].db_callback_port=callback_port;
	db_record_access[record_type].db_callback_addr=get_my_addr();
	callback_addr = db_record_access[record_type].db_callback_addr;

        switch(db_record_access[record_type].db_arg_count) {
           case 0: sprintf(rnametmp, "%s", db_record_access[record_type].db_access_name); break;
	   case 1: sprintf(rnametmp, "%s:%x", db_record_access[record_type].db_access_name, arg1); break;
	   case 2: sprintf(rnametmp, "%s:%x:%d", db_record_access[record_type].db_access_name, arg1, arg2); break;
	}

	/* register for event of slot 0 only */
	mb = -1; /* just in case */
        if(flag==0) { /* auto unregister */
	    ns_open();
	    mb=ns_record_callback_register(AUTOUNREG | EXIST,callback_addr,callback_port,rnametmp,0,tag,lastgen);
	    ns_close();
#ifdef VERBOSE
            printf("[%s:%d] AUTO-UN We got mb is %d\n",__FILE__,__LINE__,mb); 
#endif
	} else {
	    ns_open();
	    mb=ns_record_callback_register(DEFAULT | EXIST,callback_addr,callback_port,rnametmp,0,tag,lastgen);
	    ns_close();
	    db_record_access[record_type].db_callback_sockfd=callback_sockfd;
#ifdef VERBOSE
            printf("[%s:%d] Leave reg: We got mb is %d\n",__FILE__,__LINE__,mb); 
#endif
	}
	if(mb>=0) return mb;  /* The record is already there */ 
    }

    while(1) {
	if (breakonerror) {
	   ret = pollconn (breakonerror, 0, 10);	/* check the socket */
	   if (ret>0) { /* we have a failure */
	       return (DB_FAILURE_DETECT); 
	   }
	}	
	/* printf("BEFORE POLL\n"); */
	ret = pollconn (callback_sockfd, 0, 100);	/* check the socket */
	/* printf("AFTER POLL\n"); */
	if (ret>0) {
           ntag=5;
	   new_sockfd=allowconn(callback_sockfd,0,NULL);
	   recv_pkmesg (new_sockfd, &from, &ntag, rc, &mb); /* do the recv */
	   closeconn(new_sockfd);
	   if(rc[1]!=tag) {
               return (DB_BAD_CONN);
	   } 
	   if(rc[2]==203) { /* i.e delete record */
               continue; /* If someone delete the record. We pretend like nothing happen. */
	   }
	   if(flag==0) { /* auto unregister */
	       db_record_access[record_type].db_callback_sockfd=-1;
               closeconn(callback_sockfd);
	   }
	   return mb;
	}
	/* actually a sleep here only means that we wait a while before */
	/* reading a socket.. so we ignore this and the poll timeouts prevent */
	/* us spinning too much above. */
/* 	usleep (db_record_access[record_type].db_retry_interval); */
    }
}

int    init_lsocket2 (tp)
int *tp;	/* target port */
{
int sl;
int p0;
int p;

		if (!tp) return (-1);	/* must be able to report the port */
		if (*tp<1024) p0 = 0;	/* i.e. give me a port :) */
		else
			p0 = *tp;

        p = setportconn (&sl, p0, 2000);
        if (p>0) {
            *tp = p;
            return (sl);
        }

        else 
			return (-1);
}

