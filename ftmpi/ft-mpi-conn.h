
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg    <fagg@cs.utk.edu>	<project lead>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_CONN
#define _FT_MPI_H_CONN
/* low level connection library headerfile */

/* low level header */
typedef struct { 

/* first part type identifier */
	unsigned short type;		

/* mixed mode info */
	int fromgid;
	int togid;
	int datasize;

/* system message counters / ordering info */
	int msgid;

/* MPI specific */
	unsigned short com;
	unsigned short from;
	unsigned short to;
	int tag;
} msg_hdr_t;

/* low level header2 */
/* this is used to combine writes for v small messages */
#define FTMPI_HDR_PAYLOAD (512-sizeof(msg_hdr_t))
typedef struct { 
  	msg_hdr_t	hdr;
	char payload[FTMPI_HDR_PAYLOAD];
} msg_hdr_t2;

/* how many fields/ints does it contain */
#define msg_hdr_fields 9

/* macro of message header size */
#define msg_hdr_size sizeof(msg_hdr_t)
#define msg_hdr_size2 sizeof(msg_hdr_t2)

typedef struct {
	int inuse; /* entry free? */

	int gid; /* of the other end */

	/* TCP socket comms info */
	unsigned long addr;
	int ports[2];	/* their ports, accept, state */
						/* we only use the first */
	int sock; /* socket */

	int connected;		/* basic connection */


	/* basic connect stats */
	int send_msgcnt;
	int recv_msgcnt;

	/* basic connection states */
	int sendallowed;
	int initialack;
	int error;

	/* basic flow control */
	int sentxon;
	int sentxoff;

	/* low level NB SNIPE2 and OVM Driver state */
	int	snipe2chan;		/* snipe2 / device connection structure */

	/* Additional misc SNIPE2 status */
	/* this is required due to the way re interoperate the flowcontrol */
	/* and recving of data */

	/* low level request info */
	/* formultipart recvs */
	/* recvs are used for both flowcontrol and data handling */
	snipe2_msg_list_t* recvreqptr;
	int recvreq;
	/* sends are only used for flow control when forced */
	snipe2_msg_list_t* sendreqptr;
	int sendreq;
	msg_hdr_t xhdr;

	/* for truncated data */
	snipe2_msg_list_t* recvtruncreqptr;
	int recvtruncreq;
	int recvtruncbufid;

	/* high level */
	int recvstate;
	int recvtruncstate;
	int sendstate; /* used for flow control */


	/* message header we use / recvd */
	msg_hdr_t recvhdr;

	/* cached req info or unexpected list info */
	/* if we post a recv data call.. what is it for? a NB req or unexpecetd recv? */
	/* we cache here as the call back in SNIPE2 only returns one user value and we might need 3 of them.. grr */
	struct req_list *rlp;
	struct msg_list *mlp;

} conn_info_t;



/* some prototypes of shared functions */

int ftmpi_conn_init ();
int find_free_conn ();
int conn_clr_entry (int);
int find_conn_entry_by_gid (int);
int remove_conn_entry_by_gid (int);
int remove_conn_entry_not_in_list (int *, int);
int ftmpi_conn_fillin_conn_info (int *, int *, int *, int *, int);
int conn_entry_dump (int);
int conn_mkconn (int);	/* defunct */
int conn_mkconn2 (int, int);
int ftmpi_conn_set_local (int, int, int, int, int);
int conn_handle_con_attempts (int, int, int *, int *);
int conn_check_for_event (int);
int conn_handle_req (int, int, int);
int conn_send_reply (int, int);
int conn_send_req (int, int);
int conn_fail (int);
int conn_send_data (int, char*, int, msg_hdr_t*);
int conn_check_for_incomming (int, int, msg_hdr_t*);
int conn_dump_counters ();
int conn_recv_mpi_hdr (int, msg_hdr_t *, int *, int, int);
int conn_recv_mpi_hdr_any (int *, int , msg_hdr_t *, int *);
int conn_poll_and_recv_mpi_hdr_any (int *, int , msg_hdr_t *, int *);
int conn_flush (int, int, int);
int conn_get_data (int, int, char *, int);
int conn_get_msg_hdr (int, msg_hdr_t *);
int conn_get_recvptrs (int con, struct req_list **rptr, struct msg_list **mptr);
int conn_connection_event_matcher ();
int conn_restart_all_coms ();
int conn_stop_all_coms ();
int conn_stop_and_flush ();
int conn_halt_all_coms ();
int ft_mpi_conn_any_error ();
void conn_set_conn_fail_sends_events_flag (int sendeventonfail);


/* temporary / debug / testing */
int conn_all_mkconn (int *gids, int ext);
int	conn_snipe2_getchan (int con, int* snipe2chan);
int conn_send_allowed (int con);
int conn_connected (int con);
int conn_snipe2_disable_newconnections_and_events ();
int conn_status (int con, int *connected, int *sendallowed);
int conn_hexdump (char *ptr, int len);
int conn_debug_recv (char* ptr, int len);
int conn_debug_send (char* ptr, int len);

int conn_snipe2_post_recvtrunc (int con, long len);
int conn_snipe2_post_recvhdr (int con);
int conn_snipe2_setup_event_callback (int es);
int conn_snipe2_setup_newconnection_callback (int cs);
int conn_snipe2_post_recvdata (int con, char* buf, long len, struct req_list *rlp, struct msg_list *mlp);

int conn_snipe2_setup_callbacks ();

/* arg passing */
msg_hdr_t* conn_get_recvhdr (int con);




#define make_hdr(h,h1,h2,h3,h4,h5,h6,h7,h8,h9) \
{ \
(h).type = (unsigned short) h1; \
(h).fromgid = h2; \
(h).togid = h3; \
(h).datasize = h4; \
(h).msgid = h5; \
(h).com = (unsigned short) h6; \
(h).from = (unsigned short) h7; \
(h).to = (unsigned short) h8; \
(h).tag = h9; \
}

#define get_hdr(h,h1,h2,h3,h4,h5,h6,h7,h8,h9) \
{ \
h1 = (int) (h).type; \
h2 = (h).fromgid; \
h3 = (h).togid; \
h4 = (h).datasize; \
h5 = (h).msgid; \
h6 = (int) (h).com; \
h7 = (int) (h).from; \
h8 = (int) (h).to; \
h9 = (h).tag; \
}

#define trans_hdr_to_network(h)\
{ \
(h).type = htons ( (h).type ); \
(h).fromgid = htonl ((h).fromgid); \
(h).togid = htonl ((h).togid); \
(h).datasize = htonl ((h).datasize); \
(h).msgid = htonl ((h).msgid); \
(h).com = htons ((h).com); \
(h).from = htons ((h).from); \
(h).to = htons ((h).to); \
(h).tag = htonl ((h).tag); \
}

#define trans_hdr_from_network(h)\
{ \
(h).type = ntohs ((h).type); \
(h).fromgid = ntohl ((h).fromgid); \
(h).togid = ntohl ((h).togid); \
(h).datasize = ntohl ((h).datasize); \
(h).msgid = ntohl ((h).msgid); \
(h).com = ntohs ((h).com); \
(h).from = ntohs ((h).from); \
(h).to = ntohs ((h).to); \
(h).tag = ntohl ((h).tag); \
}

/* message types used */
#define XON 1111     /* sends alllowed to me */
#define XOFF 9999    /* NO more sends please */
#define XALT 7777    /* already connecting to you */
#define XHDR 2222    /* sending data to you next */
#define XCTL 3333    /* higher level flow control messages */
                    /* i.e. nb or long message requests */





#endif /*   _FT_MPI_H_CONN */

