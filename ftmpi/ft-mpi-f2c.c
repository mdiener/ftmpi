
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




#include "ft-mpi-f2c.h"
#include "ft-mpi-fprot.h"

#define MPI_Info int
#define MPI_Offset int

int  FTMPI_KEYVAL_FORTRAN = 0;



/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Attr_delete(MPI_Comm comm,int keyval) */
void mpi_attr_delete_(int * comm,int * keyval,int * __ierr)
{
  *__ierr = MPI_Attr_delete((MPI_Comm)(*((int *)comm)),
			    (*((int *)keyval)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*int MPI_Attr_get(MPI_Comm comm,int keyval,void *attribute_val,
  int *flag)*/
void mpi_attr_get_(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr)
{
  int *cval;

  *__ierr = MPI_Attr_get((MPI_Comm)(*((int *)comm)),(*((int *)keyval)),
			 (void*)&cval,(int *)ToPtr(flag));
  if ( *flag )
    {
      switch (*keyval)
	{
	case MPI_TAG_UB:	
	case MPI_IO :
	case MPI_HOST:
	case MPI_WTIME_IS_GLOBAL:
	case MPI_LASTUSEDCODE:
	case FTMPI_ERROR_FAILURE:
	case FTMPI_NUM_FAILED_PROCS:
	  *attribute_val = (int)(*cval);
	  break;
	default:
	  *attribute_val = (int)(cval); 
	  break;
	}	  

    }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Attr_put(MPI_Comm comm,int keyval,void * attribute_val) */
void mpi_attr_put_(int *comm,int *keyval,int *attribute_val,int *__ierr)
{
  *__ierr = MPI_Attr_put((MPI_Comm)(*((int *)comm)),(*((int *)keyval)),
			 (void *)((int)*attribute_val));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_coords(MPI_Comm comm,int rank,int maxdims,int *coords)*/
void mpi_cart_coords_(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr)
{
  *__ierr = MPI_Cart_coords((MPI_Comm)(*((int *)comm)),(*((int *)rank)),
			    (*((int *)maxdims)),(int *)ToPtr(coords));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_create(MPI_Comm comm_old,int ndims,int *dims,int *periods,
   int reorder,MPI_Comm * comm_cart) */
void mpi_cart_create_(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr)
{
  *__ierr = MPI_Cart_create((MPI_Comm)(*((int *)comm_old)),
			    (*((int *)ndims)),
			    (int *)ToPtr(dims),(int *)ToPtr(periods),
			    (*((int *)reorder)),
			    (MPI_Comm *)ToPtr(comm_cart));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cartdim_get(MPI_Comm comm,int * ndims) */
void mpi_cartdim_get_(int * comm,int * ndims,int * __ierr)
{
  *__ierr = MPI_Cartdim_get((MPI_Comm)(*((int *)comm)),
			    (int *)ToPtr(ndims));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_get(MPI_Comm comm,int maxdims,int * dims,int * periods,
   int * coords) */
void mpi_cart_get_(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr)
{
  *__ierr = MPI_Cart_get((MPI_Comm)(*((int *)comm)),(*((int *)maxdims)),
			 (int *)ToPtr(dims),(int *)ToPtr(periods),
			 (int *)ToPtr(coords));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_map(MPI_Comm comm,int ndims,int * dims,int * periods,
   int * newrank) */
void mpi_cart_map_(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr)
{
  *__ierr = MPI_Cart_map((MPI_Comm)(*((int *)comm)),(*((int *)ndims)),
			 (int *)ToPtr(dims),(int *)ToPtr(periods),
			 (int *)ToPtr(newrank));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_rank(MPI_Comm comm,int * coords,int * rank) */
void mpi_cart_rank_(int * comm,int * coords,int * rank,int * __ierr)
{
  *__ierr = MPI_Cart_rank((MPI_Comm)(*((int *)comm)),(int *)ToPtr(coords),
			  (int *)ToPtr(rank));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_shift(MPI_Comm comm,int direction,int disp,
   int *rank_source, int * rank_dest) */
void mpi_cart_shift_(int *comm,int *direction,int *disp,
		     int *rank_source,
		     int *rank_dest,int *__ierr)
{
  *__ierr = MPI_Cart_shift((MPI_Comm)(*((int *)comm)),
			   (*((int *)direction)),
			   (*((int *)disp)),(int *)ToPtr(rank_source),
			   (int *)ToPtr(rank_dest));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cart_sub(MPI_Comm comm,int *remain_dims,MPI_Comm *newcomm)*/
void mpi_cart_sub_(int * comm,int * remain_dims,int * newcomm,int *__ierr)
{
  *__ierr = MPI_Cart_sub((MPI_Comm)(*((int *)comm)),
			 (int *)ToPtr(remain_dims),
			 (MPI_Comm *)ToPtr(newcomm));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Dims_create(int nnodes,int ndims,int * dims) */
void mpi_dims_create_(int * nnodes,int * ndims,int * dims,int * __ierr)
{
  *__ierr = MPI_Dims_create((*((int *)nnodes)),(*((int *)ndims)),
			    (int *)ToPtr(dims));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Errhandler_create(MPI_Handler_function *function,
   MPI_Errhandler *errhandler) */
void mpi_errhandler_create_(int * function,int * errhandler,int * __ierr)
{
  *__ierr = MPI_Errhandler_create((MPI_Handler_function *)ToPtr(function),
				  (MPI_Errhandler *)ToPtr(errhandler));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Errhandler_free(MPI_Errhandler *errhandler) */
void mpi_errhandler_free_(int * errhandler,int * __ierr)
{
  *__ierr = MPI_Errhandler_free((MPI_Errhandler *)ToPtr(errhandler));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Errhandler_get(MPI_Comm comm,MPI_Errhandler *errhandler) */
void mpi_errhandler_get_(int * comm,int *errhandler,int * __ierr)
{
  *__ierr = MPI_Errhandler_get((MPI_Comm)(*((int *)comm)),
			       (MPI_Errhandler *)ToPtr(errhandler));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Errhandler_set(MPI_Comm comm,MPI_Errhandler errhandler) */
void mpi_errhandler_set_(int * comm,int * errhandler,int * __ierr)
{
  *__ierr = MPI_Errhandler_set((MPI_Comm)(*((int *)comm)),
			       (MPI_Errhandler)(*((int *)errhandler)));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Error_class(int errorcode,int * errorclass) */
void mpi_error_class_(int * errorcode,int * errorclass,int * __ierr)
{
  *__ierr = MPI_Error_class((*((int *)errorcode)),
			    (int *)ToPtr(errorclass));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Error_string(int errorcode,char * string,int * resultlen) */
void mpi_error_string_(int * errorcode,char * string,int * resultlen,
		       int * __ierr)
{
  *__ierr = MPI_Error_string((*((int *)errorcode)),(char *)ToPtr(string),
			     (int *)ToPtr(resultlen));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graph_create(MPI_Comm comm_old,int nnodes,int *index,
   int *edges, int reorder,MPI_Comm * comm_graph) */
void mpi_graph_create_(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr)
{
  *__ierr = MPI_Graph_create((MPI_Comm)(*((int *)comm_old)),
			     (*((int *)nnodes)),(int *)ToPtr(index),
			     (int *)ToPtr(edges),(*((int *)reorder)),
			     (MPI_Comm *)ToPtr(comm_graph));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graphdims_get(MPI_Comm comm,int * nnodes,int * nedges) */
void mpi_graphdims_get_(int *comm,int *nnodes,int *nedges,int *__ierr)
{
  *__ierr = MPI_Graphdims_get((MPI_Comm)(*((int *)comm)),
			      (int *)ToPtr(nnodes),(int *)ToPtr(nedges));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graph_get(MPI_Comm comm,int maxindex,int maxedges,int *index,
   int * edges) */
void mpi_graph_get_(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr)
{
  *__ierr = MPI_Graph_get((MPI_Comm)(*((int *)comm)),(*((int *)maxindex)),
			  (*((int *)maxedges)),(int *)ToPtr(index),
			  (int *)ToPtr(edges));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graph_map(MPI_Comm comm,int nnodes,int * index,int * edges,
   int * newrank) */
void mpi_graph_map_(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr)
{
  *__ierr = MPI_Graph_map((MPI_Comm)(*((int *)comm)),(*((int *)nnodes)),
			  (int *)ToPtr(index),(int *)ToPtr(edges),
			  (int *)ToPtr(newrank));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graph_neighbors_count(MPI_Comm comm,int rank,int *neighbors)*/
void mpi_graph_neighbors_count_(int * comm,int * rank,int * neighbors,
				int * __ierr)
{
  *__ierr = MPI_Graph_neighbors_count((MPI_Comm)(*((int *)comm)),
				      (*((int *)rank)),
				      (int *)ToPtr(neighbors));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Graph_neighbors(MPI_Comm comm,int rank,int maxneighbors,
   int * neighbors) */
void mpi_graph_neighbors_(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr)
{
  *__ierr = MPI_Graph_neighbors((MPI_Comm)(*((int *)comm)),
				(*((int *)rank)),(*((int *)maxneighbors)),
				(int *)ToPtr(neighbors));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_compare(MPI_Group group1,MPI_Group group2,int *result)*/
void mpi_group_compare_(int * group1,int *group2,int *result,int *__ierr)
{
  *__ierr = MPI_Group_compare((MPI_Group)(*((int *)group1)),
			      (MPI_Group)(*((int *)group2)),
			      (int *)ToPtr(result));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_difference(MPI_Group group1,MPI_Group group2,
   MPI_Group *newgroup) */
void mpi_group_difference_(int *group1,int *group2,int *newgroup,
			   int * __ierr)
{
  *__ierr = MPI_Group_difference((MPI_Group)(*((int *)group1)),
				 (MPI_Group)(*((int *)group2)),
				 (MPI_Group *)ToPtr(newgroup));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_excl(MPI_Group group,int n,int *ranks,
   MPI_Group *newgroup)*/
void mpi_group_excl_(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr)
{
  *__ierr = MPI_Group_excl((MPI_Group)(*((int *)group)),(*((int *)n)),
			   (int *)ToPtr(ranks),
			   (MPI_Group *)ToPtr(newgroup));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_free(MPI_Group *group); */
void mpi_group_free_(int * group,int * __ierr)
{
 *__ierr = MPI_Group_free((MPI_Group *)ToPtr(group));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_incl(MPI_Group group,int n,int *ranks, 
   MPI_Group *newgroup); */
void mpi_group_incl_(int * group,int * n,int * ranks, int * newgroup,
		    int * __ierr)
{
 *__ierr = MPI_Group_incl((MPI_Group)(*((int*)group)),(*((int*)n)),
			  (int *)ToPtr(ranks),
			  (MPI_Group *)ToPtr(newgroup));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_intersection(MPI_Group group1,MPI_Group group2,
   MPI_Group *newgroup) */
void mpi_group_intersection_(int * group1,int * group2,int * newgroup,
			     int * __ierr)
{
  *__ierr = MPI_Group_intersection((MPI_Group)(*((int *)group1)),
				   (MPI_Group)(*((int *)group2)),
				   (MPI_Group *)ToPtr(newgroup));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_group_rank_ (int *group, int *rank, int *__ierr)
{
  *__ierr = MPI_Group_rank ( (MPI_Group)(*((int*)group)), rank );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_group_size_ (int *group, int *size, int *__ierr)
{
  *__ierr = MPI_Group_size ( (MPI_Group)(*((int*)group)), size );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_range_excl(MPI_Group group,int n,int ranges[][3],
   MPI_Group *newgroup) */
void mpi_group_range_excl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) 
{
  *__ierr = MPI_Group_range_excl((MPI_Group)(*((int *)group)),
				 (*((int *)n)),
				 (int (*)[3])ranges,
				 (MPI_Group *)ToPtr(newgroup)); 
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_range_incl(MPI_Group group,int n,int ranges[][3],
   MPI_Group *newgroup) */
void mpi_group_range_incl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) 
{
  *__ierr = MPI_Group_range_incl((MPI_Group)(*((int *)group)),
				 (*((int *)n)),
				 (int (*)[3]) ranges,
				 (MPI_Group *)ToPtr(newgroup));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_translate_ranks(MPI_Group group1,int n,int * ranks1,
   MPI_Group group2,int * ranks2) */
void mpi_group_translate_ranks_(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr)
{
  *__ierr = MPI_Group_translate_ranks((MPI_Group)(*((int *)group1)),
				      (*((int *)n)),
				      (int *)ToPtr(ranks1),
				      (MPI_Group)(*((int *)group2)),
				      (int *)ToPtr(ranks2));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Group_union(MPI_Group group1,MPI_Group group2,
   MPI_Group *newgroup) */
void mpi_group_union_(int *group1,int *group2,int *newgroup,int *__ierr)
{
  *__ierr = MPI_Group_union((MPI_Group)(*((int *)group1)),
			    (MPI_Group)(*((int *)group2)),
			    (MPI_Group *)ToPtr(newgroup));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Keyval_create(MPI_Copy_function *copy_fn, 
   MPI_Delete_function *delete_fn,int * keyval,void * extra_state) */
void mpi_keyval_create_(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr)
{
  /* This is a global variable checked deep in ftmpi whether the
     MPI_Keyval_create call is coming from fortran or not. The reason
     for this is, that when calling the copy-functions provided by the
     user, the arguments to this function have to be passed
     differently depending on whether it is implemented in fortran or
     in C.

     I know this solution is not the "non plus ultra" solution, but
     the alternative would be to introduce an 'own' version of
     Keyval_create with an additional argument which indicates whether
     it is a fortran or a c-routine. I preferr currently this version,
     howver, the other might be realized later on.

     EG Feb 27 2003 */

  FTMPI_KEYVAL_FORTRAN = 1;
  *__ierr = MPI_Keyval_create((MPI_Copy_function *)ToPtr(copy_fn),
			      (MPI_Delete_function *)ToPtr(delete_fn),
			      (int *)ToPtr(keyval),
			      (void *)ToPtr(extra_state)); 
  FTMPI_KEYVAL_FORTRAN = 0;

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Keyval_free(int * keyval) */
void mpi_keyval_free_(int * keyval,int * __ierr)
{
  *__ierr = MPI_Keyval_free((int *)ToPtr(keyval));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_NULL_COPY_FN(MPI_Comm a,int b,void * c,void * d,void *f,
   int *h)*/

void mpi_null_copy_fn_(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr)
{
  *f = 0;
  *__ierr = MPI_SUCCESS;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_dup_fn_ ( int *comm, int *k, int *exs, int *ain, int *aout, 
		       int *f, int *__ierr )
{
  *f      = 1;
  *aout   = *ain;
  *__ierr = MPI_SUCCESS;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_NULL_DELETE_FN(MPI_Comm a,int b,void * c,void * d ) */
void mpi_null_delete_fn_(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr)
{
  *__ierr = MPI_SUCCESS;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Op_create(MPI_User_function *func,int commute, MPI_Op *op)*/
void mpi_op_create_(int * function,int * commute, int * op,int * __ierr)
{
  *__ierr = MPI_Op_create((MPI_User_function *)ToPtr(function),
			  (*((int*)commute)), (MPI_Op *)ToPtr(op));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Op_free(MPI_Op * op) */
void mpi_op_free_(int * op,int * __ierr)
{
  *__ierr = MPI_Op_free((MPI_Op *)ToPtr(op));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Topo_test(MPI_Comm comm,int * status) */
void mpi_topo_test_(int * comm,int * status,int * __ierr)
{
  *__ierr = MPI_Topo_test((MPI_Comm)(*((int *)comm)),
			  (int *)ToPtr(status));
}
/**********************************************************************/
/* MPI-2 */
/**********************************************************************/

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Add_error_class ( int *errorclass ) */
void mpi_add_error_class_ ( int *errorclass, int * __ierr )
{
  *__ierr = MPI_Add_error_class ( (int *)ToPtr(errorclass) );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Add_error_code ( int errorcode, int *errorclass ) */
void mpi_add_error_code_ ( int* class, int *code, int* __ierr )
{
  *__ierr = MPI_Add_error_code ( (*((int *)class)), (int *)ToPtr(code) );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Add_error_string ( int errorcode, char *string ) */
void mpi_add_error_string_ ( int *code,  char *string, int* __ierr )
{
  *__ierr = MPI_Add_error_string ( (*((int *)code)),(char *)ToPtr(string));
}



/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_create(MPI_Info * info) */
/*void mpi_info_create_(int * info,int * __ierr)
{
  *__ierr = MPI_Info_create((MPI_Info *)ToPtr(info));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_delete(MPI_Info info,char * key) */
/*void mpi_info_delete_(int * info,char * key,int * __ierr)
{
  *__ierr = MPI_Info_delete((MPI_Info)(*((int *)info)),
			    (char *)ToPtr(key));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_dup(MPI_Info info,MPI_Info * newinfo) */
/*void mpi_info_dup_(int * info,int * newinfo,int * __ierr)
{
  *__ierr = MPI_Info_dup((MPI_Info)(*((int *)info)),
			 (MPI_Info *)ToPtr(newinfo));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_free(MPI_Info * info) */
/*void mpi_info_free_(int * info,int * __ierr)
{
  *__ierr = MPI_Info_free((MPI_Info *)ToPtr(info));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_get(MPI_Info info,char * key,int valuelen,char * value,
   int * flag) */
/*void mpi_info_get_(int * info,char * key,int * valuelen,char * value,
		   int * flag,int * __ierr)
{
  *__ierr = MPI_Info_get((MPI_Info)(*((int *)info)),(char *)ToPtr(key),
			 (*((int *)valuelen)),(char *)ToPtr(value),
			 (int *)ToPtr(flag));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_get_nkeys(MPI_Info info,int * nkeys) */
/*void mpi_info_get_nkeys_(int * info,int * nkeys,int * __ierr)
{
  *__ierr = MPI_Info_get_nkeys((MPI_Info)(*((int *)info)),
			       (int *)ToPtr(nkeys));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_get_nthkey(MPI_Info info,int n,char * key) */
/*void mpi_info_get_nthkey_(int * info,int * n,char * key,int * __ierr)
{
  *__ierr = MPI_Info_get_nthkey((MPI_Info)(*((int *)info)),(*((int *)n)),
				(char *)ToPtr(key));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_get_valuelen(MPI_Info info,char * key,int * valuelen,
   int * flag) */
/*void mpi_info_get_valuelen_(int * info,char * key,int * valuelen,
			    int * flag,int * __ierr)
{
  *__ierr = MPI_Info_get_valuelen((MPI_Info)(*((int *)info)),
				  (char *)ToPtr(key),
				  (int *)ToPtr(valuelen),
				  (int *)ToPtr(flag));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Info_set(MPI_Info info,char * key,char * value) */
/*void mpi_info_set_(int * info,char * key,char * value,int * __ierr)
{
  *__ierr = MPI_Info_set((MPI_Info)(*((int *)info)),(char *)ToPtr(key),
			 (char *)ToPtr(value));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/



