/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_COLL
#define _FT_MPI_H_F2C_COLL

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"


/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS
#    pragma weak  MPI_BCAST           = mpi_bcast_
#    pragma weak  PMPI_BCAST          = mpi_bcast_ 
#    pragma weak  MPI_REDUCE          = mpi_reduce_
#    pragma weak  PMPI_REDUCE         = mpi_reduce_ 
#    pragma weak  MPI_BARRIER         = mpi_barrier_
#    pragma weak  PMPI_BARRIER        = mpi_barrier_ 
#    pragma weak  MPI_ALLREDUCE       = mpi_allreduce_
#    pragma weak  PMPI_ALLREDUCE      = mpi_allreduce_ 
#    pragma weak  MPI_ALLTOALL        = mpi_alltoall_
#    pragma weak  PMPI_ALLTOALL       = mpi_alltoall_ 
#    pragma weak  MPI_ALLTOALLV       = mpi_alltoallv_
#    pragma weak  PMPI_ALLTOALLV      = mpi_alltoallv_ 
#    pragma weak  MPI_GATHER          = mpi_gather_
#    pragma weak  PMPI_GATHER         = mpi_gather_ 
#    pragma weak  MPI_GATHERV         = mpi_gatherv_
#    pragma weak  PMPI_GATHERV        = mpi_gatherv_ 
#    pragma weak  MPI_ALLGATHER       = mpi_allgather_
#    pragma weak  PMPI_ALLGATHER      = mpi_allgather_ 
#    pragma weak  MPI_ALLGATHERV      = mpi_allgatherv_
#    pragma weak  PMPI_ALLGATHERV     = mpi_allgatherv_ 
#    pragma weak  MPI_REDUCE_SCATTER  = mpi_reduce_scatter_	
#    pragma weak  PMPI_REDUCE_SCATTER = mpi_reduce_scatter_
#    pragma weak  MPI_SCATTER         = mpi_scatter_
#    pragma weak  PMPI_SCATTER        = mpi_scatter_ 
#    pragma weak  MPI_SCATTERV        = mpi_scatterv_
#    pragma weak  PMPI_SCATTERV       = mpi_scatterv_ 
#    pragma weak  MPI_SCAN            = mpi_scan_
#    pragma weak  PMPI_SCAN           =  mpi_scan_ 

#  elif defined(FORTRANDOUBLEUNDERSCORE)
#    pragma weak  mpi_bcast__           = mpi_bcast_
#    pragma weak  pmpi_bcast__          = mpi_bcast_
#    pragma weak  mpi_reduce__          = mpi_reduce_
#    pragma weak  pmpi_reduce__         = mpi_reduce_
#    pragma weak  mpi_barrier__         = mpi_barrier_
#    pragma weak  pmpi_barrier__        = mpi_barrier_
#    pragma weak  mpi_allreduce__       = mpi_allreduce_
#    pragma weak  pmpi_allreduce__      = mpi_allreduce_
#    pragma weak  mpi_alltoall__        = mpi_alltoall_
#    pragma weak  pmpi_alltoall__       = mpi_alltoall_
#    pragma weak  mpi_alltoallv__       = mpi_alltoallv_
#    pragma weak  pmpi_alltoallv__      = mpi_alltoallv_
#    pragma weak  mpi_gather__          = mpi_gather_
#    pragma weak  pmpi_gather__         = mpi_gather_
#    pragma weak  mpi_gatherv__         = mpi_gatherv_
#    pragma weak  pmpi_gatherv__        = mpi_gatherv_
#    pragma weak  mpi_allgather__       = mpi_allgather_
#    pragma weak  pmpi_allgather__      = mpi_allgather_
#    pragma weak  mpi_allgatherv__      = mpi_allgatherv_
#    pragma weak  pmpi_allgatherv__     = mpi_allgatherv_
#    pragma weak  mpi_scatter__         = mpi_scatter_
#    pragma weak  pmpi_scatter__        = mpi_scatter_
#    pragma weak  mpi_scatterv__        = mpi_scatterv_
#    pragma weak  pmpi_scatterv__       = mpi_scatterv_
#    pragma weak  mpi_scan__            = mpi_scan_
#    pragma weak  pmpi_scan__           = mpi_scan_
#    pragma weak  mpi_reduce_scatter__  = mpi_reduce_scatter_
#    pragma weak  pmpi_reduce_scatter__ = mpi_reduce_scatter_

#  elif defined(FORTRANNOUNDERSCORE)
#    pragma weak  mpi_bcast           = mpi_bcast_
#    pragma weak  pmpi_bcast          = mpi_bcast_
#    pragma weak  mpi_reduce          = mpi_reduce_
#    pragma weak  pmpi_reduce         = mpi_reduce_
#    pragma weak  mpi_barrier         = mpi_barrier_
#    pragma weak  pmpi_barrier        = mpi_barrier_
#    pragma weak  mpi_allreduce       = mpi_allreduce_
#    pragma weak  pmpi_allreduce      = mpi_allreduce_
#    pragma weak  mpi_alltoall        = mpi_alltoall_
#    pragma weak  pmpi_alltoall       = mpi_alltoall_
#    pragma weak  mpi_alltoallv       = mpi_alltoallv_
#    pragma weak  pmpi_alltoallv      = mpi_alltoallv_
#    pragma weak  mpi_gather          = mpi_gather_
#    pragma weak  pmpi_gather         = mpi_gather_
#    pragma weak  mpi_gatherv         = mpi_gatherv_
#    pragma weak  pmpi_gatherv        = mpi_gatherv_
#    pragma weak  mpi_allgather       = mpi_allgather_
#    pragma weak  pmpi_allgather      = mpi_allgather_
#    pragma weak  mpi_allgatherv      = mpi_allgatherv_
#    pragma weak  pmpi_allgatherv     = mpi_allgatherv_
#    pragma weak  mpi_scatter         = mpi_scatter_
#    pragma weak  pmpi_scatter        = mpi_scatter_
#    pragma weak  mpi_scatterv        = mpi_scatterv_
#    pragma weak  pmpi_scatterv       = mpi_scatterv_
#    pragma weak  mpi_scan            = mpi_scan_
#    pragma weak  pmpi_scan           = mpi_scan_
#    pragma weak  mpi_reduce_scatter  = mpi_reduce_scatter_
#    pragma weak  pmpi_reduce_scatter = mpi_reduce_scatter_

#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */
#    pragma weak  pmpi_bcast_          = mpi_bcast_
#    pragma weak  pmpi_reduce_         = mpi_reduce_
#    pragma weak  pmpi_barrier_        = mpi_barrier_
#    pragma weak  pmpi_allreduce_      = mpi_allreduce_
#    pragma weak  pmpi_alltoall_       = mpi_alltoall_
#    pragma weak  pmpi_alltoallv_      = mpi_alltoallv_
#    pragma weak  pmpi_gather_         = mpi_gather_
#    pragma weak  pmpi_gatherv_        = mpi_gatherv_
#    pragma weak  pmpi_allgather_      = mpi_allgather_
#    pragma weak  pmpi_allgatherv_     = mpi_allgatherv_
#    pragma weak  pmpi_scatter_        = mpi_scatter_
#    pragma weak  pmpi_scatterv_       = mpi_scatterv_
#    pragma weak  pmpi_scan_           = mpi_scan_
#    pragma weak  pmpi_reduce_scatter_ = mpi_reduce_scatter_

#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define mpi_bcast_           PMPI_BCAST 
#      define mpi_reduce_          PMPI_REDUCE 
#      define mpi_barrier_         PMPI_BARRIER 
#      define mpi_allreduce_       PMPI_ALLREDUCE 
#      define mpi_alltoall_        PMPI_ALLTOALL 
#      define mpi_alltoallv_       PMPI_ALLTOALLV 
#      define mpi_gather_          PMPI_GATHER 
#      define mpi_gatherv_         PMPI_GATHERV 
#      define mpi_allgather_       PMPI_ALLGATHER 
#      define mpi_allgatherv_      PMPI_ALLGATHERV 
#      define mpi_scatter_         PMPI_SCATTER 
#      define mpi_scatterv_        PMPI_SCATTERV 
#      define mpi_scan_            PMPI_SCAN 
#      define mpi_reduce_scatter_  PMPI_REDUCE_SCATTER   

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_bcast_          pmpi_bcast__ 
#      define  mpi_reduce_         pmpi_reduce__ 
#      define  mpi_barrier_        pmpi_barrier__ 
#      define  mpi_allreduce_      pmpi_allreduce__ 
#      define  mpi_alltoall_       pmpi_alltoall__ 
#      define  mpi_alltoallv_      pmpi_alltoallv__ 
#      define  mpi_gather_         pmpi_gather__ 
#      define  mpi_gatherv_        pmpi_gatherv__ 
#      define  mpi_allgather_      pmpi_allgather__ 
#      define  mpi_allgatherv_     pmpi_allgatherv__ 
#      define  mpi_scatter_        pmpi_scatter__ 
#      define  mpi_scatterv_       pmpi_scatterv__ 
#      define  mpi_scan_           pmpi_scan__ 
#      define  mpi_reduce_scatter_ pmpi_reduce_scatter__ 

#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_bcast_          pmpi_bcast 
#      define  mpi_reduce_         pmpi_reduce 
#      define  mpi_barrier_        pmpi_barrier 
#      define  mpi_allreduce_      pmpi_allreduce 
#      define  mpi_alltoall_       pmpi_alltoall 
#      define  mpi_alltoallv_      pmpi_alltoallv 
#      define  mpi_gather_         pmpi_gather 
#      define  mpi_gatherv_        pmpi_gatherv 
#      define  mpi_allgather_      pmpi_allgather 
#      define  mpi_allgatherv_     pmpi_allgatherv 
#      define  mpi_scatter_        pmpi_scatter 
#      define  mpi_scatterv_       pmpi_scatterv 
#      define  mpi_scan_           pmpi_scan 
#      define  mpi_reduce_scatter_ pmpi_reduce_scatter

#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define  mpi_bcast_          pmpi_bcast_ 
#      define  mpi_reduce_         pmpi_reduce_ 
#      define  mpi_barrier_        pmpi_barrier_ 
#      define  mpi_allreduce_      pmpi_allreduce_ 
#      define  mpi_alltoall_       pmpi_alltoall_ 
#      define  mpi_alltoallv_      pmpi_alltoallv_ 
#      define  mpi_gather_         pmpi_gather_ 
#      define  mpi_gatherv_        pmpi_gatherv_ 
#      define  mpi_allgather_      pmpi_allgather_ 
#      define  mpi_allgatherv_     pmpi_allgatherv_ 
#      define  mpi_scatter_        pmpi_scatter_ 
#      define  mpi_scatterv_       pmpi_scatterv_ 
#      define  mpi_scan_           pmpi_scan_ 
#      define  mpi_reduce_scatter_ pmpi_reduce_scatter_ 
#     endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#      define mpi_bcast_           MPI_BCAST 
#      define mpi_reduce_          MPI_REDUCE 
#      define mpi_barrier_         MPI_BARRIER 
#      define mpi_allreduce_       MPI_ALLREDUCE 
#      define mpi_alltoall_        MPI_ALLTOALL 
#      define mpi_alltoallv_       MPI_ALLTOALLV 
#      define mpi_gather_          MPI_GATHER 
#      define mpi_gatherv_         MPI_GATHERV 
#      define mpi_allgather_       MPI_ALLGATHER 
#      define mpi_allgatherv_      MPI_ALLGATHERV 
#      define mpi_scatter_         MPI_SCATTER 
#      define mpi_scatterv_        MPI_SCATTERV 
#      define mpi_scan_            MPI_SCAN 
#      define mpi_reduce_scatter_  MPI_REDUCE_SCATTER   

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_bcast_          mpi_bcast__ 
#      define  mpi_reduce_         mpi_reduce__ 
#      define  mpi_barrier_        mpi_barrier__ 
#      define  mpi_allreduce_      mpi_allreduce__ 
#      define  mpi_alltoall_       mpi_alltoall__ 
#      define  mpi_alltoallv_      mpi_alltoallv__ 
#      define  mpi_gather_         mpi_gather__ 
#      define  mpi_gatherv_        mpi_gatherv__ 
#      define  mpi_allgather_      mpi_allgather__ 
#      define  mpi_allgatherv_     mpi_allgatherv__ 
#      define  mpi_scatter_        mpi_scatter__ 
#      define  mpi_scatterv_       mpi_scatterv__ 
#      define  mpi_scan_           mpi_scan__ 
#      define  mpi_reduce_scatter_ mpi_reduce_scatter__ 

#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_bcast_          mpi_bcast 
#      define  mpi_reduce_         mpi_reduce 
#      define  mpi_barrier_        mpi_barrier 
#      define  mpi_allreduce_      mpi_allreduce 
#      define  mpi_alltoall_       mpi_alltoall 
#      define  mpi_alltoallv_      mpi_alltoallv 
#      define  mpi_gather_         mpi_gather 
#      define  mpi_gatherv_        mpi_gatherv 
#      define  mpi_allgather_      mpi_allgather 
#      define  mpi_allgatherv_     mpi_allgatherv 
#      define  mpi_scatter_        mpi_scatter 
#      define  mpi_scatterv_       mpi_scatterv 
#      define  mpi_scan_           mpi_scan 
#      define  mpi_reduce_scatter_ mpi_reduce_scatter

#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */
#endif
