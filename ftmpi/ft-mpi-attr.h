/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@Cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef __FT_MPI_ATTR_H__
#define __FT_MPI_ATTR_H__
/* Typedefs for the fortran versions of the user provided functions... */
typedef int  MPI_Fcopy_function (MPI_Comm *oldcomm, int *keyval, 
				 void *extra_state,
				 void *attribute_val_in, 
				 void *attribute_val_out, int *flag,
				 int *ierr);

typedef int  MPI_Fdelete_function (MPI_Comm *comm, int *keyval, 
				   void *attribute_val, 
				   void *extra_state, 
				   int *ierr);

typedef int  MPI_Fcomm_copy_attr_function (MPI_Comm *comm, int *keyval, 
					   void *extra_state,
					   void *attribute_val_in, 
					   void *attribute_val_out, 
					   int *flag, int *ierr);

typedef int  MPI_Fcomm_delete_attr_function (MPI_Comm *comm, 
					     int *keyval, 
					     void *attribute_val, 
					     void *extra_state, 
					     int *ierr);
typedef int  MPI_Ftype_copy_attr_function (MPI_Datatype *type, 
					   int *keyval, 
					   void *extra_state,
					   void *attribute_val_in, 
					   void *attribute_val_out, 
					   int *flag, int *ierr);

typedef int  MPI_Ftype_delete_attr_function (MPI_Datatype *type, 
					     int *keyval, 
					     void *attribute_val, 
					     void *extra_state,
					     int *ierr);

typedef int  MPI_Fwin_copy_attr_function (MPI_Win *win, int *keyval, 
					  void *extra_state,
					  void *attribute_val_in, 
					  void *attribute_val_out, 
					  int *flag, int *ierr);

typedef int  MPI_Fwin_delete_attr_function (MPI_Win *win, int *keyval, 
					    void *attribute_val, 
					    void *extra_state,
					    int *ierr);





typedef struct {
  int                in_use;       /* is this object marked as currently used ? */
  void             *attr_val;     /* attribute value */
} keyval_info_t;

typedef struct {
  int                 key;         /* key returned with Keyval_create or equiv. functions*/
  int                 in_use;      /* is this element already used ? */
  int                 freed;       /* did the user call keyval_free ? */
  int                 fortran;     /* 0 : C  1: Fortran */
  void                *extra_state;/* given by user */
  int                 type;        /* FTMPI_KEY_UNKNOWN, FTMPI_KEY_COMM1, FTMPI_KEY_COMM2, 
				      FTMPI_KEY_WIN, FTMPI_KEY_TYPE */
  union funcs { 
    struct mpi1{
      MPI_Copy_function   *copy_fn;    /* pointer to the user-function */
      MPI_Delete_function *del_fn;     /* pointer to the user provided function */
      MPI_Fcopy_function   *fcopy_fn;    /* pointer to the user-function (fortran)*/
      MPI_Fdelete_function *fdel_fn;     /* pointer to the user provided function (fortran)*/
    } mpi1;
    struct mpi2comm{
      MPI_Comm_copy_attr_function   *copy_fn;    /* pointer to the user-function */
      MPI_Comm_delete_attr_function *del_fn;     /* pointer to the user provided function */
      MPI_Fcomm_copy_attr_function   *fcopy_fn;    /* pointer to the user-function (fortran)*/
      MPI_Fcomm_delete_attr_function *fdel_fn;     /* pointer to the user provided function (f)*/
    } mpi2comm;
    struct mpi2win{
      MPI_Win_copy_attr_function   *copy_fn;    /* pointer to the user-function */
      MPI_Win_delete_attr_function *del_fn;     /* pointer to the user provided function */
      MPI_Fwin_copy_attr_function   *fcopy_fn;    /* pointer to the user-function (fortran)*/
      MPI_Fwin_delete_attr_function *fdel_fn;     /* pointer to the user provided function (f)*/
    } mpi2win;
    struct mpi2type{
      MPI_Type_copy_attr_function   *copy_fn;    /* pointer to the user-function */
      MPI_Type_delete_attr_function *del_fn;     /* pointer to the user provided function */
      MPI_Ftype_copy_attr_function   *fcopy_fn;    /* pointer to the user-function (fortran) */
      MPI_Ftype_delete_attr_function *fdel_fn;     /* pointer to the user provided function (f)*/
    } mpi2type;
  } funcs;
  int                 maxvalues;   /* maximum length of the values-array */
  int                 nvalues;     /* number of objects using this keyval */
  keyval_info_t       *values;     /* array of values allocated during the createion of the keyval */
} key_info_t;

/* Constants: */
#define FTMPI_KEY_UNKNOWN -1935
#define FTMPI_KEY_COMM1   -1940
#define FTMPI_KEY_COMM2   -1945
#define FTMPI_KEY_WIN     -1950
#define FTMPI_KEY_TYPE    -1955

#define FTMPI_COMM_ATTR_BLOCK_SIZE 5 /* how many elements for the attribute-array are 
					    allocated at once ? */

#define FTMPI_TYPE_ATTR_BLOCK_SIZE 5 /* how many elements for the attribute-array are 
					allocated at once ? */

/* Prototypes */
key_info_t * ftmpi_key_init ( key_info_t *k );
int ftmpi_key_free ( key_info_t *k);
int ftmpi_key_copy ( key_info_t *k1, key_info_t *k2 );
int ftmpi_key_get_next_free ( int type );
int ftmpi_key_mark_free ( int key );
int ftmpi_key_test_freed ( int key );
int ftmpi_key_release ( int key );
int ftmpi_key_test ( int key, int type );
int ftmpi_key_set ( int key, void *copy_fn,void *del_fn, void *extra_state );


int ftmpi_keyval_set ( int key, int object, void *attr_val );
int ftmpi_keyval_get ( int key, int object, void *attr_val );
int ftmpi_keyval_delete ( int key, int object, int *nvalues );
int ftmpi_keyval_test ( int key, int object, int type );
int ftmpi_keyval_copy ( int  key, int object1, int object2 );

#endif/*  __FT_MPI_ATTR_H__ */



