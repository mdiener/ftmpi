
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* 
	Modes used by FT-MPI
*/

#ifndef _FT_MPI_H_MODES
#define _FT_MPI_H_MODES

/* This is the mode the communicator operations care about under failure */

typedef enum {
		FT_MODE_SHRINK,	/* Force the user to reform the comm and repack ids */
		FT_MODE_REBUILD,/* Spawn a replacement save MCW only */
		FT_MODE_REBUILD_ALL,/* Spawn a replacement save **ALL** */
		FT_MODE_BLANK,  /* sync communicator on error but don't fill in failed nodes */
		FT_MODE_ABORT	/* On error, shut down as in vinilia MPI */
		} ft_com_mode_t;

/* This is the mode the commications (connections) care about during failure */

typedef enum {
		FT_MODE_CONT, 	/* just continue, NOP any ops on the failed */
		FT_MODE_NOP		/* NO OP all users comms on error */
} ft_con_mode_t;

/* This is our actual communicator states */

typedef enum {
	FT_OK,		/* Everything is A ok and always has been */
	FT_DETECTED,	/* An error has been detected, we need to recover */
	FT_RECOVER,	/* An error has occured and we are in recover state */
	FT_RECOVERED,	/* we have recovered. But user needs to reform coms */
	FT_MPI_ABORT,	/* someone has called MPI_ABORT */
	FT_FAILED	/* We cannot recover, and so will exit probably */
} ft_com_state_t;

/* This is what we think other processes are doing */

typedef enum {
	FT_PROC_OK,		/* Everything is fine. */
	FT_PROC_UNAVAIL,/* Cannot talk to this proc */
	FT_PROC_JOINING,/* This proc is joining wait for it to be OK first */
	FT_PROC_FAILED,	/* node is unavailabe and cannot be recovered etc */
	FT_PROC_ABORTING /* node is aborting as it should */
} ft_proc_state_t;

/* This is what happens at startup */

typedef enum {			/* MPI_Init return codes */
	FT_INIT_NEW,	/* This process was started up at spawn time */
	FT_INIT_RESTART /* this is a restarted process... */
}	ft_init_state_t;

/* This is the real internal startup/failure handling states we go though */
/* these are posted in the state records held by the NS or MD ring */

typedef enum {
	FT_STATE_OK,	/* what happens when all is well */
	FT_STATE_WAIT,	/* Wait for the BUILD mode or CHECK mode */
	FT_STATE_ACK,	/* Send an ACK to the current leader */
	FT_STATE_BUILD,	/* Build your communicators */
	FT_STATE_SPAWN, /* Leader is spawning */
	FT_STATE_CHECK,	/* check yourself and those around you ;) */
	FT_STATE_ELECT,	/* This is what we do when we have lost our leader */
	FT_STATE_START, /* This is a special one used by FTMPIRUN not spawn */
	FT_STATE_ABORT, /* Someone has called MPI_Abort. Almost same as Halt */
	FT_STATE_HALT	/* pathalogical failure or someone called MPI_Finialise */
}	ft_state_t;

#endif /* _FT_MPI_H_MODES */
