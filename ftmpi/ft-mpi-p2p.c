
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 ftmpi-pp-send does the real work
 and uses functions from ft-mpi-com and ft-mpi-conn
 aka the communicator DB and the connections manager

 */

/* default types */
#include "mpi.h"

/* nonblocking request lists */
#include "ft-mpi-req-list.h"

/* internal dts */
#include "ft-mpi-sys.h"

/* recv message list structures */
#include "ft-mpi-msg-list.h"


/* low level communications i.e. moving data */
#include "ft-mpi-conn.h"

/* prototypes for communicator lowlevel ops */
#include "ft-mpi-com.h"

/* groups intercom/intracom checks */
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"

/* my own prototypes */
#include "ft-mpi-p2p.h"

/* procinfo */
#include "ft-mpi-procinfo.h"

/* some nonblocking polling option constants NB_XX */
#include "ft-mpi-nb.h"

#include "ft-mpi-lib.h"
#include "debug.h"
#include <stdio.h>
#include <string.h>	/* for memcpy */
#include <assert.h>

/* as we share a couple of data structure with the nb routines we need to */
/* import them here by external refs */

extern long req_list_recv_count;
extern struct req_list * req_list_recv_head;

extern struct msg_list * incomplete_msg_list_head;
extern long incomplete_msg_list_count;

extern struct msg_list * system_msg_list_head;
extern long system_msg_list_count;


/* ----------------------------------------------------------------------- */

/* ******************************************************************************** */

/*
 * This routine posts a SNIPE2 recv on an entry in the msg list of unexpected messages..
 * The msglist entry mlp is not put onto the message list until the end of the op
 * via a callback. This means that at the end we need to check that a NB recv hasn't been posted that matches it.
 * In a later verson (i.e. TODO) we will have it on the msg list and make it possible for a NB recv to match it
 * before it is completed. GEF Oct03
 */

int ftmpi_pp_post_recv_into_msg_list (int recvconn, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize)
{
comm_info_t *incptr=NULL;
struct msg_list* mlp=NULL;
struct msg_list* mlh=NULL;
int rc;
int systemq = 0;	/* into system queue or normal queues */


	/* here we have to place the data in an msg list entry on the recv q */
	/* first we need to create the msg list entry */

	/* its its an empty message / no payload it goes on the com unexpQ */
	/* if it has a payload it goes on the incompleted unexpQ until completed */

/* we must also check which comm for those nice messages that don't belong anywhwere
 * yet */

	   /* we need to locate the msg_list for the incomming message */
	incptr = ftmpi_com_get_ptr (incomm);
    if (!incptr) {
#ifdef VERBOSE
		   fprintf(stderr,"note: ftmpi_pp_post_recv_into_msg_list: incomm %d doesn't exist yet, posting to system msg list\n", incomm);
#endif
		   systemq = 1;

		   /* get element to fill in. The system queue does not have an incomplete */
		   /* statusi. When a element is completed it is just left there until done */
		   mlp = msg_list_new (system_msg_list_head); 
		   system_msg_list_count++;

		   mlp->ml_queue = systemQ; /* flag it */

	}
	else { /* not on the system queue but the other queues */
		   systemq = 0;

		if (insize) {
		   /* we get an element on the incompleted unexpected message list */
		   /* try saying that with a mouth full of marbles ! */
		   mlp = msg_list_new (incomplete_msg_list_head); 
		   incomplete_msg_list_count++;
		   mlp->ml_queue = incompmsgQ; /* flag it */
		   }

	   else { /* no payload so put it on the coms msg list */


		   mlh = incptr->msg_list_head;

		/* now get a msg_list entry point i.e. a new place to put the message */
		   mlp = msg_list_new(mlh); 

		/* update message count in recv queue for this communicator */
		(incptr->msg_list_count)++;

		   mlp->ml_queue = unexpmsgQ; /* flag it */
	   }

	} /* if not on the system queue */

    /* now fill it with what we have so far */
    /* i.e. its header */

    mlp->ml_senders_rank = infrom;
    mlp->ml_mpi_tag = intag;
    mlp->ml_mpi_comm = incomm;
    mlp->ml_msg_type = intype;
    mlp->ml_prot_type = 0;      /* not implemented ? */
    mlp->ml_msg_id = inmsgid;
    mlp->ml_target_rank = into;
	mlp->ml_sgid = infromgid;
	mlp->ml_rgid = intogid;
    mlp->ml_msg_length = insize;   /* we recheck this in a sec.. */
    mlp->ml_msg_com_ver = 0;    /* not implemented ? */

	mlp->ml_matched_nb_rlp = NULL; /* not matched a NB yet */

	/* if recving a zero length mpi unexpected message or one with a payload */
	if (insize) {

    mlp->ml_msg_buf = (char*) _MALLOC (insize);
	if (!(mlp->ml_msg_buf)) {
		fprintf(stderr,"ftmpi_p2p:ftmpi_pp_recv:ftmpi_pp_put_into_msg_list: Fatal error.\nUnable to allocate memory for out of order message on the\ncurrent recv message queue.");

		/* make sure queue counts correct */
		if (mlp->ml_queue == systemQ) system_msg_list_count--;
		if (mlp->ml_queue == unexpmsgQ) incomplete_msg_list_count--;
		/* free entry from message list (recv queue) */
		msg_list_free (mlp);
/* 		MPI_Abort (comm, -999); */
		return (MPI_ERR_INTERN);
		}

	/* have structure so post the recv for getting the actual data into it */

	rc = conn_snipe2_post_recvdata (recvconn, mlp->ml_msg_buf, insize, NULL, mlp);

	if (rc<0) {	/* problem with connection etc */
		/* make sure queue counts correct */
		if (mlp->ml_queue == systemQ) system_msg_list_count--;
		if (mlp->ml_queue == unexpmsgQ) incomplete_msg_list_count--;
		/* free up data structure etc */
		_FREE (mlp->ml_msg_buf);
		/* free entry from message list (recv queue) */
		msg_list_free (mlp);
		return (MPI_ERR_OTHER);
		}


	}  /* unexpected message with a payload */

	else {	/* no payload unexp message */
	   	mlp->ml_msg_buf = (char*) NULL; /* make sure it is an empty buffer */
	}

return (0);
}

/* ******************************************************************************** */

int	ftmpi_pp_add_msg_to_msg_list (struct msg_list *mlp)
{
comm_info_t *incptr;
struct msg_list* mlh;
int incomm;



if (mlp->ml_queue==systemQ) {
	/* first we detach the message from the systemQ */
   msg_list_detach(mlp);
   system_msg_list_count--;
}

if (mlp->ml_queue==incompmsgQ) {
	/* first we detach the message from the incomplete unexpQ */
   msg_list_detach(mlp);
   incomplete_msg_list_count--;
}

if (mlp->ml_queue==unexpmsgQ) {
   fprintf(stderr,"ftmpi_pp_add_msg_to_msg_list: attempt to move mlp 0x%p from unexpected message queue to unexpected queue??\n", mlp);
   return (-1);
}
   

	/* then add it to the com based unexpQ */

	incomm = mlp->ml_mpi_comm;
	incptr = ftmpi_com_get_ptr (incomm);
	if (!incptr) { /* bad com, here ? */
		fprintf(stderr,"ftmpi_pp_add_msg_to_msg_list: recv message for comm %d that does not exist!\n", 
							incomm);
			return (-999);
	}

	mlh = incptr->msg_list_head;

	msg_list_add_2tail (mlh, mlp);	/* add to tail of unexp msgQ */

	(incptr->msg_list_count)++;		/* inc count of messages on unexpQ */

	mlp->ml_queue = unexpmsgQ;

return (0);
}

/* ******************************************************************************** */

/* 
 * This is the snipe2 version of ftmpi_pp_put_into_req_list()
 * Here we tell the system to post a recv to the correct buffer etc etc 
 */

int ftmpi_pp_post_recv_into_req_list (struct req_list *rlp, int recvconn, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize)
{
  comm_info_t *incptr;
  /* struct msg_list* mlp; */
  /* struct msg_list* mlh; */
  /* long dsize; */
  int rc;
  long actualreadsize=0;


  /* here we have to place the data in a req list entry on the nb recv q */

  incptr = ftmpi_com_get_ptr (incomm);
  if (!incptr) { /* bad com, here ? */
    fprintf(stderr,"ftmpi_pp_recv:ftmpi_post_recv_into_req_list: recv message for comm %d that does not exist!\n", 
	    incomm);
    return (-999);
  }
  
  /* now fill it with what we have so far */
  /* i.e. its header */

  rlp->rl_status.MPI_SOURCE = infrom;
  rlp->rl_status.MPI_TAG = intag;
  rlp->rl_status.MPI_ERROR = MPI_SUCCESS;
  /*     intype */
  /*     inmsgid */
  /*     into */
  rlp->rl_sgid = infromgid;
  rlp->rl_rgid = intogid;
  rlp->rl_req_com_ver = 0;    	/* not implemented ? */

/*   assert(rlp->code_info == NULL); */
  rlp->code_info = (FTMPI_DDT_CODE_INFO *) ftmpi_procinfo_get_ddt_code_info(rlp->rl_sgid);
  rlp->rl_req_dt_op = ftmpi_ddt_decode_size_det(rlp->rl_req_dt,rlp->rl_req_cnt,
						&(rlp->rl_status.msglength),rlp->code_info);
  
  /* OK now check for truncation. This should be done before anything else
   * as we should be able to detect truncations in a heterogeneous environment.
   */
  if( rlp->rl_status.msglength != insize ) {
    if( rlp->rl_status.msglength < insize ) {
	printf( "%s:%d truncation for request %d(%p) [source %d tag %d] from %d to %d bytes\n",
		__FILE__, __LINE__, rlp->rl_req_handle, rlp, infrom, intag,
		insize, rlp->rl_status.msglength );    
	rlp->rl_status.MPI_ERROR = MPI_ERR_TRUNCATE;
	actualreadsize = rlp->rl_status.msglength; /* we can only read what is expected */
    }
    else {
	int dt_size;
	
	ftmpi_mpi_type_size ( rlp->rl_req_dt, &dt_size);
	/* if rlp->rl_status.msglength > insize, then we are just receiving less data than we actually wanted ! NOT an error */
	actualreadsize = insize;
	rlp->rl_status.msglength = insize;
	rlp->rl_req_cnt = insize/dt_size;
    }
  }
  else
	 actualreadsize = insize;

  /* we should only process a recv if its not a zero length recv */
  /* if we ask for zero bytes and there is a trunc the follow up will  */
  /* flush the socket / channel for us */

  if (!actualreadsize)  {		/* if zero length nb recv */
      ftmpi_nb_move_recv_inprogress_2_done (rlp);	/* its done now */
      return (rlp->rl_status.MPI_ERROR);
  }


  /* depending on the dt_op we will either read directly or convert the data read */

  if (rlp->rl_req_dt_op==0) { /* no conversion needed, read into userspace buffer */
      rc = conn_snipe2_post_recvdata (recvconn, 
				      rlp->rl_req_usrbuf + ftmpi_ddt_get_first_offset(rlp->rl_req_dt),
				      actualreadsize, rlp, NULL);
      rlp->rl_status.msglength = actualreadsize;
  } 
  else { /* operations asking for an additional buffer */
      /* here we read the data into a RAW buffer that we allocate */
#ifdef USE_MALLOC
      rlp->rl_req_rawbuf = (char*) _MALLOC (actualreadsize);
      if (!(rlp->rl_req_rawbuf)) {
	  rlp->rl_status.MPI_ERROR = MPI_ERR_INTERN;
	  return (MPI_ERR_INTERN);
      }
	  rlp->rl_req_rawbufid = -1;
#else
	  fprintf(stderr,"use message bufs missing in ftmpi_pp_post_recv_into_req_list\n");
	  MPI_Abort (MPI_COMM_WORLD);
#endif
    
      /* have structure so put the actual data into it */
    
      /* post the recv data from channel */
      /* 	   conn[recvconn].recvstate=3; */

      rc = conn_snipe2_post_recvdata (recvconn, rlp->rl_req_rawbuf, actualreadsize, rlp, NULL);
      rlp->rl_status.msglength = insize; /* amount of data we should receive */
  } /* op needing additional buffer space */

  return (rlp->rl_status.MPI_ERROR);
}

/* ******************************************************************************** */



/* ----------------------------------------------------------------------- */
/* ----------------------------------------------------------------------- */

/* This routine just checks the unexpected message queue for a match */
/* if there is one, it returns the result via status and flag */
/* note we handle rank not gid */
/* if there is not a match and blocking flag is set then we loop until a match */

/* unless there is an error, pp_probe returns MPI_SUCCESS */

int ftmpi_pp_probe (int rank, int tag, int comm, int *flag, MPI_Status *status, 
	  				int blocking)

{
  int rc;
  /* msg_hdr_t hdr;  */
  struct msg_list *mlp;	/* ptrs to message list elements */
  comm_info_t  *cptr;	/* ptr to the comm data structure */
  /* comm_info_t  *incptr; */ /* ptr to the comm data structure for incom */ 
  /* struct req_list *rlp; */ /* ptr to a match nb req */
  int done=0;		    /* done flag used to control looping */
  
  /* to do anything we need to know a few things about the comm DS */
  cptr = ftmpi_com_get_ptr (comm);
  if (!cptr) { /* bad com, here ? */
      return MPI_ERR_COMM;
  }
  
  /* reset pointers */
  mlp = (struct msg_list *) NULL;
  
  
  /* search for previously recvd data and out of order stuff */
  /* NOTE we only search if the number of queued message is >0 :) */
  
  while (!done) {
    if (cptr->msg_list_count) {
      
      mlp = msg_list_find_by_header (cptr->msg_list_head, rank, tag);
      
      if (mlp) {/* we found our message on the previously stored message list! */
	/* this means we can try and set the message status */
	/* and flag */
	
		if (flag) *flag=1;
	
		/* ok return status if requested */
		if (status!=MPI_STATUS_IGNORE) {
		  status->MPI_SOURCE = mlp->ml_senders_rank;
		  status->MPI_TAG = mlp->ml_mpi_tag;
		  status->MPI_ERROR = MPI_SUCCESS;
		  /* just set this now */
		  status->msglength = (long) mlp->ml_msg_length;
		  status->dt = MPI_DATATYPE_NULL;
		  status->elements = 0;
		  status->flags = FTMPI_REQ_DONE | FTMPI_REQ_FREED;
		}
	
		return (MPI_SUCCESS);
	
      }	/* if match found */
      
    } /* if we check the queue (message count>0) */

	/* no matter what happens, we attempt progress here to help all channels move data */
	/* if blocking we do a forced wait on progress to avoid spining at this level */
	if (blocking) rc = ftmpi_nb_progress (1);
	else
		rc = ftmpi_nb_progress (0);

	if (rc<0) return (rc);
    
    /* If we are looking for a message from ourselves... there is no point */
    /* looping as it would already be on the unexpected message queue... */
    /* or in a NB op. */
	/* So test and break out here */
    /* GEF discovered with testnbrecvsendself test code June03 */
    
    if ((ftmpi_com_rank (comm))==rank) {
	   	return (MPI_SUCCESS);
	}
    
    /* ok we are here because there was not a match.. so if poll and first loop */
    if (!blocking) {
      done = 1;	/* if not blocking and no match on unexp list then return */
    }
    
  } /* while not done loop */
  
  /* ok we are here as we never found a match.. boo */
  if (flag) *flag=0;
  return (MPI_SUCCESS);
}



/* 
 This is a support function that sets a NULL status incase we have a PROCNULL
 */

void ftmpi_pp_set_nullstatus (MPI_Status *status) 
{
  if( status == NULL ) return;  /* should we check that ?? */
  if( status == MPI_STATUS_IGNORE ) return;
  status->MPI_SOURCE = MPI_PROC_NULL;
  status->MPI_TAG = MPI_ANY_TAG;
  status->MPI_ERROR = 0;
  /* just set this now */
  status->msglength = 0;
  status->dt = MPI_DATATYPE_NULL;
  status->elements = 0;
  status->flags = FTMPI_REQ_EMPTY;
}

/* 
 This is a support function that sets a EMPTY status incase we have a REQNULL
 */

void ftmpi_pp_set_emptystatus (MPI_Status *status) 
{
  if( status == NULL ) return;  /* should we check that ?? */
  if( status == MPI_STATUS_IGNORE ) return;
  status->MPI_SOURCE = MPI_ANY_SOURCE;
  status->MPI_TAG = MPI_ANY_TAG;
  status->MPI_ERROR = 0;
  /* just set this now */
  status->msglength = 0;
  status->dt = MPI_DATATYPE_NULL;
  status->elements = 0;
  status->flags = FTMPI_REQ_EMPTY;
}



/*
Other misc helper functions 
Some of these might move into another misc library later on
*/


/* 
 This routine builds a gid array from using the target rank and com
 ** Note even if only one id found this routine MALLOCS the list ! **
*/

int	ftmpi_build_gid_array_from_rankcom (int rank, MPI_Comm comm, int *ngids, int **gids)
{

  int *fgids;	/* array of ids */
  int tngids;
  int inter;
  int i, j;
  int mygid;
  
  /* init routines */
  fgids=NULL;
  tngids=0;
  
  /* get my gid as its not allowed in the array/list */
  mygid = ftmpi_sys_get_gid();
  
  if (rank!=MPI_ANY_SOURCE) {
    fgids = (int*)_MALLOC(sizeof(int));	/* malloc one id only! */
    fgids[0] = ftmpi_com_gid (comm, rank);
    if (fgids[0]<0) {
      if (ngids) *ngids=0;
      if (gids) *gids=NULL;
      i = fgids[0];
      _FREE(fgids);
      return i;
    }
    if (fgids[0]!=mygid) tngids=1;
  }
  else { /* mpi any source, so we need to build a list of gids to recv from */
    /* but we cannot be in the list! */
    
    /* Determine whether we are using an inter or an intra-communicator */
    inter = ftmpi_com_test_inter ( comm );
    
    tngids=0;    /* if an intracomm we would not be in the list */
    if ( inter )
      {
        MPI_Group grp;
	
        grp = ftmpi_com_get_remote_group ( comm );
        j = ftmpi_group_size ( grp );
      }
    else
      j = ftmpi_com_size(comm);
    
    fgids = (int*)_MALLOC(sizeof(int)*j);
    
    for(i=0;i<j;i++) {
      fgids[tngids] = ftmpi_com_gid (comm, i);
      if (fgids[tngids]!=mygid) tngids++;
    }
    
    /*
      printf("recv any watching %d nodes: ", tngids);
      for(i=0;i<tngids;i++) {
      printf("0x%x ", fgids[i]);
      }
      printf("\n");
    */
    
  } /* else mpi any source build list of ids */

  if (ngids) *ngids=tngids;
  if (gids) *gids=fgids;
  
  return (0);
}

/*
 * move messages from systemQ to the unexpQ of a named communicator 
 *
 * used after com_create etc to handle premature messages delivery
 *
 * returns number moved or -1 for error?
 *
 */

int ftmpi_move_sys_msgs_to_unexp_msg_list (int comm)
{
   int cnt=0;
   int possible=0;
   int done=0;
   comm_info_t  *cptr;
   struct msg_list *mlp;

   possible = system_msg_list_count;

   if (!possible) return (0);

   cptr = ftmpi_com_get_ptr (comm); /* paranoid check */

   /* only error possible */
   if (!cptr) {
	  fprintf(stderr,"ftmpi_move_sys_msgs_to_unexp_msg_list: com %d still doesn't exist so cannot move messages! I would panic if I was you!\n", comm);
	  return (-1);
   }

   while (!done) {
	  mlp = msg_list_find_by_comm (system_msg_list_head, comm);
	  if (mlp) {
		 ftmpi_pp_add_msg_to_msg_list (mlp);
		 cnt++;
	  }
	  else 
		 done = 1;
   }


  return (cnt);
}

