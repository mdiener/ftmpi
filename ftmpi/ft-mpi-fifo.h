
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 * This is a linked list that acts as a FIFO
 * hense the user ops allowed are 
 * init (to create fifo/queue)
 * add an item to the tail
 * get an item from the head
 * howmany items in the queue
 * destroy queue
 * (+ some debug routines)
*/




#ifndef FIFOQUEUE
#define FIFOQUEUE

#define FIFODATAMAX 3

/* Message list, used by receive handler routines. */
typedef struct fifo_queue {
  struct fifo_queue *fq_link, *fq_rlink;  /* linked list stuff */
  int fq_type;                  /* type of item */
  void* fq_val[FIFODATAMAX];	/* values of data */
  long count;					/* count of element, only counts at head */
} fifo_t;


fifo_t *     fifo_queue_init();		/* called only once */
void         fifo_queue_destroy(); 	/* destroys list */
int          fifo_queue_get(fifo_t* fq_head, void** valp1, void** valp2, 
	  							void** valp3);  
int   	     fifo_queue_add (fifo_t* fq_head, int type, void* val1, 
	  						void* val2, void *val3);
long		 fifo_queue_count();
void         fifo_queue_dump();      /* dumps contents of whole list */
void 		fifo_queue_dump_freeinfo ();


#endif /* FIFOQUEUE */


