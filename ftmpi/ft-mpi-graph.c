
/*

    HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* NOTE: The implementation of this file is based on the implementation
   of the general graph-topologies in PACX-MPI. */



#include <stdio.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-graph.h"
#include "debug.h"

/* we have three classes of functions in this file:
1. Management functions:
   - ftmpi_graph_set 
   - ftmpi_graph_free
   - ftmpi_graph_copy
   - ftmpi_graph_compare
2. Retrieve information functions
   - ftmpi_graph_get_nnodes
   - ftmpi_graph_get_index_ptr
   - ftmpi_graph_get_edges_ptr
3. Functions, where we calculate some values
   - ftmpi_graph_calc_neighbors_count
   - ftmpi_graph_calc_neighbors
*/


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
graph_info_t* ftmpi_graph_set ( int nnodes, int *index, int *edges ) 
{
  graph_info_t *graph = NULL;
  int i;
  
  graph = (graph_info_t *)_MALLOC(sizeof(graph_info_t));
  if ( graph == NULL )
    return (NULL);

  graph->nnodes=nnodes;
  graph->nedges=index[nnodes-1];
  graph->index = (int *)_MALLOC( sizeof(int) * graph->nnodes );
  graph->edges = (int *)_MALLOC( sizeof(int) * graph->nedges );
  if ( (graph->index == NULL ) || (graph->edges == NULL ) )
    return (NULL);

  for(i=0;i<graph->nnodes;i++)
    {
      graph->index[i]=index[i];
    }

  for(i=0;i<graph->nedges;i++)
    {
      graph->edges[i]=edges[i];
    }

  return ( graph );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* free the arrays dims and periods first, and finally the 
   whole structure */
void ftmpi_graph_free (graph_info_t *graph)
{
  _FREE( graph->index);
  _FREE( graph->edges );
  _FREE( graph );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Allocate memory for the new structure, and copy elements */
graph_info_t* ftmpi_graph_copy (graph_info_t *graph) 
{
  graph_info_t *newgraph=NULL;

  newgraph = ftmpi_graph_set ( graph->nnodes, graph->index, graph->edges);

  return (newgraph);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* What shall we compare precisly ?
   - number of nnodes 
   - number of edges
   - each element of the index and edges list ?
*/

int ftmpi_graph_compare (graph_info_t *graph1, graph_info_t *graph2)
{
  int ret1, ret2, ret3, ret4;
  int i;
  
  if ( graph1->nnodes == graph2->nnodes )
    ret1 = MPI_IDENT;
  else
    ret1 = MPI_UNEQUAL;

  if ( graph1->nedges == graph2->nedges )
    ret2 = MPI_IDENT;
  else
    ret2 = MPI_UNEQUAL;

  /* if one of the conditions above doesn't match,
     we need not continue */
  if ( (ret1 != MPI_IDENT) || ( ret2 != MPI_IDENT ))
    return ( MPI_UNEQUAL );

  ret3 = MPI_IDENT;
  for ( i = 0; i < graph1->nnodes; i ++ )
    if ( graph1->index[i] != graph2->index[i] )
      {
	ret3 = MPI_UNEQUAL;
	break;
      }
  ret4 = MPI_IDENT;
  for ( i = 0; i < graph1->nedges; i ++ )
    if ( graph1->edges[i] != graph2->edges[i] )
      {
	ret3 = MPI_UNEQUAL;
	break;
      }
  
  if ( (ret3!=MPI_IDENT) || (ret4!=MPI_IDENT))
    return (MPI_UNEQUAL);
  else
    return (MPI_IDENT);
      
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_graph_get_nnodes ( graph_info_t *graph )
{
  return (graph->nnodes );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_graph_get_nedges ( graph_info_t *graph )
{
  return (graph->nedges );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int* ftmpi_graph_get_index_ptr ( graph_info_t *graph )
{
  return ( graph->index );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int* ftmpi_graph_get_edges_ptr ( graph_info_t *graph )
{
  return ( graph->edges );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_graph_calc_neighbors_count ( graph_info_t *graph, int rank)
{
  if (!rank )
    return ( graph->index[0] );
  else
    return ( (graph->index[rank] - graph->index[rank-1] ));

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_graph_calc_neighbors ( int* neighbors, int maxneighbors,
				 int rank, graph_info_t *graph)
{
  int num, from_edge;
  int i;

  if ( !rank )
    {
      num = graph->index[0];
      from_edge = 0;
    }
  else
    {
      num  = graph->index[rank] - graph->index[rank-1];
      from_edge = graph->index[rank-1];
    }

  if ( num > maxneighbors )
    num = maxneighbors;

  for ( i = 0 ; i < num; i++)
    neighbors[i] = graph->edges[from_edge+i];

  return ( MPI_SUCCESS );
}
