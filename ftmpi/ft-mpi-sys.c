
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* 
 This is the ft-mpi systems file. It handles all those confusing things like
 distributed state, failure detection and handling and anything involving state 

 Graham, HLRS Stuttgart 2001
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "mpi.h"
#include "ft-mpi-db-types.h"
#include "ft-mpi-common.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-group.h"
#include "ft-mpi-misc.h" /* array of GID handling routines */
#include "ft-mpi-ddt-sys.h"		/* DDT decode info needed by procinfo */
#include "ft-mpi-procinfo.h"	/* procinfo header */
#include "ft-mpi-convinfo.h"	/* get conversion info */
#include "ft-mpi-err.h"
#include "ft-mpi-com.h"
#include "ft-mpi-conn.h"
#include "ft-mpi-nb.h"
#include "ft-mpi-cinfo.h"
#include "ft-mpi-misc.h"


#include "envs.h"		/* this is from Harness g_hcore_d */

#include "snipe_lite.h"
#include "snipe2.h"
#include "msg.h"
#include "msgbuf.h"
#include "ns_lib.h"
#include "ns_id.h"		/* to get base address for IDs */
#include "libstartup.h"
#include "notifier.h"    /* get protos for notifier comms */
#include "debug.h"

/***************************************************************************/

extern int MAXGROUPS;

/* HARNESS NAME SERVICES */
/* char*   HARNESS_NS_HOST; */
/* int     HARNESS_NS_HOST_PORT; */
/* char*   HARNESS_RS_HOST; */
/* int     HARNESS_RS_HOST_PORT; */

/* Other globals */
int		conn_addr;		/* connection IPv4 address */
int		conn_as;		/* connection accept socket */
int		conn_p;			/* connection accept port */

int		ns_ci_group;		/* NS Contact info group */
							/* used for clearing it up */

/* these are used for state update messages */
int		state_as;		/* state change accept socket */
int		state_p;		/* state change accept port */

/* these are used for event update messages (i.e. EXIT notifies) */
int		event_as;		/* event detect accept socket */
int		event_p;		/* event detect accept port */

int		ileader_id;		/* initial guess at leader given by FTMPIRUN, discarded after startup */
int		leader_id;		/* GID of the leader */
int		leader_addr;	/* address of the leader */
int		leader_port;	/* conn port of the leader */
int		iamleader=0;	/* it could be you */

/* My local process info structure */
static ftmpi_procinfo_t*	my_procinfo_p;

/* my local conversion information mask */
unsigned int ftmpi_my_convinfo=0;

/* Inital runid */
int	runid=0;

/* my gid */
int	gid=0;

/* my parent (used to detect orphans, workhouse but not Mr Twist) */
int pgid=0;

/* Global communicator and message modes */
int gcommode = 0;
int gmsgmode = 0;

/* My arguments */
char my_argc;
char **my_argv;	/* pointer to argv */
char myexename[256];

/* expected/requested job size */
/* should be atleast '1'=me */
int rjs=0;

/* current low level global leader state */
int gstate=0;
int gepoch=0;
int gnproc=0;
int gextent=0;

/* state info in the form of lists */
int *ggids=NULL;	/* global list of gids */
					/* this should be between gnproc & gextent depending */
					/* error recovery state etc */

int *newgids=NULL;	/* temp gids list that leader builds during ACK phase */

int *gcaddr=NULL;    /* tmp list of addr for gids */
int *gcport=NULL;   /* tmp list of connect ports for gids */
int *gsport=NULL;   /* tmp list of state ports for gids */

unsigned int *gconvs=NULL;   /* tmp list of convinfo values */

int required_procs;	/* how many we should have if FT_MODE_REBUILD */

/* some one off variables */
int amioriginal=0;		/* i.e. was I started by FTMPIRUN or as a recover spawn */
int grank=-1;			/* my global rank as far as I can guess */
						/* this starts from STARTUP and get updated */
						/* by the BUILD state */

/* Error detected ? */
int error_d = 0;

/* monitoring and graphical output */
int displaycinfo = 0;
int displaycinfofound = 0;

/* output redirection */
static int	output_ip;
static int	output_port;

/* misc env info */
char ftmpi_dvm_name[256];
char ftmpi_cwdir[256];
int  ftmpi_silent=0;

/* list of processes failed since the last event */
static int* ftmpi_failed_list=NULL;
static int  ftmpi_failed_list_count=0;

/* recovery count for fun :) */
static int ftmpi_recovery_count=0;


/***************************************************************************/

void mkcinfoname (char*, int);	/* makes a contact name string */
void mklname (char*, int);	/* makes a leader name string */
void mksname (char*, int);		/* makes a state name string */
void mksupname (char*, int);		/* makes a startup name string */

/* clear the tmp cinfo lists */
void ftmpi_sys_clrtmp_cinfo (int);


/* actual abort operation */
void ftmpi_sys_mpi_abort ();

/* setting local information */
int ftmpi_sys_set_local_procinfo ();

int test_copy_gids (int *newlp, int s);
int test_dump_cinfo ();
int test_dump_all (int *ids);

int ftmpi_sys_show_all (char* label);

/* handle the message and req queues and tables */
/* i.e. remap requests or remove them depending on msg_mode CONT or RESET */
int ftmpi_sys_recovery_handle_queues (int orgcnt, int* orglist, int currentcnt, int* currentlist, int failedcnt, int* failedlist);


/***************************************************************************/

/* start everything routine */
int ftmpi_sys_init (int *argc, char **argv[])
{
int r;
char* envptr;	/* initial envs ptr */
int irid;
int igid;
int ipgid;
int irjs;
int ilid;
/* char cinfoname[256]; */	/* my contact info string */
int poffset;	/* port offset */
				/* used to avoid an exleader race and also help find */
				/* a free port when an application has a high proc turn over */

/* First see if we have a RUN ID */
/* IF not we are in trouble (until I put in the find my runid alt routine */

envptr = getenv ("FTMPI-RUNID");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No RUNID specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
else { /* get the runid and check its value */
	irid = atoi (envptr);
	if (irid<0) {
		fprintf(stderr,"Initial run ID was invalid [%d]\n", irid);
		fprintf(stderr,"Maybe a direct startup without ftmpirun or mpi_spawn.");
		exit (-1);
	}

	/* it looks ok, so save it */
	runid = irid;
}

envptr = getenv ("FTMPI-GID");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No GID specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
else { /* get the runid and check its value */
	igid = atoi (envptr);
	if (igid<0) {
		fprintf(stderr,"Initial global ID was invalid [%d]\n", igid);
		fprintf(stderr,"Maybe a direct startup without ftmpirun or mpi_spawn.");
		exit (-1);
	}

	/* it looks ok, so save it */
	gid = igid;
}

envptr = getenv ("FTMPI-PGID");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No Parent GID specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
else { /* get the runid and check its value */
	ipgid = atoi (envptr);
	if (ipgid<0) {
		fprintf(stderr,"Initial global Parent ID was invalid [%d]\n", ipgid);
		fprintf(stderr,"Maybe a direct startup without ftmpirun or mpi_spawn.");
		exit (-1);
	}

	/* it looks ok, so save it */
	pgid = ipgid;
}

envptr = getenv ("FTMPI-EXENAME");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No EXENAME? specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}

/* copy it and make sure it terminated */
strncpy (myexename, envptr, sizeof(myexename)-1);
myexename[sizeof(myexename)-1] = '\0';

envptr = getenv ("FTMPI-REQJOBSIZE");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No expected REGUESTED JOB SIZE specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	fprintf(stderr,"Maybe you are using an out of date ftmpirun?\n");
	exit (-1);
}
else { /* get the jobsize and check its value */
	irjs = atoi (envptr);
	if (irjs<0) {
		fprintf(stderr,"Initial Expected/requested job size was invalid [%d]\n", irjs);
		fprintf(stderr,"Maybe a direct startup without ftmpirun or mpi_spawn.");
		exit (-1);
	}

	/* it looks ok, so save it */
	rjs = irjs;
}

envptr = getenv ("FTMPI-LEADER");

if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No expected initial leader specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	fprintf(stderr,"Maybe you are using an out of date ftmpirun?\n");
	exit (-1);
}
else { /* get the initial leader and check its value */
	ilid = atoi (envptr);
	if (ilid<=0) {
		fprintf(stderr,"Initial leader ID was invalid [%d]\n", ilid);
		fprintf(stderr,"Maybe a direct startup without ftmpirun or mpi_spawn.");
		exit (-1);
	}

	/* it looks ok, so save it */
	ileader_id = ilid;
}

/* Now check if we need to talk to a display monitor of any kind */
/* if we do then the 'leader' will update the monitor */
/* later on the notifier will take over some of this functionality */

envptr = getenv ("FTMPI-DISPLAYCINFO");
if (envptr) displaycinfo = 1;
else
			displaycinfo = 0;


/* get output redirection */
/* first host which is really an IP address? */
envptr = getenv ("FTMPI-OUTPUT-HOST");
if (envptr) output_ip = atoi(envptr);
else
			output_ip = 0;

/* second is the port number */
envptr = getenv ("FTMPI-OUTPUT-PORT");
if (envptr) output_port = atoi(envptr);
else
			output_port = 0;

/* get vmname */
envptr = getenv ("FTMPI-VMNAME");
if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No VMNAME? specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
/* copy it and make sure it terminated */
strncpy (ftmpi_dvm_name, envptr, sizeof(ftmpi_dvm_name)-1);
ftmpi_dvm_name[sizeof(ftmpi_dvm_name)-1] = '\0';

/* get cwdir */
envptr = getenv ("FTMPI-CWDIR");
if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No CWDIR? specified by my starting daemon/method?\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
/* copy it and make sure it terminated */
strncpy (ftmpi_cwdir, envptr, sizeof(ftmpi_cwdir)-1);
ftmpi_cwdir[sizeof(ftmpi_cwdir)-1] = '\0';

/* get cwdir */
envptr = getenv ("FTMPI-SILENT");
if (!envptr) { /* NO env, search NS maybe? */
	fprintf(stderr,"No SILENT flag specified by my starting daemon/\n");
	fprintf(stderr,"Direct starts without ftmpirun not allowed.\n");
	exit (-1);
}
/* copy it and make sure it terminated */
ftmpi_silent = atoi(envptr);

/* ok just for now until FTMPIRUN is updated hard code it */

displaycinfo=1;

displaycinfofound = 0;	/* make sure */

/* VM name check? */
get_envs_local ();

/* thats all the ENV variables */

/* OK here we do an ophan check */


/* Make our accept sockets */

/* first get a port offset using our GID */
poffset = (gid - NS_ID_FTMPI_BASE) % 1000;

conn_p = 5000+poffset;	/* let the system choose */
conn_as = init_lsocket2 (&conn_p);
state_p = 7000+poffset;	/* let the system choose */
state_as = init_lsocket2 (&state_p);
event_p = 9000+poffset;	/* let the system choose */
event_as = init_lsocket2 (&event_p);

conn_addr = get_my_addr (); /* put after the socket creation so that */
							/* windows version does not get upset GEF */

if ((conn_p<0)||(state_p<0)) {
	fprintf(stderr, "Problem creating sockets for communication.\n");
	exit (-1);
}
else {
	if (!ftmpi_silent) printf("conn [%d:%d] state [%d:%d] event [%d:%d]\n",
				  conn_as, conn_p, state_as, state_p, 
				  event_as, event_p);

}

/* Here we tell the connection management library about it */
ftmpi_conn_set_local (gid, conn_addr, conn_as, state_as, event_as);
/* this can be done here are its Not a list item but a local variable */

/* OK, now get the rest of the stuff we need */

get_envs_ns  ();  /* get environment variables needed for the NS */

if (!HARNESS_NS_HOST) {
    if (!ftmpi_silent) fprintf(stderr,"No HARNESS_NS_HOST set\n");
    return (-1);
}

if (!HARNESS_NS_HOST_PORT) {
    if (!ftmpi_silent) fprintf(stderr,"No HARNESS_NS_HOST PORT set\n");
    return (-1);
}

r = ns_init (HARNESS_NS_HOST, HARNESS_NS_HOST_PORT);

if (r) {
    if (!ftmpi_silent) fprintf(stderr,"Harness NS host not contactable\n");
    return (-1);
}

/* now publish our contact info */
#ifdef PUBCONREC
mkcinfoname (cinfoname, gid);
r = 0;
r += ns_add (cinfoname, conn_addr, conn_p, &i1, &i2);
r += ns_add (cinfoname, conn_addr, state_p, &i1, &i2);
r += ns_add (cinfoname, conn_addr, event_p, &i1, &i2);
if (r) {
	fprintf(stderr,"Could not publish my contact info [%s]\n", cinfoname);
	ns_close ();
	return (-1);
	}
else
	ns_ci_group = i1; /* remember our group */
#endif

ns_close ();	/* we open it when we need it again later */
				/* That way we don't lock up the name_service */

/* OK we now also have to tell the notifier about ourself now */
/* that we have a set of ports and addresses. */

/* first find the notifier */
r = notifier_find ();

/* Then add me to the list of those of need to know */
/* about the ways of the world (death destruction missed xmass/new year etc) */
r = notifier_addme (runid, gid, conn_addr, event_p);

/* now just general stuff like message buffers and then I am done */
r =  init_msg_bufs ();

if (r<0) {
	fprintf(stderr,"Internal message buffer problem?\n");
	return (-1);
}

/* clear our event loop */
r = notifier_clear_all_events ();


/* remember arguments (well their pointer and count) */
/* I have a bad bad feeling about this ifdef.... */
/* includes a quick fix from tone for NULL args from F2c programs */
if (!ftmpi_silent) printf("Pre ARG check\n"); fflush(stdout);
if(argc != NULL){
	    my_argc = *argc;
	      }
  else {
	      my_argc = 0;
	        }

  if(argv != NULL){
	      my_argv = argv[0];  /* ptr to first */
	        }
  else {
	      my_argv = NULL;
	        }
if (!ftmpi_silent) printf("RUNID 0x%x GID 0x%x PGID 0x%x exe [%s] RJS %d "
		       "ILeader 0x%x\n", runid, gid, pgid, myexename, 
		       rjs, ileader_id);
return (0);	/* initial startup ok */
}

/***************************************************************************/

/* Finally I get to read the initial state at startup stuff */
/* This allows me to set leaders and all that jaz... */

/* the tout value is based on 1 sec per tout unit */
/***************************************************************************/

int ftmpi_sys_init_startup_state (int tout)
{
  int r;
  int i, mb;
  int *ogids; /* original GID list */
  long delay=0;
  
  /* 
     we loop arount tout time for the FTMPI-RUN produced startup record to appear
  */
  
  /* if we are NOT the expected leader, we delay looking for the startup record */
  /* this is the first of two delays that non leaders do, so that leaders can do their work faster */
  
  
  if (gid!=ileader_id) {
    if (!ftmpi_silent) printf("I am GID %x and Expected Leader is %x, "
			      "delaying search for startup_record\n",
			      gid,ileader_id);
    delay=(long)rjs;								/* delay factor of requested job size */
    delay*=FTMPI_WAITFOR_LEADER_STARTUP_DELAY;		/* default was 60 ms per node */
    if (delay>0) usleep (delay);
    if (!ftmpi_silent) 
      printf( "I am GID %x finished delaying search for startup_record\n",
	     gid );
  }
  
  mb = -1;
  r = 0;
#ifdef FTMPI_SYS_STARTUP_POLL
  r = ftmpi_poll_for_db_record(&mb,gid,0,0,DB_FT_MPI_STARTUP,runid,0,tout,0);
#else
  
  /* 	printf("ftmpi_wait_for_db_record on startup 0x%x %d\n", runid, runid); */
  /* 	fflush(stdout); */
  
  mb=ftmpi_wait_for_db_record(DB_FT_MPI_STARTUP,runid,0,0,0,0); /* use auto unregister (one time callback only) */
  
  /* 	printf("ftmpi_wait_for_db_record mb = %d\n", mb); */
  /* 	fflush(stdout); */
  
#endif /* FTMPI_SYS_POLL_NS */
  
  
  if ((r<0)||(mb<0)) { /* this is very bad. FTMPI never uploaded any state to the
			  NS! or it was blank? */
    fprintf(stderr,"ft_mpi_sys_init_startup_state: FTMPIRUN startup record CANNOT BE FOUND or was corrupt (check that with ns_dump NSHOST NSPORT).\n Cannot recover from that. Check the NS and FTMPIRUN error message log.\n");
    
#ifdef PUBCONREC
    clearup_ns_records (gid);
#endif
    exit (-9);
  }
  
  /* OK, so we have a record */
  /* get the info from it and then maybe, build communicators etc etc */
  
  upk_int32 (mb, &gstate, 1);
  upk_int32 (mb, &leader_id, 1);
  upk_int32 (mb, &gepoch, 1);
  upk_int32 (mb, &gnproc, 1);
  upk_int32 (mb, &gextent, 1);
  
  /* OK read the original GID list as well (so we know if we are are restart) */
  ogids = (int*)_MALLOC (gnproc*sizeof(int));
  
  upk_int32 (mb, ogids, gnproc);	/* get original gid list out */
  
  /* and finally the commicator and message modes (or how to hanle failures) */
  upk_int32 (mb, &gcommode, 1);
  upk_int32 (mb, &gmsgmode, 1);
  
  free_msg_buf (mb);	/* it should be free anyway */
  
  /* If we are FT_MODE_REBUILD, then we need a min num of proc */
  
  if (gcommode==FT_MODE_REBUILD) required_procs = gnproc;	/* REMEMBER IT */
  else 
    required_procs = 1;	/* i.e. me :) */
  
  /* I think all is well, so I'll update the ggids list here now as well */
  ftmpi_array_free (&ggids);
  ftmpi_array_mkspace (&ggids, gextent);
  for(i=0;i<gnproc;i++) ftmpi_array_add (ggids, gextent, ogids[i]);
  _FREE(ogids);	/* free it... */
  
  /* now check to see if I am original */
  /* and also note my grank */
  amioriginal=0;
  grank = ftmpi_array_rank (ggids, gextent, gid);
  if (grank>=0) amioriginal = 1;
  
  /* #ifdef REALVERBOSE */
  if (!ftmpi_silent) printf("ftmpi_sys_init_startup_state: state %d leader %x ep %d nproc %d ext %d\n\t\t\tgrank %d original %d comm_mode %d message_mode %d\n",
			    gstate, leader_id, gepoch, gnproc, gextent,
			    grank, amioriginal, gcommode, gmsgmode);
  fflush(stdout);
  /* #endif */
  if (leader_id==gid)  { /* I am the leader ?! */
    iamleader = 1;
    ftmpi_sys_new_leader_first_time_only ();
  }
  else { /* I am not the leader, so I try and find his contact info... */
    iamleader = 0;
    ftmpi_sys_get_new_leader_first_time_only (FTMPI_WAITFOR_LEADER_LOOPS, grank);
    if ((!amioriginal)&&(pgid!=leader_id)) { /* I am an orphan */
      fprintf(stderr, "0x%x I am orphanded. My parent 0x%x is not the current leader 0x%x. Don't want to go to the workhouse so am leaving now.\n", 
	      gid, pgid, leader_id);
      ftmpi_sys_leave (0, 0);
    }
  }	
  
  /* here we start to interact with the DARK/MPI side */
  /* these have been moved to MPI_Init via sys_start_mpi */
  
  /* ftmpi_com_init ();	 */
  /* ftmpi_conn_init ();	 */
  /* ftmpi_procinfo_init_htable (rjs);  */
  /* ftmpi_sys_set_local_procinfo ();   */
  
  /* setup free lists for communication libraries here as well */
  /* ftmpi_nb_init_reqfreelist (); */
  
  /* we also build MPI COMM WORLD here */
  /* ftmpi_com_build (MPI_COMM_WORLD, gid, gcommode, gmsgmode, gepoch, ggids, */
  /* 					gextent, gnproc, 1); */
  
  
  /* ok, if I am a leader, I can now post for the first time the MCW to the monitor ! */
  /* if (leader_id==gid)  */
  /* 	if (displaycinfo&&displaycinfofound) {  */
  /* 		ftmpi_sys_monitor_post_mcw (1);	 */
  /* 	}	 */

  return (amioriginal);
}

/***************************************************************************/

/***************************************************************************/

void ftmpi_sys_start_mpi ()
{
ftmpi_com_init ();  /* clears the DS for communicators */
ftmpi_conn_init (); /* clears the DS for the connection list */

snipe2_init (MAXPERAPP+2);		/* start low level communications library */
								/* +2 as we need N-1 +2 channels max */
conn_snipe2_setup_callbacks (); /* done asap :) */
   
ftmpi_procinfo_init_htable (rjs); /* setup the procinfo table DS */
ftmpi_sys_set_local_procinfo ();  /* put myself in it */

/* setup all NB lists for the communication libraries here as well */
ftmpi_nb_init_req_and_msg_lists ();

/* we also build MPI COMM WORLD here */
ftmpi_com_build (MPI_COMM_WORLD, gid, gcommode, gmsgmode, gepoch, ggids,
					gextent, gnproc, 1);
/* ok, if I am a leader, I can now post for the first time the MCW to the monitor ! */
if (leader_id==gid) /* I am the leader */
	if (displaycinfo&&displaycinfofound) { /* if we can even display anything */
		ftmpi_sys_monitor_post_mcw (1);	/* 1 =  first time baby */
	}	


/* this is moved before the recovery loop or else the */
/* set_ddt_decode in the recovery loop will fail... GEF */
ftmpi_ddt_init ();

/* Initiate the error-code and error-class lists */
ftmpi_errcode_and_errclass_init ();

/* if we get a failure at this point we want the system to send death events */
conn_set_conn_fail_sends_events_flag (1);

}
/***************************************************************************/

void	ftmpi_sys_leave (int allowdeathevent, int remove_runstate_record)
{
int rc;
char rname[256];


/* stop connection failures sending events from now on */
conn_set_conn_fail_sends_events_flag (0);

if ((!allowdeathevent)) notifier_removeall (runid, gid);	
	/* removes all of runid from notifier */ 

/* will put this back as soon as we have a local startup daemon watchlist
 * notifier cancel
 */

/* two ways to do this */

#ifdef NONSYNCEXIT
/* nonsyncexits are faster but you can get unuseful exit messages */

/* Remove callback channels on events and new connections */
conn_snipe2_disable_newconnections_and_events ();

/* before stopping all communications completely, send XOFFs */
conn_stop_all_coms ();

/* now we are all syncd up */
/* Stop all communications completely */
conn_halt_all_coms ();

#else /* SYNC STYLE EXIT (slower but safer) */
/* Remove callback channels on events and new connections */
conn_snipe2_disable_newconnections_and_events ();

/* before stopping all communications completely, do a sync flush */
conn_stop_and_flush ();

/* now we are all syncd up */
/* Stop all communications completely */
conn_halt_all_coms ();

#endif /* SYNCEXIT */



/* remove my contact info first */
if (!iamleader) {

	ftmpi_unreg_wait_for_db_record(DB_FT_MPI_STATE,runid,0);
/* 	if (!allowdeathevent) notifier_removeme (runid, gid); */
#ifdef PUBCONREC
	/* we can always clean up ns records as the notifer does not lookup */
	/* via the ns any event addresses and ports */
	clearup_ns_records (gid);
#endif
	return;
	}

/* If I am the leader, post halt, clear all NS records etc */

/* first stop all notifier messages */
/* if I need a death event for me then we do not remove notifer data */
/* this will cause everyone to get my state change after I post ABORT..? */

/* if ((iamleader)&&(!allowdeathevent)) notifier_removeall (runid, gid);	 */
/* older version */
/* we make everyone do a complete removeall above :) */


/* then remove all NS contact records */
#ifdef PUBCONREC
for (i=0;i<gextent;i++) if (ggids[i]) clearup_ns_records (ggids[i]);
#endif

/* Now all other records */

ns_open ();


/* Now the leader record */
mklname (rname, runid);
/*
rc = ns_record_get (gid, rname, RECORD_DB_REMOVE | RECORD_DB_INDEXED, 
					0, NULL, NULL, NULL, NULL);
*/

/* TA use record_del instead of record_get to clean all callback of the record */
rc = ns_record_del (rname);

/* and now the original startup record */
mksupname (rname, runid);
/*
rc = ns_record_get (gid, rname, RECORD_DB_REMOVE | RECORD_DB_INDEXED, 
					0, NULL, NULL, NULL, NULL);
*/

/* TA use record_del instead of record_get to clean all callback of the record */
rc = ns_record_del (rname);

ns_close ();

/* if we remove this too other processes might not ever know what happened! */
/* so we sleep a little before doing this.. sorry */
/* this delay isn't really needed except we want to make sure all know */

if (remove_runstate_record) {
	sleep (1);
	ns_open ();
	mksname (rname, runid);
	/*
	rc = ns_record_get (gid, rname, RECORD_DB_REMOVE | RECORD_DB_INDEXED, 
					0, NULL, NULL, NULL, NULL);
       */

       /* TA use record_del instead of record_get to clean all callback of the record */
       rc = ns_record_del (rname);
       ns_close ();
}

return;
}

/***************************************************************************/

/* Remove contact records if they exist */
/***************************************************************************/
int	clearup_ns_records (tgid) 
int tgid;
{
char cinfoname[256];
int i1, i2;

/* if I am trying to delete someone else record */
if ((tgid!=gid) && (!iamleader)) return -1; /* I had better be the leader */

if (gid==tgid)  /* if us */
	if (ns_ci_group>=0) {	/* we know where the entry is delete it */
		mkcinfoname (cinfoname, tgid);
		ns_open ();
		ns_del (cinfoname, ns_ci_group, 0);	/* conn_a/p info */
		ns_del (cinfoname, ns_ci_group, 1);	/* state_a/p info */
		ns_del (cinfoname, ns_ci_group, 2);	/* event a/p info */
		ns_close ();
		fprintf(stderr,"Cleared up 0x%x contact info from NS\n", tgid);
		return 0;
		}

/* ok find the group this entry belongs to and then clear it */
mkcinfoname (cinfoname, tgid);
ns_open ();
ns_info (cinfoname, 0, &i1, &i2, NULL, NULL, NULL);
/* i1 = group */
if (i1>=0) { /* if its still there */
	ns_del (cinfoname, i1, 0);	/* conn_a/p info */
	ns_del (cinfoname, i1, 1);	/* state_a/p info */
	ns_del (cinfoname, i1, 2);	/* event a/p info */
	fprintf(stderr,"Cleared up 0x%x contact info from NS\n", tgid);
	}
ns_close ();
return 0;
}

/***************************************************************************/
/* local routine to set the local procinfo record for this process */
/* procinfo_init must have already been called */
/***************************************************************************/
int ftmpi_sys_set_local_procinfo ()
{
   my_procinfo_p = ftmpi_procinfo_new (gid); /* this creates the local record */
   my_procinfo_p->pl_conn_index =-1;
/*    local->pl_conn = NULL; */
   my_procinfo_p->pl_mcw_rank = grank;

   /* get my local convinfo value */
   ftmpi_my_convinfo=0;
   ftmpi_convinfo_mysettings (&ftmpi_my_convinfo);
   /* put it in the procinfo structure */
   my_procinfo_p->pl_convinfo= ftmpi_my_convinfo;

   /* other local info that we exchange during a rebuild etc */
   my_procinfo_p->pl_conn_addr = conn_addr ;
   my_procinfo_p->pl_conn_p = conn_p ;
   my_procinfo_p->pl_state_p = state_p ;
   my_procinfo_p->pl_event_p = event_p ;
   return (0);
}

/***************************************************************************/
/* inquiry routines */
/***************************************************************************/

int ftmpi_sys_get_runid ()
{
return (runid);
}

/***************************************************************************/
int ftmpi_sys_get_gid ()
{
return (gid);
}

/***************************************************************************/
int ftmpi_sys_get_grank ()
{
return (grank);
}

/***************************************************************************/
int ftmpi_sys_get_leader ()
{
return (leader_id);
}

/***************************************************************************/
/* ID handling routine to be used by the communicator calls */
/* this is here so we isolate all NS calls */
/***************************************************************************/

int ftmpi_sys_getnextcomid ()
{
int rc;
int ngid=0;

rc = ns_open ();
if (rc<0) return (-1);

rc = ns_gid (NS_ID_FTMPI_COMS, 1, &ngid);
/* printf("ns_gid on %d returned %x\n", NS_ID_FTMPI_COMS, rc); */

ns_close ();

if (rc<0) return (rc);
else
	return (ngid);
}

/***************************************************************************/
/* simple routines first */

/***************************************************************************/
/* short hand helper routine to check states */
/* This one waits for the state to change from its previous value */
/* to a different one that is returned. if the state changes it reports 0 */
/* if the leader dies, it reports -1 */
/***************************************************************************/

int ftmpi_sys_waitfor_new_state (int *newstate, int oldstate, int leader)
{
int tid;
int rc, done;
int tmp[10];
int nstate, nleader;
/* check leader is not already dead! */

/* flush events to the event loop! */
rc = notifier_get_queued_events (event_as, runid, &tid, -1);	

/* check the loop for our leader! */
rc = find_event_by_gid (leader);
if (rc) return (-1);

done =0;
while (!done) { /* loop for new event OR dead leader */

	rc = ftmpi_sys_get_state (&nstate, &nleader, &tmp[1], &tmp[2],
								&tmp[3], NULL, 0, 0, 1);

	/* we check for dead leaders first */
	if (rc==1) { /* flush events and then search */
		rc = notifier_get_queued_events (event_as, runid, &tid, -1);
		if (find_event_by_gid (leader)) return (-1); /* leader did die */
		} /* all others are in the loop for later */
		
	if ((rc==0)&&(nstate!=oldstate)) {	/* state changed in a good way */
		*newstate=nstate;
		return (0);
		}
	
	if ((rc==0)&&(leader!=nleader)) {	
			/* only happens if I miss election! */
			/* do not cache it, so I do vote anyway */
		return (-1);		/* force me to goto election */
		}

#ifndef FTMPI_SYS_STATE_PUSH
	/* 	ftmpi_sys_get_state has a conf delay, but add a little more */
	/* this is because it only waits if the record does not exist! */
	usleep (100000);
#endif /* FTMPI_SYS_STATE_PUSH */

	} /* while not done */
 return 0; 
}


/***************************************************************************/
/* like how to check state */
/***************************************************************************/

int	ftmpi_sys_get_state (int *state, int *lleader, int *lepoch, 
				int *lnproc, int* lextent, int** lpp,
				int updatecinfo, int loops, int checkerr) 
{
int rd=0;			/* record handle */
int r=0;
/* char sname[256]; */
int a[5];	      /* tmp vars used to get state info out of the NS */
/* int buf, base,len; */ /* debug */
/* int *ids; */


rd = 0;

if (checkerr) 
	r = ftmpi_poll_for_db_record (&rd, gid, 0, 0, DB_FT_MPI_STATE, runid, 0, 
							loops, event_as);
else 
	r = ftmpi_poll_for_db_record (&rd, gid, 0, 0, DB_FT_MPI_STATE, runid, 0, 
							loops, 0);
#ifdef REALVERBOSE
printf ("ftmpi_poll_for_db_record on DB_FT_MPI_STATE and runid %d returned %d\n", 
			runid, r); fflush(stdout);
#endif

if (!r) { /* we found a record so get the info needed */
	/* but check its message buffer first */
	if(rd<0) return (-1);	/* VERY VERY BAD */

	/* else we have valid state info so return it */
	upk_int32 (rd, a, 5);

	/* if BUILD we need to pass data back to a list */
	if ((a[0]==FT_STATE_BUILD)&&(lpp)) {			/* gids */
		if (*lpp) ftmpi_array_free (lpp);	/* paranoid */
		ftmpi_array_mkspace (lpp, a[4]+1);
		upk_int32 (rd, *lpp, a[4]);
		}

	/* if its a build and we can update our cached contact info! */
	if ((a[0]==FT_STATE_BUILD)&&(updatecinfo)) {	/* conn addrs */

		ftmpi_sys_clrtmp_cinfo (a[4]+1);	/* clear the old values */

		/*		upk_int32 (rd, gcaddr, a[4]); */
		upk_raw32 (rd, gcaddr, a[4]);
		upk_int32 (rd, gcport, a[4]);
		upk_int32 (rd, gsport, a[4]);
		upk_int32 (rd, gconvs, a[4]);

	/* debugger */

#ifdef DB_PEON_SHOW_STATE_BUILD
		ids = *lpp;
		for(i=0;i<a[4];i++){
			printf("STATE_BUILD %d gid %d addr %x cport %d sport %d conv 0x%x\n",
					i, ids[i], gcaddr[i], gcport[i], gsport[i], gconvs[i]);
		}
#endif /* DB_PEON_SHOW_STATE_BUILD */


	}



	free_msg_buf (rd);	/* it should be free anyway */

	if (state) *state=a[0];
	if (lleader) *lleader=a[1];
	if (lepoch) *lepoch=a[2];
	if (lnproc) *lnproc=a[3];
	if (lextent) *lextent=a[4];
	}

return (r);
}


/***************************************************************************/
/* ftmpi_sys_wait_state - push version of ftmpi_sys_get_state */
/***************************************************************************/

int	ftmpi_sys_wait_state (int *state, int *lleader, int *lepoch, 
				int *lnproc, int* lextent, int** lpp,
				int updatecinfo, int loops, int checkerr) 
{
int rd=0;			/* record handle */
/* char sname[256]; */
int a[5];	      /* tmp vars used to get state info out of the NS */
/* int buf, base,len; */ /* debug */
/* int *ids; */


rd = 0;

if (checkerr) 
	rd = ftmpi_wait_for_db_record(DB_FT_MPI_STATE,runid,0,0,event_as,1); 
else 
	rd = ftmpi_wait_for_db_record(DB_FT_MPI_STATE,runid,0,0,0,1); 

#ifdef REALVERBOSE
printf ("ftmpi_wait_for_db_record on DB_FT_MPI_STATE and runid %d returned %d\n", 
			runid, r); fflush(stdout);
#endif

if (rd >=0 ) {

	upk_int32 (rd, a, 5);

	/* if BUILD we need to pass data back to a list */
	if ((a[0]==FT_STATE_BUILD)&&(lpp)) {			/* gids */
		if (*lpp) ftmpi_array_free (lpp);	/* paranoid */
		ftmpi_array_mkspace (lpp, a[4]+1);
		upk_int32 (rd, *lpp, a[4]);
		}

	/* if its a build and we can update our cached contact info! */
	if ((a[0]==FT_STATE_BUILD)&&(updatecinfo)) {	/* conn addrs */

		ftmpi_sys_clrtmp_cinfo (a[4]+1);	/* clear the old values */

		upk_raw32 (rd, gcaddr, a[4]);
		upk_int32 (rd, gcport, a[4]);
		upk_int32 (rd, gsport, a[4]);
		upk_int32 (rd, gconvs, a[4]);

	/* debugger */

#ifdef DB_PEON_SHOW_STATE_BUILD
		ids = *lpp;
		for(i=0;i<a[4];i++){
			printf("STATE_BUILD %d gid %d addr %x cport %d sport %d convs %x\n",
					i, ids[i], gcaddr[i], gcport[i], gsport[i], gconvs[i]);
		}
#endif /* DB_PEON_SHOW_STATE_BUILD */


	}

	free_msg_buf (rd);	/* it should be free anyway */

	if (state) *state=a[0];
	if (lleader) *lleader=a[1];
	if (lepoch) *lepoch=a[2];
	if (lnproc) *lnproc=a[3];
	if (lextent) *lextent=a[4];

	}

   return rd;
}

/***************************************************************************/

#ifdef PUBCONREC
/***************************************************************************/
/*
 Here we loop until we find the contact info required to communicate
 with someone.
 This routine generally does NOT need to be used if a BUILD state has occured
*/
/***************************************************************************/

int	ftmpi_sys_get_conninfo (int tgid, int *values,  int loops, int checkerr) 
{
int err_d=0;	/* error detected ? */
int rd=0;			/* record handle */
int r=0;
int es=0;			/* error socket if there is a failure while we are here */
int r2;
char cname[256];
int a[3];			/* tmp vars (addrs) used to get state info out of the NS */
int b[3];			/* tmp vars (ports) */
int buf, base,len;	/* debug */
int i1, i2;

rd = 0;
if (checkerr) 
	r = ftmpi_poll_for_db_record (&rd, gid, 1, 0, DB_FT_MPI_CONN, runid, tgid, 
							loops, event_as);
else 
	r = ftmpi_poll_for_db_record (&rd, gid, 1, 0, DB_FT_MPI_CONN, runid, tgid, 
							loops, 0);

#ifdef REALVERBOSE
printf ("ftmpi_poll_for_db_record on DB_FT_MPI_CONN and runid %x gid %x returned %d\n", runid, tgid, r);
#endif

if (!r) { /* we found a record so get the info needed */
	/* but check its message buffer first */
	mkcinfoname (cname, tgid);
	ns_open ();
	ns_info (cname, 3, &i1, &i2, NULL, a[3], b[3]);
	ns_close ();
	if (i1>=0) { /* valid data */
		if (values) { /* if they want the info back */
			values[0] = a[0]; values[1] = b[0];	/* conn a/p */
			values[2] = a[1]; values[3] = b[1]; /* state a/p */
			values[4] = a[2]; values[5] = b[2];	/* event a/p */
			/* conv info not included ? */
			}	
		return (0);	/* data returned ok */
		}
	}
return (-1);	/* no conn info */
}
#endif /* PUBCONREC */

/***************************************************************************/
/*
 This is a general get ack code (used by non leaders) 
 We break out when we have got 'an' ack or the target gid died! 
 If we set tid as -1 we break after ANY acks or after ONE EVENT
*/
/***************************************************************************/
int	ftmpi_sys_waitfor_ack (int tid, int tepoch, int *val)
{
int lid;
int rc;
int done=0;
int tmp;

/* 
 Make sure they are not already dead!
 */

if (tid!=-1) {
	rc = find_event_by_gid (tid); 
	if (rc) return (-1); /* yep they are ALREADY deaded */
	}

while (!done) {
	lid = 0;
	rc = ftmpi_sys_check_for_ack (&lid, NULL, NULL, NULL, NULL, val, 0, tepoch);
						/* poll for ack */
	if ((rc==1)&&(tid==lid)) return (0); /* as we got an ack from target */
	if ((rc==1)&&(tid==-1)) return(0);	/* we got an ack from anybody! */

	/* ok, no ack, so.. erm did the target die? */
	tmp = 0;
	ftmpi_sys_check_for_events (&tmp, 0);

	if (tmp==tid) return (-1);	/* our target died! */ 
	if ((tmp>0)&&(tid==-1)) return (-2); /* if we just care about anyone */
	
	/* else nothing happened, so emmm sleep a very small amount */
	usleep (1000);
	} /* not done */
 return 0; 
}

/***************************************************************************/

void	mkcinfoname (char *p, int id)
{
if (p) sprintf(p,"FTMPI:conn:%x:%x", runid, id);
return;
}

/***************************************************************************/
void	mklname (char *p, int id)
{
if (p) sprintf(p,"FTMPI:leader:%x", id);
return;
}

/***************************************************************************/
void	mksname (char *p, int id)
{
if (p) sprintf(p,"FTMPI:state:%x", id);
return;
}

/***************************************************************************/
void	mksupname (char *p, int id)
{
if (p) sprintf(p,"FTMPI:startup:%x", id);
return;
}

/***************************************************************************/
/*
 This routine gets an ack message. If max=0 it means probe
 It expects to be able to return ATLEAST one GID.
 It discards messages with the wrong epoch tag used to indicate 
 which communicator rebuild cycle we are in.
 an epoch of -1 always matchs though
 But it should not be used in normal ACK, just ring loops
*/
/***************************************************************************/

int  ftmpi_sys_check_for_ack (int *ids, int *caddr, int *cport, 
			      int *sport, int *convs, int *vals, int max, 
			      int tepoch) 
{
int i, rc;
int k, s;
int from, ntag, tags[10];
int done=0;
int found=0;
int tout;
int mb;

if (max) tout = 10000;  /* we take our time more */
/* if you only want to probe, we are quicker, i.e. as fast as possible */
else    tout = 1000;	/* ack checking can be slower than event probing */  

while (!done) {
    k = pollconn (state_as, 0, tout);
                        /* should be there or else why did we call it ? */

	if (k>0) { /* yep we have one or more */
    	s = allowconn (state_as, 0, 0);
    	if (s>0) {
        	ntag = 7; /* proto_ack, runid, gid, epoch, ca, cp, sp, convinfo */
        	for(i=0;i<ntag;i++) tags[i]=0;

        	rc= recv_pkmesg (s, &from, &ntag, tags, &mb);

		if (tepoch==-1) tepoch=tags[3]; /* if epoch = -1 always match */

        	if ((ntag==7)&&(tags[0]==FTMPI_SYS_ACK)&&(tags[1]==runid)&&
		    (tags[3]==tepoch)) {
		  if (ids) ids[found] = tags[2];  /* return it to caller */
		  /* if (caddr) caddr[found] = tags[4];*/	/* c addr */
		  if (cport) cport[found] = tags[4];	/* c port */
		  if (sport) sport[found] = tags[5];	/* s port */
		  if (vals) vals[found] = tags[6];	/* extra values */
		  if (caddr)  upk_raw32 ( mb, &caddr[found], 1 ); /* IPv4 address */
		  if (convs) upk_int32 (mb, &convs[found], 1); /* conv info */
		  free_msg_buf ( mb );

		  found++; /* update local count */
            	} /* if valid exit message that we care about */
		else
		  if (!ftmpi_silent) printf("0x%x rid 0x%x ep %d ftmpi_sys:get_ack:bad message from 0x%x dropped rid 0x%x ep %d\n", gid, runid, tepoch, from, tags[1], tags[3] );
        	} /* valid socket */
    	closeconn(s);
    }

	if (found>=max) break;  /* no more so we stop */
	/* we break as soon as we are finished */
} /* while looking for ACKS */

return (found);
}


/***************************************************************************/
/*
 This routine gets an event. If max=0 it means probe
 if max=-1, it flushes all events to the event_loop and return events flushed
 It relies on the event routines in the libnotifier library in
 plugins/ftmpi
 It expects to be able to return ATLEAST one GID.
*/
/***************************************************************************/

int		ftmpi_sys_check_for_events (int *ids, int max)
{
int rc;

rc = notifier_get_queued_events (event_as, runid, ids, max);
/* if (rc>0) dump_event_loop (); */
return (rc);
}


int		ftmpi_sys_elect_new_leader()
{
int mb;
char rn[256];
int i1, i2;
int r;
int rgen;

/* OK, everybody races where with an atomic swap on the record DB */

mb = get_msg_buf_of_size (128, 0, 0);	/* get a small buffer */
if (mb<0) { /* debug */
	printf("mb=%d\n", mb);
	dump_msg_bufs ();
	fflush(stdout);
	exit(-1);
	}


/* The leader record is just the LEADERs GID, ADDR and PORT values */
pk_int32(mb, &gid, 1);
pk_raw32(mb, &conn_addr, 1);
pk_int32(mb, &state_p, 1);

mklname(rn, runid);
ns_open ();
r = ns_record_swap (gid, rn, mb, RECORD_DB_INDEXED, 0, leader_id, &i1, &i2, 1);
/* 1 at end means we delete mb after request */
mb = -1;

if (!r) { /* we are now the leader.. wow! */
	ns_close ();
	leader_id = gid;
	iamleader = 1;
	return (leader_id);
	}

if (r<0) { /* we failed */
	/* so we just read the record to find out who the new leader is instead */
	r = ns_record_get (gid, rn, RECORD_DB_INDEXED, 0 , &i1, &i2, &mb, &rgen);
	ns_close ();
	}

if(r<0) { /* failed to read the leader record??? */
	fprintf(stderr,"ft_mpi_sys_elect_new_leader:pathalogical state failure.\n");
	exit (-999);
	}

/*upk_int32 (mb, tmp, 3); */	/* get all the data */
/* leader_id = tmp[0];
leader_addr = tmp[1];
leader_port = tmp[2]; */

upk_int32(mb, &leader_id, 1);
upk_raw32(mb, &leader_addr, 1);
upk_int32(mb, &leader_port, 1);

free_msg_buf (mb);
iamleader = 0;

return (leader_id);
}


/***************************************************************************/
/* Attempts to start one process up */
/* uses round robin on any available startup_ds it can find */

/* some of this is not neat, but its much nicer than the writev rpc call */
/* used before the libstartup version below */
/***************************************************************************/

#define MAX_ARGS 13
int ftmpi_sys_spawn ( )
{
  int rc;
  int nsd;
  int tmpgid;
  char **envp;        /* inital ENV values */
  int i;
  int notifier = 0;
  static int last_nsd=0;
  int tnsd;
  
  /* nsd = startup_finder_vm (HARNESS_LOCAL_VMNAME);      */
  nsd = startup_finder_vm (ftmpi_dvm_name);     
  /* one stop GEF startup_d finder :) Modified by Thara */
  
  /* Now make sure last_nsd fits incase we lost a startup_d since last time */
  last_nsd = last_nsd % nsd;
  
  /* check for really really bad error */
  if (nsd<0) {
    fprintf(stderr,"ft_mpi_sys:ft_mpi_spawn: Cannot find any startup_d's?\n");
    return (-1);
  }
  
  /* if the notifier service exists */
  /* find it and use it */
  
  notifier = 0;
  notifier =  notifier_find ();
  
  /* We have to pass each process some envs */
  /* so we make some space here for that */
  envp = (char**)_MALLOC (14*sizeof(char*));
  for(i = 0; i < MAX_ARGS; i++ ) {
    envp[i] = (char *)_MALLOC (256);
  }
  envp[MAX_ARGS] = NULL;
  
  /* we can set the first ENV value */
  sprintf(envp[0],"FTMPI-RUNID=%d", runid);
  
  /* the second is the GID */
  ns_open ();
  rc = ns_gid (NS_ID_FTMPI, 1, &tmpgid); /* get a global ID for this proc */
  ns_close ();
  sprintf(envp[1],"FTMPI-GID=%d", tmpgid);
  
  /* the third is the ns host and forth the ns port */
  sprintf(envp[2],"HARNESS_NS_HOST=%s", getenv("HARNESS_NS_HOST"));
  sprintf(envp[3],"HARNESS_NS_HOST_PORT=%s", getenv("HARNESS_NS_HOST_PORT"));
  
  /* we can set the parent ENV value */
  sprintf(envp[4],"FTMPI-PGID=%d", gid);	/* I want to be the parent */
  
  /* we can set the exename ENV value */
  sprintf(envp[5],"FTMPI-EXENAME=%s", myexename);	/* I want to be the parent */
  
  /* the follow could be left blank.. but.. init_startup would bail if we did */
  /* REQJOBSIZE */
  sprintf(envp[6],"FTMPI-REQJOBSIZE=%d", rjs);	/* inital requested job size */
  
  /* initial leader before we find the LEADER record in the NS */
  sprintf(envp[7],"FTMPI-LEADER=%d", leader_id);					/* set it to me :) */
  
  sprintf(envp[8],"FTMPI-OUTPUT-HOST=%d",output_ip);
  sprintf(envp[9],"FTMPI-OUTPUT-PORT=%d",output_port);
  sprintf(envp[10],"FTMPI-VMNAME=%s",ftmpi_dvm_name);
  sprintf(envp[11],"FTMPI-CWDIR=%s",ftmpi_cwdir);
  sprintf(envp[12],"FTMPI-SILENT=%d",ftmpi_silent);
  
  /* printf(Envs [%s] [%s] [%s] [%s] \n", envp[0], envp[1], envp[2], envp[3]); */
  
  for (i=0;i<nsd;i++) { /* try each startup d until one says ok */
    /* we used last_nsd to help even the load */
    tnsd = (last_nsd+i) % nsd;
    rc = 0;
    rc = startup_spawn(tnsd, gid, runid, &tmpgid, myexename, ftmpi_cwdir, my_argc, my_argv, 13, envp, output_ip, output_port);
    if (rc>0) break;
  } /* for loop */
  
  if (rc<0) { /* couldn't start one up! ops. */
    fprintf(stderr,"ft_mpi_sys:ft_mpi_spawn: failed to start up exe?\n");
    return (-1);
  }
  
  /* started one up! */
  /* do some house keeping? maybe */
  
  /* move the last_nsd round to make round_robin work */
  last_nsd = (last_nsd+1)%nsd;
  
  /* if the notifier exits add this gid to its list to watch for me */
  if (notifier) notifier_addme (runid, tmpgid, 0, 0);
  
  
#ifdef REALVERBOSE
  printf("ft_mpi_sys:ft_mpi_spawn spawned new task with GID 0x%x\n", tmpgid);
#endif
  
  /* Oh and don't forget to free up used memory as well */
  for( i = 0; i < MAX_ARGS; i++ ) {
    _FREE( envp[i] );
  }
  
  _FREE(envp); 

  return (tmpgid);
}
/***************************************************************************/


/***************************************************************************/
int ftmpi_sys_post_state (int newstate, int *ids, int ext)
{
/* int t0, t1; */
int t2, t3;	/* NS op args */
int mb;
int rc;
char statename[256];


#ifdef REALVERBOSE9
printf("POST !!!!!!!! \n\n");
printf("NEWSTATE %d buf %x ext %d cinfo? 0x%x ep %d\n", 
		newstate, (char*)ids, ext, (char*) gcaddr, gepoch);
#endif

if ((!iamleader)&&(newstate!=FT_STATE_ELECT)) return (-1);	
	/* SHOULD NOT happen, but I check anyway */
	/* Only in a special case will a non leader force the FT_STATE_ELECT */
	/* usually a process will see the dead leader, take tmp control */
	/* long enough to post the state, and then do an election */

/* ok I am leader I post a record up of what I think the state is */
if (newstate!=FT_STATE_BUILD)
	mb = get_msg_buf_of_size (5*sizeof(int), 0, 0);   /* get a small buffer */
else 
	mb = get_msg_buf_of_size ((5+(ext*4))*sizeof(int), 0, 0); /* biggest */

        pk_int32(mb, &newstate, 1);
        pk_int32(mb, &leader_id, 1);	/* should be me */
        pk_int32(mb, &gepoch, 1);
        pk_int32(mb, &gnproc, 1);
        pk_int32(mb, &gextent, 1);

		if (newstate==FT_STATE_BUILD) { /* I pack IDs, exactly as needed */
			pk_int32(mb, ids, ext);
			/* if in BUILD I always send the g conn info */
			/* so make sure you have it! */
			/* if you don't I clear it to prevent a sig fault ! tut ! */
			if (!gcaddr) ftmpi_sys_clrtmp_cinfo (ext);
			/*			pk_int32(mb, gcaddr, ext); */
			pk_raw32(mb, gcaddr, ext);
			pk_int32(mb, gcport, ext);
			pk_int32(mb, gsport, ext);
			pk_int32(mb, gconvs, ext);
#ifdef REALVERBOSE9
			printf("DEBUG %x %x %x %x ****************\n",
			ids[0], ids[1], ids[2], ids[3]);
#endif

#ifdef DB_LEADER_SHOW_STATE_BUILD
	test_dump_all (ids);
#endif /* DB_LEADER_SHOW_STATE_BUILD */
			}

		mksname (statename, runid);
		ns_open ();
rc = ns_record_put (gid, statename, mb, RECORD_DB_INDEXED, 0, &t2, &t3, 1);
		ns_close ();
	/* put in first slot, del mb after */
	free_msg_buf (mb);

#ifdef REALVERBOSE9
printf("POST [ %d ] %d %d  \n\n", rc, t2, t3);
#endif 

if (rc<0) return (-1);
else
	return (0);
}
/***************************************************************************/

/***************************************************************************/
int ftmpi_sys_send_leader_ack (int epoch)
{
int s;
int ntag;
int tags[10];
int p;
int mb;

p = leader_port;  /* make tmp copy */
s = getconn_addr(leader_addr, &p, 0); /* note 0 */

if (s<0) return (-1);   /* cannot connect */
						/* if fact this is bad */

tags[0] = FTMPI_SYS_ACK;
tags[1] = runid;
tags[2] = gid;
tags[3] = epoch;
tags[4] = conn_p;
tags[5] = state_p;
tags[6] = 0;			/* extra value ignored for normal ack but not ring */

mb = get_msg_buf_of_size ( sizeof(int), 1, 0 ); /* resizable */
pk_raw32 ( mb, &conn_addr, 1 );			/* IPv4 addr */
pk_int32 ( mb, &ftmpi_my_convinfo, 1 ); /* conversion info value */

ntag = 7; /* proto_ack, runid, gid, epoch, ca, cp, sp */
send_pkmesg (s, gid, ntag, tags, mb, 0);  /* send req */
free_msg_buf ( mb );

#ifdef DB_PEON_ACK
/* debugging */
fflush(stdout);
printf("SENDING LEADER ACK me %d myaddr %x cp %d sp%d\n", gid, conn_addr,
conn_p, state_p );
fflush(stdout);
#endif /* DB_PEON_ACK */

closeconn (s);
return (0);
}
/***************************************************************************/

/***************************************************************************/
/*
 General send ack routine that uses the gcaddr etc list
 */
/***************************************************************************/
int ftmpi_sys_send_gid_ack (int tid, int epoch, int val)
{
int s;
int ntag;
int tags[10];
int p;
int r;
int mb;

r = ftmpi_array_rank (ggids, gextent, tid);
if (r<0) return (-1);	/* unknown gid in list */
if (!gcaddr) return (-2);	/* no cinfo list */

/* else we assume all is well */

p = gsport[r] ;  /* make tmp copy */
s = getconn_addr(gcaddr[r], &p, 0); /* note 0 */

if (s<0) return (-3);   /* cannot connect */
						/* if fact this is bad */

tags[0] = FTMPI_SYS_ACK;
tags[1] = runid;
tags[2] = gid;
tags[3] = epoch;
tags[4] = conn_p;
tags[5] = state_p;
tags[6] = val;


mb = get_msg_buf_of_size ( sizeof(int), 1, 0 ); /* resizable */
pk_raw32 ( mb, &conn_addr, 1 );			/* IPv4 addr */
pk_int32 ( mb, &ftmpi_my_convinfo, 1 ); /* conversion info value */

ntag = 7; /* proto_ack, runid, gid, epoch, ca, cp, sp */
send_pkmesg (s, gid, ntag, tags, mb, 0);  /* send req */
free_msg_buf ( mb );

closeconn (s);
return (0);
}
/***************************************************************************/

/***************************************************************************/
/*
 This routine clears the tmp caddr/cport/sport/convs lists 
*/
/***************************************************************************/

void ftmpi_sys_clrtmp_cinfo (int s) 
{
if (gcaddr) ftmpi_array_free (&gcaddr);
if (gcport) ftmpi_array_free (&gcport);
if (gsport) ftmpi_array_free (&gsport);
if (gconvs) ftmpi_array_free ((int**)&gconvs);
ftmpi_array_mkspace (&gcaddr, s);
ftmpi_array_mkspace (&gcport, s);
ftmpi_array_mkspace (&gsport, s);
ftmpi_array_mkspace ((int**)&gconvs, s);
}

/***************************************************************************/
/* This routine is a leader routine */
/* the leader waits for n events, either acks or exit events */
/* we also check to see if someone has already died in our event list */
/* This is used during FT_STATE_ACK */
/* we pass in the list we check from, the expected n of tasks */

/* glp is the expected gids */
/* i.e. we ignore exit events unless they are in this list */
/* rlp is the resulting gids, packed and in order */

/* not sure if we should count ourself in this list or not! */
/* but so far, we put ourselves in at the start if we are */
/* this allows us to use this routine to test subsets of tasks not */
/* just the who MPI_COMM_WORLD.		GEF */

/* Extra note updatecinfo is used so that we can choose whether we want to update */
/* the cinfo or not. i.e. during the ACK of a recovery its yes, during check_ack its NO */
/***************************************************************************/

int ftmpi_sys_leader_waitfor_acks (int *glp, int *rlp, int s, int n, int allowdead, int updatecinfo)
{
int i, j;
int sofar=0;
int c;
int e;	/* event y/n */
int alreadydead = 0;
int eid;	/* id of event/ack task */
int tad, tcp, tsp;	/* tmp addr, tmp conn port, tmp state port */ 
unsigned int tci;	/* tmp conv info */

if (updatecinfo) {
	/* as we also update the caddr/cport/sport entries we clear them */
	/* then realloc them to the right size */
	ftmpi_sys_clrtmp_cinfo (s);
}

/* very first put ourselves in the result list ! */
/* if and only if we are in the original gid watch list */

if (ftmpi_array_rank(glp, s, gid)>=0) {
		i = ftmpi_array_add (rlp, s, gid);	/* add to list, and at that index update cinfo */
		if (updatecinfo) {
			gcaddr[i] = conn_addr; 
			gcport[i] = conn_p; 
			gsport[i] = state_p;
			gconvs[i] = ftmpi_my_convinfo;
		}
		sofar = 1; /* us :) */
		}

/* increase 'sofar' by the number of processors which we allow to be dead for the 
   blank mode */
sofar += allowdead; 

/* then check off any that are in the event exit list so we don't wait for */
/* them by accident. note we should have cleared these before entering in */
/* but I am just being carefull about avoiding possible deadlocks    GEF */

c = ftmpi_array_extent (glp, s);	/* # of entries in list */
for(i=0;i<=c;i++) 
	if (glp[i]) /* valid entry in list */
		if (find_event_by_gid (glp[i])) { /* ? ek... */
			/* This used to be an error before the conn library started */
			/* checking not only if someone had died but also who.. */
			/* thus they have to push things into the event loop */
/* 			printf("waitfor_acks: GID %x was in event list. Should have been
cleared first!\n", glp[i]); */
/* 			printf("pass this message on to the developers.\n"); */

			/* Update the already dead count */
			alreadydead++;

			/* Also if we are monitoring we can now post this info to the monitoring tool... :) */
			if (displaycinfo&&displaycinfofound) {
				/* send message to monitor that this task is deaded :) */
				ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, i, 1);	/* 1= red is dead */
				printf("monitor proc state update for %d\n", i);
				}

			}

if ((alreadydead+sofar)==n) return (0);	
									/* we had no acks as they are all dead */

/* ok, that is done */
/* Now for the real testing for events/acks */

while ((alreadydead+sofar)<n) {
	/* check for receive acks */
	e = ftmpi_sys_check_for_ack (&eid, &tad, &tcp, &tsp, (int*)&tci, NULL, 
									0, gepoch);/* 0=probe, not wait */
	if (e) { /* an ack, make sure we are interested in it */
		if (ftmpi_array_rank(glp, s, eid)>=0) { /* in watch list */
			if (find_event_by_gid(eid)) { /* BUT they are already dead? */
#ifdef REALVERBOSE
				printf("waitfor_acks: 0x%x ACK'd but it was already marked as dead.\n", eid);
#endif
				/* we ignore them */
				}
			else { /* they are [still] alive! */
				/* make sure its not a duplicate ACK */
				if (ftmpi_array_rank(rlp, s, eid)>=0) { /* they had ack'd ! */
#ifdef REALVERBOSE
					printf("waitfor_acks: 0x%x Duplicate Ack\n", eid);
#endif
					/* ignore */
					}
				else { /* they are alive, on the list and its not a dup */
#ifdef REALVERBOSE
					printf("waitfor_acks: 0x%x Ackd\n", eid);
#endif
					i = ftmpi_array_add (rlp, s, eid);	/* add to result list */
												/* note we add cinfo at same index */
					if (updatecinfo) {
						gcaddr[i] = tad; 	/* their IPv4 Address */
						gcport[i] = tcp; 	/* their connection port */
						gsport[i] = tsp;	/* their update state port */
						gconvs[i] = tci;	/* their conversion info value */
					}
					sofar++; 
					} /* if not a dup */
				} /* alive */
			} /* if in watch list */
		} /* if we had an ack */

	/* check for exits */
	e = ftmpi_sys_check_for_events (&eid, 0);	/* 0 = probe but not wait */
	if (e) { /* make sure we are interested in it */
#ifdef REALVERBOSE9
	printf("event on %x", eid);
#endif
		if (ftmpi_array_rank(glp, s, eid)>=0) { /* in watch list */
#ifdef REALVERBOSE
			printf("waitfor_acks: 0x%x died when I was waiting for their ACK.\n", eid);
#endif
			alreadydead++;

			/* Also if we are monitoring we can now post this info to the monitoring tool... :) */
			/* note we have to calc there 'previous' rank as we are interested in their glp not rlp position */
			if (displaycinfo&&displaycinfofound) {
				/* send message to monitor that this task is deaded :) */
				j = ftmpi_array_rank(glp, s, eid);
				ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, j, 1);	/* 1= red is dead */
				printf("monitor proc state update for %d\n", j);
				}


			/* check for them in case they ack'd and then died !! */
			i = ftmpi_array_rank(rlp, s, eid);
			if (i>=0) { /* they had ack'd ! */
#ifdef REALVERBOSE
				printf("waitfor_acks: 0x%x they acked and then died?!\n", eid);
#endif
				/* update counters */
				sofar--;
				if (updatecinfo) {
				   	/* remove/reset cinfo data */
					gcaddr[i] = 0; 
					gcport[i] = 0; 
					gsport[i] = 0;	
					gconvs[i] = 0;
				}
				ftmpi_array_rm (rlp, s, eid);
				} /* if dead after acking */
			} /* if in watch list */
		} /* if we had an event */
#ifdef REALVERBOSE
		else						/* This else must be in the ifdef !! */
			printf("no event\n");
#endif
	} /* while awaiting 'n' events/deaths and ACKs */
return (0);
}
/***************************************************************************/



/***************************************************************************/
/*
 Ring codes
 */

/*
 This code uses State ACK message to circle a ring
 It assumes the list of ids is packed, i.e. location 0 is a valid ID 

 It beaks out if the LEADER has died!
 */
/***************************************************************************/

int	ftmpi_sys_ring_loop (int *lp, int s, int tepoch)
{
int sz;
int rc,  me, next, prev, c, sent, ackd;
int lcnt;	/* loop counter */
int first=-1;

if (!ftmpi_silent) ftmpi_array_dump (lp, s);
sz = ftmpi_array_extent (lp, s)+1; /* we want max size, not n-1 */
me = ftmpi_array_rank (lp, s, gid);	/* calc my rank */

next = (me+1)%sz;
prev = me-1; if (prev<0) prev = sz-1;

#ifdef REALVERBOSE9
printf("me %d p %d n %d gep %d ep %d\n", me, prev, next, gepoch, tepoch);
#endif

while (!lp[next]) {
	next = (next+1)%sz;
	}

while (!lp[prev]) {
	prev--;
	if (prev<0) prev = sz-1;
	}

if (sz==1) return (1); /* just me */

/* first = ftmpi_array_first (lp, s); */

/* TA:The first one should be leader */
if(iamleader) first=me;

lcnt = 0; /* default */
if (me==first) { /* I send first */
    sent = 0;
	lcnt = 1;	/* count at least me! */
    while (!sent) {
        rc = ftmpi_sys_send_gid_ack (lp[next], tepoch, lcnt);
#ifdef REALVERBOSE9
        printf("Send to %d rc %d ep %d\n", next, rc, tepoch);
#endif
        if (rc<0) { /* bad send, choose next */
            next = (next+1)%sz;
			while (!lp[next]) {
				next = (next+1)%sz;
				}
            if (next==me) {
#ifdef REALVERBOSE9
                printf("Only me %d... ek\n", me);
#endif
                break;
                }
            }
        else { /* good send */
         sent=1;
         break;
         }
    } /* while not sent */
if (sent) { /* I can expect an ack from previous? */
    ackd = 0;
    c = sz;
    while (!ackd) {
#ifdef REALVERBOSE9
        printf("%d awaiting an ack from anyone ep %d\n", me, tepoch);
#endif
        rc = ftmpi_sys_waitfor_ack (-1, -1, &lcnt);
        if (rc==0) ackd=1; /* we got an ack */
        if (rc<0) {
				c--;  /* we reduce our count on death */
				if (ftmpi_any_in_list_died (lp, s)) break;
				}
        if (c==1) break; /* we are the only ones left */
        }
    }
}

else { /* I am not the start, so I ack first and then send :) */
    ackd = 0;
    c = sz;
    while (!ackd) {
#ifdef REALVERBOSE9
        printf("%d awaiting an ack from anyone ep %d\n", me, tepoch);
#endif
        rc = ftmpi_sys_waitfor_ack (-1, -1, &lcnt);
        if (rc==0) ackd=1; /* we got an ack */
        if (rc<0) {
				c--;  /* we reduce our count on death */
/* 				if (ftmpi_any_in_list_died ()) break;   */
				if (rc) break;	/* ops, yes so no ring for me */
				}
        if (c==1) break; /* we are the only ones left */
        }
    sent = 0;
    while ((!sent)||(!ackd)) {
        rc = ftmpi_sys_send_gid_ack (lp[next], tepoch, lcnt+1);
#ifdef REALVERBOSE9
        printf("Send to %d rc %d ep %d\n", next, rc, tepoch);
#endif
        if (rc<0) { /* bad send, choose next */
            next = (next+1)%sz;
			while (!lp[next]) {
				next = (next+1)%sz;
				}
            if (next==me) {
#ifdef REALVERBOSE9
                printf("Only me %d... ek\n", me);
#endif
                break;
                }
            }
        else { /* good send */
         sent=1;
         break;
         }
    } /* while not sent */
} /* if I am not the start/rank 0 */

#ifdef REALVERBOSE9
printf("Sent %d Ackd %d lcnt %d\n", sent, ackd, lcnt);
#endif
if ((sent)&&(me!=first)) return (0);
if ((sent)&&(me==first)) return (lcnt);		/* so the head gets the proc count! */
return (-1);
}
/***************************************************************************/

/***************************************************************************/
/*
 This checks to see if the one you are looking for is deaded 
*/
/***************************************************************************/
int ftmpi_any_in_list_died (int *lp, int s)
{
int i;
int tmp;

ftmpi_sys_check_for_events (&tmp, -1); /* flush all events */

for (i=0;i<s;i++) 
	if (lp[i]) 
		if (find_event_by_gid (lp[i])) return (1);

return (0);
}


/* These routines are for collecting and distributing info */





/*
 This is where the new leader, updates the leader record 
 NOTE this should only be called once ever, as elect usually does this
 except in the case where you have been elected by FTMPIRUN.
 */

/* The new leader also checks for any monitoring GUI's etc etc here */

int		ftmpi_sys_new_leader_first_time_only()
{
int mb;
char rn[256];
int i1, i2;

mb = get_msg_buf_of_size (128, 0, 0);	/* get a small buffer */
if (mb<0) return (-1);	/* shouldn't happen, ever */

/* The leader record is just the LEADERs GID, ADDR and PORT values */
pk_int32(mb, &gid, 1);
pk_raw32(mb, &conn_addr, 1);
pk_int32(mb, &state_p, 1);

mklname(rn, runid);
ns_open ();
												/* 1 at end means we delete mb after request */
(void)ns_record_swap (gid, rn, mb, RECORD_DB_INDEXED, 0, leader_id, &i1, &i2, 1);
/* SHOULD check this return as if it fails this is a truly SERIOUS PATHALOGICAL failure. GEF */

ns_close ();
free_msg_buf (mb);	/* paranoid free */

(void)ftmpi_sys_find_monitor ();	/* find a monitor if we have been told to */
	
return (0);
}
/***************************************************************************/

/***************************************************************************/
/* this routine we just loop around until we get the details out of the */
/* leader record. */
/* IF the leader dies, we exit with a return code. */
/* if we time out, we exit with a return code */
/* THIS routine is only called after ftmpi_sys_init_startup ONCE */
/* After that, an election is used only */
/***************************************************************************/
int	ftmpi_sys_get_new_leader_first_time_only (int loops, int offset) {
int tags[3];
int egid;
int rd;
int t=0;

long delay=0;

/* we do an additional delay so that we know the leader record is there before we poll harder */
/* this allows the NS to allow the other tasks to register before we pound it into the ground */
/* this goes away when we move to a PUSH-PULL model rather than the PULL/POLL model currently */

delay=(long)gextent+(long)offset;
delay*=FTMPI_WAITFOR_LEADER_STARTUP_DELAY;		/* default was 150ms per node */
						/* 4 nodes delay (rank1, rank3) = 0.75 - 1.05 Sec */
						/* 32 nodes = 4.95 - 9.45 */
						/* 64 nodes = 9.75 - 19.05 */
						/* 128 nodes = 19.35 - 38.25 */

if (delay>0) usleep (delay);

/* here we use the call in ft-mpi-common */
while (1) {

#ifdef FTMPI_SYS_LEADER_POLL
	r = ftmpi_poll_for_db_record (&rd, gid, 0, 0, DB_FT_MPI_LEADER, runid, 0,
	                            loops, event_as);
#else
	/* ok wait for the 2nd generation count of this record */
	/* 1st is plublished by libstartup/ftmpirun */
	/* 2nd (the one we want) is published by the new leader! */
	rd = ftmpi_wait_for_db_record(DB_FT_MPI_LEADER,runid,0,2,event_as,1); 
	/*
	printf("We got rd is %d\n",rd);
	fflush(stdout);
	*/
#endif

	/* ok we are here because we found it, someone died, or no reader info? */
#ifdef FTMPI_SYS_LEADER_POLL
	if (r==0) {	/* we have the record */
#else
	if (rd>=0) {	/* we have the record */
#endif
				/* so we read it and if good, break, if not, sleep loop agin */
				/* we do this loop times */
				/* after which we give up do an election */
				tags[0] = 0; tags[1] = 0; tags[2] = 0;
				/*	upk_int32 (rd, tags, 3); */	/* get all the data */
				upk_int32 (rd, &tags[0], 1);
				upk_raw32 (rd, &tags[1], 1);
				upk_int32 (rd, &tags[2], 1);
				free_msg_buf (rd);

				if ((tags[1])||(tags[2])) { /* good data, copy it and run */
					leader_id = tags[0];
					leader_addr = tags[1];
					leader_port = tags[2];

#ifndef FTMPI_SYS_LEADER_POLL
					ftmpi_unreg_wait_for_db_record(DB_FT_MPI_LEADER, runid, 0);
#endif

					return (0);
					}
				} /* if we got a record */
					
#ifdef FTMPI_SYS_LEADER_POLL
	if (r==DB_FAILURE_DETECT) { /* some one died */
#else
	if (rd==DB_FAILURE_DETECT) { /* some one died */
#endif

		/* if it was the leader, we fix it */
		ftmpi_sys_check_for_events	(&egid, 1);

		if (egid==leader_id) { /* pop goes the leader */
			fprintf(stderr,"FTMPIRUN choosen leader died before making it to the office. Early elections.\n");
			return (-1);	/* bad */
			}
		} /* some one died */

	t++;
	if (t==loops) break;	/* we tried */

	} /* while loop */

return (-1);	/* no *valid* record ops */
}
/***************************************************************************/

/***************************************************************************/
/* debug routines */
/* these are used during developement only */
/* GEF */
/***************************************************************************/
int	test_lists ()
{
int *p=NULL;
int s=10;
int i;

ftmpi_array_mkspace (&p, s);
for(i=0;i<5;i++) ftmpi_array_add (p, s, i);
ftmpi_array_add (p, s, 7);
ftmpi_array_add (p, s, 2);
ftmpi_array_rm (p, s, 1);
ftmpi_array_rm (p, s, 3);
ftmpi_array_rm (p, s, 5);
ftmpi_array_rm (p, s, 5);

ftmpi_array_dump (p, s);
ftmpi_array_pack (p, s);
ftmpi_array_dump (p, s);
 return 0; 
}

/* this copies out the internal ggids for debugging */
int test_copy_gids (int *newlp, int s)
{
int i;
int c;
c = ftmpi_array_count (ggids, gextent);
if (c>s) c = s;
for (i=0;i<c;i++) newlp[i] = ggids[i];
return (c);
}

int test_dump_cinfo ()
{
printf("Dump of caddr, cport, gport and conv info\n");
ftmpi_array_dump (gcaddr, gextent);
ftmpi_array_dump (gcport, gextent);
ftmpi_array_dump (gsport, gextent);
ftmpi_array_dump ((int*)gconvs, gextent);
 return 0; 
}

int test_dump_all (int *ids)
/* note ids not ggids.. */
{
printf("Dump of all info by %d\n", gid);
ftmpi_array_dump (ids, gextent);
ftmpi_array_dump (gcaddr, gextent);
ftmpi_array_dump (gcport, gextent);
ftmpi_array_dump (gsport, gextent);
ftmpi_array_dump ((int*)gconvs, gextent);
 return 0;
}


/* ***************************************************************** */

/*
 ftmpi_sys_reorder_ids (newgids, originalgids, ext)

 This routine takes the newgids list and reorders it so that it is as
 close to the originalgids list as possible. THIS includes the cinfo DS
 as well. This is used to make process keep their original ranks if possible 

 GEF STR-HLRS 2002
*/
/***************************************************************************/


int	ftmpi_sys_reorder_ids (int *new, int *old, int extent)
{
int *cpy_caddr=NULL;
int *cpy_cport=NULL;
int *cpy_sport=NULL;
int *cpy_convs=NULL;
int *cpy_new=NULL;
int *order=NULL;
int i, r;

/* 
printf("ftmpi_sys_reorder_ids: extent %d\n", extent);
fflush(stdout);
*/

ftmpi_array_mkspace (&cpy_caddr, extent);
ftmpi_array_mkspace (&cpy_cport, extent);
ftmpi_array_mkspace (&cpy_sport, extent);
ftmpi_array_mkspace (&cpy_convs, extent);
ftmpi_array_mkspace (&cpy_new, extent);
ftmpi_array_mkspace (&order, extent);

/* first we build an order up */

for (i=0;i<extent;i++) {
	order[i] = ftmpi_array_rank (old, extent, new[i]);
	}

/* ** debug do not uncomment ** 
printf("ftmpi_sys_reorder_ids: lists before reordering (new, old, order)\n");
fflush(stdout);
ftmpi_array_dump (new, extent);
ftmpi_array_dump (old, extent);
ftmpi_array_dump (order, extent);
fflush(stdout);
*/

/* ok, we copy new list and the cinfo into a safe place */

ftmpi_array_copy (cpy_caddr, gcaddr, extent);
ftmpi_array_copy (cpy_cport, gcport, extent);
ftmpi_array_copy (cpy_sport, gsport, extent);
ftmpi_array_copy (cpy_convs, (int*)gconvs, extent);
ftmpi_array_copy (cpy_new, new, extent);

/* empty the old new list now... */
for(i=0;i<extent;i++) { 
   new[i]=0; gcaddr[i]=0; gcport[i]=0; gsport[i]=0; gconvs[i]=0;
}

/* now we re-order the result lists */
/* first we move the ones in the original set to their correct location */
/* Then we add the new ones to fill in gaps */

for (i=0;i<extent;i++) {
	if (order[i]!=-1) { /* in original list so put them in same place */

/* 		new[i] = cpy_new[i]; */

/* fudge this just copies them into the same place as the original new list */
/* i.e. if reordered them its wrong */
/* we need to put them in the 'order' location instead */

		new[order[i]] = cpy_new[i];

		gcaddr[order[i]] = cpy_caddr[i];
		gcport[order[i]] = cpy_cport[i];
		gsport[order[i]] = cpy_sport[i];
		gconvs[order[i]] = cpy_convs[i];
		}
	}

/* Ok the old ones are back in their original place, so add the unknowns now */
for (i=0;i<extent;i++) {
	if (order[i]==-1) { /* not in original list so put them anywhere */
		r = ftmpi_array_add (new, extent, cpy_new[i]);
		/* r = new location that we copy the cinfo into as well */
		gcaddr[r] = cpy_caddr[i];
		gcport[r] = cpy_cport[i];
		gsport[r] = cpy_sport[i];
		gconvs[r] = cpy_convs[i];
		}
	}

/* ** debugging do not uncomment **
printf("ftmpi_sys_reorder_ids: lists after reordering (new, old)\n");
fflush(stdout);
ftmpi_array_dump (new, extent);
ftmpi_array_dump (old, extent);
fflush(stdout);
*/

  /* free temp arrays */
  ftmpi_array_free (&cpy_caddr);
  ftmpi_array_free (&cpy_cport);
  ftmpi_array_free (&cpy_sport);
  ftmpi_array_free (&cpy_convs);

return (0);

}
/***************************************************************************/
/* This routine is compacting the arrays for the shrink mode and
** allocates then new arrays of the correct size */
int ftmpi_sys_pack_ids ( int extent )
{
  int *cpy_caddr=NULL;
  int *cpy_cport=NULL;
  int *cpy_sport=NULL;
  unsigned int *cpy_convs=NULL;
  int *cpy_new=NULL;
  int i;
  int size;

  /* Compact the arrays for the shrink mode*/
  ftmpi_array_pack ( newgids, extent );
  ftmpi_array_pack ( gcaddr, extent );
  ftmpi_array_pack ( gcport, extent );
  ftmpi_array_pack ( gsport, extent );
  ftmpi_array_pack ( (int*) gconvs, extent );
  
  /* determine number of really used elements */
  size = ftmpi_array_count ( newgids, extent);

  /* just for debugging, remove later on ! */
  i = ftmpi_array_count ( gcaddr, extent );
  if ( i != size ) 
    printf("Number of valid entries in gcaddr != entries in newgids (%d != %d )\n", i, size);

  i = ftmpi_array_count ( gcport, extent );
  if ( i != size ) 
    printf("Number of valid entries in gcport != entries in newgids (%d != %d )\n", i, size);

  i = ftmpi_array_count ( gsport, extent );
  if ( i != size ) 
    printf("Number of valid entries in gsport != entries in newgids (%d != %d )\n", i, size);

  i = ftmpi_array_count ( (int *)gconvs, extent );
  if ( i != size ) 
    printf("Number of valid entries in gconvs != entries in newgids (%d != %d )\n", i, size);

  /* allocate temp arrays of the correct size */
  ftmpi_array_mkspace (&cpy_caddr, size);
  ftmpi_array_mkspace (&cpy_cport, size);
  ftmpi_array_mkspace (&cpy_sport, size);
  ftmpi_array_mkspace ((int **) &cpy_convs, size);
  ftmpi_array_mkspace (&cpy_new, size);

  /* copy data into temp arrays */
  ftmpi_array_copy (cpy_caddr, gcaddr, size);
  ftmpi_array_copy (cpy_cport, gcport, size);
  ftmpi_array_copy (cpy_sport, gsport, size);
  ftmpi_array_copy ((int*) cpy_convs, (int *) gconvs, size);
  ftmpi_array_copy (cpy_new, newgids, size);

  /* free original arrays */
  ftmpi_array_free (&gcaddr);
  ftmpi_array_free (&gcport);
  ftmpi_array_free (&gsport);
  ftmpi_array_free ((int**) &gconvs);
  ftmpi_array_free (&newgids);

  /* re-allocate original with the right size */
  ftmpi_array_mkspace (&gcaddr, size);
  ftmpi_array_mkspace (&gcport,  size);
  ftmpi_array_mkspace (&gsport,  size);
  ftmpi_array_mkspace ((int **)&gconvs,  size);
  ftmpi_array_mkspace (&newgids, size);

  /* copy data from temp arrays back into original arrays */
  ftmpi_array_copy ( gcaddr, cpy_caddr,size);
  ftmpi_array_copy ( gcport, cpy_cport,size);
  ftmpi_array_copy ( gsport, cpy_sport,size);
  ftmpi_array_copy ( (int*) gconvs, (int*) cpy_convs,size);
  ftmpi_array_copy ( newgids,cpy_new, size);

  /* free temp arrays */
  ftmpi_array_free (&cpy_caddr);
  ftmpi_array_free (&cpy_cport);
  ftmpi_array_free (&cpy_sport);
  ftmpi_array_free ((int**) &cpy_convs);
  ftmpi_array_free (&cpy_new);

  /* test_dump_cinfo (); */

  return ( 0 );
}
/* ***************************************************************** */

/* Ok, here is the mother of all recovery loops */

/* Process check in here and only leave if its possible to recover */
/* OR a pathalogical error has occured */

/* we return 1 when we have recovered in this loop */
/* we return -1 if we failed */
/* if we discover abort we abort and do not return! */

int	ftmpi_sys_recovery_loop ( )
{
int rc;
int recovered=0;
int oldsize;
int *oldgids; 
int numchanged;


 
 ftmpi_recovery_count++;

#ifdef VERBOSERECOVERY
fprintf(stderr,"%d: Recovery loop %d\n", gid, ftmpi_recovery_count); 
if (ftmpi_recovery_count>1)
	ftmpi_sys_show_all ("start of sys_recovery_loop\0");
#endif /* VERBOSERECOVERY */

/* OK, first we stop and flush all communications (i.e. manage the connections)
 This hopefully does not block...  GEF */

conn_stop_and_flush ( );


while (!recovered) {

	/* if the commicator is FT_MODE_ABORT we abort on error detect! */
	/* this is the communicator mode not a check for a MPI_Abort! */
	if ( (gcommode==FT_MODE_ABORT) && (ftmpi_recovery_count>1)){
		recovered = -1;
		break;
	}

	/* we must check for a rouge MPI_ABORT state every loop */
/* 	ftmpi_sys_check_for_mpi_abort ();  */

	/* First find the leader */
	/* He will lead us from the darkness, or be killed */

	if (iamleader) {
		rc = ftmpi_sys_recovery_leader_loop (0); /* 0 = old leader */
		if (rc<0) { /* leader could not save us? or spawn new tasks etc */
			recovered = -1;
			break;
			}
		if (rc==0) {
			recovered = 1;
			break;
			}
		} /* if the leader */

	else { /* I am not a leader.. yet */ 
		rc = ftmpi_sys_recovery_peon_loop ();
		if (rc==0) {
			recovered = 1;
			break;
			}
		if (rc<0) { /* pathalogical failure OR halt issued etc */
			recovered = -1;
			break;
			}
		/* I have been made the leader! */
		} /* I am a peon or was a peon */
		/* we only get here if the leader loops or a new leader appears */
	} /* while NOT recovered */

if (recovered>0) { /* if we recovered ok */ 
	/* clear our event loop */
	rc = notifier_clear_all_events ();	
				/* so we have space for future ones */
	error_d = 0;		/* no errors detected (yet) */




	oldsize = ftmpi_group_size ( MPI_GROUP_WORLD );
	oldgids = ftmpi_group_get_gids_ptr (MPI_GROUP_WORLD);
	
	numchanged = ftmpi_sys_create_failedlist ( oldsize, oldgids, 
						   gextent, ggids);

	/* remapping of data on the different queues must be done here */
	/* reason (1) old gid for remapping on shrink is cleared on */
	/*            ftmpi_group_clr_all_but_world....  */
	/* reason (2) com_clr_all_but_world.... */
	/* reason (3) the above clr destroys info on shadow coms etc */
	/* reason (4) we can only do this after knowing which are now dead */
	/*            after the recovery succeeded i.e. failedlist */
	
	/* but only call this function if this is an actual recovery */
	/* not the first loop through */
	if (ftmpi_recovery_count>1)
		ftmpi_sys_recovery_handle_queues ( oldsize, oldgids, gextent, ggids, 
			  					ftmpi_failed_list_count, ftmpi_failed_list);




	
	  /* Create error code and error string for this list of 
	     failed procs */
	ftmpi_err_set_ft_attributes (numchanged, ftmpi_failed_list);
	  
	/* Now for some strange MPI stuff */
	/* First we nuke all but MPI_COMM_WORLD */
	ftmpi_com_clr_all_but_world ();
	
	/* same for Groups */
	ftmpi_group_clr_all_but_world ();
	

	/* Now rebuild MPI_COMM_WORLD */
	ftmpi_com_build (MPI_COMM_WORLD, gid, gcommode, gmsgmode, gepoch, 
			 ggids, gextent, gnproc, 1);
	/* 1= reset list which is now needed as ftmpi_sys_recovery_handle_queues */
	/* handles the moving of mcw unexp q to sys and back again */
	
	/* we do need to build the WORLD group here */
	ftmpi_group_build (MPI_GROUP_WORLD, gid, ggids, gextent, gnproc);

	/* now to set MPI_COMM_WORLDs group */
	/* we need to do this as build group does not take an 'owned by' arg */

	ftmpi_com_set_group (MPI_COMM_WORLD, MPI_GROUP_WORLD);

	/* now for comm and group self */

	ftmpi_com_build (MPI_COMM_SELF, gid, gcommode, gmsgmode, gepoch,
						&gid, 1, 1, 1);

	ftmpi_group_build (MPI_GROUP_SELF, gid, &gid, 1, 1);

	ftmpi_com_set_group (MPI_COMM_SELF, MPI_GROUP_SELF);


	/* Initiate the error-code and error-class lists */
	/* ftmpi_errcode_and_errclass_init (); */

	/* create shadow of MPI_COMM_WORLD and MPI_COMM_SELF */
	ftmpi_com_create_shadow ( MPI_COMM_WORLD, FTMPI_COMM_WORLD_SHADOW);
	ftmpi_com_create_shadow ( MPI_COMM_SELF, FTMPI_COMM_SELF_SHADOW);

	/* handles for NB requests? */
	/* hope someone free's any handles already used! */

	/*	ftmpi_nb_clearall_reqhandles (); */


	/* here we should check for REBUILD_ALL */
	/* and then rebuild all the other groups, comms and keys */
	/* ToDo */

	/* now to update connection contact info */
	ftmpi_conn_fillin_conn_info (ggids, gcaddr, gcport, gsport, gextent);

	/* creates new procinfo records and sets the ddt decode infos */
	/* if the record already exists this routine skips update */
	ftmpi_procinfo_add_and_set_all_info (ggids, gcaddr, gcport, gsport, 
					     (int*)gconvs, gextent, ftmpi_my_convinfo);

	/* now we can restart all user level communications */
	/* This send XONs to all alive previously opened connections we had */
	conn_restart_all_coms ( );

	/* Now you are free to go :) */

	if ((leader_id==gid)&&(displaycinfo&&displaycinfofound)) {
		ftmpi_sys_monitor_post_mcw (0);
		puts("Posted recovered MPI_COMM_WORLD to the monitoring GUI");
		}

	}

#ifdef VERBOSERECOVERY
if (ftmpi_recovery_count>1)
	ftmpi_sys_show_all ("end of sys_recovery_loop\0");
#endif /* VERBOSERECOVERY */

 if ( ( ftmpi_recovery_count == 1 ) && ( gcommode == FT_MODE_ABORT ) ) 
   ftmpi_com_init_errh_toabort ();


return (recovered);
}
/***************************************************************************/






/***************************************************************************/
/* 
	ftmpi_sys_recovery_leader_loop

*/

/*
 0 return means we made it out ok (FT_STATE_OK) 
 -1 means something horrendeous happened, like we could not spawn anyone etc 
  if we discover abort we abort and do not return!
*/

/*
 To handle the GID lists correctly we now have multiple lists:

 (A) official : This is the current correct list including the cinfo.
 		This list is the one that is published and passed the ACK/RING 
		verification as was the STATE_BUILD record.
				
 (B) working :	This is the working list we use when checking who exists (discovery)
 		as well as for verification.
		This include both GIDs and cinfo.

 (C) ordering :	This list is produced from the previous official list and is used
 		together with the comminicator mode (gcommode) such as rebuild/shrink/blank 
		to produce the rankings for MPI_COMM_WORLD and the internal MPI_GROUP_WORLD.
		(ie to reduce the number of rank changes in rebuild/blank or keep the same
		ordering but compressed ranks in the shrink mode).
	        This is just a GIDs list.

 (D) previous : This is the last accepted official list.
 		This is both GIDs and cinfo.

 GEF STR Oct2002
*/
/***************************************************************************/
				

int	ftmpi_sys_recovery_leader_loop (int new)
{
int recovered=0;
int rc;
int i, j;
int tgid, tnproc, tc = 0, tdiff = 0;
int astate; /* attempted state */
int most;	/* number of procs we might expect results from */
int toget = 0;	/* additional # we are spawning */
int got;	/* how many did we get right out of the additional? */

int *order_ggids=NULL;	/* ordering list based on last offcial list (ggids at entry point) */
static int allowdead=0;
int tmpallowdead=0;

if (gcommode==FT_MODE_REBUILD) most=required_procs;
else
	most=gextent;	/* else use what we had before */


astate = FT_STATE_ACK; /* first we do discover */

 if ( gcommode == FT_MODE_BLANK ) {
   tnproc = most - allowdead;
   tmpallowdead = allowdead;
   allowdead = 0;
 }
else
  tnproc = most;	/* this is how many we expect, including us */


/* make a copy of entry point GID list to use for ordering */
ftmpi_array_mkspace (&order_ggids, most);
ftmpi_array_copy (order_ggids, ggids, most);

/* if we are recovering cinfo MAY or MAY NOT already exist! */
/* if we are entering from an _init_ state, then cinfo doesn't exist yet! */

if (displaycinfo&&displaycinfofound) {
	/* send message to monitor that this task is deaded :) */
	ft_mpi_cinfo_req_com_state (MPI_COMM_WORLD, gextent, 1, 1);	/* 2= white is right (for recovery) */
	printf("monitor com state update to recovery mode\n");
}

while (!recovered) {

	/* check for a rouge abort */
	ftmpi_sys_check_for_mpi_abort ();

	switch (astate) {

		case FT_STATE_ACK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x leader in FT_STATE_ACK\n", gid); fflush(stdout);
#endif
			/* we do this so tasks can distinguish between ACK reqs */


			/* first we post the ACK state */
			ftmpi_sys_post_state (FT_STATE_ACK, NULL, 0); 

#ifdef REALVERBOSE9
printf("ACK most %d tnproc %d\n", most, tnproc); fflush(stdout);
#endif


			/* now we make a clean result list */
			if (newgids) ftmpi_array_free (&newgids);
			ftmpi_array_mkspace (&newgids, most);

			/* wait for ACKs or deaths etc */
			/* note, here we build a new cinfo and newgids list */
			/* BUT we only accept gids from the list 'gids' */
			rc = ftmpi_sys_leader_waitfor_acks (ggids, newgids, most, tnproc, allowdead, 1);

#ifdef REALVERBOSE9
printf("PACK rc %d\n", rc); fflush(stdout);
#endif
			/* got acks or death, this is the set we work with now */

			/* count how many we really got back */
			tc = ftmpi_array_count (newgids, most);

			tdiff = tnproc - tc;	/* any missing */

			/* before posting !again! we update the epoch count */
			gepoch++;
			gepoch = (gepoch % 10000); /* prevent roll over */

			/* decision time */
			/* if newgids is missing some and we are into rebuilding */
			/* we SPAWN. Else we handle the IDs as needed and goto BUILD */


			/* if non missing or some missing and we do not rebuild.. ie shrink/blank etc */
			if ((gcommode!=FT_MODE_REBUILD)||(!tdiff)) { 
				astate = FT_STATE_BUILD;

				/* before entering the posting build state we always make sure */
				/* the posted GID and cinfo lists are in the right order */

				ftmpi_sys_reorder_ids (newgids, order_ggids, most);


				if ( gcommode == FT_MODE_SHRINK ) {
				  ftmpi_sys_pack_ids ( most );
				  tnproc = tc;
				  most   = tc;
				  gextent = tc;
				  gnproc = tc;
				}
				
				if ( gcommode == FT_MODE_BLANK ) {
				  allowdead += tdiff;
				  tnproc = tc;
				  gnproc = tc;
				  /* test_dump_cinfo ();
				     ftmpi_array_dump (newgids, most ); */
				}

				break;
				}

			if (gcommode==FT_MODE_REBUILD) {	/* we have MIA and rebuild */
				astate = FT_STATE_SPAWN;
				toget = tdiff;
				break;
				}

			break;

		case FT_STATE_SPAWN:
#ifdef VERBOSE
			fprintf(stdout,"0x%x leader in FT_STATE_SPAWN\n", gid); fflush(stdout);
#endif

#ifdef VERBOSE
			ftmpi_array_dump (newgids, most);
			printf("Spawn: most %d tc %d toget %d tdiff %d\n",
					most, tc, toget, tdiff);
#endif

			/* Ok we are to spawn some missing tasks */
			/* We make sure the state is WAIT so all tasks will wait */
			ftmpi_sys_post_state (FT_STATE_WAIT, NULL, 0);

			/* before we pack newgids, we update the monitor if needed that the new tasks are being respawned */
			if (displaycinfo&&displaycinfofound) {
			  int *display_ggids=NULL;	

			  /* make a copy of entry point GID list to use for ordering & displaying  */
			  ftmpi_array_mkspace (&display_ggids, most);
			  ftmpi_array_copy (display_ggids, newgids, most);
			  ftmpi_sys_reorder_ids (display_ggids, order_ggids, most);

				for (j=0;j<most;j++) {
					if (display_ggids[j]<=0) {
						ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, j, 2);	
							/* 2 = white for a ghost to return */
					}
					if (display_ggids[j]>0) {
						ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, j, 0);	
							/* 0 = ok as in acked */
					}

				}
				for(i=j;i<most;i++)
					ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, i, 3); 
							/* 3 = blanked out! */

			printf("monitor proc state updated\n");
			
			ftmpi_array_free(&display_ggids);
			}

			/* we pack the newgids as the spawned are added to the end */
			ftmpi_array_pack (newgids, most);

			/* we must also pack the contact info cinfo stuff as well !!! */
			/* if we don't then we will get this info in the wrong order? */
			/*
			ftmpi_array_pack (gcaddr, most);
			ftmpi_array_pack (gcport, most);
			ftmpi_array_pack (gsport, most);
			ftmpi_array_pack (gconvs, most);
			*/

			/* nope, cinfo is thown away by us entering ACK again */
			/* ok, it would be nice to keep, and only get acks */
			/* from the new ones... */
			/* big TODO for GEF in the future */

			/* We simply loop and spawn */
			got=0;
			for(i=0;i<toget;i++) {
				tgid = ftmpi_sys_spawn ();
				if (tgid>0) { /* spawned ok, I should wait for its ACK */
					/* But instead will force all to re-ack */
					/* Now we add to known/watch list */
					ftmpi_array_add (newgids, most, tgid); 
					got++;
					tc++;
					}
				} /* loop spawning */

			/* decision time again */
			/* if we spawned ok, we go back to ACKing */
			/* if we couldnot spawn we go to HALTING */

			if (got==toget) { /* got all we needed */

				/* previously we reordered here */
				/* we don't do this anymore as we now do this before posting BUILD state */
				/*
				ftmpi_sys_reorder_ids (newgids, ggids, most);
				*/

				/* Now clear up the list of GIDs that we accept as a workng set */
				ftmpi_array_free (&ggids);
				ftmpi_array_mkspace (&ggids, most);

				ftmpi_array_copy (ggids, newgids, most); 
					/* we make them semi-offical */
					/* note this list just controls who is */
					/* noticed when we get an event or an ACK */
					/* until after check/ok */
				gextent = tc;
				gnproc  = tc;
				tnproc  = tc;
				astate = FT_STATE_ACK;
				}

			else { /* we could not spawn all we needed */
				fprintf(stderr,"0x%x couldnot spawn %d [%s] tasks needed.\n",
						gid, toget, myexename);
				astate = FT_STATE_HALT;
				}
			break;

		case FT_STATE_BUILD:
#ifdef VERBOSE
			fprintf(stdout,"0x%x leader in FT_STATE_BUILD\n", gid); fflush(stdout);
#endif
			/* before posting we update the epoch count */
			gepoch++;
			gepoch = (gepoch % 10000); /* prevent roll over */


			/* Here we post the new communicator members into the NS */
			/* as we enter this only from the ACK state then only newgids has the correct list */
			ftmpi_sys_post_state (FT_STATE_BUILD, newgids, most);


			/* I now do a blind ring test (FT_STATE_CHECK) */
			/* if everybody passes, I goto OK */

			astate = FT_STATE_CHECK;

		/* check will use the ggids list to tick off all the processes.. */
	/* so we copy update it. Note the newgids will get over written by ACKs */
		/* if we use the check_ack mode rather than the check_ring */

			ftmpi_array_free (&ggids);
			ftmpi_array_mkspace (&ggids, most);

			ftmpi_array_copy (ggids, newgids, most); 
			/* we make them semi-offical */
			/* note this list just controls who is */
			/* noticed when we get an event or an ACK */
			break;

		case FT_STATE_CHECK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x leader in FT_STATE_CHECK\n", gid); 
			fflush(stdout);
#endif
			/* we don't post this. It is implicit of a BUILD */
#ifdef CHECK_RING
			rc = ftmpi_sys_ring_loop (ggids, tnproc, gepoch);
			
			if (rc==tnproc) { /* all passed */
				astate = FT_STATE_OK;
				}
			else {
				/* someone bombed ? startagain */
				tnproc = most;	/* reset counters */
				astate = FT_STATE_ACK;
				}
#endif /* CHECK_RING */
#ifdef CHECK_ACK
			/* The Build state tell them to build and then ACK */

			/* now we make a clean result list */
			if (newgids) ftmpi_array_free (&newgids);
			ftmpi_array_mkspace (&newgids, most);

			/* wait for the ACKs or deaths etc */
			rc = ftmpi_sys_leader_waitfor_acks (ggids, newgids, most, tnproc, allowdead, 0);
#ifdef REALVERBOSE9
			printf("PACK rc %d\n", rc); fflush(stdout);
#endif
			/* got acks or death, this is the set we work with now */

			/* count how many we really got back */
			tc = ftmpi_array_count (newgids, most);
			tdiff = tnproc - tc;	/* any missing */

			/* before posting !again! we update the epoch count */
			gepoch++;
			gepoch = (gepoch % 10000); /* prevent roll over */

			/* the value received here is now depending on the communicator
			   mode for. Set also the values used to build MPI_COMM_WORLD
			   for the SHRINK and the BLANK mode here */
			if ( gcommode == FT_MODE_BLANK ) {
			  if ( tdiff == allowdead ) {
			    astate = FT_STATE_OK;
			    /* This line is just to store the value for the next error */
			    allowdead = tmpallowdead + tdiff;
			    printf("allowdead = %d\n ",  allowdead);
			  }
			  else {
			    tnproc = most;
			    astate = FT_STATE_ACK;
			  }
			}
			else if ( gcommode == FT_MODE_SHRINK ) {
			  if ( !tdiff ) 
			    astate = FT_STATE_OK;
			  else
			    astate = FT_STATE_ACK;
			}
			else {			  
			  if ( !tdiff )  { /* all passed */
			    astate = FT_STATE_OK;
			  }
			  else {
			    /* someone bombed ? startagain */
			    tnproc = most;	/* reset counters */
			    astate = FT_STATE_ACK;
			  }
			}
#endif /* CHECK_ACK */
			break;

		case FT_STATE_ABORT:
#ifdef VERBOSE
			fprintf(stderr,"0x%x Aborting FTMPI application.\n", gid);
#endif
			/* clean up NS but don't send death event */
			ftmpi_sys_leave (0, 0);	/* second 0 = don't remove runstate rec */

			exit (MPI_ERR_ABORT_CALLED);	/* yep die */
			break;

		case FT_STATE_HALT:
#ifdef VERBOSE
			fprintf(stderr,"0x%x halting FTMPI application.\n", gid);
#endif
			ftmpi_sys_post_state (FT_STATE_HALT, NULL, 0);
			recovered = -1;
			break;

		case FT_STATE_OK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x leader in FT_STATE_OK\n", gid); fflush(stdout);
#endif
			ftmpi_sys_post_state (FT_STATE_OK, ggids, gextent);
			recovered = 1;
			break;

		default:
			/* never happens */
			fprintf(stderr,"GEF forgot something!\nEmail him fagg@hlrs.de\n");
			ftmpi_sys_leave (1,0);
			break;

		} /* switch on state */

	} /* not recovered */


ftmpi_array_free (&order_ggids);	/* remove the ordering list of GIDs */

if (recovered>0) {
	/* if recovered update monitor if available */
	if (displaycinfo&&displaycinfofound) {
		/* send message to monitor that this comm is fixed :) */
		ft_mpi_cinfo_req_com_state (MPI_COMM_WORLD, gextent, 1, 0);	
										/* 0= ok (recovered) */
		printf("monitor com state updated to recovered\n");
		}
	return (0);
	}
else return (-1);
}
/***************************************************************************/







/***************************************************************************/
/*
	ftmpi_sys_recovery_peon_loop
*/

/*
	we loop doing what the leader wants
	if we reach FT_STATE_OK return 0 and are happy
	if we discover a pathalogical failure (like HALT) we return (-1)
	if we discover abort we abort and do not return!
	if we become the new leader, we return (1) and do as a leader
*/
/***************************************************************************/

int	ftmpi_sys_recovery_peon_loop ()
{
int rc;
int tgid, tnproc, textent, tc, tepoch;
int recovered=0;
int tleader;
int astate; /* attempted state */
int newstate;
int lastack_leader=0;	/* so we don't flood a slow leader with ACKs */
int lastack_epoch=0;
int lastbuild_leader=0;	/* so we don't flood a slow leader with ACKs */
int lastbuild_epoch=0;
int acked_yet=0;		/* We must ACK at least once during a recovery */

/* First of all we have to check to see if the leader died */
/* If he died, we go to an election, if not we go to WAIT */
/* If he did die, and we are the new leader we break out and run for it */
/* This occurs many time in the code */

ftmpi_sys_check_for_events (&tgid, -1);
rc = find_event_by_gid (leader_id);
if (rc) { /* leader that i know of died */
	astate = FT_STATE_ELECT;
}
else
	astate = FT_STATE_WAIT;		
	/* so we get the correct ACK epoch value */
/* 	astate = FT_STATE_ACK; */

while (!recovered) { /* loop till we recover */

	/* check for a rouge abort */
/* 	ftmpi_sys_check_for_mpi_abort (); */
   /* not needed as get a state update via a registered NS callback */

	switch (astate) {

		/* send an ack message to the leader */
		case FT_STATE_ACK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_ACK\n", gid); fflush(stdout);
#endif

			/* Send an ACK message */
			rc = ftmpi_sys_send_leader_ack (tepoch);

			/* if all was well, goto WAIT for further instructions */
			/* if we had an error code, try again and THEN goto wait! */
			/* we don't go to ELECT until we get an event for him */
			/* if (rc<0) { */
			/* 	usleep (100000);  rc = ftmpi_sys_send_leader_ack (tepoch); */
			/* 	} */

			lastack_leader=tleader;
			lastack_epoch=tepoch;
			
			astate = FT_STATE_WAIT;	
			acked_yet = 1;			/* so we can now escape if we get an OK */
									/* or else the leader might be slow */
									/* and our state info outofdate */
									/* i.e. we see the old FT_OK */

			break;

		case FT_STATE_BUILD:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_BUILD\n", gid); fflush(stdout);
#endif

			/* here we get the new global state from our leader */
			/* Actually it has already happened as we move from FT_STATE_WAIT */
			/* here we just record it and get on with our lives */
			/* afterward we have to do a loop for the leader */

			/* copy the tmp info over */
#ifdef VERBOSE
			fprintf(stdout,"0x%x FT_STATE_BUILD new ext %d nproc %d epoch %d\n",
					gid, textent, tnproc, tepoch);
#endif
			gextent = textent;
			gnproc = tnproc;
			gepoch = tepoch;

			/* again we make sure we don't do too many builds! */
			lastbuild_leader=tleader;
			lastbuild_epoch=tepoch;

			ftmpi_array_free (&ggids);
			ftmpi_array_mkspace (&ggids, gextent);
			ftmpi_array_copy (ggids, newgids, gextent);

			astate = FT_STATE_CHECK;
			
			break;

		case FT_STATE_WAIT:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_WAIT\n", gid); fflush(stdout);
#endif

			/* Ok, first we make sure the leader hasnot popped his socks */
			ftmpi_sys_check_for_events (&tc, -1);
			rc = find_event_by_gid (leader_id);

			if (rc) { /* our leader has died! */
#ifdef VERBOSE
				printf("0x%x FT_STATE_WAIT delected loss of leader 0x%x\n",
						gid, leader_id);
#endif
				astate = FT_STATE_ELECT;
				break;
				}

			/* Here we loop around until the state changes to something else */
#ifdef FTMPI_SYS_STATE_PUSH
			rc = ftmpi_sys_wait_state (&newstate, &tleader, &tepoch, &tnproc, 
							&textent, &newgids, 1, 1000, 1);
#else
			rc = ftmpi_sys_get_state (&newstate, &tleader, &tepoch, &tnproc, 
							&textent, &newgids, 1, 1000, 1);
#endif

			if (rc==1) { /* someone exited, we loop back to check the leader */
				astate = FT_STATE_WAIT;
				}
			if (rc<0) { /* no state record?? at this point pathalogical */
					fprintf(stderr,"0x%x get state failed here?  Halting!!\n", gid);
				astate = FT_STATE_HALT;
				}
			/* Note: rc > 0 for push, rc=0 for both push & poll */
			if (rc>=0) { /* we found it, so check the state carefully */
				if (tleader==leader_id) { /* the one I trust? */
					if (newstate==FT_STATE_ACK) {
						if ((lastack_leader!=tleader)||(lastack_epoch!=tepoch))
							{	/* we have not acked this request yet */
								/* this check prevents ACK flooding */
							astate = FT_STATE_ACK;
							lastack_leader=tleader;
							lastack_epoch=tepoch;
							}
						}
					if (newstate==FT_STATE_CHECK) astate = FT_STATE_CHECK;
					if (newstate==FT_STATE_BUILD) 
						if ((lastbuild_leader!=tleader)||(lastbuild_epoch!=tepoch))
							{	/* we have not acked this request yet */
								/* this check prevents ACK flooding */
							astate = FT_STATE_BUILD;
							lastbuild_leader=tleader;
							lastbuild_epoch=tepoch;
							}
					if (newstate==FT_STATE_ELECT) astate = FT_STATE_ELECT;
					if (newstate==FT_STATE_OK) astate = FT_STATE_OK;
					if (newstate==FT_STATE_WAIT) astate = FT_STATE_WAIT;
					if (newstate==FT_STATE_WAIT) usleep (15000);	/* longer time */
					}
				else { /* I am not sure I trust the posted state */
#ifdef REALVERBOSE
					printf("0x%x FT_STATE_WAIT state %d post by 0x%x not 0x%x\n", gid, newstate, tleader, leader_id);
#endif
					astate = FT_STATE_WAIT;
					} /* not sure */
				usleep (100000);
			 	} /* found a state record */
			break;

		case FT_STATE_OK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_OK\n", gid); fflush(stdout);
#endif
			if (acked_yet) {
				/* we are so happy to be here ! */
				recovered = 1;
				return (0);	/* get us outa here */
				}
			else { /* it could be just an out of date state */
				   /* we are not allowed to escape until we do an ACK */
				   /* so we force ourselves back to WAIT */
					astate = FT_STATE_WAIT;
					usleep (10000);
				 }

			break;

		case FT_STATE_ELECT:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_ELECT\n", gid); fflush(stdout);
#endif
	
			/* check for a rouge abort */
			ftmpi_sys_check_for_mpi_abort ();
			/* this is done here as it will catch an abort by a leader! */

			/* here we try and become the new leader */
			rc = ftmpi_sys_elect_new_leader ();

			if (rc<0) { /* pathalogical error */
				fprintf(stderr,"0x%x leader elect failed? Halting!!\n", gid);
				astate = FT_STATE_HALT;
				}
			else { /* we have a new leader! */
				if (rc==gid) { /* I am the ONE */
					recovered=1; /* not really but now its my turn! */
					gepoch+=10;	/* leap frog my epoch count */
            		gepoch = (gepoch % 10000); /* prevent roll over */
                                        ftmpi_unreg_wait_for_db_record(DB_FT_MPI_STATE, runid, 0);
					return (1);	/* I am a new leader */
					}
				else  /* another new leader, another euro */
					astate = FT_STATE_WAIT; /* for new words of wisdom */
				} /* new leader */

			break;

		case FT_STATE_CHECK:
#ifdef VERBOSE
			fprintf(stdout,"0x%x in FT_STATE_CHECK\n", gid); fflush(stdout);
#endif

#ifdef CHECK_RING
			rc = ftmpi_sys_ring_loop (ggids, gextent, gepoch);
#endif /* CHECK_RING */

#ifdef CHECK_ACK

			/* Send an ACK message */
			rc = ftmpi_sys_send_leader_ack (tepoch);

#endif /* CHECK_ACK */

			/* we don't check the error code as if there was the leader */
			/* will post a new ACK phase. */
			/* if all was well, he will post an OK state (we hope) */
			/* so we go back and await our fate */
			
			astate = FT_STATE_WAIT;
			break;

		case FT_STATE_HALT:
			fprintf(stdout,"0x%x in FT_STATE_HALT\n", gid); fflush(stdout);
			fprintf(stderr,"0x%x Halting\n", gid); fflush(stderr);
			ftmpi_sys_leave (0,0);
			recovered = -1;
			break;	

		case FT_STATE_ABORT:
			ftmpi_sys_mpi_abort ();
			break;

		default:
			/* never happens */
			fprintf(stderr,"GEF forgot something!\nEmail fagg@cs.utk.edu\n");
			ftmpi_sys_leave (0,0);
			break;

		} /* switch on attempted state */
	} /* while not recovered */


return (recovered);
}
/***************************************************************************/

/***************************************************************************/
/*
	ftmpi_sys_check_for_mpi_abort

	This routine is called once at the entry to the recovery loop
	It is designed to catch a processes posting a MPI_Abort prompted
	FT_STATE_ABORT record.

	We have to check for this on every entry to the MPI recovery because:
	(a) leaders post ACK states without checking
	(b) peons would not catch a leader calling ABORT as it would
		force them to post a ELECT state to get a new leader!

	i.e. this routine is here to prevent races and too much polling for 
	record state changes.

	GEF Stuttgart June2002
	Second attempt!

	Third attempt.
	Now leaders still use this call before doing an ACK state
	Peons do different things.
	(1) they check on a death event of a LEADER for a FT_STATE_ABORT 
	if they find this they just abort. (this is done in the elect calls).
	(2) if a FT_STATE_ABORT state is set they now call the ftmpi_sys_mpi_abort
	call directly.

*/
/***************************************************************************/
		
void	ftmpi_sys_check_for_mpi_abort ( )
{

int newstate;
int rc;

/* check the db, only return state, and no looping, updating etc etc */
newstate = ! FT_STATE_ABORT; /* anything but ABORT so we will know */

rc = ftmpi_sys_get_state (&newstate, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);

if (newstate!=FT_STATE_ABORT) return;	/* not abort, phew */
else
   ftmpi_sys_mpi_abort ();
}


/* abort routine that can be called from anywhere without polling first */
/* this is only to be called AFTER an ABORT has been posted via the NS */
/* i.e. this is what the 'others' should call on detecting an abort */

void	ftmpi_sys_mpi_abort ( )
{
/* #ifdef VERBOSE */
			fprintf(stderr,"0x%x Aborting FTMPI application.\n", gid);
/* #endif */
			/* clean up NS but don't send death event */
			/* second 0 = don't remove runstate */
			ftmpi_sys_leave (0, 0);	

			exit (MPI_ERR_ABORT_CALLED);	/* yep die */
}
/***************************************************************************/


/***************************************************************************/
/* Monitor wrappers and macros etc */
/***************************************************************************/

int ftmpi_sys_find_monitor ()
{
int m;

/* Ok now for the monitoring GUI checks */
if (displaycinfo) { /* we can attach to a monitor */
	if (!displaycinfofound) {
		m = ft_mpi_cinfo_find ();
		if (!m) {
			displaycinfofound=1;	/* found a monitoring process */
			if (!ftmpi_silent) puts("Monitor display found\n");
		}
		else {
			displaycinfofound = 0;		/* didn't find one... */
			if (!ftmpi_silent) puts("Monitor display enabled, but did NOT find a monitoring tool");
			}

	}
}
else
	if (!ftmpi_silent) puts("Monitor Display not enabled");
 return 0; 
}
/***************************************************************************/


/***************************************************************************/
/*
 This is where the leader can post the 'correct' or 'new' MCW
 firsttime = 1 if its the first time else its an 'update'
 'updates are slow as we need to send the status info one by one currently...
 I try and get around this by having a 'change' map of processes that have changed...
GEF Nov02
*/
/***************************************************************************/

int ftmpi_sys_monitor_post_mcw (int firsttime)
{
int i;
char appname[512];
int pstate[MAXPERAPP];

if (firsttime) {
	/* build app name and indicate RUNID also */
	sprintf(appname,"%s : RUNID=0x%x", myexename, runid);
	printf("App name sent to Monitor is %s\n", appname);
	for(i=0;i<MAXPERAPP;i++) pstate[i]=0;	/* init process states */


	ft_mpi_cinfo_req_new_comm(MPI_COMM_WORLD,gextent,1,0,appname,ggids,pstate);
}
else {
#ifdef update_com_works
	for(i=0;i<MAXPERAPP;i++) pstate[i]=0;	/* init process states */
	ft_mpi_cinfo_req_update_com(MPI_COMM_WORLD,gextent,1,0,appname,ggids,pstate);
#else
	for(i=0;i<gextent;i++) {
		if (ggids[i]<=0)  
    		ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, i, 3); 
				/* 3 = black and away... aka a gap, a hole.. a space */
		else
    		ft_mpi_cinfo_req_proc_state (MPI_COMM_WORLD, i, 0); /* 0 = ok */
		}	
#endif

	}
 return 0; 
}
/***************************************************************************/
/* This function determins the ranks which have changed after a recovery.
   Assumption: the position of the gid in the gid-array is euqal to
   its rank in MPI_COMM_WORLD.
*/
int ftmpi_sys_create_failedlist ( int oldarrsize, int oldgids[], 
				  int newarrsize, int newgids[] )
{
  int i, j, k, numchanged=0;
  int found;

  ftmpi_failed_list_count =0; /* reset count just in case, reset me */

  if ( (oldarrsize <= 0 ) || (newarrsize <= 0 ) ||
       (oldgids == NULL ) || ( newgids == NULL ) )
    return ( 0 );


  /* Determine how many have changed first */
  if (oldarrsize != newarrsize ) {
    /* This section is for the shrink mode */
    for ( i = 0; i < oldarrsize; i++ ) {
      found = 0;
      for ( j = 0; j < newarrsize; j++ ) {
	if ( oldgids[i] == newgids[j] ) {
	  found=1;
	  break;
	}
      }
      if ( !found )
	numchanged++;
    }
  }
  else {
    /* This section is for the blank/rebuild mode */
    for ( i = 0; i < oldarrsize; i++ ) {
      if ( oldgids[i]  != newgids[i] ) 
	numchanged++;
    }
  }
  
  ftmpi_array_free (&ftmpi_failed_list);
  ftmpi_array_mkspace (&ftmpi_failed_list, numchanged);
  
  /* Determine now who has changed  */
  if (oldarrsize != newarrsize ) {
    /* This section is for the shrink mode */
    for ( k = 0, i = 0; i < oldarrsize; i++ ) {
      found = 0;
      for ( j = 0; j < newarrsize; j++ ) {
	if ( oldgids[i] == newgids[j] ) {
	  found=1;
	  break;
	}
      }
      if ( !found )
	ftmpi_failed_list[k++] = i;
    }
  }
  else {
    /* This section is for the blank/rebuild mode */
    for ( k= 0, i = 0; i < oldarrsize; i++ ) {
      if ( oldgids[i]  != newgids[i] ) 
	ftmpi_failed_list[k++] = i;
    }
  }

  ftmpi_failed_list_count = numchanged; /* remember me */

  return ( numchanged );
}
/***************************************************************************/


int ftmpi_sys_force_fullyconnected ()
{
   int rc=0;

   /* this routine is called here as we have the GID list and extent */
   /* these are needed by the conn library to open the connections */

#ifdef VERBOSES2
   printf("ftmpi_sys_force_fullyconnected on %d gids. My gid 0x%x\n", 
		 gextent, gid);
#endif

   	/* non xon/off version */
	/* rc = conn_all_mkconn (ggids, gextent); */

   printf("0x%x:force fullyconnected ignored. using dynamic connection on post\n", gid);

#ifdef VERBOSES2
	printf("fullyconnected on 0x%x finished with rc %d\n", gid, rc);
#endif

	return (rc);
}


/***************************************************************************/

int ftmpi_sys_show_all (char* label)
{
   	 printf("Show all for gid %d : %s\n", gid, label);
     ftmpi_comm_dump_all_compact ();
	 ftmpi_nb_dump_all_queues ();
	 ftmpi_nb_dump_req_table (1);
return (0);
}

/***************************************************************************/


/***************************************************************************/
/* routine that moves data between queues depending on the message modes */
/***************************************************************************/
int ftmpi_sys_recovery_handle_queues (int orgcnt, int* orglist, int currentcnt, int* currentlist, int failedcnt, int* failedlist)
{
   int i;
   /* as we need the failed list in gids not ranks */
   int deadgidcnt=0;
   int *deadgidlist;

   if (gcommode==FT_MODE_REBUILD)	printf("REBUILD\n");
   if (gcommode==FT_MODE_BLANK)	printf("BLANK\n");
   if (gcommode==FT_MODE_SHRINK)	printf("SHRINK\n");
   if (gmsgmode==FT_MODE_CONT)	printf("CONT\n");
   if (gmsgmode==FT_MODE_NOP)	printf("NOP/RESET\n");


	  /* NOP/RESET this is the simplest mode */
   	  /* here everything is reset no matter what commode we have */
   	  /* all queues, reqtable ie. users requests gets reset */
   if (gmsgmode==FT_MODE_NOP) { 

	  /* reset send/recv/done/inprogress/incomplete & system queues */
	  ftmpi_nb_reset_req_and_msg_lists ();

	  /* reset each unexpected msg list for each valid com */
	  ftmpi_com_reset_all_msg_lists ();

	  /* now to clear the request table of invalid handles which covers */
	  /* everything as we cleared all the other req lists */
	  /* in the 2 steps above */
	  ftmpi_nb_clearall_reqhandles ();

	  /* for the NOP/RESET mode we are all done */
	  /* have a very nice and empty restart day :) GEF */

	  return (0);
   }

   		/* CONT message mode is more complex and depends on the com mode */
   if (gmsgmode==FT_MODE_CONT) { 

	  /* build deadgid lists */
	  if (failedcnt>0) 
		 deadgidlist = (int*) _MALLOC(sizeof(int)*failedcnt);
	  else
		 deadgidlist = (int*) NULL;

	  for(i=0;i<failedcnt;i++) {
		 if (failedlist[i]<orgcnt) {
/* 			printf("dead rank %d was GID %d\n", failedlist[i], orglist[failedlist[i]]); */
			deadgidlist[deadgidcnt++] = orglist[failedlist[i]];
		 }
	  }
		

	  /* remove all unexpected messages from any shadow comms */
	  /* this is done until the collectives are FT safe */
	  ftmpi_com_reset_shadow_msg_lists ();

	  /* delete the inprogress & incomplete lists */
	  ftmpi_nb_reset_unexpected_and_inprogress_msg_lists ();

	  /* now copy all messages from all the coms msg_lists to the system list */
	  /*  move systems lists from all coms including com self and world */ 
	  ftmpi_com_move_all_lists_to_system_list (0);

	  if (deadgidcnt) {
	  /* which will be the target of the filtering or remapping as needed */
		 ftmpi_nb_filter_system_list (deadgidcnt, deadgidlist);

	  /* filter the send/recv/done queues and reqtables as well */
		 ftmpi_nb_filter_req_lists_and_reqtable (deadgidcnt, deadgidlist);

		 if (gcommode==FT_MODE_SHRINK) {
			   /* for shrink mode we have to remap and filter */
			   /* here I have to build the remap / dead throw away list */
			   fprintf(stderr,"Warning: FT_MODE_SHRINK and FT_MODE_CONT may not remap all target ranks as expected.\nSee Readme and/or email ftmpi@cs.utk.edu for more information\n");
		 } /* if mode SHRINK */

	  if (deadgidlist) {
		 _FREE(deadgidlist);
		 deadgidlist=(int*)NULL;
	  }

	  } /* if any filtering to be done */

	  return (0);
   }

   return (MPI_ERR_INTERN); /* bad com or message mode to reach here */
}

/***************************************************************************/

