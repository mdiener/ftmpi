/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
	     Graham E Fagg <fagg@cs.utk.edu>
	     Antonin Bukovsky <tone@cs.utk.edu>
	     Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*

	MPI API group calls
*/


#include "ft-mpi-lib.h"
#include "ft-mpi-group.h"
#include "ft-mpi-lib-group.h"
#include "ft-mpi-com.h"
#include "debug.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Comm_group           = PMPI_Comm_group
#    pragma weak  MPI_Group_compare        = PMPI_Group_compare
#    pragma weak  MPI_Group_excl           = PMPI_Group_excl
#    pragma weak  MPI_Group_free           = PMPI_Group_free
#    pragma weak  MPI_Group_incl           = PMPI_Group_incl
#    pragma weak  MPI_Group_rank           = PMPI_Group_rank
#    pragma weak  MPI_Group_size           = PMPI_Group_size
#    pragma weak  MPI_Group_translate_ranks = PMPI_Group_translate_ranks
#    pragma weak  MPI_Group_difference     = PMPI_Group_difference
#    pragma weak  MPI_Group_intersection   = PMPI_Group_intersection
#    pragma weak  MPI_Group_range_excl     = PMPI_Group_range_excl
#    pragma weak  MPI_Group_range_incl     = PMPI_Group_range_incl
#    pragma weak  MPI_Group_union          = PMPI_Group_union

#endif

#    define  MPI_Comm_group            PMPI_Comm_group
#    define  MPI_Group_compare         PMPI_Group_compare
#    define  MPI_Group_excl            PMPI_Group_excl
#    define  MPI_Group_free            PMPI_Group_free
#    define  MPI_Group_incl            PMPI_Group_incl
#    define  MPI_Group_rank            PMPI_Group_rank
#    define  MPI_Group_size            PMPI_Group_size
#    define  MPI_Group_translate_ranks PMPI_Group_translate_ranks
#    define  MPI_Group_difference      PMPI_Group_difference
#    define  MPI_Group_intersection    PMPI_Group_intersection
#    define  MPI_Group_range_excl      PMPI_Group_range_excl
#    define  MPI_Group_range_incl      PMPI_Group_range_incl
#    define  MPI_Group_union           PMPI_Group_union



/* temp gid lists for compares etc etc */
static int gids1[MAXPERAPP];
static int gids2[MAXPERAPP];
static int tmp_diff[MAXPERAPP];


#define MARKED 1
#define UNMARKED 0


int MPI_Group_size (MPI_Group grp, int *size)
{
  int rc;

  CHECK_MPIINIT;
  CHECK_GROUP (MPI_COMM_WORLD, grp);
  if (!size) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);	
  
  rc = ftmpi_group_size (grp);
  if (rc<0) RETURNERR (MPI_COMM_WORLD, rc);
  
  *size = rc;
  
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_rank (MPI_Group grp, int *rank)
{
  int rc;
  
  CHECK_MPIINIT;
  CHECK_GROUP (MPI_COMM_WORLD, grp);
  if (!rank) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);	
  
  rc = ftmpi_group_rank (grp);
  *rank = rc;
  
  if ( (rc<0) && ( rc != MPI_UNDEFINED)) RETURNERR (MPI_COMM_WORLD, rc);
  
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
 Gets a new handle to the group used to define MPI_Comm
 */

int MPI_Comm_group(MPI_Comm comm, MPI_Group *grp)
{
  int rc;
  MPI_Group newgrp, oldgrp;

  /* check args in order */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  if (!grp) RETURNERR (comm, MPI_ERR_ARG);	/* no null pointers thanks */
  
  /* now get the group in the original communicator */
  oldgrp = ftmpi_com_get_group (comm);
  if (oldgrp==MPI_UNDEFINED)  /* ? */
    RETURNERR (comm, MPI_ERR_INTERN);

  rc = ftmpi_group_copy (oldgrp, &newgrp);
  if ( rc != MPI_SUCCESS )
    RETURNERR ( comm, rc );
  
  *grp=newgrp;

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_free(MPI_Group *grp)
{
  int rc;

  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, *grp);
  
  rc = ftmpi_group_free (grp);
  if ( rc != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, rc );

  *grp = MPI_GROUP_NULL;
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_incl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp)
{
  int rc;
  int s;
  int i, j;

  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, grp);
  if (!newgrp) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG); 
  if ( ((n==0)&&(!ranks)) || (n<0)) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG); 
  
  for ( i = 0; i < n; i++ ) {
    for ( j = 0; j < n; j++ ) {
      if ( ( i != j  ) && ( ranks[i] == ranks[j] ) )
	RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);
    }
  }
  
  if (n==0) {
    *newgrp = MPI_GROUP_EMPTY;
    return ( MPI_SUCCESS);
  }

  s = ftmpi_group_size (grp);
  if (n>s) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);	/* too many ranks */

  rc = ftmpi_mpi_group_incl ( grp, n, ranks, newgrp);
  if ( rc != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, rc );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_group_incl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp)
{
  int i, j;
  int ngrp;
  int gid;	/* needed so I can set my rank in the new group */
  int rc;
  int s;

  /* NOTE use of n vs s from here */
  /* esp as n can be less than s */

  s = ftmpi_group_size (grp);
  
  /* Make a list for us */
  for(i=0;i<s;i++)	gids1[i]=ftmpi_group_gid (grp, i);

  /* make sure the target list is init correctly */
  for(i=0;i<s;i++)	gids2[i]= MPI_UNDEFINED;

  /* ok build reordered list */
  /* if error.. then just abort */

  for (i=0;i<n;i++) {
    j = ranks[i];
    if ((j<0)||(j>=s)) { /* bad rank */
      return ( MPI_ERR_RANK );
    }

    gids2[i] = gids1[j];  /* proc with rank i in newgroup is proc with rank */
                          /* ranks[i] in group */
  }	

  /* we now have a list of ranks.. so we need to build a 'new' 
     group with them */

  /* we copy the old group.. */
  rc =  ftmpi_group_copy (grp, &ngrp);
  
  /* get my gid */
  gid = ftmpi_com_my_gid ();

  /* then update the ids and size, my rank within it etc */
  rc = ftmpi_group_build (ngrp, gid, gids2, n, n);

  /* if all is well set the return code correctly */
  *newgrp = ngrp;

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_translate_ranks(MPI_Group group1,int n, int *ranks1,
			      MPI_Group group2, int *ranks2)
{
  int i,j;
  group_t * gptr1 = NULL,* gptr2 = NULL;
  int size1,size2;

  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, group1);
  CHECK_GROUP(MPI_COMM_WORLD, group2);
  
  if(n <= 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  if(ranks1 == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  size1 = ftmpi_group_size (group1);
  if(n>size1)
    RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);

  if(ranks2 == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  /* 
  if(n>size2)
    RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);
  */

  for(i=0;i<n;i++){
    if(ranks1[i] < 0 || ranks1[i] >= size1){
      RETURNERR(MPI_COMM_WORLD, MPI_ERR_RANK);
    }
    /* initialize in the same loop ranks2 */
    ranks2[i] = MPI_UNDEFINED;
  }

  size1 = ftmpi_group_size (group1);
  size2 = ftmpi_group_size(group2);

  
  gptr1 = (group_t *)ftmpi_group_get_ptr(group1);
  gptr2 = (group_t *)ftmpi_group_get_ptr(group2);

  if(gptr1 != NULL && gptr2 != NULL){
    for(i=0;i<n;i++){
      for(j=0;j<size2;j++){
        if(gptr1->gids[ranks1[i]] == gptr2->gids[j]){
          ranks2[i] = j;
          break;
        }
      }
      if(j==size2)
        ranks2[i] = MPI_UNDEFINED;
    }
  }
  
  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_range_incl(MPI_Group group,int n, int ranges[][3],
			 MPI_Group * newgroup)
{
  int ret;
  int size;
  int i;

  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, group);
  if(n < 0) RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);
  if(ranges == NULL) RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);


  for ( i = 0; i < n; i ++ ) {
  /* Check for stride < 0 AND first < last */
  if ( ( ranges[i][2] < 0 ) && ( ranges[i][0] < ranges[i][1] ))
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);

  /* Check for stride > 0 AND first > last */
  if ( ( ranges[i][2] > 0 ) && ( ranges[i][0] > ranges[i][1] ))
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);
  }


  if (n==0) 
    {
      *newgroup = MPI_GROUP_EMPTY;
      return ( MPI_SUCCESS );
    }

  size = ftmpi_group_size(group);
  if (n>size) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);  /* too many ranks */

  ret = ftmpi_mpi_group_range_incl ( group, n, ranges, newgroup );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_group_range_incl(MPI_Group group,int n, int ranges[][3],
			       MPI_Group * newgroup)
{
  int gid,ngrp;
  int i,j,k=0;
  int ret;
  int size;

  size = ftmpi_group_size(group);

  for(i=0;i<size;i++){
    gids1[i]=ftmpi_group_gid (group, i);
    gids2[i]= MPI_UNDEFINED;
  }

  for(i=0;i<n;i++){
    if(ranges[i][2] > 0){
      if ( (ranges[i][0] <0) || (ranges[i][0] >= size ))
	return ( MPI_ERR_RANK );
      if ( (ranges[i][1] <0) || (ranges[i][1] >= size ))
	return ( MPI_ERR_RANK );

      for(j=ranges[i][0];j<=ranges[i][1];j+=ranges[i][2]){
        gids2[k] = gids1[j];
        k++;
      }
    }
    else if ( ranges[i][2] < 0 ) {
      for(j=ranges[i][0];j>=ranges[i][1];j+=ranges[i][2]){
        gids2[k] = gids1[j];
        k++;
      }
    }
    else { /* ranges[i][2] == 0 */
      printf("MPI_Group_range_incl: stride = 0 not allowed!\n" );
      return ( MPI_ERR_ARG );
    }	

  }
  
  ret =  ftmpi_group_copy (group, &ngrp);
  gid = ftmpi_com_my_gid ();
  ret = ftmpi_group_build (ngrp,gid, gids2, k, k);

  *newgroup = ngrp;
  return ( MPI_SUCCESS );
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/* Version 2.0 of this code rewritten by Edgar to use
   ftmpi_group_build, since in the old version not all
   elements of the group structure have been set !
   EG Feb. 4 200 */
int MPI_Group_difference ( MPI_Group group1, MPI_Group group2,
			   MPI_Group *group_out )
{
  int i, j, global_rank;
  int ret;
  int n;
  int found;
  MPI_Group ngrp;
  group_t * group1_ptr = NULL;
  group_t * group2_ptr = NULL;
  
  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, group1);
  CHECK_GROUP(MPI_COMM_WORLD, group2);

  if(group1 == MPI_GROUP_EMPTY && group2 == MPI_GROUP_EMPTY){
    *group_out = MPI_GROUP_EMPTY;
    return ( MPI_SUCCESS);
  }

  if( (group1 == MPI_GROUP_EMPTY) && (group2 != MPI_GROUP_EMPTY)){
    *group_out = MPI_GROUP_EMPTY;
    return ( MPI_SUCCESS);
  }

  if( (group1 != MPI_GROUP_EMPTY) && (group2 == MPI_GROUP_EMPTY)){
    ret = ftmpi_group_copy(group1,&ngrp);
    *group_out = ngrp;  
    return ( MPI_SUCCESS);
  }

  
  group1_ptr = (group_t *)ftmpi_group_get_ptr(group1);
  group2_ptr = (group_t *)ftmpi_group_get_ptr(group2);

  ret = ftmpi_group_copy(group1,&ngrp);
  
  /* Determine the processes which are in group 1 but not in group 2*/
  for(n = 0, i=0; i<group1_ptr->nprocs; i++ ) 
    {
      found = 0;
      for(j=0;j<group2_ptr->nprocs; j++ )
	{
	  if(group1_ptr->gids[i] == group2_ptr->gids[j] ) 
	    {
	      found = 1;
	      break;
	    }
	}
    
      if ( found == 0 )
	{
	  tmp_diff[n] = group1_ptr->gids[i];
	  n++;
	}
    }
  

  /* If there is a null intersection */
  if(n <= 0){
    /* OK NOW WHAT ??? I WILL NEED TO FREE THE NEW GRP AND CREATE A NEW ONE. */
    ftmpi_group_free(&ngrp);
    *group_out = MPI_GROUP_EMPTY;
    return (MPI_SUCCESS);
  }
  
   /* Find the local rank */
  global_rank = ftmpi_com_my_gid();

  /* Set the elements of the structure correctly */
  ftmpi_group_build ( ngrp, global_rank, tmp_diff, n, n );

  *group_out = ngrp;  
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Version 2.0 of this code rewritten by Edgar to use
   ftmpi_group_build, since in the old version not all
   elements of the group structure have been set !
   EG Feb. 4 200 */
int MPI_Group_intersection ( MPI_Group group1, MPI_Group group2,
			     MPI_Group *group_out )
{
  int ret;
  
  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, group1);
  CHECK_GROUP(MPI_COMM_WORLD, group2);
  
  if(group1 == MPI_GROUP_EMPTY || group2 == MPI_GROUP_EMPTY){
    *group_out = MPI_GROUP_EMPTY;
    return (MPI_SUCCESS);
  }

  ret = ftmpi_mpi_group_intersection ( group1, group2, group_out );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_group_intersection ( MPI_Group group1, MPI_Group group2,
				   MPI_Group *group_out )
{  
  int n;
  int found;
  MPI_Group ngrp;
  group_t * group1_ptr = NULL;
  group_t * group2_ptr = NULL;
  int i, j, global_rank;
  int ret;

  group1_ptr = (group_t *)ftmpi_group_get_ptr(group1);
  group2_ptr = (group_t *)ftmpi_group_get_ptr(group2);

  ret = ftmpi_group_copy(group1,&ngrp);
  
  /* Determine the processes which are in group 1 and in group 2*/
  for(n = 0, i=0; i<group1_ptr->nprocs; i++ ) 
    {
      found = 0;
      for(j=0;j<group2_ptr->nprocs; j++ )
	{
	  if(group1_ptr->gids[i] == group2_ptr->gids[j] ) 
	    {
	      found = 1;
	      break;
	    }
	}
      if ( found == 1 )
	{
	  tmp_diff[n] = group1_ptr->gids[i];
	  n++;
	}
    }
  
  
  /* If there is a null intersection */
  if(n <= 0){
    ftmpi_group_free(&ngrp);
    *group_out = MPI_GROUP_EMPTY;
    return (MPI_SUCCESS);
  }
  
  /* Find the local rank and set the group-structure */ 
  global_rank = ftmpi_com_my_gid();
  ftmpi_group_build ( ngrp, global_rank, tmp_diff, n, n );
 
  *group_out = ngrp;
  return (MPI_SUCCESS);
}
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/* Version 2.0 of this code rewritten by Edgar to use
   ftmpi_group_build, since in the old version not all
   elements of the group structure have been set !
   EG Feb. 4 200 */
int MPI_Group_union ( MPI_Group group1, MPI_Group group2,
		      MPI_Group *group_out )
{
  int i, j, global_rank;
  int ret;
  int n;
  int found;
  MPI_Group ngrp;
  group_t * group1_ptr = NULL;
  group_t * group2_ptr = NULL;
  
  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, group1);
  CHECK_GROUP(MPI_COMM_WORLD, group2);
  
  if(group1 == MPI_GROUP_EMPTY && group2 == MPI_GROUP_EMPTY){
    *group_out = MPI_GROUP_EMPTY;
    return (MPI_SUCCESS);
  }
  
  if( (group1 == MPI_GROUP_EMPTY) && (group2 != MPI_GROUP_EMPTY)){
    ret = ftmpi_group_copy(group2,&ngrp);
    *group_out = ngrp;
    return (MPI_SUCCESS);
  }

  if( (group1 != MPI_GROUP_EMPTY) && (group2 == MPI_GROUP_EMPTY)){
    ret = ftmpi_group_copy(group1,&ngrp);
    *group_out = ngrp;
    return (MPI_SUCCESS);
  }
  
  group1_ptr = (group_t *)ftmpi_group_get_ptr(group1);
  group2_ptr = (group_t *)ftmpi_group_get_ptr(group2);

  ret = ftmpi_group_copy(group1,&ngrp);

  /* To determine the superset of group1 and group2 
     we copy first all elements of group1 into tmp_diff */
  for ( i = 0 ; i < group1_ptr->nprocs; i++)
    tmp_diff[i] = group1_ptr->gids[i];
     

  /* Find now the processes which are in group2
     and not in group 1 */
  n = group1_ptr->nprocs;
  for ( i = 0; i < group2_ptr->nprocs; i ++ )
    {
      found = 0;
      for ( j = 0; j < group1_ptr->nprocs; j ++ )
	{
	  if (group2_ptr->gids[i] == group1_ptr->gids[j])
	    {
	      /* Proccess was already in group 1, no need
		 to copy it again */
	      found = 1;
	      break;
	    }
	}
      if ( found == 0 )
	{
	  tmp_diff[n] = group2_ptr->gids[i];
	  n++;
	}
    }


  global_rank = ftmpi_com_my_gid ();
  ftmpi_group_build ( ngrp, global_rank, tmp_diff, n, n );

  *group_out = ngrp;
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_compare ( MPI_Group group1, MPI_Group group2, int *result )
{
  int size1, size2;
  int size_int, i;
  MPI_Group group_int;
  group_t * group1_ptr = NULL;
  group_t * group2_ptr = NULL;

  CHECK_MPIINIT;
  
  /* This is a silly check, but the fastest way to pass an MPICH test*/
  if ( (group1 == MPI_GROUP_EMPTY) && ( group2 == MPI_GROUP_EMPTY))
    {
      *result = MPI_IDENT;
      return ( MPI_SUCCESS );
    }


  if ( group1 == MPI_GROUP_EMPTY )
    {
      CHECK_GROUP(MPI_COMM_WORLD, group2);

      size2 = ftmpi_group_size (group2);
      if ( size2 == 0 )
	*result = MPI_IDENT;
      else
	*result = MPI_UNEQUAL;

      return ( MPI_SUCCESS );
    }

  if ( group2 == MPI_GROUP_EMPTY )
    {
      CHECK_GROUP(MPI_COMM_WORLD, group1);

      size1 = ftmpi_group_size (group1);
      if ( size1 == 0 )
	*result = MPI_IDENT;
      else
	*result = MPI_UNEQUAL;
      
      return ( MPI_SUCCESS );
    }

  CHECK_GROUP(MPI_COMM_WORLD, group1);
  CHECK_GROUP(MPI_COMM_WORLD, group2);
  
  if(result == NULL) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  group1_ptr = (group_t *)ftmpi_group_get_ptr(group1);
  group2_ptr = (group_t *)ftmpi_group_get_ptr(group2);
 
  /* See if their sizes are equal */
  size1 = ftmpi_group_size ( group1);
  size2 = ftmpi_group_size ( group2 );
  if ( size1 != size2 ) {
    *result = MPI_UNEQUAL;
    return ( MPI_SUCCESS );
  }

  /* Is their intersection the same size as the original group */
  MPI_Group_intersection ( group1, group2, &group_int );
  size_int = ftmpi_group_size ( group_int );
  ftmpi_group_free ( &group_int );
  if( size_int != size1){
    *result = MPI_UNEQUAL;
    return ( MPI_SUCCESS );
  }

  /* Do a 1-1 comparison */
  *result = MPI_SIMILAR;
  for ( i=0; i<size1; i++ ){
    if ( group1_ptr->gids[i] != group2_ptr->gids[i] ) {
      return ( MPI_SUCCESS );
    }
  }
  *result = MPI_IDENT;

  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_excl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp)
{
  int rc;
  int s;
  int i, j;
 
  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, grp);

  if (!newgrp) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if (((n==0)&&(!ranks)) || (n<0)) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  for ( i = 0; i < n; i++ ) {
    for ( j = 0; j < n; j++ ) {
      if ( ( i != j  ) && ( ranks[i] == ranks[j] ) )
	RETURNERR(MPI_COMM_WORLD, MPI_ERR_ARG);
    }
  }

  s = ftmpi_group_size (grp);
  if (n>s) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);	/* too many ranks */


  rc = ftmpi_mpi_group_excl ( grp, n, ranks, newgrp );
  if ( rc != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, rc );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_group_excl(MPI_Group grp,int n,int *ranks, MPI_Group *newgrp)
{
  int s;
  int i, j, k, l;
  int ngrp;
  int gid;	/* needed so I can set my rank in the new group */
  int rc;

  if ( n == 0 )
    {
      /* Nothing to exclude, just copy the group */
      rc = ftmpi_group_copy ( grp, &ngrp );
      *newgrp = ngrp;
      return ( MPI_SUCCESS );
    }

  s = ftmpi_group_size (grp);
  if ( n == s ) 
    { /* exclude everything */
      *newgrp = MPI_GROUP_EMPTY;
      return ( MPI_SUCCESS );
    }

  /* NOTE use of n vs s from here */
  /* esp as n can be less than s */
  
  /* Make a list for us */
  for(i=0;i<s;i++)	gids1[i]=ftmpi_group_gid (grp, i);

  /* make sure the target list is init correctly */
  for(i=0;i<s;i++)	gids2[i]= MPI_UNDEFINED;

  /* ok build reordered list */
  /* if error.. then just abort */

  /* we just run through the list of procs in the group
     and compare them with the list of nodes to be excluded.
     if a processes does not match, it is included in the new
     group */

  for ( i = 0; i < n ; i ++ )
    if ( (ranks[i] < 0) || ( ranks[i] >= s ))
      return ( MPI_ERR_RANK ); /* bad ranks */

  for ( l = 0, i = 0; i < s; i ++)
    {
      j = 0;
      for ( k = 0; k < n; k ++ )
	{
	  /* compare whether this gids[] is on the list
	     to be excluded */
	  if ( i == ranks [k] )
	    {
	      j = 1;
	      break;
	    }
	}	  

      if ( j == 0 )
	{
	  /* Node was not on the list to be excluded */
	  gids2[l] = gids1[i];
	  l++;
	}
    }

  /* we now have a list of ranks.. so we need to build a 'new' 
     group with them */

  /* we copy the old group.. */
  rc =  ftmpi_group_copy (grp, &ngrp);
  
  /* get my gid */
  gid = ftmpi_com_my_gid ();
  
  
  /* then update the ids and size, my rank within it etc */
  rc = ftmpi_group_build (ngrp, gid, gids2, (s-n), (s-n) );

  /* if all is well set the return code correctly */
  *newgrp = ngrp;

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Group_range_excl (MPI_Group grp, int n, int ranges[][3], 
			  MPI_Group *newgrp )
{
  int ret;
  int rc;
  int ngrp;
  int *temp_array;
  int temp_n = 0;
  int i, j ;
  int ub;
  int size;

  CHECK_MPIINIT;
  CHECK_GROUP(MPI_COMM_WORLD, grp);

  if (!newgrp) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG); /* no null pointers */
  if (((n==0)&&(!ranges)) || (n<0)) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG); 
  
  for ( i = 0; i < n; i ++ ) {
  /* Check for stride < 0 AND first < last */
  if ( ( ranges[i][2] < 0 ) && ( ranges[i][0] < ranges[i][1] ))
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);

  /* Check for stride > 0 AND first > last */
  if ( ( ranges[i][2] > 0 ) && ( ranges[i][0] > ranges[i][1] ))
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG);
  }

  if ( n == 0 )
    {
      /* no procs to exclude */
      rc = ftmpi_group_copy ( grp, &ngrp);
      *newgrp = ngrp ;
      return ( MPI_SUCCESS );
    }

  size = ftmpi_group_size (grp);
  /* allocate temporary array */
  temp_array = (int *)_MALLOC(size * sizeof(int));
  if ( temp_array == NULL ) RETURNERR (MPI_COMM_WORLD,  MPI_ERR_INTERN );

  /* Determine nodes */
  for ( i = 0; i < n ; i ++ )
    {
      if ( ranges[i][2] == 0 )
	{
	  printf("MPI_Group_range_excl: stride = 0 not allowed!\n" );
	  RETURNERR (MPI_COMM_WORLD,  MPI_ERR_ARG );
	}
      if ( (ranges[i][0] <0) || (ranges[i][0] >= size ))
	RETURNERR( MPI_COMM_WORLD, MPI_ERR_RANK);
      if ( (ranges[i][1] <0) || (ranges[i][1] >= size ))
	RETURNERR( MPI_COMM_WORLD, MPI_ERR_RANK);

      ub = (int) ((ranges[i][1] - ranges[i][0])/ranges[i][2]);
      for ( j = 0; j <= ub; j ++ )
	{
	  temp_array[temp_n] = ranges[i][0] + j * ranges[i][2];
	  temp_n++;
	}
    }
     
  if ( size == temp_n)
    { /* exclude everything */
      *newgrp = MPI_GROUP_EMPTY;
      ret = MPI_SUCCESS;
    }
  else
    {
      ret = ftmpi_mpi_group_excl (grp, temp_n, temp_array, newgrp );
      if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
    }

  _FREE( temp_array );
  return ( ret );
}
