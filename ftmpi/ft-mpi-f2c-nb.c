/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include "ft-mpi-f2c-nb.h"
#include "ft-mpi-fprot.h"

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Bsend_init(void * buf,int cnt,MPI_Datatype ddt,int dest,
   int tag,MPI_Comm comm,MPI_Request * request) */
void mpi_bsend_init_(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr)
{
  *__ierr = MPI_Bsend_init((void *)ToPtr(buf),(*((int *)cnt)),
			   (MPI_Datatype)fdt2c((*((int *)ddt))),
			   (*((int *)dest)),(*((int *)tag)),
			   (MPI_Comm)(*((int *)comm)),
			   (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Cancel(MPI_Request * request) */
void mpi_cancel_(int *request,int *__ierr)
{
   *__ierr = MPI_Cancel((MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Ibsend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_ibsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Ibsend((void *)ToPtr(buf),(*((int *)cnt)),
		       (MPI_Datatype)fdt2c((*((int *)ddt))),
		       (*((int *)dest)),(*((int *)tag)),
		       (MPI_Comm)(*((int *)comm)),
		       (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Iprobe(int source,int tag,MPI_Comm comm,int * flag,
   MPI_Status * status) */
void mpi_iprobe_(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr)
{
  *__ierr = MPI_Iprobe((*((int *)source)),(*((int *)tag)),
		       (MPI_Comm)(*((int *)comm)),(int *)ToPtr(flag),
		       (MPI_Status *)ToPtr(status));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Irecv(void * buf,int cnt,MPI_Datatype ddt,int source,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_irecv_(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Irecv((void *)ToPtr(buf),(*((int *)cnt)),
		      (MPI_Datatype)fdt2c((*((int *)ddt))),
		      (*((int *)source)),(*((int *)tag)),
		      (MPI_Comm)(*((int *)comm)),
		      (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Irsend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_irsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Irsend((void *)ToPtr(buf),(*((int *)cnt)),
		       (MPI_Datatype)fdt2c((*((int *)ddt))),
		       (*((int *)dest)),(*((int *)tag)),
		       (MPI_Comm)(*((int *)comm)),
		       (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Isend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_isend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Isend((void *)ToPtr(buf),(*((int *)cnt)),
		      (MPI_Datatype)fdt2c((*((int *)ddt))),
		      (*((int *)dest)),(*((int *)tag)),
		      (MPI_Comm)(*((int *)comm)),
		      (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Issend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_issend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Issend((void *)ToPtr(buf),(*((int *)cnt)),
		       (MPI_Datatype)fdt2c((*((int *)ddt))),
		       (*((int *)dest)),(*((int *)tag)),
		       (MPI_Comm)(*((int *)comm)),
		       (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Recv_init(void * buf,int cnt,MPI_Datatype ddt,int source,
   int tag,MPI_Comm comm,MPI_Request * request) */
void mpi_recv_init_(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr)
{
  *__ierr = MPI_Recv_init((void *)ToPtr(buf),(*((int *)cnt)),
			  (MPI_Datatype)fdt2c((*((int *)ddt))),
			  (*((int *)source)),(*((int *)tag)),
			  (MPI_Comm)(*((int *)comm)),
			  (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Request_free(MPI_Request * request) */
void mpi_request_free_(int * request,int * __ierr)
{
  *__ierr = MPI_Request_free((MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Rsend_init(void * buf,int cnt,MPI_Datatype ddt,int dest,
   int tag,MPI_Comm comm,MPI_Request * request) */
void mpi_rsend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr)
{
  *__ierr = MPI_Rsend_init((void *)ToPtr(buf),(*((int *)cnt)),
			   (MPI_Datatype)fdt2c((*((int *)ddt))),
			   (*((int *)dest)),(*((int *)tag)),
			   (MPI_Comm)(*((int *)comm)),
			   (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Send_init(void *buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm,MPI_Request * request) */
void mpi_send_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr)
{
  *__ierr = MPI_Send_init((void *)ToPtr(buf),(*((int *)cnt)),
			  (MPI_Datatype)fdt2c((*((int *)ddt))),
			  (*((int *)dest)),(*((int *)tag)),
			  (MPI_Comm)(*((int *)comm)),
			  (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Ssend_init(void * buf,int cnt,MPI_Datatype ddt,int dest,
   int tag,MPI_Comm comm,MPI_Request * request) */
void mpi_ssend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr)
{
  *__ierr = MPI_Ssend_init((void *)ToPtr(buf),(*((int *)cnt)),
			   (MPI_Datatype)fdt2c((*((int *)ddt))),
			   (*((int *)dest)),(*((int *)tag)),
			   (MPI_Comm)(*((int *)comm)),
			   (MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Startall(int cnt,MPI_Request * array_of_requests) */
void mpi_startall_(int * cnt,int * array_of_requests,int * __ierr)
{
  *__ierr = MPI_Startall((*((int *)cnt)),
			 (MPI_Request *)ToPtr(array_of_requests));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Start(MPI_Request * request) */
void mpi_start_(int * request,int * __ierr)
{
  *__ierr = MPI_Start((MPI_Request *)ToPtr(request));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Testall(int cnt,MPI_Request * array_of_requests,int * flag,
   MPI_Status * array_of_statuses) */
void mpi_testall_(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr)
{
  *__ierr = MPI_Testall((*((int *)cnt)),
			(MPI_Request *)ToPtr(array_of_requests),
			(int *)ToPtr(flag),
			(MPI_Status *)ToPtr(array_of_statuses));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Testany(int cnt,MPI_Request * array_of_requests,int * index,
   int * flag,MPI_Status * status) */
void mpi_testany_(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr)
{
  *__ierr = MPI_Testany((*((int *)cnt)),
			(MPI_Request *)ToPtr(array_of_requests),
			(int *)ToPtr(index),(int *)ToPtr(flag),
			(MPI_Status *)ToPtr(status));
  if ( *index != MPI_UNDEFINED )
    *index += 1;

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Test_cancelled(MPI_Status * status,int * flag) */
void mpi_test_cancelled_(int * status,int * flag,int * __ierr)
{
  *__ierr = MPI_Test_cancelled((MPI_Status *)ToPtr(status),
			       (int *)ToPtr(flag));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Test(MPI_Request * request,int * flag,MPI_Status * status) */
void mpi_test_(int * request,int * flag,int * status,int * __ierr)
{
  *__ierr = MPI_Test((MPI_Request *)ToPtr(request),(int *)ToPtr(flag),
		     (MPI_Status *)ToPtr(status));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Testsome(int incount,MPI_Request * array_of_requests,
   int *outcount,int *array_of_indices,MPI_Status *array_of_statuses)*/
void mpi_testsome_(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr)
{
  int i;

  *__ierr = MPI_Testsome((*((int *)incount)),
			 (MPI_Request *)ToPtr(array_of_requests),
			 (int *)ToPtr(outcount),
			 (int *)ToPtr(array_of_indices),
			 (MPI_Status *)ToPtr(array_of_statuses));
  if ( *outcount != MPI_UNDEFINED )
    for ( i = 0; i < *outcount; i++ )
      array_of_indices[i] += 1;

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Waitall(int cnt,MPI_Request * array_of_requests,
   MPI_Status * array_of_statuses) */
void mpi_waitall_(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr)
{
  *__ierr = MPI_Waitall((*((int *)cnt)),
			(MPI_Request *)ToPtr(array_of_requests),
			(MPI_Status *)ToPtr(array_of_statuses));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Waitany(int cnt,MPI_Request * array_of_requests,int * index,
   MPI_Status * status) */
void mpi_waitany_(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr)
{
  *__ierr = MPI_Waitany((*((int *)cnt)),
			(MPI_Request *)ToPtr(array_of_requests),
			(int *)ToPtr(index),(MPI_Status *)ToPtr(status));
  *index += 1;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Wait(MPI_Request * request,MPI_Status * status) */
void mpi_wait_(int * request,int * status,int * __ierr)
{
  *__ierr = MPI_Wait((MPI_Request *)ToPtr(request),
		     (MPI_Status *)ToPtr(status));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Waitsome(int incount,MPI_Request * array_of_requests,
   int *outcount,int *array_of_indices,MPI_Status *array_of_statuses)*/
void mpi_waitsome_(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr)
{
  int i;

  *__ierr = MPI_Waitsome((*((int *)incount)),
			 (MPI_Request *)ToPtr(array_of_requests),
			 (int *)ToPtr(outcount),
			 (int *)ToPtr(array_of_indices),
			 (MPI_Status *)ToPtr(array_of_statuses));
  if ( *outcount != MPI_UNDEFINED )
    for ( i = 0; i < *outcount ; i++)
      array_of_indices[i] += 1;
}


