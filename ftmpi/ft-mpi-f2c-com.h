/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_COM
#define _FT_MPI_H_F2C_COM

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define HDR     "FTMP:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak  MPI_COMM_RANK               = mpi_comm_rank_
#    pragma weak  PMPI_COMM_RANK              = mpi_comm_rank_ 
#    pragma weak  MPI_COMM_SIZE               = mpi_comm_size_
#    pragma weak  PMPI_COMM_SIZE              = mpi_comm_size_ 
#    pragma weak  MPI_COMM_COMPARE            = mpi_comm_compare_
#    pragma weak  PMPI_COMM_COMPARE           = mpi_comm_compare_
#    pragma weak  MPI_COMM_GROUP              = mpi_comm_group_
#    pragma weak  PMPI_COMM_GROUP             = mpi_comm_group_ 
#    pragma weak  MPI_COMM_DUP                = mpi_comm_dup_
#    pragma weak  PMPI_COMM_DUP               = mpi_comm_dup_ 
#    pragma weak  MPI_COMM_FREE               = mpi_comm_free_
#    pragma weak  PMPI_COMM_FREE              = mpi_comm_free_ 
#    pragma weak  MPI_COMM_CREATE             = mpi_comm_create_
#    pragma weak  PMPI_COMM_CREATE            = mpi_comm_create_ 
#    pragma weak  MPI_COMM_REMOTE_GROUP       = mpi_comm_remote_group_  
#    pragma weak  PMPI_COMM_REMOTE_GROUP      = mpi_comm_remote_group_
#    pragma weak  MPI_COMM_REMOTE_SIZE        = mpi_comm_remote_size_	  
#    pragma weak  PMPI_COMM_REMOTE_SIZE       = mpi_comm_remote_size_
#    pragma weak  MPI_COMM_SPLIT              = mpi_comm_split_	  
#    pragma weak  PMPI_COMM_SPLIT             = mpi_comm_split_
#    pragma weak  MPI_COMM_TEST_INTER         = mpi_comm_test_inter_	  
#    pragma weak  PMPI_COMM_TEST_INTER        = mpi_comm_test_inter_
#    pragma weak  MPI_COMM_CREATE_KEYVAL      = mpi_comm_create_keyval_
#    pragma weak  PMPI_COMM_CREATE_KEYVAL     = mpi_comm_create_keyval_
#    pragma weak  MPI_COMM_FREE_KEYVAL        = mpi_comm_free_keyval_
#    pragma weak  PMPI_COMM_FREE_KEYVAL       = mpi_comm_free_keyval_
#    pragma weak  MPI_COMM_GET_ATTR           = mpi_comm_get_attr_
#    pragma weak  PMPI_COMM_GET_ATTR          = mpi_comm_get_attr_
#    pragma weak  MPI_COMM_SET_ATTR           = mpi_comm_set_attr_
#    pragma weak  PMPI_COMM_SET_ATTR          = mpi_comm_set_attr_
#    pragma weak  MPI_COMM_DELETE_ATTR        = mpi_comm_delete_attr_
#    pragma weak  PMPI_COMM_DELETE_ATTR       = mpi_comm_delete_attr_
#    pragma weak  MPI_COMM_CREATE_ERRHANDLER  = mpi_comm_create_errhandler_
#    pragma weak  PMPI_COMM_CREATE_ERRHANDLER = mpi_comm_create_errhandler_
#    pragma weak  MPI_COMM_GET_ERRHANDLER     = mpi_comm_get_errhandler_
#    pragma weak  PMPI_COMM_GET_ERRHANDLER    = mpi_comm_get_errhandler_
#    pragma weak  MPI_COMM_SET_ERRHANDLER     = mpi_comm_set_errhandler_
#    pragma weak  PMPI_COMM_SET_ERRHANDLER    = mpi_comm_set_errhandler_
#    pragma weak  MPI_COMM_CALL_ERRHANDLER    = mpi_comm_call_errhandler_
#    pragma weak  PMPI_COMM_CALL_ERRHANDLER   = mpi_comm_call_errhandler_
#    pragma weak  MPI_INTERCOMM_CREATE        = mpi_intercomm_create_	
#    pragma weak  PMPI_INTERCOMM_CREATE       = mpi_intercomm_create_
#    pragma weak  MPI_INTERCOMM_MERGE         = mpi_intercomm_merge_	
#    pragma weak  PMPI_INTERCOMM_MERGE        = mpi_intercomm_merge_



#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak  mpi_comm_rank__               = mpi_comm_rank_
#    pragma weak  pmpi_comm_rank__              = mpi_comm_rank_
#    pragma weak  mpi_comm_size__               = mpi_comm_size_
#    pragma weak  pmpi_comm_size__              = mpi_comm_size_
#    pragma weak  mpi_comm_compare__            = mpi_comm_compare_
#    pragma weak  pmpi_comm_compare__           = mpi_comm_compare_
#    pragma weak  mpi_comm_group__              = mpi_comm_group_
#    pragma weak  pmpi_comm_group__             = mpi_comm_group_
#    pragma weak  mpi_comm_dup__                = mpi_comm_dup_
#    pragma weak  pmpi_comm_dup__               = mpi_comm_dup_
#    pragma weak  mpi_comm_create__             = mpi_comm_create_
#    pragma weak  pmpi_comm_create__            = mpi_comm_create_
#    pragma weak  mpi_comm_free__               = mpi_comm_free_
#    pragma weak  pmpi_comm_free__              = mpi_comm_free_
#    pragma weak  mpi_comm_remote_group__       = mpi_comm_remote_group_
#    pragma weak  pmpi_comm_remote_group__      = mpi_comm_remote_group_
#    pragma weak  mpi_comm_remote_size__        = mpi_comm_remote_size_
#    pragma weak  pmpi_comm_remote_size__       = mpi_comm_remote_size_
#    pragma weak  mpi_comm_split__              = mpi_comm_split_
#    pragma weak  pmpi_comm_split__             = mpi_comm_split_
#    pragma weak  mpi_comm_test_inter__         = mpi_comm_test_inter_
#    pragma weak  pmpi_comm_test_inter__        = mpi_comm_test_inter_
#    pragma weak  mpi_comm_create_keyval__      = mpi_comm_create_keyval_
#    pragma weak  pmpi_comm_create_keyval__     = mpi_comm_create_keyval_
#    pragma weak  mpi_comm_free_keyval__        = mpi_comm_free_keyval_
#    pragma weak  pmpi_comm_free_keyval__       = mpi_comm_free_keyval_
#    pragma weak  mpi_comm_get_attr__           = mpi_comm_get_attr_
#    pragma weak  pmpi_comm_get_attr__          = mpi_comm_get_attr_
#    pragma weak  mpi_comm_set_attr__           = mpi_comm_set_attr_
#    pragma weak  pmpi_comm_set_attr__          = mpi_comm_set_attr_
#    pragma weak  mpi_comm_delete_attr__        = mpi_comm_delete_attr_
#    pragma weak  pmpi_comm_delete_attr__       = mpi_comm_delete_attr_
#    pragma weak  mpi_comm_create_errhandler__  = mpi_comm_create_errhandler_
#    pragma weak  pmpi_comm_create_errhandler__ = mpi_comm_create_errhandler_
#    pragma weak  mpi_comm_get_errhandler__     = mpi_comm_get_errhandler_
#    pragma weak  pmpi_comm_get_errhandler__    = mpi_comm_get_errhandler_
#    pragma weak  mpi_comm_set_errhandler__     = mpi_comm_set_errhandler_
#    pragma weak  pmpi_comm_set_errhandler__    = mpi_comm_set_errhandler_
#    pragma weak  mpi_comm_call_errhandler__    = mpi_comm_call_errhandler_
#    pragma weak  pmpi_comm_call_errhandler__   = mpi_comm_call_errhandler_
#    pragma weak  mpi_intercomm_create__        = mpi_intercomm_create_
#    pragma weak  pmpi_intercomm_create__       = mpi_intercomm_create_
#    pragma weak  mpi_intercomm_merge__         = mpi_intercomm_merge_
#    pragma weak  pmpi_intercomm_merge__        = mpi_intercomm_merge_

#  elif defined(FORTRANNOUNDERSCORE)

#    pragma weak  mpi_comm_rank               = mpi_comm_rank_
#    pragma weak  pmpi_comm_rank              = mpi_comm_rank_
#    pragma weak  mpi_comm_size               = mpi_comm_size_
#    pragma weak  pmpi_comm_size              = mpi_comm_size_
#    pragma weak  mpi_comm_compare            = mpi_comm_compare_
#    pragma weak  pmpi_comm_compare           = mpi_comm_compare_
#    pragma weak  mpi_comm_group              = mpi_comm_group_
#    pragma weak  pmpi_comm_group             = mpi_comm_group_
#    pragma weak  mpi_comm_dup                = mpi_comm_dup_
#    pragma weak  pmpi_comm_dup               = mpi_comm_dup_
#    pragma weak  mpi_comm_create             = mpi_comm_create_
#    pragma weak  pmpi_comm_create            = mpi_comm_create_
#    pragma weak  mpi_comm_free               = mpi_comm_free_
#    pragma weak  pmpi_comm_free              = mpi_comm_free_
#    pragma weak  mpi_comm_remote_group       = mpi_comm_remote_group_
#    pragma weak  pmpi_comm_remote_group      = mpi_comm_remote_group_
#    pragma weak  mpi_comm_remote_size        = mpi_comm_remote_size_
#    pragma weak  pmpi_comm_remote_size       = mpi_comm_remote_size_
#    pragma weak  mpi_comm_split              = mpi_comm_split_
#    pragma weak  pmpi_comm_split             = mpi_comm_split_
#    pragma weak  mpi_comm_test_inter         = mpi_comm_test_inter_
#    pragma weak  pmpi_comm_test_inter        = mpi_comm_test_inter_
#    pragma weak  mpi_comm_create_keyval      = mpi_comm_create_keyval_
#    pragma weak  pmpi_comm_create_keyval     = mpi_comm_create_keyval_
#    pragma weak  mpi_comm_free_keyval        = mpi_comm_free_keyval_
#    pragma weak  pmpi_comm_free_keyval       = mpi_comm_free_keyval_
#    pragma weak  mpi_comm_get_attr           = mpi_comm_get_attr_
#    pragma weak  pmpi_comm_get_attr          = mpi_comm_get_attr_
#    pragma weak  mpi_comm_set_attr           = mpi_comm_set_attr_
#    pragma weak  pmpi_comm_set_attr          = mpi_comm_set_attr_
#    pragma weak  mpi_comm_delete_attr        = mpi_comm_delete_attr_
#    pragma weak  pmpi_comm_delete_attr       = mpi_comm_delete_attr_
#    pragma weak  mpi_comm_create_errhandler  = mpi_comm_create_errhandler_
#    pragma weak  pmpi_comm_create_errhandler = mpi_comm_create_errhandler_
#    pragma weak  mpi_comm_get_errhandler     = mpi_comm_get_errhandler_
#    pragma weak  pmpi_comm_get_errhandler    = mpi_comm_get_errhandler_
#    pragma weak  mpi_comm_set_errhandler     = mpi_comm_set_errhandler_
#    pragma weak  pmpi_comm_set_errhandler    = mpi_comm_set_errhandler_
#    pragma weak  mpi_comm_call_errhandler    = mpi_comm_call_errhandler_
#    pragma weak  pmpi_comm_call_errhandler   = mpi_comm_call_errhandler_
#    pragma weak  mpi_intercomm_create        = mpi_intercomm_create_
#    pragma weak  pmpi_intercomm_create       = mpi_intercomm_create_
#    pragma weak  mpi_intercomm_merge         = mpi_intercomm_merge_
#    pragma weak  pmpi_intercomm_merge        = mpi_intercomm_merge_

#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak  pmpi_comm_rank_              = mpi_comm_rank_
#    pragma weak  pmpi_comm_size_              = mpi_comm_size_
#    pragma weak  pmpi_comm_compare_           = mpi_comm_compare_
#    pragma weak  pmpi_comm_group_             = mpi_comm_group_
#    pragma weak  pmpi_comm_dup_               = mpi_comm_dup_
#    pragma weak  pmpi_comm_create_            = mpi_comm_create_
#    pragma weak  pmpi_comm_free_              = mpi_comm_free_
#    pragma weak  pmpi_comm_remote_group_      = mpi_comm_remote_group_
#    pragma weak  pmpi_comm_remote_size_       = mpi_comm_remote_size_
#    pragma weak  pmpi_comm_split_             = mpi_comm_split_
#    pragma weak  pmpi_comm_test_inter_        = mpi_comm_test_inter_
#    pragma weak  pmpi_comm_create_keyval_     = mpi_comm_create_keyval_
#    pragma weak  pmpi_comm_free_keyval_       = mpi_comm_free_keyval_
#    pragma weak  pmpi_comm_get_attr_          = mpi_comm_get_attr_
#    pragma weak  pmpi_comm_set_attr_          = mpi_comm_set_attr_
#    pragma weak  pmpi_comm_delete_attr_       = mpi_comm_delete_attr_
#    pragma weak  pmpi_comm_create_errhandler_ = mpi_comm_create_errhandler_
#    pragma weak  pmpi_comm_get_errhandler_    = mpi_comm_get_errhandler_
#    pragma weak  pmpi_comm_set_errhandler_    = mpi_comm_set_errhandler_
#    pragma weak  pmpi_comm_call_errhandler_   = mpi_comm_call_errhandler_
#    pragma weak  pmpi_intercomm_create_       = mpi_intercomm_create_
#    pragma weak  pmpi_intercomm_merge_        = mpi_intercomm_merge_

#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define mpi_comm_rank_              PMPI_COMM_RANK 
#      define mpi_comm_size_              PMPI_COMM_SIZE 
#      define mpi_comm_compare_           PMPI_COMM_COMPARE
#      define mpi_comm_group_             PMPI_COMM_GROUP 
#      define mpi_comm_dup_               PMPI_COMM_DUP 
#      define mpi_comm_free_              PMPI_COMM_FREE 
#      define mpi_comm_create_            PMPI_COMM_CREATE 
#      define mpi_comm_remote_group_      PMPI_COMM_REMOTE_GROUP   
#      define mpi_comm_remote_size_       PMPI_COMM_REMOTE_SIZE    
#      define mpi_comm_split_             PMPI_COMM_SPLIT          
#      define mpi_comm_test_inter_        PMPI_COMM_TEST_INTER     
#      define mpi_comm_create_keyval_     PMPI_COMM_CREATE_KEYVAL
#      define mpi_comm_free_keyval_       PMPI_COMM_FREE_KEYVAL
#      define mpi_comm_set_attr_          PMPI_COMM_SET_ATTR
#      define mpi_comm_get_attr_          PMPI_COMM_GET_ATTR
#      define mpi_comm_delete_attr_       PMPI_COMM_DELETE_ATTR
#      define mpi_comm_create_errhandler_ PMPI_COMM_CREATE_ERRHANDLER
#      define mpi_comm_get_errhandler_    PMPI_COMM_GET_ERRHANDLER
#      define mpi_comm_set_errhandler_    PMPI_COMM_SET_ERRHANDLER
#      define mpi_comm_call_errhanderl_   PMPI_COMM_CALL_ERRHANDLER
#      define mpi_intercomm_create_       PMPI_INTERCOMM_CREATE   
#      define mpi_intercomm_merge_        PMPI_INTERCOMM_MERGE   

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_comm_rank_              pmpi_comm_rank__ 
#      define  mpi_comm_size_              pmpi_comm_size__ 
#      define  mpi_comm_compare_           pmpi_comm_compare__ 
#      define  mpi_comm_group_             pmpi_comm_group__ 
#      define  mpi_comm_dup_               pmpi_comm_dup__ 
#      define  mpi_comm_create_            pmpi_comm_create__ 
#      define  mpi_comm_free_              pmpi_comm_free__ 
#      define  mpi_comm_remote_group_      pmpi_comm_remote_group__ 
#      define  mpi_comm_remote_size_       pmpi_comm_remote_size__ 
#      define  mpi_comm_split_             pmpi_comm_split__ 
#      define  mpi_comm_test_inter_        pmpi_comm_test_inter__ 
#      define  mpi_comm_create_keyval_     pmpi_comm_create_keyval__
#      define  mpi_comm_free_keyval_       pmpi_comm_free_keyval__
#      define  mpi_comm_set_attr_          pmpi_comm_set_attr__
#      define  mpi_comm_get_attr_          pmpi_comm_get_attr__
#      define  mpi_comm_delete_attr_       pmpi_comm_delete_attr__
#      define  mpi_comm_create_errhandler_ pmpi_comm_create_errhandler__
#      define  mpi_comm_get_errhandler_    pmpi_comm_get_errhandler__
#      define  mpi_comm_set_errhandler_    pmpi_comm_set_errhandler__
#      define  mpi_comm_call_errhanderl_   pmpi_comm_call_errhandler__
#      define  mpi_intercomm_create_       pmpi_intercomm_create__ 
#      define  mpi_intercomm_merge_        pmpi_intercomm_merge__ 

#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_comm_rank_              pmpi_comm_rank 
#      define  mpi_comm_size_              pmpi_comm_size 
#      define  mpi_comm_compare_           pmpi_comm_compare 
#      define  mpi_comm_group_             pmpi_comm_group 
#      define  mpi_comm_dup_               pmpi_comm_dup 
#      define  mpi_comm_create_            pmpi_comm_create 
#      define  mpi_comm_free_              pmpi_comm_free 
#      define  mpi_comm_remote_group_      pmpi_comm_remote_group 
#      define  mpi_comm_remote_size_       pmpi_comm_remote_size 
#      define  mpi_comm_split_             pmpi_comm_split 
#      define  mpi_comm_test_inter_        pmpi_comm_test_inter 
#      define  mpi_comm_create_keyval_     pmpi_comm_create_keyval
#      define  mpi_comm_free_keyval_       pmpi_comm_free_keyval
#      define  mpi_comm_set_attr_          pmpi_comm_set_attr
#      define  mpi_comm_get_attr_          pmpi_comm_get_attr
#      define  mpi_comm_delete_attr_       pmpi_comm_delete_attr
#      define  mpi_comm_create_errhandler_ pmpi_comm_create_errhandler
#      define  mpi_comm_get_errhandler_    pmpi_comm_get_errhandler
#      define  mpi_comm_set_errhandler_    pmpi_comm_set_errhandler
#      define  mpi_comm_call_errhanderl_   pmpi_comm_call_errhandler
#      define  mpi_intercomm_create_       pmpi_intercomm_create 
#      define  mpi_intercomm_merge_        pmpi_intercomm_merge 


#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define  mpi_comm_rank_              pmpi_comm_rank_ 
#      define  mpi_comm_size_              pmpi_comm_size_ 
#      define  mpi_comm_compare_           pmpi_comm_compare_ 
#      define  mpi_comm_group_             pmpi_comm_group_ 
#      define  mpi_comm_dup_               pmpi_comm_dup_ 
#      define  mpi_comm_create_            pmpi_comm_create_ 
#      define  mpi_comm_free_              pmpi_comm_free_ 
#      define  mpi_comm_remote_group_      pmpi_comm_remote_group_ 
#      define  mpi_comm_remote_size_       pmpi_comm_remote_size_ 
#      define  mpi_comm_split_             pmpi_comm_split_ 
#      define  mpi_comm_test_inter_        pmpi_comm_test_inter_ 
#      define  mpi_comm_create_keyval_     pmpi_comm_create_keyval_
#      define  mpi_comm_free_keyval_       pmpi_comm_free_keyval_
#      define  mpi_comm_set_attr_          pmpi_comm_set_attr_
#      define  mpi_comm_get_attr_          pmpi_comm_get_attr_
#      define  mpi_comm_delete_attr_       pmpi_comm_delete_attr_
#      define  mpi_comm_create_errhandler_ pmpi_comm_create_errhandler_
#      define  mpi_comm_get_errhandler_    pmpi_comm_get_errhandler_
#      define  mpi_comm_set_errhandler_    pmpi_comm_set_errhandler_
#      define  mpi_comm_call_errhanderl_   pmpi_comm_call_errhandler_
#      define  mpi_intercomm_create_       pmpi_intercomm_create_ 
#      define  mpi_intercomm_merge_        pmpi_intercomm_merge_ 
#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS
 
#      define mpi_comm_rank_              MPI_COMM_RANK 
#      define mpi_comm_size_              MPI_COMM_SIZE 
#      define mpi_comm_compare_           MPI_COMM_COMPARE
#      define mpi_comm_group_             MPI_COMM_GROUP 
#      define mpi_comm_dup_               MPI_COMM_DUP 
#      define mpi_comm_free_              MPI_COMM_FREE 
#      define mpi_comm_create_            MPI_COMM_CREATE 
#      define mpi_comm_remote_group_      MPI_COMM_REMOTE_GROUP   
#      define mpi_comm_remote_size_       MPI_COMM_REMOTE_SIZE    
#      define mpi_comm_split_             MPI_COMM_SPLIT          
#      define mpi_comm_test_inter_        MPI_COMM_TEST_INTER     
#      define mpi_comm_create_keyval_     MPI_COMM_CREATE_KEYVAL
#      define mpi_comm_free_keyval_       MPI_COMM_FREE_KEYVAL
#      define mpi_comm_set_attr_          MPI_COMM_SET_ATTR
#      define mpi_comm_get_attr_          MPI_COMM_GET_ATTR
#      define mpi_comm_delete_attr_       MPI_COMM_DELETE_ATTR
#      define mpi_comm_create_errhandler_ MPI_COMM_CREATE_ERRHANDLER
#      define mpi_comm_get_errhandler_    MPI_COMM_GET_ERRHANDLER
#      define mpi_comm_set_errhandler_    MPI_COMM_SET_ERRHANDLER
#      define mpi_comm_call_errhanderl_   MPI_COMM_CALL_ERRHANDLER
#      define mpi_intercomm_create_       MPI_INTERCOMM_CREATE   
#      define mpi_intercomm_merge_        MPI_INTERCOMM_MERGE   


#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_comm_rank_              mpi_comm_rank__ 
#      define  mpi_comm_size_              mpi_comm_size__ 
#      define  mpi_comm_compare_           mpi_comm_compare__ 
#      define  mpi_comm_group_             mpi_comm_group__ 
#      define  mpi_comm_dup_               mpi_comm_dup__ 
#      define  mpi_comm_create_            mpi_comm_create__ 
#      define  mpi_comm_free_              mpi_comm_free__ 
#      define  mpi_comm_remote_group_      mpi_comm_remote_group__ 
#      define  mpi_comm_remote_size_       mpi_comm_remote_size__ 
#      define  mpi_comm_split_             mpi_comm_split__ 
#      define  mpi_comm_test_inter_        mpi_comm_test_inter__ 
#      define  mpi_comm_create_keyval_     mpi_comm_create_keyval__
#      define  mpi_comm_free_keyval_       mpi_comm_free_keyval__
#      define  mpi_comm_set_attr_          mpi_comm_set_attr__
#      define  mpi_comm_get_attr_          mpi_comm_get_attr__
#      define  mpi_comm_delete_attr_       mpi_comm_delete_attr__
#      define  mpi_comm_create_errhandler_ mpi_comm_create_errhandler__
#      define  mpi_comm_get_errhandler_    mpi_comm_get_errhandler__
#      define  mpi_comm_set_errhandler_    mpi_comm_set_errhandler__
#      define  mpi_comm_call_errhanderl_   mpi_comm_call_errhandler__
#      define  mpi_intercomm_create_       mpi_intercomm_create__ 
#      define  mpi_intercomm_merge_        mpi_intercomm_merge__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_comm_rank_              mpi_comm_rank 
#      define  mpi_comm_size_              mpi_comm_size 
#      define  mpi_comm_compare_           mpi_comm_compare 
#      define  mpi_comm_group_             mpi_comm_group 
#      define  mpi_comm_dup_               mpi_comm_dup 
#      define  mpi_comm_create_            mpi_comm_create 
#      define  mpi_comm_free_              mpi_comm_free 
#      define  mpi_comm_remote_group_      mpi_comm_remote_group 
#      define  mpi_comm_remote_size_       mpi_comm_remote_size 
#      define  mpi_comm_split_             mpi_comm_split 
#      define  mpi_comm_test_inter_        mpi_comm_test_inter 
#      define  mpi_comm_create_keyval_     mpi_comm_create_keyval
#      define  mpi_comm_free_keyval_       mpi_comm_free_keyval
#      define  mpi_comm_set_attr_          mpi_comm_set_attr
#      define  mpi_comm_get_attr_          mpi_comm_get_attr
#      define  mpi_comm_delete_attr_       mpi_comm_delete_attr
#      define  mpi_comm_create_errhandler_ mpi_comm_create_errhandler
#      define  mpi_comm_get_errhandler_    mpi_comm_get_errhandler
#      define  mpi_comm_set_errhandler_    mpi_comm_set_errhandler
#      define  mpi_comm_call_errhanderl_   mpi_comm_call_errhandler
#      define  mpi_intercomm_create_       mpi_intercomm_create 
#      define  mpi_intercomm_merge_        mpi_intercomm_merge 


#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */
#endif
