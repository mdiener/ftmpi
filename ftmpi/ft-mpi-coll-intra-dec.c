/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors:	
	  Edgar Gabriel <gabriel@cs.utk.edu>
          Graham E Fagg <fagg@cs.utk.edu>
	  Antonin Bukovsky <tone@cs.utk.edu>
	  Jeremy Millar <millar@cs.utk.edu>
	  George Bosilca <bosilca@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-com.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-op.h"

/* these routines decide, which algorithm will be used in the
   collective operation. This decision is based on the following
   facts ( at least theoretically ):
   - topology of the communicator 
   - message length
   - datatype used ( e.g. some algorithms might not work with derived
     datatypes )
   - position of root ( optionally )

   The current implementation is however trivial. We always
   return the linear algorithm as the result and a segment size
   of 0 ( which means do not segment the message but send everything
   at once.

   EG. March 10, 2003 
*/

extern int gcommode;

int ftmpi_coll_intra_barrier_dec ( MPI_Comm comm )
{
  
  return ( BARRIER_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_bcast_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				int root,  int *segsize )
{
  int mode = BCAST_LINEAR;
  *segsize = 0;

#if defined(FTMPI_TUNED_COLLECTIVE)
  if ( gcommode != FT_MODE_BLANK )
    mode = BCAST_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */

  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_reduce_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				  int root, MPI_Op op, int *segsize )
{
  int commute;
  int mode = REDUCE_LINEAR;

  *segsize = 0 ;

#if defined(FTMPI_TUNED_COLLECTIVE)
  commute = _atb_op_get_commute(op);

  if ( gcommode != FT_MODE_BLANK )
    if ( commute )
      mode =  REDUCE_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */

  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_allreduce_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				     MPI_Op op, int *segsize )
{
  int commute;
  int mode = ALLREDUCE_LINEAR;
  *segsize = 0;

#if defined(FTMPI_TUNED_COLLECTIVE)
  commute = _atb_op_get_commute(op);

  if ( gcommode != FT_MODE_BLANK )
    if ( commute )
      mode =  ALLREDUCE_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */

  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_allgather_dec (MPI_Comm comm,int scnt,MPI_Datatype sddt,
				    int rcnt, MPI_Datatype rcddt,int *segsize)
{
  int mode = ALLGATHER_LINEAR;
  *segsize = 0;

#if defined(FTMPI_TUNED_COLLECTIVE)
  if ( gcommode != FT_MODE_BLANK)
    mode = ALLGATHER_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */

  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_allgatherv_dec (MPI_Comm comm,int scnt,MPI_Datatype sddt,
				     int *rcnts, MPI_Datatype rcddt,
				     int *segsize )
{
  int mode = ALLGATHERV_LINEAR;
  *segsize = 0;

#if defined(FTMPI_TUNED_COLLECTIVE)
  if ( gcommode != FT_MODE_BLANK )
    mode = ALLGATHERV_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */

  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_alltoall_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				   int rcnt, MPI_Datatype rcddt, int *segsize)
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( ALLTOALL_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_alltoallv_dec (MPI_Comm comm,int *scnt,MPI_Datatype sddt,
				   int *rcnts, MPI_Datatype rcddt,
				   int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0 ;
  return ( ALLTOALLV_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_gather_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				 int rcnt, MPI_Datatype rddt, int root,
				 int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( GATHER_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_gatherv_dec (MPI_Comm comm, int scnt, MPI_Datatype sddt,
				 int *rcnt, MPI_Datatype rddt, int root,
				 int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( GATHERV_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_reducescatter_dec ( MPI_Comm comm, int *counts, 
					 MPI_Datatype ddt, 
					 MPI_Op op, int *segsize )
{
  int commute;
  int mode = REDUCESCATTER_LINEAR;

  *segsize = 0;
#if defined(FTMPI_TUNED_COLLECTIVE)
  commute = _atb_op_get_commute(op);
  
  if ( mode != FT_MODE_BLANK )
    if ( commute )
      mode = REDUCESCATTER_BMTREE;
#endif  /* FTMPI_TUNED_COLLECTIVE */
  
  return ( mode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_scatter_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				  int rcnt, MPI_Datatype rddt, int root,
				  int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( SCATTER_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_scatterv_dec (MPI_Comm comm, int *scnt, MPI_Datatype sddt,
				  int rcnt, MPI_Datatype rddt, int root,
				  int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( SCATTERV_LINEAR );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_coll_intra_scan_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt,
			       int *segsize )
{
#if defined(FTMPI_TUNED_COLLECTIVE)
#endif  /* FTMPI_TUNED_COLLECTIVE */
  *segsize = 0;
  return ( SCAN_LINEAR );
}



