/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors:	
          Graham E Fagg <fagg@cs.utk.edu>
	  Antonin Bukovsky <tone@cs.utk.edu>
	  Edgar Gabriel <gabriel@cs.utk.edu>
	  Jeremy Millar <millar@cs.utk.edu>


  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

/*
  Collective communication calls: this file is the top-most layer
  of the routines which does basically the following things:
  - check calling arguments
  - check for inter- or intra-communiucator
  - call the decision-routines for each function
  - call the algorithm chosen by the decision layer
    using the segment size chosen by this layer
*/


#include "ft-mpi-lib.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-com.h"
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-op.h"
#include "ft-mpi-ddt-sys.h"

/* Definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Allgather            = PMPI_Allgather
#    pragma weak  MPI_Allgatherv           = PMPI_Allgatherv
#    pragma weak  MPI_Alltoall             = PMPI_Alltoall
#    pragma weak  MPI_Alltoallv            = PMPI_Alltoallv
#    pragma weak  MPI_Allreduce            = PMPI_Allreduce
#    pragma weak  MPI_Barrier              = PMPI_Barrier
#    pragma weak  MPI_Bcast                = PMPI_Bcast
#    pragma weak  MPI_Gather               = PMPI_Gather
#    pragma weak  MPI_Gatherv              = PMPI_Gatherv
#    pragma weak  MPI_Reduce               = PMPI_Reduce
#    pragma weak  MPI_Reduce_scatter       = PMPI_Reduce_scatter
#    pragma weak  MPI_Scan                 = PMPI_Scan
#    pragma weak  MPI_Scatter              = PMPI_Scatter
#    pragma weak  MPI_Scatterv             = PMPI_Scatterv

#endif

#    define  MPI_Allgather             PMPI_Allgather
#    define  MPI_Allgatherv            PMPI_Allgatherv
#    define  MPI_Alltoall              PMPI_Alltoall
#    define  MPI_Alltoallv             PMPI_Alltoallv
#    define  MPI_Allreduce             PMPI_Allreduce
#    define  MPI_Barrier               PMPI_Barrier
#    define  MPI_Bcast                 PMPI_Bcast
#    define  MPI_Gather                PMPI_Gather
#    define  MPI_Gatherv               PMPI_Gatherv
#    define  MPI_Reduce                PMPI_Reduce
#    define  MPI_Reduce_scatter        PMPI_Reduce_scatter
#    define  MPI_Scan                  PMPI_Scan
#    define  MPI_Scatter               PMPI_Scatter
#    define  MPI_Scatterv              PMPI_Scatterv


/* Order */

/* 
MPI_Barrier
MPI_Bcast
MPI_Reduce
MPI_Allreduce
MPI_Allgather
MPI_Allgatherv
MPI_Alltoall
MPI_Alltoallv
MPI_Gather
MPI_Gatherv
MPI_Scatter
MPI_Scatterv
MPI_Scan
*/


int MPI_Barrier( MPI_Comm comm )
{
  int inter;
  int alg;
  int ret;
  MPI_Comm shadow;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_barrier_dec ( comm );
      
      switch ( alg )
	{
	case ( BARRIER_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_barrier_linear ( shadow);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Bcast ( void* buffer, int count, MPI_Datatype datatype, 
		  int root, MPI_Comm comm )
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size;
  MPI_Comm shadow;

  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );

  CHECK_DDT(comm, datatype, 1);

  if ( count < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);
  /*  if ( buffer== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_bcast_dec ( comm, count, datatype, root,
					&segsize );
      
      switch ( alg )
	{
	case ( BCAST_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_bcast_linear ( buffer, count, datatype,
						 root, shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	case ( BCAST_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_bcast_bmtree ( buffer, count, datatype,
						  root, shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Reduce(void * sendbuf,void * recvbuf,int count,
	       MPI_Datatype datatype, MPI_Op op,int root,
	       MPI_Comm comm)
{
  int ret, rc;
  int alg;
  int segsize;
  int inter;
  int size;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );

  CHECK_DDT(comm, datatype, 1);

  rc = _atb_op_ok(op);
  if (rc != MPI_SUCCESS) RETURNERR(comm, MPI_ERR_OP);

  if ( count < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_reduce_dec ( comm, count, datatype, root,
					  op, &segsize );
      
      switch ( alg )
	{
	case ( REDUCE_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_reduce_linear ( sendbuf, recvbuf,
						  count, datatype, op,
						  root, shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	case ( REDUCE_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_reduce_bmtree ( sendbuf, recvbuf,
						  count, datatype, op,
						  root, shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Allreduce(void *sendbuf,void *recvbuf,int count,
		  MPI_Datatype datatype, MPI_Op op,MPI_Comm comm)
{
  int ret, rc;
  int alg;
  int segsize;
  int inter;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT(comm, datatype, 1);

  if ( count < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  /*  if ( sendbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( recvbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  rc = _atb_op_ok(op);
  if (rc != MPI_SUCCESS) RETURNERR(comm, MPI_ERR_OP);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_allreduce_dec ( comm, count, datatype,
					     op, &segsize );
      
      switch ( alg )
	{
	case ( ALLREDUCE_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_allreduce_linear ( sendbuf, recvbuf,
						     count, datatype, op,
						     shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	case ( ALLREDUCE_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_allreduce_bmtree ( sendbuf, recvbuf,
						     count, datatype, op,
						     shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Allgather(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
		  int rcnt,MPI_Datatype rddt,MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  MPI_Comm shadow;
  
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);
  
  CHECK_DDT( comm, sddt, 1);
  CHECK_DDT( comm, rddt, 1);

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_allgather_dec ( comm, scnt, sddt,
					    rcnt, rddt, &segsize );
      switch ( alg )
	{
	case ( ALLGATHER_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_allgather_linear ( sbuf, scnt, sddt,
						     rbuf, rcnt, rddt,
						     shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	case ( ALLGATHER_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_allgather_bmtree ( sbuf, scnt, sddt,
						     rbuf, rcnt, rddt,
						     shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Allgatherv(void *sbuf,int scnt,MPI_Datatype sddt,
		   void *rbuf, int *rcnts, int *displs,
		   MPI_Datatype rddt, MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);
  

  CHECK_DDT( comm, sddt, 1);
  CHECK_DDT( comm, rddt, 1);

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( rcnts==NULL ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( displs==NULL ) RETURNERR ( comm, MPI_ERR_ARG );
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_allgatherv_dec ( comm, scnt, sddt,
					     rcnts, rddt, &segsize );
      switch ( alg )
	{
	case ( ALLGATHERV_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_allgatherv_linear ( sbuf, scnt, sddt,
						      rbuf, rcnts, displs,
						      rddt, shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	case ( ALLGATHERV_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_allgatherv_bmtree ( sbuf, scnt, sddt,
						      rbuf, rcnts, displs,
						      rddt, shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Alltoall(void* sbuf,int scnt,MPI_Datatype sddt,
		 void* rbuf,int rcnt,MPI_Datatype rddt,
		 MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT( comm, sddt, 1);
  CHECK_DDT( comm, rddt, 1);

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_alltoall_dec ( comm, scnt, sddt,
					   rcnt, rddt,
					   &segsize );
      switch ( alg )
	{
	case ( ALLTOALL_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_alltoall_linear ( sbuf, scnt, sddt,
						    rbuf, rcnt, rddt,
						    shadow, segsize);
	    if ( ret < 0 ) RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Alltoallv(void* sbuf,int *scnt,int *sdisp,MPI_Datatype sddt,
		  void* rbuf,int *rcnt,int *rdisp,MPI_Datatype rddt,
		  MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size, i;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT( comm, sddt, 1);
  CHECK_DDT( comm, rddt, 1);

  if ( scnt==NULL ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( rcnt==NULL ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( sdisp==NULL ) RETURNERR ( comm, MPI_ERR_ARG );
  if ( rdisp==NULL ) RETURNERR ( comm, MPI_ERR_ARG );
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */
  
  /* Just to pass the Intel test */
  size = ftmpi_com_size ( comm );
  for ( i = 0 ; i < size ; i ++ ) {
    if ( scnt[i] < 0 ) RETURNERR(comm, MPI_ERR_COUNT);
    if ( rcnt[i] < 0 ) RETURNERR(comm, MPI_ERR_COUNT);
  }

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_alltoallv_dec ( comm, scnt, sddt,
					     rcnt, rddt, &segsize );
      switch ( alg )
	{
	case ( ALLTOALLV_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_alltoallv_linear ( sbuf, scnt, sdisp, sddt,
						      rbuf, rcnt, rdisp, rddt,
						      shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Gather(void *sbuf, int scnt, MPI_Datatype sddt, void *rbuf, 
	       int rcnt, MPI_Datatype rddt, int root, MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size, rank;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT (comm, sddt, 1);
  size = ftmpi_com_size ( comm );
  rank = ftmpi_com_rank ( comm );

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );*/
  if ( rank == root )
    {
      /*      if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */
      
      CHECK_DDT( comm, rddt, 1);
      if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
      if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
    }

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_gather_dec ( comm, scnt, sddt,
					 rcnt, rddt, root,
					 &segsize );
      
      switch ( alg )
	{
	case ( GATHER_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_gather_linear ( sbuf, scnt, sddt,
						  rbuf, rcnt, rddt,
						  root, shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Gatherv(void *sbuf,int scnt, MPI_Datatype sddt,void *rbuf,
		int *rcnt, int *rdisp, MPI_Datatype rddt,int root,
		MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size, rank;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );
  rank = ftmpi_com_rank ( comm );

  CHECK_DDT( comm, sddt, 1);

  if ( scnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */
  if ( rank == root )
    {
      /*      if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */

      CHECK_DDT( comm, rddt, 1);

      if ( rcnt==NULL ) RETURNERR ( comm, MPI_ERR_COUNT );
      if ( rdisp==NULL ) RETURNERR ( comm, MPI_ERR_ARG );
    }

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_gatherv_dec ( comm, scnt, sddt,
					 rcnt, rddt, root,
					 &segsize );
      
      switch ( alg )
	{
	case ( GATHERV_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_gatherv_linear ( sbuf, scnt, sddt,
						   rbuf, rcnt, rdisp, rddt,
						   root, shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Reduce_scatter(void * sbuf,void * rbuf,int * counts,
		       MPI_Datatype ddt,MPI_Op op, MPI_Comm comm)
{
  int ret, rc;
  int alg;
  int segsize;
  int inter;
  int size, i;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT( comm, ddt, 1);

  size=ftmpi_com_size ( comm );
  for ( i = 0; i < size; i++ ) 
    if ( counts[i] < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );

  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  rc = _atb_op_ok(op);
  if (rc != MPI_SUCCESS) RETURNERR(comm, MPI_ERR_OP);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_reducescatter_dec ( comm, counts, ddt,	
						 op, &segsize );
      
      switch ( alg )
	{
	case ( REDUCESCATTER_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_reducescatter_linear ( sbuf, rbuf,
							 counts, ddt, op,
							 shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	case ( REDUCESCATTER_BMTREE ):
	  {
	    ret = ftmpi_coll_intra_reducescatter_bmtree ( sbuf, rbuf,
							 counts, ddt, op,
							 shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Scatter(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
		int rcnt,MPI_Datatype rddt,int root,MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size, rank;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );
  rank = ftmpi_com_rank ( comm );

  CHECK_DDT( comm, rddt, 1);

  if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);
  /*  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */
  if ( rank == root )
    {
      /*      if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */
      
      CHECK_DDT ( comm, sddt, 1);
      if ( scnt<0 ) RETURNERR ( comm, MPI_ERR_COUNT );
    }

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_scatter_dec ( comm, scnt, sddt,
					  rcnt, rddt, root,
					  &segsize );
      
      switch ( alg )
	{
	case ( SCATTER_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_scatter_linear ( sbuf, scnt, sddt,
						   rbuf, rcnt, rddt,
						   root, shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return(MPI_SUCCESS);

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Scatterv(void *sbuf,int * scnt,int * sdisp,MPI_Datatype sddt,
		 void *rbuf,int rcnt,MPI_Datatype rddt,int root,
		 MPI_Comm comm)
{
  int ret;
  int alg;
  int segsize;
  int inter;
  int size, rank;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );
  rank = ftmpi_com_rank ( comm );
  
  CHECK_DDT ( comm, rddt, 1);

  if ( rcnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  if ( (root < 0 ) || (root >= size ) ) RETURNERR (comm, MPI_ERR_ROOT);
  /*  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */
  if ( rank == root )
    {
      /*      if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER ); */

      CHECK_DDT ( comm, sddt, 1);

      if ( scnt==NULL ) RETURNERR ( comm, MPI_ERR_COUNT );
      if ( sdisp==NULL ) RETURNERR ( comm, MPI_ERR_ARG );
    }

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_scatterv_dec ( comm, scnt, sddt,
					    rcnt, rddt, root,
					    &segsize );
      
      switch ( alg )
	{
	case ( SCATTERV_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_scatterv_linear ( sbuf, scnt, sdisp, sddt,
						     rbuf, rcnt, rddt,
						     root, shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return(MPI_SUCCESS);

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Scan(void * sbuf,void * rbuf,int cnt,MPI_Datatype ddt,
	     MPI_Op op,MPI_Comm comm)
{

  int ret, rc;
  int alg;
  int segsize;
  int inter;
  MPI_Comm shadow;
  
  /* Check calling arguments */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  CHECK_DDT( comm, ddt, 1);

  if ( cnt < 0 ) RETURNERR ( comm, MPI_ERR_COUNT );
  /*  if ( sbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  if ( rbuf== NULL) RETURNERR ( comm, MPI_ERR_BUFFER );
  */

  rc = _atb_op_ok ( op );
  if ( rc != MPI_SUCCESS ) RETURNERR (comm, MPI_ERR_OP);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) RETURNERR ( comm, MPI_ERR_INTERN);

  inter = ftmpi_com_test_inter ( comm );
  if ( inter )
    {
      /* Extended collective operations from MPI-2 not supported
	 at the moment
      */
      RETURNERR ( comm, MPI_ERR_COMM );
    }
  else
    {
      alg = ftmpi_coll_intra_scan_dec ( comm, cnt, ddt,	
				       &segsize );
      
      switch ( alg )
	{
	case ( SCAN_LINEAR ):
	  {
	    ret = ftmpi_coll_intra_scan_linear ( sbuf, rbuf,
						cnt, ddt, op,
						shadow, segsize);
	    if ( ret < 0 )RETURNERR ( comm, ret  );
	    break;
	  }
	default:
	  RETURNERR ( comm, MPI_ERR_INTERN);
	}
    }
  
  return(MPI_SUCCESS);
}







