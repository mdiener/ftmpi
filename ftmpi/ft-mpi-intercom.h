/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:
                        Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_INTERCOM
#define _FT_MPI_H_INTERCOM

int ftmpi_com_test_inter ( MPI_Comm com );
MPI_Group ftmpi_com_get_remote_group ( MPI_Comm com );
int ftmpi_com_remote_size ( MPI_Comm com);
group_t *ftmpi_com_get_remote_group_ptr ( MPI_Comm com );
int ftmpi_com_get_lleader ( MPI_Comm com );
int ftmpi_com_get_lleader_gid ( MPI_Comm com );
int ftmpi_com_get_rleader_gid ( MPI_Comm com );
MPI_Comm ftmpi_com_get_local_comm ( MPI_Comm com );
int ftmpi_com_build_intercom ( MPI_Comm comm, MPI_Comm local_comm,
			       MPI_Group grp, int ll, int llg, 
			       int rl, int rlg);
group_t *ftmpi_com_exchange_groups ( MPI_Comm lcomm, MPI_Comm pcomm, 
				    int rleader, int tag);
int ftmpi_com_bcast_group ( group_t *grp_ptr, MPI_Comm comm, int root, int *rl,
			    int *rl_gid);
int ftmpi_com_exchange_high ( MPI_Comm comm, int high, int *r_high );
int ftmpi_com_copy_intercom ( MPI_Comm com, MPI_Comm newcom );

#endif /*  _FT_MPI_H_INTERCOM */

