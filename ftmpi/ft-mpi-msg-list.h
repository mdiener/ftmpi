
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Robert Manchek <pvm@msr.csm.ornl.gov>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	Originally part of PVMPI/MPI_Connect by Graham 
*/




#ifndef MSGLISTH
#define MSGLISTH

#include "ft-mpi-req-list.h"

/* Message list, used by receive handler routines. */
struct msg_list {
  struct msg_list *ml_link, *ml_rlink;  /* linked list stuff */
  int ml_senders_rank;                  /* senders rank used to update status */
  int ml_mpi_comm;						/* communicator used */
  int ml_mpi_tag;                        /* Actual Tag used in MPI Send */
  int ml_msg_type;                      /* Type of message */
  int ml_prot_type;                      /* Protocol used, read,sync, etc */
                                        /* inc only for compatability */
  int ml_msg_id;                        /* Id number used for sys CTRL msgs */

  /* for ft-mpi */
  int ml_target_rank;			/* as it might not be me now!! */
  int ml_sgid;					/* senders GID */
  int ml_rgid;					/* receivers GID */
  long ml_msg_length;			/* in bytes raw */
  char *ml_msg_buf;				/* pointer to the actual data */
  int  ml_msg_com_ver;			/* which version of the COM is it! */

  /* to handle partial recvd matching of data */
  struct req_list* ml_matched_nb_rlp;	/* have we matched an nb request? */

  /* a flag to help with multiple queues */
	int ml_queue;
};


/* values to use with the ml_queue flag */
#define unexpmsgQ	100	
#define incompmsgQ	200	
#define systemQ		900	




  /* Unread/pending message list attached to this communicator */
/*
  struct msg_list *cl_msg_list_head;  
  int   cl_msg_list_count;          
*/
				/* number pending messages */
                                /* included as a sanity check */

struct msg_list *     msg_list_init();      /* called only once */
void                  msg_list_free();		/* frees entry from list only */
void                  msg_list_free_unattached();		/* frees an entry that is not on a list yet */
void msg_list_free_all (struct msg_list *ml_head);

void                  msg_list_destroy(); /* destroys list */
void				 msg_list_detach(); /* detaches from list [caution] */

struct msg_list *      msg_list_head();  
struct msg_list *      msg_list_tail(); 
void                  msg_list_dump();      /* dumps contents of whole list */
struct msg_list *      msg_list_new();       /* makes new and adds to tail */
struct msg_list * 		msg_list_new_element_only (); /* make a new unattached element, that must be added to a list */
void 					msg_list_add_2tail(); /* adds unattached to tail of given list */

struct msg_list *      msg_list_find_by_header(); /* search by rank,tag */ 
struct msg_list *      msg_list_find_by_msg_id(); /* search by msg_id */
struct msg_list *      msg_list_find_by_comm (); /* search by comm */ 

int	msg_hdr_match ();	/* return 1 if the headers match */

#endif /* MSGLISTH */


