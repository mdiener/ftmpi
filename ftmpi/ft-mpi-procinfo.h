
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* code for handling proc tables in the form of chained hash tables */
/* GEF July 03 */

/* proc tables are used to store info that translates a proc gid to */
/* connection records, connecton state, proc state, etc etc */
/* i.e. much of what is currently in the com structures will end up here */

#ifndef _FT_MPI_PROCLIST_H

typedef struct proc_list {
   struct proc_list *pl_link, *pl_rlink;	/* linked list links */

   /* proc info details */
   int pl_gid;	/* the GID this procinfo element describes */

   /* numeric representation information */
   unsigned int pl_convinfo;	/* the conversion info value for this GID */
   FTMPI_DDT_CODE_INFO pl_code_info; /* structure used to decode data */

   /* connection info information */
   int pl_conn_index;		/* connection index when connections stored as a table */
   /* conn_info_t * pl_conn;	*/
   /* pointer to the connection structure directly */

   /* local connection info for use in the REBUILD/ACK stages or recovery */
   unsigned int pl_conn_addr;
   int pl_conn_p;
   int pl_state_p;
   int pl_event_p;

   /* other misc info */
   int pl_mcw_rank; /* the rank of this gid in MPI_COMM_WORLD if ANY */

} ftmpi_procinfo_t;


#define	ftmpi_procinfo_index(id) (id%ftmpi_procinfo_m)
#define	ftmpi_procinfo_head(id) (ftmpi_procinfo_table[id%ftmpi_procinfo_m])


/* function prototypes */
int ftmpi_procinfo_init_htable (int m);
int ftmpi_procinfo_debug (int v);
ftmpi_procinfo_t* ftmpi_procinfo_new (int gid);
void ftmpi_procinfo_free (ftmpi_procinfo_t* plp);
void ftmpi_procinfo_free_by_gid (int id);
void ftmpi_procinfo_table_destroy ();
ftmpi_procinfo_t* ftmpi_procinfo_find_by_gid (int id);
int ftmpi_procinfo_set_ddt_code_info (int* gids, int num, unsigned int myconv);
int ftmpi_procinfo_add_and_set_all_info (int* gids, int* gcaddr, int* gcport, 
	  				int* gsport, int* gconvs, int num, unsigned int myconv);
FTMPI_DDT_CODE_INFO* ftmpi_procinfo_get_ddt_code_info (int gid);


#endif /* _FT_MPI_PROCLIST_H */
