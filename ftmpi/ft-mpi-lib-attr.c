
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
			Edgar Gabriel    <egabriel@cs.utk.edu>
			Graham E Fagg    <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
  COMMENTS regarding the implementation of Win_keyval-functions
  and Type_keyval functions: make a copy of the comm-keyval functions,
  replace the constants FTMPI_KEY_COMM? by the proper
  FTMPI_KEY_WIN/TYPE constant, and you are all done for this
  interface ...
  EG, Feb. 25 2003 
*/

#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-com.h"
#include "ft-mpi-attr.h"
#include "ft-mpi-err.h"


/* Definition for the profiling interface */

#ifdef HAVE_PRAGMA_WEAK

#    pragma weak MPI_Attr_delete          = PMPI_Attr_delete
#    pragma weak MPI_Attr_get             = PMPI_Attr_get
#    pragma weak MPI_Attr_put             = PMPI_Attr_put
#    pragma weak MPI_Keyval_create        = PMPI_Keyval_create
#    pragma weak MPI_Keyval_free          = PMPI_Keyval_free

#    pragma weak MPI_Comm_create_keyval	  = PMPI_Comm_create_keyval
#    pragma weak MPI_Comm_free_keyval	  = PMPI_Comm_free_keyval
#    pragma weak MPI_Comm_set_attr        = PMPI_Comm_set_attr
#    pragma weak MPI_Comm_get_attr	  = PMPI_Comm_get_attr
#    pragma weak MPI_Comm_delete_attr	  = PMPI_Comm_delete_attr

#ifdef GIVEITATRY
#    pragma weak MPI_Type_create_keyval	  = PMPI_Type_create_keyval
#    pragma weak MPI_Type_free_keyval	  = PMPI_Type_free_keyval
#    pragma weak MPI_Type_set_attr        = PMPI_Type_set_attr
#    pragma weak MPI_Type_get_attr	  = PMPI_Type_get_attr
#    pragma weak MPI_Type_delete_attr	  = PMPI_Type_delete_attr
#endif
/*
#    pragma weak MPI_Win_create_keyval	  = PMPI_Win_create_keyval
#    pragma weak MPI_Win_keyval	          = PMPI_Win_free_keyval
#    pragma weak MPI_Win_set_attr         = PMPI_Win_set_attr
#    pragma weak MPI_Win_get_attr	  = PMPI_Win_get_attr
#    pragma weak MPI_Win_delete_attr	  = PMPI_Win_delete_attr
*/

#endif /* HAVE_PRAGMA_WEAK */

#    define MPI_Attr_delete           PMPI_Attr_delete
#    define MPI_Attr_get              PMPI_Attr_get
#    define MPI_Attr_put              PMPI_Attr_put
#    define MPI_Keyval_create         PMPI_Keyval_create
#    define MPI_Keyval_free           PMPI_Keyval_free

#    define MPI_Comm_create_keyval    PMPI_Comm_create_keyval
#    define MPI_Comm_free_keyval      PMPI_Comm_free_keyval
#    define MPI_Comm_set_attr	      PMPI_Comm_set_attr
#    define MPI_Comm_get_attr	      PMPI_Comm_get_attr
#    define MPI_Comm_delete_attr      PMPI_Comm_delete_attr

#ifdef GIVEITATRY
#    define MPI_Type_create_keyval    PMPI_Type_create_keyval
#    define MPI_Type_free_keyval      PMPI_Type_free_keyval
#    define MPI_Type_set_attr	      PMPI_Type_set_attr
#    define MPI_Type_get_attr	      PMPI_Type_get_attr
#    define MPI_Type_delete_attr      PMPI_Type_delete_attr
#endif

/*
#    define MPI_Win_create_keyval     PMPI_Win_create_keyval
#    define MPI_Win_free_keyval       PMPI_Win_free_keyval
#    define MPI_Win_set_attr	      PMPI_Win_set_attr
#    define MPI_Win_get_attr	      PMPI_Win_get_attr
#    define MPI_Win_delete_attr       PMPI_Win_delete_attr
*/


static int _mpi_tag_ub=MPI_TAG_UB;
static int _mpi_host=MPI_PROC_NULL;
static int _mpi_io=MPI_ANY_SOURCE;
static int _mpi_wtime=0;

extern int ftmpi_errcode_last;
extern int ftmpi_errcode_recover;
extern int ftmpi_num_dead_procs;

int MPI_Attr_get(MPI_Comm comm,int keyval,void *attribute_val, int *flag)
{
  int ret = MPI_SUCCESS;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  /* first check builtin types */
  /*    INTEGER MPI_TAG_UB, MPI_IO, MPI_HOST, MPI_WTIME_IS_GLOBAL */
  
  switch (keyval) 
    {
      /* for built in attributes, flag=1 / true always */
    case MPI_TAG_UB:	
      *(int**)attribute_val = &_mpi_tag_ub;	/* nasty */
      *flag = 1;
      break;
      
    case MPI_IO :
      *(int**)attribute_val = &_mpi_io;	/* all can do IO */
      *flag = 1;
      break;
      
    case MPI_HOST:
      *(int**)attribute_val = &_mpi_host;	/* no idea what it means by a host */
      *flag = 1;
      break;
      
    case MPI_WTIME_IS_GLOBAL:
      *(int**)attribute_val = &_mpi_wtime;	/* no global time */
      *flag = 1;
      break;
    case FTMPI_ERROR_FAILURE:
      /* error code of last recovery operation */
      *(int**)attribute_val = &ftmpi_errcode_recover;  
      *flag = 1;
      break;
    case FTMPI_NUM_FAILED_PROCS:
      /* number of dead processes at last death event */
      *(int**)attribute_val = &ftmpi_num_dead_procs;  
      *flag = 1;
      break;
    case MPI_LASTUSEDCODE:
      *(int**)attribute_val = &ftmpi_errcode_last;	/* last error code used*/
      *flag = 1;
      break;
    default:
      ret = ftmpi_key_test ( keyval, FTMPI_KEY_COMM1 );
      if ( ret == MPI_SUCCESS )
	{
	  ret = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM1 );
	  if ( ret == MPI_SUCCESS )
	    {
	      *flag = 1;
	      ret = ftmpi_keyval_get ( keyval, (int) comm, attribute_val );
	    }
	  else
	    {
	      *flag = 0;
	      ret = MPI_SUCCESS;
	    }
	}
      else
	ret = MPI_ERR_OTHER;
      break;
    }
  

  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Attr_delete(MPI_Comm comm,int keyval)
{
  int ret;
  int nvalues;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  ret = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM1 );
  if ( ret == MPI_SUCCESS ) {
    ret  = ftmpi_keyval_delete  (keyval, (int) comm, &nvalues );
    if ( ret != MPI_SUCCESS ) RETURNERR (comm, ret );

    if ( ( nvalues == 0 ) && ( ftmpi_key_test_freed ( keyval )))
	  ftmpi_key_release ( keyval );
    return ftmpi_com_del_attr ( comm, keyval );
  }

  RETURNERR ( comm, ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Attr_put(MPI_Comm comm,int keyval,void* attribute_val)
{
  int ret;
  int set;
  int nvalues;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  ret = ftmpi_key_test ( keyval, FTMPI_KEY_COMM1 );
  if ( ret == MPI_SUCCESS ) {
    set = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM1 );
    if ( set == MPI_SUCCESS ) {
      /* delete first the previous value */
      ret = ftmpi_keyval_delete ( keyval, (int) comm, &nvalues );
      if ( ret != MPI_SUCCESS ) RETURNERR ( comm, ret );
    }
    /* set the attribute value given by the user */
    
    ftmpi_keyval_set ( keyval, (int) comm, attribute_val );
    if ( set != MPI_SUCCESS ) {
      /* store this value in the communicator aswell, if it is not
       * already done */
      ftmpi_com_set_attr ( comm, keyval );
    }
    return ( MPI_SUCCESS );
  }
  
  RETURNERR ( comm, ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Keyval_create ( MPI_Copy_function *copy_fn, 
			MPI_Delete_function *del_fn,
			int *keyval, void *extra_state )
{
  int ret;
  int key;

  CHECK_MPIINIT;
  
  key= ftmpi_key_get_next_free  (  FTMPI_KEY_COMM1 );
  ret = ftmpi_key_set ( key, (void*) copy_fn, (void*) del_fn, extra_state );
  if ( ret == MPI_SUCCESS )
    *keyval = key;
  else
    RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Keyval_free (int *keyval )
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_key_test ( *keyval, FTMPI_KEY_COMM1 );
  if ( ret == MPI_SUCCESS )
    *keyval = ftmpi_key_release ( *keyval );
  else
    RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_create_keyval ( MPI_Comm_copy_attr_function *copy_fn,
			     MPI_Comm_delete_attr_function *del_fn,
			     int *keyval, void *extra_state )
{
  int ret;
  int key;

  CHECK_MPIINIT;

  key = ftmpi_key_get_next_free ( FTMPI_KEY_COMM2);
  ret = ftmpi_key_set ( key, (void*) copy_fn, (void *) del_fn, extra_state);
  if ( ret == MPI_SUCCESS )
    *keyval = key;
  else
    RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_free_keyval ( int *keyval )
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_key_test ( *keyval, FTMPI_KEY_COMM2 );
  if ( ret == MPI_SUCCESS )
    *keyval = ftmpi_key_release ( *keyval );
  else
    RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_set_attr ( MPI_Comm comm, int keyval, void *attribute_val )
{
  int ret;
  int set;
  int nvalues;
  
  CHECK_MPIINIT;
  CHECK_COM(comm);

  ret = ftmpi_key_test ( keyval, FTMPI_KEY_COMM2 );
  if ( ret == MPI_SUCCESS )
    {
      set = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM2 );
      if ( set == MPI_SUCCESS )
	{
	  /* delete first the previous value */
	  ret = ftmpi_keyval_delete ( keyval, (int) comm, &nvalues );
	  if ( ret != MPI_SUCCESS ) RETURNERR ( comm, ret );
	}
      /* set the attribute value given by the user */
      ftmpi_keyval_set ( keyval, (int) comm, attribute_val );
      if ( set != MPI_SUCCESS )
	{
	  /* store this value in the communicator aswell */
	  ftmpi_com_set_attr ( comm, keyval );
	}
    }

  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return ( MPI_SUCCESS );

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_get_attr ( MPI_Comm comm, int keyval, void *attribute_val, 
			int *flag)
{
  int ret = MPI_SUCCESS;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  /* first check builtin types */
  /*    INTEGER MPI_TAG_UB, MPI_IO, MPI_HOST, MPI_WTIME_IS_GLOBAL */
  
  switch (keyval)
    {
    
      /* for built in attributes, flag=1 / true always */
    case MPI_TAG_UB:	
      *(int**)attribute_val = &_mpi_tag_ub;	/* nasty */
      *flag = 1;
      break;
      
    case MPI_IO :
      *(int**)attribute_val = &_mpi_io;	/* all can do IO */
      *flag = 1;
      break;
      
    case MPI_HOST:
      *(int**)attribute_val = &_mpi_host; /* no idea what it means by a host */
      *flag = 1;
      break;
    
    case MPI_WTIME_IS_GLOBAL:
      *(int**)attribute_val = &_mpi_wtime;	/* no global time */
      *flag = 1;
      break;
    case FTMPI_ERROR_FAILURE:
      /* error code of last recovery operation */
      *(int**)attribute_val = &ftmpi_errcode_recover;  
      *flag = 1;
      break;
    case FTMPI_NUM_FAILED_PROCS:
      /* number of dead processes at last death event */
      *(int**)attribute_val = &ftmpi_num_dead_procs;  
      *flag = 1;
      break;
    case MPI_LASTUSEDCODE:
      *(int**)attribute_val = &ftmpi_errcode_last;  /* last error code used*/
      *flag = 1;
      break;
    default:
      ret = ftmpi_key_test ( keyval, FTMPI_KEY_COMM2 );
      if ( ret == MPI_SUCCESS )
	{
	  ret = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM2 );
	  if ( ret == MPI_SUCCESS )
	    {
	      *flag = 1;
	      ret = ftmpi_keyval_get ( keyval, (int) comm, attribute_val );
	    }
	  else
	    *flag = 0;
	}
      else
	ret = MPI_ERR_ARG;
      break;
    }

  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_delete_attr ( MPI_Comm comm, int keyval )
{
  int ret;
  int nvalues;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  ret = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_COMM2 );
  if ( ret == MPI_SUCCESS ) {
    ret  = ftmpi_keyval_delete  (keyval, (int) comm, &nvalues );
    if ( ret != MPI_SUCCESS ) RETURNERR ( comm, ret );

    if ( ( nvalues == 0 ) && ( ftmpi_key_test_freed ( keyval )))
	  ftmpi_key_release ( keyval );
      ret = MPI_SUCCESS;

      ftmpi_com_del_attr ( comm, keyval );
    }

  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

#ifdef GIVEITATRY
int MPI_Type_create_keyval ( MPI_Type_copy_attr_function *copy_fn,
			     MPI_Type_delete_attr_function *del_fn,
			     int *keyval, void *extra_state )
{
  int ret;
  int key;

  CHECK_MPIINIT;

  key = ftmpi_key_get_next_free ( FTMPI_KEY_TYPE);
  ret = ftmpi_key_set ( key, (void*) copy_fn, (void *) del_fn, extra_state);
  if ( ret == MPI_SUCCESS )
    *keyval = key;
  else
    RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_free_keyval ( int *keyval )
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_key_test ( *keyval, FTMPI_KEY_TYPE );
  if ( ret == MPI_SUCCESS )
    *keyval = ftmpi_key_release ( *keyval );
  else
    RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_set_attr ( MPI_Datatype type, int keyval, void *attribute_val )
{
  int ret;
  int set;
  int nvalues;
  
  CHECK_MPIINIT;
  CHECK_DDT(type,1);

  ret = ftmpi_key_test ( keyval, FTMPI_KEY_TYPE );
  if ( ret == MPI_SUCCESS )
    {
      set = ftmpi_keyval_test ( keyval, (int) type, FTMPI_KEY_TYPE );
      if ( set == MPI_SUCCESS )
	{
	  /* delete first the previous value */
	  ret = ftmpi_keyval_delete ( keyval, (int) type, &nvalues );
	  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
	}
      /* set the attribute value given by the user */
      ftmpi_keyval_set ( keyval, (int) type, attribute_val );
      if ( set != MPI_SUCCESS )
	{
	  /* store this value in the datatype aswell */
	  ftmpi_type_set_attr ( type, keyval );
	}
    }

  if ( ret != MPI_SUCCESS )
    RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_get_attr ( MPI_Datatype type, int keyval, void *attribute_val, 
			int *flag)
{
  int ret = MPI_SUCCESS;

  CHECK_MPIINIT;
  CHECK_DDT(type,1);

  /* no predefined attributes for datatypes */
  ret = ftmpi_key_test ( keyval, FTMPI_KEY_TYPE );
  if ( ret == MPI_SUCCESS )
    {
      ret = ftmpi_keyval_test ( keyval, (int) type, FTMPI_KEY_TYPE );
      if ( ret == MPI_SUCCESS )
	{
	  *flag = 1;
	  ret = ftmpi_keyval_get ( keyval, (int) type, attribute_val );
	}
      else
	*flag = 0;
    }
  else
    ret = MPI_ERR_ARG;

  if ( ret != MPI_SUCCESS )
    RETURNERR ( MPI_COMM_WORLD, ret );
  
  return( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_delete_attr ( MPI_Datatype type, int keyval )
{
  int ret;
  int nvalues;

  CHECK_MPIINIT;
  CHECK_DDT(type,1);
  
  ret = ftmpi_keyval_test ( keyval, (int) comm, FTMPI_KEY_TYPE );
  if ( ret == MPI_SUCCESS ) {
    ret  = ftmpi_keyval_delete  (keyval, (int) type, &nvalues );
    if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

    if ( ( nvalues == 0 ) && ( ftmpi_key_test_freed ( keyval )))
      ftmpi_key_release ( keyval );
    ret = MPI_SUCCESS;
    
    ftmpi_type_del_attr ( type, keyval );
  }

  if ( ret != MPI_SUCCESS )
    RETURNERR ( MPI_COMM_WORLD, ret );

  return( MPI_SUCCESS );
}
#endif
