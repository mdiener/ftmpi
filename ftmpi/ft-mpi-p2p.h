
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_P2P
#define _FT_MPI_H_P2P

typedef struct req_list req_list_t;

int  ftmpi_mpi_send( void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm );
int  ftmpi_mpi_recv( void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status *status );


int ftmpi_pp_send (int gid, int trank, int comm, int tag, char* ptr, int dsize, int flags);

int ftmpi_pp_recv (int gid, int rank, int comm, int tag, char* ptr, int dsize, int flags, MPI_Status *status);

int ftmpi_pp_get_from_msg_list (comm_info_t *cptr, struct msg_list* mlp, void* ptr, long dsize, MPI_Status *status);

/* soon to be defunct*/
int ftmpi_pp_put_into_msg_list (int recvconn, long fsize, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize);

/* SNIPE2 version */
int ftmpi_pp_post_recv_into_msg_list (int recvconn, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize);

/* soon to be defunct */
int ftmpi_pp_put_into_req_list (req_list_t* rlp, int recvconn, long fsize, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize);

/* SNIPE 2 version */
int ftmpi_pp_post_recv_into_req_list (struct req_list *rlp, int recvconn, int intype, int inmsgid, int incomm, int infrom, int into, int infromgid, int intogid, int intag, int insize);


int ftmpi_pp_probe (int rank, int tag, int comm, int *flag, MPI_Status *status,
					int blocking);

void ftmpi_pp_set_nullstatus (MPI_Status *status);
void ftmpi_pp_set_emptystatus (MPI_Status *status);

int ftmpi_pp_recv_on_gid_list (int *gids, int ngids, int blocking);

int ftmpi_pp_complete_recv_on_hdr (msg_hdr_t *hdr, int fsize, int recvconn, int *typerecvd);

int ftmpi_pp_add_msg_to_msg_list (struct msg_list *mlp);
int ftmpi_move_sys_msgs_to_unexp_msg_list (int comm);

#endif /*  _FT_MPI_H_P2P */
