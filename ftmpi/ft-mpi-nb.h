
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg    <fagg@cs.utk.edu>	<project lead>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 These are the lower level non-blocking [support] operations
 */

#ifndef _FT_MPI_H_NB
#define _FT_MPI_H_NB

/* constants */

/* first ones used in the progress calls */
#define NB_POLL	0
#define	NB_ANY 1
#define	NB_ANYNB 2
#define NB_ANYNB_LIST 3
#define NB_ALLNB_LIST 4

/* progress events shared betweem recvstate/conn/nb.c */
#define PROGRESS_NEWCONN			10
#define PROGRESS_CONNERROR			20
#define PROGRESS_CONNCLOSE			30
#define PROGRESS_SEND_COMPLETED		40
#define PROGRESS_RECV_COMPLETED		50
#define PROGRESS_TRUNC_COMPLETED	60
#define PROGRESS_HDR_RECVD 			70
#define PROGRESS_OTHER 				80
#define PROGRESS_EVENT				90
#define PROGRESS_READYCONN			100



/* prototypes for NB functions */

int ftmpi_mpi_irecv (void* buf,int count,MPI_Datatype datatype,int source,
		     int tag,MPI_Comm comm,MPI_Request *request);
int ftmpi_mpi_isend(void* buf,int count,MPI_Datatype datatype,int dest,
		    int tag,MPI_Comm comm,MPI_Request *request);
int ftmpi_mpi_test (MPI_Request *request,int *flag, MPI_Status *status);
int ftmpi_mpi_wait (MPI_Request *request,MPI_Status *status);


/* request list handling */

/* all pending, done & free list handling */
void ftmpi_nb_init_req_and_msg_lists(); 

/* release all entries on all req and msg lists (non com owned) */
void ftmpi_nb_reset_req_and_msg_lists();

/* on the otehr system queues */
void ftmpi_nb_reset_unexpected_and_inprogress_msg_lists();


/* special movement functions for recovery */
int ftmpi_nb_move_msg_list_to_system_list (struct msg_list* ml_head);
int ftmpi_nb_filter_system_list (int cnt, int* deadlist);
int ftmpi_nb_filter_req_lists_and_reqtable (int cnt, int* deadlist);
int ftmpi_nb_filter_req_list (int cnt, int* deadlist, struct req_list* rl_head, 								long *listcounter);




/* request table of handles to list element functions */

void ftmpi_nb_clearall_reqhandles ();	/* clear table */
int ftmpi_nb_get_freehandle ();			/* get a free slot in table */
int ftmpi_nb_req_ok (MPI_Request);		/* check handle */



/* request element functions */

struct req_list * ftmpi_nb_get_reqlist_ptr_by_handle (MPI_Request); /* get ptr */
int ftmpi_nb_get_req_target_rank (MPI_Request);			
int ftmpi_nb_get_req_type (MPI_Request);
int ftmpi_nb_get_req_datatype (MPI_Request req);

int ftmpi_nb_req_decref (MPI_Request);
int ftmpi_nb_req_incref (MPI_Request);
int ftmpi_nb_free_req_handle (MPI_Request rh);	/* frees reqtable entry up */

void ftmpi_nb_free_all_reqs (struct req_list* rl_head);
void ftmpi_nb_free_all_msgs (struct msg_list* ml_head);
int ftmpi_nb_req_force_free (struct req_list *rlp);
int ftmpi_nb_msg_force_free (struct msg_list *mlp);

int ftmpi_nb_req_force_cancel (struct req_list *rlp);



int ftmpi_nb_req_isdone (MPI_Request rh);
int ftmpi_nb_req_iscancelled (MPI_Request rh);

int ftmpi_nb_req_cancel (MPI_Request rh);


int ftmpi_nb_is_send (MPI_Request req);
int ftmpi_nb_is_recv (MPI_Request req);

int ftmpi_nb_get_req_refcnt (MPI_Request);



/* set data values in the request elements */

int ftmpi_nb_put_req_type (int, MPI_Request);
int ftmpi_nb_set_req (void*,int,MPI_Datatype,int,int,MPI_Comm,int,MPI_Request);

/* set/get data values in the request elements for bsendinit  */
int ftmpi_nb_set_req_bsendinit (void*, int, MPI_Datatype, MPI_Request);
int ftmpi_nb_get_req_bsendinit (void**, int*, MPI_Datatype*, MPI_Request);

int ftmpi_nb_set_req_buf_cnt_dt (void*, int, MPI_Datatype, MPI_Request);

/* handle request with MPI_PROC_NULL */
int ftmpi_nb_handle_proc_null( MPI_Comm comm, MPI_Request* request );

/* send / recv / done list operations */

int ftmpi_nb_add2_send_list (void*,int,MPI_Datatype,int,int,MPI_Comm,MPI_Request);
int ftmpi_nb_add2_recv_list (void*,int,MPI_Datatype,int,int,MPI_Comm,MPI_Request);
int ftmpi_nb_req_add2_done_list (MPI_Request req, int updaterefcnt);
int ftmpi_nb_find_completed (MPI_Request,MPI_Status*,int);

/* these add pre built requests to the send/recv lists */
/* if a persistant req then you must update the ref cnt */
/* if a normal isend/recv then you should not GEF */
int ftmpi_nb_req_add2_send_list (MPI_Request, int updaterefcnt);
int ftmpi_nb_req_add2_recv_list (MPI_Request, int updaterefcnt);

/* special version of above */
int ftmpi_nb_send2_self_test_and_do (MPI_Request req);




/* low level send or recv to done list operations */

int	ftmpi_nb_move_recv_2_done (struct req_list* rlp);
int ftmpi_nb_move_send_2_done (struct req_list* rlp);

/* higher level version of above that uses handles only */
int ftmpi_nb_move_send_request_2_done (MPI_Request sreq);
int ftmpi_nb_move_recv_inprogress_2_done (struct req_list* rlptr);

   
/* higher level data movers */
int	ftmpi_nb_progress (int testorwait);
int ftmpi_nb_progress_recv_req (MPI_Request req);
int ftmpi_nb_complete_upto_req (MPI_Request req);


/* low level data memory to memory movers */

/* moves data into user buf from recv buffer in data conversion is needed */
int ftmpi_nb_decode_buf (struct req_list *rlp); 

/* moves data from the users buf into a preallocated tmp buf if conversion is needed */
int ftmpi_nb_encode_buf (struct req_list *rlp);

/* low level device driver interfaces */
int ftmpi_nb_post_send (MPI_Request req);
int ftmpi_nb_post_recv (MPI_Request req);	/* not used by SNIPE2 but by OVM */
int ftmpi_nb_post_allpending (int tgid, int chan);
int ftmpi_nb_handle_recvd_header (int recvconn);
int ftmpi_nb_handle_recvd_data (int recvconn);



/* counts on number of requests to complete before waits on the target ones complete */
/* these routines are used by blocking p2p, progress and wait routines... */
int ftmpi_nb_total_sends (MPI_Request req);
int ftmpi_nb_total_recvs (MPI_Request req);

/* progress calls */
int ftmpi_pp_recv_on_gid_list (int *gids, int count, int blocking);


int ftmpi_nb_check_msglist_for_matching_req_and_get (int req);
int ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink (int req);
void ftmpi_nb_per_set_nullstatus_and_mark_done ( MPI_Request req);

#endif /*  _FT_MPI_H_NB */

