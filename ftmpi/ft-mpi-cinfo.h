
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
            Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




#ifndef _FT_MPI_CINFO_H
#define _FT_MPI_CINFO_H

#include<stdio.h>
#include<stdlib.h>

#include "msg.h"
#include "msgbuf.h"
#include "ns_lib.h"



#define CONNECT 2001
#define NEWCOMM 2002
#define FREECOMM 2003
#define COPYCOM 2004
#define COMSTATE 2005
#define PROCSTATE 2006
#define UPDATECOM 2007
#define FINALIZE 2008


#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 256
#endif

int ft_mpi_cinfo_find();
int ft_mpi_cinfo_make_conn();
void ft_mpi_cinfo_close_conn();
void ft_mpi_cinfo_req_new_comm(int commid,int numprocs,int active,int state,char * hostname,int * pid,int * pstate);
void ft_mpi_cinfo_req_com_state(int commid,int numprocs,int active,int state);
void ft_mpi_cinfo_req_proc_state(int commid,int rank,int pstate);
void ft_mpi_cinfo_req_update_com(int commid,int numprocs,int active,int state,int * pid,int * pstate);
void ft_mpi_cinfo_req_finalize_comm(int commid);

#endif	/* _FT_MPI_CINFO_H */
