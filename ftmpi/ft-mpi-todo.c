#include "mpi.h"
#include<stdio.h>

/* temp values */
int req=0;
MPI_Status blank_status={0,0,0,0,0,0};


/*********************************************************************************/
/*********************************************************************************/
int MPI_Attr_delete(MPI_Comm comm,int keyval)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Attr_delete< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Attr_get(MPI_Comm comm,int keyval,void *attribute_val, int *flag)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Attr_get< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Attr_put(MPI_Comm comm,int keyval,void* attribute_val)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Attr_put< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Cancel(MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Cancel< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Errhandler_create(MPI_Handler_function *function, MPI_Errhandler *errhandler)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Errhandler_create< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Errhandler_set(MPI_Comm comm,MPI_Errhandler errhandler)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Errhandler_set< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Irecv(void* buf,int count,MPI_Datatype datatype,int source,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Irecv< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Isend(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
/* int  MPI_Send( buf, count, datatype, dest, tag, comm ) */

/* this is a shell that does something so we can test stuff */

int rc;

req++;

rc = MPI_Send( buf, count, datatype, dest, tag, comm );

if (rc==MPI_SUCCESS) *request=req;
else
	*request=MPI_REQUEST_NULL;

return (rc);

}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Keyval_create(MPI_Copy_function *copy_fn,MPI_Delete_function *delete_fn,int *keyval,void* extra_state)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Keyval_create< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Recv_init(void* buf,int count,MPI_Datatype datatype,int source,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Recv_init< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Request_free(MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Request_free< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Rsend_init(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Rsend_init< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Send_init(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Send_init< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Ssend_init(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Ssend_init< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Start(MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Start< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Startall(int count,MPI_Request *array_of_requests)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Startall< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Wait(MPI_Request *request,MPI_Status *status)
{
/*   printf("************* WARNING *************\n"); */
/*   printf("*** UNIMPLEMENTED FUNCT >MPI_Wait< ***\n"); */
/*   printf("************* WARNING *************\n"); */
/*   fflush(stdout); */
/*   return (MPI_ERR_INTERN); */
if (*request==MPI_REQUEST_NULL) return (MPI_ERR_ARG);
else {
	*status=blank_status;
	*request=MPI_REQUEST_NULL; /* ? can we ? XXX */
	return (MPI_SUCCESS);
	}
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Waitall(int count,MPI_Request *array_of_requests,MPI_Status *array_of_statuses)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Waitall< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_Waitany(int count,MPI_Request *array_of_requests,int *index,MPI_Status *status)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Waitany< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_NULL_COPY_FN ( MPI_Comm a, int b, void * c, void * d, void * f, int * h )
{

  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_NULL_COPY_FN< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}
/*********************************************************************************/
/*********************************************************************************/
int MPI_NULL_DELETE_FN ( MPI_Comm a, int b, void * c, void * d )
{

  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_NULL_DELETE_FN< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

/*********************************************************************************/

/* NEW STUFF */

/*********************************************************************************/

int MPI_Error_class(int errorcode,int *errorclass)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Error_class< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

/*********************************************************************************/
int MPI_Testall(int count,MPI_Request *array_of_requests,int *flag,MPI_Status *array_of_statuses)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Testall< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

/*********************************************************************************/
int MPI_Irsend(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Irsend< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

int MPI_Cart_create(MPI_Comm comm_old,int ndims,int *dims,int *periods,int reorder,MPI_Comm *comm_cart)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Cart_create< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

int MPI_Cart_rank(MPI_Comm comm,int *coords,int *rank)
{
  printf("************* WARNING *************\n");
  printf("*** UNIMPLEMENTED FUNCT >MPI_Cart_rank< ***\n");
  printf("************* WARNING *************\n");
  fflush(stdout);
  return (MPI_ERR_INTERN);
}

