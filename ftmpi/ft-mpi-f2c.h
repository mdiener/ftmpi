/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/


/*
  Warning this code was in part originally from PVMPI/MPI_Connect
  Thus you can blame funding from the US Dept of Defence, Mod office
  PET program for making this come into existance.
)
  Graham, Feb 2002 fagg@hlrs.de
*/

#ifndef _FT_MPI_H_F2C
#define _FT_MPI_H_F2C

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
/* #include "error.h" */
#include "string.h"

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

/*#  ifdef __PRAGMA_REDEFINE_EXTNAME
Skipping currently this section. However, it is easy
to generate the redefine_extname sections out of the
pragma-weak statements with macros and/or emacs
EG, Jan. 14 2003 */
/* #  else  */

/* SOLARIS style pragma weak */

#  ifdef FORTRANCAPS

/*   Callback functions */
#    pragma weak MPI_NULL_COPY_FN           = mpi_null_copy_fn_	
#    pragma weak PMPI_NULL_COPY_FN          = mpi_null_copy_fn_
#    pragma weak MPI_NULL_DELETE_FN         = mpi_null_delete_fn_	
#    pragma weak PMPI_NULL_DELETE_FN        = mpi_null_delete_fn_
#    pragma weak MPI_DUP_FN                 = mpi_dup_fn_
#    pragma weak PMPI_DUP_FN                = mpi_dup_fn_
 
/*  Cartesian Topologies */
#    pragma weak MPI_CART_CREATE            = mpi_cart_create_
#    pragma weak PMPI_CART_CREATE           = mpi_cart_create_ 
#    pragma weak MPI_CART_RANK              = mpi_cart_rank_
#    pragma weak PMPI_CART_RANK             = mpi_cart_rank_ 
#    pragma weak MPI_CART_COORDS            = mpi_cart_coords_	
#    pragma weak PMPI_CART_COORDS           = mpi_cart_coords_
#    pragma weak MPI_CARTDIM_GET            = mpi_cartdim_get_	
#    pragma weak PMPI_CARTDIM_GET           = mpi_cartdim_get_
#    pragma weak MPI_CART_GET               = mpi_cart_get_	        
#    pragma weak PMPI_CART_GET              = mpi_cart_get_
#    pragma weak MPI_CART_MAP               = mpi_cart_map_	        
#    pragma weak PMPI_CART_MAP              = mpi_cart_map_
#    pragma weak MPI_CART_SHIFT             = mpi_cart_shift_       
#    pragma weak PMPI_CART_SHIFT            = mpi_cart_shift_
#    pragma weak MPI_CART_SUB               = mpi_cart_sub_	        
#    pragma weak PMPI_CART_SUB              = mpi_cart_sub_
#    pragma weak MPI_DIMS_CREATE            = mpi_dims_create_	  
#    pragma weak PMPI_DIMS_CREATE           = mpi_dims_create_
#    pragma weak MPI_TOPO_TEST              = mpi_topo_test_	
#    pragma weak PMPI_TOPO_TEST             = mpi_topo_test_


/* General graph topologies */
#    pragma weak MPI_GRAPH_CREATE           = mpi_graph_create_	
#    pragma weak PMPI_GRAPH_CREATE          = mpi_graph_create_
#    pragma weak MPI_GRAPHDIMS_GET          = mpi_graphdims_get_	
#    pragma weak PMPI_GRAPHDIMS_GET         = mpi_graphdims_get_
#    pragma weak MPI_GRAPH_GET              = mpi_graph_get_	
#    pragma weak PMPI_GRAPH_GET             = mpi_graph_get_
#    pragma weak MPI_GRAPH_MAP              = mpi_graph_map_	
#    pragma weak PMPI_GRAPH_MAP             = mpi_graph_map_
#    pragma weak MPI_GRAPH_NEIGHBORS_COUNT  = mpi_graph_neighbors_count_	
#    pragma weak PMPI_GRAPH_NEIGHBORS_COUNT = mpi_graph_neighbors_count_
#    pragma weak MPI_GRAPH_NEIGHBORS        = mpi_graph_neighbors_	
#    pragma weak PMPI_GRAPH_NEIGHBORS       = mpi_graph_neighbors_


/* Error handler */
#    pragma weak MPI_ERRHANDLER_SET         = mpi_errhandler_set_
#    pragma weak PMPI_ERRHANDLER_SET        = mpi_errhandler_set_ 
#    pragma weak MPI_ERROR_CLASS            = mpi_error_class_
#    pragma weak PMPI_ERROR_CLASS           = mpi_error_class_ 
#    pragma weak MPI_ERRHANDLER_CREATE      = mpi_errhandler_create_  
#    pragma weak PMPI_ERRHANDLER_CREATE     = mpi_errhandler_create_
#    pragma weak MPI_ERRHANDLER_FREE        = mpi_errhandler_free_	  
#    pragma weak PMPI_ERRHANDLER_FREE       = mpi_errhandler_free_
#    pragma weak MPI_ERRHANDLER_GET         = mpi_errhandler_get_	  
#    pragma weak PMPI_ERRHANDLER_GET        = mpi_errhandler_get_
#    pragma weak MPI_ERROR_STRING           = mpi_error_string_	  
#    pragma weak PMPI_ERROR_STRING          = mpi_error_string_

#    pragma weak MPI_ADD_ERROR_STRING       = mpi_add_error_string_
#    pragma weak PMPI_ADD_ERROR_STRING      = mpi_add_error_string_
#    pragma weak MPI_ADD_ERROR_CLASS        = mpi_add_error_class_
#    pragma weak PMPI_ADD_ERROR_CLASS       = mpi_add_error_class_
#    pragma weak MPI_ADD_ERROR_CODE         = mpi_add_error_code_
#    pragma weak PMPI_ADD_ERROR_CODE        = mpi_add_error_code_

/* Group functions */
#    pragma weak MPI_GROUP_INCL             = mpi_group_incl_
#    pragma weak PMPI_GROUP_INCL            = mpi_group_incl_ 
#    pragma weak MPI_GROUP_FREE             = mpi_group_free_
#    pragma weak PMPI_GROUP_FREE            = mpi_group_free_ 
#    pragma weak MPI_GROUP_COMPARE          = mpi_group_compare_	
#    pragma weak PMPI_GROUP_COMPARE         = mpi_group_compare_
#    pragma weak MPI_GROUP_DIFFERENCE       = mpi_group_difference_	
#    pragma weak PMPI_GROUP_DIFFERENCE      = mpi_group_difference_
#    pragma weak MPI_GROUP_EXCL             = mpi_group_excl_	
#    pragma weak PMPI_GROUP_EXCL            = mpi_group_excl_
#    pragma weak MPI_GROUP_INTERSECTION     = mpi_group_intersection_	
#    pragma weak PMPI_GROUP_INTERSECTION    = mpi_group_intersection_
#    pragma weak MPI_GROUP_RANK             = mpi_group_rank_
#    pragma weak PMPI_GROUP_RANK            = mpi_group_rank_
#    pragma weak MPI_GROUP_SIZE             = mpi_group_size_
#    pragma weak PMPI_GROUP_SIZE            = mpi_group_size_
#    pragma weak MPI_GROUP_RANGE_EXCL       = mpi_group_range_excl_	
#    pragma weak PMPI_GROUP_RANGE_EXCL      = mpi_group_range_excl_
#    pragma weak MPI_GROUP_RANGE_INCL       = mpi_group_range_incl_	
#    pragma weak PMPI_GROUP_RANGE_INCL      = mpi_group_range_incl_
#    pragma weak MPI_GROUP_TRANSLATE_RANKS  = mpi_group_translate_ranks_	
#    pragma weak PMPI_GROUP_TRANSLATE_RANKS = mpi_group_translate_ranks_
#    pragma weak MPI_GROUP_UNION            = mpi_group_union_	
#    pragma weak PMPI_GROUP_UNION           = mpi_group_union_


/* Op functions */
#    pragma weak  MPI_OP_CREATE             = mpi_op_create_
#    pragma weak  PMPI_OP_CREATE            = mpi_op_create_ 
#    pragma weak  MPI_OP_FREE               = mpi_op_free_
#    pragma weak  PMPI_OP_FREE              = mpi_op_free_                          

/* Attribute functions */
#    pragma weak MPI_ATTR_DELETE            = mpi_attr_delete_
#    pragma weak PMPI_ATTR_DELETE           = mpi_attr_delete_ 
#    pragma weak MPI_ATTR_GET               = mpi_attr_get_	       
#    pragma weak PMPI_ATTR_GET              = mpi_attr_get_
#    pragma weak MPI_ATTR_PUT               = mpi_attr_put_	        
#    pragma weak PMPI_ATTR_PUT              = mpi_attr_put_
#    pragma weak MPI_KEYVAL_CREATE          = mpi_keyval_create_	
#    pragma weak PMPI_KEYVAL_CREATE         = mpi_keyval_create_
#    pragma weak MPI_KEYVAL_FREE            = mpi_keyval_free_	
#    pragma weak PMPI_KEYVAL_FREE           = mpi_keyval_free_


/* Info-object functions 
#    pragma weak MPI_INFO_CREATE            = mpi_info_create_      
#    pragma weak PMPI_INFO_CREATE           = mpi_info_create_
#    pragma weak MPI_INFO_DELETE            = mpi_info_delete_	
#    pragma weak PMPI_INFO_DELETE           = mpi_info_delete_
#    pragma weak MPI_INFO_DUP               = mpi_info_dup_	
#    pragma weak PMPI_INFO_DUP              = mpi_info_dup_
#    pragma weak MPI_INFO_FREE              = mpi_info_free_	
#    pragma weak PMPI_INFO_FREE             = mpi_info_free_
#    pragma weak MPI_INFO_GET               = mpi_info_get_	
#    pragma weak PMPI_INFO_GET              = mpi_info_get_
#    pragma weak MPI_INFO_GET_NKEYS         = mpi_info_get_nkeys_	
#    pragma weak PMPI_INFO_GET_NKEYS        = mpi_info_get_nkeys_
#    pragma weak MPI_INFO_GET_NTHKEY        = mpi_info_get_nthkey_	
#    pragma weak PMPI_INFO_GET_NTHKEY       = mpi_info_get_nthkey_
#    pragma weak MPI_INFO_GET_VALUELEN      = mpi_info_get_valuelen_	
#    pragma weak PMPI_INFO_GET_VALUELEN     = mpi_info_get_valuelen_
#    pragma weak MPI_INFO_SET               = mpi_info_set_	
#    pragma weak PMPI_INFO_SET              = mpi_info_set_
*/
    
#  elif defined(FORTRANDOUBLEUNDERSCORE)

/*   Callback functions */
#    pragma weak mpi_null_copy_fn__         = mpi_null_copy_fn_	
#    pragma weak pmpi_null_copy_fn__        = mpi_null_copy_fn_
#    pragma weak mpi_null_delete_fn__       = mpi_null_delete_fn_	
#    pragma weak pmpi_null_delete_fn__      = mpi_null_delete_fn_
#    pragma weak mpi_dup_fn__               = mpi_dup_fn_
#    pragma weak pmpi_dup_fn__              = mpi_dup_fn_
 
/*  Cartesian Topologies */
#    pragma weak mpi_cart_create__          = mpi_cart_create_
#    pragma weak pmpi_cart_create__         = mpi_cart_create_ 
#    pragma weak mpi_cart_rank__            = mpi_cart_rank_
#    pragma weak pmpi_cart_rank__           = mpi_cart_rank_ 
#    pragma weak mpi_cart_coords__          = mpi_cart_coords_	
#    pragma weak pmpi_cart_coords__         = mpi_cart_coords_
#    pragma weak mpi_cartdim_get__          = mpi_cartdim_get_	
#    pragma weak pmpi_cartdim_get__         = mpi_cartdim_get_
#    pragma weak mpi_cart_get__             = mpi_cart_get_	        
#    pragma weak pmpi_cart_get__            = mpi_cart_get_
#    pragma weak mpi_cart_map__             = mpi_cart_map_	        
#    pragma weak pmpi_cart_map__            = mpi_cart_map_
#    pragma weak mpi_cart_shift__           = mpi_cart_shift_       
#    pragma weak pmpi_cart_shift__          = mpi_cart_shift_
#    pragma weak mpi_cart_sub__             = mpi_cart_sub_	        
#    pragma weak pmpi_cart_sub__            = mpi_cart_sub_
#    pragma weak mpi_dims_create__          = mpi_dims_create_	  
#    pragma weak pmpi_dims_create__         = mpi_dims_create_
#    pragma weak mpi_topo_test__            = mpi_topo_test_	
#    pragma weak pmpi_topo_test__           = mpi_topo_test_


/* General graph topologies */
#    pragma weak mpi_graph_create__         = mpi_graph_create_	
#    pragma weak pmpi_graph_create__        = mpi_graph_create_
#    pragma weak mpi_graphdims_get__        = mpi_graphdims_get_	
#    pragma weak pmpi_graphdims_get__       = mpi_graphdims_get_
#    pragma weak mpi_graph_get__            = mpi_graph_get_	
#    pragma weak pmpi_graph_get__           = mpi_graph_get_
#    pragma weak mpi_graph_map__            = mpi_graph_map_	
#    pragma weak pmpi_graph_map__           = mpi_graph_map_
#    pragma weak mpi_graph_neighbors_count__  = mpi_graph_neighbors_count_	
#    pragma weak pmpi_graph_neighbors_count__ = mpi_graph_neighbors_count_
#    pragma weak mpi_graph_neighbors__      = mpi_graph_neighbors_	
#    pragma weak pmpi_graph_neighbors__     = mpi_graph_neighbors_


/* Error handler */
#    pragma weak mpi_errhandler_set__       = mpi_errhandler_set_
#    pragma weak pmpi_errhandler_set__      = mpi_errhandler_set_ 
#    pragma weak mpi_error_class__          = mpi_error_class_
#    pragma weak pmpi_error_class__         = mpi_error_class_ 
#    pragma weak mpi_errhandler_create__    = mpi_errhandler_create_  
#    pragma weak pmpi_errhandler_create__   = mpi_errhandler_create_
#    pragma weak mpi_errhandler_free__      = mpi_errhandler_free_	  
#    pragma weak pmpi_errhandler_free__     = mpi_errhandler_free_
#    pragma weak mpi_errhandler_get__       = mpi_errhandler_get_	  
#    pragma weak pmpi_errhandler_get__      = mpi_errhandler_get_
#    pragma weak mpi_error_string__         = mpi_error_string_	  
#    pragma weak pmpi_error_string__        = mpi_error_string_

#    pragma weak mpi_add_error_string__     = mpi_add_error_string_
#    pragma weak pmpi_add_error_string__    = mpi_add_error_string_
#    pragma weak mpi_add_error_class__        = mpi_add_error_class_
#    pragma weak pmpi_add_error_class__       = mpi_add_error_class_
#    pragma weak mpi_add_error_code__         = mpi_add_error_code_
#    pragma weak pmpi_add_error_code__        = mpi_add_error_code_

/* Group functions */
#    pragma weak mpi_group_incl__           = mpi_group_incl_
#    pragma weak pmpi_group_incl__          = mpi_group_incl_ 
#    pragma weak mpi_group_free__           = mpi_group_free_
#    pragma weak pmpi_group_free__          = mpi_group_free_ 
#    pragma weak mpi_group_compare__        = mpi_group_compare_	
#    pragma weak pmpi_group_compare__       = mpi_group_compare_
#    pragma weak mpi_group_difference__     = mpi_group_difference_	
#    pragma weak pmpi_group_difference__    = mpi_group_difference_
#    pragma weak mpi_group_excl__           = mpi_group_excl_	
#    pragma weak pmpi_group_excl__          = mpi_group_excl_
#    pragma weak mpi_group_intersection__   = mpi_group_intersection_	
#    pragma weak pmpi_group_intersection__  = mpi_group_intersection_
#    pragma weak mpi_group_rank__           = mpi_group_rank_
#    pragma weak pmpi_group_rank__          = mpi_group_rank_
#    pragma weak mpi_group_size__           = mpi_group_size_
#    pragma weak pmpi_group_size__          = mpi_group_size_
#    pragma weak mpi_group_range_excl__     = mpi_group_range_excl_	
#    pragma weak pmpi_group_range_excl__    = mpi_group_range_excl_
#    pragma weak mpi_group_range_incl__     = mpi_group_range_incl_	
#    pragma weak pmpi_group_range_incl__    = mpi_group_range_incl_
#    pragma weak mpi_group_translate_ranks__  = mpi_group_translate_ranks_	
#    pragma weak pmpi_group_translate_ranks__ = mpi_group_translate_ranks_
#    pragma weak mpi_group_union__          = mpi_group_union_	
#    pragma weak pmpi_group_union__         = mpi_group_union_


/* Op functions */
#    pragma weak  mpi_op_create__           = mpi_op_create_
#    pragma weak  pmpi_op_create__          = mpi_op_create_ 
#    pragma weak  mpi_op_free__             = mpi_op_free_
#    pragma weak  pmpi_op_free__            = mpi_op_free_                          

/* Attribute functions */
#    pragma weak mpi_attr_delete__          = mpi_attr_delete_
#    pragma weak pmpi_attr_delete__         = mpi_attr_delete_ 
#    pragma weak mpi_attr_get__             = mpi_attr_get_	       
#    pragma weak pmpi_attr_get__            = mpi_attr_get_
#    pragma weak mpi_attr_put__             = mpi_attr_put_	        
#    pragma weak pmpi_attr_put__            = mpi_attr_put_
#    pragma weak mpi_keyval_create__        = mpi_keyval_create_	
#    pragma weak pmpi_keyval_create__       = mpi_keyval_create_
#    pragma weak mpi_keyval_free__          = mpi_keyval_free_	
#    pragma weak pmpi_keyval_free__         = mpi_keyval_free_


/* Info-object functions 
#    pragma weak mpi_info_create__          = mpi_info_create_      
#    pragma weak pmpi_info_create__         = mpi_info_create_
#    pragma weak mpi_info_delete__          = mpi_info_delete_	
#    pragma weak pmpi_info_delete__         = mpi_info_delete_
#    pragma weak mpi_info_dup__             = mpi_info_dup_	
#    pragma weak pmpi_info_dup__            = mpi_info_dup_
#    pragma weak mpi_info_free__            = mpi_info_free_	
#    pragma weak pmpi_info_free__           = mpi_info_free_
#    pragma weak mpi_info_get__             = mpi_info_get_	
#    pragma weak pmpi_info_get__            = mpi_info_get_
#    pragma weak mpi_info_get_nkeys__         = mpi_info_get_nkeys_	
#    pragma weak pmpi_info_get_nkeys__        = mpi_info_get_nkeys_
#    pragma weak mpi_info_get_nthkey__        = mpi_info_get_nthkey_	
#    pragma weak pmpi_info_get_nthkey__       = mpi_info_get_nthkey_
#    pragma weak mpi_info_get_valuelen__      = mpi_info_get_valuelen_	
#    pragma weak pmpi_info_get_valuelen__     = mpi_info_get_valuelen_
#    pragma weak mpi_info_set__             = mpi_info_set_	
#    pragma weak pmpi_info_set__            = mpi_info_set_
*/
    
#  elif defined(FORTRANNOUNDERSCORE)

/*   Callback functions */
#    pragma weak mpi_null_copy_fn         = mpi_null_copy_fn_	
#    pragma weak pmpi_null_copy_fn        = mpi_null_copy_fn_
#    pragma weak mpi_null_delete_fn       = mpi_null_delete_fn_	
#    pragma weak pmpi_null_delete_fn      = mpi_null_delete_fn_
#    pragma weak mpi_dup_fn               = mpi_dup_fn_
#    pragma weak pmpi_dup_fn              = mpi_dup_fn_
 
/*  Cartesian Topologies */
#    pragma weak mpi_cart_create          = mpi_cart_create_
#    pragma weak pmpi_cart_create         = mpi_cart_create_ 
#    pragma weak mpi_cart_rank            = mpi_cart_rank_
#    pragma weak pmpi_cart_rank           = mpi_cart_rank_ 
#    pragma weak mpi_cart_coords          = mpi_cart_coords_	
#    pragma weak pmpi_cart_coords         = mpi_cart_coords_
#    pragma weak mpi_cartdim_get          = mpi_cartdim_get_	
#    pragma weak pmpi_cartdim_get         = mpi_cartdim_get_
#    pragma weak mpi_cart_get             = mpi_cart_get_	        
#    pragma weak pmpi_cart_get            = mpi_cart_get_
#    pragma weak mpi_cart_map             = mpi_cart_map_	        
#    pragma weak pmpi_cart_map            = mpi_cart_map_
#    pragma weak mpi_cart_shift           = mpi_cart_shift_       
#    pragma weak pmpi_cart_shift          = mpi_cart_shift_
#    pragma weak mpi_cart_sub             = mpi_cart_sub_	        
#    pragma weak pmpi_cart_sub            = mpi_cart_sub_
#    pragma weak mpi_dims_create          = mpi_dims_create_	  
#    pragma weak pmpi_dims_create         = mpi_dims_create_
#    pragma weak mpi_topo_test            = mpi_topo_test_	
#    pragma weak pmpi_topo_test           = mpi_topo_test_


/* General graph topologies */
#    pragma weak mpi_graph_create         = mpi_graph_create_	
#    pragma weak pmpi_graph_create        = mpi_graph_create_
#    pragma weak mpi_graphdims_get        = mpi_graphdims_get_	
#    pragma weak pmpi_graphdims_get       = mpi_graphdims_get_
#    pragma weak mpi_graph_get            = mpi_graph_get_	
#    pragma weak pmpi_graph_get           = mpi_graph_get_
#    pragma weak mpi_graph_map            = mpi_graph_map_	
#    pragma weak pmpi_graph_map           = mpi_graph_map_
#    pragma weak mpi_graph_neighbors_count  = mpi_graph_neighbors_count_	
#    pragma weak pmpi_graph_neighbors_count = mpi_graph_neighbors_count_
#    pragma weak mpi_graph_neighbors      = mpi_graph_neighbors_	
#    pragma weak pmpi_graph_neighbors     = mpi_graph_neighbors_


/* Error handler */
#    pragma weak mpi_errhandler_set       = mpi_errhandler_set_
#    pragma weak pmpi_errhandler_set      = mpi_errhandler_set_ 
#    pragma weak mpi_error_class          = mpi_error_class_
#    pragma weak pmpi_error_class         = mpi_error_class_ 
#    pragma weak mpi_errhandler_create    = mpi_errhandler_create_  
#    pragma weak pmpi_errhandler_create   = mpi_errhandler_create_
#    pragma weak mpi_errhandler_free      = mpi_errhandler_free_	  
#    pragma weak pmpi_errhandler_free     = mpi_errhandler_free_
#    pragma weak mpi_errhandler_get       = mpi_errhandler_get_	  
#    pragma weak pmpi_errhandler_get      = mpi_errhandler_get_
#    pragma weak mpi_error_string         = mpi_error_string_	  
#    pragma weak pmpi_error_string        = mpi_error_string_

#    pragma weak mpi_add_error_string     = mpi_add_error_string_
#    pragma weak pmpi_add_error_string    = mpi_add_error_string_
#    pragma weak mpi_add_error_class      = mpi_add_error_class_
#    pragma weak pmpi_add_error_class     = mpi_add_error_class_
#    pragma weak mpi_add_error_code       = mpi_add_error_code_
#    pragma weak pmpi_add_error_code      = mpi_add_error_code_

/* Group functions */
#    pragma weak mpi_group_incl           = mpi_group_incl_
#    pragma weak pmpi_group_incl          = mpi_group_incl_ 
#    pragma weak mpi_group_free           = mpi_group_free_
#    pragma weak pmpi_group_free          = mpi_group_free_ 
#    pragma weak mpi_group_compare        = mpi_group_compare_	
#    pragma weak pmpi_group_compare       = mpi_group_compare_
#    pragma weak mpi_group_difference     = mpi_group_difference_	
#    pragma weak pmpi_group_difference    = mpi_group_difference_
#    pragma weak mpi_group_excl           = mpi_group_excl_	
#    pragma weak pmpi_group_excl          = mpi_group_excl_
#    pragma weak mpi_group_intersection   = mpi_group_intersection_	
#    pragma weak pmpi_group_intersection  = mpi_group_intersection_
#    pragma weak mpi_group_rank           = mpi_group_rank_
#    pragma weak pmpi_group_rank          = mpi_group_rank_
#    pragma weak mpi_group_size           = mpi_group_size_
#    pragma weak pmpi_group_size          = mpi_group_size_
#    pragma weak mpi_group_range_excl     = mpi_group_range_excl_	
#    pragma weak pmpi_group_range_excl    = mpi_group_range_excl_
#    pragma weak mpi_group_range_incl     = mpi_group_range_incl_	
#    pragma weak pmpi_group_range_incl    = mpi_group_range_incl_
#    pragma weak mpi_group_translate_ranks  = mpi_group_translate_ranks_	
#    pragma weak pmpi_group_translate_ranks = mpi_group_translate_ranks_
#    pragma weak mpi_group_union          = mpi_group_union_	
#    pragma weak pmpi_group_union         = mpi_group_union_


/* Op functions */
#    pragma weak  mpi_op_create           = mpi_op_create_
#    pragma weak  pmpi_op_create          = mpi_op_create_ 
#    pragma weak  mpi_op_free             = mpi_op_free_
#    pragma weak  pmpi_op_free            = mpi_op_free_                          

/* Attribute functions */
#    pragma weak mpi_attr_delete          = mpi_attr_delete_
#    pragma weak pmpi_attr_delete         = mpi_attr_delete_ 
#    pragma weak mpi_attr_get             = mpi_attr_get_	       
#    pragma weak pmpi_attr_get            = mpi_attr_get_
#    pragma weak mpi_attr_put             = mpi_attr_put_	        
#    pragma weak pmpi_attr_put            = mpi_attr_put_
#    pragma weak mpi_keyval_create        = mpi_keyval_create_	
#    pragma weak pmpi_keyval_create       = mpi_keyval_create_
#    pragma weak mpi_keyval_free          = mpi_keyval_free_	
#    pragma weak pmpi_keyval_free         = mpi_keyval_free_


/* Info-object functions 
#    pragma weak mpi_info_create          = mpi_info_create_      
#    pragma weak pmpi_info_create         = mpi_info_create_
#    pragma weak mpi_info_delete          = mpi_info_delete_	
#    pragma weak pmpi_info_delete         = mpi_info_delete_
#    pragma weak mpi_info_dup             = mpi_info_dup_	
#    pragma weak pmpi_info_dup            = mpi_info_dup_
#    pragma weak mpi_info_free            = mpi_info_free_	
#    pragma weak pmpi_info_free           = mpi_info_free_
#    pragma weak mpi_info_get             = mpi_info_get_	
#    pragma weak pmpi_info_get            = mpi_info_get_
#    pragma weak mpi_info_get_nkeys         = mpi_info_get_nkeys_	
#    pragma weak pmpi_info_get_nkeys        = mpi_info_get_nkeys_
#    pragma weak mpi_info_get_nthkey        = mpi_info_get_nthkey_	
#    pragma weak pmpi_info_get_nthkey       = mpi_info_get_nthkey_
#    pragma weak mpi_info_get_valuelen      = mpi_info_get_valuelen_	
#    pragma weak pmpi_info_get_valuelen     = mpi_info_get_valuelen_
#    pragma weak mpi_info_set             = mpi_info_set_	
#    pragma weak pmpi_info_set            = mpi_info_set_
*/
    

#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

/*   Callback functions */
#    pragma weak pmpi_null_copy_fn_        = mpi_null_copy_fn_
#    pragma weak pmpi_null_delete_fn_      = mpi_null_delete_fn_
#    pragma weak pmpi_dup_fn_              = mpi_dup_fn_
 
/*  Cartesian Topologies */
#    pragma weak pmpi_cart_create_         = mpi_cart_create_ 
#    pragma weak pmpi_cart_rank_           = mpi_cart_rank_ 
#    pragma weak pmpi_cart_coords_         = mpi_cart_coords_
#    pragma weak pmpi_cartdim_get_         = mpi_cartdim_get_
#    pragma weak pmpi_cart_get_            = mpi_cart_get_
#    pragma weak pmpi_cart_map_            = mpi_cart_map_
#    pragma weak pmpi_cart_shift_          = mpi_cart_shift_
#    pragma weak pmpi_cart_sub_            = mpi_cart_sub_
#    pragma weak pmpi_dims_create_         = mpi_dims_create_
#    pragma weak pmpi_topo_test_           = mpi_topo_test_


/* General graph topologies */
#    pragma weak pmpi_graph_create_        = mpi_graph_create_
#    pragma weak pmpi_graphdims_get_       = mpi_graphdims_get_
#    pragma weak pmpi_graph_get_           = mpi_graph_get_
#    pragma weak pmpi_graph_map_           = mpi_graph_map_
#    pragma weak pmpi_graph_neighbors_count_ = mpi_graph_neighbors_count_
#    pragma weak pmpi_graph_neighbors_     = mpi_graph_neighbors_


/* Error handler */
#    pragma weak pmpi_errhandler_set_      = mpi_errhandler_set_ 
#    pragma weak pmpi_error_class_         = mpi_error_class_ 
#    pragma weak pmpi_errhandler_create_   = mpi_errhandler_create_
#    pragma weak pmpi_errhandler_free_     = mpi_errhandler_free_
#    pragma weak pmpi_errhandler_get_      = mpi_errhandler_get_
#    pragma weak pmpi_error_string_        = mpi_error_string_

#    pragma weak pmpi_add_error_string_    = mpi_add_error_string_
#    pragma weak pmpi_add_error_class_       = mpi_add_error_class_
#    pragma weak pmpi_add_error_code_        = mpi_add_error_code_

/* Group functions */
#    pragma weak pmpi_group_incl_          = mpi_group_incl_ 
#    pragma weak pmpi_group_free_          = mpi_group_free_ 
#    pragma weak pmpi_group_compare_       = mpi_group_compare_
#    pragma weak pmpi_group_difference_    = mpi_group_difference_
#    pragma weak pmpi_group_excl_          = mpi_group_excl_
#    pragma weak pmpi_group_intersection_  = mpi_group_intersection_
#    pragma weak pmpi_group_rank_          = mpi_group_rank_
#    pragma weak pmpi_group_size_          = mpi_group_size_
#    pragma weak pmpi_group_range_excl_    = mpi_group_range_excl_
#    pragma weak pmpi_group_range_incl_    = mpi_group_range_incl_
#    pragma weak pmpi_group_translate_ranks_ = mpi_group_translate_ranks_
#    pragma weak pmpi_group_union_         = mpi_group_union_


/* Op functions */
#    pragma weak  pmpi_op_create_          = mpi_op_create_ 
#    pragma weak  pmpi_op_free_            = mpi_op_free_                          

/* Attribute functions */
#    pragma weak pmpi_attr_delete_         = mpi_attr_delete_ 
#    pragma weak pmpi_attr_get_            = mpi_attr_get_
#    pragma weak pmpi_attr_put_            = mpi_attr_put_
#    pragma weak pmpi_keyval_create_       = mpi_keyval_create_
#    pragma weak pmpi_keyval_free_         = mpi_keyval_free_


/* Info-object functions 
#    pragma weak pmpi_info_create_         = mpi_info_create_
#    pragma weak pmpi_info_delete_         = mpi_info_delete_
#    pragma weak pmpi_info_dup_            = mpi_info_dup_
#    pragma weak pmpi_info_free_           = mpi_info_free_
#    pragma weak pmpi_info_get_            = mpi_info_get_
#    pragma weak pmpi_info_get_nkeys_        = mpi_info_get_nkeys_
#    pragma weak pmpi_info_get_nthkey_       = mpi_info_get_nthkey_
#    pragma weak pmpi_info_get_valuelen_     = mpi_info_get_valuelen_
#    pragma weak pmpi_info_set_            = mpi_info_set_
*/

#  endif /*FORTRANCAPS, etc. */

/* #endif  __PRAGMA_REDEFINE_EXTNAME */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

/*   Callback functions */
#    define mpi_null_copy_fn_           PMPI_NULL_COPY_FN
#    define mpi_null_delete_fn_         PMPI_NULL_DELETE_FN
#    define mpi_dup_fn_                 PMPI_DUP_FN
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            PMPI_CART_CREATE 
#    define mpi_cart_rank_              PMPI_CART_RANK 
#    define mpi_cart_coords_            PMPI_CART_COORDS
#    define mpi_cartdim_get_            PMPI_CARTDIM_GET
#    define mpi_cart_get_               PMPI_CART_GET
#    define mpi_cart_map_               PMPI_CART_MAP
#    define mpi_cart_shift_             PMPI_CART_SHIFT
#    define mpi_cart_sub_               PMPI_CART_SUB
#    define mpi_dims_create_            PMPI_DIMS_CREATE
#    define mpi_topo_test_              PMPI_TOPO_TEST


/* General graph topologies */
#    define mpi_graph_create_           PMPI_GRAPH_CREATE
#    define mpi_graphdims_get_          PMPI_GRAPHDIMS_GET
#    define mpi_graph_get_              PMPI_GRAPH_GET
#    define mpi_graph_map_              PMPI_GRAPH_MAP
#    define mpi_graph_neighbors_count_  PMPI_GRAPH_NEIGHBORS_COUNT
#    define mpi_graph_neighbors_        PMPI_GRAPH_NEIGHBORS


/* Error handler */
#    define mpi_errhandler_set_         PMPI_ERRHANDLER_SET 
#    define mpi_error_class_            PMPI_ERROR_CLASS 
#    define mpi_errhandler_create_      PMPI_ERRHANDLER_CREATE
#    define mpi_errhandler_free_        PMPI_ERRHANDLER_FREE
#    define mpi_errhandler_get_         PMPI_ERRHANDLER_GET
#    define mpi_error_string_           PMPI_ERROR_STRING

#    define mpi_add_error_string_       PMPI_ADD_ERROR_STRING
#    define mpi_add_error_class_        PMPI_ADD_ERROR_CLASS
#    define mpi_add_error_code_         PMPI_ADD_ERROR_CODE

/* Group functions */
#    define mpi_group_incl_             PMPI_GROUP_INCL 
#    define mpi_group_free_             PMPI_GROUP_FREE 
#    define mpi_group_compare_          PMPI_GROUP_COMPARE
#    define mpi_group_difference_       PMPI_GROUP_DIFFERENCE
#    define mpi_group_excl_             PMPI_GROUP_EXCL
#    define mpi_group_intersection_     PMPI_GROUP_INTERSECTION
#    define mpi_group_rank_             PMPI_GROUP_RANK
#    define mpi_group_size_             PMPI_GROUP_SIZE
#    define mpi_group_range_excl_       PMPI_GROUP_RANGE_EXCL
#    define mpi_group_range_incl_       PMPI_GROUP_RANGE_INCL
#    define mpi_group_translate_ranks_  PMPI_GROUP_TRANSLATE_RANKS
#    define mpi_group_union_            PMPI_GROUP_UNION


/* Op functions */
#    define  mpi_op_create_             PMPI_OP_CREATE 
#    define  mpi_op_free_               PMPI_OP_FREE                          

/* Attribute functions */
#    define mpi_attr_delete_            PMPI_ATTR_DELETE 
#    define mpi_attr_get_               PMPI_ATTR_GET
#    define mpi_attr_put_               PMPI_ATTR_PUT
#    define mpi_keyval_create_          PMPI_KEYVAL_CREATE
#    define mpi_keyval_free_            PMPI_KEYVAL_FREE


/* Info-object functions 
#    define mpi_info_create_            PMPI_INFO_CREATE
#    define mpi_info_delete_            PMPI_INFO_DELETE
#    define mpi_info_dup_               PMPI_INFO_DUP
#    define mpi_info_free_              PMPI_INFO_FREE
#    define mpi_info_get_               PMPI_INFO_GET
#    define mpi_info_get_nkeys_         PMPI_INFO_GET_NKEYS
#    define mpi_info_get_nthkey_        PMPI_INFO_GET_NTHKEY
#    define mpi_info_get_valuelen_      PMPI_INFO_GET_VALUELEN
#    define mpi_info_set_               PMPI_INFO_SET
*/

#    elif defined(FORTRANDOUBLEUNDERSCORE)

/*   Callback functions */
#    define mpi_null_copy_fn_           pmpi_null_copy_fn__
#    define mpi_null_delete_fn_         pmpi_null_delete_fn__
#    define mpi_dup_fn_                 pmpi_dup_fn__
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            pmpi_cart_create__ 
#    define mpi_cart_rank_              pmpi_cart_rank__ 
#    define mpi_cart_coords_            pmpi_cart_coords__
#    define mpi_cartdim_get_            pmpi_cartdim_get__
#    define mpi_cart_get_               pmpi_cart_get__
#    define mpi_cart_map_               pmpi_cart_map__
#    define mpi_cart_shift_             pmpi_cart_shift__
#    define mpi_cart_sub_               pmpi_cart_sub__
#    define mpi_dims_create_            pmpi_dims_create__
#    define mpi_topo_test_              pmpi_topo_test__


/* General graph topologies */
#    define mpi_graph_create_           pmpi_graph_create__
#    define mpi_graphdims_get_          pmpi_graphdims_get__
#    define mpi_graph_get_              pmpi_graph_get__
#    define mpi_graph_map_              pmpi_graph_map__
#    define mpi_graph_neighbors_count_  pmpi_graph_neighbors_count__
#    define mpi_graph_neighbors_        pmpi_graph_neighbors__


/* Error handler */
#    define mpi_errhandler_set_         pmpi_errhandler_set__ 
#    define mpi_error_class_            pmpi_error_class__ 
#    define mpi_errhandler_create_      pmpi_errhandler_create__
#    define mpi_errhandler_free_        pmpi_errhandler_free__
#    define mpi_errhandler_get_         pmpi_errhandler_get__
#    define mpi_error_string_           pmpi_error_string__

#    define mpi_add_error_string_       pmpi_add_error_string__
#    define mpi_add_error_class_        pmpi_add_error_class__
#    define mpi_add_error_code_         pmpi_add_error_code__

/* Group functions */
#    define mpi_group_incl_             pmpi_group_incl__ 
#    define mpi_group_free_             pmpi_group_free__ 
#    define mpi_group_compare_          pmpi_group_compare__
#    define mpi_group_difference_       pmpi_group_difference__
#    define mpi_group_excl_             pmpi_group_excl__
#    define mpi_group_intersection_     pmpi_group_intersection__
#    define mpi_group_rank_             pmpi_group_rank__
#    define mpi_group_size_             pmpi_group_size__
#    define mpi_group_range_excl_       pmpi_group_range_excl__
#    define mpi_group_range_incl_       pmpi_group_range_incl__
#    define mpi_group_translate_ranks_  pmpi_group_translate_ranks__
#    define mpi_group_union_            pmpi_group_union__


/* Op functions */
#    define mpi_op_create_              pmpi_op_create__ 
#    define mpi_op_free_                pmpi_op_free__                          

/* Attribute functions */
#    define mpi_attr_delete_            pmpi_attr_delete__ 
#    define mpi_attr_get_               pmpi_attr_get__
#    define mpi_attr_put_               pmpi_attr_put__
#    define mpi_keyval_create_          pmpi_keyval_create__
#    define mpi_keyval_free_            pmpi_keyval_free__


/* Info-object functions 
#    define mpi_info_create_            pmpi_info_create__
#    define mpi_info_delete_            pmpi_info_delete__
#    define mpi_info_dup_               pmpi_info_dup__
#    define mpi_info_free_              pmpi_info_free__
#    define mpi_info_get_               pmpi_info_get__
#    define mpi_info_get_nkeys_         pmpi_info_get_nkeys__
#    define mpi_info_get_nthkey_        pmpi_info_get_nthkey__
#    define mpi_info_get_valuelen_      pmpi_info_get_valuelen__
#    define mpi_info_set_               pmpi_info_set__
*/


#    elif defined(FORTRANNOUNDERSCORE)

/*   Callback functions */
#    define mpi_null_copy_fn_           pmpi_null_copy_fn
#    define mpi_null_delete_fn_         pmpi_null_delete_fn
#    define mpi_dup_fn_                 pmpi_dup_fn
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            pmpi_cart_create 
#    define mpi_cart_rank_              pmpi_cart_rank 
#    define mpi_cart_coords_            pmpi_cart_coords
#    define mpi_cartdim_get_            pmpi_cartdim_get
#    define mpi_cart_get_               pmpi_cart_get
#    define mpi_cart_map_               pmpi_cart_map
#    define mpi_cart_shift_             pmpi_cart_shift
#    define mpi_cart_sub_               pmpi_cart_sub
#    define mpi_dims_create_            pmpi_dims_create
#    define mpi_topo_test_              pmpi_topo_test


/* General graph topologies */
#    define mpi_graph_create_           pmpi_graph_create
#    define mpi_graphdims_get_          pmpi_graphdims_get
#    define mpi_graph_get_              pmpi_graph_get
#    define mpi_graph_map_              pmpi_graph_map
#    define mpi_graph_neighbors_count_  pmpi_graph_neighbors_count
#    define mpi_graph_neighbors_        pmpi_graph_neighbors


/* Error handler */
#    define mpi_errhandler_set_         pmpi_errhandler_set 
#    define mpi_error_class_            pmpi_error_class 
#    define mpi_errhandler_create_      pmpi_errhandler_create
#    define mpi_errhandler_free_        pmpi_errhandler_free
#    define mpi_errhandler_get_         pmpi_errhandler_get
#    define mpi_error_string_           pmpi_error_string

#    define mpi_add_error_string_       pmpi_add_error_string
#    define mpi_add_error_class_        pmpi_add_error_class
#    define mpi_add_error_code_         pmpi_add_error_code

/* Group functions */
#    define mpi_group_incl_             pmpi_group_incl 
#    define mpi_group_free_             pmpi_group_free 
#    define mpi_group_compare_          pmpi_group_compare
#    define mpi_group_difference_       pmpi_group_difference
#    define mpi_group_excl_             pmpi_group_excl
#    define mpi_group_intersection_     pmpi_group_intersection
#    define mpi_group_rank_             pmpi_group_rank
#    define mpi_group_size_             pmpi_group_size
#    define mpi_group_range_excl_       pmpi_group_range_excl
#    define mpi_group_range_incl_       pmpi_group_range_incl
#    define mpi_group_translate_ranks_  pmpi_group_translate_ranks
#    define mpi_group_union_            pmpi_group_union


/* Op functions */
#    define mpi_op_create_              pmpi_op_create 
#    define mpi_op_free_                pmpi_op_free                          

/* Attribute functions */
#    define mpi_attr_delete_            pmpi_attr_delete 
#    define mpi_attr_get_               pmpi_attr_get
#    define mpi_attr_put_               pmpi_attr_put
#    define mpi_keyval_create_          pmpi_keyval_create
#    define mpi_keyval_free_            pmpi_keyval_free


/* Info-object functions 
#    define mpi_info_create_            pmpi_info_create
#    define mpi_info_delete_            pmpi_info_delete
#    define mpi_info_dup_               pmpi_info_dup
#    define mpi_info_free_              pmpi_info_free
#    define mpi_info_get_               pmpi_info_get
#    define mpi_info_get_nkeys_         pmpi_info_get_nkeys
#    define mpi_info_get_nthkey_        pmpi_info_get_nthkey
#    define mpi_info_get_valuelen_      pmpi_info_get_valuelen
#    define mpi_info_set_               pmpi_info_set
*/

#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

/*   Callback functions */
#    define mpi_null_copy_fn_           pmpi_null_copy_fn_
#    define mpi_null_delete_fn_         pmpi_null_delete_fn_
#    define mpi_dup_fn_                 pmpi_dup_fn_
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            pmpi_cart_create_ 
#    define mpi_cart_rank_              pmpi_cart_rank_ 
#    define mpi_cart_coords_            pmpi_cart_coords_
#    define mpi_cartdim_get_            pmpi_cartdim_get_
#    define mpi_cart_get_               pmpi_cart_get_
#    define mpi_cart_map_               pmpi_cart_map_
#    define mpi_cart_shift_             pmpi_cart_shift_
#    define mpi_cart_sub_               pmpi_cart_sub_
#    define mpi_dims_create_            pmpi_dims_create_
#    define mpi_topo_test_              pmpi_topo_test_


/* General graph topologies */
#    define mpi_graph_create_           pmpi_graph_create_
#    define mpi_graphdims_get_          pmpi_graphdims_get_
#    define mpi_graph_get_              pmpi_graph_get_
#    define mpi_graph_map_              pmpi_graph_map_
#    define mpi_graph_neighbors_count_  pmpi_graph_neighbors_count_
#    define mpi_graph_neighbors_        pmpi_graph_neighbors_


/* Error handler */
#    define mpi_errhandler_set_         pmpi_errhandler_set_ 
#    define mpi_error_class_            pmpi_error_class_ 
#    define mpi_errhandler_create_      pmpi_errhandler_create_
#    define mpi_errhandler_free_        pmpi_errhandler_free_
#    define mpi_errhandler_get_         pmpi_errhandler_get_
#    define mpi_error_string_           pmpi_error_string_

#    define mpi_add_error_string_       pmpi_add_error_string_
#    define mpi_add_error_class_        pmpi_add_error_class_
#    define mpi_add_error_code_         pmpi_add_error_code_

/* Group functions */
#    define mpi_group_incl_             pmpi_group_incl_ 
#    define mpi_group_free_             pmpi_group_free_ 
#    define mpi_group_compare_          pmpi_group_compare_
#    define mpi_group_difference_       pmpi_group_difference_
#    define mpi_group_excl_             pmpi_group_excl_
#    define mpi_group_intersection_     pmpi_group_intersection_
#    define mpi_group_rank_             pmpi_group_rank_
#    define mpi_group_size_             pmpi_group_size_
#    define mpi_group_range_excl_       pmpi_group_range_excl_
#    define mpi_group_range_incl_       pmpi_group_range_incl_
#    define mpi_group_translate_ranks_  pmpi_group_translate_ranks_
#    define mpi_group_union_            pmpi_group_union_


/* Op functions */
#    define mpi_op_create_              pmpi_op_create_ 
#    define mpi_op_free_                pmpi_op_free_                          

/* Attribute functions */
#    define mpi_attr_delete_            pmpi_attr_delete_ 
#    define mpi_attr_get_               pmpi_attr_get_
#    define mpi_attr_put_               pmpi_attr_put_
#    define mpi_keyval_create_          pmpi_keyval_create_
#    define mpi_keyval_free_            pmpi_keyval_free_


/* Info-object functions 
#    define mpi_info_create_            pmpi_info_create_
#    define mpi_info_delete_            pmpi_info_delete_
#    define mpi_info_dup_               pmpi_info_dup_
#    define mpi_info_free_              pmpi_info_free_
#    define mpi_info_get_               pmpi_info_get_
#    define mpi_info_get_nkeys_         pmpi_info_get_nkeys_
#    define mpi_info_get_nthkey_        pmpi_info_get_nthkey_
#    define mpi_info_get_valuelen_      pmpi_info_get_valuelen_
#    define mpi_info_set_               pmpi_info_set_
*/

#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

/*   Callback functions */
#    define mpi_null_copy_fn_           MPI_NULL_COPY_FN
#    define mpi_null_delete_fn_         MPI_NULL_DELETE_FN
#    define mpi_dup_fn_                 MPI_DUP_FN
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            MPI_CART_CREATE 
#    define mpi_cart_rank_              MPI_CART_RANK 
#    define mpi_cart_coords_            MPI_CART_COORDS
#    define mpi_cartdim_get_            MPI_CARTDIM_GET
#    define mpi_cart_get_               MPI_CART_GET
#    define mpi_cart_map_               MPI_CART_MAP
#    define mpi_cart_shift_             MPI_CART_SHIFT
#    define mpi_cart_sub_               MPI_CART_SUB
#    define mpi_dims_create_            MPI_DIMS_CREATE
#    define mpi_topo_test_              MPI_TOPO_TEST


/* General graph topologies */
#    define mpi_graph_create_           MPI_GRAPH_CREATE
#    define mpi_graphdims_get_          MPI_GRAPHDIMS_GET
#    define mpi_graph_get_              MPI_GRAPH_GET
#    define mpi_graph_map_              MPI_GRAPH_MAP
#    define mpi_graph_neighbors_count_  MPI_GRAPH_NEIGHBORS_COUNT
#    define mpi_graph_neighbors_        MPI_GRAPH_NEIGHBORS


/* Error handler */
#    define mpi_errhandler_set_         MPI_ERRHANDLER_SET 
#    define mpi_error_class_            MPI_ERROR_CLASS 
#    define mpi_errhandler_create_      MPI_ERRHANDLER_CREATE
#    define mpi_errhandler_free_        MPI_ERRHANDLER_FREE
#    define mpi_errhandler_get_         MPI_ERRHANDLER_GET
#    define mpi_error_string_           MPI_ERROR_STRING

#    define mpi_add_error_string_       MPI_ADD_ERROR_STRING
#    define mpi_add_error_class_        MPI_ADD_ERROR_CLASS
#    define mpi_add_error_code_         MPI_ADD_ERROR_CODE

/* Group functions */
#    define mpi_group_incl_             MPI_GROUP_INCL 
#    define mpi_group_free_             MPI_GROUP_FREE 
#    define mpi_group_compare_          MPI_GROUP_COMPARE
#    define mpi_group_difference_       MPI_GROUP_DIFFERENCE
#    define mpi_group_excl_             MPI_GROUP_EXCL
#    define mpi_group_intersection_     MPI_GROUP_INTERSECTION
#    define mpi_group_rank_             MPI_GROUP_RANK
#    define mpi_group_size_             MPI_GROUP_SIZE
#    define mpi_group_range_excl_       MPI_GROUP_RANGE_EXCL
#    define mpi_group_range_incl_       MPI_GROUP_RANGE_INCL
#    define mpi_group_translate_ranks_  MPI_GROUP_TRANSLATE_RANKS
#    define mpi_group_union_            MPI_GROUP_UNION


/* Op functions */
#    define  mpi_op_create_             MPI_OP_CREATE 
#    define  mpi_op_free_               MPI_OP_FREE                          

/* Attribute functions */
#    define mpi_attr_delete_            MPI_ATTR_DELETE 
#    define mpi_attr_get_               MPI_ATTR_GET
#    define mpi_attr_put_               MPI_ATTR_PUT
#    define mpi_keyval_create_          MPI_KEYVAL_CREATE
#    define mpi_keyval_free_            MPI_KEYVAL_FREE


/* Info-object functions 
#    define mpi_info_create_            MPI_INFO_CREATE
#    define mpi_info_delete_            MPI_INFO_DELETE
#    define mpi_info_dup_               MPI_INFO_DUP
#    define mpi_info_free_              MPI_INFO_FREE
#    define mpi_info_get_               MPI_INFO_GET
#    define mpi_info_get_nkeys_         MPI_INFO_GET_NKEYS
#    define mpi_info_get_nthkey_        MPI_INFO_GET_NTHKEY
#    define mpi_info_get_valuelen_      MPI_INFO_GET_VALUELEN
#    define mpi_info_set_               MPI_INFO_SET
*/


#    elif defined(FORTRANDOUBLEUNDERSCORE)


/*   Callback functions */
#    define mpi_null_copy_fn_           mpi_null_copy_fn__
#    define mpi_null_delete_fn_         mpi_null_delete_fn__
#    define mpi_dup_fn_                 mpi_dup_fn__
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            mpi_cart_create__ 
#    define mpi_cart_rank_              mpi_cart_rank__ 
#    define mpi_cart_coords_            mpi_cart_coords__
#    define mpi_cartdim_get_            mpi_cartdim_get__
#    define mpi_cart_get_               mpi_cart_get__
#    define mpi_cart_map_               mpi_cart_map__
#    define mpi_cart_shift_             mpi_cart_shift__
#    define mpi_cart_sub_               mpi_cart_sub__
#    define mpi_dims_create_            mpi_dims_create__
#    define mpi_topo_test_              mpi_topo_test__


/* General graph topologies */
#    define mpi_graph_create_           mpi_graph_create__
#    define mpi_graphdims_get_          mpi_graphdims_get__
#    define mpi_graph_get_              mpi_graph_get__
#    define mpi_graph_map_              mpi_graph_map__
#    define mpi_graph_neighbors_count_  mpi_graph_neighbors_count__
#    define mpi_graph_neighbors_        mpi_graph_neighbors__


/* Error handler */
#    define mpi_errhandler_set_         mpi_errhandler_set__ 
#    define mpi_error_class_            mpi_error_class__ 
#    define mpi_errhandler_create_      mpi_errhandler_create__
#    define mpi_errhandler_free_        mpi_errhandler_free__
#    define mpi_errhandler_get_         mpi_errhandler_get__
#    define mpi_error_string_           mpi_error_string__

#    define mpi_add_error_string_       mpi_add_error_string__
#    define mpi_add_error_class_        mpi_add_error_class__
#    define mpi_add_error_code_         mpi_add_error_code__

/* Group functions */
#    define mpi_group_incl_             mpi_group_incl__ 
#    define mpi_group_free_             mpi_group_free__ 
#    define mpi_group_compare_          mpi_group_compare__
#    define mpi_group_difference_       mpi_group_difference__
#    define mpi_group_excl_             mpi_group_excl__
#    define mpi_group_intersection_     mpi_group_intersection__
#    define mpi_group_rank_             mpi_group_rank__
#    define mpi_group_size_             mpi_group_size__
#    define mpi_group_range_excl_       mpi_group_range_excl__
#    define mpi_group_range_incl_       mpi_group_range_incl__
#    define mpi_group_translate_ranks_  mpi_group_translate_ranks__
#    define mpi_group_union_            mpi_group_union__


/* Op functions */
#    define mpi_op_create_              mpi_op_create__ 
#    define mpi_op_free_                mpi_op_free__                          

/* Attribute functions */
#    define mpi_attr_delete_            mpi_attr_delete__ 
#    define mpi_attr_get_               mpi_attr_get__
#    define mpi_attr_put_               mpi_attr_put__
#    define mpi_keyval_create_          mpi_keyval_create__
#    define mpi_keyval_free_            mpi_keyval_free__


/* Info-object functions 
#    define mpi_info_create_            mpi_info_create__
#    define mpi_info_delete_            mpi_info_delete__
#    define mpi_info_dup_               mpi_info_dup__
#    define mpi_info_free_              mpi_info_free__
#    define mpi_info_get_               mpi_info_get__
#    define mpi_info_get_nkeys_         mpi_info_get_nkeys__
#    define mpi_info_get_nthkey_        mpi_info_get_nthkey__
#    define mpi_info_get_valuelen_      mpi_info_get_valuelen__
#    define mpi_info_set_               mpi_info_set__
*/

#    elif defined(FORTRANNOUNDERSCORE)

/*   Callback functions */
#    define mpi_null_copy_fn_           mpi_null_copy_fn
#    define mpi_null_delete_fn_         mpi_null_delete_fn
#    define mpi_dup_fn_                 mpi_dup_fn
 
/*  Cartesian Topologies */
#    define mpi_cart_create_            mpi_cart_create 
#    define mpi_cart_rank_              mpi_cart_rank 
#    define mpi_cart_coords_            mpi_cart_coords
#    define mpi_cartdim_get_            mpi_cartdim_get
#    define mpi_cart_get_               mpi_cart_get
#    define mpi_cart_map_               mpi_cart_map
#    define mpi_cart_shift_             mpi_cart_shift
#    define mpi_cart_sub_               mpi_cart_sub
#    define mpi_dims_create_            mpi_dims_create
#    define mpi_topo_test_              mpi_topo_test


/* General graph topologies */
#    define mpi_graph_create_           mpi_graph_create
#    define mpi_graphdims_get_          mpi_graphdims_get
#    define mpi_graph_get_              mpi_graph_get
#    define mpi_graph_map_              mpi_graph_map
#    define mpi_graph_neighbors_count_  mpi_graph_neighbors_count
#    define mpi_graph_neighbors_        mpi_graph_neighbors


/* Error handler */
#    define mpi_errhandler_set_         mpi_errhandler_set 
#    define mpi_error_class_            mpi_error_class 
#    define mpi_errhandler_create_      mpi_errhandler_create
#    define mpi_errhandler_free_        mpi_errhandler_free
#    define mpi_errhandler_get_         mpi_errhandler_get
#    define mpi_error_string_           mpi_error_string

#    define mpi_add_error_string_       mpi_add_error_string
#    define mpi_add_error_class_        mpi_add_error_class
#    define mpi_add_error_code_         mpi_add_error_code

/* Group functions */
#    define mpi_group_incl_             mpi_group_incl 
#    define mpi_group_free_             mpi_group_free 
#    define mpi_group_compare_          mpi_group_compare
#    define mpi_group_difference_       mpi_group_difference
#    define mpi_group_excl_             mpi_group_excl
#    define mpi_group_intersection_     mpi_group_intersection
#    define mpi_group_rank_             mpi_group_rank
#    define mpi_group_size_             mpi_group_size
#    define mpi_group_range_excl_       mpi_group_range_excl
#    define mpi_group_range_incl_       mpi_group_range_incl
#    define mpi_group_translate_ranks_  mpi_group_translate_ranks
#    define mpi_group_union_            mpi_group_union


/* Op functions */
#    define mpi_op_create_              mpi_op_create 
#    define mpi_op_free_                mpi_op_free                          

/* Attribute functions */
#    define mpi_attr_delete_            mpi_attr_delete 
#    define mpi_attr_get_               mpi_attr_get
#    define mpi_attr_put_               mpi_attr_put
#    define mpi_keyval_create_          mpi_keyval_create
#    define mpi_keyval_free_            mpi_keyval_free


/* Info-object functions 
#    define mpi_info_create_            mpi_info_create
#    define mpi_info_delete_            mpi_info_delete
#    define mpi_info_dup_               mpi_info_dup
#    define mpi_info_free_              mpi_info_free
#    define mpi_info_get_               mpi_info_get
#    define mpi_info_get_nkeys_         mpi_info_get_nkeys
#    define mpi_info_get_nthkey_        mpi_info_get_nthkey
#    define mpi_info_get_valuelen_      mpi_info_get_valuelen
#    define mpi_info_set_               mpi_info_set
*/

#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */

#endif

