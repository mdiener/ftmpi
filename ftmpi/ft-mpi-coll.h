/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors:	
	  Edgar Gabriel <gabriel@cs.utk.edu>
          Graham E Fagg <fagg@cs.utk.edu>
	  Antonin Bukovsky <tone@cs.utk.edu>
	  Jeremy Millar <millar@cs.utk.edu>


  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

#ifndef __FT_MPI_COLL_H__
#define __FT_MPI_COLL_H__

/* constants */
#define BARRIER_LINEAR    1000

#define BCAST_LINEAR      2000
#define BCAST_BMTREE      2001

#define REDUCE_LINEAR     3000
#define REDUCE_BMTREE     3001

#define ALLREDUCE_LINEAR  4000
#define ALLREDUCE_BMTREE  4001

#define ALLGATHER_LINEAR  5000
#define ALLGATHER_BMTREE  5001

#define ALLGATHERV_LINEAR 6000
#define ALLGATHERV_BMTREE 6001

#define ALLTOALL_LINEAR 7000

#define ALLTOALLV_LINEAR 8000

#define GATHER_LINEAR 9000

#define GATHERV_LINEAR 10000

#define SCATTER_LINEAR 11000

#define SCATTERV_LINEAR 12000

#define SCAN_LINEAR 13000

#define REDUCESCATTER_LINEAR 14000
#define REDUCESCATTER_BMTREE 14001


/* prototypes for the decision routines */
int ftmpi_coll_intra_barrier_dec ( MPI_Comm comm );
int ftmpi_coll_intra_bcast_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				int root,  int *segsize );
int ftmpi_coll_intra_reduce_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				 int root, MPI_Op op, int *segsize );
int ftmpi_coll_intra_allreduce_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt, 
				    MPI_Op op, int *segsize );
int ftmpi_coll_intra_allgather_dec ( MPI_Comm comm, int scnt,MPI_Datatype sddt,
				    int rcnt, MPI_Datatype rcddt,int *segsize);

int ftmpi_coll_intra_allgatherv_dec ( MPI_Comm comm,int scnt,MPI_Datatype sddt,
				     int *rcnts, MPI_Datatype rcddt,
				     int *segsize );
int ftmpi_coll_intra_alltoall_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				   int rcnt, MPI_Datatype rcddt, int *segsize);

int ftmpi_coll_intra_alltoallv_dec (MPI_Comm comm,int *scnt,MPI_Datatype sddt,
				   int *rcnts, MPI_Datatype rcddt,
				   int *segsize );
int ftmpi_coll_intra_gather_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				 int rcnt, MPI_Datatype rddt, int root,
				 int *segsize );
int ftmpi_coll_intra_gatherv_dec (MPI_Comm comm, int scnt, MPI_Datatype sddt,
				 int *rcnt, MPI_Datatype rddt, int root,
				 int *segsize );
int ftmpi_coll_intra_reducescatter_dec ( MPI_Comm comm, int *counts, 
					MPI_Datatype ddt, MPI_Op op, 
					 int *segsize );
int ftmpi_coll_intra_scatter_dec ( MPI_Comm comm, int scnt, MPI_Datatype sddt,
				  int rcnt, MPI_Datatype rddt, int root,
				  int *segsize );
int ftmpi_coll_intra_scatterv_dec (MPI_Comm comm, int *scnt, MPI_Datatype sddt,
				  int rcnt, MPI_Datatype rddt, int root,
				  int *segsize );
int ftmpi_coll_intra_scan_dec ( MPI_Comm comm, int cnt, MPI_Datatype ddt,
			       int *segsize );



/* prototypes for the implemented algorithms */

int ftmpi_coll_intra_barrier_linear ( MPI_Comm comm );

int ftmpi_coll_intra_bcast_linear ( void* buf, int cnt, MPI_Datatype ddt, 
				   int root, MPI_Comm comm, int segsize );
int ftmpi_coll_intra_bcast_bmtree ( void* buf, int cnt, MPI_Datatype ddt, 
				   int root, MPI_Comm comm, int segsize );


int ftmpi_coll_intra_reduce_linear (void *sbuf,void *rbuf,int count,
				   MPI_Datatype datatype, MPI_Op op,
				   int root, MPI_Comm comm, int segsize);
int ftmpi_coll_intra_reduce_bmtree (void *sbuf,void *rbuf,int count,
				   MPI_Datatype datatype, MPI_Op op,
				   int root, MPI_Comm comm, int segsize);

int ftmpi_coll_intra_allreduce_linear(void *sbuf,void *rbuf,int count,
				     MPI_Datatype ddt, MPI_Op op,
				     MPI_Comm comm, int segsize);
int ftmpi_coll_intra_allreduce_bmtree(void *sbuf,void *rbuf,int count,
				     MPI_Datatype ddt, MPI_Op op,
				     MPI_Comm comm, int segsize);


int ftmpi_coll_intra_allgather_linear (void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf,int rcnt,MPI_Datatype rddt,
				      MPI_Comm comm, int segsize);
int ftmpi_coll_intra_allgather_bmtree (void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf,int rcnt,MPI_Datatype rddt,
				      MPI_Comm comm, int segsize);

int ftmpi_coll_intra_allgatherv_linear(void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf, int *rcnts, int *displs,
				      MPI_Datatype rddt, MPI_Comm comm, 
				      int segsize);
int ftmpi_coll_intra_allgatherv_bmtree(void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf, int *rcnts, int *displs,
				      MPI_Datatype rddt, MPI_Comm comm, 
				      int segsize);

int ftmpi_coll_intra_alltoall_linear(void* sbuf,int scnt,MPI_Datatype sddt,
				    void* rbuf,int rcnt,MPI_Datatype rddt,
				    MPI_Comm comm, int segsize);
int ftmpi_coll_intra_alltoallv_linear(void* sbuf,int *scnt,int *sdisp,
				     MPI_Datatype sddt, void* rbuf,int *rcnt,
				     int *rdisp,MPI_Datatype rddt, 
				     MPI_Comm comm, int segsize);
int ftmpi_coll_intra_gather_linear (void *sbuf, int scnt, MPI_Datatype sddt, 
				   void *rbuf, int rcnt, MPI_Datatype rddt, 
				   int root, MPI_Comm comm, int segsize);
int ftmpi_coll_intra_gatherv_linear (void *sbuf,int scnt, MPI_Datatype sddt,
				    void *rbuf, int *rcnt, int *rdisp, 
				    MPI_Datatype rddt,int root,
				    MPI_Comm comm, int segsize);

int ftmpi_coll_intra_reducescatter_linear(void * sbuf,void * rbuf,int * counts,
					 MPI_Datatype ddt,MPI_Op op, 
					 MPI_Comm comm, int segsize);
int ftmpi_coll_intra_reducescatter_bmtree(void * sbuf,void * rbuf,int * counts,
					 MPI_Datatype ddt,MPI_Op op, 
					 MPI_Comm comm, int segsize);


int ftmpi_coll_intra_scatter_linear (void *sbuf,int scnt,MPI_Datatype sddt,
				    void *rbuf,	int rcnt,MPI_Datatype rddt,
				    int root,MPI_Comm comm, int segsize);
int ftmpi_coll_intra_scatterv_linear (void *sbuf,int * scnt,int * sdisp,
				     MPI_Datatype sddt, void *rbuf,int rcnt,
				     MPI_Datatype rddt,int root, 
				     MPI_Comm comm, int segsize);
int ftmpi_coll_intra_scan_linear (void * sbuf,void * rbuf,int cnt,
				 MPI_Datatype ddt, MPI_Op op,
				 MPI_Comm comm, int segsize);



#endif
