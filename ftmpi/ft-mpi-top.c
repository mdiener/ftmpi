
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS internal topology code for collective operations

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>
			George Bosilca <bosilca@cs.utk.edu>
 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/





#include <stdio.h>
#include <math.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-top.h"
#include "ft-mpi-com.h"

extern comm_info_t _ftmpi_comm[MAXCOMS];

/* globals */
static int proc_ranks[MAXPERAPP]; /* a list of valid procs via rank */
static int proc_rank_sizes;	/* how many of them */
static int proc_rank_me;		/* my offset in this table */

static int pow2 ();
static int calc_i ();
static void calc_l_p ();

int top_info_list (MPI_Comm com)
{
  int i;
  int r;


  r = _ftmpi_comm[com].my_rank;

  printf("[%d] ring [%d,%d] tree[%d][", r, _ftmpi_comm[com].ring_prev, 
	 _ftmpi_comm[com].ring_next, _ftmpi_comm[com].tree_prev);
  for(i=0;i<MAXTREEFANOUT;i++)  
    if ((_ftmpi_comm[com].tree_next[i]!=r)&&(_ftmpi_comm[com].tree_next[i]!=-1))
      printf("%d ", _ftmpi_comm[com].tree_next[i]);
  printf("]\n");
  printf("binomial tree[%d][", _ftmpi_comm[com].bmtree_prev);
  for(i=0;i<MAXTREEFANOUT;i++)
    if ((_ftmpi_comm[com].bmtree_next[i]!=r)&&(_ftmpi_comm[com].bmtree_next[i]!=-1))
      printf("%d ", _ftmpi_comm[com].tree_next[i]);
  printf("]\n");
  
  fflush(stdout);
  
  return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int top_build_list (MPI_Comm com)
{
  int i, j;
  int poss_size;
  int cnt=0;
  int r;
  
  proc_rank_me = -1;	/* default if I am not valid! */
  poss_size = _ftmpi_comm[com].maxsize;
  r = _ftmpi_comm[com].my_rank;
  
  for(i=0;i<poss_size;i++) {
    /*    if (_ftmpi_comm[com].proc_ft_states[i]==FT_PROC_OK) { */
      if (i==r) proc_rank_me = cnt;
      proc_ranks[cnt] = i;
      cnt++;
    }
  /*   } */
  
  for(j=cnt;j<MAXPERAPP;j++) proc_ranks[j]=-1;	/* blank out the rest */
  
  if (cnt==0) {
    fprintf(stderr,"FT-MPI:topology build list of valid ranks returns zero...?\n");
    fflush(stderr);
  }
  
  proc_rank_sizes = cnt;
  
  return (cnt);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void tester () {
/* main () { */

  int n;
  int p;
  int x1, x2;
  int x; 	/* rank or x */
  int y;
  int size; /* size of column n */
  
  for(n=0;n<4;n++) {
    size = pow2(n);
    for (p=0;p<size;p++) {
      
      x = size + p -1;	/* -1 as size starts off as 1 not 0 */
      
      x = calc_i (n,p);
      x1 = calc_i (n+1,p*2);
      x2 = calc_i (n+1,(p*2)+1);
      
      printf("rank %d (%d,%d) next is x1 %d and x2 %d\n",
	     x, n, p, x1, x2);
      
    }
  }
  
  for(x=0;x<31;x++) {
    calc_l_p (x, &n, &p);
    y = calc_i (n, p);
    printf("rank %d -> (%d,%d) -> %d\n", x, n, p, y);
  }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
static int pow2 (int i) 
{
  int j, p;

  p = 1;
  for (j=0;j<i;j++) 
    p *= 2;
  return (p);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Calculate a location in a table according to location in a binary tree */
static int calc_i (int l, int p)
{
  return (pow2(l)+p-1);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
static void calc_l_p (int i, int *l, int *p)
{
  int ll;
  int previous_max;

  previous_max = 0;
  for(ll=0;i>=(previous_max+pow2(ll));previous_max+=pow2(ll),ll++)
    /* 	{  */
    /* 		printf("ll %d, previous %d new max %d i %d\n",  */
    /* 				ll, 	previous_max, previous_max+pow2(ll), i ); */
    /* 	} */
    { } ;
  
  *l = ll;

  /* so the position in that layer is just i - (2^l) -1 */
  *p = i - (pow2(ll) -1);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int top_build_ring (MPI_Comm com)
{
  int left, right;
  int p;
  
  /* Set defaults.. i.e. no valid previous next */
  /* i.e. ourselves */
  
  p = proc_rank_me;
  
  _ftmpi_comm[com].ring_prev = proc_ranks[p];
  _ftmpi_comm[com].ring_next = proc_ranks[p];
  
  if (proc_rank_sizes<2) return (0);	/* none or singleton check */ 
  
  left = p -1; if (left<0) left = proc_rank_sizes-1;
  right = (p+1)%proc_rank_sizes;
  _ftmpi_comm[com].ring_prev = proc_ranks[left];
  _ftmpi_comm[com].ring_next = proc_ranks[right];
  
  return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int top_build_tree (int fanout, MPI_Comm com)
{
  int p; /* location of my rank in the proc_rank_list */
  int level, pos; /* of me in the tree structure */
  int n; /* location of next */
  int i;

  /* Set defaults.. i.e. no valid previous next */
  /* i.e. ourselves */
  
  p = proc_rank_me;

  _ftmpi_comm[com].tree_prev = proc_ranks[p];
  for(i=0;i<MAXTREEFANOUT;i++) _ftmpi_comm[com].tree_next[i] = proc_ranks[p];
  
  if (proc_rank_sizes<2) return (0);	/* none or singleton check */ 
  
  calc_l_p (p, &level, &pos);	/* find my position in the tree */

  /* calc previous first */
  
  if (level!=0) 
    _ftmpi_comm[com].tree_prev = proc_ranks[ calc_i (level-1, pos/fanout)];
  
  /* calc next */
  
  for(i=0;i<fanout;i++) {
    n = calc_i ( level+1, (pos*fanout)+i );
    if (n<= proc_rank_sizes) /* we have a tree node within ranks */
      _ftmpi_comm[com].tree_next[i] = proc_ranks [n];
  }
  
  return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
#ifdef FOO
int top_build_bmtree (MPI_Comm com)
{
  int p; /* location of my rank in the proc_rank_list */
  int level, pos; /* of me in the tree structure */
  int n; /* location of next */
  int i, count, next_count, temp;
  int lower_limit, upper_limit, prev_low, prev_up;
  
  p = proc_rank_me;
  
  _ftmpi_comm[com].bmtree_prev = proc_ranks[p];
  for(i=0;i<MAXTREEFANOUT;i++) _ftmpi_comm[com].bmtree_next[i] = -1;
  
  if (proc_rank_sizes<2) return (0); 
  
  lower_limit = 0;
  upper_limit = proc_rank_sizes-1;
  count = 0;
  while(p != lower_limit){
    prev_low = lower_limit; prev_up = upper_limit;
    
    if(upper_limit == p && (upper_limit - lower_limit) == 1){
      _ftmpi_comm[com].bmtree_prev = lower_limit;
      count++;
      break;
    }
    
    if(p > (temp = (lower_limit + upper_limit)/2)){
      lower_limit = temp+1;
    }
    else{
      upper_limit = temp;
    }
  }
  if(count == 0 && p != 0){
      _ftmpi_comm[com].bmtree_prev = prev_low;
  }

  /* determining nexts */
  lower_limit = 0;
  upper_limit = proc_rank_sizes-1;
  count = 0;
  next_count = 0;
  while(p != upper_limit){

    if(lower_limit == p)
      count++;
    if(count > 1){
      _ftmpi_comm[com].bmtree_next[next_count++] = upper_limit+1;
   }
    if(lower_limit == p && (upper_limit - lower_limit) == 1){
      _ftmpi_comm[com].bmtree_next[next_count++] = p+1;
    }

    if(p > (temp = (lower_limit + upper_limit)/2)){
      lower_limit = temp+1;
    }
    else{
      upper_limit = temp;
    }
  }

  _ftmpi_comm[com].bmtree_nextsize = next_count;
}

int top_build_bmtree( MPI_Comm comm, int root )
{
  int size, rank;
  int num_level;
  double dnum_level;
  int srank;
  int j, k, l;
  double double_k, double_l;
  int child, father;
  double double_child;
  int num_child=0;

  if ( root == _ftmpi_comm[comm].bmtree_root )
    {
      /* Everything is set up already for this case,
	 no need to recalculate the tree */
      return ( MPI_SUCCESS );
    }

  size = ftmpi_com_size ( comm );
  rank = ftmpi_com_rank ( comm );

  /*
  ** Determine, how many levels our tree has
  */
  dnum_level = (log10 ( (double)size )) / (log10 ( 2.0 ));
  num_level  = ( int ) ceil (  dnum_level);

  /*
  ** Shift all ranks by root, so that the algorithm
  ** can be designed as if root would be always 0
  ** i is the shifted rank now of each process
  */
  srank = rank -root;
  if ( srank < 0 ) srank = srank + size;

  /* 
  ** Determine how many levels are above and below me
  ** k = number of levels below me ( = number of msgs. to receive)
  ** l = number of levels above me
  */
  if ( srank == 0 )
    {
      k = -1;
      l = -1;
    }
  else if ( srank == 1)
    {
      k = 0; 
      l = 0;
    }
  else
    {
      double_k = (log10 ( (double)srank ) ) / (log10 ( 2.0 ) ) ;
      k        = (int ) floor ( double_k); 
      double_l = pow(2,k);
      l        = srank - (int) floor ( double_l);
    }

  /*
  ** Determin children
  */
  for ( j = num_level; j >= k+1; j-- )
    {
      double_child = pow(2.0 , (double) j);
      child = (int) double_child + srank;
      if ( child < size )
        {
          child = child + root;
          if ( child >= size )
            child = child - size;
	  
	  _ftmpi_comm[comm].bmtree_next[num_child]= child;
          num_child++;
        }
    }
  _ftmpi_comm[comm].bmtree_nextsize = num_child;
  _ftmpi_comm[comm].bmtree_root     = root;

  /* 
  ** Determine parent
  */
  if ( l >= 0 ) 
    {
      father = l + root;
      if ( father >= size ) 
        father = father - size;
      _ftmpi_comm[comm].bmtree_prev = father;
    }
  else
      _ftmpi_comm[comm].bmtree_prev = -1;


  return ( MPI_SUCCESS );
}
#endif 

/* closest first */
int top_build_bmtree( MPI_Comm comm, int root )
{
  int childs = 0;
  int rank = ftmpi_com_rank(comm);
  int size = ftmpi_com_size(comm);
  int mask = 1;
  int index = rank - root;
  int remote;

  if( _ftmpi_comm[comm].bmtree_root == root ) {
    /* the bmtree was computed before */
    return MPI_SUCCESS;
  }

/*   printf( "Compute bmtree with root = %d and size %d\n", root, size ); */
  if( index < 0 ) index += size;

  while( mask <= index ) mask <<= 1;

  /* Now I can compute my father rank */
  if( root == rank ) _ftmpi_comm[comm].bmtree_prev = root;
  else {
    remote = (index ^ (mask >> 1)) + root;
    if( remote >= size ) remote -= size;
    _ftmpi_comm[comm].bmtree_prev = remote;
  }
/*   printf( "node %d parent = %d (index %d)\n", rank, */
/* 	  _ftmpi_comm[comm].bmtree_prev, index ); */
  /* And now let's fill my childs */
  while( mask < size ) {
    remote = (index ^ mask);
    if( remote >= size ) break;
    remote += root;
    if( remote >= size ) remote -= size;
    _ftmpi_comm[comm].bmtree_next[childs] = remote;
/*     printf( "node %d child index %d = %d\n", */
/* 	    rank, childs, _ftmpi_comm[comm].bmtree_next[childs] ); */
    mask <<= 1;
    childs++;
  }
/*   printf( "node %d numnber of childs %d\n", rank, childs ); */
  _ftmpi_comm[comm].bmtree_nextsize = childs;
  _ftmpi_comm[comm].bmtree_root     = root;
  return MPI_SUCCESS;
}

/* closest last */
int top_build_bmtree_( MPI_Comm comm, int root )
{
  int childs = 0, remote, index, size, rank;
  int mask = 1;

  if( _ftmpi_comm[comm].bmtree_root == root ) {
    /* the bmtree was computed before */
    return MPI_SUCCESS;
  }

  size = ftmpi_com_size( comm );
  rank = ftmpi_com_rank( comm );
  remote = rank;
  index = rank - root;

  if( index < 0 ) index += size;

  while( mask <= size ) {
    if( index & mask ) {
      /* find the father */
      remote = ( rank >= mask ? rank - mask : rank - mask + size );
      break;
    }
    mask <<= 1;
  }
  _ftmpi_comm[comm].bmtree_prev = remote;
/*   printf( "node %2d [%2d] {%2d} ", rank, remote, index ); */
  mask >>= 1;

  while( mask > 0 ) {
    if( index + mask < size ) {
      /* the childs */
      remote = rank + mask;
      if( remote >= size ) remote -= size;
      _ftmpi_comm[comm].bmtree_next[childs] = remote;
      childs++;
/*       printf( "(%2d) ", remote ); */
    }
    mask >>= 1;
  }

  _ftmpi_comm[comm].bmtree_nextsize = childs;
  _ftmpi_comm[comm].bmtree_root     = root;
/*   printf( "\n" ); */
  return MPI_SUCCESS;
}

