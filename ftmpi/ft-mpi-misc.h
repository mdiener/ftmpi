
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gaberiel	 <egaberiel@cs.utk.edu>
			Thara Angskun	 <angskun@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This file contains misc routines that are general support routines
 These are no general enough to be put in Harness/share (yet)

 GEF.
 */


/* list of gids (or other integer)  handling routines */

void ftmpi_array_free (int**);
int  ftmpi_array_mkspace (int**, int);
int  ftmpi_array_add (int*, int, int);
int  ftmpi_array_rm (int*, int, int);
int  ftmpi_array_rank (int*, int, int);
int  ftmpi_array_pack (int*, int);
int  ftmpi_array_count (int*, int);
int  ftmpi_array_first (int*, int);
int  ftmpi_array_extent (int*, int);
int  ftmpi_array_copy   (int*, int*, int);
void ftmpi_array_dump (int*, int);




