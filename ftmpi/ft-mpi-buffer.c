/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors of this file:	
           Edgar Gabriel <egabriel@cs.utk.edu>
           Graham E Fagg <fagg@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-buffer.h"
#include "ft-mpi-com.h"
#include "ft-mpi-nb.h"

#include "debug.h"

/* 
   The management of the buffer consists of three parts:
   - first, we have to remember the original buffer point its complete size
   - second, we have to keep track of which parts of the buffer is in use
   - third, we need to maintain a list aswell of all elements which are still
     available
*/

/* The first part is more or less trivial, the following routines have to be provided:
   - ftmpi_buffer_init( buffer,size): store the general information about the buffer on
                                      which we will work; mark the whole buffer as unused;
   - ftmpi_buffer_isinitialied:       check whether the buffer is already initialized;
   - ftmpi_buffer_flush:              flush all messages still in the buffer and marc
                                      everything as unused;
   - ftmpi_buffer_free:               marc this buffer as not used anymore;
*/


/* 
   The list of memory segments which are unused are stored as a doubly-linked
   list. The list is maintained in a way, that the elements are ordered
   according to the address of the buffer segment which they are describing.
   - ftmpi_buffer_unused_from_used:  move a buffer segment from the used-list
                                     to the unused list. Find the correct place,
				     and try to merge the new entries.
   - ftmpi_buffer_unused_merge_all:  try to shrink the list and minimize
                                     the number of entries.
   - ftmpi_buffer_unused_merge_two:  merge two entries, which are handling
                                     subsequent memory areas.
   - ftmpi_buffer_unused_split:      split a memory segment into two entries
   - ftmpi_buffer_unused_removeseg:  remove a segment from the unused list
   - ftmpi_buffer_unused_getnext:    get a handle to the memory segment which can
                                     be used for the next operation.
*/
                                      

ftmpi_buffer_t ftmpi_bsend_buffer;
static int initialized=0;  


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_init (void *buffer, int size )
{
  /* initialize the structure, move everything as a single segment to 
     the unused_buffer list */

  if ( !initialized ) 
    initialized = 1;

  /* Set the general elements */
  ftmpi_bsend_buffer.buffer     = buffer;
  ftmpi_bsend_buffer.size       = size;
  
  /* Mark the whole buffer as unused */
  ftmpi_bsend_buffer.num_unused = 1;
  ftmpi_bsend_buffer.unused = (unused_buffer *)_MALLOC( sizeof(unused_buffer));
  if ( ftmpi_bsend_buffer.unused == NULL ) return ( MPI_ERR_INTERN);
  ftmpi_bsend_buffer.unused->buffer = buffer;
  ftmpi_bsend_buffer.unused->size   = size;
  ftmpi_bsend_buffer.unused->next   = NULL;
  ftmpi_bsend_buffer.unused->prev   = NULL;

  /* No segments of the buffer used yet */
  ftmpi_bsend_buffer.num_used  = 0;
  ftmpi_bsend_buffer.used = NULL;

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_isattached ( void )
{
  int ret;
  
  if (!initialized ) 
    ret = 0;
  else
    {
      if (ftmpi_bsend_buffer.buffer == NULL )
	ret = 0;
      else
	ret = 1;
    }
  
  return ( ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_size ( void)
{
  return( ftmpi_bsend_buffer.size );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* flush all messages from the buffer and move everything to the
   unused buffer list */
void ftmpi_buffer_flush ( void )
{
  int i;
  used_buffer *tmp1, *tmp1next;
  unused_buffer *tmp2, *tmp2next;

  tmp1 = ftmpi_bsend_buffer.used;
  /* if the num_used is not ZERO something goes wrong here as we cant
     free a not yet completed operation !!!
  */
  for ( i = 0; i < ftmpi_bsend_buffer.num_used; i ++ )
    {
      fprintf( stderr, "Buffer_flush: freeing a not yet completed send)\n" );
      tmp1next = tmp1->next;
      if ( tmp1 != NULL )
	_FREE  (tmp1 );
      tmp1 = tmp1next;
    }

  tmp2 = ftmpi_bsend_buffer.unused;
  for ( i = 0; i < ftmpi_bsend_buffer.num_unused; i ++ )
    {
      tmp2next = tmp2->next;
      if ( tmp2 != NULL )
	_FREE (tmp2 );
      tmp2 = tmp2next;
    }


  /* Mark the whole buffer as unused */
  ftmpi_bsend_buffer.num_unused = 1;
  ftmpi_bsend_buffer.unused = (unused_buffer *)_MALLOC( sizeof(unused_buffer));
  if ( ftmpi_bsend_buffer.unused == NULL ) printf("Error allocating memory\n");
  ftmpi_bsend_buffer.unused->buffer = ftmpi_bsend_buffer.buffer;
  ftmpi_bsend_buffer.unused->size = ftmpi_bsend_buffer.size;
  ftmpi_bsend_buffer.unused->next   = NULL;
  ftmpi_bsend_buffer.unused->prev   = NULL;

  /* No segments of the buffer used yet */
  ftmpi_bsend_buffer.num_used  = 0;
  ftmpi_bsend_buffer.used = NULL;
  
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_detach ( void **buf,int *size )
{
  int ret;

  ret = ftmpi_buffer_used_wait();
  if ( ret != MPI_SUCCESS ) return ( ret );

  ftmpi_buffer_flush ();
  
  _FREE( ftmpi_bsend_buffer.unused );
  ftmpi_bsend_buffer.num_unused = 0;
  
  *buf  = ftmpi_bsend_buffer.buffer;
  *size = ftmpi_bsend_buffer.size;

  ftmpi_bsend_buffer.buffer = NULL;
  ftmpi_bsend_buffer.size   = 0;

  initialized = 0;

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
used_buffer *ftmpi_buffer_getnextseg ( int packsize,  void **bufp)
{
  unused_buffer *tmp;
  used_buffer *utmp;
  int ret;
  
  /* Check all initiated communication for completion and remove them
     from the used-buffer list if finished */
  ret = ftmpi_buffer_used_test();
  if ( ret != MPI_SUCCESS ) return ( NULL );
  /* Get a segment of size = packsize */
  tmp = ftmpi_buffer_unused_getnext ( packsize );
  /* mark this segment of the buffer as USED */
  utmp = ftmpi_buffer_used_from_unused ( tmp );
  /* remove this segment from the unused-buffer list */
  ftmpi_buffer_unused_remove (tmp);

  *bufp = utmp->buffer;

  return ( utmp );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_unused_from_used (used_buffer *useg)
{
  /* add a new unused buffer segment (probably move from the used list */
  unused_buffer *unseg;
  unused_buffer *tmp, *tmp2=NULL;
  int done = 0;

  unseg = (unused_buffer *)_MALLOC( sizeof(unused_buffer));
  if ( unseg == NULL ) return ( MPI_ERR_INTERN);

  unseg->size   = useg->size;
  unseg->buffer = useg->buffer;

  /* find the place where we have to insert this entry */
  if ( ftmpi_bsend_buffer.num_unused == 0 )
    {
      /* no need to search for the correct place,
	 since there is no entry up to now */
      ftmpi_bsend_buffer.unused = unseg;
      unseg->next = NULL;
      unseg->prev = NULL;
    }
  else
    {
      tmp = ftmpi_bsend_buffer.unused; 
      while ( tmp != NULL ) 
	{
	  if ( tmp->buffer > unseg->buffer )
	    {
	      done = 1;
	      break;
	    }
	  tmp2 = tmp;
	  tmp = tmp->next;
	}
    
      if ( done )
	{
	  /* found an entry, whose bufferpointer is greater than ours 
	     this can still be the first entry or one in the middle */
	  unseg->next = tmp;
	  unseg->prev = tmp->prev;
	  tmp->prev   = unseg;
	  
	  tmp = unseg->prev;
	  if ( tmp != NULL )
	    {
	      /* ok, we are somewhere in the middle */
	      tmp->next = unseg;
	    }
	  else
	    {
	      /* we have been put into the first place in the list */
	      ftmpi_bsend_buffer.unused = unseg;
	    }
	}
      else
	{
	  /* we are the last in the row */
	  unseg->next = NULL;
	  unseg->prev = tmp2;
	  tmp2->next  = unseg;
	}
    }
  ftmpi_bsend_buffer.num_unused++;

  /* now check whether we can merge some of the entries in the unused
     buffer list */
  ftmpi_buffer_unused_merge_all ();

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_unused_merge_all ( void )
{
  /* go through the list of unused segments and try to merge them */

  unused_buffer *tmp1, *tmp2;
  int prevnumunused;
  int done = 0;

  if ( ftmpi_bsend_buffer.num_unused > 1 )
    {
      while ( !done )
	{
	  tmp1 = ftmpi_bsend_buffer.unused;
	  prevnumunused = ftmpi_bsend_buffer.num_unused;
      
	  while ( tmp1 != NULL )
	    {
	      tmp2 = tmp1->next;
	      if ( tmp2 == NULL )
		{
		  /* Can't do more, in this turnaroun, the end
		     of the list is reached 
		  */
		  break;
		}
	      if ( tmp2->buffer == ( (char *)tmp1->buffer + tmp1->size ) )
		{
		  /* since we make sure that the entries in the 
		     unused buffer list are ordered according
		     to the buffer address, thats the only case which we
		     have to check
		  */
		  ftmpi_buffer_unused_merge_two ( tmp1, tmp2 );
		  ftmpi_bsend_buffer.num_unused--;
		}
	      tmp1 = tmp1->next;
	    }
	  if ( prevnumunused == ftmpi_bsend_buffer.num_unused )
	    {
	      /* we couldn't merge anything, lets exit */
	      done = 1;
	    }
	  /* else next round */
	}
    }
  
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_unused_merge_two (unused_buffer *seg1, 
				    unused_buffer *seg2)
{

  /* Assumption: these two segments are neighbors, therefore we merge them
     to a single element
  */
  unused_buffer *tmp;

  
  tmp = seg2->next;
  seg1->size += seg2->size;
  seg1->next  = seg2->next;
  if ( tmp != NULL )
    { /* if there is an element behind us*/
      tmp->prev   = seg1;
    }
  
  /* After the data is stored, free seg2 */
  _FREE( seg2 );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_unused_split (unused_buffer *seg, long newsize)
{
  /* split an unused buffer segment into two elements. Goal,
     one of the segments will be moved to the used-list, the other
     remains in the unused-list */

  unused_buffer *newseg;
  unused_buffer *tmp;

  if ( newsize > seg->size ) return ( MPI_ERR_INTERN );
  tmp = seg->next;

  newseg = (unused_buffer*)_MALLOC( sizeof ( unused_buffer));
  if ( newseg == NULL ) return ( MPI_ERR_INTERN);

  newseg->buffer = (char *)seg->buffer+newsize;
  newseg->size   = seg->size - newsize;
  newseg->next   = seg->next;
  newseg->prev   = seg;

  /* modify the settings seg */
  seg->size = newsize;
  if ( tmp != NULL )
    tmp->prev = newseg;
  seg->next = newseg;

  ftmpi_bsend_buffer.num_unused++;
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_unused_remove ( unused_buffer *seg )
{
  /* remove a segment from the unused_buffer_list */
  unused_buffer *tmpnext, *tmpprev;
  
  tmpnext = seg->next;
  tmpprev = seg->prev;

  if ( seg->next != NULL )
      tmpnext->prev = seg->prev;

  if ( seg->prev != NULL )
    tmpprev->next = seg->next;
  else
    {
      /* if my predecessor is NULL, than I am the root */
      ftmpi_bsend_buffer.unused = tmpnext;
    }

  ftmpi_bsend_buffer.num_unused--;
  _FREE( seg );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* This returns a handle to an unused buffer. This routine is used to find
   an segment for moving it to the used_buffer list*/
unused_buffer* ftmpi_buffer_unused_getnext ( int size )
{
  unused_buffer *tmp;
  int found=0;

  if ( ftmpi_bsend_buffer.num_unused == 0 )
    return ( NULL ); /* no memory left */

  tmp = ftmpi_bsend_buffer.unused;
  
  while ( !found && (tmp!=NULL) )
    {
      if ( tmp->size > size )
	{
	  /* found an element */
	  ftmpi_buffer_unused_split ( tmp, size );
	  found = 1;
	  return ( tmp );
	}
      else if ( tmp->size == size )
	{
	  found = 1;
	  return ( tmp );
	}

      tmp = tmp->next;  
    }
  
  /* no memory segment left to return */
  return ( NULL );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_unused_dump ( void )
{
  int i;
  unused_buffer *tmp;
  int rank;

  rank = ftmpi_com_rank (MPI_COMM_WORLD);

  tmp = ftmpi_bsend_buffer.unused;
  printf("[%d]: Dumping unused buffer list. Number of segments: %d\n",
	 rank, ftmpi_bsend_buffer.num_unused);
  for ( i = 0; i < ftmpi_bsend_buffer.num_unused; i ++ )
    {
      printf("[%d] Entry: %d, buffer = %p, size =%ld, next=%p, prev = %p,"
	     "own start address: %p\n", rank, i, tmp->buffer, tmp->size,
	     tmp->next, tmp->prev, tmp );
      tmp = tmp->next;
    }

  if ( tmp != NULL )
    {
      printf("[%d] It look like we would have another entry ??? \n", rank);
      printf("[%d] Entry unknown, buffer = %p, size =%ld, next=%p, prev = %p,"
	     "own start address: %p\n", rank, tmp->buffer, tmp->size,
	     tmp->next, tmp->prev, tmp );
    }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* moves a memory segment from the unused list to the used list */
used_buffer* ftmpi_buffer_used_from_unused ( unused_buffer *seg )
{
  /* add a new used buffer segment (probably move from the unused list */
  used_buffer *useg;
  used_buffer *tmp, *tmp2=NULL;
  int done = 0;

  useg = (used_buffer *)_MALLOC( sizeof(used_buffer));
  if ( useg == NULL ) return ( NULL );

  useg->size   = seg->size;
  useg->buffer = seg->buffer;
  useg->lcontrol = 0;

  /* find the place where we have to insert this entry */
  if ( ftmpi_bsend_buffer.num_used == 0 )
    {
      /* no need to search for the correct place,
	 since there is no entry up to now */
      ftmpi_bsend_buffer.used = useg;
      useg->next = NULL;
      useg->prev = NULL;
    }
  else
    {
      tmp = ftmpi_bsend_buffer.used; 
      while ( tmp != NULL )
	{
	  if ( tmp->buffer > useg->buffer )
	    {
	      done = 1;
	      break;
	    }
	  tmp2 = tmp;
	  tmp = tmp->next;
	}
    
      if ( done )
	{
	  /* found an entry, whose bufferpointer is greater than ours 
	     this can still be the first entry or one in the middle */
	  useg->next = tmp;
	  useg->prev = tmp->prev;
	  tmp->prev   = useg;
	  
	  tmp = useg->prev;
	  if ( tmp != NULL )
	    {
	      /* ok, we are somewhere in the middle */
	      tmp->next = useg;
	    }
	  else
	    {
	      /* we have been put into the first place in the list */
	      ftmpi_bsend_buffer.used = useg;
	    }
	}
      else
	{
	  /* we are the last in the row */
	  useg->next = NULL;
	  useg->prev = tmp2;
	  tmp2->next = useg;
	}
    }
  ftmpi_bsend_buffer.num_used++;
  return  (useg);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* test whether the messages in the used buffer list have finished.
   if yes, move them to the unused buffer list
   Remeber, just the messages, for which we have created the request
   handles can be handled in this function. 
*/
int ftmpi_buffer_used_test ( void)
{
  int i;
  int flag;
  used_buffer *tmp, *tmpnext;
  MPI_Status status;
  int ret;

  tmp = ftmpi_bsend_buffer.used;
  /* for ( i = 0; i < ftmpi_bsend_buffer.num_used; i++ )
     num_used being decreased in ftmpi_buffer_used_remove
     the function never test all the pending operations registered
     with the buffer.
  */
  for ( i = ftmpi_bsend_buffer.num_used; i > 0; i-- )
    {
      tmpnext = tmp->next;
      if ( tmp->lcontrol )
	{
	  /* printf("REQ -- %d\n",tmp->req); */
	  ret = ftmpi_mpi_test ( &tmp->req, &flag, &status );
	  /* printf("RET TEST %d\n",ret); */
	  if ( ret != MPI_SUCCESS) return ( ret );
	  if ( flag )
	    {
	      /*remove the entry */
	      ftmpi_buffer_unused_from_used ( tmp );
	      ftmpi_buffer_used_remove ( tmp );
	    }
	}
      tmp = tmpnext;
      if ( tmp == NULL )
	break;
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_buffer_used_wait ( void)
{
  /* the same like above, but it blocks until all messages are done
     called from MPI_Buffer_detach */
  int i;
  used_buffer *tmp, *tmpnext;
  MPI_Status status;
  int ret;

  tmp = ftmpi_bsend_buffer.used;
  /* for ( i = 0; i < ftmpi_bsend_buffer.num_used; i++ )
     num_used being decreased in ftmpi_buffer_used_remove
     the function never send all the pending messages.
  */
  for ( i = ftmpi_bsend_buffer.num_used; i > 0; i-- )
    {
      tmpnext = tmp->next;
      if ( tmp->lcontrol )
	{
	  /*  printf("REQ -- %d\n",tmp->req); */
	   ret = ftmpi_mpi_wait ( &tmp->req, &status ); 
	   /* printf("RET WAIT %d\n",ret); */
	   if ( ret != MPI_SUCCESS) return (ret);
	  /*remove the entry */
	  ftmpi_buffer_unused_from_used ( tmp );
	  ftmpi_buffer_used_remove ( tmp );
	}
      tmp = tmpnext;
      if ( tmp == NULL )
	break;
    }

  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_used_remove ( used_buffer *seg )
{
  /* remove a segment from the used_buffer_list */
  used_buffer *tmpnext, *tmpprev;
  
  tmpnext = seg->next;
  tmpprev = seg->prev;

  if ( seg->next != NULL )
      tmpnext->prev = seg->prev;

  if ( seg->prev != NULL )
    tmpprev->next = seg->next;
  else
    ftmpi_bsend_buffer.used = tmpnext;

  ftmpi_bsend_buffer.num_used--;
  _FREE( seg );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_used_dump ( void )
{
  int i;
  used_buffer *tmp;
  int rank;

  rank = ftmpi_com_rank ( MPI_COMM_WORLD );

  tmp = ftmpi_bsend_buffer.used;
  printf("[%d] Dumping used buffer list. Number of segments: %d\n",
	 rank, ftmpi_bsend_buffer.num_used);
  for ( i = 0; i < ftmpi_bsend_buffer.num_used; i ++ )
    {
      printf("[%d] Entry: %d, buffer = %p, size =%ld, next=%p, prev = %p,"
	     "own start address: %p\n", rank, i, tmp->buffer, tmp->size,
	     tmp->next, tmp->prev, tmp );
      tmp = tmp->next;
    }

  if ( tmp != NULL )
    {
      printf("[%d] It look like we would have another entry ??? \n", rank);
      printf("[%d] Entry unknown, buffer = %p, size =%ld, next=%p, prev = %p,"
	     "own start address: %p\n", rank, tmp->buffer, tmp->size,
	     tmp->next, tmp->prev, tmp );
    }
}
   
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_used_setreq ( used_buffer *handle, MPI_Request req )
{
  handle->req = req;
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_used_setlcontrol ( used_buffer *handle )
{
  handle->lcontrol = 1;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_buffer_used_free ( MPI_Request req )
{
  used_buffer *tmp, *tmpnext;
  int i;

  tmp = ftmpi_bsend_buffer.used;
  for ( i = 0; i < ftmpi_bsend_buffer.num_used; i++ )
    {
      tmpnext = tmp->next;
      if ( tmp->req == req )
	break;

      tmp = tmpnext;
      if ( tmp == NULL )
	break;
    }
      
  if ( tmp != NULL )
    {
      ftmpi_buffer_unused_from_used ( tmp );
      ftmpi_buffer_used_remove ( tmp );
    }
}


