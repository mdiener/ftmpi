
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* 
	Prototypes for functions that work on communicator data structures

	GEF
*/

#ifndef __FT_MPI_COM_H__
#define __FT_MPI_COM_H__

comm_info_t* ftmpi_com_get_ptr (MPI_Comm );
int ftmpi_com_ok (int com);
int ftmpi_com_set_group (MPI_Comm com, MPI_Group grp);
MPI_Group ftmpi_com_get_group (MPI_Comm com);

int ftmpi_com_get_send_count (MPI_Comm com);
int ftmpi_com_get_recv_count (MPI_Comm com);

int ftmpi_com_init ();
int ftmpi_com_clr_all_but_world ();
int ftmpi_com_clr (int);
int ftmpi_com_build (int,int,int,int,int,int *,int,int,int);
int ftmpi_com_rank (int);
int ftmpi_com_size (int);
int ftmpi_com_gid (int,int);
int ftmpi_com_map_rank  ( MPI_Comm com, int gid);
int ftmpi_com_fptype (int,int);
int ftmpi_com_my_gid ();
int ftmpi_com_get_com_mode (int com);
int ftmpi_com_get_msg_mode (int com);
int ftmpi_com_copy (int com, int *newcom);
int ftmpi_com_copy_subset (int c, int *newc, int *g, int ext, int np);
int ftmpi_com_free (MPI_Comm *com) ;
int ftmpi_com_check_freed (MPI_Comm com);

int ftmpi_com_display (MPI_Comm com);


int ftmpi_comm_attach_cart(MPI_Comm com,cart_info_t * cart);
cart_info_t * ftmpi_comm_get_cart_ptr(MPI_Comm com);
int ftmpi_comm_attach_graph(MPI_Comm com,graph_info_t * graph);
graph_info_t * ftmpi_comm_get_graph_ptr(MPI_Comm com);
int ftmpi_com_get_topo ( MPI_Comm comm );
int ftmpi_com_lib_next_com ( int mode, int lleader, MPI_Comm comm,
			     MPI_Comm ocomm, int rleader, int tag );

int ftmpi_com_get_free_comm(void);
int ftmpi_com_get_errhandler ( MPI_Comm com );
int ftmpi_com_set_errhandler ( MPI_Comm comm, int err_handle);
int ftmpi_com_copy_errhandler ( MPI_Comm cold, MPI_Comm cnew );

int ftmpi_com_del_attr ( MPI_Comm com, int key );
int ftmpi_com_set_attr ( MPI_Comm com, int key );
int ftmpi_com_copy_attr ( MPI_Comm corg, MPI_Comm cnew );
int ftmpi_com_get_nattr ( MPI_Comm com );

int ftmpi_com_create_shadow ( MPI_Comm comm, MPI_Comm shadow );
MPI_Comm ftmpi_com_get_shadow ( MPI_Comm comm );
int ftmpi_com_test_shadow ( MPI_Comm comm );
MPI_Comm ftmpi_com_get_parent ( MPI_Comm com );

int ftmpi_mpi_comm_split(MPI_Comm comm,int color,int key,MPI_Comm *newcomm);
int ftmpi_mpi_comm_create(MPI_Comm comm, MPI_Group group, MPI_Comm *newcomm);
int ftmpi_mpi_comm_dup( MPI_Comm comm, MPI_Comm *newcom );

int ftmpi_com_reset_all_msg_lists ();
int ftmpi_com_reset_shadow_msg_lists ();

int ftmpi_com_init_errh_toabort (void);
/*
 * special recovery filtering and mapping calls 
 */
int ftmpi_com_move_all_lists_to_system_list (int startcom);




#endif /* __FT_MPI_COM_H__ */
