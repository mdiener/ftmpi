/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_P2P
#define _FT_MPI_H_F2C_P2P

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define HDR     "FTMPI:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif


#ifdef DECLARE_MPIIO


/* Modification to handle the Profiling Interface
   January 8 2003, EG */
#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak MPI_FILE_CLOSE                = mpi_file_close_	  
#    pragma weak PMPI_FILE_CLOSE               = mpi_file_close_
#    pragma weak MPI_FILE_CREATE_ERRHANDLER    = mpi_file_create_errhandler_	
#    pragma weak PMPI_FILE_CREATE_ERRHANDLER   = mpi_file_create_errhandler_
#    pragma weak MPI_FILE_DELETE               = mpi_file_delete_	  
#    pragma weak PMPI_FILE_DELETE              = mpi_file_delete_
#    pragma weak MPI_FILE_GET_AMODE            = mpi_file_get_amode_	  
#    pragma weak PMPI_FILE_GET_AMODE           = mpi_file_get_amode_
#    pragma weak MPI_FILE_GET_ERRHANDLER       = mpi_file_get_errhandler_  
#    pragma weak PMPI_FILE_GET_ERRHANDLER      = mpi_file_get_errhandler_
#    pragma weak MPI_FILE_GET_GROUP            = mpi_file_get_group_	  
#    pragma weak PMPI_FILE_GET_GROUP           = mpi_file_get_group_
#    pragma weak MPI_FILE_GET_INFO             = mpi_file_get_info_	  
#    pragma weak PMPI_FILE_GET_INFO            = mpi_file_get_info_
#    pragma weak MPI_FILE_GET_SIZE             = mpi_file_get_size_	  
#    pragma weak PMPI_FILE_GET_SIZE            = mpi_file_get_size_
#    pragma weak MPI_FILE_GET_VIEW             = mpi_file_get_view_	  
#    pragma weak PMPI_FILE_GET_VIEW            = mpi_file_get_view_
#    pragma weak MPI_FILE_IREAD_AT             = mpi_file_iread_at_	  
#    pragma weak PMPI_FILE_IREAD_AT            = mpi_file_iread_at_
#    pragma weak MPI_FILE_IWRITE_AT            = mpi_file_iwrite_at_	  
#    pragma weak PMPI_FILE_IWRITE_AT           = mpi_file_iwrite_at_
#    pragma weak MPI_FILE_OPEN                 = mpi_file_open_	  
#    pragma weak PMPI_FILE_OPEN                = mpi_file_open_
#    pragma weak MPI_FILE_READ_AT_ALL          = mpi_file_read_at_all_	  
#    pragma weak PMPI_FILE_READ_AT_ALL         = mpi_file_read_at_all_
#    pragma weak MPI_FILE_READ_AT              = mpi_file_read_at_	  
#    pragma weak PMPI_FILE_READ_AT             = mpi_file_read_at_
#    pragma weak MPI_FILE_SET_ERRHANDLER       = mpi_file_set_errhandler_  
#    pragma weak PMPI_FILE_SET_ERRHANDLER      = mpi_file_set_errhandler_
#    pragma weak MPI_FILE_SET_INFO             = mpi_file_set_info_	  
#    pragma weak PMPI_FILE_SET_INFO            = mpi_file_set_info_
#    pragma weak MPI_FILE_SET_SIZE             = mpi_file_set_size_	  
#    pragma weak PMPI_FILE_SET_SIZE            = mpi_file_set_size_
#    pragma weak MPI_FILE_SET_VIEW             = mpi_file_set_view_	  
#    pragma weak PMPI_FILE_SET_VIEW            = mpi_file_set_view_
#    pragma weak MPI_FILE_SYNC                 = mpi_file_sync_	  
#    pragma weak PMPI_FILE_SYNC                = mpi_file_sync_
#    pragma weak MPI_FILE_WRITE_AT_ALL         = mpi_file_write_at_all_   
#    pragma weak PMPI_FILE_WRITE_AT_ALL        = mpi_file_write_at_all_
#    pragma weak MPI_FILE_WRITE_AT             = mpi_file_write_at_	
#    pragma weak PMPI_FILE_WRITE_AT            = mpi_file_write_at_


#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak mpi_file_close__              = mpi_file_close_
#    pragma weak pmpi_file_close__             = mpi_file_close_
#    pragma weak mpi_file_create_errhandler__  = mpi_file_create_errhandler_
#    pragma weak pmpi_file_create_errhandler__ = mpi_file_create_errhandler_
#    pragma weak mpi_file_delete__             = mpi_file_delete_
#    pragma weak pmpi_file_delete__            = mpi_file_delete_
#    pragma weak mpi_file_get_amode__          = mpi_file_get_amode_
#    pragma weak pmpi_file_get_amode__         = mpi_file_get_amode_
#    pragma weak mpi_file_get_errhandler__     = mpi_file_get_errhandler_
#    pragma weak pmpi_file_get_errhandler__    = mpi_file_get_errhandler_
#    pragma weak mpi_file_get_group__          = mpi_file_get_group_
#    pragma weak pmpi_file_get_group__         = mpi_file_get_group_
#    pragma weak mpi_file_get_info__           = mpi_file_get_info_
#    pragma weak pmpi_file_get_info__          = mpi_file_get_info_
#    pragma weak mpi_file_get_size__           = mpi_file_get_size_
#    pragma weak pmpi_file_get_size__          = mpi_file_get_size_
#    pragma weak mpi_file_get_view__           = mpi_file_get_view_
#    pragma weak pmpi_file_get_view__          = mpi_file_get_view_
#    pragma weak mpi_file_iread_at__           = mpi_file_iread_at_
#    pragma weak pmpi_file_iread_at__          = mpi_file_iread_at_
#    pragma weak mpi_file_iwrite_at__          = mpi_file_iwrite_at_
#    pragma weak pmpi_file_iwrite_at__         = mpi_file_iwrite_at_
#    pragma weak mpi_file_open__               = mpi_file_open_
#    pragma weak pmpi_file_open__              = mpi_file_open_
#    pragma weak mpi_file_read_at_all__        = mpi_file_read_at_all_
#    pragma weak pmpi_file_read_at_all__       = mpi_file_read_at_all_
#    pragma weak mpi_file_read_at__            = mpi_file_read_at_
#    pragma weak pmpi_file_read_at__           = mpi_file_read_at_
#    pragma weak mpi_file_set_errhandler__     = mpi_file_set_errhandler_
#    pragma weak pmpi_file_set_errhandler__    = mpi_file_set_errhandler_
#    pragma weak mpi_file_set_info__           = mpi_file_set_info_
#    pragma weak pmpi_file_set_info__          = mpi_file_set_info_
#    pragma weak mpi_file_set_size__           = mpi_file_set_size_
#    pragma weak pmpi_file_set_size__          = mpi_file_set_size_
#    pragma weak mpi_file_set_view__           = mpi_file_set_view_
#    pragma weak pmpi_file_set_view__          = mpi_file_set_view_
#    pragma weak mpi_file_sync__               = mpi_file_sync_
#    pragma weak pmpi_file_sync__              = mpi_file_sync_
#    pragma weak mpi_file_write_at_all__       = mpi_file_write_at_all_
#    pragma weak pmpi_file_write_at_all__      = mpi_file_write_at_all_
#    pragma weak mpi_file_write_at__           = mpi_file_write_at_
#    pragma weak pmpi_file_write_at__          = mpi_file_write_at_


#  elif defined(FORTRANNOUNDERSCORE)

#    pragma weak mpi_file_close              = mpi_file_close_
#    pragma weak pmpi_file_close             = mpi_file_close_
#    pragma weak mpi_file_create_errhandler  = mpi_file_create_errhandler_
#    pragma weak pmpi_file_create_errhandler = mpi_file_create_errhandler_
#    pragma weak mpi_file_delete             = mpi_file_delete_
#    pragma weak pmpi_file_delete            = mpi_file_delete_
#    pragma weak mpi_file_get_amode          = mpi_file_get_amode_
#    pragma weak pmpi_file_get_amode         = mpi_file_get_amode_
#    pragma weak mpi_file_get_errhandler     = mpi_file_get_errhandler_
#    pragma weak pmpi_file_get_errhandler    = mpi_file_get_errhandler_
#    pragma weak mpi_file_get_group          = mpi_file_get_group_
#    pragma weak pmpi_file_get_group         = mpi_file_get_group_
#    pragma weak mpi_file_get_info           = mpi_file_get_info_
#    pragma weak pmpi_file_get_info          = mpi_file_get_info_
#    pragma weak mpi_file_get_size           = mpi_file_get_size_
#    pragma weak pmpi_file_get_size          = mpi_file_get_size_
#    pragma weak mpi_file_get_view           = mpi_file_get_view_
#    pragma weak pmpi_file_get_view          = mpi_file_get_view_
#    pragma weak mpi_file_iread_at           = mpi_file_iread_at_
#    pragma weak pmpi_file_iread_at          = mpi_file_iread_at_
#    pragma weak mpi_file_iwrite_at          = mpi_file_iwrite_at_
#    pragma weak pmpi_file_iwrite_at         = mpi_file_iwrite_at_
#    pragma weak mpi_file_open               = mpi_file_open_
#    pragma weak pmpi_file_open              = mpi_file_open_
#    pragma weak mpi_file_read_at_all        = mpi_file_read_at_all_
#    pragma weak pmpi_file_read_at_all       = mpi_file_read_at_all_
#    pragma weak mpi_file_read_at            = mpi_file_read_at_
#    pragma weak pmpi_file_read_at           = mpi_file_read_at_
#    pragma weak mpi_file_set_errhandler     = mpi_file_set_errhandler_
#    pragma weak pmpi_file_set_errhandler    = mpi_file_set_errhandler_
#    pragma weak mpi_file_set_info           = mpi_file_set_info_
#    pragma weak pmpi_file_set_info          = mpi_file_set_info_
#    pragma weak mpi_file_set_size           = mpi_file_set_size_
#    pragma weak pmpi_file_set_size          = mpi_file_set_size_
#    pragma weak mpi_file_set_view           = mpi_file_set_view_
#    pragma weak pmpi_file_set_view          = mpi_file_set_view_
#    pragma weak mpi_file_sync               = mpi_file_sync_
#    pragma weak pmpi_file_sync              = mpi_file_sync_
#    pragma weak mpi_file_write_at_all       = mpi_file_write_at_all_
#    pragma weak pmpi_file_write_at_all      = mpi_file_write_at_all_
#    pragma weak mpi_file_write_at           = mpi_file_write_at_
#    pragma weak pmpi_file_write_at          = mpi_file_write_at_


#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak pmpi_file_close_             = mpi_file_close_
#    pragma weak pmpi_file_create_errhandler_ = mpi_file_create_errhandler_
#    pragma weak pmpi_file_delete_            = mpi_file_delete_
#    pragma weak pmpi_file_get_amode_         = mpi_file_get_amode_
#    pragma weak pmpi_file_get_errhandler_    = mpi_file_get_errhandler_
#    pragma weak pmpi_file_get_group_         = mpi_file_get_group_
#    pragma weak pmpi_file_get_info_          = mpi_file_get_info_
#    pragma weak pmpi_file_get_size_          = mpi_file_get_size_
#    pragma weak pmpi_file_get_view_          = mpi_file_get_view_
#    pragma weak pmpi_file_iread_at_          = mpi_file_iread_at_
#    pragma weak pmpi_file_iwrite_at_         = mpi_file_iwrite_at_
#    pragma weak pmpi_file_open_              = mpi_file_open_
#    pragma weak pmpi_file_read_at_all_       = mpi_file_read_at_all_
#    pragma weak pmpi_file_read_at_           = mpi_file_read_at_
#    pragma weak pmpi_file_set_errhandler_    = mpi_file_set_errhandler_
#    pragma weak pmpi_file_set_info_          = mpi_file_set_info_
#    pragma weak pmpi_file_set_size_          = mpi_file_set_size_
#    pragma weak pmpi_file_set_view_          = mpi_file_set_view_
#    pragma weak pmpi_file_sync_              = mpi_file_sync_
#    pragma weak pmpi_file_write_at_all_      = mpi_file_write_at_all_
#    pragma weak pmpi_file_write_at_          = mpi_file_write_at_


#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define  mpi_file_close_             PMPI_FILE_CLOSE          
#      define  mpi_file_create_errhandler_ PMPI_FILE_CREATE_ERRHANDLER   
#      define  mpi_file_delete_            PMPI_FILE_DELETE         
#      define  mpi_file_get_amode_         PMPI_FILE_GET_AMODE      
#      define  mpi_file_get_errhandler_    PMPI_FILE_GET_ERRHANDLER   
#      define  mpi_file_get_group_         PMPI_FILE_GET_GROUP      
#      define  mpi_file_get_info_          PMPI_FILE_GET_INFO       
#      define  mpi_file_get_size_          PMPI_FILE_GET_SIZE       
#      define  mpi_file_get_view_          PMPI_FILE_GET_VIEW       
#      define  mpi_file_iread_at_          PMPI_FILE_IREAD_AT       
#      define  mpi_file_iwrite_at_         PMPI_FILE_IWRITE_AT      
#      define  mpi_file_open_              PMPI_FILE_OPEN           
#      define  mpi_file_read_at_all_       PMPI_FILE_READ_AT_ALL    
#      define  mpi_file_read_at_           PMPI_FILE_READ_AT        
#      define  mpi_file_set_errhandler_    PMPI_FILE_SET_ERRHANDLER   
#      define  mpi_file_set_info_          PMPI_FILE_SET_INFO       
#      define  mpi_file_set_size_          PMPI_FILE_SET_SIZE       
#      define  mpi_file_set_view_          PMPI_FILE_SET_VIEW       
#      define  mpi_file_sync_              PMPI_FILE_SYNC           
#      define  mpi_file_write_at_all_      PMPI_FILE_WRITE_AT_ALL    
#      define  mpi_file_write_at_          PMPI_FILE_WRITE_AT   


#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_file_close_             pmpi_file_close__ 
#      define  mpi_file_create_errhandler_ pmpi_file_create_errhandler__ 
#      define  mpi_file_delete_            pmpi_file_delete__ 
#      define  mpi_file_get_amode_         pmpi_file_get_amode__ 
#      define  mpi_file_get_errhandler_    pmpi_file_get_errhandler__ 
#      define  mpi_file_get_group_         pmpi_file_get_group__ 
#      define  mpi_file_get_info_          pmpi_file_get_info__ 
#      define  mpi_file_get_size_          pmpi_file_get_size__ 
#      define  mpi_file_get_view_          pmpi_file_get_view__ 
#      define  mpi_file_iread_at_          pmpi_file_iread_at__ 
#      define  mpi_file_iwrite_at_         pmpi_file_iwrite_at__ 
#      define  mpi_file_open_              pmpi_file_open__ 
#      define  mpi_file_read_at_all_       pmpi_file_read_at_all__ 
#      define  mpi_file_read_at_           pmpi_file_read_at__ 
#      define  mpi_file_set_errhandler_    pmpi_file_set_errhandler__ 
#      define  mpi_file_set_info_          pmpi_file_set_info__ 
#      define  mpi_file_set_size_          pmpi_file_set_size__ 
#      define  mpi_file_set_view_          pmpi_file_set_view__ 
#      define  mpi_file_sync_              pmpi_file_sync__ 
#      define  mpi_file_write_at_all_      pmpi_file_write_at_all__ 
#      define  mpi_file_write_at_          pmpi_file_write_at__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_file_close_             pmpi_file_close 
#      define  mpi_file_create_errhandler_ pmpi_file_create_errhandler 
#      define  mpi_file_delete_            pmpi_file_delete 
#      define  mpi_file_get_amode_         pmpi_file_get_amode 
#      define  mpi_file_get_errhandler_    pmpi_file_get_errhandler 
#      define  mpi_file_get_group_         pmpi_file_get_group 
#      define  mpi_file_get_info_          pmpi_file_get_info 
#      define  mpi_file_get_size_          pmpi_file_get_size 
#      define  mpi_file_get_view_          pmpi_file_get_view 
#      define  mpi_file_iread_at_          pmpi_file_iread_at 
#      define  mpi_file_iwrite_at_         pmpi_file_iwrite_at 
#      define  mpi_file_open_              pmpi_file_open 
#      define  mpi_file_read_at_all_       pmpi_file_read_at_all 
#      define  mpi_file_read_at_           pmpi_file_read_at 
#      define  mpi_file_set_errhandler_    pmpi_file_set_errhandler 
#      define  mpi_file_set_info_          pmpi_file_set_info 
#      define  mpi_file_set_size_          pmpi_file_set_size 
#      define  mpi_file_set_view_          pmpi_file_set_view 
#      define  mpi_file_sync_              pmpi_file_sync 
#      define  mpi_file_write_at_all_      pmpi_file_write_at_all 
#      define  mpi_file_write_at_          pmpi_file_write_at 


#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define  mpi_file_close_             pmpi_file_close_ 
#      define  mpi_file_create_errhandler_ pmpi_file_create_errhandler_ 
#      define  mpi_file_delete_            pmpi_file_delete_ 
#      define  mpi_file_get_amode_         pmpi_file_get_amode_ 
#      define  mpi_file_get_errhandler_    pmpi_file_get_errhandler_ 
#      define  mpi_file_get_group_         pmpi_file_get_group_ 
#      define  mpi_file_get_info_          pmpi_file_get_info_ 
#      define  mpi_file_get_size_          pmpi_file_get_size_ 
#      define  mpi_file_get_view_          pmpi_file_get_view_ 
#      define  mpi_file_iread_at_          pmpi_file_iread_at_ 
#      define  mpi_file_iwrite_at_         pmpi_file_iwrite_at_ 
#      define  mpi_file_open_              pmpi_file_open_ 
#      define  mpi_file_read_at_all_       pmpi_file_read_at_all_ 
#      define  mpi_file_read_at_           pmpi_file_read_at_ 
#      define  mpi_file_set_errhandler_    pmpi_file_set_errhandler_ 
#      define  mpi_file_set_info_          pmpi_file_set_info_ 
#      define  mpi_file_set_size_          pmpi_file_set_size_ 
#      define  mpi_file_set_view_          pmpi_file_set_view_ 
#      define  mpi_file_sync_              pmpi_file_sync_ 
#      define  mpi_file_write_at_all_      pmpi_file_write_at_all_ 
#      define  mpi_file_write_at_          pmpi_file_write_at_ 



#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#      define  mpi_file_close_             MPI_FILE_CLOSE          
#      define  mpi_file_create_errhandler_ MPI_FILE_CREATE_ERRHANDLER   
#      define  mpi_file_delete_            MPI_FILE_DELETE         
#      define  mpi_file_get_amode_         MPI_FILE_GET_AMODE      
#      define  mpi_file_get_errhandler_    MPI_FILE_GET_ERRHANDLER   
#      define  mpi_file_get_group_         MPI_FILE_GET_GROUP      
#      define  mpi_file_get_info_          MPI_FILE_GET_INFO       
#      define  mpi_file_get_size_          MPI_FILE_GET_SIZE       
#      define  mpi_file_get_view_          MPI_FILE_GET_VIEW       
#      define  mpi_file_iread_at_          MPI_FILE_IREAD_AT       
#      define  mpi_file_iwrite_at_         MPI_FILE_IWRITE_AT      
#      define  mpi_file_open_              MPI_FILE_OPEN           
#      define  mpi_file_read_at_all_       MPI_FILE_READ_AT_ALL    
#      define  mpi_file_read_at_           MPI_FILE_READ_AT        
#      define  mpi_file_set_errhandler_    MPI_FILE_SET_ERRHANDLER   
#      define  mpi_file_set_info_          MPI_FILE_SET_INFO       
#      define  mpi_file_set_size_          MPI_FILE_SET_SIZE       
#      define  mpi_file_set_view_          MPI_FILE_SET_VIEW       
#      define  mpi_file_sync_              MPI_FILE_SYNC           
#      define  mpi_file_write_at_all_      MPI_FILE_WRITE_AT_ALL    
#      define  mpi_file_write_at_          MPI_FILE_WRITE_AT   


#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_file_close_             mpi_file_close__ 
#      define  mpi_file_create_errhandler_ mpi_file_create_errhandler__ 
#      define  mpi_file_delete_            mpi_file_delete__ 
#      define  mpi_file_get_amode_         mpi_file_get_amode__ 
#      define  mpi_file_get_errhandler_    mpi_file_get_errhandler__ 
#      define  mpi_file_get_group_         mpi_file_get_group__ 
#      define  mpi_file_get_info_          mpi_file_get_info__ 
#      define  mpi_file_get_size_          mpi_file_get_size__ 
#      define  mpi_file_get_view_          mpi_file_get_view__ 
#      define  mpi_file_iread_at_          mpi_file_iread_at__ 
#      define  mpi_file_iwrite_at_         mpi_file_iwrite_at__ 
#      define  mpi_file_open_              mpi_file_open__ 
#      define  mpi_file_read_at_all_       mpi_file_read_at_all__ 
#      define  mpi_file_read_at_           mpi_file_read_at__ 
#      define  mpi_file_set_errhandler_    mpi_file_set_errhandler__ 
#      define  mpi_file_set_info_          mpi_file_set_info__ 
#      define  mpi_file_set_size_          mpi_file_set_size__ 
#      define  mpi_file_set_view_          mpi_file_set_view__ 
#      define  mpi_file_sync_              mpi_file_sync__ 
#      define  mpi_file_write_at_all_      mpi_file_write_at_all__ 
#      define  mpi_file_write_at_          mpi_file_write_at__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_file_close_             mpi_file_close 
#      define  mpi_file_create_errhandler_ mpi_file_create_errhandler 
#      define  mpi_file_delete_            mpi_file_delete 
#      define  mpi_file_get_amode_         mpi_file_get_amode 
#      define  mpi_file_get_errhandler_    mpi_file_get_errhandler 
#      define  mpi_file_get_group_         mpi_file_get_group 
#      define  mpi_file_get_info_          mpi_file_get_info 
#      define  mpi_file_get_size_          mpi_file_get_size 
#      define  mpi_file_get_view_          mpi_file_get_view 
#      define  mpi_file_iread_at_          mpi_file_iread_at 
#      define  mpi_file_iwrite_at_         mpi_file_iwrite_at 
#      define  mpi_file_open_              mpi_file_open 
#      define  mpi_file_read_at_all_       mpi_file_read_at_all 
#      define  mpi_file_read_at_           mpi_file_read_at 
#      define  mpi_file_set_errhandler_    mpi_file_set_errhandler 
#      define  mpi_file_set_info_          mpi_file_set_info 
#      define  mpi_file_set_size_          mpi_file_set_size 
#      define  mpi_file_set_view_          mpi_file_set_view 
#      define  mpi_file_sync_              mpi_file_sync 
#      define  mpi_file_write_at_all_      mpi_file_write_at_all 
#      define  mpi_file_write_at_          mpi_file_write_at 


#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */


#endif  /* DECLARE_MPIIO */

#endif
