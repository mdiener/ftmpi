
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gaberiel	 <egaberiel@cs.utk.edu>
			Thara Angskun	 <angskun@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This file contains misc routines that are general support routines
 These are no general enough to be put in Harness/share (yet)

 GEF.
 */


#include <stdlib.h>
#include <stdio.h>
#include "debug.h"

/* global list operations on our gid lists */
/* these are short hand ops for simple things */
/* note lpp means pointer list-array pointer and lp means list-array pointer */

void ftmpi_array_free (int** lpp)
{
  if (*lpp) {
    _FREE(*lpp);
    *lpp = NULL;
  }
}
    
int ftmpi_array_mkspace (int** lpp, int n)
{
  if (n<0) return (n);
  if (*lpp) ftmpi_array_free (lpp);
  *lpp = (int*)_CALLOC(n,sizeof(int));
  return ( (*lpp) == NULL ? 0 : n );
}

/* list is 's' big and add 'id' */
int ftmpi_array_add (int *lp, int s, int id)
{
int i;
int found;

/* first check to see if its already in */
/* if it is just return its index and NOP */
for(i=0;i<s;i++) if (lp[i]==id) return (i);

/* if not find a slot and add it */
/* could be a linked list but this still quick */
found=0;
for(i=0;i<s;i++)
    if (lp[i]==0) {
        lp[i] = id;
        return (i);
        } /* if */

/* else no space */
return (-1);
}

/* rm removes a gid from list and returns its old position */
int ftmpi_array_rm (int *lp, int s, int id)
{
int i;

for(i=0;i<s;i++)
    if (lp[i]==id) {
        lp[i]=0;
        return (i);
        }

return (-1);
}


/* returns your location in the list or -1 if non */
int ftmpi_array_rank (int *lp, int s, int id)
{
int i;
for(i=0;i<s;i++)
    if (lp[i]==id)
        return (i);

return (-1);
}

/* pack is a short hand routine that takes all gids and moves them to the */
/* start of the space without leaving any holes. Used by com operations */
/* returns number of entries packed at start of list */
int ftmpi_array_pack (int *lp, int s)
{
int i;
int j;  /* next and count combo */
int copy;

/* there is a slow way and a slower way */
/* here we build a packed list as we go */

j = 0; /* next location */
for (i=0;i<s;i++) { /* search through list */
    if (lp[i]) {
        copy = lp[i];
        lp[i] = 0;  /* if you don't clear it you leave multiple copies */
        lp[j] = copy;
        j++;
        }
    } /* whole list */

return (j); /* how many we have */
}

/* debug list routine */
void ftmpi_array_dump (int *lp, int s)
{
int i;

for (i=0;i<s;i++) printf("0x%x ", lp[i]);
printf("\n\n");
}

int ftmpi_array_count (int *lp, int s)
{
int i, c;
c = 0;
for (i=0;i<s;i++) if (lp[i]) c++;
return (c);
}

int ftmpi_array_first (int *lp, int s)
{
int i;
for (i=0;i<s;i++) if (lp[i]) return (i);
return (-1);
}

int ftmpi_array_extent (int *lp, int s)
{
int i, c;
c = -1;
for (i=0;i<s;i++) if (lp[i]) c = i;
return (c);
}

/* copy slp -> tlp */
int ftmpi_array_copy (int *tlp, int *slp, int s)
{
int i, c;
c = 0;
for (i=0;i<s;i++) tlp[i] = slp[i];
return (c);
}













