/*
        HARNESS G_HCORE
        HARNESS FT_MPI

        Innovative Computer Laboratory,
        University of Tennessee,
        Knoxville, TN, USA.

        harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
                        Graham E Fagg <fagg@cs.utk.edu>
						Edgar Gabriel <egabriel@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/


/*
 This is the simple header file needed by each of the MPI API library files
 Only the base ft-mpi-lib.c file doesn't need this as it is the owner
 of the global variables that are listed here as externs

 GEF June2002
*/

#ifndef _FT_MPI_H_LIB
#define _FT_MPI_H_LIB

#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <unistd.h>
#include "debug.h"
#include "mpi.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-sys.h"

#include "ft-mpi-profile.h"

/* #include "ft-mpi-ddt.h" */

#include "ft-mpi-check.h"	/* check arg macros */


/* lowlevel coms driver DS */
#include "snipe2.h"

/* connection library prototypes */
#include "ft-mpi-conn.h"

/* Collective internal offset tags */
#define USR_COLL_BAR		0x7000
#define	USR_COLL_BCAST		0x7100
#define USR_COLL_GATHER 	0x7200
#define USR_COLL_REDUCE		0x7300
#define USR_COLL_SCAN		0x7400
#define USR_COLL_SCATTER 	0x7500
#define USR_COLL_ALLTOALL 	0x7600
#define USR_COLL_ALLTOALLV 	0x7700

/* #define USR_COLL_ 0x80 */

extern  int MPI_INITIALISED;
extern  int MPI_EXITING;

/* used to indicate an error to a collective */
extern  int failuredetected;

extern int my_gid;    		/* global id */
extern int p_gid;           /* global id for the parent of MCW */

#define CALL_ERRHANDLER(COMM, ERROR) \
do { \
  MPI_Comm_call_errhandler( (COMM), (ERROR) ); \
} while(0);

#define BANG {fprintf(stderr,"BANG: %s line %d\n\n", __FILE__, __LINE__); exit (-1); }

#define RETURNERR(comm,ret) {MPI_Comm_call_errhandler(comm,ret);return(ret);}

#endif /*   _FT_MPI_H_LIB */
