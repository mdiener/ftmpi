/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
*/


#include "stdio.h"
#include "stdlib.h"
#include <sys/time.h>
#include <unistd.h>

#include "mpi.h"
#include "ft-mpi-modes.h"
#include "ft-mpi-sys.h"

#include "ft-mpi-profile.h"

#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-buffer.h"
#include "ft-mpi-attr.h"
#include "ft-mpi-coll.h"
#include "debug.h"

int MPI_INITIALISED = 0;
int MPI_EXITING = 0;

/* used to indicate an error to a collective */
int failuredetected = 0;

int my_gid=0;		/* global id */
int p_gid=0;		/* global id for the parent of MCW */

extern comm_info_t _ftmpi_comm[MAXCOMS]; 

#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Initialized          = PMPI_Initialized
/* #    pragma weak  MPI_Pcontrol             = PMPI_Pcontrol */
#    pragma weak  MPI_Abort                = PMPI_Abort
#    pragma weak  MPI_Finalize             = PMPI_Finalize
#    pragma weak  MPI_Init                 = PMPI_Init

#endif

#    define MPI_Initialized           PMPI_Initialized
/* #    define MPI_Pcontrol              PMPI_Pcontrol */
#    define MPI_Abort                 PMPI_Abort
#    define MPI_Finalize              PMPI_Finalize
#    define MPI_Init                  PMPI_Init


/* MPI_Init function */

int MPI_Init (argc, argv)
int *argc;
char **argv[];
{
	int rc;

	int go=MPI_SUCCESS;

	if (MPI_INITIALISED) RETURNERR (MPI_COMM_WORLD, MPI_ERR_OTHER);

	MPI_EXITING  = 0;

	/* Do startup stage by stage */
	rc = 0;

	/* create internal buffers and do NS stuff */
	/* and remember my exe name in case I have to spawn someone */
	rc = ftmpi_sys_init (argc, argv);	

	rc = ftmpi_sys_init_startup_state (600);	/* 10 minutes to get startup info */

	/* At this point we know if we were a recovered/spawned node or not */

	if (rc==1) go = MPI_SUCCESS;
	if (rc==0) go = MPI_INIT_RESTARTED_NODE;

	/* the following does all the mpi related stuff needed */
	/* i.e. all queues, coms, conns and error classes + ddt structures */
	ftmpi_sys_start_mpi ();  

	/* although we do not have a failure yet (I hope) */
	/* we do the recovery loop until we have a clean state */

	rc = ftmpi_sys_recovery_loop ();

	if (rc<0)  go = MPI_INIT_PATHALOGICAL;	/* rare but possible */

	if (rc>=0) MPI_INITIALISED = 1;

	/* last thing we do is cache our GID */
	my_gid = ftmpi_sys_get_gid();

	/* for debugging purposes */

/* 	ftmpi_sys_force_fullyconnected (); */

	return (go); 
}

int MPI_Initialized(int *flag)
{
	/* cannot call error handler for this one as MCW might not exist! */

	if (!flag) return (MPI_ERR_ARG);	/* bad ptr check */

	*flag=MPI_INITIALISED;

	return(MPI_SUCCESS);
}


int	MPI_Finalize ()
{
  int flag, size, i;
  void *buffer;
  MPI_Comm shadow;
  int dummy;

  if (!MPI_INITIALISED) return (MPI_ERR_COMM);
  MPI_EXITING = 1;


  flag = ftmpi_buffer_isattached ();
  if ( flag ) {
    /* User didn't deallocat buffer, so we have to do it.
       Note, internally MPI_Wait is called on all not-finished
       buffered sends */
    size = ftmpi_buffer_size ();
    ftmpi_buffer_detach ( &buffer, &size );
  }

  /* This part implements section 4.8 of the MPI-2 document,
     saying that in MPI_Finalize the delete callback functions
     attached to MPI_COMM_SELF will be executed */
  if (_ftmpi_comm[MPI_COMM_SELF].nattr > 0 ) {
    for ( i = 0 ; i < _ftmpi_comm[MPI_COMM_SELF].nattr; i++ )
      ftmpi_keyval_delete ( _ftmpi_comm[MPI_COMM_SELF].attrs[i], MPI_COMM_SELF, &dummy );
  }

  ftmpi_sys_check_for_mpi_abort ();

  shadow = ftmpi_com_get_shadow ( MPI_COMM_WORLD );
  if ( shadow != MPI_COMM_NULL )
    ftmpi_coll_intra_barrier_linear (shadow);
  else
    ftmpi_coll_intra_barrier_linear (MPI_COMM_WORLD);

  ftmpi_sys_leave (0, 1);	
  /* this stops notify events and cleans up NS DB */
  /* 0 = do not send death event via notifier */
  /* the second 1 = remove runstate rec if you are the leader only */
  ftmpi_ddt_finalize();
  
  fflush(stdout);
  fflush(stderr);

  /* DUMP_ALLOCATED_MEMORY(); */

  return (MPI_SUCCESS);
}



int	MPI_Abort (MPI_Comm comm, int errorcode)
{

  /* If they have not started MPI yet we do have a problem */ 

  if (!MPI_INITIALISED) {
    fprintf(stderr,"FT-MPI: You have called MPI_ABORT before MPI_INIT\n");
    fprintf(stderr,"THIS may cause serious problems and is an incorrect program\n");
    fflush(stderr);
    exit (MPI_ERR_COMM);
  }
  
  CHECK_COM(comm);
  MPI_EXITING = -1;
  /* 	MPI_Barrier (MPI_COMM_WORLD); */
  
  /* we post a state change record to indicate that ABORT */
  /* has been posted, then allow our exit via notifier to cause an event */
  ftmpi_sys_post_state (FT_STATE_ABORT, NULL, 0);
  
  /* then other tasks will catch the abort from within the recover loop */
  /* They will exit normally via ftmpi_sys_leave */
  
  ftmpi_sys_leave (1, 0);
  /* this stops notify events and cleans up NS DB */
  /* 1 = we allow a death event to be sent */
  /* but we do not clean up the runstate record */
  
  fflush(stdout);
  fflush(stderr);
  exit (errorcode);	/* only we return the error code */
  /* all others return MPI_ERR_ABORT_CALLED */
  return (MPI_SUCCESS);  /* just to satisfy the compiler */
}

