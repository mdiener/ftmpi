
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>
			

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* This will oneday contain all the MPI_Type_* calls and their related 
   functions
Graham STR2002
*/


#include "stdlib.h"
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"
#include "debug.h"

#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Address              = PMPI_Address
#    pragma weak  MPI_Get_count            = PMPI_Get_count
#    pragma weak  MPI_Get_elements         = PMPI_Get_elements
#    pragma weak  MPI_Type_commit          = PMPI_Type_commit
#    pragma weak  MPI_Type_contiguous      = PMPI_Type_contiguous
#    pragma weak  MPI_Type_extent          = PMPI_Type_extent
#    pragma weak  MPI_Type_free            = PMPI_Type_free
#    pragma weak  MPI_Type_hindexed        = PMPI_Type_hindexed
#    pragma weak  MPI_Type_hvector         = PMPI_Type_hvector
#    pragma weak  MPI_Type_indexed         = PMPI_Type_indexed
#    pragma weak  MPI_Type_lb              = PMPI_Type_lb
#    pragma weak  MPI_Type_size            = PMPI_Type_size
#    pragma weak  MPI_Type_struct          = PMPI_Type_struct
#    pragma weak  MPI_Type_ub              = PMPI_Type_ub
#    pragma weak  MPI_Type_vector          = PMPI_Type_vector


#    pragma weak MPI_Type_get_envelope	   = PMPI_Type_get_envelope
#    pragma weak MPI_Type_get_contents	   = PMPI_Type_get_contents
#    pragma weak MPI_Type_create_subarray  = PMPI_Type_create_subarray 
#    pragma weak MPI_Type_create_darray	   = PMPI_Type_create_darray 

#endif

#    define  MPI_Address               PMPI_Address
#    define  MPI_Get_count             PMPI_Get_count
#    define  MPI_Get_elements          PMPI_Get_elements
#    define  MPI_Type_commit           PMPI_Type_commit
#    define  MPI_Type_contiguous       PMPI_Type_contiguous
#    define  MPI_Type_extent           PMPI_Type_extent
#    define  MPI_Type_free             PMPI_Type_free
#    define  MPI_Type_hindexed         PMPI_Type_hindexed
#    define  MPI_Type_hvector          PMPI_Type_hvector
#    define  MPI_Type_indexed          PMPI_Type_indexed
#    define  MPI_Type_lb               PMPI_Type_lb
#    define  MPI_Type_size             PMPI_Type_size
#    define  MPI_Type_struct           PMPI_Type_struct
#    define  MPI_Type_ub               PMPI_Type_ub
#    define  MPI_Type_vector           PMPI_Type_vector

#    define MPI_Type_get_envelope      PMPI_Type_get_envelope
#    define MPI_Type_get_contents      PMPI_Type_get_contents
#    define MPI_Type_create_subarray   PMPI_Type_create_subarray 
#    define MPI_Type_create_darray     PMPI_Type_create_darray 


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* For improving the readability */

#define XTROOT xtemp->root_add
#define TROOT  temp->root_add
#define ALIGN_TO(ptr, type)\
  ((ptr + sizeof(type) - 1) & ~(sizeof(type) - 1))

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Get_count(MPI_Status *status,MPI_Datatype datatype,int *count)
{
  int size;
  int ret;

  *count = -1;

  CHECK_MPIINIT;
  CHECK_DDT(MPI_COMM_WORLD,datatype,1);

  ret = ftmpi_mpi_type_size(datatype,&size);

  if(size == 0) {
    *count = 0;
    return(MPI_SUCCESS);
  }
    

  if(status->msglength%size != 0){
    *count = MPI_UNDEFINED;
    printf("Get_count: TAG=%d ERROR= %d msglength=%d\n",
	   status->MPI_TAG, status->MPI_ERROR, status->msglength);
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }

  *count = status->msglength/size;
  return(ret);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Get_elements(MPI_Status * status,MPI_Datatype datatype,int * count)
{
  int size;
  int ret;

  CHECK_MPIINIT;
  CHECK_DDT(MPI_COMM_WORLD,datatype,1);

  ret = ftmpi_mpi_type_size(datatype,&size);
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
    
  if(size > 0){
    ret = ftmpi_ddt_get_element_count(datatype,status->msglength,
				      count,NULL,-1);
  }
  else 
    *count = 0;

  return(MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_free(MPI_Datatype * ddt_id)
{
  int ret;

  CHECK_MPIINIT;

  if((*ddt_id > 0 && *ddt_id < FTMPI_DDT_B_DT_MAX) || 
     (*ddt_id >= 500 && *ddt_id < 500+FTMPI_DDT_B_DT_ADD)){
    RETURNERR(MPI_COMM_WORLD, MPI_ERR_TYPE);
  }
  CHECK_DDT(MPI_COMM_WORLD,*ddt_id,0);

  ret = ftmpi_mpi_type_free(ddt_id);
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_free(MPI_Datatype *ddt_id)
{
  FTMPI_DDT_R_ITEM * temp = NULL,* ptemp = NULL;

  if((*ddt_id > 0 && *ddt_id < FTMPI_DDT_B_DT_MAX)|| 
     (*ddt_id >= 500 && *ddt_id < 500+FTMPI_DDT_B_DT_ADD)){
    return(MPI_SUCCESS);
  }

  temp = FTMPI_DDT_ROOT[(*ddt_id)%FTMPI_DDT_HASH_SIZE];

  /* Note: TROOT is a macro for temp->root_add */
  if(temp != NULL){
    while(temp != NULL){
      if(temp->dt == *ddt_id){
        temp->uses--;
	if(temp->uses == 0) {
	  TROOT->ref_cnt--;
	  if(TROOT->ref_cnt == 0){
	    if(TROOT->first_e != NULL){
	      TROOT->last_e = TROOT->first_e->next;
	      ftmpi_mpi_type_free(&TROOT->first_e->dt);
	      _FREE(TROOT->first_e);
	      while(TROOT->last_e != NULL){
		
		TROOT->first_e = TROOT->last_e;
		
		TROOT->last_e = TROOT->last_e->next;
		ftmpi_mpi_type_free(&TROOT->first_e->dt);
		_FREE(TROOT->first_e);
	      }
	    }
	    
	    if(TROOT->i != NULL)_FREE(TROOT->i);
	    if(TROOT->a != NULL)_FREE(TROOT->a);
	    if(TROOT->d != NULL)_FREE(TROOT->d);
	    
	    _FREE(TROOT);

	    if(ptemp != NULL){
	      ptemp->next = temp->next;
	    }
	    else {
	      FTMPI_DDT_ROOT[(*ddt_id)%FTMPI_DDT_HASH_SIZE] = temp->next;
	    }
	  }
	  _FREE(temp);
	  *ddt_id = -1;
	  break;
	}
      }
      ptemp = temp;
      temp = temp->next;
    }
  }

  *ddt_id = MPI_DATATYPE_NULL;
  return(MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_size ( MPI_Datatype type, int *size)
{
  int ret;

  CHECK_MPIINIT;
  CHECK_DDT(MPI_COMM_WORLD, type,0);

  ret = ftmpi_mpi_type_size ( type, size );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}


int ftmpi_mpi_type_size(MPI_Datatype type, int * size)
{
  FTMPI_DDT_R_ITEM * temp = NULL;

  if(type < FTMPI_DDT_B_DT_MAX)
    *size = _ftmpi_btype[type]->size;
  else {
    temp = ftmpi_ddt_get_root(type);
    if(temp == NULL ){
      return ( MPI_ERR_TYPE );
    }
    *size = temp->size;
  }
  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_extent ( MPI_Datatype type, MPI_Aint *extent)
{   
  int ret;

  CHECK_MPIINIT;
  CHECK_DDT(MPI_COMM_WORLD,type,0);
  
  ret = ftmpi_mpi_type_extent ( type, extent );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_extent ( MPI_Datatype type, MPI_Aint *extent )
{
  FTMPI_DDT_R_ITEM * temp = NULL;
  int size,ret_var;
  
  if(type < FTMPI_DDT_B_DT_MAX){
    ret_var = ftmpi_mpi_type_size(type,&size);
    if(ret_var == MPI_SUCCESS){
      *extent = size;
      return(MPI_SUCCESS);
    }
    return ( MPI_ERR_TYPE );
  } 
  else {
    temp = ftmpi_ddt_get_root(type);
    if(temp == NULL ){
      return ( MPI_ERR_TYPE );
    }
    else {
      *extent = temp->extent;
      return(MPI_SUCCESS);
    }
  } 

  return ( MPI_SUCCESS );
}   
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_struct( int cnt, int *array_block, MPI_Aint *array_disp,
		     MPI_Datatype *array_types, MPI_Datatype *ret_handle)
{   
  int ret = 0;
  int i;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  for(i=0;i<cnt;i++){
    if(array_block[i] < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    CHECK_DDT(MPI_COMM_WORLD,array_types[i],0);
  }

  ret = ftmpi_mpi_type_struct ( cnt, array_block, array_disp,
				array_types, ret_handle );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_struct( int cnt, int *array_block, MPI_Aint *array_disp, 
			   MPI_Datatype *array_types, MPI_Datatype *ret_handle)
{
  FTMPI_DDT_E_ITEM * bs_xdr_temp = NULL,* e_temp = NULL;
  FTMPI_DDT_R_ITEM * xtemp = NULL,* temp = NULL;
  int i, size=0, total_size=0, total_xdr_size = 0;
  int dt_total_size=0, last=0, j, dtm = 1;
  int last_hash=0, free_sp, bs_xdr_last = -1, bs_xdr_type=-1, stemp=0;
  int ub=0, lb=0;
  int last_ext=0;
  int *i_a[2];
  MPI_Aint *a_a[1];

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  *ret_handle = FTMPI_DDT_NEXT_DATATYPE ++;
  if(FTMPI_DDT_NEXT_DATATYPE == 501){  /* create a new list for datatypes */
    /*  FTMPI_DDT_DATATYPE_HANDLE = */ ftmpi_ddt_create_list(20);
  } 
  xtemp = ftmpi_ddt_add_root(*ret_handle,1);
  if (xtemp == NULL){
    return ( MPI_ERR_TYPE);
  } 

  xtemp->ub_set = 0;
  xtemp->lb_set = 0;
  xtemp->ub = 0;
  xtemp->lb = 0;
  xtemp->contig = 0;
  xtemp->elem_cnt = 0;


  for(i=0;i<cnt;i++){
    if(array_types[i] < FTMPI_DDT_B_DATATYPE_TOTAL){                                                               /* BDT */
      if(array_types[i] != MPI_UB && array_types[i] != MPI_LB){
	
	if ( array_block[i] == 0 )
	  continue;

        /*************************************************************************/
        if(XTROOT->t_comp >= FTMPI_DDT_COMP_MAX){
          ftmpi_ddt_check_array(XTROOT->comp,&XTROOT->t_comp);
          if(XTROOT->t_comp >= FTMPI_DDT_COMP_MAX){
            XTROOT->t_comp = -1;
          }
        }
        /*************************************************************************/

      
        size = _ftmpi_btype[array_types[i]]->size;
        total_size += size * array_block[i];
        total_xdr_size += _ftmpi_btype[array_types[i]]->xdr_size
	  * array_block[i];
 
        dt_total_size = size * array_block[i];
      
        XTROOT->type_count[array_types[i]] += array_block[i];
      
        if(last < array_disp[i]+dt_total_size-1){
          last = array_disp[i]+dt_total_size-1;
        }
  
        free_sp = (array_disp[i] - stemp);
        if(dtm == 1){
          last_hash = (((array_types[i]+i+1)*array_block[i])+free_sp)*dtm;
          dtm +=2;
        }
        else {
          last_hash = last_hash ^ ((((array_types[i]+i+1)*array_block[i])+
				    free_sp)*dtm);
          dtm +=2;
        }
      
        /*************************************************************************/
        if(XTROOT->t_comp >= 0){
          XTROOT->comp[XTROOT->t_comp].dt    = array_types[i];
          XTROOT->comp[XTROOT->t_comp].count = array_block[i];
          XTROOT->comp[XTROOT->t_comp].disp  = array_disp[i];

          if(i+1<cnt){
            if(array_types[i+1] == MPI_UB){  
              XTROOT->comp[XTROOT->t_comp].extent = array_disp[i+1];
            }
            else 
              XTROOT->comp[XTROOT->t_comp].extent = ALIGN_TO(dt_total_size, int);
          }
          else 
            XTROOT->comp[XTROOT->t_comp].extent = ALIGN_TO(dt_total_size, int);
	  
          XTROOT->comp[XTROOT->t_comp].nitems   = 1;
          XTROOT->comp[XTROOT->t_comp].nrepeats = 1;
      
          XTROOT->t_comp++;
        }
        /******************************************************************************/
        bs_xdr_type = ftmpi_ddt_bs_xdr_det(size);
        e_temp = ftmpi_ddt_add_element(xtemp,array_types[i]);
      }
      else {
        if(array_types[i] == MPI_UB){
          if(xtemp->ub_set != 0){
            if(xtemp->ub < array_disp[i]){
              xtemp->ub = array_disp[i];
            }
          }
          else {
            xtemp->ub_set = 1;
            xtemp->ub = array_disp[i];
          }

        }
        else{
          if(xtemp->lb_set != 0){
            if(xtemp->lb > array_disp[i]){
              xtemp->lb = array_disp[i];
            }
          }
          else {
            xtemp->lb_set = 1;
            xtemp->lb = array_disp[i];
          }
        }
      }
    }
    else {                                            
      /********************************************************   NON_BDT */
      if ( array_block[i] == 0 ) 
	continue;

      temp = ftmpi_ddt_get_root(array_types[i]);
    
      if(temp == NULL){ 
	/* Datatype not known, hopefully catched on the previous level! */   

        ftmpi_mpi_type_free(ret_handle);
        fprintf(stderr, "FTMPI: type_struct: datatype %d unknown\n",array_types[i]);
        *ret_handle = -1;
        FTMPI_DDT_NEXT_DATATYPE--;
        return ( MPI_ERR_TYPE);
      }
    
      size            = temp->size;
      total_size     += temp->size * array_block[i];
      total_xdr_size += temp->xdr_size*array_block[i];
    
      if(last < array_disp[i]+(temp->extent*(array_block[i]-1))+temp->last)
        last = array_disp[i]+(temp->extent*(array_block[i]-1))+temp->last;
      
      for(j=1;j<FTMPI_DDT_B_DATATYPE_TOTAL;j++)
        XTROOT->type_count[j] += TROOT->type_count[j]*array_block[i];
          
      free_sp = (array_disp[i] - stemp);

      /************************************************/
      if(dtm == 1){
        last_hash = (((temp->hash + i + 1) * array_block[i]) + free_sp) * dtm;
        dtm +=2;
      }
      else {
        last_hash = last_hash ^ ((((temp->hash+i+1) * array_block[i])+free_sp) * dtm);
        dtm +=2;
      }
      /************************************************/
    
      if(TROOT->t_comp >= 0 && XTROOT->t_comp >= 0){
        for(j=0;j<TROOT->t_comp;j++){
	  
          if(XTROOT->t_comp >= FTMPI_DDT_COMP_MAX){
            ftmpi_ddt_check_array(XTROOT->comp,&XTROOT->t_comp);
            if(XTROOT->t_comp >= FTMPI_DDT_COMP_MAX){
              XTROOT->t_comp = -1;
              break;
            }
          }

          if(XTROOT->t_comp < FTMPI_DDT_COMP_MAX && XTROOT->t_comp >= 0){
            XTROOT->comp[XTROOT->t_comp].dt    = TROOT->comp[j].dt;
            XTROOT->comp[XTROOT->t_comp].count = TROOT->comp[j].count;
            XTROOT->comp[XTROOT->t_comp].disp  = TROOT->comp[j].disp + array_disp[i];
            XTROOT->comp[XTROOT->t_comp].extent = TROOT->comp[j].extent; 
            XTROOT->comp[XTROOT->t_comp].nitems = TROOT->t_comp;
	    XTROOT->comp[XTROOT->t_comp].nrepeats = TROOT->comp[j].nrepeats*array_block[i];
						
            last_ext = TROOT->comp[j].nrepeats;
            XTROOT->t_comp++;

#ifdef DEBUG_DDT
            printf("DT %d CNT %d DISP %d EXT %d ODT %d TCNT %d\n",
		   XTROOT->comp[XTROOT->t_comp-1].dt,
		   XTROOT->comp[XTROOT->t_comp-1].count,
		   XTROOT->comp[XTROOT->t_comp-1].disp,
		   XTROOT->comp[XTROOT->t_comp-1].extent,
		   XTROOT->comp[XTROOT->t_comp-1].nitems,
		   XTROOT->comp[XTROOT->t_comp-1].nrepeats);
#endif
          }
          else {
            XTROOT->t_comp = -1;
            break;
          }
        }
	if(XTROOT->t_comp >0) 
	  XTROOT->comp[XTROOT->t_comp-1].extent =  temp->extent/last_ext;

        bs_xdr_type = temp->bs_xdr_type;
      }
      else 
        XTROOT->t_comp = -1;

      bs_xdr_type = temp->bs_xdr_type;
      e_temp = ftmpi_ddt_add_element(xtemp,array_types[i]);
      temp->uses++;
  

      if(temp->lb_set != 0){
        if(xtemp->lb_set != 0){
          if(temp->lb+array_disp[i] < xtemp->lb)
            xtemp->lb = temp->lb+array_disp[i];
        }
        else {
          xtemp->lb_set = 1;
          xtemp->lb = temp->lb+array_disp[i];
        }
      }

      if(temp->ub_set != 0){
        if(xtemp->ub_set != 0){
          if(array_disp[i]+temp->extent*(array_block[i]-1)+temp->ub > xtemp->ub)
            xtemp->ub = array_disp[i]+temp->extent*(array_block[i]-1)+temp->ub;
        }
        else {
          xtemp->ub_set = 1;
          xtemp->ub = array_disp[i]+temp->extent*(array_block[i]-1)+temp->ub;
        }
      }
    }

    stemp = last;

    if(e_temp != NULL){
      e_temp->dt     = array_types[i];
      e_temp->size   = size;
      e_temp->extent = ALIGN_TO(((size * array_block[i])+array_disp[i]), int);
      e_temp->count   = array_block[i];
      e_temp->padding = array_disp[i];
  
      e_temp->bs_xdr_type  = bs_xdr_type; 
      e_temp->bs_xdr_count = array_block[i];


#ifdef DEBUG_DDT
      printf("ADDED size:%d count:%d extent:%d DT:%d padding:%d last:%d\n",
	     e_temp->size,e_temp->count,e_temp->extent,e_temp->dt,
	     e_temp->padding,last);
#endif
      /***********************************************************/
      if(bs_xdr_temp == NULL){    /* HAS NOT YET BEEN USED */
        bs_xdr_temp = e_temp;
        bs_xdr_last = e_temp->bs_xdr_type;
        xtemp->bs_xdr_type  = e_temp->bs_xdr_type;
        xtemp->bs_xdr_count = e_temp->bs_xdr_count;
      }
      else {
        if(bs_xdr_last == e_temp->bs_xdr_type){
          bs_xdr_temp->bs_xdr_count += e_temp->bs_xdr_count;
          if(xtemp->bs_xdr_type != 0)
            xtemp->bs_xdr_count += e_temp->bs_xdr_count;
        }
        else{
          if(xtemp->bs_xdr_type != 0){
            xtemp->bs_xdr_type = 0;
            xtemp->bs_xdr_count = -1;
          }
          bs_xdr_temp = e_temp;
        }
      }
    }
    e_temp = NULL;
    /***********************************************************/
    xtemp->elem_cnt+=array_block[i];
  } 

  xtemp->size = total_size;
  xtemp->xdr_size = total_size;
  size = 0;
  if(xtemp->ub_set == 0){
    for(j=1;j<FTMPI_DDT_B_DT_MAX;j++){
      if((XTROOT->type_count[j] > 0) && (size < _ftmpi_btype[j]->size))
        size = _ftmpi_btype[j]->size;
    }
    if(size > 0)
      ub = (((last)+size)/size)*size;
    else 
      ub = size;

  }
  else 
    ub = xtemp->ub;

  if(xtemp->lb_set != 0)
    lb = xtemp->lb;

  xtemp->extent = ub-lb;
  xtemp->dt     = *ret_handle;
  xtemp->count  = cnt;
  xtemp->last   = last;
  xtemp->hash   = last_hash;
  xtemp->uses   = 1;


  ftmpi_ddt_check_array(XTROOT->comp,&XTROOT->t_comp);

  /* Check whether datatype is contiguous */
  if(XTROOT->t_comp == 1){
    if(XTROOT->comp[0].disp == 0 && xtemp->lb == 0 && 
       xtemp->extent == xtemp->size){
      xtemp->contig = 1;
    }
  }

  /* Set the information for the type decoding functions */
  i_a[0] = &cnt;
  i_a[1] = array_block;
  a_a[0] = array_disp;
  
  ftmpi_ddt_set_args(*ret_handle,i_a,a_a,array_types,MPI_COMBINER_STRUCT);

#ifdef DNU
  printf("%d =================================== %d\n",XTROOT->t_comp,
	 *ret_handle); 
  for(i=0;i<XTROOT->t_comp;i++){ 
    if(i==0){ 
      printf(" i -   dt  cnt dspl  ext itms rpts\n"); 
      printf("-----------------------------------\n"); 
    } 
    printf("%2d - %4d %4d %4d %4d %4d %4d\n",i,XTROOT->comp[i].dt,
	   XTROOT->comp[i].count,XTROOT->comp[i].disp,
	   XTROOT->comp[i].extent,XTROOT->comp[i].nitems,
	   XTROOT->comp[i].nrepeats); 
  } 
  printf("%d =================================== %d\n",XTROOT->t_comp,
	 *ret_handle); 
#endif
    
    
#ifdef DEBUG_DDT
  printf("T_EXT: %5d  --  T_SIZE: %5d T_XDR_SIZE %5d HASH: %5x\n",
	 xtemp->extent,xtemp->size,xtemp->xdr_size,xtemp->hash);
#endif
  return(MPI_SUCCESS);
}   


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_contiguous(int cnt, MPI_Datatype dt, MPI_Datatype *ret_handle)
{
  int ret;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  CHECK_DDT(MPI_COMM_WORLD, dt, 0);

  ret = ftmpi_mpi_type_contiguous ( cnt, dt, ret_handle );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}



int ftmpi_mpi_type_contiguous(int cnt, MPI_Datatype dt, 
			      MPI_Datatype *ret_handle)
{
  int array_blocks[2],array_types[2],ret;
  MPI_Aint array_disp[2];

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  array_types[0]  = dt;
  array_disp[0]   = 0;
  array_blocks[0] = cnt;

  ret = ftmpi_mpi_type_struct(1,array_blocks,array_disp,
			      array_types,ret_handle);
  if(ret == MPI_SUCCESS){ 
    int * i_a[1];

    i_a[0] = &cnt;

    ftmpi_ddt_set_args(*ret_handle,i_a,NULL,&dt,MPI_COMBINER_CONTIGUOUS);
  }

  return ( ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_vector(int cnt, int block_length, int stride, MPI_Datatype dt,
		    MPI_Datatype *ret_handle)
{               
  int ret;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  if(block_length < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  CHECK_DDT(MPI_COMM_WORLD,dt,0);

  ret = ftmpi_mpi_type_vector ( cnt, block_length, stride, dt, ret_handle);
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_vector(int cnt, int block_length, int stride, 
			  MPI_Datatype dt, MPI_Datatype *ret_handle)
{
  int i, size, array_b[20], *array_blocks, ret;
  MPI_Datatype *array_types, array_t[20];
  MPI_Aint array_d[20], *array_disp;
  FTMPI_DDT_R_ITEM * temp;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }
 
  if(cnt > 20){
    array_blocks = (int*)_MALLOC(sizeof(int)*cnt);
    array_disp   = (MPI_Aint*)_MALLOC(sizeof(MPI_Aint)*cnt);
    array_types  = (MPI_Datatype*)_MALLOC(sizeof(MPI_Datatype)*cnt);
  }                                 
  else{                             
    array_blocks = array_b;
    array_types  = array_t;
    array_disp   = array_d; 
  }                       
                        
  
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    size = _ftmpi_btype[dt]->size;
  else{                             
    temp = ftmpi_ddt_get_root(dt);
    if(temp == NULL){               
      printf("ERROR Datatype %d does NOT exist\n",dt);
      return ( MPI_ERR_TYPE);
    }                               
    size = temp->extent;
  }                  
                     
  for(i=0;i<cnt;i++){
    array_blocks[i] = block_length;
    array_disp[i]   = i * (size * stride);
    array_types[i]  = dt;     
  }                                
  ret = ftmpi_mpi_type_struct(cnt,array_blocks,array_disp,
			      array_types,ret_handle);

  if( cnt> 20){
    _FREE(array_blocks);
    _FREE(array_types);
    _FREE(array_disp);
  }

  if(ret == MPI_SUCCESS){ 
    int * i_a[3];

    i_a[0] = &cnt;
    i_a[1] = &block_length;
    i_a[2] = &stride;

    ftmpi_ddt_set_args(*ret_handle,i_a,NULL,&dt,MPI_COMBINER_VECTOR);
  }

  return ( ret );
}                                   

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_hvector(int cnt, int block_length, MPI_Aint stride,
		     MPI_Datatype dt, MPI_Datatype *ret_handle)
{               
  int ret;

  CHECK_MPIINIT;
  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  if(block_length < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  CHECK_DDT(MPI_COMM_WORLD,dt,0);

  ret = ftmpi_mpi_type_hvector ( cnt, block_length, stride,
				 dt, ret_handle );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  
  return ( MPI_SUCCESS );
}
  
int ftmpi_mpi_type_hvector(int cnt, int block_length, MPI_Aint stride,
			   MPI_Datatype dt, MPI_Datatype *ret_handle)
{
  int i, size, array_b[20], *array_blocks, ret;
  MPI_Datatype array_t[20], *array_types;
  MPI_Aint array_d[20], *array_disp; 
  FTMPI_DDT_R_ITEM * temp;

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();

  if(cnt > 20){
    array_blocks = (int *)_MALLOC(sizeof(int)*cnt);
    array_types  = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*cnt);
    array_disp   = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*cnt);
  }                                 
  else{                             
    array_blocks = array_b;
    array_types  = array_t;
    array_disp   = array_d; 
  }

  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    size = _ftmpi_btype[dt]->size;
  else{                             
    temp = ftmpi_ddt_get_root(dt);
    if(temp == NULL){               
      printf("ERROR Datatype %d does NOT exist\n",dt);
      return ( MPI_ERR_TYPE);
    }                               
    size = temp->size;
  }                  
  
  for(i=0;i<cnt;i++){
     array_blocks[i] = block_length;
     array_disp[i]   = i * stride;
     array_types[i]  = dt;     
  }                   
              
  ret = ftmpi_mpi_type_struct(cnt,array_blocks,array_disp,
			      array_types,ret_handle);

  if( cnt> 20){ 
    _FREE(array_blocks);
    _FREE(array_types);
    _FREE(array_disp);
  }

  if(ret == MPI_SUCCESS){ 
    int * i_a[2];
    MPI_Aint * a_a[1];

    i_a[0] = &cnt;
    i_a[1] = &block_length;
    a_a[0] = &stride;

    ftmpi_ddt_set_args(*ret_handle,i_a,a_a,&dt,MPI_COMBINER_HVECTOR);
  }

  return( ret );  
}                                   
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_indexed(int cnt,int *array_blocks, int *array_disp,
		     MPI_Datatype dt, MPI_Datatype *ret_handle)
{               
  int i, ret;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT ==0)
    ftmpi_ddt_init();

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  for(i=0;i<cnt;i++){
    if(array_blocks[i] < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }
  CHECK_DDT(MPI_COMM_WORLD,dt,0);

  ret = ftmpi_mpi_type_indexed ( cnt, array_blocks, array_disp,
				 dt, ret_handle );

  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_indexed(int cnt,int *array_blocks, int *array_disp,
			   MPI_Datatype dt, MPI_Datatype *ret_handle)
{
  int i,size,array_t[20];
  MPI_Aint array_d[20],*array_dp; 
  int * array_types,ret;    
  FTMPI_DDT_R_ITEM * temp;        

  if(cnt > 20){
    array_types = (int *)_MALLOC(sizeof(int)*cnt);
    array_dp    = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*cnt);
  }                                 
  else{                             
    array_types = array_t;
    array_dp    = array_d;
  }                      
                         
  
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    size = _ftmpi_btype[dt]->size;
  else{                             
    temp = ftmpi_ddt_get_root(dt);
    if(temp == NULL){               
      printf("ERROR Datatype %d does NOT exist\n",dt);
      return ( MPI_ERR_TYPE );
    }                               
    size = temp->extent;
  }                  
                     
  for(i=0;i<cnt;i++){
    array_dp[i]    = (MPI_Aint)(array_disp[i] * size);
    array_types[i] = dt;     
  }                                 

  ret = ftmpi_mpi_type_struct(cnt,array_blocks,array_dp,
			      array_types,ret_handle);

  if(cnt > 20){
    _FREE(array_types);
    _FREE(array_dp);
  }

  if(ret == MPI_SUCCESS){
    int * i_a[3];

    i_a[0] = &cnt;
    i_a[1] = array_blocks;
    i_a[2] = array_disp;

    ftmpi_ddt_set_args(*ret_handle,i_a,NULL,&dt,MPI_COMBINER_INDEXED);
  }

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_hindexed(int cnt, int *array_blocks, MPI_Aint *array_disp,
		      MPI_Datatype dt, MPI_Datatype *ret_handle)
{               
  int i, ret;
  MPI_Datatype array_t[20], *array_types;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT ==0)
    ftmpi_ddt_init();

  if(cnt < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COUNT);
  for(i=0;i<cnt;i++)
    if(array_blocks[i] < 0) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  CHECK_DDT(MPI_COMM_WORLD,dt,0);
                   
  if(cnt > 20)
    array_types = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*cnt);
  else
    array_types = array_t;
                         
  for(i=0;i<cnt;i++)
     array_types[i] = dt;

  ret = ftmpi_mpi_type_struct(cnt,array_blocks,array_disp,
			      array_types,ret_handle);

  if(cnt > 20)
    _FREE(array_types);

  if(ret == MPI_SUCCESS){ 
    int * i_a[2];
    MPI_Aint * a_a[1];
    
    i_a[0] = &cnt;
    i_a[1] = array_blocks;
    a_a[0] = array_disp;

    ftmpi_ddt_set_args(*ret_handle,i_a,a_a,&dt,MPI_COMBINER_HINDEXED);
  }

  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );
  return ( MPI_SUCCESS );
}                                   

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int  MPI_Address(void *particle, MPI_Aint *disp)
{

  CHECK_MPIINIT;

  if ( !particle ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_ARG );

  *disp = (MPI_Aint) particle;
  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Type_commit( MPI_Datatype *dt)
{
  int ret;

  CHECK_MPIINIT;

  ret = ftmpi_mpi_type_commit ( dt );
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_type_commit ( MPI_Datatype *dt )
{
  FTMPI_DDT_R_ITEM * temp = NULL;

  temp = ftmpi_ddt_get_root(*dt);
  if(temp != NULL )
    temp->commited = 1;
  else {
    printf("ERROR: dt %d does NOT exist - MPI_Type_commit\n",*dt);
    return ( MPI_ERR_TYPE );
  }   

  return(MPI_SUCCESS);
}

/******************************************************************/
/******************************************************************/
/******************************************************************/
int MPI_Type_create_subarray(int ndims,int *array_of_sizes,
			     int *array_of_subsizes,int *array_of_starts,
			     int order,MPI_Datatype oldtype,
			     MPI_Datatype *newtype)
{
  int i;
  int size;
  MPI_Aint extent, disp[3];
  int lens[3];
  MPI_Datatype tmp1, tmp2, types[3];
  int ret;

  /**** ERRROR CHECKING ****/
  CHECK_MPIINIT;

  if(ndims <= 0)
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_sizes == NULL)
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_subsizes == NULL)
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_starts == NULL)
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  for(i=0;i<ndims;i++){
    if(array_of_sizes[i] <= 0)
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if(array_of_subsizes[i] <= 0)
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if(array_of_starts[i] < 0)
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }

  if(oldtype == MPI_DATATYPE_NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_TYPE);

  ret = ftmpi_mpi_type_extent(oldtype, &extent);
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  if (order == MPI_ORDER_FORTRAN) {
    if (ndims == 1)
      ftmpi_mpi_type_contiguous(array_of_subsizes[0], oldtype, &tmp1);
    else {
      ftmpi_mpi_type_vector(array_of_subsizes[1], array_of_subsizes[0],
			    array_of_sizes[0], oldtype, &tmp1);
      
      size = array_of_sizes[0]*extent;
      for (i=2; i<ndims; i++) {
        size *= array_of_sizes[i-1];
        ftmpi_mpi_type_hvector(array_of_subsizes[i], 1, size, tmp1, &tmp2);
        ftmpi_mpi_type_free(&tmp1);
        tmp1 = tmp2;
      }
    }
    
    disp[1] = array_of_starts[0];
    size = 1;
    for (i=1; i<ndims; i++) {
      size *= array_of_sizes[i-1];
      disp[1] += size*array_of_starts[i];
    }  
  }
  else if (order == MPI_ORDER_C) {
    if (ndims == 1)
      ftmpi_mpi_type_contiguous(array_of_subsizes[0], oldtype, &tmp1);
    else {
      ftmpi_mpi_type_vector(array_of_subsizes[ndims-2],
			    array_of_subsizes[ndims-1],
			    array_of_sizes[ndims-1], oldtype, &tmp1);
      
      size = array_of_sizes[ndims-1]*extent;
      for (i=ndims-3; i>=0; i--) {
        size *= array_of_sizes[i+1];
        ftmpi_mpi_type_hvector(array_of_subsizes[i], 1, size, tmp1, &tmp2);
        ftmpi_mpi_type_free(&tmp1);
        tmp1 = tmp2;
      }
    }
    
    disp[1] = array_of_starts[ndims-1];
    size = 1;
    for (i=ndims-2; i>=0; i--) {
      size *= array_of_sizes[i+1];
      disp[1] += size*array_of_starts[i];
    }
  }
  else 
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);


  disp[1] *= extent;
  disp[2] = extent;

  for(i=0;i<ndims;i++) 
    disp[2] *= array_of_sizes[i];
  
  disp[0]  = 0;
  lens[0]  = lens[1] = lens[2] = 1;
  types[0] = MPI_LB;
  types[1] = tmp1;
  types[2] = MPI_UB;
  
  ret = ftmpi_mpi_type_struct(3, lens, disp, types, newtype);
  if ( ret != MPI_SUCCESS ) RETURNERR ( MPI_COMM_WORLD, ret );

  ftmpi_mpi_type_free(&tmp1);

  if(ret == MPI_SUCCESS){
    int * i_a[5];

    i_a[0] = &ndims;
    i_a[1] = array_of_sizes;
    i_a[2] = array_of_subsizes;
    i_a[3] = array_of_starts;
    i_a[4] = &order;

    ftmpi_ddt_set_args(*newtype,i_a,NULL,&oldtype,MPI_COMBINER_SUBARRAY);
  }
  
  return MPI_SUCCESS;
}
/**************************************************************************/
/**************************************************************************/
/**************************************************************************/
int MPI_Type_create_darray(int size, int rank, int ndims,
			   int *array_of_gsizes, int *array_of_distribs,
			   int *array_of_dargs, int *array_of_psizes,
			   int order, MPI_Datatype oldtype,
			   MPI_Datatype *newtype) 
{
  MPI_Datatype type_old, type_new, types[3];
  int procs, tmp_rank, i, tmp_size, blklens[3], *coords;
  MPI_Aint *st_offsets, orig_extent, disps[3];
  int mpi_errno = MPI_SUCCESS;
  int ret;

  
  CHECK_MPIINIT;

  if (size <= 0) 
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if (rank < 0) 
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if (ndims <= 0) 
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_gsizes == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_distribs == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_dargs == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if(array_of_psizes == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  for (i=0; i<ndims; i++) {
    if (array_of_gsizes[i] <= 0)
      RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if ((array_of_dargs[i] != MPI_DISTRIBUTE_DFLT_DARG) && (array_of_dargs[i] <= 0))
      RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if (array_of_psizes[i] <= 0)
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }
  
  /* order argument checked below */
  if (oldtype == MPI_DATATYPE_NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  ftmpi_mpi_type_extent(oldtype, &orig_extent);
  
  coords   = (int *)_MALLOC(ndims*sizeof(int));
  procs    = size;
  tmp_rank = rank;
  for (i=0; i<ndims; i++) {
    procs     = procs/array_of_psizes[i];
    coords[i] = tmp_rank/procs;
    tmp_rank  = tmp_rank % procs;
  }
  
  st_offsets = (MPI_Aint *)_MALLOC(ndims*sizeof(MPI_Aint));
  type_old   = oldtype;
  
  if (order == MPI_ORDER_FORTRAN) {
    /* dimension 0 changes fastest */
    for (i=0; i<ndims; i++) {
      switch(array_of_distribs[i]) {
        case MPI_DISTRIBUTE_BLOCK:
          mpi_errno = type_block(array_of_gsizes, i, ndims, 
				 array_of_psizes[i],coords[i], 
				 array_of_dargs[i], order, orig_extent, 
				 type_old, &type_new, st_offsets+i); 
          break;
        case MPI_DISTRIBUTE_CYCLIC:
          mpi_errno = type_cyclic(array_of_gsizes, i, ndims, 
				  array_of_psizes[i], coords[i], 
				  array_of_dargs[i], order,orig_extent, 
				  type_old, &type_new, st_offsets+i);
          break;
        case MPI_DISTRIBUTE_NONE:
          if (array_of_psizes[i] != 1)
			  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
          /* treat it as a block distribution on 1 process */
          mpi_errno = type_block(array_of_gsizes, i, ndims, 1, 0, 
				 MPI_DISTRIBUTE_DFLT_DARG, order, 
				 orig_extent, type_old, &type_new, 
				 st_offsets+i); 
          break;
        default:
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
      }
      if(mpi_errno) 
            RETURNERR (MPI_COMM_WORLD, mpi_errno);
      
      if(i)
        ftmpi_mpi_type_free(&type_old);
      type_old = type_new;
    }
    
    disps[1] = st_offsets[0];
    tmp_size = 1;
    for (i=1; i<ndims; i++) {
      tmp_size *= array_of_gsizes[i-1];
      disps[1] += tmp_size*st_offsets[i];
    }
  }
  else if (order == MPI_ORDER_C) {
    for (i=ndims-1; i>=0; i--) {
      switch(array_of_distribs[i]) {
        case MPI_DISTRIBUTE_BLOCK:
          type_block(array_of_gsizes, i, ndims, array_of_psizes[i],
		     coords[i], array_of_dargs[i], order, orig_extent, 
		     type_old, &type_new, st_offsets+i); 
          break;
        case MPI_DISTRIBUTE_CYCLIC:
          type_cyclic(array_of_gsizes, i, ndims, array_of_psizes[i], 
		      coords[i], array_of_dargs[i], order, orig_extent, 
		      type_old, &type_new, st_offsets+i);
          break;
        case MPI_DISTRIBUTE_NONE:
          if (array_of_psizes[i] != 1)
		  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
          type_block(array_of_gsizes, i, ndims, array_of_psizes[i],
		     coords[i], MPI_DISTRIBUTE_DFLT_DARG, order, 
		     orig_extent, type_old, &type_new, st_offsets+i); 
          break;
        default:
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
      }
      if (i != ndims-1) 
        ftmpi_mpi_type_free(&type_old);
      type_old = type_new;
    }
    
    disps[1] = st_offsets[ndims-1];
    tmp_size = 1;
    for (i=ndims-2; i>=0; i--) {
      tmp_size *= array_of_gsizes[i+1];
      disps[1] += tmp_size*st_offsets[i];
    }
  }
  else 
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  disps[1] *= orig_extent;
  
  disps[2] = orig_extent;
  for(i=0; i<ndims; i++) 
    disps[2] *= array_of_gsizes[i];
  
  disps[0] = 0;
  blklens[0] = blklens[1] = blklens[2] = 1;
  types[0] = MPI_LB;
  types[1] = type_new;
  types[2] = MPI_UB;
  
  ret = ftmpi_mpi_type_struct(3, blklens, disps, types, newtype);
  ftmpi_mpi_type_free(&type_new);

  if(ret == MPI_SUCCESS){
    int * i_a[8];

    i_a[0] = &size;
    i_a[1] = &rank;
    i_a[2] = &ndims;
    i_a[3] = array_of_gsizes;
    i_a[4] = array_of_distribs;
    i_a[5] = array_of_dargs;
    i_a[6] = array_of_psizes;
    i_a[7] = &order;

    ftmpi_ddt_set_args(*newtype,i_a,NULL,&oldtype,MPI_COMBINER_DARRAY);
  }

  _FREE(st_offsets);
  _FREE(coords);
  return MPI_SUCCESS;
}
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
int type_block(int *array_of_gsizes, int dim, int ndims, int nprocs,int rank, 
	       int darg, int order, MPI_Aint orig_extent,MPI_Datatype type_old, 
	       MPI_Datatype *type_new,MPI_Aint *st_offset) 
{
  int blksize, global_size, mysize, i, j;
  MPI_Aint stride;
  
  global_size = array_of_gsizes[dim];
  
  if (darg == MPI_DISTRIBUTE_DFLT_DARG) 
    blksize = (global_size + nprocs - 1)/nprocs;
  else {
    blksize = darg;
    if (blksize <= 0) 
      RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if (blksize * nprocs < global_size) 
      RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  }
  
  j = global_size - blksize*rank;
  mysize = blksize < j ? blksize : j;
  if (mysize < 0) mysize = 0;
  
  stride = orig_extent;
  if(order == MPI_ORDER_FORTRAN) {
    if (dim == 0) 
      ftmpi_mpi_type_contiguous(mysize, type_old, type_new);
    else {
      for (i=0; i<dim; i++) 
        stride *= array_of_gsizes[i];
      ftmpi_mpi_type_hvector(mysize, 1, stride, type_old, type_new);
    }
  }
  else {
    if (dim == ndims-1) 
      ftmpi_mpi_type_contiguous(mysize, type_old, type_new);
    else {
      for (i=ndims-1; i>dim; i--) 
        stride *= array_of_gsizes[i];
      ftmpi_mpi_type_hvector(mysize, 1, stride, type_old, type_new);
    }
  }
  
  *st_offset = blksize * rank;
  if (mysize == 0) *st_offset = 0;
  return MPI_SUCCESS;
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int type_cyclic(int *array_of_gsizes, int dim, int ndims, int nprocs,int rank, 
		int darg, int order, MPI_Aint orig_extent,MPI_Datatype type_old, 
		MPI_Datatype *type_new,MPI_Aint *st_offset) 
{
  int blksize, i, blklens[2], st_index, end_index, local_size, rem, count;
  MPI_Aint stride, disps[2];
  MPI_Datatype type_tmp, types[2];
  
  if(darg == MPI_DISTRIBUTE_DFLT_DARG) 
    blksize = 1;
  else 
    blksize = darg;
  
  if (blksize <= 0)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  st_index = rank*blksize;
  end_index = array_of_gsizes[dim] - 1;
  
  if(end_index < st_index) 
    local_size = 0;
  else {
    local_size = ((end_index - st_index + 1)/(nprocs*blksize))*blksize;
    rem = (end_index - st_index + 1) % (nprocs*blksize);
    local_size += (rem < blksize ? rem : blksize);
  }
  
  count = local_size/blksize;
  rem   = local_size % blksize;
  
  stride = nprocs*blksize*orig_extent;
  if (order == MPI_ORDER_FORTRAN)
    for (i=0; i<dim; i++) 
      stride *= array_of_gsizes[i];
  else 
    for (i=ndims-1; i>dim; i--) 
      stride *= array_of_gsizes[i];
    
  ftmpi_mpi_type_hvector(count, blksize, stride, type_old, type_new);
  
  if(rem){
    types[0] = *type_new;
    types[1] = type_old;
    disps[0] = 0;
    disps[1] = count*stride;
    blklens[0] = 1;
    blklens[1] = rem;
    
    ftmpi_mpi_type_struct(2, blklens, disps, types, &type_tmp);
    
    ftmpi_mpi_type_free(type_new);
    *type_new = type_tmp;
  }
  
  *st_offset = rank * blksize; 
  if(local_size == 0) *st_offset = 0;
  
  return MPI_SUCCESS;
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int MPI_Type_ub(MPI_Datatype datatype, MPI_Aint *displacement)
{
  FTMPI_DDT_R_ITEM * temp;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  CHECK_DDT(MPI_COMM_WORLD,datatype,0);
  if(datatype < FTMPI_DDT_B_DATATYPE_TOTAL){              
    *displacement = _ftmpi_btype[datatype]->size;
    return(MPI_SUCCESS);
  }

  temp = ftmpi_ddt_get_root(datatype);
  if(temp == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_TYPE);

  if(temp->ub_set == 0)
    *displacement = temp->last+1;
  else 
    *displacement = temp->ub;
 

  return(MPI_SUCCESS);
}
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int MPI_Type_lb(MPI_Datatype datatype, MPI_Aint *displacement)
{
  FTMPI_DDT_R_ITEM * temp;
  
  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }
  CHECK_DDT(MPI_COMM_WORLD,datatype,0);
  if(datatype < FTMPI_DDT_B_DATATYPE_TOTAL){
    *displacement = 0;
    return(MPI_SUCCESS);
  }
  temp = ftmpi_ddt_get_root(datatype);
  if(temp == NULL)
    RETURNERR (MPI_COMM_WORLD, MPI_ERR_TYPE);

  if(temp->lb_set == 0)
    *displacement = TROOT->first_e->padding;
  else
    *displacement = temp->lb;

  return(MPI_SUCCESS);
}



/****************************************************************************/
/**** NEW MPI-2 STUFF --- no thanx to Graham and George ...  HEHEHE *********/
/****************************************************************************/

int MPI_Type_get_envelope(MPI_Datatype ddt, int * num_int, int * num_addr, 
			  int * num_ddt,int * combiner)
{
  int ret;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0) ftmpi_ddt_init();
  CHECK_DDT(MPI_COMM_WORLD,ddt,0);

  ret = ftmpi_ddt_get_args(ddt,0,num_int, NULL,num_addr, NULL,num_ddt,
			   NULL,combiner);
  if(ret == MPI_SUCCESS) return MPI_SUCCESS;

  RETURNERR (MPI_COMM_WORLD, ret);
}


/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int MPI_Type_get_contents(MPI_Datatype ddt,int max_int,int max_addr,
			  int max_ddt,int * array_int,MPI_Aint * array_addr,
			  MPI_Datatype * array_ddt)
{
  int ret;

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0) ftmpi_ddt_init();
  CHECK_DDT(MPI_COMM_WORLD,ddt,0);

  ret = ftmpi_ddt_get_args(ddt,1,&max_int, array_int,&max_addr, array_addr,
			   &max_ddt,array_ddt,NULL);
  if(ret == MPI_SUCCESS) return MPI_SUCCESS;
  
  RETURNERR (MPI_COMM_WORLD, ret);
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
int MPI_Type_dup(MPI_Datatype ddt,MPI_Datatype * nddt)
{

  CHECK_MPIINIT;

  if(FTMPI_DDT_INIT == 0) ftmpi_ddt_init();
  CHECK_DDT(MPI_COMM_WORLD,ddt,0);

  if(ftmpi_mpi_type_dup(ddt,nddt) != MPI_SUCCESS)
     RETURNERR (MPI_COMM_WORLD, MPI_ERR_INTERN);
  
  return(MPI_SUCCESS);
}


int ftmpi_mpi_type_dup(MPI_Datatype ddt,MPI_Datatype * nddt)
{

  FTMPI_DDT_R_ITEM * otemp = NULL,* temp = NULL;

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();
  

  *nddt = FTMPI_DDT_NEXT_DATATYPE ++;
  if(FTMPI_DDT_NEXT_DATATYPE == 501){  /* create a new list for datatypes */
    /* FTMPI_DDT_DATATYPE_HANDLE = */ ftmpi_ddt_create_list(20);
  }

  temp = ftmpi_ddt_add_root(*nddt,0);
  if (temp == NULL) return ( MPI_ERR_TYPE);


  otemp = ftmpi_ddt_get_root(ddt);
  
  memcpy(temp,otemp,sizeof(FTMPI_DDT_R_ITEM));
  TROOT->ref_cnt++;

  return(MPI_SUCCESS);
}


















