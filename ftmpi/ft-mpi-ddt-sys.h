
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
 			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_DDT_SYS
#define _FT_MPI_H_DDT_SYS

/* NOTE FOR THE        ---> DO_CODE = 2 MEANS PERFORM XDR
                            DO_CODE = 1 MEANS PERFORM BS
                            DO_CODE = 0 MEANS JUST COMPACT
*/

#ifndef TB_DATATYPE_HEADER_TB
#define TB_DATATYPE_HEADER_TB

#include <stdio.h>
#include <stdlib.h>
#include <rpc/rpc.h>
#include <rpc/xdr.h>
#include <sys/time.h>
#include "mpi.h"
#include "ft-mpi-check.h"
#include "ft-mpi-convinfo.h"

/*******************/
/* Various Macros  */
/*******************/

/* THESE ARE HERE TO MAKE IT EASIER TO MODIFY THE TOTAL NUMBER OF BASIC DATATYPE TO BE ABLE TO DESCRIBE AND ALSO TO MODIFY THE MAXIMUM NUMBER OF PARTICLES TO DESCRIBE THE DERIVED DATATYPE */

#define FTMPI_XDR 3
#define FTMPI_RSC 2
#define FTMPI_SSC 1
#define FTMPI_NOE 0


#ifdef HAVE_LONG_LONG
#define FTMPI_DDT_B_DT_MAX 21
#else 
#define FTMPI_DDT_B_DT_MAX 20
#endif
/* #define FTMPI_DDT_COMP_MAX 1 */
#define FTMPI_DDT_COMP_MAX 25

/*******************/
/*******************/

#define FTMPI_DDT_B_DT_CHECK(dt,size) \
  if(dt > 0 && dt < B_DATATYPE_TOTAL){ \
    size = BASIC_DATATYPE[dt]->size; \
  } \
  else { \
    size = 0; \
  } 



/*******************/
/*******************/

#define FTMPI_BSWAP_A_WORD(DATA)\
  do {\
    unsigned int * _pdata = (unsigned int*)(DATA); \
    unsigned int __value;\
    __value = *(_pdata);\
    *(_pdata)  = ( __value & 0xff000000 ) >> 24; \
    *(_pdata) |= ( __value & 0x00ff0000 ) >> 8;\
    *(_pdata) |= ( __value & 0x0000ff00 ) << 8;\
    *(_pdata) |= ( __value & 0x000000ff ) << 24;\
  } while (0)

/*******************/
/*******************/

#define FTMPI_BSWAP_32( DATA, CNT )\
  do { \
    int _i;\
    int *_data = (DATA);\
    int _cnt = (CNT);\
    for(_i=0; _i<_cnt; _i++) \
      FTMPI_BSWAP_A_WORD(&data[_i]);\
  } while (0)

/*******************/
/*******************/

#define FTMPI_BSWAP_64( DATA, CNT)\
  do {\
    int _i;\
    int *_tdat;\
    int  _tmp;\
    int _cnt = (CNT);\
    _tdat = (int *) (DATA);\
    for (_i = 0; _i < _cnt*2; _i+=2){\
      FTMPI_BSWAP_A_WORD(&_tdat[_i]);\
      FTMPI_BSWAP_A_WORD(&_tdat[_i+1]);\
      _tmp       = _tdat[_i];\
      _tdat[_i]   = _tdat[_i+1];\
      _tdat[_i+1] = _tmp;\
    }\
  } while (0)

/*******************/
/*******************/

#define FTBSWAP_64_v2(DATA, CNT)\
  do {\
    unsigned long long _value;\
    unsigned long long *_data = DATA;\
    int _i; \
    int _cnt = CNT; \
    for(_i=0; _i<_cnt; _i++){\
      _value = _data[_i];\
      _data[_i]  = ( _value & 0xff00000000000000 ) >> 56;\
      _data[_i] |= ( _value & 0x00ff000000000000 ) >> 40;\
      _data[_i] |= ( _value & 0x0000ff0000000000 ) >> 24;\
      _data[_i] |= ( _value & 0x000000ff00000000 ) >> 8;\
      _data[_i] |= ( _value & 0x00000000ff000000 ) << 8;\
      _data[_i] |= ( _value & 0x0000000000ff0000 ) << 24;\
      _data[_i] |= ( _value & 0x000000000000ff00 ) << 40;\
      _data[_i] |= ( _value & 0x00000000000000ff ) << 56;\
    }\
  } while ( 0 )

/*******************/
/*******************/

#define FTMPI_DDT_mem_CP_BS_vars \
  char *dt_dest; \
  char *dt_src; \
  int dt_i, dt_mode=-1,l1,l2; \
  MPI_Datatype dt_type; \
  size_t dt_size; \
  int *long32arr; \
  long long *long64arr;
    

#define FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode) \
  l1 = 0; \
  l2 = 0; \
	if(dt_mode == -2){ \
	} \
  else if(dt_mode == -1){ \
    if((dt_type==1)&&(dt_size!=0)) \
      memcpy(dt_dest,dt_src,dt_size); \
    else if(dt_type==2){ \
      dt_src += 1; \
      for(dt_i=0;dt_i<dt_size/2;dt_i++){ \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        dt_src += 4; \
      } \
    } \
    else if(dt_type==4){ \
      dt_src += 3; \
      for(dt_i=0;dt_i<dt_size/4;dt_i++){ \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        dt_src += 8; \
      } \
    } \
    else if(dt_type==8){ \
      dt_src += 7; \
      for(dt_i=0;dt_i<dt_size/8;dt_i++){ \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        dt_src+=16; \
      } \
    } \
    else if(dt_type==12){ \
      dt_src+=11; \
      for(dt_i=0;dt_i<dt_size/12;dt_i++){ \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        dt_src+=24; \
      } \
    } \
    else if(dt_type==16){ \
      dt_src += 15; \
      for(dt_i=0;dt_i<dt_size/16;dt_i++){ \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        *dt_dest++ = *dt_src--; \
        dt_src+=32; \
      } \
    } \
    else { \
      printf("cannot perform BYTESWAP due to incorrect number of data available\n"); \
      return(-1); \
    } \
  } \
  else if ( ( dt_mode == 0 ) || ( dt_mode == 3) ){ \
    long32arr = (int *) dt_src; \
    long64arr = (long long *) dt_dest; \
    for ( dt_i = 0; dt_i < dt_size/4; dt_i++ ) { /* not tested yet */ \
      long64arr[dt_i] = (long long) long32arr[dt_i]; \
    } \
  } \
  else if ( (dt_mode == 1 ) || ( dt_mode == 2 )){ \
    long32arr = (int *) dt_src; \
    long64arr = (long long *) dt_dest; \
    for ( dt_i = 0; dt_i < dt_size/4; dt_i++ ) { /* not tested yet */ \
      FTMPI_BSWAP_A_WORD (&long32arr[dt_i]); \
      long64arr[dt_i] = (long long) long32arr[dt_i]; \
    } \
  } \
  else if ( ( dt_mode == 4 ) || ( dt_mode == 7 )) { \
    long64arr = ( long long *) dt_src; \
    long32arr = ( int *) dt_dest; \
    for ( dt_i = 0; dt_i < dt_size/8; dt_i++ ) { /* not tested yet */ \
      long32arr[dt_i] = ( int ) long64arr[dt_i]; \
    } \
  } \
  else if ( ( dt_mode == 5 ) || ( dt_mode == 6 )) { \
    long64arr = ( long long *) dt_src; \
    long32arr = ( int *) dt_dest; \
    for ( dt_i = 0; dt_i <  dt_size/8; dt_i++ ) { /* not tested yet */ \
      FTMPI_BSWAP_64(&long64arr[dt_i], 1); \
      long32arr[dt_i] = ( int ) long64arr[dt_i]; \
    } \
  } \
  else \
    printf("not yet implemented %d\n",dt_mode);




















/*************************/
/* Structure Defions */
/*************************/

typedef struct code_info_str {
  int bs;
  int mode_l;
  int size_l;

  unsigned int ld_flag;
  int ld_sigl;
  int ld_expl;
  int size_ld;

  int code_type;
}  FTMPI_DDT_CODE_INFO;

typedef struct node {
  int size;
  int xdr_size;
  MPI_Aint extent;
  MPI_Aint xdr_extent;
  int hash;
} FTMPI_DDT_ITEM;

typedef struct _ftmpi_comp {
  long   dt;       /* 0 - datatype */
  long   count;    /* 1 - count */
  long   disp;     /* 2 - displacement */
  long   extent;   /* 3 - extent */
  long   nitems;   /* 4 - number of items */
  long   nrepeats; /* 5 - number of repeats */
} _ftmpi_comp_t;


typedef struct root_node_add {
  struct element_node * first_e;
  struct element_node * last_e;
  int type_count[FTMPI_DDT_B_DT_MAX];
  int t_comp;
  _ftmpi_comp_t comp[FTMPI_DDT_COMP_MAX];
  int * i;
  MPI_Aint * a;
  MPI_Datatype * d;
  int create_type;
  int ref_cnt;
} FTMPI_DDT_RA_ITEM;

typedef struct root_node {
  struct root_node * next;  
  int dt;
  int size;
  int xdr_size;
  int uses;
  MPI_Aint extent;
  MPI_Aint xdr_extent;
  int count;
  MPI_Aint padding;
  int last;
  int hash;
  int bs_xdr_count;
  int bs_xdr_type;
  int ub_set,lb_set;
  MPI_Aint ub,lb;
  int contig;
  int elem_cnt;
  int commited;
  FTMPI_DDT_RA_ITEM * root_add;
} FTMPI_DDT_R_ITEM;


typedef struct element_node {
  struct element_node * next;
  int dt;
  int size;
  MPI_Aint extent;
  int count;
  MPI_Aint padding;
  int bs_xdr_type;
  int bs_xdr_count;
} FTMPI_DDT_E_ITEM;


typedef struct bnode{
  int offset;
  int tmp_off; 
  int last_off; 
  int b_dt;
  int b_dt_s;
  int t_cnt;
  int cont;
  int cur_off;
} FTMPI_DDT_XNODE;

/******************************/
/* Temporary Global Variables */
/******************************/


extern int FTMPI_DDT_B_DT_ADD;
extern FTMPI_DDT_R_ITEM ** FTMPI_DDT_ROOT;
extern int FTMPI_DDT_INIT;
extern int FTMPI_DDT_HASH_SIZE;
extern int FTMPI_DDT_ROOT_VAL;
extern int FTMPI_DDT_NEXT_DATATYPE,FTMPI_DDT_INIT_CREATE,FTMPI_DDT_DATATYPE_HANDLE,FTMPI_DDT_DATATYPE_BASIC_INFO,FTMPI_DDT_B_DATATYPE_TOTAL;

extern FTMPI_DDT_ITEM * _ftmpi_btype[FTMPI_DDT_B_DT_MAX];
extern char * FTMPI_DDT_TMP_BUFFER;
extern char * FTMPI_DDT_TMP_MPI_BUFFER;

/************************/
/* Function Declaration */
/************************/

void ftmpi_ddt_init();
void ftmpi_ddt_finalize();
void ftmpi_ddt_remove_all_but_basic(void);
void ftmpi_ddt_reset_ref_counts();

int ftmpi_ddt_set_xdr_size ( MPI_Datatype type, int *size );
int ftmpi_ddt_set_size ( MPI_Datatype type, int *size );
int ftmpi_mpi_type_xdr_size(MPI_Datatype type, int * size);
int ftmpi_ddt_set_xdr_extent(MPI_Datatype type, MPI_Aint* extent);

int write_datatype(int,int,char*,int);
int read_datatype(int,int,char*,int);

void ftmpi_ddt_increment_dt_uses(int ddt);
void ftmpi_ddt_decrement_dt_uses(int ddt);
int ftmpi_ddt_get_first_offset(int dt);

/* these functions cannot and will not continue if insufficient buffer space */
/* but they are more efficient thent the other four */
int ftmpi_ddt_write_dt_to_buffer(char*,int,int,int,char*,int,int *,int);
int ftmpi_ddt_read_dt_from_buffer(char*,int,int,int,char*,int,int *,FTMPI_DDT_CODE_INFO *);
int ftmpi_ddt_write_dt_to_buffer_block(char*,int,int,int,char*,int,int*,int);
int ftmpi_ddt_read_dt_from_buffer_block(char*,int,int,int,char*,int,int*,int);

/* these functions will continue if insufficient buffer space */
int ftmpi_ddt_extract_b_dt(char*,int,char*,int,int,FTMPI_DDT_XNODE*,int,int);
int ftmpi_ddt_insert_b_dt(char*,int,char*,int,int,FTMPI_DDT_XNODE*,int,int);
int ftmpi_ddt_extract_dt(char*,int,char*,int,FTMPI_DDT_XNODE*,int,int,int);
int ftmpi_ddt_insert_dt(char*,int,char*,int,FTMPI_DDT_XNODE*,int,int,int);

int ftmpi_ddt_xdr_encode_dt_to_buffer(char *,char *,char *,int,int,int,FTMPI_DDT_CODE_INFO *);
int ftmpi_ddt_xdr_decode_dt_to_buffer(char *,char *,char *,int,int,int,FTMPI_DDT_CODE_INFO *);

int ftmpi_ddt_xdr_bs_code(char*,char*,int,int,int);
int ftmpi_ddt_xdr_code(char * out,char * in,int dt,int count,int code);
int ftmpi_ddt_bs_xdr_det(int);


int ftmpi_ddt_dt_mode(int,FTMPI_DDT_CODE_INFO *);
int ftmpi_ddt_get_bdt_sizes(FTMPI_DDT_CODE_INFO *, int dt);
void ftmpi_ddt_set_dt_mode(unsigned int me,unsigned int other,FTMPI_DDT_CODE_INFO * code_info);
int ftmpi_ddt_decode_size_det(int dt,int count,int * size,FTMPI_DDT_CODE_INFO * code_info);
int ftmpi_ddt_encode_size_det(int dt,int count,int * size,FTMPI_DDT_CODE_INFO * code_info);


void ftmpi_ddt_set_type(int ddt, int type);
void ftmpi_ddt_set_args(int ddt, int ** i,MPI_Aint ** a,int * d,int type);
int ftmpi_ddt_get_args(MPI_Datatype ddt,int which,int * cb, int * b,int * cd, MPI_Aint * d,
		       int * ct, MPI_Datatype * t,int * type);


void ftmpi_ddt_determine_xdr_bs(int);
void ftmpi_ddt_dt_address(void *,int *);
int ftmpi_ddt_struct_commit(int *);
int ftmpi_ddt_check_array(_ftmpi_comp_t *old,int *size);
double ftmpi_ddt_do_bs_det();
int ftmpi_ddt_write_read_size_det(int,int,int *,int);
int ftmpi_ddt_copy_ddt_to_ddt(void * fbuf,int fddt,int fcnt,void * tbuf,int tddt,
			      int tcnt);
int ftmpi_ddt_get_bdt_count(int ddt,int cnt);
int ftmpi_ddt_get_element_count(int ddt,long rsize,int * cnt, 
				FTMPI_DDT_R_ITEM * ddt_ptr,int ddt_cnt);

int type_block(int *array_of_gsizes, int dim, int ndims, 
	       int nprocs,int rank, int darg, int order, 
	       MPI_Aint orig_extent,MPI_Datatype type_old, 
	       MPI_Datatype *type_new,MPI_Aint *st_offset);
int type_cyclic(int *array_of_gsizes, int dim, int ndims, int nprocs,
		int rank, int darg, int order, MPI_Aint orig_extent,
		MPI_Datatype type_old, MPI_Datatype *type_new,
		MPI_Aint *st_offset);


int ftmpi_ddt_check_ddt(int ddt,int cmt);


/* Used to manage the hashed lists */
int ftmpi_ddt_create_list(int);
FTMPI_DDT_R_ITEM * ftmpi_ddt_add_root(int,int);
FTMPI_DDT_R_ITEM * ftmpi_ddt_get_root(int);
FTMPI_DDT_E_ITEM * ftmpi_ddt_add_element(FTMPI_DDT_R_ITEM *,int); 
FTMPI_DDT_E_ITEM * ftmpi_ddt_get_element(FTMPI_DDT_R_ITEM *,FTMPI_DDT_E_ITEM *);




void set_rainbow_buffer(int,int,char *);
void print_buffer(char*,char*,int);           /* used for  debuging */


/* Prototypes for the ftmpi_mpi_* functions */
int ftmpi_mpi_type_free(MPI_Datatype *ddt_id);
int ftmpi_mpi_type_size(MPI_Datatype type, int * size);
int ftmpi_mpi_type_extent ( MPI_Datatype type, MPI_Aint *extent );
int ftmpi_mpi_type_struct( int cnt, int *array_block, MPI_Aint *array_disp, 
			   MPI_Datatype *array_types, MPI_Datatype *ddt);
int ftmpi_mpi_type_contiguous(int cnt, MPI_Datatype dt, 
			      MPI_Datatype *ret_handle);
int ftmpi_mpi_type_vector(int cnt, int block_length, int stride, 
			  MPI_Datatype dt, MPI_Datatype *ret_handle);
int ftmpi_mpi_type_hvector(int cnt, int block_length, MPI_Aint stride,
			   MPI_Datatype dt, MPI_Datatype *ret_handle);
int ftmpi_mpi_type_indexed(int cnt,int *array_blocks, int *array_disp,
			   MPI_Datatype dt, MPI_Datatype *ret_handle);
int ftmpi_mpi_type_commit ( MPI_Datatype *dt );
int ftmpi_mpi_type_dup(MPI_Datatype ddt,MPI_Datatype * nddt);





/* Comm ops */
#define MPI_SOURCE_ANY_TB -1
#define MPI_RANK_ANY_TB -2

/* #define MPI_REQUEST_NULL_TB 0 */

#endif

#endif /* _FT_MPI_H_DDT_SYS */
