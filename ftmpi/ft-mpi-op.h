
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
            Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@hlrs.de>
			MPICH team at msc.anl.gov (!)

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/





#ifndef _FT_MPI_OP
#define _FT_MPI_OP


#define _ATB_ERROR -460917	/* used in ft-mpi-op and ft-mpi-lib-op */


typedef struct _atb_mpi_op {
  MPI_User_function * op;
  int commute;
  int permanent;
}_ATB_MPI_OP;

extern int B_DATATYPE_TOTAL;

void _atb_init_op();
int _atb_op_ok(MPI_Op op);
MPI_User_function* _atb_op_get(MPI_Op); 
int _atb_op_add(_ATB_MPI_OP * new_op);
int _atb_op_get_commute(MPI_Op op);

#endif
