/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>
 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	MPI API communicator calls
*/

#include <stdlib.h>

#include "ft-mpi-lib.h"
#include "mpi.h"
#include "ft-mpi-com.h"
#include "ft-mpi-cart.h"
#include "ft-mpi-graph.h"
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-p2p.h"
#include "debug.h"

/* Definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Comm_compare         = PMPI_Comm_compare
#    pragma weak  MPI_Comm_create          = PMPI_Comm_create
#    pragma weak  MPI_Comm_dup             = PMPI_Comm_dup
#    pragma weak  MPI_Comm_free            = PMPI_Comm_free
#    pragma weak  MPI_Comm_rank            = PMPI_Comm_rank
#    pragma weak  MPI_Comm_size            = PMPI_Comm_size
#    pragma weak  MPI_Comm_split           = PMPI_Comm_split
#    pragma weak  MPI_Topo_test            = PMPI_Topo_test

#endif

#    define  MPI_Comm_compare          PMPI_Comm_compare
#    define  MPI_Comm_create           PMPI_Comm_create
#    define  MPI_Comm_dup              PMPI_Comm_dup
#    define  MPI_Comm_free             PMPI_Comm_free
#    define  MPI_Comm_rank             PMPI_Comm_rank
#    define  MPI_Comm_size             PMPI_Comm_size
#    define  MPI_Comm_split            PMPI_Comm_split
#    define  MPI_Topo_test             PMPI_Topo_test



/* prototypes of support f() */

int idcompare(const void *, const void *);
int rankkeycompare(const void *, const void *);


/* these are used in compare operations */
static int	gids1[MAXPERAPP];
static int	gids2[MAXPERAPP];
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

/* Notes */
/* all communicators now get their own original copies of a group */
/* this is in addition to the ref counting we do already when freeing groups */
/* which makes sure that all groups are not freed completely if still refd */
/* by a com */
/* the one group per comm wastes more memory but is simplier to maintain */
/* it may be changed back in the future, GEF Dec 2002 */



int MPI_Comm_size (MPI_Comm comm, int *size) 
{
  int rc;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  if (!size) RETURNERR (comm, MPI_ERR_ARG);    /* no null pointers thanks */

  rc = ftmpi_com_size (comm);
  if (rc<0) RETURNERR (comm, rc);

  *size = rc;

  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Comm_rank (MPI_Comm comm, int *rank)
{
  int rc;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  if (!rank) RETURNERR (comm, MPI_ERR_ARG);    /* no null pointers thanks */


  rc = ftmpi_com_rank (comm);
  if (rc<0) RETURNERR (comm, rc);

  *rank = rc;
  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Comm_free(MPI_Comm *comm)
{
  int rc;
  MPI_Comm shadow;

  CHECK_MPIINIT;
  CHECK_COM(*comm);

  if (( *comm == MPI_COMM_WORLD ) || ( *comm == MPI_COMM_SELF))
    RETURNERR( *comm, MPI_ERR_COMM ); /* not allowed to free them ! */

  if ( *comm == FTMPI_COMM_WORLD_SHADOW )
    RETURNERR ( MPI_COMM_WORLD, MPI_ERR_COMM);

  if ( *comm == FTMPI_COMM_SELF_SHADOW )
    RETURNERR ( MPI_COMM_SELF, MPI_ERR_COMM);


  shadow = ftmpi_com_get_shadow ( *comm );
  if ( shadow != MPI_COMM_NULL )
    rc = ftmpi_com_free (&shadow); 

  rc = ftmpi_com_free (comm);

  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_split(MPI_Comm comm,int color,int key,MPI_Comm *newcomm)
{
  int rc;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  /* check the rest of the args */
  /*  if (key<0) RETURNERR (comm, MPI_ERR_ARG); */

  if (color<0) /* check to make sure its within the right range */
    if (color!=MPI_UNDEFINED) RETURNERR (comm, MPI_ERR_ARG);

  if (!newcomm) RETURNERR (comm, MPI_ERR_ARG);


  rc = ftmpi_mpi_comm_split ( comm, color, key, newcomm );
  if ( rc != MPI_SUCCESS ) RETURNERR ( comm, rc );

  return ( MPI_SUCCESS );
}

int ftmpi_mpi_comm_split(MPI_Comm comm,int color,int key,MPI_Comm *newcomm)
{
  int i, loc;
  int myinfo[2];	/* colour, key. Rank is assumed from location */
				/* within results array */
  int *results;
  int size;		/* size of the results array and org comm */
  int *sorted;		/* [i][0] list of ranks in my new comm */
                        /* [i][1] list of keys in my new comm */
  int *sorted_ranks;	/* array of the ranks sorted.. */
  int my_size;		/* size of my new comm */
  /* int my_org_rank; */	/* my original rank */
  MPI_Group org_grp;	/* group that defines original comm */
  MPI_Group new_grp;	/* the new group that defined the new comm */
  int rc;

  /* get memory for global info */
  size = ftmpi_com_size (comm);

  results = (int*)_MALLOC(sizeof(int)*2*size);
  if (!results) return ( MPI_ERR_OTHER );

  /* Ok now to perform the split */
  /* this requires that all processes know who is in this group */

  myinfo[0] = color; 
  myinfo[1] = key;

  rc = ftmpi_coll_intra_allgather_bmtree ((void*)myinfo, 2, MPI_INT, (void*)results, 
					  2,  MPI_INT, comm, 0);
  if (rc<0) {
    _FREE(results);        /* always free this array */
    RETURNERR (comm, rc);  /* this should be an atomic check for FT reasons */
  }
  /* ok we now have the global list of values so now we can calc who is in our */
  /* group and comm and who is not! */
  /* first we find which group we belong and then we create a new group */
  /* from this group so we can create the comm needed */
  
  org_grp = ftmpi_com_get_group(comm);	/* orginal group (where we get GIDs from) */

  /* now how many do we have in 'my/our' new group */
  /*  my_org_rank = ftmpi_com_rank (comm); */
  my_size = 0;
  for(i=0;i<size;i++) if (results[(2*i)+0]==color) my_size++;
  
  sorted = (int*)_MALLOC(sizeof(int)*my_size*2);
  if (!sorted) return ( MPI_ERR_OTHER );

  /* ok we can now fill this info */
  loc=0; /* fill from start */
  for(i=0;i<size;i++) 
    if (results[(2*i)+0]==color) {
      sorted[(2*loc)+0]=i;			/* copy org rank */
      sorted[(2*loc)+1]=results[(2*i)+1];	/* copy key */
      loc++;
    }
  
  /* 'results' array no longer needed */
  _FREE(results); /* always free this array */
  
  /* the new array needs to be sorted so that it is in 'key' order */
  /* if two keys are equal then it is sorted in original rank order! */
  
  if(my_size>1)
    qsort ((int*)sorted, my_size, sizeof(int)*2, rankkeycompare);
  
  /* to build a new group we need a separate array of ranks! */
  
  sorted_ranks = (int*)_MALLOC(sizeof(int)*my_size);
  for(i=0;i<my_size;i++) sorted_ranks[i] = sorted[(i*2)+0];
  
  /* we can now free the 'sorted' array */
  _FREE(sorted);
  
  /* we now have enough info to build a new group! */
  rc = ftmpi_mpi_group_incl(org_grp, my_size, sorted_ranks, &new_grp);
  
  if (rc<0) return (rc);

  /* we can free the temp array of ranks */
  _FREE(sorted_ranks);
  
  /* from the group we can build a new COM.. */
  rc = ftmpi_mpi_comm_create (comm, new_grp, newcomm);

  /* we do not need to do a set group as this is done by create com */

  /* dispose of the temp grp.. */
  ftmpi_group_free (&new_grp);

  /* Since we changed to the internal interface and we are not 
  ** using the MPI functions any more, we mustn't free this group! 
  */
  /*   ftmpi_group_free (&org_grp); */

  if (color==MPI_UNDEFINED) {
    MPI_Comm shadow;

    shadow = ftmpi_com_get_shadow ( *newcomm );
    if ( shadow != MPI_COMM_NULL )
      ftmpi_com_free (&shadow); 

    rc = ftmpi_com_free( newcomm );
  }
  
  return (rc);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

int MPI_Comm_compare(MPI_Comm comm1,MPI_Comm comm2,int *result)
{
  int s1, s2, i;
  int sameranks=1;
  int sameorder=1;
  int inter1, inter2;

  CHECK_MPIINIT;
  /* we don't do a comm_ok check here as the ftmpi_com_size does it for us */
  
  /* check sizes */
  s1 = ftmpi_com_size (comm1);
  s2 = ftmpi_com_size (comm2);
  
  if ((s1<0)||(s2<0)) RETURNERR (MPI_COMM_WORLD, MPI_ERR_COMM);	/* bad communicators */
  
  /* now for half of 1 of the 4 compare functions */
  
  /* they can be unequal later if they don't have the same IDs */
  
  if (s1!=s2) 
    {
      *result = MPI_UNEQUAL;	/* not the same size so unequal */
      return ( MPI_SUCCESS );
    }
  
  /* check whether both are intra- or inter-communicators */
  inter1 = ftmpi_com_test_inter ( comm1 );
  inter2 = ftmpi_com_test_inter ( comm2 );
  if ( inter1 != inter2 )
    {
      /* Mismatch, no need to continue */
      *result = MPI_UNEQUAL;
      return ( MPI_SUCCESS );
    }  
  
  /* check the internals to see if they are dupped from each other */
  /* if they are then they are identical.. maybe */
  /* 
     NOTE
     This maybe the correct test for identical.. for now just compare IDs 
     we cannot make the contexts identical so they have to be the same com 
     return (MPI_IDENT); 
  */

  if (comm1==comm2)
    {
      *result = MPI_IDENT;
      return ( MPI_SUCCESS );
    }

  if ( inter1 ) 
    {
      MPI_Group grp1, grp2;
      int remote_result;
      
      /* Handling inter-communicators */

      /* we need to compare the local AND the remote group */
      /* First, compare the local group */
      /* make copy and compare at same time */
      grp1 = ftmpi_com_get_group ( comm1 );
      grp2 = ftmpi_com_get_group ( comm2 );

      for(i=0;i<s1;i++) {
	gids1[i] = ftmpi_group_gid(grp1, i);
	gids2[i] = ftmpi_group_gid(grp2, i);
	if (gids1[i]!=gids2[i]) sameorder=0;
      }
      
      /* to check ranks are the same in each we need them sorted */
      qsort ((int*)gids1, s1, sizeof(int), idcompare);
      qsort ((int*)gids2, s2, sizeof(int), idcompare);
      
      for(i=0;i<s1<i;i++) {
	if (gids1[i]!=gids2[i]) {
	  sameranks=0;
	  break;
	}
      }
      
      /* same ranks but different ordering */
      if ((sameranks)&&(!sameorder)) 
	*result = MPI_SIMILAR;
      /* same ranks and ordering */
      else if ((sameranks)&&(sameorder)) 
	*result = MPI_CONGRUENT;
      else 
	/* else ther are unequal again */
	*result = MPI_UNEQUAL;

      /* Second, compare the remote group */
      /* make copy and compare at same time */

      grp1 = ftmpi_com_get_remote_group ( comm1 );
      grp2 = ftmpi_com_get_remote_group ( comm2 );
      
      s1 = ftmpi_group_size ( grp1 );
      s2 = ftmpi_group_size ( grp2 );
      if ( s1 != s2 )
	{
	  *result = MPI_UNEQUAL;
	  return (MPI_SUCCESS );
	}	  
      

      for(i=0;i<s1;i++) {
	gids1[i] = ftmpi_group_gid(grp1, i);
	gids2[i] = ftmpi_group_gid(grp2, i);
	if (gids1[i]!=gids2[i]) sameorder=0;
      }
      
      /* to check ranks are the same in each we need them sorted */
      
      qsort ((int*)gids1, s1, sizeof(int), idcompare);
      qsort ((int*)gids2, s2, sizeof(int), idcompare);
      
      for(i=0;i<s1<i;i++) {
	if (gids1[i]!=gids2[i]) {
	  sameranks=0;
	  break;
	}
      }
      
      /* same ranks but different ordering */
      if ((sameranks)&&(!sameorder)) 
	remote_result = MPI_SIMILAR;
      /* same ranks and ordering */
      else if ((sameranks)&&(sameorder)) 
	remote_result = MPI_CONGRUENT;
      else 
	/* else ther are unequal again */
	remote_result = MPI_UNEQUAL;

      /* now determine the global result */
      if ( remote_result == MPI_SIMILAR )
	{
	  /* nothing to be done, since the setting
	     for *result is either as good as remote_result
	     or worse ( in which case its setting is correct)
	  */
	}
      else if ( remote_result == MPI_CONGRUENT )
	{
	  if ( *result == MPI_SIMILAR )
	    *result = MPI_CONGRUENT; 
	}
      else if ( remote_result == MPI_UNEQUAL )
	*result = MPI_UNEQUAL; /* no matter what its previous setting was */
    }
  else
    {
      /* Handling intra-communicators */

      /* now to check the ranks and order */
      /* we say true to both and then reset if anything is out of order.. */
      
      /* make copy and compare at same time */
      for(i=0;i<s1;i++) {
	gids1[i] = ftmpi_com_gid(comm1, i);
	gids2[i] = ftmpi_com_gid(comm2, i);
	if (gids1[i]!=gids2[i]) sameorder=0;
      }
      
      /* to check ranks are the same in each we need them sorted */
      qsort ((int*)gids1, s1, sizeof(int), idcompare);
      qsort ((int*)gids2, s2, sizeof(int), idcompare);
      
      for(i=0;i<s1<i;i++) {
	if (gids1[i]!=gids2[i]) {
	  sameranks=0;
	  break;
	}
      }
      
      /* same ranks but different ordering */
      if ((sameranks)&&(!sameorder)) 
	*result = MPI_SIMILAR;
      /* same ranks and ordering */
      else if ((sameranks)&&(sameorder)) 
	*result = MPI_CONGRUENT;
      else 
	/* else ther are unequal again */
	*result = MPI_UNEQUAL;
    }

  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_create(MPI_Comm comm, MPI_Group group, MPI_Comm *newcomm)
{
  /* ft_com_mode_t comm_mode;
     ft_com_state_t comm_state; */
  int sc, sg;
  int rc;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  if (newcomm) 
    *newcomm = MPI_COMM_NULL;	/* default value in case we fail */
  else
    RETURNERR (comm, MPI_ERR_ARG);  /* we must have a return communicator sorry! */

  rc = ftmpi_group_ok (group);
  if (rc<0) RETURNERR (comm, rc);

  /* we do not need to do attributes for this call */

  /* we SHOULD really check that group is a subset of COMMs group here */
  /* for now we just check the relative sizes */
  /* TODO */

  sc = ftmpi_com_size (comm);
  sg = ftmpi_group_size (group);
  
  if (sg>sc) RETURNERR (comm, MPI_ERR_ARG);	/* should not happen */


  rc = ftmpi_mpi_comm_create ( comm, group, newcomm );
  if ( rc != MPI_SUCCESS ) RETURNERR ( comm, rc );
  
  return ( MPI_SUCCESS );
}


int ftmpi_mpi_comm_create(MPI_Comm comm, MPI_Group group, MPI_Comm *newcomm)
{
  int i;
  int newcomsize;
  MPI_Comm nc;
  int rc;
  int grp;
  int mygid;	/* mygid */
  int sg;


  /* the get next_com is a collective on comm and MUST be called by ALL */
  /* nc = 0; */
  nc = ftmpi_com_lib_next_com (FT_INTRA_INTRA, 0, comm, MPI_COMM_NULL,
			       MPI_PROC_NULL, MPI_ANY_TAG );

  sg = ftmpi_group_size (group);

  /* then find out if we are in the new com or not */
  
  /* if we are not in group we return a comm of MPI_COMM_NULL  */
  /* this is NOT an error! */
  rc = ftmpi_group_rank (group);
  if (rc<0) {
    *newcomm = MPI_COMM_NULL;
    return (MPI_SUCCESS);
  }

  /* build the GID list */
  for(i=0;i<sg;i++) gids1[i]= ftmpi_group_gid (group, i);
  for(i=sg;i<MAXPERAPP;i++) gids1[i]=MPI_UNDEFINED;
  
  /* ok for the rest, we copy the supplied coms internals BUT change its GID list
   */
  
  newcomsize = sg;		/* new com has only the group size ranks */
  
  rc = ftmpi_com_copy_subset (comm, &nc, gids1, newcomsize, sg);
  if (rc<0) return ( rc ); /* ftmpi_com_copy_subset sets the correct MPI rc */

  *newcomm = nc;	/* done, here you go */
  
  /* com is created but does not have a group attached to it */
  /* get group slot */
  rc = ftmpi_group_get_free (&grp);
  if (rc<0) return ( rc );
  
  /* build group */
  mygid = ftmpi_com_my_gid ();
  rc = ftmpi_group_build (grp, mygid, gids1, newcomsize, sg);
  
  /* attach it to the communicator */
  ftmpi_com_set_group (nc, grp);

  /* Note, we do not free this group! */

  /* copy the error-handler */
  ftmpi_com_copy_errhandler (comm, nc );

  /* Create shadow-communicator for coll-ops */
  rc = ftmpi_com_create_shadow ( nc, nc+1);
  if ( rc < 0 ) return ( rc );

  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int   MPI_Comm_dup( MPI_Comm comm, MPI_Comm *newcom )
{
  int rc;
  MPI_Comm reqcom;

  CHECK_MPIINIT;
  CHECK_COM(comm);
/*   CHECK_FT(comm); */

  if( newcom == NULL) RETURNERR (comm, MPI_ERR_ARG);

  reqcom = *newcom;
  *newcom = MPI_COMM_NULL;	/* default value in case we fail */

  /* if ((comm!=MPI_COMM_WORLD)) return (MPI_ERR_COMM); */

  /* *** not enabled until we can sort out the error modes correctly!! *** */
  /* as most of the users never initialize variables before using them
   * here in reqcom we can get any value !!! So we could generate a recover
   * if we are really unlucky...
   */
  if ((reqcom==FT_MPI_CHECK_RECOVER)&&(comm==MPI_COMM_WORLD)) {
    printf("Checking for a failure and doing a recovery if found.\n");
    rc = ftmpi_sys_recovery_loop ();
    printf("DONE Checking for a failure and doing a recovery if found.\n");
    if (rc<0) return (MPI_ERR_OTHER);
    if (rc==1) {	/* did a recovery */
      printf("Performed a recovery\n");
      *newcom = comm;	/* should be MPI_COMM_WORLD */
      return (MPI_SUCCESS);
    }
    if (rc==0) {
      printf("No recovery needed\n");
    }
  }
  /* For Jeff */
  
  rc = ftmpi_mpi_comm_dup ( comm, newcom );
  if ( rc != MPI_SUCCESS ) RETURNERR ( comm, rc );

  return ( MPI_SUCCESS );
}


int ftmpi_mpi_comm_dup( MPI_Comm comm, MPI_Comm *newcom )
{
  /* ft_com_mode_t comm_mode;
     ft_com_state_t comm_state; */
  int nc;
  int rc;
  int org_grp;
  int new_grp;
  int inter;
  int nattr;

  *newcom = MPI_COMM_NULL;
  /* Here we have to distinguish between inter- and intra-comms, since
     the first communicator in com_lib_next_com has to be always
     an intra-comm.               EG. March 11 2003 */

  inter = ftmpi_com_test_inter ( comm ) ;
  /* nc = 0; */
  if ( inter )
    {
      MPI_Comm local_comm;

      local_comm = ftmpi_com_get_local_comm ( comm );
      nc = ftmpi_com_lib_next_com (FT_INTER_INTER, 0, local_comm, comm, 
				   0, 315);
    }
  else
    nc = ftmpi_com_lib_next_com (FT_INTRA_INTRA, 0, comm, MPI_COMM_NULL, 
				 MPI_PROC_NULL, MPI_ANY_TAG);

  if( nc <= 0 ) { /* something's wrong there */
    return nc;
  }
  rc = ftmpi_com_copy (comm, &nc); /* This routine copies the topology */
                                   /* stuff aswell.   EG Feb. 5 2003 */
  
  
  if (rc<0) return ( rc ); /* ftmpi_com_copy sets the correct MPI rc */
                           /* take note Tone, rc's get passed through */
                           /* multiple layers */

  /* we need to also update the com to group relationships... */
  org_grp = ftmpi_com_get_group (comm);	/* get original group */
  
  /* ftmpi_com_set_group (nc, org_grp);	*/
  /* make new com link to same group */
  /* note this increments grp ref cnts */
  /* we originally set both coms to point to the same group */
  /* but now they have their own groups */
  
  rc = ftmpi_group_copy (org_grp, &new_grp);
  if (rc<0) return ( rc );
  
  rc = ftmpi_com_set_group (nc, new_grp);
  if (rc<0) return ( rc );

  if ( inter ) ftmpi_com_copy_intercom ( comm, nc ); 
    
  /* Did the original communicator have any attributes attached to it ? */
  nattr  = ftmpi_com_get_nattr ( comm );
  if ( nattr > 0 ) {
    rc = ftmpi_com_copy_attr ( comm, nc ); /* this routine also calls the user-provided
					      copy-routine */
    if ( rc != MPI_SUCCESS ) {
      ftmpi_com_clr ( nc );
      *newcom = MPI_COMM_NULL;
      return ( rc );
    }
  }

  /* Copy error - handler */
  ftmpi_com_copy_errhandler ( comm, nc );

  /* Create shadow-communicator for coll-ops 
     For inter-communicators take nc+3 (instead of nc+1)
     since nc+1 is already the copy of the local_comm */
  if ( inter )
    rc = ftmpi_com_create_shadow ( nc, nc+3);
  else
    rc = ftmpi_com_create_shadow ( nc, nc+1);
  if ( rc < 0 ) return ( rc );

  *newcom = nc;	/* done, here you go */

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Topo_test ( MPI_Comm comm, int *status )
{
  CHECK_MPIINIT;
  CHECK_COM(comm);

  *status = ftmpi_com_get_topo ( comm );
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/


/* Support functions */


/* get next non clashing communicator value */
/* this could be in the next level down except it uses MPI_Calls */
/* and I don't want them in a layer below -lib-* */

/* This routine looks up the local next highest free location */
/* the lets the root decide the overall value and then */
/* distributes this value */
/* this value can then be used in a com_copy com_copy_subset etc */

/* Comments:
   local_leader: 0 if mode = FT_INTRA_INTRA, the value give by the user 
                 in the other two cases
   comm:       has to be always an intra-communicator; the old comm for 
               INTRA-INTA, the local_comm for FT_INTRA_INTER, and stored
	       copy of the local_comm for FT_INTER_INTRA.
   other_comm: is an intra-comm for FT_INTRA_INTER, an inter-comm 
               for FT_INTER_INTRA
   
*/

int ftmpi_com_lib_next_com (int mode, int local_leader, MPI_Comm comm, 
			    MPI_Comm other_comm, int remote_leader,
			    int tag) 
{

  int i, r, s;
  int next;
  int highest;
  int *ptr;
  int rc;
  MPI_Comm shadow;

  /* The first part is independent of the mode. Determine the
     highest possible value within the group
  */

  next = ftmpi_com_get_free_comm();
  s    = ftmpi_com_size (comm);
  r    = ftmpi_com_rank (comm);

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) return (MPI_ERR_INTERN);

  if (r==local_leader) 
    {
      ptr = (int*)_MALLOC(sizeof(int)*s);  /* make space for them */
      if (!ptr) return (MPI_ERR_OTHER);
    }
  else
    ptr = (int*)0;
  
  rc = ftmpi_coll_intra_gather_linear (&next, 1, MPI_INT, ptr, 1, MPI_INT, 
				       local_leader, shadow, 0);
  if (rc<0) 
    {
      if (r==local_leader) _FREE(ptr);
      return (rc);
    }
  
  /* ok the root can now decide the next com value */
  if (r==local_leader ) 
    {
      highest = 0;
      for(i=0;i<s;i++) 
	{
	  if (ptr[i]>highest) highest = ptr[i];	/* we keep highest */
	  if (ptr[i]==-1) 
	    {	/* if anyone is a -1 all bets are off */
	      highest=-1;
	      break;
	    }
	} /* for each submitted com value */
      
      _FREE(ptr);   /* finished with temp space so free it */
    } /* if root */
  

  switch ( mode )
    {
      /* ************************************************************/
      /* ************************************************************/
    case ( FT_INTRA_INTRA ):
      {
	/* it looks like for this case nothing else has to be done */
	break;
      }
      /* ************************************************************/
      /* ************************************************************/
    case ( FT_INTRA_INTER ):
    case ( FT_INTER_INTRA ):
    case ( FT_INTER_INTER ):
      {
	MPI_Status status;
	int r_highest;       /* highest of remote_group */
	int mygid, rgid;
	
	/* Additional step for these two cases: determine the highest
	   high from both groups/comms */

	if ( r == local_leader )
	  {
	    mygid = ftmpi_com_my_gid ();
	    rgid  = ftmpi_com_gid ( other_comm, remote_leader ); 
	    /* to avoid a deadlock, we try to get an order
	       in who is sending/receiveing first. This is
	       currently based on the gid of the local_leaders */
	    if ( mygid > rgid )
	      {
		rc = ftmpi_mpi_send (&highest, 1, MPI_INT, remote_leader, tag, 
				     other_comm );
		if ( rc != MPI_SUCCESS ) return ( rc );
		rc = ftmpi_mpi_recv (&r_highest, 1, MPI_INT, remote_leader, tag,
				     other_comm, &status );
		if ( rc != MPI_SUCCESS ) return ( rc );
	      }
	    else
	      {
		rc = ftmpi_mpi_recv (&r_highest, 1, MPI_INT, remote_leader, tag,
				     other_comm, &status );
		if ( rc != MPI_SUCCESS ) return ( rc );
		rc = ftmpi_mpi_send (&highest, 1, MPI_INT, remote_leader, tag, 
				     other_comm );
		if ( rc != MPI_SUCCESS ) return ( rc );
	      }
	    
	    if ( r_highest > highest ) 
	      highest = r_highest;
	  }

	break;
      }
      /* ************************************************************/
      /* ************************************************************/
    default:
      {
	printf("Unknown mode in ftmpi-lib-next-com: %d  \n", mode );
	return ( -1 );
      }
    }

  /* ok we now make sure everyone has a copy of the highest value :) */
  rc = ftmpi_coll_intra_bcast_bmtree (&highest, 1, MPI_INT, local_leader, 
				      shadow, 0);
  if (rc<0) return (rc);
  
  return ( highest );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* idcompare() compares gids so we can produce sorted lists of ids */

int idcompare (const void *p, const void *q)
{
int *a, *b;

/* i.e. we cast and just compare the first INTs */
a = (int*)p;
b = (int*)q;

if (*a < *b) return (-1);
if (*a == *b) return (0);
return (1);
}

/* rankkeygidcompare() compares a tuple of (rank,key,gid) producing sorted lists */
/* that match the rules needed for a MPI_Comm_split */


int rankkeycompare (const void *p, const void *q)
{
int *a, *b;

/* ranks at [0] key at [1] */
/* i.e. we cast and just compare the keys and then the original ranks.. */
a = (int*)p;
b = (int*)q;

/* simple tests are those where the keys are different */
if (a[1] < b[1]) return (-1);
if (a[1] > b[1]) return (1);

/* ok, if the keys are the same then we check the original ranks */
if (a[1] == b[1]) {
	if (a[0] < b[0]) return (-1);
	if (a[0] == b[0]) return (0);
	if (a[0] > b[0]) return (1);
}
 return ( 0 );
}

