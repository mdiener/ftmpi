
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@hlrs.de>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include "ft-mpi-f2c-ddt.h"
#include "ft-mpi-fprot.h"
#include "ft-mpi-ddt-sys.h"

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* void MPI_Address(void * particle,int * disp) */
void mpi_address_(void * particle,int * disp,int * __ierr)   
{
  *__ierr = MPI_Address((void *)ToPtr(particle),(MPI_Aint *)ToPtr(disp));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Get_count(MPI_Status *status,MPI_Datatype datatype,
   int *count)*/
void mpi_get_count_(int * status,int * ddt,int *cnt,int * __ierr)   
{
  *__ierr = MPI_Get_count((MPI_Status *)ToPtr(status),
			  (MPI_Datatype)fdt2c((*((int *)ddt))),
			  &(*((int *)cnt)));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Get_elements(MPI_Status *status,MPI_Datatype ddt,int *count)*/
void mpi_get_elements_(int * status,int * ddt,int * cnt,int * __ierr)   
{
  *__ierr = MPI_Get_elements((MPI_Status *)ToPtr(status),
			     (MPI_Datatype)fdt2c((*((int *)ddt))),
			     (int *)ToPtr(cnt));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_free(int * ddt) */
void mpi_type_free_(int * ddt,int * __ierr) 
{
  *__ierr = MPI_Type_free((MPI_Datatype*)ToPtr(ddt));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_size (int type,int * size) */
void mpi_type_size_(int * ddt,int *size,int * __ierr)   
{
  *__ierr = MPI_Type_size((MPI_Datatype)fdt2c((*((int *)ddt))),
			  (int *)ToPtr(size));
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_extent (int type,int * extent) */
void mpi_type_extent_(int * ddt,int *extent,int * __ierr)   
{
  *__ierr = MPI_Type_extent((MPI_Datatype)fdt2c((*((int *)ddt))),
			    (MPI_Aint *)ToPtr(extent));
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_struct(int cnt,int *array_block,int *array_disp,
   int *array_types,int *ret_handle) */
void mpi_type_struct_(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr)
{
  *__ierr = MPI_Type_struct((*((int *)cnt)),(int *)ToPtr(array_block),
			    (MPI_Aint *)ToPtr(array_disp),
			    (MPI_Datatype *)ToPtr(ddt),
			    (int *)ToPtr(ret_handle));
  if(*__ierr == MPI_SUCCESS) ftmpi_ddt_set_type(*(int *)ToPtr(ret_handle),MPI_COMBINER_STRUCT_INTEGER);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_contiguous(int cnt,int ddt,int *ret_handle) */
void mpi_type_contiguous_(int *cnt,int *ddt,int *ret_handle,int *__ierr)  
{
  *__ierr = MPI_Type_contiguous((*((int *)cnt)),
				(MPI_Datatype)fdt2c((*((int *)ddt))),
				(int *)ToPtr(ret_handle));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_vector(int cnt,int block_length,int stride,int ddt,
   int *ret_handle) */
void mpi_type_vector_(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr)  
{
  *__ierr = MPI_Type_vector((*((int *)cnt)),(*((int *)block_length)),
			    (*((int *)stride)),
			    (MPI_Datatype)fdt2c((*((int *)ddt))),
			    (int *)ToPtr(ret_handle));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_hvector(int cnt,int block_length,int stride,int ddt,
   int * ret_handle) */
void mpi_type_hvector_(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr)  
{
  *__ierr = MPI_Type_hvector((*((int *)cnt)),(*((int *)block_length)),
			     (*((int *)stride)),
			     (MPI_Datatype)fdt2c((*((int *)ddt))),
			     (int *)ToPtr(ret_handle));
  if(*__ierr == MPI_SUCCESS) ftmpi_ddt_set_type(*(int *)ToPtr(ret_handle),MPI_COMBINER_HVECTOR_INTEGER);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_indexed(int cnt,int * array_blocks,int * array_disp,
   int ddt,int * ret_handle) */
void mpi_type_indexed_(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr)  
{
  *__ierr = MPI_Type_indexed((*((int *)cnt)),(int *)ToPtr(array_blocks),
			     (int *)ToPtr(array_disp),
			     (MPI_Datatype)fdt2c((*((int *)ddt))),
			     (int *)ToPtr(ret_handle));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_hindexed(int cnt,int * array_blocks,int * array_disp,
   int ddt,int * ret_handle) */
void mpi_type_hindexed_(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr)  
{
  *__ierr = MPI_Type_hindexed((*((int *)cnt)),(int *)ToPtr(array_blocks),
			      (MPI_Aint *)ToPtr(array_disp),
			      (MPI_Datatype)fdt2c((*((int *)ddt))),
			      (int *)ToPtr(ret_handle));
  if(*__ierr == MPI_SUCCESS) ftmpi_ddt_set_type(*(int *)ToPtr(ret_handle),MPI_COMBINER_HINDEXED_INTEGER);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_lb(MPI_Datatype ddt,int * displacement) */
void mpi_type_lb_(int * ddt,int * displacement,int * __ierr)
{
  *__ierr = MPI_Type_lb((MPI_Datatype)(*((int *)ddt)),
			(MPI_Aint *)ToPtr(displacement));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_ub(MPI_Datatype ddt,int * displacement) */
void mpi_type_ub_(int * ddt,int * displacement,int * __ierr)
{
  *__ierr = MPI_Type_ub((MPI_Datatype)(*((int *)ddt)),
			(MPI_Aint *)ToPtr(displacement));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_commit(int * ddt) */
void mpi_type_commit_(int * ddt,int * __ierr)  
{
  *__ierr = MPI_Type_commit((MPI_Datatype *)ToPtr(ddt));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Pack_size(int incount,MPI_Datatype ddt,MPI_Comm comm,
   int *size)*/
void mpi_pack_size_(int * cnt,int *ddt,int *comm,int *size,int *__ierr)
{
  *__ierr = MPI_Pack_size((*((int *)cnt)),
			  (MPI_Datatype)fdt2c((*((int *)ddt))),
			  (MPI_Comm)(*((int *)comm)),(int *)ToPtr(size));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Pack(void * inbuf,int cnt, MPI_Datatype ddt,void * outbuf,int
   out_size,int * position,MPI_Comm comm) */
void mpi_pack_(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	      int * position,int * comm,int * __ierr)
{
  *__ierr = MPI_Pack((void *)ToPtr(inbuf),(*((int *)cnt)), 
		     (MPI_Datatype)(*((int *)ddt)),(void *)ToPtr(outbuf), 
		     (*((int*)out_size)),(int *)ToPtr(position), 
		     (MPI_Comm)(*((int *)comm)));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Unpack(void * inbuf,int insize,int * position,void * outbuf,
   int cnt,MPI_Datatype ddt,MPI_Comm comm) */

void mpi_unpack_(void * inbuf,int * insize,int * position,void * outbuf,
		int * cnt,int *ddt,int *comm, int * __ierr)
{
  *__ierr = MPI_Unpack((void *)ToPtr(inbuf),(*((int *)insize)),
		       (int *)ToPtr(position),(void *)ToPtr(outbuf),
		       (*((int *)cnt)),(MPI_Datatype)(*((int *)ddt)),
		       (MPI_Comm)(*((int *)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_create_darray(int size,int rank,int ndims,
   int array_of_gsizes[],int array_of_distribs[],int array_of_dargs[],
   int array_of_psizes[],int order,MPI_Datatype oldtype,
   MPI_Datatype *newtype) */
void mpi_type_create_darray_(int *size,int *rank,int *ndims,
			     int *array_of_gsizes,int *array_of_distribs,
			     int *array_of_dargs,int *array_of_psizes,
			     int *order,int *oldtype,int * newtype,
			     int * __ierr)
{
  *__ierr = MPI_Type_create_darray((*((int *)size)),(*((int *)rank)),
				   (*((int *)ndims)),
				   (int *)ToPtr(array_of_gsizes),
				   (int *)ToPtr(array_of_distribs),
				   (int *)ToPtr(array_of_dargs),
				   (int *)ToPtr(array_of_psizes),
				   (*((int *)order)),
				   (MPI_Datatype)(*((int *)oldtype)),
				   (MPI_Datatype *)ToPtr(newtype));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_create_subarray(int ndims,int array_of_sizes[],
   int array_of_subsizes[],int array_of_starts[],int order,
   MPI_Datatype oldtype,MPI_Datatype *newtype) */
 void mpi_type_create_subarray_(int *ndims,int *array_of_sizes,
			       int *array_of_subsizes,
			       int * array_of_starts,int * order,
			       int * oldtype,int * newtype,int * __ierr)
{
  *__ierr = MPI_Type_create_subarray((*((int *)ndims)),
				     (int *)ToPtr(array_of_sizes),
				     (int *)ToPtr(array_of_subsizes),
				     (int *)ToPtr(array_of_starts),
				     (*((int *)order)),
				     (MPI_Datatype)(*((int *)oldtype)),
				     (MPI_Datatype *)ToPtr(newtype));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_get_contents(MPI_Datatype ddt,int * max_integers,
   int * max_addresses,int * max_datatypes,int array_of_integers[],
   int array_of_addresses[],int array_of_datatypes[]) */
 void mpi_type_get_contents_(int *ddt,int *max_integers,
			    int *max_addresses,int *max_datatypes,
			    int *array_of_integers,int *array_of_addresses,
			    int * array_of_datatypes,int * __ierr)
{
  *__ierr = MPI_Type_get_contents((MPI_Datatype)(*((int *)ddt)),
				  (int)*max_integers,
				  (int)*max_addresses,
				  (int)*max_datatypes,
				  (int *)ToPtr(array_of_integers),
				  (MPI_Aint *)ToPtr(array_of_addresses),
				  (MPI_Datatype *)ToPtr(array_of_datatypes));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Type_get_envelope(MPI_Datatype ddt,int * num_integers,
   int * num_addresses,int * num_datatypes,int * combiner) */
 void mpi_type_get_envelope_(int * ddt,int * num_integers,
			    int * num_addresses,int * num_datatypes,
			    int * combiner,int * __ierr)
{
  *__ierr = MPI_Type_get_envelope((MPI_Datatype)(*((int *)ddt)),
				  (int *)ToPtr(num_integers),
				  (int *)ToPtr(num_addresses),
				  (int *)ToPtr(num_datatypes),
				  (int *)ToPtr(combiner));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
