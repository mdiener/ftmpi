#include "stdio.h"
#include "stdlib.h"
#include <sys/time.h>
#include <unistd.h>
#include "ft-mpi-lib.h"
#include "ft-mpi-op.h"


extern int _ATB_MPI_OP_INIT;
extern int _ATB_MPI_OP_B_CNT;
extern int _ATB_MPI_OP_CNT;
extern int _ATB_MPI_OP_MAX;
extern _ATB_MPI_OP * _ATB_MPI_OPS_ARRAY;


/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Op_create            = PMPI_Op_create
#    pragma weak  MPI_Op_free              = PMPI_Op_free

#endif

#    define  MPI_Op_create             PMPI_Op_create
#    define  MPI_Op_free               PMPI_Op_free




int MPI_Op_create(MPI_User_function * function,int commute, MPI_Op * op)
{
  _ATB_MPI_OP new_op;

  if(_ATB_MPI_OP_INIT != 1){
    _atb_init_op();
  }
  if(!_ATB_MPI_OP_INIT){
    printf("FTMPI: _ATB_INIT_OP has not been called ... error ..exiting\n");
    exit(MPI_ERR_INTERN);
  }

  new_op.commute = commute;
  new_op.op = function;
  new_op.permanent = 0;

  *op = _atb_op_add(&new_op);
  return(MPI_SUCCESS);
}



int MPI_Op_free(MPI_Op * op)
{
  if(_ATB_MPI_OP_INIT != 1){
    _atb_init_op();
  }

  if(!_ATB_MPI_OP_INIT){
    printf("FTMPI: _ATB_INIT_OP has not been called ... error ..exiting\n");
    exit(MPI_ERR_INTERN);
  }

  if(*op < 0 || *op > _ATB_MPI_OP_CNT){
    printf("FTMPI: MPI_OP is an invalid value [%d]\n",*op);
    return(MPI_ERR_OP);
  }

  if(_ATB_MPI_OPS_ARRAY[*op].permanent == 0){ /* if I CAN REMOVE IT ... */
    /*    printf("FTMPI: REMOVING OP %d\n",*op); */
    _ATB_MPI_OPS_ARRAY[*op].op = NULL;
    _ATB_MPI_OPS_ARRAY[*op].commute = -1;
    _ATB_MPI_OPS_ARRAY[*op].permanent = 1;
    _ATB_MPI_OP_CNT--;
  }
  else {
    printf("FTMPI: CANNOT REMOVE A PERMANENT OPERATION AT [%d]\n",*op);
    return(MPI_ERR_OP);
  }
  *op = MPI_OP_NULL;
  return(MPI_SUCCESS);
}
