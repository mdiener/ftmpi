
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include "ft-mpi-f2c-com.h"
#include "ft-mpi-fprot.h"

extern int FTMPI_KEYVAL_FORTRAN;

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_comm_rank_ (int* comm,int * rank,int * __ierr )
{
  *__ierr = MPI_Comm_rank((MPI_Comm)(*((int*)comm)),(int *)ToPtr(rank));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_comm_size_(int* comm,int * size,int * __ierr )
{
  *__ierr = MPI_Comm_size((MPI_Comm)(*((int*)comm)),(int *)ToPtr(size));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_comm_dup_ (int * comm,int * newcomm,int * __ierr)
{
  *__ierr = MPI_Comm_dup((MPI_Comm)(*((int*)comm)),
			 (MPI_Comm *)ToPtr(newcomm));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_create(MPI_Comm comm,MPI_Group group,MPI_Comm *newcomm)*/
void mpi_comm_create_(int * comm,int * group,int * newcomm,int * __ierr)
{
 *__ierr = MPI_Comm_create((MPI_Comm)(*((int*)comm)),
			   (MPI_Group)(*((int*)group)),
			   (MPI_Comm *)ToPtr(newcomm));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_free(MPI_Comm *comm); */
void mpi_comm_free_(int * comm,int * __ierr)
{
 *__ierr = MPI_Comm_free((MPI_Comm *)ToPtr(comm));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_group(MPI_Comm comm,MPI_Group * group); */
void mpi_comm_group_(int * comm,int * group,int * __ierr)
{
 *__ierr = MPI_Comm_group((MPI_Comm)(*((int*)comm)),
			  (MPI_Group *)ToPtr(group));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_remote_group(MPI_Comm comm,MPI_Group * group) */
void mpi_comm_remote_group_(int * comm,int * group,int * __ierr)
{
  *__ierr = MPI_Comm_remote_group((MPI_Comm)(*((int *)comm)),
				  (MPI_Group *)ToPtr(group));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_remote_size(MPI_Comm comm,int * size) */
void mpi_comm_remote_size_(int * comm,int * size,int * __ierr)
{
  *__ierr = MPI_Comm_remote_size((MPI_Comm)(*((int *)comm)),
				 (int *)ToPtr(size));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_split(MPI_Comm comm,int color,int key,MPI_Comm *newcomm)*/
void mpi_comm_split_(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr)
{
   *__ierr = MPI_Comm_split((MPI_Comm)(*((int *)comm)),(*((int *)color)),
			    (*((int *)key)),(MPI_Comm *)ToPtr(newcomm));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_test_inter(MPI_Comm comm,int * flag) */
void mpi_comm_test_inter_(int * comm,int * flag,int * __ierr)
{
  *__ierr = MPI_Comm_test_inter((MPI_Comm)(*((int *)comm)),
				(int *)ToPtr(flag));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_comm_compare_ (int *comm1, int* comm2, int* result, int* __ierr)
{
  *__ierr = MPI_Comm_compare ( (MPI_Comm) (*((int *)comm1)),
			       (MPI_Comm) (*((int *)comm2)), 
			       (int *)ToPtr(result)); 
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_create_keyval(MPI_Comm_copy_attr_function *copy_fn, 
   MPI_Comm_delete_attr_function *delete_fn,int * keyval,
   void * extra_state) */
void mpi_comm_create_keyval_(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr)
{
  /* This is a global variable checked deep in ftmpi whether the
     MPI_Keyval_create call is coming from fortran or not. The reason
     for this is, that when calling the copy-functions provided by the
     user, the arguments to this function have to be passed
     differently depending on whether it is implemented in fortran or
     in C.

     I know this solution is not the "non plus ultra" solution, but
     the alternative would be to introduce an 'own' version of
     Keyval_create with an additional argument which indicates whether
     it is a fortran or a c-routine. I preferr currently this version,
     howver, the other might be realized later on.

     EG Feb 27 2003 */

  FTMPI_KEYVAL_FORTRAN = 1;
  *__ierr = MPI_Comm_create_keyval((MPI_Comm_copy_attr_function *)ToPtr(copy_fn),
				   (MPI_Comm_delete_attr_function *)ToPtr(delete_fn),
				   (int *)ToPtr(keyval),
				   (void *)ToPtr(extra_state)); 
  FTMPI_KEYVAL_FORTRAN = 0;

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_free_keyval (int * keyval) */
void mpi_comm_free_keyval_ (int * keyval,int * __ierr)
{
  *__ierr = MPI_Comm_free_keyval((int *)ToPtr(keyval));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_delete_attr_(MPI_Comm comm,int keyval) */
void mpi_comm_delete_attr_(int * comm,int * keyval,int * __ierr)
{
  *__ierr = MPI_Comm_delete_attr((MPI_Comm)(*((int *)comm)),
				 (*((int *)keyval)));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*int MPI_Comm_get_attr(MPI_Comm comm,int keyval,void *attribute_val,
  int *flag)*/
void mpi_comm_get_attr_(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr)
{
  int *cval;

  *__ierr = MPI_Comm_get_attr((MPI_Comm)(*((int *)comm)),(*((int *)keyval)),
			      (void*)&cval,(int *)ToPtr(flag));
  if ( *flag )
    {
      switch (*keyval)
	{
	case MPI_TAG_UB:	
	case MPI_IO :
	case MPI_HOST:
	case MPI_WTIME_IS_GLOBAL:
	case MPI_LASTUSEDCODE:
	  *attribute_val = (int)(*cval);
	  break;
	default:
	  *attribute_val = (int)(cval);
	  break;
	}	  

    }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_set_attr(MPI_Comm comm,int keyval,void * attribute_val) */
void mpi_comm_set_attr_(int *comm,int *keyval,int *attribute_val,int *__ierr)
{
  *__ierr = MPI_Comm_set_attr((MPI_Comm)(*((int *)comm)),(*((int *)keyval)),
			      (void *)((int)*attribute_val));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_create_errhandler(MPI_Comm_errhandler_fn *function,
   MPI_Errhandler *errhandler) */
void mpi_comm_create_errhandler_(int * function,int * errhandler,int * __ierr)
{
  *__ierr = MPI_Comm_create_errhandler((MPI_Comm_errhandler_fn *)ToPtr(function),
				       (MPI_Errhandler *)ToPtr(errhandler));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_get_errhandler(MPI_Comm comm,MPI_Errhandler *errhandler) */
void mpi_comm_get_errhandler_(int * comm,int *errhandler,int * __ierr)
{
  *__ierr = MPI_Comm_get_errhandler((MPI_Comm)(*((int *)comm)),
				    (MPI_Errhandler *)ToPtr(errhandler));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Comm_set_errhandler(MPI_Comm comm,MPI_Errhandler errhandler) */
void mpi_comm_set_errhandler_(int * comm,int * errhandler,int * __ierr)
{
  *__ierr = MPI_Comm_set_errhandler((MPI_Comm)(*((int *)comm)),
				    (MPI_Errhandler)(*((int *)errhandler)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_comm_call_errhandler_(int *comm, int* errcode, int * __ierr )
{
  *__ierr = MPI_Comm_call_errhandler ( (MPI_Comm) *comm, (int) *errcode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Intercomm_create(MPI_Comm local_comm,int local_leader,
   MPI_Comm peer_comm,int remote_leader,int tag,MPI_Comm *newintercom)*/

void mpi_intercomm_create_(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr)
{
  *__ierr = MPI_Intercomm_create((MPI_Comm)(*((int *)local_comm)),
				 (*((int *)local_leader)),
				 (MPI_Comm)(*((int *)peer_comm)),
				 (*((int *)remote_leader)),
				 (*((int *)tag)),
				 (MPI_Comm *)ToPtr(newintercom));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*MPI_Intercomm_merge(MPI_Comm intercomm,int high,
  MPI_Comm *newintracom) */
void mpi_intercomm_merge_(int *intercomm,int * high,int *newintracom,
			  int *__ierr)
{
  *__ierr = MPI_Intercomm_merge((MPI_Comm)(*((int *)intercomm)),
				(*((int *)high)),
				(MPI_Comm *)ToPtr(newintracom));
}
