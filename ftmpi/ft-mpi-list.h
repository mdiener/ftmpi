
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Robert Manchek <pvm@msr.csm.ornl.gov>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	Originally part of PVMPI/MPI_Connect by Graham 
*/




#ifndef LISTSH
#define LISTSH


/* MACROS required to manipulate the list structures */
/* These macros are originaly from a {broken} PVM3.3 tasker demo. */
/* Used with thanks to Bob Manchek, who made PVM what it is today ;) */

#define LISTPUTAFTER(o,n,f,r) \
  { (n)->f=(o)->f; (n)->r=o; (o)->f->r=n; (o)->f=n; }
#define LISTPUTBEFORE(o,n,f,r) \
  { (n)->r=(o)->r; (n)->f=o; (o)->r->f=n; (o)->r=n; }
#define LISTDELETE(e,f,r) \
  { (e)->f->r=(e)->r; (e)->r->f=(e)->f; (e)->r=(e)->f=0; }

#define TALLOC(n,t,g) (t*)_MALLOC((n)*sizeof(t))
#define FREE(p) _FREE((char *)p)
#define STRALLOC(s)    strcpy(TALLOC(strlen(s)+1,char,"str"),s)

#endif /* LISTSH */


