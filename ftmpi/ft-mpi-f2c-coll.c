
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include "ft-mpi-f2c-coll.h"
#include "ft-mpi-fprot.h"

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Barrier(MPI_Comm comm ) */
void mpi_barrier_ (int *comm,int * __ierr )
{
  *__ierr = MPI_Barrier((*((MPI_Comm*)comm)));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*int MPI_Bcast(void *buf,int cnt,MPI_Datatype ddt,int root,
  MPI_Comm comm)*/ 
void mpi_bcast_(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) 
{
  *__ierr = MPI_Bcast((void *)ToPtr(buf),(*((int *)cnt)),
		      (MPI_Datatype)fdt2c((*((int *)ddt))),
		      (*((int*)root)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Reduce(void * sbuf,void * rbuf,int cnt,MPI_Datatype ddt,
   MPI_Op op,int root,MPI_Comm comm) */
void mpi_reduce_(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) 
{
  *__ierr = MPI_Reduce((void *)ToPtr(sbuf),(void *)ToPtr(rbuf),
		       (*((int *)cnt)),
		       (MPI_Datatype)fdt2c((*((int *)ddt))),
		       (MPI_Op)(*((int *)op)),(*((int *)root)),
		       (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Reduce_scatter(void * sbuf,void * rbuf,int * cnt,
   MPI_Datatype ddt,MPI_Op op,MPI_Comm comm) */
void mpi_reduce_scatter_(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr)
{
  *__ierr = MPI_Reduce_scatter((void *)ToPtr(sbuf),(void *)ToPtr(rbuf),
			       (int *)ToPtr(cnt),
			       (MPI_Datatype)fdt2c((*((int *)ddt))),
			       (MPI_Op)(*((int *)op)),
			       (MPI_Comm)(*((int *)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Allreduce(void * sbuf,void * rbuf,int cnt,MPI_Datatype ddt,
   MPI_Op op,MPI_Comm comm) */
void mpi_allreduce_(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) 
{
  *__ierr = MPI_Allreduce((void *)ToPtr(sbuf),(void *)ToPtr(rbuf),
			  (*((int *)cnt)),
			  (MPI_Datatype)fdt2c((*((int *)ddt))),
			  (MPI_Op)(*((int *)op)),
			  (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Allgather(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
   int rcnt,MPI_Datatype rddt,MPI_Comm comm) */
void mpi_allgather_(void *sbuf,int * scnt,int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) 
{
  *__ierr = MPI_Allgather((void *)ToPtr(sbuf),(*((int *)scnt)),
			  (MPI_Datatype)fdt2c(*sddt),
			  (void*)ToPtr(rbuf),(*((int *)rcnt)),
			  (MPI_Datatype)fdt2c(*rddt),
			  (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_allgatherv_(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr)
{
  *__ierr = MPI_Allgatherv((void *)sbuf,(*((int *)scnt)),
			   (MPI_Datatype)fdt2c((*((int *)sddt))),
			   (void *)ToPtr(rbuf),(int *)ToPtr(rcnt),
			   (int *)ToPtr(displs),
			   (MPI_Datatype)(*((int *)rddt)),
			   (MPI_Comm)(*((int *)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Alltoall(void* sbuf,int scnt,MPI_Datatype sddt,void* rbuf,
   int rcnt,MPI_Datatype rddt,MPI_Comm comm); */
void mpi_alltoall_(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) 
{
  *__ierr = MPI_Alltoall((void *)ToPtr(sbuf),(*((int *)scnt)),
			 (MPI_Datatype)fdt2c((*((int *)sddt))),
			 (void *)ToPtr(rbuf),(*((int *)rcnt)),
			 (MPI_Datatype)fdt2c((*((int *)rddt))),
			 (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Alltoallv(void *sbuf,int *scnt,int *sdisp,MPI_Datatype sddt,
   void* rbuf,int *rcnts,int *rdisp,MPI_Datatype rddt,MPI_Comm comm)*/
void mpi_alltoallv_(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) 
{
  *__ierr = MPI_Alltoallv((void *)ToPtr(sbuf),(int *)ToPtr(scnt),
			  (int *)ToPtr(sdisp),
			  (MPI_Datatype)fdt2c((*((int *)sddt))),
			  (void *)ToPtr(rbuf),(int *)ToPtr(rcnt),
			  (int *)ToPtr(rdisp),
			  (MPI_Datatype)fdt2c((*((int *)rddt))),
			  (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Gather(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
   int rcnt,MPI_Datatype rddt,int root,MPI_Comm comm) */
void mpi_gather_(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) 
{
  *__ierr = MPI_Gather((void *)ToPtr(sbuf),(*((int *)scnt)),
		       (MPI_Datatype)fdt2c((*((int *)sddt))),
		       (void *)ToPtr(rbuf),(*((int *)rcnt)),
		       (MPI_Datatype)fdt2c((*((int *)rddt))),
		       (*((int*)root)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Gatherv(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
   int * rcnt,int *rdisp,MPI_Datatype rddt,int root,MPI_Comm comm) */
void mpi_gatherv_(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) 
{
  *__ierr = MPI_Gatherv((void *)ToPtr(sbuf),(*((int *)scnt)),
			(MPI_Datatype)fdt2c((*((int *)sddt))),
			(void *)ToPtr(rbuf),(int *)ToPtr(rcnt),
			(int *)ToPtr(rdisp),
			(MPI_Datatype)fdt2c((*((int *)rddt))),
			(*((int*)root)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Scatter(void *sbuf,int scnt,MPI_Datatype sddt,void *rbuf,
   int rcnt,MPI_Datatype rddt,int root,MPI_Comm comm) */
void mpi_scatter_(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr)
{
  *__ierr = MPI_Scatter((void *)ToPtr(sbuf),(*((int *)scnt)),
			(MPI_Datatype)fdt2c((*((int *)sddt))),
			(void *)ToPtr(rbuf),(*((int *)rcnt)),
			(MPI_Datatype)fdt2c((*((int *)rddt))),
			(*((int *)root)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Scatterv(void *sbuf,int * scnt,int * sdisp,MPI_Datatype sddt,
   void *rbuf,int rcnt,MPI_Datatype rddt,int root,MPI_Comm comm) */
void mpi_scatterv_(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) 
{
  *__ierr = MPI_Scatterv((void *)ToPtr(sbuf),(int *)ToPtr(scnt),
			 (int *)ToPtr(sdisp),
			 (MPI_Datatype)fdt2c((*((int *)sddt))),
			 (void *)ToPtr(rbuf),(*((int *)rcnt)),
			 (MPI_Datatype)fdt2c((*((int *)rddt))),
			 (*((int *)root)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Scan(void * sbuf,void * rbuf,int cnt,MPI_Datatype ddt,
   MPI_Op op,MPI_Comm comm) */
void mpi_scan_(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) 
{
  *__ierr = MPI_Scan((void *)ToPtr(sbuf),(void *)ToPtr(rbuf),
		     (*((int *)cnt)),(MPI_Datatype)fdt2c((*((int *)ddt))),
		     (MPI_Op)(*((int *)op)),(MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
