/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors of this file:	
           Edgar Gabriel <egabriel@cs.utk.edu>
           Graham E Fagg <fagg@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

#ifndef _FT_MPI_BUFFER_H_
#define _FT_MPI_BUFFER_H_

typedef struct _unused_buffer {
  void *buffer;
  long size;
  struct _unused_buffer *next;
  struct _unused_buffer *prev;
} unused_buffer;


typedef struct _used_buffer {
  void        *buffer;
  long        size;
  int         lcontrol; /* 0: user controled request; 1: local controled request */
  MPI_Request req;
  struct _used_buffer *next;
  struct _used_buffer *prev;
} used_buffer;

typedef struct{
  void *buffer;
  long size;
  int num_unused;
  unused_buffer *unused;
  int num_used;
  used_buffer *used;
} ftmpi_buffer_t;


/* Prototypes */
int ftmpi_buffer_init (void *buffer, int size );
int ftmpi_buffer_isattached (void);
int ftmpi_buffer_size ( void);
void ftmpi_buffer_flush ( void );
int ftmpi_buffer_detach ( void **buf,int *size );
int ftmpi_buffer_unused_from_used (used_buffer *useg);
void ftmpi_buffer_unused_merge_all ( void );
void ftmpi_buffer_unused_merge_two (unused_buffer *seg1, unused_buffer *seg2);
int ftmpi_buffer_unused_split (unused_buffer *seg, long newsize);
void ftmpi_buffer_unused_remove ( unused_buffer *seg );
unused_buffer* ftmpi_buffer_unused_getnext ( int size );
void ftmpi_buffer_unused_dump ( void );
used_buffer* ftmpi_buffer_used_from_unused ( unused_buffer *seg );
int ftmpi_buffer_used_test ( void);
int ftmpi_buffer_used_wait ( void);
void ftmpi_buffer_used_remove ( used_buffer *seg );
void ftmpi_buffer_used_dump ( void );
used_buffer *ftmpi_buffer_getnextseg ( int packsize,  void **bufp);
void ftmpi_buffer_used_setlcontrol ( used_buffer *handle );
void ftmpi_buffer_used_setreq ( used_buffer *handle, MPI_Request req );
void ftmpi_buffer_used_free ( MPI_Request req );

#endif
