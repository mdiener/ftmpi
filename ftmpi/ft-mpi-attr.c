
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@Cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* The following routines are implemented in this file:
 - ftmpi_key_init 
 - ftmpi_key_free 
 - ftmpi_key_copy 
 - ftmpi_key_get_next_free 
 - ftmpi_key_mark_free 
 - ftmpi_key_test_freed 
 - ftmpi_key_release 
 - ftmpi_key_test 
 - ftmpi_key_set 

 - ftmpi_keyval_set 
 - ftmpi_keyval_get
 - ftmpi_keyval_delete 
 - ftmpi_keyval_test 

 - ftmpi_comm_null_copy_fn 
 - ftmpi_comm_dup_fn 
 - ftmpi_comm_delete_fn 
 - ftmpi_type_null_copy_fn 
 - ftmpi_type_dup_fn 
 - ftmpi_type_delete_fn 
 - ftmpi_win_null_copy_fn 
 - ftmpi_win_dup_fn 
 - ftmpi_win_delete_fn 
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-attr.h"
#include "debug.h"

key_info_t *ftmpi_keyarr          = NULL;
int         ftmpi_keyarr_size     = 5;
int         ftmpi_key_initialized = 0;

/* This variable is declared in  ft-mpi-f2c.c 
   Explanations to this variable can be found at
   ft-mpi-f2.c in the MPI_Keyval_create section 

   EG Feb. 27 2003 
*/

extern int FTMPI_KEYVAL_FORTRAN;

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Allocate and Initialize the key-array; called either from MPI_Init  */
/* or at first usage */
key_info_t * ftmpi_key_init ( key_info_t *k )
{
  int i;


  k = ( key_info_t *)_MALLOC( ftmpi_keyarr_size * sizeof ( key_info_t ));
  if ( k == NULL ) return ( NULL );

  memset ( (void* ) k, 0, ftmpi_keyarr_size * sizeof(key_info_t));

  for ( i = 0; i < ftmpi_keyarr_size; i ++ )
    {
      k[i].key    = i;
      k[i].in_use = 0; /* not really necessary, e.g. memset ? */
    }
  
  return ( k );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* free the key-array. Called usually from MPI_Finalize or when
   reallocating the key-array */
int ftmpi_key_free ( key_info_t *k)
{
  int i;

  if ( k != NULL )
    {
      for ( i = 0; i < ftmpi_keyarr_size; i ++ )
	{
	  if ( ftmpi_keyarr[i].in_use )
	    ftmpi_key_release ( i );
	}
      
      _FREE( k );
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Copy an element of the key-array into another element. Used */
/* when we have to re-allocate the key-array to increase the size of */
/* the array */
/* copy k2 onto k1  */
int ftmpi_key_copy ( key_info_t *k1, key_info_t *k2 )
{

  k1->in_use      = k2->in_use;
  k1->freed       = k2->freed;
  k1->extra_state = k2->extra_state;
  k1->type        = k2->type;
  k1->fortran     = k2->fortran;

  if ( k1->type == FTMPI_KEY_COMM1 )
    {
      k1->funcs.mpi1.copy_fn  = k2->funcs.mpi1.copy_fn;
      k1->funcs.mpi1.del_fn   = k2->funcs.mpi1.del_fn;
      k1->funcs.mpi1.fcopy_fn = k2->funcs.mpi1.fcopy_fn;
      k1->funcs.mpi1.fdel_fn  = k2->funcs.mpi1.fdel_fn;
    }
  else if ( k1->type == FTMPI_KEY_COMM2 )
    {
      k1->funcs.mpi2comm.copy_fn  = k2->funcs.mpi2comm.copy_fn;
      k1->funcs.mpi2comm.del_fn   = k2->funcs.mpi2comm.del_fn;
      k1->funcs.mpi2comm.fcopy_fn = k2->funcs.mpi2comm.fcopy_fn;
      k1->funcs.mpi2comm.fdel_fn  = k2->funcs.mpi2comm.fdel_fn;
    }
  else if ( k1->type == FTMPI_KEY_WIN )
    {
      k1->funcs.mpi2win.copy_fn  = k2->funcs.mpi2win.copy_fn;
      k1->funcs.mpi2win.del_fn   = k2->funcs.mpi2win.del_fn;
      k1->funcs.mpi2win.fcopy_fn = k2->funcs.mpi2win.fcopy_fn;
      k1->funcs.mpi2win.fdel_fn  = k2->funcs.mpi2win.fdel_fn;
    }
  else if ( k1->type == FTMPI_KEY_TYPE )
    {
      k1->funcs.mpi2type.copy_fn  = k2->funcs.mpi2type.copy_fn;
      k1->funcs.mpi2type.del_fn   = k2->funcs.mpi2type.del_fn;
      k1->funcs.mpi2type.fcopy_fn = k2->funcs.mpi2type.fcopy_fn;
      k1->funcs.mpi2type.fdel_fn  = k2->funcs.mpi2type.fdel_fn;
    }


  k1->maxvalues  = k2->maxvalues;
  k1->nvalues    = k2->nvalues;

  k1->values = (keyval_info_t *)_MALLOC( k1->maxvalues * sizeof(keyval_info_t));
  if ( k1->values == NULL ) return ( MPI_ERR_INTERN );

  memcpy ( k1->values, k2->values, (size_t) (k1->maxvalues * sizeof(keyval_info_t)));

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* return the key-value for the next free entry, reserve it */
/* and allocate the values-array according to the type-setting */
/* type can be: */
/*     FTMPI_KEY_COMM1: called from Keyval_create */
/*     FTMPI_KEY_COMM2: called from Comm_keyval_create */
/*     FTMPI_KEY_WIN:  called from Win_keyval_create */
/*     FTMPI_KEY_TYPE: called from Type_keyval_create */
int ftmpi_key_get_next_free ( int type )
{
  int i, found = 0;
  int ret = MPI_ERR_INTERN ;
  key_info_t *nk=NULL;
  key_info_t *dummy;

  if ( !ftmpi_key_initialized )
    {
      ftmpi_keyarr = ftmpi_key_init ( ftmpi_keyarr );
      if ( ftmpi_keyarr == NULL ) return ( MPI_ERR_INTERN );
      ftmpi_key_initialized = 1;
    }
  
  /* Search next free element; if no free element found, increase*/
  /* the size of the array */
  for ( i = 0; i < ftmpi_keyarr_size; i ++ )
    {
      if ( !ftmpi_keyarr[i].in_use )
	{
	  ftmpi_keyarr[i].in_use = 1;
	  found = 1;
	  ret = i;
	  break;
	}
    }

  if ( !found )
    {
      ftmpi_keyarr_size *= 2;
      nk = ftmpi_key_init ( nk );
      if ( nk == NULL ) return ( MPI_ERR_INTERN );

      for ( i = 0; i < ftmpi_keyarr_size/2; i ++ )
	{
	  ftmpi_key_copy ( &(nk[i]), &(ftmpi_keyarr[i]) );
	  ftmpi_key_mark_free ( i );
	}

      dummy = ftmpi_keyarr;
      ftmpi_keyarr = nk;
      ftmpi_key_free ( dummy );
      ret = (ftmpi_keyarr_size/2) + 1;
    }

  /* Now we have definitly a free element in the array; */
  /* Mark it as used, and allocate the according values-field */

  ftmpi_keyarr[ret].in_use  = 1;
  ftmpi_keyarr[ret].type    = type; 
  ftmpi_keyarr[ret].nvalues = 0;
  ftmpi_keyarr[ret].freed   = 0;
  
  /* Now check whether MPI_Keyval_create (or a derivative function )
     has been called from fortran or from C and set the accorindg
     flag 
  */
  if ( FTMPI_KEYVAL_FORTRAN )
    ftmpi_keyarr[ret].fortran = 1;

  if ( (type == FTMPI_KEY_COMM1 ) || (type == FTMPI_KEY_COMM2))
    {
      ftmpi_keyarr[ret].values = (keyval_info_t *)_MALLOC(MAXCOMS * sizeof(keyval_info_t));
      if ( ftmpi_keyarr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_keyarr[ret].maxvalues = MAXCOMS;
    }
  else if ( type == FTMPI_KEY_WIN )
    {
      /*
      ftmpi_keyarr[ret].values = (keyval_info_t *)_MALLOC(MAXWIN * sizeof(keyval_info_t));
      if ( ftmpi_keyarr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_keyarr[ret].maxvalues = MAXWIN;
      */
      printf("FTMPI doesn't support currently One-sided communication\n");
      return  (MPI_ERR_INTERN);
    }
  else if ( type == FTMPI_KEY_TYPE )
    {
      /* PROBLEM: the datatypes are store in a linked list and NOT
	 in an array . Have to think about it later, for now
	 we are not supporting attribute caching for ddt's */
#ifdef GIVEITATRY
      ftmpi_keyarr[ret].values = (keyval_info_t *)_MALLOC(MAXTYPE * sizeof(keyval_info_t));
      if ( ftmpi_keyarr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_keyarr[ret].maxvalues = MAXTYPE;
#else
      printf("FTMPI doesn't support currently attribute caching for datatypes\n");
      return  (MPI_ERR_INTERN);
#endif
    }

  for ( i = 0; i < ftmpi_keyarr[ret].maxvalues ; i ++ )
    ftmpi_keyarr[ret].values[i].in_use = 0 ; /*false */
  
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_key_mark_free ( int key )
{
  ftmpi_keyarr[key].freed   = 1;
  ftmpi_keyarr[key].nvalues = 0;

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_key_test_freed ( int key )
{
  return ( ftmpi_keyarr[key].freed );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* release an entry in the list and free its according values-array */
int ftmpi_key_release ( int key )
{
  if ( (ftmpi_keyarr[key].in_use) )
    ftmpi_keyarr[key].freed = 1;

  if ( ftmpi_keyarr[key].nvalues == 0 )
    {
      if ( ftmpi_keyarr[key].values != NULL )
	_FREE( ftmpi_keyarr[key].values );
      ftmpi_keyarr[key].in_use    = 0;
      ftmpi_keyarr[key].nvalues   = 0;
      ftmpi_keyarr[key].maxvalues = 0;
      ftmpi_keyarr[key].nvalues   = 0;
      ftmpi_keyarr[key].type      = FTMPI_KEY_UNKNOWN;
      ftmpi_keyarr[key].fortran   = 0;

      /* from everything what I know about unions in C, I think
	 this should be enough to reset all pointers */
      ftmpi_keyarr[key].funcs.mpi1.copy_fn  = NULL;
      ftmpi_keyarr[key].funcs.mpi1.fcopy_fn = NULL;
      ftmpi_keyarr[key].funcs.mpi1.del_fn   = NULL;
      ftmpi_keyarr[key].funcs.mpi1.fdel_fn  = NULL;
      
    }

  return ( MPI_KEYVAL_INVALID );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_key_test ( int key, int type )
{
  int ret;

  if ( key >= 0 && key < ftmpi_keyarr_size )
    {
      if ( ftmpi_keyarr[key].in_use && ftmpi_keyarr[key].type == type ) 
	ret = MPI_SUCCESS ;
      else
	ret = MPI_ERR_ARG ;
    }
  else
    ret = MPI_ERR_OTHER;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_key_set ( int key, void *copy_fn,void *del_fn, void *extra_state )
{
  
  ftmpi_keyarr[key].extra_state = extra_state;

  if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM1 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi1.fcopy_fn = (MPI_Fcopy_function *)   copy_fn;
	  ftmpi_keyarr[key].funcs.mpi1.fdel_fn  = (MPI_Fdelete_function *) del_fn;
	}
      else
	{
	  ftmpi_keyarr[key].funcs.mpi1.copy_fn = (MPI_Copy_function *)   copy_fn;
	  ftmpi_keyarr[key].funcs.mpi1.del_fn  = (MPI_Delete_function *) del_fn;
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM2 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2comm.fcopy_fn = (MPI_Fcomm_copy_attr_function *)  copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2comm.fdel_fn  = (MPI_Fcomm_delete_attr_function *)del_fn;
	}
      else
	{
	  ftmpi_keyarr[key].funcs.mpi2comm.copy_fn = (MPI_Comm_copy_attr_function *)  copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2comm.del_fn  = (MPI_Comm_delete_attr_function *)del_fn;
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_WIN )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2win.fcopy_fn = (MPI_Fwin_copy_attr_function *)copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2win.fdel_fn  = (MPI_Fwin_delete_attr_function *)del_fn;
	}
      else
	{
	  ftmpi_keyarr[key].funcs.mpi2win.copy_fn = (MPI_Win_copy_attr_function *)copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2win.del_fn  = (MPI_Win_delete_attr_function *)del_fn;
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_TYPE )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2type.fcopy_fn = (MPI_Ftype_copy_attr_function *) copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2type.fdel_fn  = (MPI_Ftype_delete_attr_function *) del_fn;
	}
      else
	{
	  ftmpi_keyarr[key].funcs.mpi2type.copy_fn = (MPI_Type_copy_attr_function *) copy_fn;
	  ftmpi_keyarr[key].funcs.mpi2type.del_fn  = (MPI_Type_delete_attr_function *) del_fn;
	}
    }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_keyval_set ( int key, int object, void *attr_val )
{
  /* set in the key-array at position  KEY in the values-array at */
  /* position OBJECT the provided value */

  ftmpi_keyarr[key].values[object].attr_val = attr_val;
  ftmpi_keyarr[key].values[object].in_use   = 1; /* true */
  
  return ( ftmpi_keyarr[key].nvalues++ );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_keyval_get ( int key, int object, void *attr_val )
{
  /* return the value set in the key-array at position  KEY in the */
  /* values-array at position OBJECT */
  
  (*(void**)attr_val) = ftmpi_keyarr[key].values[object].attr_val;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_keyval_delete ( int key, int object, int *nvalues )
{
  int ret;

  /* call the user provided function to delete the attribute */
  if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM1 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{ /* Fortran syntax */
	  ftmpi_keyarr[key].funcs.mpi1.fdel_fn ((MPI_Comm *) &object, &key, 
						&(ftmpi_keyarr[key].values[object].attr_val), 
						ftmpi_keyarr[key].extra_state, &ret );
	}
      else
	{ /* C syntax */
	  ret = ftmpi_keyarr[key].funcs.mpi1.del_fn ((MPI_Comm) object, key, 
						     ftmpi_keyarr[key].values[object].attr_val, 
						     ftmpi_keyarr[key].extra_state );
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM2 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2comm.fdel_fn ((MPI_Comm*) &object, &key, 
						    &(ftmpi_keyarr[key].values[object].attr_val), 
						    ftmpi_keyarr[key].extra_state, &ret );
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2comm.del_fn ((MPI_Comm) object, key, 
							 ftmpi_keyarr[key].values[object].attr_val, 
							 ftmpi_keyarr[key].extra_state );
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_WIN )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2win.fdel_fn ((MPI_Win *) &object, &key, 
						   &ftmpi_keyarr[key].values[object].attr_val, 
						   ftmpi_keyarr[key].extra_state, &ret );
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2win.del_fn ((MPI_Win) object, key, 
							ftmpi_keyarr[key].values[object].attr_val, 
							ftmpi_keyarr[key].extra_state );
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_TYPE )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2type.fdel_fn ((MPI_Datatype *) &object, &key, 
						    &(ftmpi_keyarr[key].values[object].attr_val),
						    ftmpi_keyarr[key].extra_state, &ret );
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2type.del_fn ((MPI_Datatype) object, key, 
							 ftmpi_keyarr[key].values[object].attr_val,
							 ftmpi_keyarr[key].extra_state );
	}
    }

  /* remove the value set in the key-array at position  KEY in the */
  /* values-array at position OBJECT */
  
  if ( ret == MPI_SUCCESS ) {
    ftmpi_keyarr[key].values[object].attr_val = NULL;
    ftmpi_keyarr[key].values[object].in_use   = 0; 
    ftmpi_keyarr[key].nvalues--;
    *nvalues = ftmpi_keyarr[key].nvalues;
  }

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* - is key valid ?
   - is the object within a reasonable range ? (this check should avoid
     segmentation violations )
   - is the object marked as in use ? 
*/
int ftmpi_keyval_test ( int key, int object, int type )
{

  int ret;
  
  ret = ftmpi_key_test ( key, type );
  if ( ret == MPI_SUCCESS )
    {
      if ( object < 0 || object > ftmpi_keyarr[key].maxvalues )
	ret = MPI_ERR_ARG;
      else
	{
	  if ( ftmpi_keyarr[key].values[object].in_use )
	    ret = MPI_SUCCESS;
	  else
	    ret = MPI_ERR_ARG;
	}
    }
  else
    ret = MPI_ERR_ARG;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* call the copy function of the key using object 1, depending on the 
   result, set object2 */
int ftmpi_keyval_copy ( int  key, int object1, int object2 )
{
  char *attr_val_out=NULL;
  int flag;
  int ret;

  if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM1 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi1.fcopy_fn ((MPI_Comm *) &object1, &key, 
						 ftmpi_keyarr[key].extra_state,
						 &(ftmpi_keyarr[key].values[object1].attr_val), 
						 (void *) &attr_val_out,
						 &flag, &ret);
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi1.copy_fn ((MPI_Comm) object1, key, 
						      ftmpi_keyarr[key].extra_state,
						      ftmpi_keyarr[key].values[object1].attr_val, 
						      (void *) &attr_val_out,
						      &flag);
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM2 )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2comm.fcopy_fn ((MPI_Comm *) &object1, &key, 
						     ftmpi_keyarr[key].extra_state,
						     &(ftmpi_keyarr[key].values[object1].attr_val), 
						     (void *) &attr_val_out,
						     &flag, &ret);
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2comm.copy_fn ((MPI_Comm) object1, key, 
							  ftmpi_keyarr[key].extra_state,
							  ftmpi_keyarr[key].values[object1].attr_val, 
							  (void *) &attr_val_out,
							  &flag);
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_WIN )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2win.fcopy_fn ((MPI_Win *) &object1, &key, 
						    ftmpi_keyarr[key].extra_state,
						    &(ftmpi_keyarr[key].values[object1].attr_val), 
						    (void *) &attr_val_out,
						    &flag, &ret);
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2win.copy_fn ((MPI_Win) object1, key, 
							 ftmpi_keyarr[key].extra_state,
							 ftmpi_keyarr[key].values[object1].attr_val, 
							 (void *) &attr_val_out,
							 &flag);
	}
    }
  else if ( ftmpi_keyarr[key].type == FTMPI_KEY_TYPE )
    {
      if ( ftmpi_keyarr[key].fortran )
	{
	  ftmpi_keyarr[key].funcs.mpi2type.fcopy_fn ((MPI_Datatype *) &object1, &key, 
						     ftmpi_keyarr[key].extra_state,
						     &(ftmpi_keyarr[key].values[object1].attr_val),
						     (void *) &attr_val_out,
						     &flag, &ret);
	}
      else
	{
	  ret = ftmpi_keyarr[key].funcs.mpi2type.copy_fn ((MPI_Datatype) object1, key, 
							  ftmpi_keyarr[key].extra_state,
							  ftmpi_keyarr[key].values[object1].attr_val, 
							  (void *) &attr_val_out,
							  &flag);
	}
    }

  if ( ( flag == 1 )  && ( ret == MPI_SUCCESS ) )
    {
      /* set the returned value for the new object */
      ftmpi_keyval_set ( key, object2, (void*) attr_val_out );
      if (( ftmpi_keyarr[key].type == FTMPI_KEY_COMM1 ) || 
	  ( ftmpi_keyarr[key].type == FTMPI_KEY_COMM2 ))
	ftmpi_com_set_attr ( (MPI_Comm) object2, key );
#ifdef GIVEITATRY
      else if ( ftmpi_keyarr[key].type == FTMPI_KEY_TYPE )
	ftmpi_type_set_attr ( (MPI_Datatype) object2, key);
#endif
      /* the other cases (FTMPI_KEY_WIN and FTMPI_KEY_TYPE) still have to be written */
    }
 
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_comm_null_copy_fn (MPI_Comm c, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 0;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_comm_dup_fn (MPI_Comm c, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 1;
  (*(void**)aout) = ain;
  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_comm_null_delete_fn (MPI_Comm c, int k, void* attr_val, void *exs)
{
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_type_null_copy_fn (MPI_Datatype t, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 0;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_type_dup_fn (MPI_Datatype t, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 1;
  (*(void**)aout) = ain;
  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_type_delete_fn (MPI_Datatype t, int k, void* attr_val, void *exs)
{
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_win_null_copy_fn (MPI_Win w, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 0;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_win_dup_fn (MPI_Win w, int k, void *exs, void *ain, void *aout, int *f)
{
  *f = 1;
  (*(void**)aout) = ain;
  return ( MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_win_delete_fn (MPI_Win w, int k, void* attr_val, void *exs)
{
  return ( MPI_SUCCESS );
}
