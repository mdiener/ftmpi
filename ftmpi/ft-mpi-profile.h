/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
			Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
** This file contains the redefinitions of 
** the profiling interface. We provide in this file two possibilities:
1. support for weak symbols SOLARIS style pragma weak
2. support for platforms and compilers without weak symbols
*/

#ifndef _FT_MPI_PROFILE_H
#define _FT_MPI_PROFILE_H


#ifdef HAVE_PRAGMA_WEAK

/*
** These are MPI-2 Functions
*/
/*
#    pragma weak MPI_Comm_set_name		       = PMPI_Comm_set_name
#    pragma weak MPI_Comm_get_name		       = PMPI_Comm_get_name
#    pragma weak MPI_Type_set_name		       = PMPI_Type_set_name
#    pragma weak MPI_Type_get_name		       = PMPI_Type_get_name
#    pragma weak MPI_Status_f2c		               = PMPI_Status_f2c
#    pragma weak MPI_Status_c2f		               = PMPI_Status_c2f
#    pragma weak MPI_Request_get_status	               = PMPI_Request_get_status 
#    pragma weak MPI_Finalized			       = PMPI_Finalized
#    pragma weak MPI_Type_create_indexed_block	       = PMPI_Type_create_indexed_block
*/
#ifdef DECLARE_MPIIO
#    pragma weak MPI_Info_create		       = PMPI_Info_create 
#    pragma weak MPI_Info_set			       = PMPI_Info_set 
#    pragma weak MPI_Info_delete		       = PMPI_Info_delete 
#    pragma weak MPI_Info_get			       = PMPI_Info_get 
#    pragma weak MPI_Info_get_valuelen		       = PMPI_Info_get_valuelen 
#    pragma weak MPI_Info_get_nkeys		       = PMPI_Info_get_nkeys 
#    pragma weak MPI_Info_get_nthkey		       = PMPI_Info_get_nthkey 
#    pragma weak MPI_Info_dup			       = PMPI_Info_dup 
#    pragma weak MPI_Info_free			       = PMPI_Info_free 
#    pragma weak MPI_Info_c2f			       = PMPI_Info_c2f 
#endif  /* DECLARE_MPIIO */
/*
#    pragma weak MPI_Alloc_mem			       = PMPI_Alloc_mem 
#    pragma weak MPI_Free_mem			       = PMPI_Free_mem 
#    pragma weak MPI_Win_c2f			       = PMPI_Win_c2f
#    pragma weak MPI_Request_c2f		       = PMPI_Request_c2f
#    pragma weak MPI_Win_create_errhandler	       = PMPI_Win_create_errhandler
#    pragma weak MPI_Win_set_errhandler	               = PMPI_Win_set_errhandler
#    pragma weak MPI_Win_get_errhandler	               = PMPI_Win_get_errhandler
#    pragma weak MPI_Type_create_resized	       = PMPI_Type_create_resized 
#    pragma weak MPI_Pack_external		       = PMPI_Pack_external 
#    pragma weak MPI_Unpack_external		       = PMPI_Unpack_external 
#    pragma weak MPI_Pack_external_size	               = PMPI_Pack_external_size 
#    pragma weak MPI_Comm_spawn		               = PMPI_Comm_spawn 
#    pragma weak MPI_Comm_spawn_multiple	       = PMPI_Comm_spawn_multiple 
#    pragma weak MPI_Comm_get_parent		       = PMPI_Comm_get_parent 
#    pragma weak MPI_Open_port			       = PMPI_Open_port    
#    pragma weak MPI_Close_port		               = PMPI_Close_port   
#    pragma weak MPI_Comm_accept		       = PMPI_Comm_accept  
#    pragma weak MPI_Comm_connect		       = PMPI_Comm_connect 
#    pragma weak MPI_Publish_name		       = PMPI_Publish_name   
#    pragma weak MPI_Unpublish_name		       = PMPI_Unpublish_name 
#    pragma weak MPI_Lookup_name		       = PMPI_Lookup_name    
#    pragma weak MPI_Comm_join			       = PMPI_Comm_join       
#    pragma weak MPI_Comm_disconnect		       = PMPI_Comm_disconnect 
#    pragma weak MPI_Win_create		               = PMPI_Win_create 
#    pragma weak MPI_Win_free			       = PMPI_Win_free 
#    pragma weak MPI_Win_get_group		       = PMPI_Win_get_group 
#    pragma weak MPI_Put			       = PMPI_Put 
#    pragma weak MPI_Get			       = PMPI_Get 
#    pragma weak MPI_Accumulate		               = PMPI_Accumulate 
#    pragma weak MPI_Win_fence			       = PMPI_Win_fence 
#    pragma weak MPI_Win_start			       = PMPI_Win_start 
#    pragma weak MPI_Win_complete		       = PMPI_Win_complete 
#    pragma weak MPI_Win_post			       = PMPI_Win_post 
#    pragma weak MPI_Win_wait			       = PMPI_Win_wait 
#    pragma weak MPI_Win_test			       = PMPI_Win_test 
#    pragma weak MPI_Win_lock			       = PMPI_Win_lock 
#    pragma weak MPI_Win_unlock		               = PMPI_Win_unlock 
#    pragma weak MPI_Grequest_cancel_function	       = PMPI_Grequest_cancel_function 
#    pragma weak MPI_Grequest_query_function	       = PMPI_Grequest_query_function 
#    pragma weak MPI_Grequest_free_function	       = PMPI_Grequest_free_function 
#    pragma weak MPI_Grequest_start		       = PMPI_Grequest_start 
#    pragma weak MPI_Grequest_complete		       = PMPI_Grequest_complete 
#    pragma weak MPI_Status_set_elements	       = PMPI_Status_set_elements  
#    pragma weak MPI_Status_set_cancelled	       = PMPI_Status_set_cancelled 
#    pragma weak MPI_Win_set_name		       = PMPI_Win_set_name 
#    pragma weak MPI_Win_get_name		       = PMPI_Win_get_name 
#    pragma weak MPI_Win_call_errhandler	       = PMPI_Win_call_errhandler  
#    pragma weak MPI_Init_thread		       = PMPI_Init_thread 
#    pragma weak MPI_Query_thread		       = PMPI_Query_thread
#    pragma weak MPI_Is_thread_main		       = PMPI_Is_thread_main
#    pragma weak MPI_Type_create_keyval	               = PMPI_Type_create_keyval
#    pragma weak MPI_Type_free_keyval		       = PMPI_Type_free_keyval
#    pragma weak MPI_Type_set_attr		       = PMPI_Type_set_attr
#    pragma weak MPI_Type_get_attr		       = PMPI_Type_get_attr
#    pragma weak MPI_Type_delete_attr		       = PMPI_Type_delete_attr
#    pragma weak MPI_Win_create_keyval		       = PMPI_Win_create_keyval 
#    pragma weak MPI_Win_free_keyval		       = PMPI_Win_free_keyval
#    pragma weak MPI_Win_set_attr		       = PMPI_Win_set_attr
#    pragma weak MPI_Win_get_attr		       = PMPI_Win_get_attr
#    pragma weak MPI_Win_delete_attr		       = PMPI_Win_delete_attr
#    pragma weak MPI_Type_dup			       = PMPI_Type_dup
*/
#ifdef DECLARE_MPIIO
#    pragma weak MPI_File_c2f			       = PMPI_File_c2f 
#    pragma weak MPI_File_create_errhandler	       = PMPI_File_create_errhandler 
#    pragma weak MPI_File_get_errhandler	       = PMPI_File_get_errhandler    
#    pragma weak MPI_File_set_errhandler	       = PMPI_File_set_errhandler    
#    pragma weak MPI_File_call_errhandler	       = PMPI_File_call_errhandler 
#    pragma weak MPI_File_open			       = PMPI_File_open 
#    pragma weak MPI_File_close		               = PMPI_File_close 
#    pragma weak MPI_File_delete		       = PMPI_File_delete 
#    pragma weak MPI_File_set_size		       = PMPI_File_set_size 
#    pragma weak MPI_File_preallocate		       = PMPI_File_preallocate 
#    pragma weak MPI_File_get_size		       = PMPI_File_get_size 
#    pragma weak MPI_File_get_group		       = PMPI_File_get_group 
#    pragma weak MPI_File_get_amode		       = PMPI_File_get_amode 
#    pragma weak MPI_File_set_info		       = PMPI_File_set_info 
#    pragma weak MPI_File_get_info		       = PMPI_File_get_info 
#    pragma weak MPI_File_set_view		       = PMPI_File_set_view 
#    pragma weak MPI_File_get_view		       = PMPI_File_get_view 
#    pragma weak MPI_File_read_at		       = PMPI_File_read_at 
#    pragma weak MPI_File_read_at_all		       = PMPI_File_read_at_all 
#    pragma weak MPI_File_write_at		       = PMPI_File_write_at 
#    pragma weak MPI_File_write_at_all		       = PMPI_File_write_at_all 
#    pragma weak MPI_File_iread_at		       = PMPI_File_iread_at 
#    pragma weak MPI_File_iwrite_at		       = PMPI_File_iwrite_at 
#    pragma weak MPI_File_read			       = PMPI_File_read 
#    pragma weak MPI_File_read_all		       = PMPI_File_read_all 
#    pragma weak MPI_File_write		               = PMPI_File_write 
#    pragma weak MPI_File_write_all		       = PMPI_File_write_all 
#    pragma weak MPI_File_iread		               = PMPI_File_iread 
#    pragma weak MPI_File_iwrite		       = PMPI_File_iwrite 
#    pragma weak MPI_File_seek			       = PMPI_File_seek 
#    pragma weak MPI_File_get_position		       = PMPI_File_get_position 
#    pragma weak MPI_File_get_byte_offset	       = PMPI_File_get_byte_offset 
#    pragma weak MPI_File_read_shared		       = PMPI_File_read_shared  
#    pragma weak MPI_File_write_shared		       = PMPI_File_write_shared 
#    pragma weak MPI_File_seek_shared		       = PMPI_File_seek_shared  
#    pragma weak MPI_File_get_position_shared	       = PMPI_File_get_position_shared 
#    pragma weak MPI_File_read_ordered		       = PMPI_File_read_ordered  
#    pragma weak MPI_File_write_ordered	               = PMPI_File_write_ordered 
#    pragma weak MPI_File_iread_shared		       = PMPI_File_iread_shared  
#    pragma weak MPI_File_iwrite_shared	               = PMPI_File_iwrite_shared 
#    pragma weak MPI_File_read_ordered_begin	       = PMPI_File_read_ordered_begin  
#    pragma weak MPI_File_write_ordered_begin	       = PMPI_File_write_ordered_begin 
#    pragma weak MPI_File_read_all_begin	       = PMPI_File_read_all_begin      
#    pragma weak MPI_File_write_all_begin	       = PMPI_File_write_all_begin     
#    pragma weak MPI_File_read_at_all_begin	       = PMPI_File_read_at_all_begin   
#    pragma weak MPI_File_write_at_all_begin	       = PMPI_File_write_at_all_begin  
#    pragma weak MPI_File_read_ordered_end	       = PMPI_File_read_ordered_end    
#    pragma weak MPI_File_read_all_end		       = PMPI_File_read_all_end        
#    pragma weak MPI_File_read_at_all_end	       = PMPI_File_read_at_all_end     
#    pragma weak MPI_File_write_ordered_end	       = PMPI_File_write_ordered_end   
#    pragma weak MPI_File_write_all_end	               = PMPI_File_write_all_end       
#    pragma weak MPI_File_write_at_all_end	       = PMPI_File_write_at_all_end    
#    pragma weak MPI_File_get_type_extent	       = PMPI_File_get_type_extent 
#    pragma weak MPI_File_set_atomicity	               = PMPI_File_set_atomicity 
#    pragma weak MPI_File_get_atomicity	               = PMPI_File_get_atomicity 
#    pragma weak MPI_File_sync			       = PMPI_File_sync 
#endif  /* DECLARE_MPIIO */
/*
#    pragma weak MPI_Datarep_extent_function	       = PMPI_Datarep_extent_function 
#    pragma weak MPI_Datarep_conversion_function       = PMPI_Datarep_conversion_function 
#    pragma weak MPI_Register_datarep		       = PMPI_Register_datarep 
#    pragma weak MPI_Type_match_size		       = PMPI_Type_match_size 
#    pragma weak MPI_Type_create_f90_complex	       = PMPI_Type_create_f90_complex 
#    pragma weak MPI_Type_create_f90_real	       = PMPI_Type_create_f90_real    
#    pragma weak MPI_Type_create_f90_integer	       = PMPI_Type_create_f90_integer  
*/
#endif /* HAVE_PRAGMA_WEAK */


/* The redefinition always takes place, if profiling interface is required.
   The question just is, do we set a weak link to the PMPI routines,
   or do we have to write wrapper-routines (ft-mpi-prof-wrappers.c) */


/*
** These are MPI-2 Functions
*/
/*
#    define MPI_Comm_set_name			PMPI_Comm_set_name
#    define MPI_Comm_get_name			PMPI_Comm_get_name
#    define MPI_Type_set_name			PMPI_Type_set_name
#    define MPI_Type_get_name			PMPI_Type_get_name
#    define MPI_Status_f2c			PMPI_Status_f2c
#    define MPI_Status_c2f			PMPI_Status_c2f
#    define MPI_Request_get_status		PMPI_Request_get_status 
#    define MPI_Finalized			PMPI_Finalized
#    define MPI_Type_create_indexed_block	PMPI_Type_create_indexed_block
*/
#ifdef DECLARE_MPIIO
#    define MPI_Info_create			PMPI_Info_create 
#    define MPI_Info_set			PMPI_Info_set 
#    define MPI_Info_delete			PMPI_Info_delete 
#    define MPI_Info_get			PMPI_Info_get 
#    define MPI_Info_get_valuelen		PMPI_Info_get_valuelen 
#    define MPI_Info_get_nkeys			PMPI_Info_get_nkeys 
#    define MPI_Info_get_nthkey			PMPI_Info_get_nthkey 
#    define MPI_Info_dup			PMPI_Info_dup 
#    define MPI_Info_free			PMPI_Info_free 
#    define MPI_Info_c2f			PMPI_Info_c2f 
#endif  /* DECLARE_MPIIO */
/*
#    define MPI_Alloc_mem			PMPI_Alloc_mem 
#    define MPI_Free_mem			PMPI_Free_mem 
#    define MPI_Win_c2f				PMPI_Win_c2f
#    define MPI_Request_c2f			PMPI_Request_c2f
#    define MPI_Win_create_errhandler		PMPI_Win_create_errhandler
#    define MPI_Win_set_errhandler		PMPI_Win_set_errhandler
#    define MPI_Win_get_errhandler		PMPI_Win_get_errhandler
#    define MPI_Type_create_resized		PMPI_Type_create_resized 
#    define MPI_Pack_external			PMPI_Pack_external 
#    define MPI_Unpack_external			PMPI_Unpack_external 
#    define MPI_Pack_external_size		PMPI_Pack_external_size 
#    define MPI_Comm_spawn			PMPI_Comm_spawn 
#    define MPI_Comm_spawn_multiple		PMPI_Comm_spawn_multiple 
#    define MPI_Comm_get_parent			PMPI_Comm_get_parent 
#    define MPI_Open_port			PMPI_Open_port    
#    define MPI_Close_port			PMPI_Close_port   
#    define MPI_Comm_accept			PMPI_Comm_accept  
#    define MPI_Comm_connect			PMPI_Comm_connect 
#    define MPI_Publish_name			PMPI_Publish_name   
#    define MPI_Unpublish_name			PMPI_Unpublish_name 
#    define MPI_Lookup_name			PMPI_Lookup_name    
#    define MPI_Comm_join			PMPI_Comm_join       
#    define MPI_Comm_disconnect			PMPI_Comm_disconnect 
#    define MPI_Win_create			PMPI_Win_create 
#    define MPI_Win_free			PMPI_Win_free 
#    define MPI_Win_get_group			PMPI_Win_get_group 
#    define MPI_Put				PMPI_Put 
#    define MPI_Get				PMPI_Get 
#    define MPI_Accumulate			PMPI_Accumulate 
#    define MPI_Win_fence			PMPI_Win_fence 
#    define MPI_Win_start			PMPI_Win_start 
#    define MPI_Win_complete			PMPI_Win_complete 
#    define MPI_Win_post			PMPI_Win_post 
#    define MPI_Win_wait			PMPI_Win_wait 
#    define MPI_Win_test			PMPI_Win_test 
#    define MPI_Win_lock			PMPI_Win_lock 
#    define MPI_Win_unlock			PMPI_Win_unlock 
#    define MPI_Grequest_cancel_function	PMPI_Grequest_cancel_function 
#    define MPI_Grequest_query_function		PMPI_Grequest_query_function 
#    define MPI_Grequest_free_function		PMPI_Grequest_free_function 
#    define MPI_Grequest_start			PMPI_Grequest_start 
#    define MPI_Grequest_complete		PMPI_Grequest_complete 
#    define MPI_Status_set_elements		PMPI_Status_set_elements  
#    define MPI_Status_set_cancelled		PMPI_Status_set_cancelled 
#    define MPI_Win_set_name			PMPI_Win_set_name 
#    define MPI_Win_get_name			PMPI_Win_get_name 
#    define MPI_Win_call_errhandler		PMPI_Win_call_errhandler  
#    define MPI_Init_thread			PMPI_Init_thread 
#    define MPI_Query_thread			PMPI_Query_thread
#    define MPI_Is_thread_main			PMPI_Is_thread_main
#    define MPI_Type_create_keyval		PMPI_Type_create_keyval
#    define MPI_Type_free_keyval		PMPI_Type_free_keyval
#    define MPI_Type_set_attr			PMPI_Type_set_attr
#    define MPI_Type_get_attr			PMPI_Type_get_attr
#    define MPI_Type_delete_attr		PMPI_Type_delete_attr
#    define MPI_Win_create_keyval		PMPI_Win_create_keyval 
#    define MPI_Win_free_keyval			PMPI_Win_free_keyval
#    define MPI_Win_set_attr			PMPI_Win_set_attr
#    define MPI_Win_get_attr			PMPI_Win_get_attr
#    define MPI_Win_delete_attr			PMPI_Win_delete_attr
#    define MPI_Type_dup			PMPI_Type_dup
*/
#ifdef DECLARE_MPIIO
#    define MPI_File_c2f			PMPI_File_c2f 
#    define MPI_File_create_errhandler		PMPI_File_create_errhandler 
#    define MPI_File_get_errhandler		PMPI_File_get_errhandler    
#    define MPI_File_set_errhandler		PMPI_File_set_errhandler    
#    define MPI_File_call_errhandler		PMPI_File_call_errhandler 
#    define MPI_File_open			PMPI_File_open 
#    define MPI_File_close			PMPI_File_close 
#    define MPI_File_delete			PMPI_File_delete 
#    define MPI_File_set_size			PMPI_File_set_size 
#    define MPI_File_preallocate		PMPI_File_preallocate 
#    define MPI_File_get_size			PMPI_File_get_size 
#    define MPI_File_get_group			PMPI_File_get_group 
#    define MPI_File_get_amode			PMPI_File_get_amode 
#    define MPI_File_set_info			PMPI_File_set_info 
#    define MPI_File_get_info			PMPI_File_get_info 
#    define MPI_File_set_view			PMPI_File_set_view 
#    define MPI_File_get_view			PMPI_File_get_view 
#    define MPI_File_read_at			PMPI_File_read_at 
#    define MPI_File_read_at_all		PMPI_File_read_at_all 
#    define MPI_File_write_at			PMPI_File_write_at 
#    define MPI_File_write_at_all		PMPI_File_write_at_all 
#    define MPI_File_iread_at			PMPI_File_iread_at 
#    define MPI_File_iwrite_at			PMPI_File_iwrite_at 
#    define MPI_File_read			PMPI_File_read 
#    define MPI_File_read_all			PMPI_File_read_all 
#    define MPI_File_write			PMPI_File_write 
#    define MPI_File_write_all			PMPI_File_write_all 
#    define MPI_File_iread			PMPI_File_iread 
#    define MPI_File_iwrite			PMPI_File_iwrite 
#    define MPI_File_seek			PMPI_File_seek 
#    define MPI_File_get_position		PMPI_File_get_position 
#    define MPI_File_get_byte_offset		PMPI_File_get_byte_offset 
#    define MPI_File_read_shared		PMPI_File_read_shared  
#    define MPI_File_write_shared		PMPI_File_write_shared 
#    define MPI_File_seek_shared		PMPI_File_seek_shared  
#    define MPI_File_get_position_shared	PMPI_File_get_position_shared 
#    define MPI_File_read_ordered		PMPI_File_read_ordered  
#    define MPI_File_write_ordered		PMPI_File_write_ordered 
#    define MPI_File_iread_shared		PMPI_File_iread_shared  
#    define MPI_File_iwrite_shared		PMPI_File_iwrite_shared 
#    define MPI_File_read_ordered_begin		PMPI_File_read_ordered_begin  
#    define MPI_File_write_ordered_begin	PMPI_File_write_ordered_begin 
#    define MPI_File_read_all_begin		PMPI_File_read_all_begin      
#    define MPI_File_write_all_begin		PMPI_File_write_all_begin     
#    define MPI_File_read_at_all_begin		PMPI_File_read_at_all_begin   
#    define MPI_File_write_at_all_begin		PMPI_File_write_at_all_begin  
#    define MPI_File_read_ordered_end		PMPI_File_read_ordered_end    
#    define MPI_File_read_all_end		PMPI_File_read_all_end        
#    define MPI_File_read_at_all_end		PMPI_File_read_at_all_end     
#    define MPI_File_write_ordered_end		PMPI_File_write_ordered_end   
#    define MPI_File_write_all_end		PMPI_File_write_all_end       
#    define MPI_File_write_at_all_end		PMPI_File_write_at_all_end    
#    define MPI_File_get_type_extent		PMPI_File_get_type_extent 
#    define MPI_File_set_atomicity		PMPI_File_set_atomicity 
#    define MPI_File_get_atomicity		PMPI_File_get_atomicity 
#    define MPI_File_sync			PMPI_File_sync 
#endif  /* DECLARE_MPIIO */
/*
#    define MPI_Datarep_extent_function		PMPI_Datarep_extent_function 
#    define MPI_Datarep_conversion_function	PMPI_Datarep_conversion_function 
#    define MPI_Register_datarep		PMPI_Register_datarep 
#    define MPI_Type_match_size			PMPI_Type_match_size 
#    define MPI_Type_create_f90_complex		PMPI_Type_create_f90_complex 
#    define MPI_Type_create_f90_real		PMPI_Type_create_f90_real    
#    define MPI_Type_create_f90_integer		PMPI_Type_create_f90_integer  
*/
#endif /* _FT_MPI_PROFILE_H */









