
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS internal topology routines used by collecives


	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_TOPO
#define _FT_MPI_H_TOPO


int top_build_list ();  /* builds proc_ranks list of living processes */
int top_info_list ();   /* reports on my topology structures */
int top_build_ring ();  /* builds ring struct from proc_ranks */
int top_build_tree ();  /* builds tree struct from proc_ranks */

int top_build_bmtree (); /* builds a binomial tree structure */

#endif /*  _FT_MPI_H_TOPO */
