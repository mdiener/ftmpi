
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg    <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include "stdio.h"
#include "stdlib.h"
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-procinfo.h"

#ifdef USE_MALLOC
#include "debug.h"
#endif


#define FTMPI_PACK_TYPE FTMPI_NOE
/* #define FTMPI_PACK_TYPE FTMPI_XDR */

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Pack                 = PMPI_Pack
#    pragma weak  MPI_Pack_size            = PMPI_Pack_size
#    pragma weak  MPI_Unpack               = PMPI_Unpack

#endif

#    define  MPI_Pack                  PMPI_Pack
#    define  MPI_Pack_size             PMPI_Pack_size
#    define  MPI_Unpack                PMPI_Unpack




int MPI_Pack(void *inbuf,int cnt, MPI_Datatype ddt,
	     void *outbuf,int out_size, int *position,
	     MPI_Comm comm)
{
  int size;
  int rc;
#ifndef USE_MALLOC
  int ibuf2;
  int len;
#endif
  char *buf2 = NULL;
  FTMPI_DDT_CODE_INFO code_info;

  memcpy((char *)&code_info,
	 (char *)ftmpi_procinfo_get_ddt_code_info(my_gid),
	 sizeof(FTMPI_DDT_CODE_INFO));

  CHECK_MPIINIT
  CHECK_COM(comm);

  CHECK_DDT(comm,ddt,1);


  if(cnt < 0) RETURNERR (comm, MPI_ERR_COUNT);
  if(out_size < 0) RETURNERR (comm, MPI_ERR_ARG);

  rc = ftmpi_mpi_type_size(ddt,&size);
  if (rc<0) RETURNERR (comm, MPI_ERR_TYPE);

  if(out_size < cnt * size) RETURNERR (comm, MPI_ERR_ARG);
  
  if(code_info.code_type != FTMPI_NOE) 
    code_info.code_type = FTMPI_XDR;
  
  rc = ftmpi_ddt_encode_size_det(ddt,cnt,&size,&code_info);

  /* ########################################### */
  
  if ( rc ) {
#ifdef USE_MALLOC
    buf2 = _MALLOC ( size );
    if ( buf2 == NULL ) {
      fprintf(stderr, "MPI_Pack: could not allocate %d bytes of memory\n", size);
      MPI_Abort ( comm, MPI_ERR_INTERN );
    }
#else
    ibuf2 = get_msg_buf_of_size(size,1,0);
    if(ibuf2 == -1){
      MPI_Abort (comm, MPI_ERR_INTERN);
    return (MPI_ERR_INTERN);
    }
    get_msg_buf_info(ibuf2,&buf2,&len);
#endif
  }

  /* ########################################### */

  rc = ftmpi_ddt_xdr_encode_dt_to_buffer(inbuf,
					 ((char*)(outbuf))+*position,
					 buf2,size,cnt,ddt, 
					 &code_info);
  if(rc < 0) RETURNERR(comm, MPI_ERR_UNKNOWN);


  /* ########################################### */
  if ( buf2  != NULL ) {
#ifdef USE_MALLOC
    _FREE ( buf2 );
#else
    free_msg_buf (ibuf2);
#endif
  }
  *position += size;

  return(MPI_SUCCESS);
}



int MPI_Unpack(void * inbuf,int insize,int * position,void * outbuf,
	       int cnt,MPI_Datatype ddt,MPI_Comm comm)
{
  int size;
  int rc= 0;
#ifndef USE_MALLOC
  int ibuf2,len;
#endif
  char *buf2 = NULL;
  FTMPI_DDT_CODE_INFO code_info;

  memcpy((char *)&code_info,
	 (char *)ftmpi_procinfo_get_ddt_code_info(my_gid),
	 sizeof(FTMPI_DDT_CODE_INFO));


  CHECK_MPIINIT
  CHECK_COM(comm);
  CHECK_DDT(comm,ddt,1);

  if(insize < 0) RETURNERR (comm, MPI_ERR_ARG);
  if(cnt < 0) RETURNERR (comm, MPI_ERR_COUNT);

  if(code_info.code_type != FTMPI_NOE) 
    code_info.code_type = FTMPI_XDR;
  
  rc = ftmpi_ddt_decode_size_det(ddt,cnt,&size,&code_info);

  if ( rc ) {
#ifdef USE_MALLOC
    buf2 = _MALLOC ( size );
    if ( buf2 == NULL ) {
      fprintf(stderr, "MPI_Unpack: could not allocate %d bytes of  memory\n", size);
      MPI_Abort ( comm, MPI_ERR_INTERN );
    }
#else
    ibuf2 = get_msg_buf_of_size(size,1,0);
    if(ibuf2 == -1){
      MPI_Abort (comm, MPI_ERR_INTERN);
      return (MPI_ERR_INTERN);
    }
    get_msg_buf_info(ibuf2,&buf2,&len);
#endif
  }

  rc = ftmpi_ddt_xdr_decode_dt_to_buffer(((char*)inbuf)+*position,
					 outbuf,buf2,size,
					 cnt,ddt,&code_info);
  if(rc < 0) RETURNERR (comm, MPI_ERR_UNKNOWN);
  
  *position += size;
  if ( buf2 != NULL ) {
#ifdef USE_MALLOC
    _FREE ( buf2 );
#else
    free_msg_buf(ibuf2);
#endif
  }

  return(MPI_SUCCESS);
}


 int MPI_Pack_size(int incount,MPI_Datatype datatype, 
		   MPI_Comm comm, int *size)
{
  int ret;
  int code_type = 0;

  CHECK_MPIINIT
  CHECK_COM(comm);
  CHECK_DDT(comm,datatype,1);
  if(incount < 0) RETURNERR (comm, MPI_ERR_COUNT);

  ret = ftmpi_ddt_write_read_size_det(datatype,incount,
				      size,code_type);
  if(ret >= 0)return(MPI_SUCCESS);

  RETURNERR(comm, MPI_ERR_TYPE);
}

