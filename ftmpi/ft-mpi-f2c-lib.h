/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_LIB
#define _FT_MPI_H_F2C_LIB

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak MPI_INIT                = mpi_init_
#    pragma weak PMPI_INIT               = mpi_init_ 
#    pragma weak MPI_INITILIZED          = mpi_initialized_
#    pragma weak PMPI_INITILIZED         = mpi_initialized_ 
#    pragma weak MPI_FINALIZE            = mpi_finalize_
#    pragma weak PMPI_FINALIZE           = mpi_finalize_ 
#    pragma weak MPI_ABORT               = mpi_abort_
#    pragma weak PMPI_ABORT              = mpi_abort_ 
#    pragma weak MPI_GET_PROCESSOR_NAME  = mpi_get_processor_name_
#    pragma weak PMPI_GET_PROCESSOR_NAME = mpi_get_processor_name_
#    pragma weak MPI_WTICK               = mpi_wtick_
#    pragma weak PMPI_WTICK              = mpi_wtick_
#    pragma weak MPI_WTIME               = mpi_wtime_
#    pragma weak PMPI_WTIME              = mpi_wtime_
#    pragma weak MPI_PCONTROL            = mpi_pcontrol_	
#    pragma weak PMPI_PCONTROL           = mpi_pcontrol_

#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak mpi_init__                = mpi_init_
#    pragma weak pmpi_init__               = mpi_init_
#    pragma weak mpi_initialized__         = mpi_initialized_ 
#    pragma weak pmpi_initialized__        = mpi_initialized_
#    pragma weak mpi_finalize__            = mpi_finalize_
#    pragma weak pmpi_finalize__           = mpi_finalize_
#    pragma weak mpi_abort__               = mpi_abort_
#    pragma weak pmpi_abort__              = mpi_abort_
#    pragma weak mpi_get_processor_name__  = mpi_get_processor_name_ 
#    pragma weak pmpi_get_processor_name__ = mpi_get_processor_name_ 
#    pragma weak mpi_wtick__               = mpi_wtick_
#    pragma weak pmpi_wtick__              = mpi_wtick_
#    pragma weak mpi_wtime__               = mpi_wtime_
#    pragma weak pmpi_wtime__              = mpi_wtime_
#    pragma weak mpi_pcontrol__            = mpi_pcontrol_	
#    pragma weak pmpi_pcontrol__           = mpi_pcontrol_

#  elif defined(FORTRANNOUNDERSCORE)

#    pragma weak mpi_init                = mpi_init_
#    pragma weak pmpi_init               = mpi_init_
#    pragma weak mpi_initialized         = mpi_initialized_
#    pragma weak pmpi_initialized        = mpi_initialized_
#    pragma weak mpi_finalize            = mpi_finalize_
#    pragma weak pmpi_finalize           = mpi_finalize_
#    pragma weak mpi_abort               = mpi_abort_
#    pragma weak pmpi_abort              = mpi_abort_
#    pragma weak mpi_get_processor_name  = mpi_get_processor_name_
#    pragma weak pmpi_get_processor_name = mpi_get_processor_name_
#    pragma weak mpi_wtick               = mpi_wtick_
#    pragma weak pmpi_wtick              = mpi_wtick_
#    pragma weak mpi_wtime               = mpi_wtime_
#    pragma weak pmpi_wtime              = mpi_wtime_
#    pragma weak mpi_pcontrol            = mpi_pcontrol_	
#    pragma weak pmpi_pcontrol           = mpi_pcontrol_

#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak pmpi_init_               = mpi_init_
#    pragma weak pmpi_initialized_        = mpi_initialized_
#    pragma weak pmpi_finalize_           = mpi_finalize_
#    pragma weak pmpi_abort_              = mpi_abort_
#    pragma weak pmpi_get_processor_name_ = mpi_get_processor_name_
#    pragma weak pmpi_wtick_              = mpi_wtick_
#    pragma weak pmpi_wtime_              = mpi_wtime_
#    pragma weak pmpi_pcontrol_           = mpi_pcontrol_

#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define mpi_init_               PMPI_INIT 
#      define mpi_initialized_        PMPI_INITILIZED 
#      define mpi_finalize_           PMPI_FINALIZE 
#      define mpi_abort_              PMPI_ABORT 
#      define mpi_get_processor_name_ PMPI_GET_PROCESSOR_NAME 
#      define mpi_wtick_              PMPI_WTICK
#      define mpi_wtime_              PMPI_WTIME
#      define mpi_pcontrol_           PMPI_PCONTROL

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_init_                pmpi_init__ 
#      define  mpi_initialized_         pmpi_initialized__ 
#      define  mpi_finalize_            pmpi_finalize__ 
#      define  mpi_abort_               pmpi_abort__ 
#      define  mpi_get_processor_name_  pmpi_get_processor_name__ 
#      define  mpi_wtick_               pmpi_wtick__
#      define  mpi_wtime_               pmpi_wtime__
#      define  mpi_pcontrol_            pmpi_pcontrol__

#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_init_                pmpi_init 
#      define  mpi_initialized_         pmpi_initialized 
#      define  mpi_finalize_            pmpi_finalize 
#      define  mpi_abort_               pmpi_abort 
#      define  mpi_get_processor_name_  pmpi_get_processor_name 
#      define  mpi_wtick_               pmpi_wtick
#      define  mpi_wtime_               pmpi_wtime
#      define  mpi_pcontrol_            pmpi_pcontrol

#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define  mpi_init_                pmpi_init_ 
#      define  mpi_initialized_         pmpi_initialized_ 
#      define  mpi_finalize_            pmpi_finalize_ 
#      define  mpi_abort_               pmpi_abort_ 
#      define  mpi_get_processor_name_  pmpi_get_processor_name_ 
#      define  mpi_wtick_               pmpi_wtick_
#      define  mpi_wtime_               pmpi_wtime_
#      define  mpi_pcontrol_            pmpi_pcontrol_
#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#      define mpi_init_               MPI_INIT 
#      define mpi_initialized_        MPI_INITILIZED 
#      define mpi_finalize_           MPI_FINALIZE 
#      define mpi_abort_              MPI_ABORT 
#      define mpi_get_processor_name_ MPI_GET_PROCESSOR_NAME 
#      define mpi_wtick_              MPI_WTICK
#      define mpi_wtime_              MPI_WTIME
#      define mpi_pcontrol_           MPI_PCONTROL


#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define  mpi_init_                mpi_init__ 
#      define  mpi_initialized_         mpi_initialized__ 
#      define  mpi_finalize_            mpi_finalize__ 
#      define  mpi_abort_               mpi_abort__ 
#      define  mpi_get_processor_name_  mpi_get_processor_name__ 
#      define  mpi_wtick_               mpi_wtick__
#      define  mpi_wtime_               mpi_wtime__
#      define  mpi_pcontrol_            mpi_pcontrol__

#    elif defined(FORTRANNOUNDERSCORE)

#      define  mpi_init_                mpi_init 
#      define  mpi_initialized_         mpi_initialized 
#      define  mpi_finalize_            mpi_finalize 
#      define  mpi_abort_               mpi_abort 
#      define  mpi_get_processor_name_  mpi_get_processor_name 
#      define  mpi_wtick_               mpi_wtick
#      define  mpi_wtime_               mpi_wtime
#      define  mpi_pcontrol_            pmpi_pcontrol
#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */
#endif
