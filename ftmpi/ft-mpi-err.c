
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@Cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* This file implements several things at onces, at least four classes:
  
 1. the errhandler-functions ( errh_info_t ):
    - initialiaze the dynamically extendeble array of error-handlers
    - free it
    - get the next free element
    - release an element
 2. the errhandler-value function (errhval_info_t):
    - mark which errhandler is attached to which communicator 
      (it doesn't sound according to the MPI standard that we really
       need to take care about it, but with the rules for freeing
       an error handler, I think it is easier, if we keep track of it).
 3. Management of errclasses (errclass_info_t):
    - static array (non extendable), each class can hold currently
      up to FTMPI_ERRCLASS_MAX_CODES error codes.
    - the number the user is dealing with is NOT the position
      of the errorclass in the ftmpi_errclassarr field, therefore
      each function should get first the handle to the errorclass
      and work in the rest of the function with the handle. This
      solution avoids that all elements have to start with 0,
      but we can take any values for the error-codes and classes.
      - store the predefined classes,
    - get next free element
    - ATTENTION: the MPI-2 standard just offers an interface to 
      add errorclasses but NOT to free one again.
 4. Management of errorcodes (errcode_info_t):
    - static array
    - like in the errclass, the value the user is dealing with
      is not the position of the errorcode in the ftmpi_errcodearr
      field. Explanation see above...
    - each error code contains a string, the handle of the errorclass it
      belongs to 
    - a flag indicating whether it was a predefined error code by the
      MPI library or by the user defined (Reason: it is not allowed
      to change the string of a predefined error-code).


  Furthermore, this file contains the implementation of the two predefined
  error-handler, MPI_ERRORS_ARE_FATAL and MPI_ERRORS_RETURN.

EG March 4, 2003
*/
 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-err.h"
#include "debug.h"

errh_info_t *ftmpi_errharr         = NULL;
int         ftmpi_errharr_size     = 5;
int         ftmpi_errh_initialized = 0;

errcode_info_t *ftmpi_errcodearr         = NULL;
int            ftmpi_errcodearr_size     = 25;
int            ftmpi_errcode_initialized = 0;
int            ftmpi_errcode_last        = 0;

errclass_info_t *ftmpi_errclassarr         = NULL;
int             ftmpi_errclassarr_size        = 25;
int             ftmpi_errclass_initialized = 0;
int             ftmpi_errclass_last        = 0;

/* These are the integers holding the information about
   the dead procs in case of a failure */
int ftmpi_errcode_recover=MPI_SUCCESS;
int ftmpi_num_dead_procs=0;

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Allocate and Initialize the key-array; called either from MPI_Init  */
/* or at first usage */
errh_info_t * ftmpi_errh_init ( errh_info_t *e )
{
  int i;


  e = ( errh_info_t *)_MALLOC( ftmpi_errharr_size * sizeof ( errh_info_t ));
  if ( e == NULL ) 
    {
      printf("Error allocating error-handler array \n");
      return ( NULL );
    }

  memset ( (void* ) e, 0, ftmpi_errharr_size * sizeof(errh_info_t));

  for ( i = 0; i < ftmpi_errharr_size; i ++ )
    {
      e[i].err    = i;
      e[i].in_use = 0; /* not really necessary, e.g. memset ? */
    }
  
  if ( !ftmpi_errh_initialized )
    {
      /* Set now the predefined error-handlers ERRORS_RETURN
	 and ERRORS_ARE_FATAL 

	 ATTENTION: this is definitly a difference to the MPI standard
	 since our predefined error handler for communicators is NOT
	 MPI_ERRORS_ARE_FATAL but MPI_ERRORS_RETURN;
      */

      e[0].err    = MPI_ERRORS_RETURN;
      e[0].in_use = 1;
      e[0].freed  = 0;
      e[0].type   = FTMPI_ERR_COMM2;
      e[0].pfErrorHandler = (MPI_Handler_function*)ftmpi_errors_return;
      e[0].maxvalues = MAXCOMS;
      e[0].values    = (errhval_info_t *)_MALLOC( MAXCOMS * 
						  sizeof(errhval_info_t));
      if ( e[0].values == NULL ) printf("Error allocating memory in "
					"ftmpi_errh_init" );
      e[0].nvalues                       = 2;
      e[0].values[MPI_COMM_WORLD].in_use = 1;
      e[0].values[MPI_COMM_SELF].in_use  = 1;
      
      

      e[1].err    = MPI_ERRORS_ARE_FATAL;
      e[1].in_use = 1;
      e[1].freed  = 0;
      e[1].type   = FTMPI_ERR_COMM2;
      e[1].pfErrorHandler = (MPI_Handler_function*)ftmpi_errors_are_fatal;
      e[1].maxvalues = MAXCOMS;
      e[1].values = (errhval_info_t *)_MALLOC( MAXCOMS * 
					       sizeof(errhval_info_t));
      if ( e[1].values == NULL ) printf("Error allocating memory in "
					"ftmpi_errh_init" );
      e[1].nvalues = 0;


      ftmpi_errh_initialized = 1;
    }

  return ( e );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* free the errh-array. Called usually from MPI_Finalize or when
   reallocating the errh-array */
int ftmpi_errh_free ( errh_info_t *e)
{
  int i;

  if ( e != NULL )
    {
      for ( i = 0; i < ftmpi_errharr_size; i ++ )
	{
	  if ( ftmpi_errharr[i].in_use )
	    ftmpi_errh_release ( i );
	}
      
      _FREE( e );
    }


  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Copy an element of the errh-array into another element. Used */
/* when we have to re-allocate the errh-array to increase the size of */
/* the array */
/* copy e2 onto e1  */
int ftmpi_errh_copy ( errh_info_t *e1, errh_info_t *e2 )
{
  e1->err         = e2->err;
  e1->in_use      = e2->in_use;
  e1->freed       = e2->freed;
  e1->type        = e2->type;
  e1->pfErrorHandler = e2->pfErrorHandler;

  e1->maxvalues  = e2->maxvalues;
  e1->nvalues    = e2->nvalues;

  e1->values = (errhval_info_t *)_MALLOC( e1->maxvalues * sizeof(errhval_info_t));
  if ( e1->values == NULL ) return ( MPI_ERR_INTERN );

  memcpy ( e1->values, e2->values, (size_t) (e1->maxvalues * sizeof(errhval_info_t)));

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* return the errh-value for the next free entry, reserve it */
/* and allocate the values-array according to the type setting*/
int ftmpi_errh_get_next_free ( int type )
{
  int i, found = 0;
  int ret=MPI_SUCCESS;
  errh_info_t *ne=NULL;
  errh_info_t *dummy;

  if ( !ftmpi_errh_initialized )
    {
      ftmpi_errharr = ftmpi_errh_init ( ftmpi_errharr );
      if ( ftmpi_errharr == NULL ) return ( MPI_ERR_INTERN );
    }
  
  /* Search next free element; if no free element found, increase*/
  /* the size of the array */
  for ( i = 0; i < ftmpi_errharr_size; i ++ )
    {
      if ( !ftmpi_errharr[i].in_use )
	{
	  ftmpi_errharr[i].in_use = 1;
	  found = 1;
	  ret = i;
	  break;
	}
    }

  if ( !found )
    {
      ftmpi_errharr_size *= 2;
      ne = ftmpi_errh_init ( ne );
      if ( ne == NULL ) return ( MPI_ERR_INTERN );

      for ( i = 0; i < ftmpi_errharr_size/2; i ++ )
	{
	  ftmpi_errh_copy ( &(ne[i]), &(ftmpi_errharr[i]) );
	  ftmpi_errh_mark_free ( i );
	}

      dummy = ftmpi_errharr;
      ftmpi_errharr = ne;
      ftmpi_errh_free ( dummy );
      ret = (ftmpi_errharr_size/2) + 1;
    }

  /* Now we have definitly a free element in the array; */
  /* Mark it as used, and allocate the according values-field */

  ftmpi_errharr[ret].in_use  = 1;
  ftmpi_errharr[ret].type    = type; 
  ftmpi_errharr[ret].nvalues = 0;
  ftmpi_errharr[ret].freed   = 0;
  
  /* Now check whether MPI_Errhandler_create (or a derivative function )
     has been called from fortran or from C and set the accorindg
     flag 
  */
  if ( (type == FTMPI_ERR_COMM1 ) || (type == FTMPI_ERR_COMM2))
    {
      ftmpi_errharr[ret].values = (errhval_info_t *)_MALLOC(MAXCOMS * sizeof(errhval_info_t));
      if ( ftmpi_errharr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_errharr[ret].maxvalues = MAXCOMS;
    }
  else if ( type == FTMPI_ERR_WIN )
    {
      /*
      ftmpi_errharr[ret].values = (errhval_info_t *)_MALLOC(MAXWIN * sizeof(errhval_info_t));
      if ( ftmpi_errharr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_errharr[ret].maxvalues = MAXWIN;
      */
      printf("FT-MPI doesn't support currently one-sided communication\n");
      return  (MPI_ERR_INTERN);
    }
  else if ( type == FTMPI_ERR_FILE )
    {
      /*
      ftmpi_errharr[ret].values = (errhval_info_t *)_MALLOC(MAXFILE * sizeof(errhval_info_t));
      if ( ftmpi_errharr[ret].values == NULL ) return (MPI_ERR_INTERN);
      ftmpi_errharr[ret].maxvalues = MAXFILE;
      */
      printf("FT-MPI doesn't support currently errhor handlers for files\n");
      return  (MPI_ERR_INTERN);
    }

  for ( i = 0; i < ftmpi_errharr[ret].maxvalues ; i ++ )
    ftmpi_errharr[ret].values[i].in_use = 0 ; /*false */
  
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errh_mark_free ( int handle )
{
  ftmpi_errharr[handle].freed   = 1;
  ftmpi_errharr[handle].nvalues = 0;

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errh_test_freed ( int handle )
{
  return ( ftmpi_errharr[handle].freed );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* release an entry in the list and free its according values-array */
int ftmpi_errh_release ( int handle )
{
  if ( (ftmpi_errharr[handle].in_use) )
    ftmpi_errharr[handle].freed = 1;

  if ( ftmpi_errharr[handle].nvalues == 0 )
    {
      if ( ftmpi_errharr[handle].values != NULL )
	_FREE( ftmpi_errharr[handle].values );
      ftmpi_errharr[handle].in_use    = 0;
      ftmpi_errharr[handle].maxvalues = 0;
      ftmpi_errharr[handle].nvalues   = 0;
      ftmpi_errharr[handle].type      = FTMPI_ERR_UNKNOWN;
      ftmpi_errharr[handle].pfErrorHandler  = NULL;
      
    }
  return ( MPI_ERRHANDLER_NULL );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errh_set ( int handle, void *handler_fn )
{
  ftmpi_errharr[handle].pfErrorHandler = (MPI_Handler_function*)handler_fn;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void* ftmpi_errh_get_function ( int handle )
{
  return (void*)ftmpi_errharr[handle].pfErrorHandler;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errh_get_handle ( int errh )
{
  int i;
  int handle=MPI_ERR_ARG;

  for ( i = 0; i < ftmpi_errharr_size; i ++ )
    {
      if ( ftmpi_errharr[i].err == errh )
	handle = i;
    }
  return ( handle );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errh_get_err ( int handle )
{
  return ( ftmpi_errharr[handle].err );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errhval_mark ( int handle, int object)
{
  ftmpi_errharr[handle].values[object].in_use   = 1; /* true */
  return ( ftmpi_errharr[handle].nvalues++ );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errhval_unmark ( int handle, int object )
{
  /* remove the value set in the key-array at position  KEY in the */
  /* values-array at position OBJECT */
  
  ftmpi_errharr[handle].values[object].in_use   = 0; 
  ftmpi_errharr[handle].nvalues--;

  return ( ftmpi_errharr[handle].nvalues );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* - is errorhandler valid ?
   - is the given object attached to the errhandler ? 
*/
int ftmpi_errhval_test ( int errh, int object, int type )
{

  int ret;
  int handle;
  
  handle = ftmpi_errh_get_handle ( errh );
  if ( handle >= 0 )
    {
      if ( object < 0 || object > ftmpi_errharr[handle].maxvalues )
	ret = MPI_ERR_ARG;
      else
	{
	  if ( ftmpi_errharr[handle].values[object].in_use )
	    ret = MPI_SUCCESS;
	  else
	    ret = MPI_ERR_ARG;
	}
    }
  else
    ret = MPI_ERR_ARG;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errhval_copy ( int  handle, int object1, int object2 )
{

  /* set the returned value for the new object */
  ftmpi_errhval_mark ( handle, object2);
  if (( ftmpi_errharr[handle].type == FTMPI_ERR_COMM1 ) || 
      ( ftmpi_errharr[handle].type == FTMPI_ERR_COMM2 ))
	ftmpi_com_set_errhandler ( (MPI_Comm) object2, handle );
      /* the other cases (FTMPI_ERR_WIN and FTMPI_ERR_FILE) still have to be written */

 
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errcode_and_errclass_init ( void)
{


  /* First; static initialization of the errcode_array. 
     This is NOT an extendable array like the errh_info_t
     array 
  */

  ftmpi_errcodearr = ( errcode_info_t *)_MALLOC( ftmpi_errcodearr_size * sizeof ( errcode_info_t ));
  if ( ftmpi_errcodearr == NULL ) printf("Error allocating memory\n");

  memset ( (void* ) ftmpi_errcodearr, 0, ftmpi_errcodearr_size * sizeof(errcode_info_t));

  /* Set all predefined errorcodes */
  ftmpi_errcodes_init (); 

  /* Second: static initialization of the errclass_array; */
  ftmpi_errclassarr = ( errclass_info_t *)_MALLOC( ftmpi_errclassarr_size * sizeof ( errclass_info_t ));
  if ( ftmpi_errclassarr == NULL )  printf("Error allocating memory\n");

  memset ( (void* ) ftmpi_errclassarr, 0, ftmpi_errclassarr_size * sizeof(errclass_info_t));

  /* Set all predefined error-classes */
  ftmpi_errclass_init (); 

  if ( !ftmpi_errh_initialized )
    {
      ftmpi_errharr = ftmpi_errh_init ( ftmpi_errharr );
      if ( ftmpi_errharr == NULL ) 
	printf("Error initializing the error-handler array\n");

      ftmpi_com_set_errhandler ( MPI_COMM_WORLD, 0 );
      ftmpi_com_set_errhandler ( MPI_COMM_SELF, 0 );

    }
  
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errcodes_init (void)
{
  /* set all errorcodes defined in mpi.h 
     mark them as non-changeable*/


  /* 0      0    MPI_SUCCESS   "No error" */
  ftmpi_errcodearr[0].val = 0;
  ftmpi_errcodearr[0].errstrlen = (int ) (strlen ( "No error")+1);
  memset ( ftmpi_errcodearr[0].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[0].errstring, "No error",
	    ftmpi_errcodearr[0].errstrlen );
  ftmpi_errcodearr[0].errclass  = 0;
  ftmpi_errcodearr[0].predefined = 1;

  /* 1   -10  MPI_ERR_ARG "Invalid argument of some other kind" */
  ftmpi_errcodearr[1].val = -10;
  ftmpi_errcodearr[1].errstrlen = (int ) (strlen("Invalid argument of some other kind")+1);
  memset ( ftmpi_errcodearr[1].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[1].errstring,"Invalid argument of some other kind" ,
	    ftmpi_errcodearr[1].errstrlen);
  ftmpi_errcodearr[1].errclass  = 1;
  ftmpi_errcodearr[1].predefined = 1;

  /*  2      -11  MPI_ERR_BUFFER "Invalid buffer pointer" */
  ftmpi_errcodearr[2].val = -11;
  ftmpi_errcodearr[2].errstrlen = (int ) (strlen ("Invalid buffer pointer")+1);
  memset ( ftmpi_errcodearr[2].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[2].errstring, "Invalid buffer pointer" , 
	    ftmpi_errcodearr[2].errstrlen);
  ftmpi_errcodearr[2].errclass  = 2;
  ftmpi_errcodearr[2].predefined = 1;

  /* 3      -12  MPI_ERR_COMM   "Invalid communicator" */
  ftmpi_errcodearr[3].val = -12;
  ftmpi_errcodearr[3].errstrlen = (int ) (strlen ("Invalid communicator")+1);
  memset ( ftmpi_errcodearr[3].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[3].errstring,"Invalid communicator", 
	    ftmpi_errcodearr[3].errstrlen);
  ftmpi_errcodearr[3].errclass  = 3;
  ftmpi_errcodearr[3].predefined = 1;

  /* 4      -13  MPI_ERR_COUNT  "Invalid count argument" */
  ftmpi_errcodearr[4].val = -13;
  ftmpi_errcodearr[4].errstrlen = (int ) (strlen ("Invalid count argument")+1);
  memset ( ftmpi_errcodearr[4].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[4].errstring,"Invalid count argument", 
	    ftmpi_errcodearr[4].errstrlen);
  ftmpi_errcodearr[4].errclass  = 4;
  ftmpi_errcodearr[4].predefined = 1;

  /*     5      -14  MPI_ERR_DIMS     "Invalid dimension argument" */
  ftmpi_errcodearr[5].val = -14;
  ftmpi_errcodearr[5].errstrlen = (int) (strlen("Invalid dimension argument")+1);
  memset ( ftmpi_errcodearr[5].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[5].errstring,"Invalid dimension argument", 
	    ftmpi_errcodearr[5].errstrlen);
  ftmpi_errcodearr[5].errclass  = 5;
  ftmpi_errcodearr[5].predefined = 1;

  /*     6      -15  MPI_ERR_GROUP   "Invalid group" */
  ftmpi_errcodearr[6].val = -15;
  ftmpi_errcodearr[6].errstrlen = (int ) (strlen ("Invalid group")+1);
  memset ( ftmpi_errcodearr[6].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[6].errstring,"Invalid group", 
	    ftmpi_errcodearr[6].errstrlen );
  ftmpi_errcodearr[6].errclass  = 6;
  ftmpi_errcodearr[6].predefined = 1;

  /*     7      -16  MPI_ERR_IN_STATUS "Error code in status" */
  ftmpi_errcodearr[7].val = -16;
  ftmpi_errcodearr[7].errstrlen = (int ) (strlen ("Error code in status")+1);
  memset ( ftmpi_errcodearr[7].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[7].errstring,"Error code in status", 
	    ftmpi_errcodearr[7].errstrlen );
  ftmpi_errcodearr[7].errclass  = 7;
  ftmpi_errcodearr[7].predefined = 1;

  /*     8      -17  MPI_ERR_INTERN   "Internal MPI implementation error" */
  ftmpi_errcodearr[8].val = -17;
  ftmpi_errcodearr[8].errstrlen = (int ) (strlen ("Internal MPI implementation error")+1);
  memset ( ftmpi_errcodearr[8].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[8].errstring,"Internal MPI implementation error", 
	    ftmpi_errcodearr[8].errstrlen);
  ftmpi_errcodearr[8].errclass  = 8;
  ftmpi_errcodearr[8].predefined = 1;

  /*     9      -18  MPI_ERR_OP      "Invalid operation" */
  ftmpi_errcodearr[9].val = -18;
  ftmpi_errcodearr[9].errstrlen = (int ) (strlen ("Invalid operation")+1);
  memset ( ftmpi_errcodearr[9].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[9].errstring,"Invalid operation", 
	    ftmpi_errcodearr[9].errstrlen);
  ftmpi_errcodearr[9].errclass  = 9;
  ftmpi_errcodearr[9].predefined = 1;

  /*     10     -19  MPI_ERR_OTHER    "Known error not in list" */
  ftmpi_errcodearr[10].val = -19;
  ftmpi_errcodearr[10].errstrlen = (int ) (strlen ("Known error not in list")+1);
  memset ( ftmpi_errcodearr[10].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[10].errstring,"Known error not in list", 
	    ftmpi_errcodearr[10].errstrlen);
  ftmpi_errcodearr[10].errclass  = 10;
  ftmpi_errcodearr[10].predefined = 1;

  /*     11     -20  MPI_ERR_PENDING   "Pending request" */
  ftmpi_errcodearr[11].val = -20;
  ftmpi_errcodearr[11].errstrlen = (int ) (strlen ("Pending request")+1);
  memset ( ftmpi_errcodearr[11].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[11].errstring,"Pending request", 
	    ftmpi_errcodearr[11].errstrlen);
  ftmpi_errcodearr[11].errclass  = 11;
  ftmpi_errcodearr[11].predefined = 1;

  /*     12     -21  MPI_ERR_RANK   "Invalid rank" */
  ftmpi_errcodearr[12].val = -21;
  ftmpi_errcodearr[12].errstrlen = (int ) (strlen ("Invalid rank")+1);
  memset ( ftmpi_errcodearr[12].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[12].errstring,"Invalid rank", 
	    ftmpi_errcodearr[12].errstrlen);
  ftmpi_errcodearr[12].errclass  = 12;
  ftmpi_errcodearr[12].predefined = 1;

  /*     13     -22  MPI_ERR_REQUEST "Invalid request (handle)" */
  ftmpi_errcodearr[13].val = -22;
  ftmpi_errcodearr[13].errstrlen = (int ) (strlen ("Invalid request (handle)")+1);
  memset ( ftmpi_errcodearr[13].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[13].errstring,"Invalid request (handle)", 
	    ftmpi_errcodearr[13].errstrlen);
  ftmpi_errcodearr[13].errclass  = 13;
  ftmpi_errcodearr[13].predefined = 1;

  /*     14     -23  MPI_ERR_ROOT    "Invalid root" */
  ftmpi_errcodearr[14].val = -23;
  ftmpi_errcodearr[14].errstrlen = (int ) (strlen ("Invalid root")+1);
  memset ( ftmpi_errcodearr[14].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[14].errstring,"Invalid root", 
	    ftmpi_errcodearr[14].errstrlen);
  ftmpi_errcodearr[14].errclass  = 14;
  ftmpi_errcodearr[14].predefined = 1;

  /*     15     -24  MPI_ERR_TAG    "Invalid tag argument" */
  ftmpi_errcodearr[15].val = -24;
  ftmpi_errcodearr[15].errstrlen = (int ) (strlen ("Invalid tag argument")+1);
  memset ( ftmpi_errcodearr[15].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[15].errstring,"Invalid tag argument", 
	    ftmpi_errcodearr[15].errstrlen);
  ftmpi_errcodearr[15].errclass  = 15;
  ftmpi_errcodearr[15].predefined = 1;

  /*     16     -25  MPI_ERR_TOPOLOGY "Invalid topology" */
  ftmpi_errcodearr[16].val = -25;
  ftmpi_errcodearr[16].errstrlen = (int ) (strlen ("Invalid topology")+1);
  memset ( ftmpi_errcodearr[16].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[16].errstring,"Invalid topology", 
	    ftmpi_errcodearr[16].errstrlen);
  ftmpi_errcodearr[16].errclass  = 16;
  ftmpi_errcodearr[16].predefined = 1;

  /*     17     -26  MPI_ERR_TUNCATE  "Message truncated on receive" */
  ftmpi_errcodearr[17].val = -26;
  ftmpi_errcodearr[17].errstrlen = (int ) (strlen ("Message truncated on receive")+1);
  memset ( ftmpi_errcodearr[17].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[17].errstring,"Message truncated on receive", 
	    ftmpi_errcodearr[17].errstrlen);
  ftmpi_errcodearr[17].errclass  = 17;
  ftmpi_errcodearr[17].predefined = 1;

  /*     18     -27  MPI_ERR_TYPE   "Invalid datatype argument" */
  ftmpi_errcodearr[18].val = -27;
  ftmpi_errcodearr[18].errstrlen = (int) (strlen("Invalid datatype argument")+1);
  memset ( ftmpi_errcodearr[18].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[18].errstring,"Invalid datatype argument", 
	    ftmpi_errcodearr[18].errstrlen);
  ftmpi_errcodearr[18].errclass  = 18;
  ftmpi_errcodearr[18].predefined = 1;

  /*     19     -28  MPI_ERR_UNKNOWN  "Unknown error" */
  ftmpi_errcodearr[19].val = -28;
  ftmpi_errcodearr[19].errstrlen = (int ) (strlen ("Unknown error")+1);
  memset ( ftmpi_errcodearr[19].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[19].errstring,"Unknown error", 
	    ftmpi_errcodearr[19].errstrlen);
  ftmpi_errcodearr[19].errclass  = 19;
  ftmpi_errcodearr[19].predefined = 1;

  /*     20     -29  MPI_ERR_KEYVAL  "Invalid key-value" */
  ftmpi_errcodearr[20].val = -29;
  ftmpi_errcodearr[20].errstrlen = (int ) (strlen ("Invalid key value")+1);
  memset ( ftmpi_errcodearr[20].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[20].errstring,"Invalid key value", 
	    ftmpi_errcodearr[20].errstrlen);
  ftmpi_errcodearr[20].errclass  = 20;
  ftmpi_errcodearr[20].predefined = 1;

  /*     21     -30  MPI_ERR_LASTCODE  "Last error code" */
  ftmpi_errcodearr[21].val = -30;
  ftmpi_errcodearr[21].errstrlen = (int ) (strlen ("Last error code")+1);
  memset ( ftmpi_errcodearr[21].errstring, 0, MPI_MAX_ERROR_STRING);
  strncpy ( ftmpi_errcodearr[21].errstring,"Last error code", 
	    ftmpi_errcodearr[21].errstrlen);
  ftmpi_errcodearr[21].errclass  = 30;
  ftmpi_errcodearr[21].predefined = 1;
  

  ftmpi_errcode_last = 22;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_get_class (int ecode)
{
  return ( ftmpi_errcodearr[ecode].errclass );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_test ( int ecode )
{
  int i;
  int ret = MPI_ERR_ARG;
  
  for ( i = 0; i < ftmpi_errcode_last; i ++ )
    {
      if ( ftmpi_errcodearr[i].val == ecode )
	ret = MPI_SUCCESS;
    }

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_get_string (int ecode, char *str, int *len )
{

  *len = ftmpi_errcodearr[ecode].errstrlen - 1;
  strncpy ( str, ftmpi_errcodearr[ecode].errstring, ( *len + 1));
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_set_string ( int ecode, char *str )
{
  int ret=MPI_SUCCESS;

  if ( ftmpi_errcodearr[ecode].predefined )
    ret = MPI_ERR_ARG; /* user is not allowed to set a string for a predefined errorcode */
  else
    {
      strncpy (ftmpi_errcodearr[ecode].errstring, str, MPI_MAX_ERROR_STRING);
      ftmpi_errcodearr[ecode].errstrlen = strlen ( str );
    }

  return ( ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_get_next_free ( int errclass, int *errcode )
{
  int ret;
  ret = ftmpi_errcode_last;
  if ( ret >= ftmpi_errcodearr_size )
    {
      /* since we are using a static array, there is currently
	 no way to overcome this problem */
      return ( MPI_ERR_INTERN );
    }
  
  /* Attention: determining the next val is depending on whether
     we have negative or positive values. For the current version,
     we are dealing with negative values, therefore we have to
     decrease val to achieve the next unused  value. Later on 
     we might change to positive values (since it is required 
     by the MPI standard), than we have to change the next line 
     EG. March 3 2003 */
  ftmpi_errcodearr[ret].val        = ftmpi_errcodearr[ret-1].val - 1 ;
  ftmpi_errcodearr[ret].errstrlen  = 0;
  ftmpi_errcodearr[ret].errclass   = errclass;
  ftmpi_errcodearr[ret].predefined = 0;
  memset ( (void *) ftmpi_errcodearr[ret].errstring, 0, MPI_MAX_ERROR_STRING );

  *errcode = ftmpi_errcodearr[ret].val;

  ftmpi_errcode_last++;
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errcode_get_handle ( int errcode )
{
  int i;
  int ret=MPI_ERR_ARG;

  for (i = 0; i < ftmpi_errcodearr_size; i ++ )
    {
      if ( ftmpi_errcodearr[i].val == errcode )
	{
	  ret = i;
	  break;
	}
    }
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errcode_set_predefined( int ecode )
{
  ftmpi_errcodearr[ecode].predefined = 1;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errcode_unset_predefined( int ecode )
{
  ftmpi_errcodearr[ecode].predefined = 0;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errclass_init (void)
{
  /* set all error-classes defined in the MPI-standard */

  /* 0      0    MPI_SUCCESS   "No error" */
  ftmpi_errclassarr[0].val = 0;
  ftmpi_errclassarr[0].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[0].nerrcode = 1;
  ftmpi_errclassarr[0].errcodes[0] = 0;

  /* 1   -10  MPI_ERR_ARG "Invalid argument of some other kind" */
  ftmpi_errclassarr[1].val = -10;
  ftmpi_errclassarr[1].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[1].nerrcode = 1;
  ftmpi_errclassarr[1].errcodes[0] =  1;

  /*  2      -11  MPI_ERR_BUFFER "Invalid buffer pointer" */
  ftmpi_errclassarr[2].val = -11;
  ftmpi_errclassarr[2].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[2].nerrcode = 1;
  ftmpi_errclassarr[2].errcodes[0] =  2;


  /* 3      -12  MPI_ERR_COMM   "Invalid communicator" */
  ftmpi_errclassarr[3].val = -12;
  ftmpi_errclassarr[3].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[3].nerrcode = 1;
  ftmpi_errclassarr[3].errcodes[0] = 3;

  /* 4      -13  MPI_ERR_COUNT  "Invalid count argument" */
  ftmpi_errclassarr[4].val = -13;
  ftmpi_errclassarr[4].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[4].nerrcode = 1;
  ftmpi_errclassarr[4].errcodes[0] = 4;

  /*     5      -14  MPI_ERR_DIMS     "Invalid dimension argument" */
  ftmpi_errclassarr[5].val = -14;
  ftmpi_errclassarr[5].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[5].nerrcode = 1;
  ftmpi_errclassarr[5].errcodes[0] = 5;

  /*     6      -15  MPI_ERR_GROUP   "Invalid group" */
  ftmpi_errclassarr[6].val = -15;
  ftmpi_errclassarr[6].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[6].nerrcode = 1;
  ftmpi_errclassarr[6].errcodes[0] = 6;

  /*     7      -16  MPI_ERR_IN_STATUS "Error code in status" */
  ftmpi_errclassarr[7].val = -16;
  ftmpi_errclassarr[7].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[7].nerrcode = 1;
  ftmpi_errclassarr[7].errcodes[0] = 7;

  /*     8      -17  MPI_ERR_INTERN   "Internal MPI implementation error" */
  ftmpi_errclassarr[8].val = -17;
  ftmpi_errclassarr[8].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[8].nerrcode = 1;
  ftmpi_errclassarr[8].errcodes[0] = 8;

  /*     9      -18  MPI_ERR_OP      "Invalid operation" */
  ftmpi_errclassarr[9].val = -18;
  ftmpi_errclassarr[9].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[9].nerrcode = 1;
  ftmpi_errclassarr[9].errcodes[0] = 9;

  /*     10     -19  MPI_ERR_OTHER    "Known error not in list" */
  ftmpi_errclassarr[10].val = -19;
  ftmpi_errclassarr[10].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[10].nerrcode = 1;
  ftmpi_errclassarr[10].errcodes[0] = 10;

  /*     11     -20  MPI_ERR_PENDING   "Pending request" */
  ftmpi_errclassarr[11].val = -20;
  ftmpi_errclassarr[11].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[11].nerrcode = 1;
  ftmpi_errclassarr[11].errcodes[0] = 11;

  /*     12     -21  MPI_ERR_RANK   "Invalid rank" */
  ftmpi_errclassarr[12].val = -21;
  ftmpi_errclassarr[12].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[12].nerrcode = 1;
  ftmpi_errclassarr[12].errcodes[0] = 12;

  /*     13     -22  MPI_ERR_REQUEST "Invalid request (handle)" */
  ftmpi_errclassarr[13].val = -22;
  ftmpi_errclassarr[13].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[13].nerrcode = 1;
  ftmpi_errclassarr[13].errcodes[0] = 13;

  /*     14     -23  MPI_ERR_ROOT    "Invalid root" */
  ftmpi_errclassarr[14].val = -23;
  ftmpi_errclassarr[14].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[14].nerrcode = 1;
  ftmpi_errclassarr[14].errcodes[0] = 14;

  /*     15     -24  MPI_ERR_TAG    "Invalid tag argument" */
  ftmpi_errclassarr[15].val = -24;
  ftmpi_errclassarr[15].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[15].nerrcode = 1;
  ftmpi_errclassarr[15].errcodes[0] = 15;

  /*     16     -25  MPI_ERR_TOPOLOGY "Invalid topology" */
  ftmpi_errclassarr[16].val = -25;
  ftmpi_errclassarr[16].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[16].nerrcode = 1;
  ftmpi_errclassarr[16].errcodes[0] = 16;

  /*     17     -26  MPI_ERR_TUNCATE  "Message truncated on receive" */
  ftmpi_errclassarr[17].val = -26;
  ftmpi_errclassarr[17].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[17].nerrcode = 1;
  ftmpi_errclassarr[17].errcodes[0] = 17;

  /*     18     -27  MPI_ERR_TYPE   "Invalid datatype argument" */
  ftmpi_errclassarr[18].val = -27;
  ftmpi_errclassarr[18].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[18].nerrcode = 1;
  ftmpi_errclassarr[18].errcodes[0] = 18;

  /*     19     -28  MPI_ERR_UNKNOWN  "Unknown error" */
  ftmpi_errclassarr[19].val = -28;
  ftmpi_errclassarr[19].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[19].nerrcode = 1;
  ftmpi_errclassarr[19].errcodes[0] = 19;

  /*     20     -29  MPI_ERR_KEYVAL  "Invalid key value" */
  ftmpi_errclassarr[20].val = -29;
  ftmpi_errclassarr[20].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[20].nerrcode = 1;
  ftmpi_errclassarr[20].errcodes[0] = 20;

  /*     21     -30  MPI_ERR_LASTCODE  "Last error code" */
  ftmpi_errclassarr[21].val = -30;
  ftmpi_errclassarr[21].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[21].nerrcode = 1;
  ftmpi_errclassarr[21].errcodes[0] = 21;

  ftmpi_errclass_last = 22;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errclass_get_value ( int handle )
{
  return ( ftmpi_errclassarr[handle].val );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errclass_get_handle ( int errclass )
{
  int i;
  int ret=MPI_ERR_ARG;

  for (i = 0; i < ftmpi_errclassarr_size; i ++ )
    {
      if ( ftmpi_errclassarr[i].val == errclass )
	{
	  ret = i;
	  break;
	}
    }
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errclass_get_next_free ( int *class )
{
  int ret;
  int i;

  ret = ftmpi_errclass_last;
  if ( ret >= ftmpi_errclassarr_size )
    {
      /* since we are using a static array, there is currently
	 no way to overcome this problem */
      return ( MPI_ERR_INTERN );
    }

  /* Attention: determining the next val is depending on whether
     we have negative or positive values. For the current version,
     we are dealing with negative values, therefore we have to
     decrease val to achieve the next unused  value. Later on 
     we might change to positive values (since it is required 
     by the MPI standard), than we have to change the next line */
  ftmpi_errclassarr[ret].val        = ftmpi_errclassarr[ret-1].val - 1 ;
  ftmpi_errclassarr[ret].maxerrcode = FTMPI_ERRCLASS_MAX_CODES;
  ftmpi_errclassarr[ret].nerrcode   = 0;

  for ( i = 0; i < FTMPI_ERRCLASS_MAX_CODES; i ++ )
    ftmpi_errclassarr[i].errcodes[i] = -1;

  *class = ftmpi_errclassarr[ret].val;
  ftmpi_errclass_last++;
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_errclass_add_errcode ( int class, int code )
{
  int f;
  int ret = MPI_SUCCESS;
  
  f = ftmpi_errclassarr[class].nerrcode + 1;
  if ( f >= ftmpi_errclassarr[class].maxerrcode )
    {
      /* since we are using a static array, there is currently
	 no way to overcome this problem */
      fprintf(stderr, "FT-MPI: errclass_add_errcode: running out of memory."
	      " Please increase FTMPI_ERRCLASS_MAX_CODE in ft-mpi-err.h"
	      ", recompile FT-MPI and your application.");
      return ( MPI_ERR_INTERN );
    }
  else
    {
      ftmpi_errclassarr[class].errcodes[f] = code;
      ftmpi_errclassarr[class].nerrcode++;
    }
  
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_err_set_ft_attributes (int numdead, int* procarr)
{
  int class;
  char errstring[MPI_MAX_ERROR_STRING];
  int ret;
  int i, offset;
  int classhandle, codehandle;
  int code;

  if ( numdead == 0 )
    return ( MPI_SUCCESS );

  if ( ftmpi_errcode_recover == MPI_SUCCESS)
    {
      classhandle = ftmpi_errclass_get_next_free (&class);
      if ( classhandle == MPI_ERR_INTERN )
	return ( classhandle );
      
      codehandle = ftmpi_errcode_get_next_free ( classhandle, &code );
      ret = ftmpi_errclass_add_errcode ( classhandle, codehandle );
      if ( ret == MPI_ERR_INTERN ) 
	return ( ret ); 
      
      ftmpi_errcode_recover = code;
     
    }
  else
    {
      code = ftmpi_errcode_recover;
      codehandle = ftmpi_errcode_get_handle ( code );
    }

  ftmpi_errcode_unset_predefined ( codehandle );
  ftmpi_num_dead_procs = numdead;
  sprintf (errstring, "%s", "List of dead processes: ");
  offset = strlen("List of dead processes: ");
  for ( i = 0; i < numdead; i++ ) {
    sprintf (errstring+offset, "%4.4d ", procarr[i]);
    offset += 5;
  }
  sprintf (errstring+offset, "\n");
  ret = ftmpi_errcode_set_string ( codehandle, errstring );
  ftmpi_errcode_set_predefined ( codehandle );

  return (code);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errors_are_fatal ( MPI_Comm *comm, int *errcode, ... )
{
  int handle;
  char output[1024];
  int len;
  int rank;

  /*  va_list args;

      va_start(args, errcode );
      vsprintf (output, errcode, args );
      va_end ( args ); 

      fprintf(stderr,"FTMPI Error with communicator %d, error code %d %s\n",
              *comm, *errcode, output );
  */

  rank = ftmpi_com_rank ( MPI_COMM_WORLD );
  
  handle = ftmpi_errcode_get_handle ( *errcode);
  if ( handle == MPI_ERR_ARG )
    {
      fprintf(stderr,"[%d] FTMPI Error with communicator %d, unknown error "
	      "code %d \n", rank, *comm, *errcode );
    }
  else
    {
      ftmpi_errcode_get_string ( handle, &(output[0]), &len );
      fprintf(stderr,"[%d] FTMPI Error with communicator %d, error code %d "
	      "%s \n", rank, *comm, *errcode, output );
    }

  MPI_Abort ( *comm, *errcode );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_errors_return ( MPI_Comm *comm, int *errcode, ... )
{
  int handle;
  char output[1024];
  int len;
  int rank;

  rank = ftmpi_com_rank ( MPI_COMM_WORLD );

  handle = ftmpi_errcode_get_handle ( *errcode);
  if ( handle == MPI_ERR_ARG )
    {
      fprintf(stderr,"[%d] FTMPI Error with communicator %d, unknown error "
	      "code %d \n", rank, *comm, *errcode );
    }

  else {

  	ftmpi_errcode_get_string ( handle, &(output[0]), &len );
  	fprintf(stderr,"[%d] FTMPI Error with communicator %d, error code %d"
		" %s \n",rank, *comm, *errcode, output );

   }
}
