
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authorsof this file:	
			Edgar Gabriel <egabriel@cs.utk.edu>
 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* we have to think about the include statements, since we mustn't redefine
   here the names again ! */

#define _FT_MPI_PROFILE_H

/*#define MPI_File int
  #define MPI_File_errhandler_fn int */
/* #define MPI_Info int */
#define MPI_Offset int

#include "mpi.h"

/* MPI 1 */
int MPI_Abort ( MPI_Comm comm, int code )
{
return (  PMPI_Abort (  comm,  code ) );
}
int MPI_Address (void *loc, MPI_Aint* address )
{
  return (  PMPI_Address (loc, address ));
}
int MPI_Allgatherv( void* buf, int count, MPI_Datatype dat, 
		    void* buf2, int* count2 , int* displs, 
		    MPI_Datatype dat2, MPI_Comm comm)
{
return ( PMPI_Allgatherv(  buf, count, dat, buf2, 
		       count2,  displs,  dat2, comm) );
}

int MPI_Allgather ( void* buf, int count, MPI_Datatype dat, 
		    void* buf2, int count2, 
		    MPI_Datatype dat2 , MPI_Comm comm)
{
return ( PMPI_Allgather (  buf,  count,  dat,  buf2,  count2, 
			    dat2,  comm) );
}
int MPI_Allreduce (void* buf, void* buf2, int count, MPI_Datatype dat, 
		    MPI_Op op, MPI_Comm comm)
{
return ( PMPI_Allreduce ( buf, buf2, count, dat, op, comm) );
}
int MPI_Alltoall ( void* buf, int count, MPI_Datatype dat, void* buf2, 
		    int count2, MPI_Datatype dat2, MPI_Comm comm)
{
return ( PMPI_Alltoall ( buf, count, dat,  buf2, 
			  count2, dat2, comm) );
}
int MPI_Alltoallv (void* buf, int* count, int* displs, MPI_Datatype dat, void* buf2, 
		    int* count2, int* displs2, MPI_Datatype dat2,
		    MPI_Comm comm )
{
return ( PMPI_Alltoallv ( buf, count, displs, dat, buf2, 
			   count2, displs2, dat2, comm ) );
}
int MPI_Attr_delete ( MPI_Comm comm, int val)
{
return ( PMPI_Attr_delete ( comm, val) );
}
int MPI_Attr_get ( MPI_Comm comm, int val, void* buf, int* key)
{
return ( PMPI_Attr_get ( comm, val, buf, key) );
}
int MPI_Attr_put ( MPI_Comm comm, int val, void* buf)
{ 
return ( PMPI_Attr_put ( comm, val, buf) );
}
 
int MPI_Barrier (MPI_Comm comm )
{
return ( PMPI_Barrier ( comm ) );
}
int MPI_Bcast (void* buf, int count, MPI_Datatype dat, int root, 
		MPI_Comm comm)
{
return ( PMPI_Bcast ( buf, count, dat, root, comm) );
}
int MPI_Bsend (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm  comm)
{
return ( PMPI_Bsend ( buf, count, datatype, dest, tag, comm ));
}
int MPI_Bsend_init (void*  buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Bsend_init ( buf, count, datatype, dest, tag,
		 comm, request) );
}
int MPI_Buffer_attach ( void *buf, int size)
{
return ( PMPI_Buffer_attach ( buf, size) );
}
int MPI_Buffer_detach ( void *buf, int *size )
{
return ( PMPI_Buffer_detach ( buf, size ) );
}
int MPI_Cancel ( MPI_Request* req)
{
return ( PMPI_Cancel (  req) );
}
int MPI_Cart_coords ( MPI_Comm comm, int rank, int ndims, int *dims)
{
return ( PMPI_Cart_coords ( comm, rank, ndims, dims) );
}
int MPI_Cart_create ( MPI_Comm comm, int ndims , int *dims, int *periods, 
		       int reorder, MPI_Comm* newcomm)
{
return ( PMPI_Cart_create ( comm, ndims, dims, periods, 
			reorder,  newcomm) );
}
int MPI_Cart_get ( MPI_Comm comm, int maxdims, int *dims, int *periods, int *coords)
{
return ( PMPI_Cart_get ( comm, maxdims, dims, periods, coords) );
}
int MPI_Cart_map ( MPI_Comm comm, int ndims, int *dims, int *periods, int *newrank)
{
return ( PMPI_Cart_map ( comm, ndims, dims, periods, newrank) );
}
int MPI_Cart_rank ( MPI_Comm comm, int *coords, int *rank)
{
return ( PMPI_Cart_rank ( comm, coords, rank) );
}
int MPI_Cart_shift ( MPI_Comm comm, int dir, int disp, int *r_s, int *r_d)
{
return ( PMPI_Cart_shift ( comm, dir, disp, r_s, r_d) );
}
int MPI_Cart_sub ( MPI_Comm comm, int *r_dims, MPI_Comm *newcomm)
{
return ( PMPI_Cart_sub ( comm, r_dims, newcomm) );
}
int MPI_Cartdim_get ( MPI_Comm comm, int *ndims)
{
return ( PMPI_Cartdim_get ( comm, ndims) );
}
int MPI_Comm_compare ( MPI_Comm comm1, MPI_Comm comm2, int *result)
{
return ( PMPI_Comm_compare ( comm1, comm2, result) );
}
int MPI_Comm_create ( MPI_Comm comm, MPI_Group group, MPI_Comm *newcomm )
{
return ( PMPI_Comm_create ( comm, group, newcomm ) );
}
int MPI_Comm_dup ( MPI_Comm comm, MPI_Comm* newcomm)
{
return ( PMPI_Comm_dup ( comm,  newcomm) );
}
int MPI_Comm_free ( MPI_Comm *comm )
{
return ( PMPI_Comm_free ( comm ) );
}
int MPI_Comm_group ( MPI_Comm comm, MPI_Group *group)
{
return ( PMPI_Comm_group ( comm, group) );
}
int MPI_Comm_rank ( MPI_Comm comm, int *rank)
{
return ( PMPI_Comm_rank ( comm, rank) );
}
int MPI_Comm_remote_group ( MPI_Comm comm, MPI_Group *group)
{
return ( PMPI_Comm_remote_group ( comm, group) );
}
int MPI_Comm_remote_size ( MPI_Comm comm, int *size)
{
return ( PMPI_Comm_remote_size ( comm, size) );
}
int MPI_Comm_size ( MPI_Comm comm, int *size)
{
return ( PMPI_Comm_size ( comm, size) );
}
int MPI_Comm_split ( MPI_Comm comm, int color, int key, MPI_Comm *newcomm)
{
return ( PMPI_Comm_split ( comm, color, key, newcomm ) );
}
int MPI_Comm_test_inter ( MPI_Comm comm, int* flag)
{
return ( PMPI_Comm_test_inter ( comm,  flag) );
}
int MPI_Dims_create (int nnodes, int ndims, int *dims)
{
return ( PMPI_Dims_create (nnodes, ndims, dims) );
}
int MPI_Errhandler_get ( MPI_Comm comm, MPI_Errhandler* err)
{
return ( PMPI_Errhandler_get ( comm,  err) );
}
int MPI_Errhandler_set ( MPI_Comm comm, MPI_Errhandler  err)
{
return ( PMPI_Errhandler_set ( comm, err) );
}
int MPI_Errhandler_create (MPI_Handler_function *f, MPI_Errhandler *e)
{
  return (PMPI_Errhandler_create (f,e));
}
int MPI_Errhandler_free (MPI_Errhandler *e)
{
  return (PMPI_Errhandler_free (e));
}
int MPI_Error_class (int code, int* class)
{
  return (PMPI_Error_class (code, class));
}
int MPI_Error_string (int errorcode, char *string, int* resultlen)
{
  return (PMPI_Error_string (errorcode, string, resultlen));
}

int MPI_Finalize  ( void )
{
return ( PMPI_Finalize  () );
}
int MPI_Gather ( void* buf, int count, MPI_Datatype dat, void* buf2, 
		  int count2, MPI_Datatype dat2, int root, MPI_Comm comm )
{
return ( PMPI_Gather (  buf, count, dat,  buf2, 
		   count2, dat2, root, comm ) );
}
int MPI_Gatherv ( void* buf, int count, MPI_Datatype dat, void* buf2, int* count2, 
		   int* disps, MPI_Datatype dat2,  int root, MPI_Comm comm )
{
return ( PMPI_Gatherv (  buf, count, dat,  buf2,  count2, 
		     disps, dat2,  root, comm ) );
}
int MPI_Get_count (MPI_Status *status, MPI_Datatype datatype, int *count)
{
return ( PMPI_Get_count (status, datatype, count) );
}
int MPI_Get_elements (MPI_Status *status, MPI_Datatype datatype, int *count)
{
  return ( PMPI_Get_elements (status, datatype, count) );
}
int MPI_Get_processor_name (char *name, int *resultlen )
{
  return ( PMPI_Get_processor_name ( name, resultlen ));
}
int MPI_Get_version( int *version, int *subversion )
{
  return ( PMPI_Get_version ( version, subversion ));
}

int MPI_Graph_create ( MPI_Comm comm, int nnodes, int *index, int *edges,
			int reorder, MPI_Comm *newcomm )
{
  return ( PMPI_Graph_create ( comm, nnodes, index, edges, reorder, newcomm ));
}
int MPI_Graph_get ( MPI_Comm comm, int mindex, int medges, int *index, int* edges)
{
  return ( PMPI_Graph_get ( comm, mindex, medges, index, edges ));
}
int MPI_Graph_map ( MPI_Comm comm, int nnodes, int *index, int *edges, int *newrank)
{
  return (PMPI_Graph_map ( comm, nnodes, index, edges, newrank ));
}
int MPI_Graph_neighbors ( MPI_Comm comm, int rank, int mneighbors, int *neighbors)
{
  return ( PMPI_Graph_neighbors ( comm, rank, mneighbors, neighbors ));
}
int MPI_Graph_neighbors_count (MPI_Comm comm, int rank, int* nneighbors )
{
  return ( PMPI_Graph_neighbors_count ( comm, rank, nneighbors ));
}
int MPI_Graphdims_get ( MPI_Comm comm, int *nnodes, int* nedges)
{
  return ( PMPI_Graphdims_get ( comm, nnodes, nedges ));
}
int MPI_Group_compare ( MPI_Group group1, MPI_Group group2, int *result)
{
return ( PMPI_Group_compare ( group1, group2, result) );
}
int MPI_Group_difference ( MPI_Group group1, MPI_Group group2, MPI_Group* newgroup)
{
return ( PMPI_Group_difference ( group1, group2, newgroup) );
}
int MPI_Group_excl ( MPI_Group group, int n, int *ranks, MPI_Group *newgroup)
{
return ( PMPI_Group_excl (group, n, ranks, newgroup) );
}
int MPI_Group_free ( MPI_Group *group)
{
return ( PMPI_Group_free ( group) );
}
int MPI_Group_incl ( MPI_Group group, int n, int *ranks, MPI_Group *newgroup)
{
return ( PMPI_Group_incl ( group, n, ranks, newgroup) );
}
int MPI_Group_intersection ( MPI_Group group1, MPI_Group group2, MPI_Group* newgroup)
{
return ( PMPI_Group_intersection ( group1, group2, newgroup) );
}
int MPI_Group_range_excl ( MPI_Group group, int n, int ranges[][3], MPI_Group* newgroup)
{
return ( PMPI_Group_range_excl ( group, n, ranges,  newgroup) );
}
int MPI_Group_range_incl ( MPI_Group group, int n, int ranges[][3], 
			    MPI_Group* newgroup)
{
return ( PMPI_Group_range_incl ( group, n, ranges,  newgroup) );
}
int MPI_Group_rank ( MPI_Group group, int *rank)
{
return ( PMPI_Group_rank ( group, rank) );
}
int MPI_Group_size ( MPI_Group group, int *size)
{
return ( PMPI_Group_size ( group, size) );
}
int MPI_Group_translate_ranks (MPI_Group group1, int n, int *ranks1,
                                MPI_Group group2, int *ranks2)
{
return ( PMPI_Group_translate_ranks (group1, n, ranks1,
				      group2, ranks2) );
}
int MPI_Group_union ( MPI_Group group1, MPI_Group group2, 
		       MPI_Group *newgroup)
{
return ( PMPI_Group_union ( group1, group2, newgroup) );
}
int MPI_Ibsend (void* buf, int count, MPI_Datatype datatype, int dest,
		int tag, MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Ibsend ( buf, count, datatype, dest,
		  tag, comm, request) );
}
int MPI_Init ( int *argc, char ***argv )
{
return ( PMPI_Init ( argc, argv ) );
}
int MPI_Initialized (int *flag )
{
  return (PMPI_Initialized (flag));
}
int MPI_Intercomm_create ( MPI_Comm l_comm, int l_leader, MPI_Comm p_comm, 
			    int r_leader, int tag, MPI_Comm* icomm)
{
return ( PMPI_Intercomm_create ( l_comm, l_leader, p_comm, 
			     r_leader, tag,  icomm) );
}
int MPI_Intercomm_merge ( MPI_Comm comm, int high, MPI_Comm* newcomm)
{
return ( PMPI_Intercomm_merge ( comm, high,  newcomm) );
}
int MPI_Iprobe ( int source, int tag, MPI_Comm comm, int* flag, 
		  MPI_Status* stat)
{
return ( PMPI_Iprobe ( source, tag, comm,  flag, stat) );
}
int MPI_Irecv (void* buf, int count, MPI_Datatype datatype, int source,
	        int tag, MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Irecv ( buf, count, datatype, source,
		 tag, comm, request) );
}
int MPI_Irsend (void* buf, int count, MPI_Datatype datatype, int dest,
		 int tag, MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Irsend ( buf, count, datatype, dest,
		  tag, comm, request) );
}
int MPI_Isend (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Isend ( buf, count, datatype, dest, tag,
		       comm, request) );
}
int MPI_Issend (void* buf, int count, MPI_Datatype datatype, int dest,
		 int tag, MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Issend ( buf, count, datatype, dest,
		  tag, comm, request) );
}
int MPI_Keyval_create ( MPI_Copy_function* copy_fn, MPI_Delete_function* del_fn, 
			 int* key, void* buf)
{
return ( PMPI_Keyval_create (  copy_fn,  del_fn, key,  buf) );
}
int MPI_Keyval_free ( int *key)
{
return ( PMPI_Keyval_free ( key) );
}
int MPI_Op_create( MPI_User_function* fn, int commute, MPI_Op* op)
{  
return ( PMPI_Op_create(  fn, commute,  op) );
}
 
int MPI_Op_free ( MPI_Op* op)
{
return ( PMPI_Op_free (  op) );
}
int MPI_Pack (void* inbuf, int count, MPI_Datatype datatype, void* outbuf, 
	       int outsize, int *position, MPI_Comm comm)
{
return ( PMPI_Pack ( inbuf, count, datatype,  outbuf, outsize, 
		position, comm) );
}
int MPI_Pack_size ( int count, MPI_Datatype datatype, MPI_Comm comm, int *size)
{
return ( PMPI_Pack_size (count, datatype, comm, size) );
}
int MPI_Probe ( int source, int tag, MPI_Comm comm, MPI_Status* stat)
{
return ( PMPI_Probe ( source, tag, comm,  stat) );
}
int MPI_Pcontrol ( int level, ... )
{
  return ( PMPI_Pcontrol ( level ));
}
int MPI_Recv (void* buf, int count, MPI_Datatype datatype, int source,
	       int tag, MPI_Comm comm, MPI_Status *status)
{
return ( PMPI_Recv (  buf, count, datatype, source,
		tag, comm, status) );
}
int MPI_Recv_init (void* buf, int count, MPI_Datatype datatype, int source,
	       int tag, MPI_Comm comm,  MPI_Request *request)
{
return ( PMPI_Recv_init ( buf, count, datatype, source,
		tag, comm, request) );
}
int MPI_Reduce (void* sendbuf, void* recvbuf, int count, MPI_Datatype datatype,
		 MPI_Op op, int root, MPI_Comm comm)
{
return ( PMPI_Reduce ( sendbuf,  recvbuf, count, datatype,
		  op, root, comm) );
}
int MPI_Reduce_scatter ( void* buf, void* buf2, int* count, MPI_Datatype dat, 
			  MPI_Op op, MPI_Comm comm )
{
return ( PMPI_Reduce_scatter (  buf,  buf2,  count, dat, 
			   op, comm ) );
}
int MPI_Request_free( MPI_Request* req)
{
return ( PMPI_Request_free(  req) );
}
int MPI_Rsend (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm)
{
return ( PMPI_Rsend ( buf, count, datatype, dest, tag,
		       comm) );
}
int MPI_Rsend_init (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Rsend_init ( buf, count, datatype, dest, tag,
			    comm, request) );
}
int MPI_Scan ( void* buf, void* buf2, int count, MPI_Datatype dat, MPI_Op op, MPI_Comm comm )
{
return ( PMPI_Scan (  buf,  buf2, count, dat, op, comm ) );
}
int MPI_Scatter ( void* buf, int count, MPI_Datatype dat, void* buf2, int count2, 
		   MPI_Datatype dat2,  int root, MPI_Comm comm )
{
return ( PMPI_Scatter (  buf, count, dat,  buf2, count2, 
		    dat2, root, comm ) );
}
int MPI_Scatterv ( void* buf, int* count, int* disps, MPI_Datatype dat, void* buf2, 
		    int count2, MPI_Datatype dat2, int root, MPI_Comm comm )
{
return ( PMPI_Scatterv (  buf,  count,  disps, dat,  buf2, 
		     count2, dat2, root, comm ) );
}
int MPI_Send (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	       MPI_Comm comm)
{
return ( PMPI_Send ( buf, count, datatype, dest, tag,
		comm) );
}
int MPI_Send_init (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Send_init ( buf, count, datatype, dest, tag,
		 comm, request) );
}
int MPI_Sendrecv ( void* buf, int count, MPI_Datatype dat, int dest, int tag, 
		    void* buf2, int count2, MPI_Datatype dat2, int source, int tag2, 
		    MPI_Comm comm, MPI_Status* stat)
{
return ( PMPI_Sendrecv (  buf, count, dat, dest, tag, 
		      buf2, count2, dat2, source, tag2, 
		     comm,  stat) );
}
int MPI_Sendrecv_replace ( void* buf, int count, MPI_Datatype dat, int dest, int tag, 
			    int source, int tag2, MPI_Comm comm, MPI_Status* stat)
{
return ( PMPI_Sendrecv_replace (  buf, count, dat, dest, tag, 
			     source, tag2, comm,  stat) );
}
int MPI_Ssend (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm)
{
return ( PMPI_Ssend ( buf, count, datatype, dest, tag,
		 comm) );
}
int MPI_Ssend_init (void* buf, int count, MPI_Datatype datatype, int dest, int tag,
	        MPI_Comm comm, MPI_Request *request)
{
return ( PMPI_Ssend_init ( buf, count, datatype, dest, tag,
		 comm, request) );
}
int MPI_Start ( MPI_Request *request)
{
return ( PMPI_Start ( request) );
}
int MPI_Startall (int count,  MPI_Request *request)
{
return ( PMPI_Startall (count, request) );
}
int MPI_Test (MPI_Request *request, int *flag, MPI_Status *status)
{
return ( PMPI_Test (request, flag, status) );
}
int MPI_Test_cancelled ( MPI_Status* stat, int *flag)
{
return ( PMPI_Test_cancelled (  stat, flag) );
}
int MPI_Testall ( int n, MPI_Request* req, int* flags, MPI_Status* stat)
{
return ( PMPI_Testall ( n,  req,  flags,  stat) );
}
int MPI_Testany ( int n, MPI_Request* req, int* index, int* flag, MPI_Status* stat)
{
return ( PMPI_Testany ( n,  req,  index,  flag,  stat) );
}
int MPI_Testsome (int n, MPI_Request* req, int* index, int* flag, MPI_Status* stat)
{
return ( PMPI_Testsome (n, req,  index,  flag,  stat) );
}
int MPI_Topo_test ( MPI_Comm comm, int *topo)
{
return ( PMPI_Topo_test ( comm, topo) );
}
int MPI_Type_commit ( MPI_Datatype* newtype)
{
return ( PMPI_Type_commit (  newtype) );
}
int MPI_Type_contiguous ( int n, MPI_Datatype dat, MPI_Datatype* newtype)
{
return ( PMPI_Type_contiguous ( n, dat,  newtype) );
}
int MPI_Type_extent ( MPI_Datatype dat, MPI_Aint* disp)
{
return ( PMPI_Type_extent ( dat,  disp) );
}
int MPI_Type_free ( MPI_Datatype* dat)
{
return ( PMPI_Type_free (  dat) );
}
int MPI_Type_hindexed ( int count, int* blength, MPI_Aint* disp, 
			MPI_Datatype dat, MPI_Datatype* newtype)
{
return ( PMPI_Type_hindexed ( count,  blength,  disp, dat, newtype) );
}
int MPI_Type_hvector ( int count, int blength, MPI_Aint stride, 
		       MPI_Datatype dat, MPI_Datatype *newdat)
{
return ( PMPI_Type_hvector ( count, blength, stride, dat, newdat) );
}
int MPI_Type_indexed ( int count, int* blength, int* disps, 
		       MPI_Datatype dat, MPI_Datatype* newtype)
{
return ( PMPI_Type_indexed ( count,  blength,  disps, dat,  newtype) );
}

int MPI_Type_lb ( MPI_Datatype dat, MPI_Aint* disp)
{
return ( PMPI_Type_lb ( dat,  disp) );
}

int MPI_Type_size ( MPI_Datatype dat, int* size)
{
return ( PMPI_Type_size ( dat,  size) );
}
int MPI_Type_struct (int count, int* blength, MPI_Aint* disp, 
		     MPI_Datatype* dat, MPI_Datatype* newdat)
{ 
return ( PMPI_Type_struct (count,  blength,  disp,  dat,  newdat) );
}
 
int MPI_Type_ub ( MPI_Datatype dat, MPI_Aint* disp)
{
return ( PMPI_Type_ub ( dat,  disp) );
}

int MPI_Type_vector ( int count, int blength, int stride, 
		      MPI_Datatype dat, MPI_Datatype *newdat)
{
return ( PMPI_Type_vector ( count, blength, stride, dat, newdat) );
}
int MPI_Unpack (void* inbuf, int insize,int *position,void* outbuf, int outcount,  
                 MPI_Datatype datatype, MPI_Comm comm)
{
return ( PMPI_Unpack ( inbuf, insize, position,  outbuf, outcount,  
		  datatype, comm) );
}
int MPI_Wait (MPI_Request *request, MPI_Status *status)
{
return ( PMPI_Wait (request, status) );
}
int MPI_Waitall (int count, MPI_Request *request, MPI_Status *status)
{
return ( PMPI_Waitall (count, request, status) );
}
int MPI_Waitany (int count, MPI_Request *request, int *index, 
		 MPI_Status *status)
{
return ( PMPI_Waitany (count, request, index, status) );
}
int MPI_Waitsome ( int count, MPI_Request* req, int* outcount, 
		   int* indeces, MPI_Status* stat)
{
return ( PMPI_Waitsome ( count,  req,  outcount,  indeces,  stat) );
}


/* MPI - 2 functions */

/*
int MPI_Comm_set_name(MPI_Comm comm, char *c)
{
  return (PMPI_Comm_set_name(comm, c));
}
int MPI_Comm_get_name(MPI_Comm comm, char *c, int *i)
{
  return (PMPI_Comm_get_name( comm, c, i));
}
int MPI_Type_set_name(MPI_Datatype dat, char *c)
{
  return (PMPI_Type_set_name(dat, c));
}
int MPI_Type_get_name(MPI_Datatype dat, char *c, int *i)
{
  return (PMPI_Type_get_name(dat, c, i));
}
int MPI_Status_f2c( MPI_Fint *fstat, MPI_Status *cstat)
{
  return (PMPI_Status_f2c( fstat, cstat));
}
int MPI_Status_c2f( MPI_Status *cstat, MPI_Fint *fstat)
{
  return (PMPI_Status_c2f( cstat, fstat));
}
int MPI_Request_get_status (MPI_Request request, int *flag, MPI_Status *status)
{
  return (PMPI_Request_get_status (request, flag, status));
}
int MPI_Finalized( int *flag)
{
  return (PMPI_Finalized( flag));
}
int MPI_Type_create_indexed_block(int i1, int i2, int *ip, MPI_Datatype dat, 
				  MPI_Datatype *newdat)
{
  return (PMPI_Type_create_indexed_block(i1, i2, ip, dat, newdat));
}
*/
int MPI_Type_get_envelope(MPI_Datatype dat, int *ip1, int
			  *ip2, int *ip3, int *ip4)
{
  return (PMPI_Type_get_envelope(dat, ip1, ip2, ip3, ip4));
}
int MPI_Type_get_contents(MPI_Datatype dat, int i1, int i2, 
			  int i3, int *ip, MPI_Aint *addr, 
			  MPI_Datatype *datp)
{
  return (PMPI_Type_get_contents(dat, i1, i2, i3, ip, addr, datp));
}

#ifdef DECLARE_MPIIO
int MPI_Info_create (MPI_Info *info)
{
  return (PMPI_Info_create ( info));
}

int MPI_Info_set (MPI_Info info, char *key, char *value)
{
  return (PMPI_Info_set ( info, key, value));
}

int MPI_Info_delete (MPI_Info info, char *key)
{
  return (PMPI_Info_delete (info, key));
}
int MPI_Info_get (MPI_Info info, char *key, int valuelen, char *value, int *flag)
{
  return (PMPI_Info_get ( info,  key,  valuelen,  *value,  flag));
}
int MPI_Info_get_valuelen (MPI_Info info, char *key, int *valuelen, int *flag)
{
  return (PMPI_Info_get_valuelen ( info, key, valuelen, flag));
}
int MPI_Info_get_nkeys (MPI_Info info, int *nkeys)
{
  return (PMPI_Info_get_nkeys (info, nkeys));
}
int MPI_Info_get_nthkey (MPI_Info info, int n, char *key)
{
  return (PMPI_Info_get_nthkey ( info,  n, key));
}
int MPI_Info_dup (MPI_Info info, MPI_Info *newinfo)
{
  return (PMPI_Info_dup ( info, newinfo));
}
int MPI_Info_free (MPI_Info *info)
{
  return (PMPI_Info_free (info));
}
#endif  /* DECLARE_MPIIO */

/*
int MPI_Alloc_mem (MPI_Aint ad, MPI_Info inf, void *p)
{
  return (PMPI_Alloc_mem (ad, inf, p));
}
int MPI_Free_mem (void *p)
{
  return (PMPI_Free_mem (p));
}
MPI_File MPI_File_f2c (MPI_Fint fh)
{
  return (PMPI_File_f2c(fh));
}
MPI_Fint MPI_File_c2f (MPI_File fh)
{
  return (PMPI_File_c2f ( fh));
}
MPI_Fint MPI_Info_c2f (MPI_Info info)
{
  return (PMPI_Info_c2f ( info));
}
MPI_Info MPI_Info_f2c (MPI_Fint info)
{
  return (PMPI_Info_f2c(info));
}
MPI_Win  MPI_Win_f2c(MPI_Fint win)
{
  return (PMPI_Win_f2c(win));
}
MPI_Fint MPI_Win_c2f(MPI_Win win)
{
  return (PMPI_Win_c2f(win));
}
MPI_Fint MPI_Request_c2f( MPI_Request req)
{
  return (PMPI_Request_c2f( req));
}
MPI_Request MPI_Request_f2c( MPI_Fint freq)
{
  return (PMPI_Request_f2c(freq));
}
*/
int MPI_Comm_create_errhandler (MPI_Comm_errhandler_fn *function, 
				MPI_Errhandler *errhandler)
{
  return (PMPI_Comm_create_errhandler (function, errhandler));
}
int MPI_Comm_get_errhandler (MPI_Comm fh, MPI_Errhandler *errhandler)
{
  return (PMPI_Comm_get_errhandler ( fh, errhandler));
}
int MPI_Comm_set_errhandler (MPI_Comm fh, MPI_Errhandler  errhandler)
{
  return (PMPI_Comm_set_errhandler ( fh, errhandler));
}
/*

int MPI_Win_create_errhandler (MPI_Win_errhandler_fn *w, MPI_Errhandler *e)
{
  return (PMPI_Win_create_errhandler ( w, e));
}
int MPI_Win_set_errhandler(MPI_Win w, MPI_Errhandler e)
{
  return (PMPI_Win_set_errhandler( w, e));
}
int MPI_Win_get_errhandler(MPI_Win w, MPI_Errhandler *e)
{
  return (PMPI_Win_get_errhandler( w, e));
}
*/

#ifdef DECLARE_MPIIO
int MPI_File_create_errhandler ( MPI_File_errhandler_fn *function, 
				 MPI_Errhandler *errhandler)
{
  return (PMPI_File_create_errhandler ( function, errhandler));
}
int MPI_File_get_errhandler ( MPI_File fh, MPI_Errhandler *errhandler)
{
  return (PMPI_File_get_errhandler ( fh, errhandler));
}
int MPI_File_set_errhandler  ( MPI_File fh, MPI_Errhandler  errhandler)
{
  return (PMPI_File_set_errhandler (fh, errhandler));
}
#endif  /* DECLARE_MPIIO */

/*
int MPI_Type_create_resized (MPI_Datatype oldtype, MPI_Aint lb, 
			     MPI_Aint extent, MPI_Datatype *newtype)
{
  return (PMPI_Type_create_resized ( oldtype, lb, extent, newtype));
}
*/
int MPI_Type_create_subarray (int ndims, int *array_of_sizes, 
			      int *array_of_subsizes, int *array_of_starts, 
			      int order,  MPI_Datatype oldtype, 
			      MPI_Datatype *newtype)
{
  return (PMPI_Type_create_subarray ( ndims, array_of_sizes, array_of_subsizes, 
				      array_of_starts, order, oldtype, newtype));
}
int MPI_Type_create_darray (int size, int rank, int ndims, int *array_of_gsizes, 
			    int *array_of_distribs,  int *array_of_dargs, 
			    int *array_of_psizes, int order, 
			    MPI_Datatype oldtype, MPI_Datatype *newtype)
{
  return (PMPI_Type_create_darray ( size, rank, ndims, array_of_gsizes, 
				    array_of_distribs, array_of_dargs, 
				    array_of_psizes, order, oldtype, newtype));
}

/*
int MPI_Pack_external (char *datarep, void *inbuf, int count, 
		       MPI_Datatype datatype, void *outbuf, 
		       MPI_Aint outsize, MPI_Aint *position)
{
  return (PMPI_Pack_external ( datarep, inbuf, count,  datatype, 
			       outbuf,  outsize, position));
}
int MPI_Unpack_external (char *datarep, void *inbuf, MPI_Aint insize, 
			 MPI_Aint *position, void *outbuf, int outcount, 
			 MPI_Datatype datatype)
{
  return (PMPI_Unpack_external (datarep, inbuf,  insize, position, 
				outbuf, outcount, datatype));
}
int MPI_Pack_external_size (char *datarep, int incount, MPI_Datatype datatype, 
			    MPI_Aint *size)
{
  return (PMPI_Pack_external_size (datarep, incount, datatype, size));
}
int MPI_Comm_spawn (char *command, char *argv[], int maxprocs,  
		    MPI_Info info, int root, MPI_Comm comm, 
		    MPI_Comm *intercomm, int array_of_errcodes[])
{
  return (PMPI_Comm_spawn (command, argv, maxprocs, info, root,  comm, 
			   intercomm, array_of_errcodes));
}
int MPI_Comm_spawn_multiple (int count, char *array_of_commands[], 
			     char **array_of_argv[], int array_of_maxprocs[], 
			     MPI_Info array_of_info[], int root, 
			     MPI_Comm comm, MPI_Comm *intercomm, 
			     int array_of_errcodes[])
{
  return (PMPI_Comm_spawn_multiple ( count, array_of_commands, array_of_argv, 
				     array_of_maxprocs,  array_of_info, root, 
				     comm, intercomm, array_of_errcodes));
}
int MPI_Comm_get_parent (MPI_Comm *parent)
{
  return (PMPI_Comm_get_parent (parent));
}
int MPI_Open_port (MPI_Info info, char *port_name)
{
  return (PMPI_Open_port (info, port_name));
}
int MPI_Close_port (char *port_name)
{
  return (PMPI_Close_port (port_name));
}
int MPI_Comm_accept (char *port_name, MPI_Info info, int root, 
		     MPI_Comm comm, MPI_Comm *newcomm)
{
  return (PMPI_Comm_accept (port_name, info, root, comm, newcomm));
}
int MPI_Comm_connect (char *port_name, MPI_Info info, int root, 
		      MPI_Comm comm, MPI_Comm *newcomm)
{
  return (PMPI_Comm_connect (port_name,  info, root,  comm, newcomm));
}
int MPI_Publish_name (char *service_name, MPI_Info info, char *port_name)
{
  return (PMPI_Publish_name (service_name,  info, port_name));
}
int MPI_Unpublish_name (char *service_name, MPI_Info info, char *port_name)
{
  return (PMPI_Unpublish_name (service_name,  info, _name));
}
int MPI_Lookup_name (char *service_name, MPI_Info info, char *port_name)
{
  return (PMPI_Lookup_name (service_name,  info, _name));
}
int MPI_Comm_join (int fd, MPI_Comm *intercomm)
{
  return (PMPI_Comm_join (fd, intercomm));
}
int MPI_Comm_disconnect (MPI_Comm *comm)
{
  return (PMPI_Comm_disconnect ( comm));
}
int MPI_Win_create (void *p, MPI_Aint addr, int i, MPI_Info info,  
		    MPI_Comm comm, MPI_Win *win)
{
  return (PMPI_Win_create (p, addr, i, info, comm, win));
}
int MPI_Win_free (MPI_Win *win)
{
  return (PMPI_Win_free (win));
}
int MPI_Win_get_group (MPI_Win win, MPI_Group *group)
{
  return (PMPI_Win_get_group ( win, group));
}
int MPI_Put (void *p, int i1, MPI_Datatype dat1, int i2, 
	     MPI_Aint addr, int i3, MPI_Datatype dat2, 
	     MPI_Win win)
{
  return (PMPI_Put (p, i1, dat1, i2, addr, i3, dat2, win));
}
int MPI_Get (void *p, int i1, MPI_Datatype dat1, int i2, 
	     MPI_Aint addr, int i3, MPI_Datatype dat2, 
	     MPI_Win win)
{
  return (PMPI_Get (p, i1, dat1, i2, addr, i3, dat2, win));
}
int MPI_Accumulate (void *p, int i1, MPI_Datatype dat1, int i2, 
		    MPI_Aint addr, int i3, MPI_Datatype dat2, 
		    MPI_Op op, MPI_Win win)
{
  return (PMPI_Accumulate (p, i1, dat1, i2, addr, i3, dat2, op, win));
}
int MPI_Win_fence (int i, MPI_Win win)
{
  return (PMPI_Win_fence (i, win));
}
int MPI_Win_start (MPI_Group group, int i, MPI_Win win)
{
  return (PMPI_Win_start (group, i, win));
}
int MPI_Win_complete (MPI_Win win)
{
  return (PMPI_Win_complete (win));
}
int MPI_Win_post (MPI_Group group, int i, MPI_Win win)
{
  return (PMPI_Win_post (group, i, win));
}
int MPI_Win_wait (MPI_Win win)
{
  return (PMPI_Win_wait (win));
}
int MPI_Win_test (MPI_Win win, int *ip)
{
  return (PMPI_Win_test (win, ip));
}
int MPI_Win_lock (int i1, int i2, int i3,  MPI_Win win)
{
  return (PMPI_Win_lock (i1, i2, i3, win));
}
int MPI_Win_unlock (int i, MPI_Win win)
{
  return (PMPI_Win_unlock (i, win));
}
int MPI_Grequest_start (MPI_Grequest_query_function *query_fn, 
			MPI_Grequest_free_function  *free_fn, 
			MPI_Grequest_cancel_function *cancel_fn, 
			void *extra_state, MPI_Request *request)
{
  return (PMPI_Grequest_start (query_fn, free_fn, cancel_fn, 
			       extra_state, request));
}
int MPI_Grequest_complete (MPI_Request request)
{
  return (PMPI_Grequest_complete (request));
}
int MPI_Status_set_elements  (MPI_Status *status, MPI_Datatype datatype, 
			      int count)
{
  return (PMPI_Status_set_elements  (status,  datatype, count));
}
int MPI_Status_set_cancelled (MPI_Status *status, int flag)
{
  return (PMPI_Status_set_cancelled ( status, flag));
}
int MPI_Win_set_name (MPI_Win win, char *c)
{
  return (PMPI_Win_set_name (win, c));
}
int MPI_Win_get_name (MPI_Win win, char *c, int *i)
{
  return (PMPI_Win_get_name (win, c, i));
}
*/
int MPI_Add_error_class (int *errorclass)
{
  return (PMPI_Add_error_class (errorclass));
}
int MPI_Add_error_code  (int  errorclass, int *errorcode)
{
  return (PMPI_Add_error_code  ( errorclass, errorcode));
}
int MPI_Add_error_string(int  errorcode, char *string)
{
  return (PMPI_Add_error_string(errorcode, string));
}
int MPI_Comm_call_errhandler (MPI_Comm comm, int code)
{
  return (PMPI_Comm_call_errhandler (comm, code));
}
/*
int MPI_File_call_errhandler (MPI_File fh, int code)
{
  return (PMPI_File_call_errhandler ( fh, code));
}
int MPI_Win_call_errhandler  (MPI_Win  win,  int code)
{
  return (PMPI_Win_call_errhandler  (win, code));
}
int MPI_Init_thread (int *argc, char ***argv, int required, int *provided)
{
  return (PMPI_Init_thread (argc, argv, required, provided));
}
int MPI_Query_thread (int *provided)
{
  return (PMPI_Query_thread (provided));
}
int MPI_Is_thread_main (int *flag)
{
  return (PMPI_Is_thread_main ( flag));
}
*/
int MPI_Comm_create_keyval (MPI_Comm_copy_attr_function *copy_fn, 
			    MPI_Comm_delete_attr_function *delete_fn, 
			    int *i, void *p)
{
  return (PMPI_Comm_create_keyval ( copy_fn, delete_fn, i, p));
}
int MPI_Comm_free_keyval ( int *keyval)
{
  return (PMPI_Comm_free_keyval ( keyval));
}
int MPI_Comm_set_attr ( MPI_Datatype dat, int i, void *p)
{
  return (PMPI_Comm_set_attr ( dat, i, p));
}
int MPI_Comm_get_attr ( MPI_Datatype dat, int i, void *p, int *ip)
{
  return (PMPI_Comm_get_attr ( dat, i, p, ip));
}
int MPI_Comm_delete_attr ( MPI_Datatype dat, int i)
{
  return (PMPI_Comm_delete_attr ( dat, i));
}
/*
int MPI_Type_create_keyval ( MPI_Type_copy_attr_function *copy_fn,  
			     MPI_Type_delete_attr_function *delete_fn, 
			     int *i, void *p)
{
  return (PMPI_Type_create_keyval ( copy_fn, delete_fn, i, p));
}
int MPI_Type_free_keyval ( int *keyval)
{
  return (PMPI_Type_free_keyval ( keyval));
}
int MPI_Type_set_attr ( MPI_Datatype dat, inti, void *p)
{
  return (PMPI_Type_set_attr ( dat, i, p));
}
int MPI_Type_get_attr ( MPI_Datatype dat, int i, void *p, int *ip)
{
  return (PMPI_Type_get_attr ( dat, i, p, ip));
}
int MPI_Type_delete_attr ( MPI_Datatype dat, int i)
{
  return (PMPI_Type_delete_attr ( dat, i));
}
int MPI_Win_create_keyval ( MPI_Win_copy_attr_function *win_copy_attr_fn, 
			    MPI_Win_delete_attr_function *win_delete_attr_fn, 
			    int *keyval, void *extra_state)
{
  return (PMPI_Win_create_keyval ( win_copy_attr_fn, win_delete_attr_fn, 
				   keyval, extra_state));
}
int MPI_Win_free_keyval ( int *win_keyval)
{
  return (PMPI_Win_free_keyval ( win_keyval));
}
int MPI_Win_set_attr ( MPI_Win win, int win_keyval, void *attribute_val)
{
  return (PMPI_Win_set_attr ( win, win_keyval, attribute_val));
}
int MPI_Win_get_attr ( MPI_Win win, int win_keyval, void *attribute_val, int *flag)
{
  return (PMPI_Win_get_attr ( win, win_keyval, attribute_val, flag));
}
int MPI_Win_delete_attr ( MPI_Win win, int win_keyval)
{
  return (PMPI_Win_delete_attr ( win, win_keyval));
}
int MPI_Type_dup ( MPI_Datatype dat, MPI_Datatype *newdat)
{
  return (PMPI_Type_dup ( dat, newdat));
}
*/

#ifdef DECLARE_MPIIO
/* MPI-IO function prototypes */
int MPI_File_open ( MPI_Comm comm, char *filename, int amode, 
		    MPI_Info info, MPI_File *fh)
{
  return (PMPI_File_open ( comm, filename, amode, info, fh));
}
int MPI_File_close ( MPI_File *fh)
{
  return (PMPI_File_close ( fh));
}
int MPI_File_delete ( char *filename, MPI_Info info)
{
  return (PMPI_File_delete ( filename,  info));
}
int MPI_File_set_size ( MPI_File fh, MPI_Offset size)
{
  return (PMPI_File_set_size ( fh, size));
}

/* int MPI_File_preallocate ( MPI_File fh, MPI_Offset size)
{
  return (PMPI_File_preallocate ( fh, size));
} */
int MPI_File_get_size ( MPI_File fh, MPI_Offset *size)
{
  return (PMPI_File_get_size ( fh, size));
}
int MPI_File_get_group ( MPI_File fh, MPI_Group *group)
{
  return (PMPI_File_get_group ( fh, group));
}
int MPI_File_get_amode ( MPI_File fh, int *amode)
{
  return (PMPI_File_get_amode ( fh, amode));
}
int MPI_File_set_info ( MPI_File fh, MPI_Info info)
{
  return (PMPI_File_set_info ( fh, info));
}
int MPI_File_get_info ( MPI_File fh, MPI_Info *info_used)
{
  return (PMPI_File_get_info ( fh, info_used));
}
int MPI_File_set_view ( MPI_File fh, MPI_Offset disp, 
			MPI_Datatype etype, MPI_Datatype filetype, 
			char *datarep, MPI_Info info)
{
  return (PMPI_File_set_view ( fh, disp, etype, filetype, datarep, info));
}
int MPI_File_get_view ( MPI_File fh, MPI_Offset *disp, MPI_Datatype *etype, 
			MPI_Datatype *filetype, char *datarep)
{
  return (PMPI_File_get_view ( fh, disp, etype, filetype, datarep));
}
int MPI_File_read_at ( MPI_File fh, MPI_Offset offset, void *buf, 
		       int count, MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read_at ( fh, offset, buf, count, datatype, status));
}
int MPI_File_read_at_all ( MPI_File fh, MPI_Offset offset, void *buf, 
			   int count, MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read_at_all ( fh, offset, buf, count, datatype, status));
}
int MPI_File_write_at ( MPI_File fh, MPI_Offset offset, void *buf, 
			int count, MPI_Datatype datatype, 
			MPI_Status *status)
{
  return (PMPI_File_write_at ( fh, offset, buf, count, datatype, status));
}
int MPI_File_write_at_all ( MPI_File fh, MPI_Offset offset, 
			    void *buf, int count, 
			    MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_write_at_all ( fh, offset, buf, count, datatype, status));
}
int MPI_File_iread_at ( MPI_File fh, MPI_Offset offset, void *buf, 
			int count, MPI_Datatype datatype, 
			MPI_Request *request)
{
  return (PMPI_File_iread_at ( fh, offset, buf, count, datatype, request));
}
int MPI_File_iwrite_at ( MPI_File fh, MPI_Offset offset, void *buf, 
			 int count, MPI_Datatype datatype, 
			 MPI_Request *request)
{
  return (PMPI_File_iwrite_at ( fh, offset, buf, count, datatype, request));
}
/*
int MPI_File_read ( MPI_File fh, void *buf, int count, 
		    MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read ( fh, buf, count, datatype, status)); 
}
int MPI_File_read_all ( MPI_File fh, void *buf, int count, 
			MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read_all ( fh, buf, count, datatype, status)); 
}
int MPI_File_write ( MPI_File fh, void *buf, int count, 
		     MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_write ( fh, buf, count, datatype, status));
}
int MPI_File_write_all ( MPI_File fh, void *buf, int count, 
			 MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_write_all ( fh, buf, count, datatype, status));
}
int MPI_File_iread ( MPI_File fh, void *buf, int count, 
		     MPI_Datatype datatype, MPI_Request *request)
{
  return (PMPI_File_iread ( fh, buf, count, datatype, request)); 
}
int MPI_File_iwrite ( MPI_File fh, void *buf, int count, 
		      MPI_Datatype datatype, MPI_Request *request)
{
  return (PMPI_File_iwrite ( fh, buf, count, datatype, request));
}
int MPI_File_seek ( MPI_File fh, MPI_Offset offset, int whence)
{
  return (PMPI_File_seek (fh, offset, whence));
}
int MPI_File_get_position (MPI_File fh, MPI_Offset *offset)
{
  return (PMPI_File_get_position (fh, offset));
}
int MPI_File_get_byte_offset (MPI_File fh, MPI_Offset offset, MPI_Offset *disp)
{
  return (PMPI_File_get_byte_offset (fh, offset, disp));
}
int MPI_File_read_shared  (MPI_File fh, void *buf, int count, 
			   MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read_shared  ( fh, buf, count, datatype, status));
}
int MPI_File_write_shared (MPI_File fh, void *buf, int count, 
			   MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_write_shared ( fh, buf, count, datatype, status));
}
int MPI_File_seek_shared  (MPI_File fh, MPI_Offset offset, int whence)
{
  return (PMPI_File_seek_shared  ( fh, offset, whence));
}
int MPI_File_get_position_shared (MPI_File fh, MPI_Offset *offset)
{
  return (PMPI_File_get_position_shared ( fh, offset));
}
int MPI_File_read_ordered  (MPI_File fh, void *buf, int count, 
			    MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_read_ordered  ( fh, buf, count, datatype, status));
}
int MPI_File_write_ordered (MPI_File fh, void *buf, int count, 
			    MPI_Datatype datatype, MPI_Status *status)
{
  return (PMPI_File_write_ordered ( fh, buf, count, datatype, status));
}
int MPI_File_iread_shared  (MPI_File fh, void *buf, int count, 
			    MPI_Datatype datatype, MPI_Request *request)
{
  return (PMPI_File_iread_shared  ( fh, buf, count, datatype, request));
}
int MPI_File_iwrite_shared (MPI_File fh, void *buf, int count, 
			    MPI_Datatype datatype, MPI_Request *request)
{
  return (PMPI_File_iwrite_shared ( fh, buf, count, datatype, request));
}
int MPI_File_read_ordered_begin  (MPI_File fh, void *buf, int count, 
				  MPI_Datatype datatype)
{
  return (PMPI_File_read_ordered_begin  ( fh, buf, count, datatype));
}
int MPI_File_write_ordered_begin (MPI_File fh, void *buf, int count, 
				  MPI_Datatype datatype)
{
  return (PMPI_File_write_ordered_begin ( fh, buf, count, datatype));
}
int MPI_File_read_all_begin (MPI_File fh, void *buf, int count, 
			     MPI_Datatype datatype)
{
  return (PMPI_File_read_all_begin ( fh, buf, count, datatype));
}
int MPI_File_write_all_begin (MPI_File fh, void *buf, int count, 
			      MPI_Datatype datatype)
{
  return (PMPI_File_write_all_begin ( fh, buf, count, datatype));
}
int MPI_File_read_at_all_begin  (MPI_File fh, MPI_Offset offset, 
				 void *buf, int count, MPI_Datatype datatype)
{
  return (PMPI_File_read_at_all_begin ( fh, offset, buf, count, datatype));
}
int MPI_File_write_at_all_begin  (MPI_File fh, MPI_Offset offset, 
				  void *buf, int count, MPI_Datatype datatype)
{
  return (PMPI_File_write_at_all_begin  ( fh, offset, buf, count, datatype));
}
int MPI_File_read_ordered_end    (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_read_ordered_end    ( fh, buf, status));
}
int MPI_File_read_all_end (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_read_all_end ( fh, buf, status));
}
int MPI_File_read_at_all_end (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_read_at_all_end ( fh, buf, status));
}
int MPI_File_write_ordered_end (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_write_ordered_end   ( fh, buf, status));
}
int MPI_File_write_all_end (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_write_all_end ( fh, buf, status));
}
int MPI_File_write_at_all_end (MPI_File fh, void *buf, MPI_Status *status)
{
  return (PMPI_File_write_at_all_end ( fh, buf, status));
}
int MPI_File_get_type_extent (MPI_File fh, MPI_Datatype datatype, MPI_Aint *extent)
{
  return (PMPI_File_get_type_extent ( fh,  datatype, extent));
}

int MPI_Register_datarep (char *datarep, MPI_Datarep_conversion_function 
			  *read_conversion_fn, MPI_Datarep_con
			  version_function *write_conversion_fn, 
			  MPI_Datarep_extent_function *dtype
			  _file_extent_fn, void *extra_state)
{
  return (PMPI_Register_datarep (datarep, read_conversion_fn, 
				 write_conversion_fn, dtype_file_extent_fn, 
				 extra_state));
}

int MPI_File_set_atomicity (MPI_File fh, int flag)
{
  return (PMPI_File_set_atomicity ( fh, flag));
}
*/
int MPI_File_get_atomicity (MPI_File fh, int *flag)
{
  return (PMPI_File_get_atomicity ( fh, flag));
}
int MPI_File_sync (MPI_File fh)
{
  return (PMPI_File_sync (fh));
}

#endif  /* DECLARE_MPIIO */
/*
int MPI_Type_match_size (int typeclass, int size, MPI_Datatype *datatype)
{
  return (PMPI_Type_match_size (typeclass,  size, datatype));
}
int MPI_Type_create_f90_complex (int p, int r, MPI_Datatype *type)
{
  return (PMPI_Type_create_f90_complex ( p,  r, type));
}
int MPI_Type_create_f90_real    (int p, int r, MPI_Datatype *type)
{
  return (PMPI_Type_create_f90_real    ( p,  r, type));
}
int MPI_Type_create_f90_integer (int r, MPI_Datatype *type)
{
  return (PMPI_Type_create_f90_integer ( r, type));
}
*/

double MPI_Wtime ()
{
	return (PMPI_Wtime ());
}

double MPI_Wtick()
{
	return (PMPI_Wtick());
}

