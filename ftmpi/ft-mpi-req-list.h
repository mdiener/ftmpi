
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	Originally part of PVMPI/MPI_Connect by Graham 
*/




#ifndef REQLISTH
#define REQLISTH
/* for DDT ops */
#include "ft-mpi-ddt-sys.h"

/* for SNIPE2 DTs */
#include "snipe2-msg-list.h"


#define REQ_SEND_MASK        0x1000   /* send request */
#define REQ_RECV_MASK        0x2000   /* recv request */
#define REQ_PERSISTENT_MASK  0x4000   /* persistent request mask */

#define REQ_SEND	(REQ_SEND_MASK|0x0001)	/* normal blocking send */
#define REQ_RECV	(REQ_RECV_MASK|0x0010)	/* normal blocking recv */
#define REQ_ISEND	(REQ_SEND_MASK|0x0002)	/* non-blocking send */
#define REQ_IRECV	(REQ_RECV_MASK|0x0020)	/* non blocking recv */
#define REQ_IRSEND	(REQ_SEND_MASK|0x0004)	/* non blocking ready send */
#define REQ_IBSEND	(REQ_SEND_MASK|0x0008)	/* non blocking buffed send */
#define REQ_ISSEND	(REQ_SEND_MASK|0x0100)	/* non blocking sync send */

#define REQ_PSEND	(REQ_SEND|REQ_PERSISTENT_MASK)    /* persistant send */
#define REQ_PRECV	(REQ_RECV|REQ_PERSISTENT_MASK)    /* persistant recv */
#define REQ_PRSEND	(REQ_IRSEND|REQ_PERSISTENT_MASK)  /* persistant ready send */
#define REQ_PBSEND	(REQ_IBSEND|REQ_PERSISTENT_MASK)  /* persistant buffered send */
#define REQ_PSSEND	(REQ_ISSEND|REQ_PERSISTENT_MASK)  /* persistant sync send */

/* Request list, used by nonblocking handler routines. */
typedef struct req_list FTMPI_Request_t;
struct req_list {
  struct req_list *rl_link, *rl_rlink;  /* linked list stuff */

  /* request info */
  int rl_req_handle;      /* handle number used by user nb lib */
  long rl_req_id;	  /* internal ID number used */
  int rl_req_type;        /* Type of request i.e. REQ_XXX  */
  int rl_req_refcnt;	  /* ref count used for freeing */
  int rl_req_com;	  /* which communicator do we belong */

  /* low level message handling info */
  /* if its a send have we either send/recvd an RTS or CTS */
  int rl_req_rts;	  /* have we sent a RTS yet */					
  int rl_req_cts;	  /* 0 if not reply, -1 if cancel or 'n' for matching */
  /* just for recvs */
  int rl_req_msgid;	  /* matching message ID */

  /* request specifics */
  int rl_posters_rank; /* posters rank */
  int rl_target_rank;	  /* i.e. recv from or sendto rank */
  int rl_mpi_tag;         /* Actual Tag used in MPI either the send or recv */

  /* for ft-mpi */
  int rl_sgid;		  /* senders GID */
  int rl_rgid;		  /* receivers GID */
  int rl_req_com_ver;	  /* which version of the COM is it! */

  /* if a message operation */

  /* info needed for persistent requests */
  int rl_req_per_cnt;		/* count of data */
  MPI_Datatype rl_req_per_dt;	/* which datatype */
  char *rl_req_per_usrbuf;	/* pointer to the users buffer/data */

  /* these are different depending on what the user requests */
  /* and what the system does re a temp buffer if needed for DDT handling etc */

  /* original request info */
  int rl_req_cnt;		/* count of data */
  MPI_Datatype rl_req_dt;	/* which datatype */
  char *rl_req_usrbuf;		/* pointer to the users buffer/data */

  MPI_Status rl_status;		/* MPI status object (to be copied to user) */

  /* this is the temp buffer used with this request */
  int rl_req_dt_op;             /* if the temp buffer is used/needed */
  char *rl_req_rawbuf;          /* pointer to any data either recvd or to be sent */
  int rl_req_rawbufid;			/* the raw msg buffer id */
  FTMPI_DDT_CODE_INFO * code_info;

  /* the actual hdr we send with or recvd */
  /* used in sends only for two part operations */
  /* one part sends encode the header + encoded data in the rawbuf */
/*   msg_hdr_t	rl_req_hdr;			 */
  /* msg hdr */

  /* low level device info if needed */
  /* SNIPE 2 */

  int 					rl_snipe2_conn;		/* which connection */
  snipe2_msg_list_t* 	rl_snipe2_reqptr;	/* ptr to actual low level req */
  int 					rl_snipe2_reqid;	/* int handle to low level req */
  int					rl_snipe2_rc;		/* snipe2 rc (error code?) */
  
  /* OVM */
  /* ... */
};

/* Now some flags for the status */
#define FTMPI_REQ_CANCELLED 0x00000001
#define FTMPI_REQ_DONE      0x10000000
#define FTMPI_REQ_FREED     0x01000000
#define FTMPI_REQ_EMPTY     0x00000000

  /* Unread/pending message list attached to this communicator */
/*
  struct req_list *cl_req_list_head;  
  int   cl_req_list_count;          
*/
				/* number pending messages */
                                /* included as a sanity check */

struct req_list *     req_list_init();      /* called only once */
void req_list_init_freelist(int, int);
void                  req_list_free();		/* frees entry from list only */
void req_list_free_all (struct req_list* rl_head);
void                  req_list_destroy(); /* destroys list */
struct req_list *      req_list_head();  
struct req_list *      req_list_tail(); 
void                  req_list_dump();      /* dumps contents of whole list */
struct req_list *      req_list_new();       /* makes new and adds to tail */
struct req_list *      req_list_find_by_header(); /* search by rank,tag,com */ 
struct req_list *      req_list_find_by_req_id(); /* search by req_id */
struct req_list *      req_list_find_by_rgid(); /* search by rl_rgid */
/* search by rl_rgid and a low level connection not set to the given channel */
struct req_list *      req_list_find_by_rgid_and_notchan(); 

									/* makes new and adds to tail */
struct req_list *      req_list_new_element_only(); 

void		       req_list_add_2tail ();	/* adds to the tail.. */ 
void                   req_list_detach(struct req_list *rlp);

int	req_hdr_match ();	/* return 1 if the headers match */

struct req_list *		req_list_find_req_matching_recvhdr(); /* nb recv match */

int 					req_list_howmany_send_to_gid ();  /* just a count of reqs with rgid=gid */
int 					req_list_howmany_recv_from_gid ();  /* a count of matching sgids=gid or anysource */


/* more modern prototypes */
void req_list_dump_compact (char *comment, struct req_list *rl_head, int expectedcount);


#endif /* REQLISTH */


