			
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	This code was originally from the PVMPI/MPI_Connect projects 
*/
			
			

#include "mpi.h"
#include "stdio.h"


int msg_size (datatype, count)
int count;
MPI_Datatype datatype;
{
int rc=0;

		/* only handle simple datatypes */

		if((datatype==MPI_BYTE)||(datatype==MPI_CHAR)||(datatype==MPI_UNSIGNED_CHAR))
			rc = sizeof (char);

		else if ((datatype==MPI_SHORT)||(datatype==MPI_UNSIGNED_SHORT))
			rc = sizeof (short);

		else if ((datatype==MPI_INT)||(datatype==MPI_UNSIGNED)) 
			rc = sizeof (int);

		else if ((datatype==MPI_LONG)||(datatype==MPI_UNSIGNED_LONG))
			rc = sizeof (long);

		else if (datatype==MPI_FLOAT)
			rc = sizeof (float);

		else if (datatype==MPI_DOUBLE)
			rc = sizeof (double);

			/* note no complex types in C but they exist in Fortran */
		else {
			fprintf(stderr,"PVMPI unknown datatype in send/pack operation %d.\n",
							datatype);
			return (MPI_ERR_TYPE);
		}

		/* now for a problem part */

		rc *= count;

#ifdef VERBOSE
printf("Datatype [%d] count [%d] = [%d] bytes\n", (int) datatype, count, rc);
fflush(stdout);
#endif

		/* I think 0 length is a valid mesage length :) */
		if (rc >= 0) return rc;
		else 
			return (MPI_ERR_OTHER);
}

