
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	These calls interface to the communication and process state monitors 
*/

#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ftmpi_mon.h"
#include <stdio.h>
#include <stdlib.h>

void	findcominfomonitor ()
{
  int id;
  int aval;
  int who;
  char rname [256];
  int i1, i2;
  int e[1];
  int a[1];
  int b[1];
  int r;

  sprintf(rname, "FT-MPI:Monitor:cominfo");
  if (!ns_init_called) {
    printf("LOOKING FOR MONITOR INFO at %s %d\n",getenv("HARNESS_NS_HOST"),atoi(getenv("HARNESS_NS_HOST_PORT")));
    fflush(stdout);
    ns_init(getenv("HARNESS_NS_HOST"),atoi(getenv("HARNESS_NS_HOST_PORT")));
    ns_init_called = 1;
  }

  r = ns_info (rname, 1, &i1, &i2, e, a, b);

  if (r!=1) {
    printf ("Ops sorry no communicator monitor available.\n");
    fflush(stdout);
    cominfoid = -1;
    cominfoid_who = -1;
    id = -1; /* fake */
    aval = 0; /* fake */
  }
  else {
    id = 1; /* fake */
    aval = 1; /* fake */
    cominfoid = a[0];
    cominfoid_port = b[0];
    cominfoid_who = -1;
  }
}

/************************************************************************************/

void	monitor_register ()
{
  int sbuf;
  int mygid;
  int tag;
  int ret;

  mygid = coms[0].my_gid;

  printf("REGISTERING MONITOR \n");
  fflush(stdout);
  
  if (cominfoid==-1) {
/*     printf("Cannot register to unknown communicator monitor!\n"); */
    return;
  }
  
  if (cominfoid_who!=-1) {
/*     printf("Someone else is already registered to the communicator monitor!\n"); */
    return;
  }
  
  printf("CREATING CONNECTION TO %x %d\n",cominfoid,cominfoid_port);
  fflush(stdout);
  cominfoid_sock = getconn_addr (cominfoid, &cominfoid_port, 10);	
  if (cominfoid_sock<0) {
/*     printf("Cannot connect to monitor at addr [%d] port [%d]\n", cominfoid, cominfoid_port); */
    fflush (stdout);
    cominfoid_sock = -1;
    return ;
  }

/* 	sbuf = get_msg_buf (0); */


  tag = CONNECT;
  printf("SENDING %d %d\n",mygid,tag);
  ret = send_pkmesg (cominfoid_sock, mygid, 1, &tag, EMPTYMSGBUF, 0);

  printf("SEND_PKMESG returned %d\n",ret);
  fflush(stdout);

  printf("Sent register message from 0x%x to 0x%x\n", mygid, cominfoid);
  cominfoid_who = mygid;
}

/************************************************************************************/

void	monitor_new_com (c)
int c;
{
  int sbuf;
  int mygid;
  int i;
  char comname[256];
  int tag;
  int ret;

  printf("MONITOR NEW COM %d\n",c);

  mygid = coms[0].my_gid;
  if (cominfoid==-1) {
/*     printf("Cannot register to unknown communicator monitor!\n"); */
    return;
  }


/* 	if (cominfoid_who!=mygid) { */
/* 		printf("Someone else is already registered to the communicator monitor!\n"); */
/* 		printf("Me 0x%x them 0x%x\n", mygid, cominfoid_who); */
/* 		return; */
/* 		} */

  sbuf = get_msg_buf (0);
  if (!c){
    sprintf(comname, "MPI_COMM_WORLD");
  }
  else {
    sprintf(comname,"Comm: %d ", c);
  }

  i = pk_int32 (sbuf, &c, 1);
  i = pk_int32 (sbuf, &coms[c].currentsize , 1);
  i = pk_int32 (sbuf, &coms[c].nprocs , 1);
  i = pk_int32 (sbuf, &coms[c].ft_com_state , 1);
  i = pk_string (sbuf,comname);
  printf("INFO %d %d %d %d %s\n",c,coms[c].currentsize,coms[c].nprocs,coms[c].ft_com_state,comname);

  for (i=0;i<coms[c].currentsize;i++) {
    pk_int32 (sbuf, &(coms[c].proc_gids[i]), 1);
    pk_int32 (sbuf, &(coms[c].proc_ft_states[i]), 1);
    printf("PROCESS INFO %d -- %d %d\n",i,coms[c].proc_gids[i],coms[c].proc_ft_states[i]);
  }

  tag = NEWCOMM;

  printf("SEND_PKMESG %d %d\n",mygid,tag);
  fflush(stdout);

  ret = send_pkmesg (cominfoid_sock, mygid, 1, &tag, sbuf, 1);
  
  printf("SEND_PKMESG returnd %d\n",ret);
  fflush(stdout);

  printf("Sent new communicator message from 0x%x to 0x%x about [%s] of size %d\n", mygid, cominfoid, comname, coms[c].currentsize);
}

/************************************************************************************/

void	monitor_com_state (c)
int c;
{
int sbuf;
int mygid;
int i;
int comname[256];
int tag;

	mygid = coms[0].my_gid;

	if (cominfoid==-1) {
/* 		printf("Cannot register to unknown communicator monitor!\n"); */
		return;
		}

	if (cominfoid_who!=mygid) {
/* 		printf("Someone else is already registered to the communicator monitor!\n"); */
/* 		printf("Me 0x%x them 0x%x\n", mygid, cominfoid_who); */
		return;
		}

	/* else all is ok */
	sbuf = get_msg_buf (0);

	/* first pack the comm header info */
	i = pk_int32 (sbuf, &c, 1);
	i = pk_int32 (sbuf, &coms[c].currentsize , 1);
	i = pk_int32 (sbuf, &coms[c].nprocs , 1);
	i = pk_int32 (sbuf, &coms[c].ft_com_state , 1);

	tag = COMSTATE;
	send_pkmesg (cominfoid_sock, mygid, 1, &tag, sbuf, 1);


/* 	printf("Sent communicator change of state message from 0x%x to 0x%x about [%s] \n", mygid, cominfoid, comname ); */
  usleep(tb_monitor_sleep_var);
}

/************************************************************************************/

void	monitor_proc_state (c, r)
int c;
int r;
{
int sbuf;
int mygid;
int i;
int comname[256];
int tag;

	mygid = coms[0].my_gid;

	if (cominfoid==-1) {
/* 		printf("Cannot register to unknown communicator monitor!\n"); */
		return;
		}

	if (cominfoid_who!=mygid) {
/* 		printf("Someone else is already registered to the communicator monitor!\n"); */
/* 		printf("Me 0x%x them 0x%x\n", mygid, cominfoid_who); */
		return;
		}

	/* else all is ok */
	sbuf = get_msg_buf (0);

	/* first pack the comm header info */
	i = pk_int32 (sbuf, &c, 1);
	i = pk_int32 (sbuf, &r, 1);	/* rank */

	/* should we send the id instead so that we can do a single */
	/* update at the other end? */
	/* in which case we wouldn't need the cid sent either :) */

	pk_int32 (sbuf, &coms[c].proc_ft_states[r], 1);	/* the proc state */

	tag = PROCSTATE;
	send_pkmesg (cominfoid_sock, mygid, 1, &tag, sbuf, 1);

/* 	printf("Sent proc change of state message from 0x%x to 0x%x about [%d:%d] \n", mygid, cominfoid, c, r ); */
  usleep(tb_monitor_sleep_var);

}

/************************************************************************************/

void	monitor_com_update (c)
int c;
{
int sbuf;
int mygid;
int i;
int comname[256];
int tag;

	mygid = coms[0].my_gid;

	if (cominfoid==-1) {
/* 		printf("Cannot register to unknown communicator monitor!\n"); */
		return;
		}

	if (cominfoid_who!=mygid) {
/* 		printf("Someone else is already registered to the communicator monitor!\n"); */
/* 		printf("Me 0x%x them 0x%x\n", mygid, cominfoid_who); */
		return;
	}
	/* else all is ok */
	sbuf = get_msg_buf (0);

	/* first pack the comm header info */
	i = pk_int32 (sbuf, &c, 1);
	i = pk_int32 (sbuf, &coms[c].currentsize , 1);
	i = pk_int32 (sbuf, &coms[c].nprocs , 1);
	i = pk_int32 (sbuf, &coms[c].ft_com_state , 1);
	i = pk_string (sbuf,comname);

	for (i=0;i<coms[c].currentsize;i++) {
		pk_int32 (sbuf, &(coms[c].proc_gids[i]), 1);
		pk_int32 (sbuf, &(coms[c].proc_ft_states[i]), 1);
/*     printf("PROC %d -- %d\n",coms[c].proc_gids[i],coms[c].proc_ft_states[i]); */
	}

	tag = UPDATECOM;
	send_pkmesg (cominfoid_sock, mygid, 1, &tag, sbuf, 1);

/* 	printf("Sent update communicator message from 0x%x to 0x%x about [%s] of size %d\n", mygid, cominfoid, comname, coms[c].currentsize); */
  usleep(tb_monitor_sleep_var);

}

void monitor_finalize()
{
  int tag;
  int mygid;
  int ret;


  mygid = coms[0].my_gid;

	if (cominfoid_who==mygid) {
    tag = FINALIZE;
    printf("SENDING %d %d\n",mygid,tag);
    ret = send_pkmesg (cominfoid_sock, mygid, 1, &tag, EMPTYMSGBUF, 0);
  }
}

/************************************************************************************/
