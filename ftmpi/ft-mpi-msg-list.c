
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



/* MPI includes */
#include <mpi.h>


/* local includes */
#ifndef LISTSH
#include "ft-mpi-list.h"
#endif

#ifndef MSGLISTH
#include "ft-mpi-msg-list.h"
#endif


/* Typical PVM setup */
#ifdef HASSTDLIB
#include <stdlib.h>
#endif

#if defined(SYSVBFUNC)
#include <memory.h>
#define BZERO(d,n)      memset(d,0,n)
#define BCMP(s,d,n)     memcmp(d,s,n)
#define BCOPY(s,d,n)    memcpy(d,s,n)

#else
#define BZERO(d,n)      bzero(d,n)
#define BCMP(s,d,n)     bcmp(s,d,n)
#define BCOPY(s,d,n)    bcopy(s,d,n)
#endif

#include <stdio.h>	/* just for the dump instructions */
#include <string.h> /* needed for strcmp when checking local,remote names */
#include "debug.h"

/* struct must be of this form */
/*
 * struct type {
 *		struct type *t_link, *t_rlink;
 *		int t_data0;
 *		int t_data1;
 *        .
 *		int t_dataN;
 * };
 */

/* start data lists off this way */
/* struct msg_list *ml_head = 0; */
/* static int current_top_key = 0; */

/* the structures used here, contain an empty first element that links to */
/* itself. */


/* we need to return this first element so that it can be added to the */
/* communicator (com_list) that owns it */

struct msg_list *
msg_list_init()
{
struct msg_list *first;

	first = TALLOC(1, struct msg_list, 0);
	first->ml_link = first->ml_rlink = first; /* all points to me */
	return (first);
}

struct msg_list *
msg_list_tail (ml_head)
struct msg_list *ml_head;
{
	if (ml_head->ml_rlink != ml_head) return (ml_head->ml_rlink);
	else return ((struct msg_list *)0);
}

struct msg_list *
msg_list_head (ml_head)
struct msg_list *ml_head;
{
	if (ml_head->ml_link != ml_head) return (ml_head->ml_link);
	else return ((struct msg_list *)0);
}


/*	msg_list_new()
*
*	Make a new msg_list descriptor, adds to list at the tail.
* This makes sure that message order is preserved.
* Returns the pointer to the new item so that it can be updated/filled in 
*
*/

struct msg_list *
msg_list_new (ml_head)
struct msg_list *ml_head;
{
	struct msg_list *mlp;

	if (!(mlp = TALLOC(1, struct msg_list, 0))) {
		fprintf(stderr, "msg_list_new() can't get memory\n");
		exit(1);
	}
	LISTPUTBEFORE(ml_head, mlp, ml_link, ml_rlink);

	return mlp;
}

/*	msg_list_new_element_only()
*
*	Make a new msg_list descriptor, 
* Returns the pointer to the new item so that it can be updated/filled in 
* New element is NOT on the message list and has to be added later!
*
*/

struct msg_list *
msg_list_new_element_only ()
{
	struct msg_list *mlp;

	if (!(mlp = TALLOC(1, struct msg_list, 0))) {
		fprintf(stderr, "msg_list_new_element_only() can't get memory\n");
		exit(1);
	}

	return mlp;
}

/*	msg_list_add_2tail()
*
* adds msg to list at the tail.
* This makes sure that message order is preserved.
*
*/

void
msg_list_add_2tail (ml_head, mlp)
struct msg_list *ml_head;
struct msg_list *mlp;
{
	LISTPUTBEFORE(ml_head, mlp, ml_link, ml_rlink);
}

/*
* Note here we don't change the msgs in list counter. This has to be done by
* the calling routine as it will have access to this (being as its in the
* com_list structures anyway).
*
*/
void
msg_list_free(mlp)
	struct msg_list *mlp;
{
	LISTDELETE(mlp, ml_link, ml_rlink);
	FREE(mlp);
}

/* free element thats not on a list.. */
void
msg_list_free_unattached (mlp)
	struct msg_list *mlp;
{
/* 	LISTDELETE(mlp, ml_link, ml_rlink); */
	FREE(mlp);
}

/* detach an element from a list */
void
msg_list_detach(mlp)
	struct msg_list *mlp;
{
	LISTDELETE(mlp, ml_link, ml_rlink);
	mlp->ml_link = mlp->ml_rlink = NULL;
}

/* frees all but empty head holder element */
void msg_list_free_all (struct msg_list *ml_head)
{
  struct msg_list *mlp;
  
  if (!ml_head) return;	/* if bad list return rather than deadlock */
  
  /* loop taking the head and freeing it, when no head, return */
  while (1) {
    mlp = msg_list_head (ml_head);
    if (mlp) msg_list_free (mlp);
    else 
      return;
  }
}


void msg_list_destroy (ml_head)
struct msg_list *ml_head;
{
  struct msg_list *mlp;
  
  if (!ml_head) return;	/* if bad list return rather than deadlock */
  
  /* loop taking the head and freeing it, when no head, throw empty */
  /* record place holder away as well and then return */
  while (1) {
    mlp = msg_list_head (ml_head);
    if (mlp) msg_list_free (mlp);
    else {
      _FREE((char *) ml_head);
      return;
    }	
  }
}



/*	msg_list_find_by_header (head, rank, tag)
*
*	Find a msg by the recv tag and senders rank as required.
* Handles wildcards correctly. (Although this should be done higher up
* to save on stack push/pulls and a few ifs + cache misses etc etc)
*
*/

struct msg_list *
msg_list_find_by_header (ml_head, req_rank, req_tag)
struct msg_list * ml_head;
	int req_rank;
	int req_tag;
{
	struct msg_list *mlp;
	
	if((req_tag==MPI_ANY_TAG)&&(req_rank==MPI_ANY_SOURCE)) 	/* take off head */
		return( msg_list_head(ml_head) );

	if(req_tag==MPI_ANY_TAG) 	/* then search my source/rank */
		for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link)
				if (mlp->ml_senders_rank==req_rank) return mlp;

	if(req_rank==MPI_ANY_SOURCE) 	/* then search my tag */
		for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link)
				if (mlp->ml_mpi_tag==req_tag) return mlp;

	/* else just pattern match on both.. this should be neater TODO[] */
	for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link)
		if ((mlp->ml_mpi_tag==req_tag)&&(mlp->ml_senders_rank==req_rank))
				return mlp;
	
	/* if none of them, then ops! */	
	return (struct msg_list*)0;	/* no match, sorry */
}


/* find a message by a communicator ID */
/* used to move data from the system to unexp Q on a com create or */
/* used to remove data or relabel it on a recovery */
struct msg_list *
msg_list_find_by_comm (ml_head, req_comm)
struct msg_list * ml_head;
	int req_comm;
{
	struct msg_list *mlp;
	
	/* else just pattern match on both.. this should be neater TODO[] */
	for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link)
		if (mlp->ml_mpi_comm==req_comm)
				return mlp;
	
	/* if none of them, then ops! */	
	return (struct msg_list*)0;	/* no match, sorry */
}


/*
* This routine is used to find messages by their msg_id.
* Cancelling messages is an example of this routines use.
* Note multiple msgs may have the same id if send from different ranks
* thus we require the senders rank as well.
*
*/
struct msg_list *
msg_list_find_by_msg_id (ml_head, req_id, req_rank)
struct msg_list * ml_head;
	int req_id;
	int req_rank;
{
	struct msg_list *mlp;
	
	for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link)
		if((req_id==mlp->ml_msg_id)&&(req_rank==mlp->ml_senders_rank))
			return( mlp );

	/* if none of them, then ops! */	
	return (struct msg_list*)0;	/* no match, sorry */
}

void
msg_list_dump(ml_head)
struct msg_list * ml_head;
{
	struct msg_list *mlp;

	for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link) {
		printf("Senders rank %d Tag %d Message Type %d Proto %d Msg_Id %d\n",
				mlp->ml_senders_rank, mlp->ml_mpi_tag, mlp->ml_msg_type,
				mlp->ml_prot_type, mlp->ml_msg_id );
		}

}

void
msg_list_dump_compact (char *comment, struct msg_list *ml_head, int expcount)
{
	struct msg_list *mlp;
	int cnt=0;

	printf("msg_dump of [%s] expected count %d\n", comment, expcount);	
	for (mlp = ml_head->ml_link; mlp != ml_head; mlp = mlp->ml_link) {
		printf("%3d Com %2d gid %4d Tag %d Type %d\n",
				cnt, 
				mlp->ml_mpi_comm,
				mlp->ml_senders_rank, mlp->ml_mpi_tag, 
				mlp->ml_msg_type);
		cnt++;
		}
	  if (cnt!=expcount)
		      printf("WARNING msg_list dump of [%s] expected count %d NOT actual %d\n",
					           comment, expcount, cnt);
	  fflush(stdout);
}



/* fast function that just return true if it matches the header */

int msg_hdr_match (req_rank, req_tag, sender, tag) 
int req_rank, req_tag, sender, tag;

{

	if((req_tag==MPI_ANY_TAG)&&(req_rank==MPI_ANY_SOURCE)) return(1);

	if((req_tag==MPI_ANY_TAG)&&(req_rank==sender)) return (1);

	if((req_rank==MPI_ANY_SOURCE)&&(req_tag==tag)) return (1);

	if((req_rank==sender)&&(req_tag==tag)) return (1);

	return (0);
}
	
