/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include "ft-mpi-f2c-file.h"
#include "ft-mpi-fprot.h"

#define MPI_File int
#define MPI_File_errhandler_fn int
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_close(MPI_File * fh) */
/* void mpi_file_close_(int * fh,int * __ierr)
{
  *__ierr = MPI_File_close((MPI_File *)ToPtr(fh));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_create_errhandler(MPI_File_errhandler_fn *function,
   MPI_Errhandler *errhandler) */
/* void mpi_file_create_errhandler_(int *func,int *errh,int * __ierr)
{
  *__ierr = MPI_File_create_errhandler((MPI_File_errhandler_fn *)ToPtr(func),
				       (MPI_Errhandler *)ToPtr(errh));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_delete(char * filename,MPI_Info info) */
/* void mpi_file_delete_(char * filename,int * info,int * __ierr)
{
  *__ierr = MPI_File_delete((char *)ToPtr(filename),(MPI_Info)(*((int *)info)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_amode(MPI_File fh,int * amode) */
/* void mpi_file_get_amode_(int * fh,int * amode,int * __ierr)
{
  *__ierr = MPI_File_get_amode((MPI_File)(*((int *)fh)),(int *)ToPtr(amode));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_errhandler(MPI_File file,MPI_Errhandler *errhandler) */
/* void mpi_file_get_errhandler_(int * file,int * errhandler,int * __ierr)
{
  *__ierr = MPI_File_get_errhandler((MPI_File)(*((int *)file)),
				    (MPI_Errhandler *)ToPtr(errhandler));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_group(MPI_File fh,MPI_Group *group) */
/* void mpi_file_get_group_(int * fh,int * group,int * __ierr)
{
  *__ierr = MPI_File_get_group((MPI_File)(*((int *)fh)),
			       (MPI_Group *)ToPtr(group));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_info(MPI_File fh,MPI_Info * info_used) */
/*void mpi_file_get_info_(int * fh,int * info_used,int * __ierr)
{
  *__ierr = MPI_File_get_info((MPI_File)(*((int *)fh)),
			      (MPI_Info *)ToPtr(info_used));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_size(MPI_File fh,MPI_Offset size) */
/*void mpi_file_get_size_(int * fh,int * size,int * __ierr)
{
  *__ierr = MPI_File_get_size((MPI_File)(*((int *)fh)),
			      (MPI_Offset)(*((int *)size)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_get_view(MPI_File fh,MPI_Offset *disp,MPI_Datatype *etype,
   MPI_Datatype *filetype,char * datarep) */
/*void mpi_file_get_view_(int * fh,int *disp,int * etype,int * filetype,
			char * datarep,int * __ierr)
{
  *__ierr = MPI_File_get_view((MPI_File)(*((int *)fh)),
			      (MPI_Offset *)ToPtr(disp),
			      (MPI_Datatype *)ToPtr(etype),
			      (MPI_Datatype *)ToPtr(filetype),
			      (char *)ToPtr(datarep));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_iread_at(MPI_File fh,MPI_Offset offset,void *buf,int cnt,
   MPI_Datatype ddt,MPI_Request * request) */
/*void mpi_file_iread_at_(int *fh,int *offset,void *buf,int *cnt,int *ddt,
			int * request,int * __ierr)
{
  *__ierr = MPI_File_iread_at((MPI_File)(*((int *)fh)),
			      (MPI_Offset)(*((int *)offset)),
			      (void *)ToPtr(buf),(*((int *)cnt)),
			      (MPI_Datatype)fdt2c((*((int *)ddt))),
			      (MPI_Request *)ToPtr( request));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_iwrite_at(MPI_File fh,MPI_Offset offset,void *buf,int cnt,
   MPI_Datatype ddt,MPI_Request * request) */
/*void mpi_file_iwrite_at_(int * fh,int * offset,void * buf,int * cnt,
			 int * ddt,int * request,int * __ierr)
{
  *__ierr = MPI_File_iwrite_at((MPI_File)(*((int *)fh)),
			       (MPI_Offset )(*((int *)offset)),
			       (void *)ToPtr(buf),(*((int *)cnt)),
			       (MPI_Datatype )fdt2c((*((int *)ddt))),
			       (MPI_Request *)ToPtr(request));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_open(MPI_Comm comm,char *filename,int amode,MPI_Info info,
   MPI_File *fh) */
/*void mpi_file_open_(int * comm,char * filename,int * amode,int * info,
		    int * fh,int * __ierr)
{
  *__ierr = MPI_File_open((MPI_Comm)(*((int *)comm)),(char *)ToPtr(filename),
			  (*((int *)amode)),(MPI_Info)(*((int *)info)),
			  (MPI_File *)ToPtr(fh));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_read_at_all(MPI_File fh,MPI_Offset offset,void *buf,
   int cnt,MPI_Datatype ddt,MPI_Status * status) */
/*void mpi_file_read_at_all_(int * fh,int * offset,void *buf,int * cnt,
			   int * ddt,int * status,int * __ierr)
{
  *__ierr = MPI_File_read_at_all((MPI_File)(*((int *)fh)),
				 (MPI_Offset)(*((int *)offset)),
				 (void *)ToPtr(buf),(*((int *)cnt)),
				 (MPI_Datatype)fdt2c((*((int *)ddt))),
				 (MPI_Status *)ToPtr(status));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_read_at(MPI_File fh,MPI_Offset offset,void *buf,int cnt,
   MPI_Datatype ddt,MPI_Status * status) */
/*void mpi_file_read_at_(int *fh,int *offset,void *buf,int *cnt,int *ddt,
		       int * status,int * __ierr)
{
  *__ierr = MPI_File_read_at((MPI_File)(*((int *)fh)),
			     (MPI_Offset)(*((int *)offset)),
			     (void *)ToPtr(buf),(*((int *)cnt)),
			     (MPI_Datatype)fdt2c((*((int *)ddt))),
			     (MPI_Status *)ToPtr( status));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_set_errhandler(MPI_File fh,MPI_Errhandler errhandler) */
/*void mpi_file_set_errhandler_(int * fh,int * errhandler,int * __ierr)
{
  *__ierr = MPI_File_set_errhandler((MPI_File)(*((int *)fh)),
				    (MPI_Errhandler)(*((int *)errhandler)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_set_info(MPI_File fh,MPI_Info info) */
/*void mpi_file_set_info_(int * fh,int * info,int * __ierr)
{
  *__ierr = MPI_File_set_info((MPI_File)(*((int *)fh)),
			      (MPI_Info)(*((int *)info)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_set_size(MPI_File fh,MPI_Offset size) */
/*void mpi_file_set_size_(int * fh,int * size,int * __ierr)
{
  *__ierr = MPI_File_set_size((MPI_File)(*((int *)fh)),
			      (MPI_Offset)(*((int *)size)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_set_view(MPI_File fh,MPI_Offset disp,MPI_Datatype etype,
   MPI_Datatype filetype,char * datarep,MPI_Info info) */
/*void mpi_file_set_view_(int *fh,int *disp,int *etype,int *filetype,
			char * datarep,int * info,int * __ierr)
{
  *__ierr = MPI_File_set_view((MPI_File)(*((int *)fh)),
			      (MPI_Offset)(*((int *)disp)),
			      (MPI_Datatype)(*((int *)etype)),
			      (MPI_Datatype)(*((int *)filetype)),
			      (char *)ToPtr(datarep),
			      (MPI_Info)(*((int *)info)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_sync(MPI_File fh) */
/*void mpi_file_sync_(int * fh,int * __ierr)
{
  *__ierr = MPI_File_sync((MPI_File)(*((int *)fh)));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_write_at_all(MPI_File fh,MPI_Offset offset,void *buf,
   int cnt,MPI_Datatype ddt,MPI_Status * status) */
/*void mpi_file_write_at_all_(int * fh,int * offset,void *buf,int * cnt,
			    int * ddt,int * status,int * __ierr)
{
  *__ierr = MPI_File_write_at_all((MPI_File)(*((int *)fh)),
				  (MPI_Offset)(*((int *)offset)),
				  (void *)ToPtr(buf),(*((int *)cnt)),
				  (MPI_Datatype)fdt2c((*((int *)ddt))),
				  (MPI_Status *)ToPtr(status));
}
*/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_File_write_at(MPI_File fh,MPI_Offset offset,void *buf,int cnt,
   MPI_Datatype ddt,MPI_Status * status) */
/*void mpi_file_write_at_(int *fh,int *offset,void *buf,int *cnt,int *ddt,
			int * status,int * __ierr)
{
  *__ierr = MPI_File_write_at((MPI_File)(*((int *)fh)),
			      (MPI_Offset)(*((int *)offset)),
			      (void *)ToPtr(buf),(*((int *)cnt)),
			      (MPI_Datatype)fdt2c((*((int *)ddt))),
			      (MPI_Status *)ToPtr(status));
}
*/
