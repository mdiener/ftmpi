
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>
 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 These are the low level system interface for MPI groups.
 */


#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-group.h"
#include "debug.h"

/* group_t group[MAXGROUPS]; */	/* just for now a fixed array */
/*int groupfree = MAXGROUPS; */

group_t *_ftmpi_group=NULL;

int MAXGROUPS=20;

static int group_size        = 20;
static int group_blocksize   = 20;
static int group_initialized = 0;


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* get the group structure by handle and return ptr to it */
group_t * ftmpi_group_get_ptr (int i)
{

  if (i==MPI_GROUP_NULL) return (NULL);
  if ((i<0)||(i>=MAXGROUPS)) return (NULL);
  if (_ftmpi_group[i].group== MPI_GROUP_NULL) return (NULL);
  return (&_ftmpi_group[i]);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* get a raw GID from the group data structure */
/* we only do simple checks */
int ftmpi_group_gid (MPI_Group grp, int rank)
{
  int rc;
  rc = MPI_UNDEFINED;
  
  if (grp==MPI_GROUP_NULL) return (rc);
  if ((grp<0)||(grp>=MAXGROUPS)) return (rc);
  if (_ftmpi_group[grp].group== MPI_GROUP_NULL) return (rc);
  if ((rank<0)||(rank>=MAXPERAPP)) return (rc);

  if ( rank >= _ftmpi_group[grp].currentsize ) return ( rc );

  return (_ftmpi_group[grp].gids[rank]);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* return MPI_SUCCESS if group is accessable via handle */
/* used as a check in the MPI group API calls only */
int ftmpi_group_ok (MPI_Group grp)
{
  if ( grp == MPI_GROUP_EMPTY) return ( MPI_SUCCESS);
  
  if (grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ( (grp<0) || (grp>=MAXGROUPS)) return (MPI_ERR_GROUP);
  if (_ftmpi_group[grp].group==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[grp].in_use ) return ( MPI_ERR_GROUP );

return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
group_t* ftmpi_group_init (group_t *g) 
{
  int i;

  g = (group_t *)_MALLOC( group_size * sizeof(group_t));
  if ( g == NULL ) return ( NULL );

  memset ( (void* ) g, 0, group_size * sizeof(group_t));

  for(i=0;i<group_size;i++) 
    {
      /* just need to set the values, which are not 0 */
      g[i].group   = MPI_GROUP_NULL;
      g[i].my_rank = MPI_UNDEFINED;
    }

  /*  groupfree = MAXGROUPS; */
  return ( g );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_group_clr_all_but_world () 
{
  int i;

  if ( !group_initialized )
    {
      _ftmpi_group = ftmpi_group_init ( _ftmpi_group );
      if ( _ftmpi_group == NULL ) return ( MPI_ERR_INTERN );
      group_initialized = 1;
      MAXGROUPS = group_size;
    }

  /* check all groups unless MPI_GROUP_WORLD and MPI_GROUP_SELF */
  for(i=0;i<MAXGROUPS;i++) 
    if ((_ftmpi_group[i].group!=MPI_GROUP_NULL)&&
	((i!=MPI_GROUP_WORLD) || (i!=MPI_GROUP_SELF)))
      ftmpi_group_clr (i);
  
 return ( MPI_SUCCESS ); 
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* clear a single grp */
int ftmpi_group_clr (int i) 
{
  int j;

  /* Not needed. EG March 25 2003 */
  /* if it was in use and is now freed inc free count */
  /*  if (_ftmpi_group[i].group!= MPI_GROUP_NULL) groupfree++; */

  /* if it had been in use, was then freed and now can be released inc cnt */
  /*  if ((_ftmpi_group[i].group==MPI_GROUP_NULL)&&(_ftmpi_group[i].freed==1)) 
  **    groupfree++; 
  */
	
  _ftmpi_group[i].refcnt = 0;
  _ftmpi_group[i].in_use = 0;
  
  _ftmpi_group[i].group = MPI_GROUP_NULL;
  _ftmpi_group[i].freed = 0;	
  _ftmpi_group[i].maxsize =0;
  _ftmpi_group[i].currentsize = 0;
  _ftmpi_group[i].nprocs = 0;
  _ftmpi_group[i].my_rank = MPI_UNDEFINED;
  _ftmpi_group[i].my_gid = 0;
  for (j=0;j<MAXPERAPP;j++) {
    _ftmpi_group[i].gids[j] = 0;
  }

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
 This routine finds a free slot for a group 
 after this you could do a group build
*/
int ftmpi_group_get_free (int *newgrp)
{
  int i, found = 0;
  int oldsize;
  int ret = -1;  /* just to keep the compilers happy */
  group_t *ng=NULL;
  group_t *dummy;

  if ( !group_initialized )
    {
      _ftmpi_group = ftmpi_group_init ( _ftmpi_group );
      if ( _ftmpi_group == NULL ) return ( MPI_ERR_INTERN );
      group_initialized = 1;
      MAXGROUPS = group_size;
    }
  
  /* Search next free element; if no free element found, increase*/
  /* the size of the array */
  for ( i = 0; i < group_size; i ++ )
    {
      if ( !_ftmpi_group[i].in_use )
	{
	  _ftmpi_group[i].in_use = 1;
	  found = 1;
	  ret = i;
	  break;
	}
    }

  if ( !found )
    {
      oldsize= group_size;
      group_size += group_blocksize;

      ng = ftmpi_group_init ( ng );
      if ( ng == NULL ) return ( MPI_ERR_INTERN );

      for ( i = 0; i < oldsize; i ++ )
	  ftmpi_group_relocate ( &(ng[i]), &(_ftmpi_group[i]) );
      
      dummy = _ftmpi_group;
      _ftmpi_group = ng;
      ftmpi_group_arrfree ( dummy );
      ret = oldsize + 1;
      MAXGROUPS=group_size;
    }

  /* Now we have definitly a free element in the array; */
  /* Mark it as used, and allocate the according values-field */
  _ftmpi_group[ret].in_use  = 1;
    
  *newgrp = ret;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_group_relocate ( group_t *g1, group_t *g2 )
{
  int i;

  g1->group  = g2->group;
  g1->freed  = g2->freed;
  g1->refcnt = g2->refcnt;
  g1->in_use = g2->in_use;

  g1->maxsize     = g2->maxsize;
  g1->currentsize = g2->currentsize;
  g1->nprocs      = g2->nprocs;
  g1->my_rank     = g2->my_rank;
  g1->my_gid      = g2->my_gid;

  for ( i = 0; i < MAXPERAPP; i ++ )
    g1->gids[i] = g2->gids[i]; 
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_group_arrfree ( group_t *g )
{
  if ( g!=NULL )
    _FREE( g );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* 
 In here we (re)build a particular grp
*/
int ftmpi_group_build (int grp, int gid, int *ids, int ext, int procs)
{
  int i, j, r;
  int m;

#ifdef VERBOSE
  /*
  printf("ftmpi_group_build grp %d gid %d cm %d mm %d ep %d ext %d np %d\n",
	 grp, gid, cm, mm, epoch, ext, procs); 
  fflush(stdout);
  */
#endif

  _ftmpi_group[grp].in_use = 1;
  _ftmpi_group[grp].group = grp;
  _ftmpi_group[grp].freed =0;
  _ftmpi_group[grp].refcnt =0;
  _ftmpi_group[grp].maxsize =ext;
  _ftmpi_group[grp].currentsize = ext;
  _ftmpi_group[grp].nprocs = procs;
  _ftmpi_group[grp].my_gid = gid;
  
  /* now we check for our rank */
  r = MPI_UNDEFINED;
  for (i=0;i<ext;i++) if (ids[i]==gid) { r = i; break; }
  _ftmpi_group[grp].my_rank = r;
  
  /* first clear it */
  for (j=0;j<MAXPERAPP;j++) {
    _ftmpi_group[grp].gids[j] = MPI_UNDEFINED;
  }
  
  /* now copy data into it */
  if (MAXPERAPP>(ext)) m=ext;
  else m = MAXPERAPP;
  for (j=0;j<m;j++) {
    _ftmpi_group[grp].gids[j] = ids[j];
  }

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_group_rank (int grp)
{
  if ( grp == MPI_GROUP_EMPTY ) return ( MPI_UNDEFINED);

  /* printf("grp rank grp%d rank0 %d\n", grp, _ftmpi_group[grp].my_rank); */

  if (grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ((grp<0)||(grp>=MAXGROUPS)) return (MPI_ERR_GROUP);
  if (_ftmpi_group[grp].group==MPI_UNDEFINED) return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[grp].in_use ) return ( MPI_ERR_GROUP );

  return (_ftmpi_group[grp].my_rank);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_group_size (int grp)
{
  if ( !group_initialized ) 
    return (0);

  if ( grp == MPI_GROUP_EMPTY ) return ( 0 );

  /* printf("grp size grp%d size %d\n", grp, _ftmpi_group[grp].currentsize); */
  if (grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ((grp<0)||(grp>=MAXGROUPS)) return (MPI_ERR_GROUP);
  if (_ftmpi_group[grp].group==MPI_UNDEFINED) return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[grp].in_use ) return ( MPI_ERR_GROUP );
  
  return (_ftmpi_group[grp].currentsize);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_group_copy (int grp, int *newgrp)
{
  int n;


  if (grp==MPI_GROUP_NULL) {
    *newgrp = MPI_GROUP_NULL;
    return (MPI_ERR_GROUP);
  }
  if ((grp<0)||(grp>=MAXGROUPS)) {
    *newgrp = MPI_GROUP_NULL;
    return (MPI_ERR_GROUP);
  }

  if (_ftmpi_group[grp].group==MPI_UNDEFINED) {
    *newgrp = MPI_GROUP_NULL;
    return (MPI_ERR_GROUP);
  }

  /* ok, now find empty slot */
  /* that is free to be allocated */
  n = MPI_GROUP_NULL;	/* 0 is a valid group id, but MPI_GROUP_NULL isn't! */

  ftmpi_group_get_free (&n);

  if (n==MPI_GROUP_NULL) 
    {
      /* ops no slots! */
      fprintf(stderr,"FT-MPI:ft-mpi-group:group_copy cannot find memory for "
	      "a new group descriptor!\n");
    *newgrp = MPI_GROUP_NULL;
    return (MPI_ERR_INTERN);
  }
  
  /* ok have a slot so fill it using a previous function :) */
  /* noting that it is an identical copy */
  ftmpi_group_build (n, _ftmpi_group[grp].my_gid, &(_ftmpi_group[grp].gids[0]), 
		     _ftmpi_group[grp].currentsize, _ftmpi_group[grp].nprocs);
  *newgrp = n;

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_group_free (MPI_Group *grp) 
{
  if (grp==NULL) return (MPI_ERR_GROUP);
  if (*grp==MPI_GROUP_WORLD) return (MPI_ERR_GROUP);	/* can not do that */

  if (*grp == MPI_GROUP_EMPTY) return (MPI_SUCCESS); /*nothing to do*/
  
  if (*grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ((*grp<0)||(*grp>=MAXGROUPS))  return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[*grp].in_use ) return ( MPI_ERR_GROUP );

  /* if already freed this would be true for example */
  if (_ftmpi_group[*grp].group==MPI_GROUP_NULL) {
    return (MPI_ERR_GROUP);
  }

  /* ok, we JUST mark it free here unless it really is free */
  if (_ftmpi_group[*grp].refcnt) 
    { 
      /* has pending nb ops on it */

      /* means we can no longer look up on it */
      _ftmpi_group[*grp].group=MPI_GROUP_NULL; 
      _ftmpi_group[*grp].freed = 1;

      /* return null handle so they can't use it :) */
      *grp = MPI_GROUP_NULL;	
      
      /* NOTE, the com calls now have to clear this grp when the ref count */
      /* reaches zero for us! */
    
      return (MPI_SUCCESS);
    }

  /* ok, there are no MPI comunicators using this so we can free it */

  ftmpi_group_clr (*grp);
  return (MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
This is a shorthand routine that checks to see if a group has
been marked as freed but isn't yet as it has pending ops on it 
if it doesn't anymore then free it!
This is usualy called from the com routines when they free a comm 
*/
int ftmpi_group_check_freed (MPI_Group grp)
{
  if (grp==MPI_GROUP_WORLD) return (0);
  if ((grp<0)||(grp>=MAXGROUPS)) return (0);

  if ((_ftmpi_group[grp].freed)&&(!_ftmpi_group[grp].refcnt)) 
    return ftmpi_group_clr (grp);
  return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* This routine handles the ref count for [com] functions */
int ftmpi_group_refinc (MPI_Group grp)
{

  if (grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ((grp<0)||(grp>=MAXGROUPS)) return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[grp].in_use ) return ( MPI_ERR_GROUP );

  /* cannot inc count if being not in use */ 
  if (_ftmpi_group[grp].group==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  
  /* cannot inc count if being freed! */
  if (_ftmpi_group[grp].freed==1) return (MPI_ERR_GROUP);
  
  /* ok, inc my ref count! */
  _ftmpi_group[grp].refcnt++;

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* This routine handles the ref count for [group] functions */
/* In this case we dec the group ref count */
/* if the count reaches zero we check to see if it should be freed! */
int ftmpi_group_refdec (MPI_Group grp)
{
  if (grp==MPI_GROUP_NULL) return (MPI_ERR_GROUP);
  if ((grp<0)||(grp>=MAXGROUPS)) return (MPI_ERR_GROUP);
  if ( !_ftmpi_group[grp].in_use ) return ( MPI_ERR_GROUP );
  
  /* cannot inc count if being not in use unless being freed */
  if ((_ftmpi_group[grp].group==MPI_GROUP_NULL)&&(_ftmpi_group[grp].freed!=1)) 
    return (MPI_ERR_GROUP);
  
  /* ok, dec my ref count! */
  _ftmpi_group[grp].refcnt--;
  
  if (_ftmpi_group[grp].refcnt<0) {
    fprintf(stderr,"FT-MPI:ft-mpi-group:group_refdec ref count [%d] "
	    "below zero!!\n", grp);
    fprintf(stderr,"Please report this to harness@cs.utk.edu\n");
    _ftmpi_group[grp].refcnt=0;
  }

  /* check to see if this should now be removed... */
  if ((!_ftmpi_group[grp].refcnt)&&(_ftmpi_group[grp].freed==1)) 
    ftmpi_group_check_freed (grp);
  
  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int *ftmpi_group_get_gids_ptr ( MPI_Group grp )
{
  if ( group_initialized )
    return ( _ftmpi_group[grp].gids );
  
  return ( NULL );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* debug / testing routines */
int ftmpi_group_display (MPI_Group grp)
{
  int j;

  printf("Group info on group [%d]\n", grp);
  if ((grp<0)||(grp>=MAXGROUPS)) {
    printf("Bad group handle\n");
    return (-1);
  }

  printf("In use handle %d \t Freed? %d\n", (int)_ftmpi_group[grp].group, 
	 _ftmpi_group[grp].freed);
  printf("Sizes. Max %d \tCurrent %d\tnprocs %d\n", _ftmpi_group[grp].maxsize,
	 _ftmpi_group[grp].currentsize, _ftmpi_group[grp].nprocs);
  printf("My rank in group %d\t mygid %d\n", _ftmpi_group[grp].my_rank, 
	 _ftmpi_group[grp].my_gid);
  printf("Group members\n");
  for (j=0;j<_ftmpi_group[grp].nprocs;j++) {
    printf("%d:%d\t", j, _ftmpi_group[grp].gids[j]);
  }
  printf("\n\n");

  return (MPI_SUCCESS);
}





