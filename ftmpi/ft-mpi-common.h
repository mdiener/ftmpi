/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Thara Angskun <angskun@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_COMMON
#define _FT_MPI_H_COMMON

/* this is a polling call that waits/or just polls for a record */
/* this routine can spin, or poll n times and can also break out on an */
/* event on a given socket */
/* handles both table/group and record types */
int ftmpi_poll_for_db_record (int *handle, int myid, int db_type, int index, 
	  int record_type, int arg1, int arg2, int tryloops, int breakonfailure_as); 
/* this is a replacement for the above */
/* this uses a name_service push method */
int ftmpi_wait_for_db_record(int record_type, int arg1, int arg2, 
	  							int lastgen, int breakonerror, int flag);

/* this unregisters the request with the name service if the above call is */
/* not called with flag=0->delete after reading registered */
int ftmpi_unreg_wait_for_db_record(int record_type, int arg1, int arg2);


#endif /*  _FT_MPI_H_COMMON */
