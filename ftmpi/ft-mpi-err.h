/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@Cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#ifndef __FT_MPI_ERR_H__
#define __FT_MPI_ERR_H__

typedef struct {
  int                in_use;       /* is this object marked as currently used ? */
} errhval_info_t;

typedef struct {
  int                 err;         /* key returned with Keyval_create or equiv. functions*/
  int                 in_use;      /* is this element already used ? */
  int                 freed;       /* did the user call keyval_free ? */
  int                 type;        /* FTMPI_ERR_UNKNOWN, FTMPI_ERR_COMM1, FTMPI_ERR_COMM2, */
				   /* FTMPI_ERR_WIN, FTMPI_ERR_FILE */
  /* We keep here only the generic pointer */
  MPI_Handler_function *pfErrorHandler;
  int                 maxvalues;   /* maximum length of the values-array */
  int                 nvalues;     /* number of objects using this keyval */
  errhval_info_t      *values;     /* array of values allocated during the createion of the keyval */
} errh_info_t;

typedef struct{
  int                  val;  /* Errorcode used by the MPI-implementation */
  int            errstrlen;  /* Length of the error string */
  char          errstring[MPI_MAX_ERROR_STRING];  /* Error string attached to this error code */
  int             errclass;  /* Error class to which this code is belonging */
  int           predefined;  /* 1: defined by MPI, 0: defined by user (MPI-2) */
} errcode_info_t;

#define FTMPI_ERRCLASS_MAX_CODES 5

typedef struct {
  int                 val;   /* Error class given to user */
  int          maxerrcode;   /* Maximum number of error-codes in an error-class */
  int            nerrcode;   /* Number of error-codes attached to this error-class */
  int           errcodes[FTMPI_ERRCLASS_MAX_CODES];   /* List of error-codes attached to this class.
				The numbers in this array are the position of the
				codes in the ftmpi_errcodearr, and the their values! */
} errclass_info_t;


/*Constants */

#define FTMPI_COMM_ERRH_BLOCK_SIZE 5 /* how many elements for the attribute-array are 
				       allocated at once ? */

#define FTMPI_ERR_UNKNOWN  -1900
#define FTMPI_ERR_COMM1    -1905
#define FTMPI_ERR_COMM2    -1910
#define FTMPI_ERR_WIN      -1915
#define FTMPI_ERR_FILE     -1920
#define FTMPI_ERR_ANY      -1925


/* Prototypes */
errh_info_t * ftmpi_errh_init ( errh_info_t *e );
int ftmpi_errh_free ( errh_info_t *e);
int ftmpi_errh_copy ( errh_info_t *e1, errh_info_t *e2 );
int ftmpi_errh_get_next_free ( int type );
int ftmpi_errh_mark_free ( int errh );
int ftmpi_errh_test_freed ( int errh );
int ftmpi_errh_release ( int errh );
int ftmpi_errh_set ( int errh, void *handler_fn );
void* ftmpi_errh_get_function ( int errh );
int ftmpi_errh_get_handle ( int errh );
int ftmpi_errh_get_err ( int handle );

int ftmpi_errhval_mark ( int errh, int object);
int ftmpi_errhval_unmark ( int errh, int object );
int ftmpi_errhval_test ( int errh, int object, int type );
int ftmpi_errhval_copy ( int  errh, int object1, int object2 );

void ftmpi_errcode_and_errclass_init ( void);

void ftmpi_errcodes_init (void);
int ftmpi_errcode_get_class (int ecode);
int ftmpi_errcode_get_string (int ecode, char *str, int *len );
int ftmpi_errcode_set_string ( int ecode, char *str );
int ftmpi_errcode_get_next_free ( int errclass, int *errcode );
int ftmpi_errcode_get_handle ( int errcode );
int ftmpi_errcode_test ( int errcode );
void ftmpi_errcode_set_predefined( int ecode );
void ftmpi_errcode_unset_predefined( int ecode );


int ftmpi_err_set_ft_attributes (int numdead, int* procarr);


void ftmpi_errclass_init (void);
int ftmpi_errclass_get_handle ( int errclass );
int ftmpi_errclass_get_next_free ( int *class );
int ftmpi_errclass_add_errcode ( int class, int code );
int ftmpi_errclass_get_value ( int handle );


void ftmpi_errors_return ( MPI_Comm *comm, int *ecode, ...);
void ftmpi_errors_are_fatal ( MPI_Comm *comm, int *ecode, ...);

#endif /* __FT_MPI_ERR_H__ */
