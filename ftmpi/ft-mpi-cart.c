
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
                        Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include <stdio.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-cart.h"
#include "ft-mpi-com.h"
#include "debug.h"

/* we have three classes of functions in this file:
1. Management functions:
   - ftmpi_cart_set 
   - ftmpi_cart_free
   - ftmpi_cart_copy
   - ftmpi_cart_compare
2. Retrieve information functions
   - ftmpi_cart_get_ndims
   - ftmpi_cart_get_dims_ptr
   - ftmpi_cart_get_periods_ptr
3. Functions, where we calculate some values
   - ftmpi_cart_calc_rank
   - ftmpi_cart_calc_coords
   - ftmpi_cart_calc_neighbors
*/


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
cart_info_t* ftmpi_cart_set ( int ndims, int num_ranks, int *dims, 
			      int *periods)
{
  cart_info_t *cart = NULL;
  int i;

  cart = (cart_info_t *)_MALLOC(sizeof(cart_info_t));
  if ( cart == NULL )
    return (NULL);

  cart->nnodes=num_ranks;
  cart->ndims=ndims;
  cart->dims    = (int *)_MALLOC( sizeof(int) * ndims );
  cart->periods = (int *)_MALLOC( sizeof(int) * ndims );
  if ( ((cart->dims == NULL ) || (cart->periods == NULL )) &&
       (ndims != 0 ) )

    return (NULL);

  for(i=0;i<ndims;i++)
    {
      cart->dims[i]=dims[i];
      cart->periods[i]=periods[i];
    }

  return ( cart );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* free the arrays dims and periods first, and finally the 
   whole structure */
void ftmpi_cart_free (cart_info_t *cart)
{
  _FREE( cart->dims );
  _FREE( cart->periods );
  _FREE( cart );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Allocate memory for the new structure, and copy elements */
cart_info_t* ftmpi_cart_copy (cart_info_t *cart) 
{
  cart_info_t *newcart=NULL;

  newcart = ftmpi_cart_set ( cart->ndims, cart->nnodes, cart->dims,
			     cart->periods );
  return (newcart);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* What shall we compare precisly ?
   - number of nnodes (should be alreadu checked for the comm in general)
   - number of dimensions
   - dimensions in each direction
   - settings of the periodic boundary conditions 
*/

int ftmpi_cart_compare (cart_info_t *cart1, cart_info_t *cart2)
{
  int  ret2, ret3;
  int i;
  
  if ( cart1->ndims != cart2->ndims )
    return ( MPI_UNEQUAL ); /* no need to continue */

  ret2 = MPI_IDENT;
  for ( i = 0; i < cart1->ndims; i ++ )
    if ( cart1->dims[i] != cart2->dims[i] )
      {
	ret2 = MPI_UNEQUAL;
	break;
      }

  ret3 = MPI_IDENT;
  for ( i = 0; i < cart1->ndims; i ++ )
    if ( cart1->periods[i] != cart2->periods[i] )
      {
	ret3 = MPI_UNEQUAL;
	break;
      }
  
  /* This is now the most difficult chapter, how to
     set the three parts together to return the correct
     result. I think basically, that as soon as one flag
     doesn't fit, I return MPI_UNEQUAL, and the comm_compare
     routine has to figure out, how to handle that correctly */

  if ( (ret2!=MPI_IDENT) || (ret3 != MPI_IDENT ))
    return (MPI_UNEQUAL);
  else
    return (MPI_IDENT);
      
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_cart_get_ndims ( cart_info_t *cart )
{
  return (cart->ndims );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int* ftmpi_cart_get_dims_ptr ( cart_info_t *cart )
{
  return ( cart->dims );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int* ftmpi_cart_get_periods_ptr ( cart_info_t *cart )
{
  return ( cart->periods );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_cart_calc_rank ( cart_info_t *cart, int *coords )
{
  int rank;
  int *tmp_coords=NULL;
  int i, j, dim, x=0;
  
  tmp_coords = (int *)_MALLOC( (sizeof(int) * cart->ndims ));
  if (tmp_coords == NULL )
    return (MPI_ERR_INTERN);

  for ( i = 0; i < cart->ndims; i++)
    {
      tmp_coords[i] = coords[i];

      /* if we don't have a closed grid in this direction
	 we cannot accept input value higher than the stored
	 one */
      if ( !(cart->periods[i]) && (coords[i]>cart->dims[i] ))
	   return ( MPI_PROC_NULL );
      /* if we do have a closed grid in the given direction,
	 we can accept higher values, we just have to determine
	 which element they really mean */
      if ( (cart->periods[i]) && ( coords[i]>cart->dims[i] ))
	tmp_coords[i] %= cart->dims[i];
      /* more or less the same like above, but what if
	 the given coordinate of the user was negative */
      if ( (cart->periods[i]) && ( tmp_coords[i] < 0 ))
	tmp_coords[i] += cart->dims[i];
    }

  for ( i = 0 ; i < cart->ndims-1; i ++ )
    {
      for ( dim = 1, j = i + 1; j < cart->ndims; j++ )
	dim *= cart->dims[j];

      x += coords[i] * dim;
    }
  
  rank = x + coords[cart->ndims-1];

  _FREE(tmp_coords);

  return ( rank );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_cart_calc_coords ( int *coords, int *dims, int rank, int ndims )
{
  int i, j, divisor;

  for ( i = 0; i < ndims - 1; i++)
    {
      divisor = 1;
      for ( j = i+1; j < ndims; j++)
	divisor *= dims[j];
      
      coords[i] = rank/divisor;
      rank = rank % divisor;
    }

  coords[ndims-1] = rank;

  return ( MPI_SUCCESS);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_cart_calc_neighbors ( int direction, int displ, int *r_source,
				int *r_dest, cart_info_t *cart, int rank )
{
  int *coords;
  int tmp;
  int org_val;
  
  coords = (int *)_MALLOC( sizeof (int) * cart->ndims );
  if ( coords == NULL )
    return ( MPI_ERR_INTERN);

  
  ftmpi_cart_calc_coords (coords, cart->dims, rank, cart->ndims );
  org_val = coords[direction];

  /* right neighbor */
  tmp = coords[direction]+displ;
  
  if ( tmp > cart->dims[direction]-1 )
    {
      if ( cart->periods[direction] )
	{
	  coords[direction] = tmp%cart->dims[direction];
	  *r_dest = ftmpi_cart_calc_rank ( cart, coords );
	}
      else
	*r_dest = MPI_PROC_NULL;
    }
  else
    {
      coords[direction] = tmp;
      *r_dest = ftmpi_cart_calc_rank ( cart, coords );
    }


  /* left neighbor */
  coords[direction] = org_val;
  tmp = coords[direction]-displ;

  if ( tmp < 0 )
    {
      if ( cart->periods[direction] )
	{
	  while ( tmp < 0 )
	      tmp += cart->dims[direction];
	  coords[direction] = tmp;
	  *r_source = ftmpi_cart_calc_rank ( cart, coords );
	}
      else
	*r_source = MPI_PROC_NULL;
    }
  else
    {
      coords[direction] = tmp;
      *r_source = ftmpi_cart_calc_rank ( cart, coords );
    }

  _FREE( coords );

  return ( MPI_SUCCESS );
}









