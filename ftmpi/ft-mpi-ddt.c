/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
 			Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include "mpi.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-attr.h"
#include <rpc/xdr.h>
#include "debug.h"


/* Subroutines implemented in this file: 

   Management routines:
   - ftmpi_ddt_create_list:          allocates memory for the ddt hash-list
   - ftmpi_ddt_add_root:             allocate the add-root struct if required
   - ftmpi_ddt_add_element:          allocate an element-struct for the linked list
   - ftmpi_ddt_get_root:             get the root struct for a ddt
   - ftmpi_ddt_get_element:          get next element-struct in the linked list
   - ftmpi_ddt_init:                 initializes the ddt stuff 
                                     (bdt list, ddt hash table etc.,
                                     defines the basic datatypes).
   - ftmpi_ddt_finalize:             free all structures
   - ftmpi_ddt_remove_all_but_basic: guess what it does :-)
   - ftmpi_ddt_reset_ref_counts:     guess again :-)
   - ftmpi_ddt_increment_dt_uses     increase the ref-count of the dt
   - ftmpi_ddt_decrement_dt_uses     decrease the ref-count of the dt
   - ftmpi_dt_set_size               set's the length for each basic dt
   - ftmpi_dt_set_xdr_size           set's the length of each basic dt in xdr-format
   - ftmpi_ddt_set_xdr_extent

   Routines moving ddt's to contiguous buffers:
   - ftmpi_ddt_write_dt_to_buffer:        compactes a ddt from user mem into a buffer
   - ftmpi_ddt_read_dt_from_buffer:       reads a (compacted) ddt from a buffer to user 
                                          memory
   - ftmpi_ddt_write_dt_to_buffer_block:  writes a ddt to a compact buffer, but
                                          the basic datatypes are sorted (for xdr).
   - ftmpi_ddt_read_dt_from_buffer_block: reads a ddt from a buffer, which is
                                          sorted according to its basic datatypes


   - ftmpi_ddt_get_first_offset           Get the real starting point of the ddt
   - ftmpi_ddt_copy_ddt_to_ddt            copy data described in one ddt into another 
                                          the buffer described by another ddt
   - ftmpi_ddt_get_element_count          used in MPI_Get_elements
   - ftmpi_ddt_check_ddt

   Data conversion routines
   - ftmpi_ddt_xdr_encode_dt_to_buffer:   general data encoding function
   - ftmpi_ddt_xdr_decode_dt_to_buffer:   general data decoding function
   - ftmpi_ddt_xdr_vector_code
   - ftmpi_ddt_xdr_code
   - ftmpi_ddt_decode_size_det
   - ftmpi_ddt_encode_size_det
   - ftmpi_ddt_bs_xdr_det

   - ftmpi_ddt_check_array
   - ftmpi_ddt_write_read_size_det        find out exact size to be sent/recv
   - ftmpi_ddt_get_remote_size


   Attribute caching functions for dt's (not yet in use)
   - ftmpi_type_set_attr                  Attach an attribute to a dt 
   - ftmpi_type_del_attr                  Detach an attribute from a dt

   - ftmpi_ddt_dt_mode
   - ftmpi_ddt_get_bdt_sizes
   - ftmpi_ddt_set_dt_mode
   - ftmpi_mpi_type_xdr_size

   MPI-2 type-decoding functions
   - ftmpi_ddt_set_type
   - ftmpi_ddt_set_args
   - ftmpi_ddt_get_args

   Data conversion for long-double 
   - ftmpi_ddt_convert_long_double
   - ftmpi_ddt_copy_bits
   - dump_memory_order
   - swap_bytes
   - swap_words

*/

/* #define DEBUG_DDT_INIT 1 */

int FTMPI_DDT_B_DT_ADD = 0;
int FTMPI_DDT_MPI_BUFFER_SIZE = 0;
char * FTMPI_DDT_MPI_BUFFER;

FTMPI_DDT_ITEM * _ftmpi_btype[FTMPI_DDT_B_DT_MAX];

FTMPI_DDT_R_ITEM ** FTMPI_DDT_ROOT = NULL;
int FTMPI_DDT_INIT = 0;
int FTMPI_DDT_HASH_SIZE= -1;
int FTMPI_DDT_ROOT_VAL = -1;
int FTMPI_DDT_NEXT_DATATYPE = 500,FTMPI_DDT_INIT_CREATE = 0,
  FTMPI_DDT_DATATYPE_HANDLE,FTMPI_DDT_DATATYPE_BASIC_INFO,
  FTMPI_DDT_B_DATATYPE_TOTAL=0;

static FTMPI_DDT_CODE_INFO ftmpi_ddt_my_longdouble;
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
/* For improving the readability */

#define XTROOT xtemp->root_add
#define TROOT  temp->root_add
#define ALIGN_TO(ptr, type)\
  ((ptr + sizeof(type) - 1) & ~(sizeof(type) - 1))

/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
int ftmpi_ddt_create_list(int hash_size)
{
  int i;

  if(FTMPI_DDT_ROOT ==  NULL){
    if(hash_size <= 0){
      printf("ERROR:  Invalid Hash Size\n");
      return(-1);
    }

#ifdef DEBUG_DDT
    printf("CREATING HASH LIST ...");
#endif
    FTMPI_DDT_ROOT = (FTMPI_DDT_R_ITEM **)_MALLOC(sizeof(FTMPI_DDT_R_ITEM *)*hash_size);
    if ( FTMPI_DDT_ROOT == NULL ) {
      fprintf(stderr, "FTMPI_ddt_create_list: could not allocate memory\n");
      return ( MPI_ERR_INTERN );
    }
#ifdef DEBUG_DDT
    printf("DONE\n");
#endif
    for (i=0;i<hash_size;i++){
      FTMPI_DDT_ROOT[i] = NULL;
    }

    FTMPI_DDT_ROOT_VAL = 460917;
    FTMPI_DDT_HASH_SIZE = hash_size;

    return(FTMPI_DDT_ROOT_VAL);
  }
  else {
    printf("ERROR:  LIST ALREADY EXISTS\n");
    return(-1);
  }
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
FTMPI_DDT_R_ITEM * ftmpi_ddt_add_root(int ddt_id,int add)
{
  int i;
  FTMPI_DDT_R_ITEM * temp = NULL;

  /* first check if the ddt already exists */

  if(ddt_id%FTMPI_DDT_HASH_SIZE < 0){
    printf("ERROR:  INVALID DDT ID\n");
    return(NULL);
  }

  /* element may exist --- need to check */
  temp = FTMPI_DDT_ROOT[ddt_id%FTMPI_DDT_HASH_SIZE];
  if(temp != NULL){
    if(temp->dt == ddt_id){
      printf("ERROR: DDT_ID %d already exists\n",ddt_id);
      return(NULL);
    }
    else {
      while(temp->next != NULL){
        temp = temp->next;
        if(temp->dt == ddt_id){
          printf("ERROR: DDT_ID %d already exists\n",ddt_id);
          return(NULL);
        }
      } 
    }
    

    /* at the last root */
    temp->next = (FTMPI_DDT_R_ITEM*)_MALLOC(sizeof(FTMPI_DDT_R_ITEM));
    temp = temp->next;
    temp->next = NULL;
    if(temp == NULL){
      printf("----- !!! NO MORE MEMORY !!! -----\n");
      exit(-1);
    }
  }
  else {
    /* inserting first root */
    temp = (FTMPI_DDT_R_ITEM*)_MALLOC(sizeof(FTMPI_DDT_R_ITEM));          
    FTMPI_DDT_ROOT[ddt_id%FTMPI_DDT_HASH_SIZE] =  temp;
    temp->next = NULL;
  }

#ifdef DEBUG_DDT
  printf("ADDING A ROOT %d @ %x\n",ddt_id,temp);
#endif


  temp->dt       = ddt_id;
  temp->size     = -999;
  temp->extent   = -999;
  temp->last     = -999;
  temp->hash     = -999;
  temp->commited = 0;
  temp->bs_xdr_count = -1;
  temp->bs_xdr_type  = -1;
  

  /* TROOT is a macro for temp->root_add */
  if(add == 1){
    TROOT =_MALLOC(sizeof(FTMPI_DDT_RA_ITEM));          
    TROOT->first_e = NULL;
    TROOT->last_e  = NULL;
    for(i=0;i<FTMPI_DDT_B_DT_MAX;i++){
      TROOT->type_count[i] = 0;
    }
    TROOT->t_comp = 0;
    memset (TROOT->comp, 0, 
	    sizeof(_ftmpi_comp_t) * FTMPI_DDT_COMP_MAX);

    TROOT->i = NULL;
    TROOT->a = NULL;
    TROOT->d = NULL;
    TROOT->create_type = -1;
    TROOT->ref_cnt     = 1;
  }
  else 
    TROOT = NULL;

  return(temp);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
FTMPI_DDT_E_ITEM * ftmpi_ddt_add_element(FTMPI_DDT_R_ITEM * root,int ddt)
{
  FTMPI_DDT_RA_ITEM *ra;

  if(root == NULL){
    printf("FTMPI: add_element: unkown root for datatype %d\n", ddt);
    return(NULL);
  }

  ra = root->root_add;
  
  if(ra->last_e == NULL){
    /* First element */
    ra->last_e       = (FTMPI_DDT_E_ITEM *)_MALLOC(sizeof(FTMPI_DDT_E_ITEM));
    ra->first_e      = ra->last_e;
    ra->last_e->next = NULL;
    }
  else {
    /* Not first element */
    ra->last_e->next = (FTMPI_DDT_E_ITEM *)_MALLOC(sizeof(FTMPI_DDT_E_ITEM));
    ra->last_e       = ra->last_e->next;
    ra->last_e->next = NULL;
  }


#ifdef DEBUG_DDT
  printf("ADDING A ELEMENT %d @ %x\n",ddt,ra->last_e);
#endif
  ra->last_e->dt      = ddt;
  ra->last_e->size    = -999;
  ra->last_e->extent  = -999;
  ra->last_e->count   = -999;
  ra->last_e->padding = -999;
  return(ra->last_e);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
FTMPI_DDT_R_ITEM * ftmpi_ddt_get_root(int ddt_id)
{
  FTMPI_DDT_R_ITEM * temp;

  if(ddt_id%FTMPI_DDT_HASH_SIZE < 0){
    return(NULL);
  }

  if(FTMPI_DDT_ROOT == NULL){
    return(NULL);
  }

  temp = FTMPI_DDT_ROOT[ddt_id%FTMPI_DDT_HASH_SIZE];
  while(temp != NULL){
    if(temp->dt == ddt_id){
#ifdef DEBUG_DDT
      printf("FOUND ROOT %d @ %x\n",temp->dt,temp);
#endif
      return(temp);
    }
    temp = temp->next;
  }
  return(NULL);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
FTMPI_DDT_E_ITEM * ftmpi_ddt_get_element(FTMPI_DDT_R_ITEM * root,
					 FTMPI_DDT_E_ITEM * last_element)
{
  FTMPI_DDT_RA_ITEM *ra;

  if(root == NULL){
    printf("FTMPI: ddt_get_element: improper datatype root ptr\n");
    return(NULL);
  }

  ra = root->root_add;
  if(last_element == NULL){
#ifdef DEBUG_DDT
    printf("FOUND FIRST element %d @ %x\n",ra->first_e->dt,
	   ra->first_e);
#endif
    return(ra->first_e);
  }
  else {
#ifdef DEBUG_DDT
    if(last_element->next != NULL){
      printf("FOUND NEXT element %d @ %x\n",last_element->next->dt,
	     last_element->next);
    }
#endif
    return(last_element->next);
  }
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
void ftmpi_ddt_init()
{   
  int i;


  if (!FTMPI_DDT_INIT) {
    int block[3],type[3],ret,reth;
    MPI_Aint disp[3];

      ftmpi_convinfo_mysettings(&ftmpi_ddt_my_longdouble.ld_flag);
    ftmpi_ddt_my_longdouble.bs = 0;
    ftmpi_ddt_my_longdouble.mode_l = -1;

    FTMPI_DDT_MPI_BUFFER = (char*)_MALLOC(1024*1);
    FTMPI_DDT_MPI_BUFFER_SIZE = 1024;

    for(i=0;i<FTMPI_DDT_B_DT_MAX;i++){
      _ftmpi_btype[i] = (FTMPI_DDT_ITEM *)_MALLOC(sizeof(FTMPI_DDT_ITEM));
      _ftmpi_btype[i]->size = -1;
      _ftmpi_btype[i]->extent = -1;
      _ftmpi_btype[i]->hash = -1;
    } 
  
    FTMPI_DDT_B_DATATYPE_TOTAL=1;
    for(i=1;i<FTMPI_DDT_B_DT_MAX;i++){
      ftmpi_ddt_set_size ( i, &_ftmpi_btype[i]->size );
      ftmpi_ddt_set_xdr_size ( i, &_ftmpi_btype[i]->xdr_size );
      ftmpi_mpi_type_extent ( i, &_ftmpi_btype[i]->extent );
      ftmpi_ddt_set_xdr_extent ( i, &_ftmpi_btype[i]->xdr_extent );
      _ftmpi_btype[i]->hash = (i+1);
      FTMPI_DDT_B_DATATYPE_TOTAL++;
    }

    /* Set up basic derived datatypes*/

   FTMPI_DDT_INIT = 1;

   /*************************************************************************/
   block[0] = 1;
   block[1] = 1;
   block[2] = 1;

   /* MPI_FLOAT_INT - 500 */
   {
     struct{float a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)(&str[0].a) - (char*)(&str[0]));
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 

     type[0] = MPI_FLOAT;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s 500 - %2d\n","MPI_FLOAT_INT",MPI_FLOAT_INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_DOUBLE_INT - 501 */
   {
     struct{double a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)&str[0].a - (char*)&str[0]);
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 

     type[0] = MPI_DOUBLE;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s %d - %2d\n","MPI_DOUBLE_INT",MPI_DOUBLE_INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_LONG_INT - 502 */
   {
     struct{long a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)&str[0].a - (char*)&str[0]);
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 
     type[0] = MPI_LONG;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s %d - %2d\n","MPI_LONG_INT",MPI_LONG_INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_2INT - 503 */
   {
     struct{int a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)&str[0].a - (char*)&str[0]);
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 
     type[0] = MPI_INT;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s %d - %2d\n","MPI_2INT",MPI_2INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_SHORT_INT - 504 */
   {
     struct{short a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)&str[0].a - (char*)&str[0]);
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 
     type[0] = MPI_SHORT;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s %d - %2d\n","MPI_SHORT_INT",MPI_SHORT_INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_LONG_DOUBLE_INT - 505 */
   {
     struct{long double a;int b;} str[2];
     disp[0] = (MPI_Aint)((char*)&str[0].a - (char*)&str[0]);
     disp[1] = (MPI_Aint)((char*)&str[0].b - (char*)&str[0]);
     disp[2] = (MPI_Aint)((char*)&str[1] - (char*)&str[0]); 
     type[0] = MPI_LONG_DOUBLE;
     type[1] = MPI_INT;
     type[2] = MPI_UB;

     ret = ftmpi_mpi_type_struct(3,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d %2d  -  ",block[0],block[1]); 
     printf("DISP  %2d %2d  -  ",disp[0],disp[1]); 
     printf("TYPE  %2d %2d  ****  ",type[0],type[1]);
     printf("%-22s %d - %2d\n","MPI_LONG_DOUBLE_INT",MPI_LONG_DOUBLE_INT,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /*******************************************************************************/
   /* MPI_2REAL - 506 */
   block[0] = 2;
   {
     disp[0] = 0;
     type[0] = MPI_REAL;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_2REAL",MPI_2REAL,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }


   /* MPI_2DOUBLE_PRECISION - 507 */
   {
     disp[0] = 0;
     type[0] = MPI_DOUBLE_PRECISION;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_2DOUBLE_PRECISION",
	    MPI_2DOUBLE_PRECISION,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_2INTEGER - 508 */
   {
     disp[0] = 0;
     type[0] = MPI_INTEGER;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_2INTEGER",MPI_2INTEGER,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }
   /********************************************************************************/
   /* MPI_COMPLEX - 509 */
   {
     disp[0] = 0;
     type[0] = MPI_REAL;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_COMPLEX",MPI_COMPLEX,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_DOUBLE_COMPLEX - 510 */
   {
     disp[0] = 0;
     type[0] = MPI_DOUBLE_PRECISION;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_DOUBLE_COMPLEX",
	    MPI_DOUBLE_COMPLEX,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }
   
   /********************************************************************************/
   block[0] = 4;
   /* MPI_2COMPLEX - 511 */
   {
     disp[0] = 0;
     type[0] = MPI_REAL;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_2COMPLEX",MPI_2COMPLEX,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }

   /* MPI_2DOUBLE_COMPLEX - 512 */
   {
     disp[0] = 0;
     type[0] = MPI_DOUBLE_PRECISION;

     ret = ftmpi_mpi_type_struct(1,block,disp,type,&reth);
     ftmpi_mpi_type_commit(&reth);
#ifdef DEBUG_DDT_INIT
     printf("BLOCK %2d     -  ",block[0]); 
     printf("DISP  %2d     -  ",disp[0]); 
     printf("TYPE  %2d     ****  ",type[0]);
     printf("%-22s %d - %2d\n","MPI_2DOUBLE_COMPLEX",
	    MPI_2DOUBLE_COMPLEX,reth);
#endif
     FTMPI_DDT_B_DT_ADD++;
   }
   /******************************************************************************/

#ifdef DEBUG_DDT_INIT
   printf("FTMPI_DDT_B_DT_ADD = %d\n",FTMPI_DDT_B_DT_ADD);
#endif
#ifdef DEBUG_DDT
    for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
      printf("BASIC DATATYPE %2d T_EXT: %2d  --  T_SIZE: %2d HASH: %4x\n",
	     i,_ftmpi_btype[i]->extent,
	     _ftmpi_btype[i]->size,
	     _ftmpi_btype[i]->hash);
    } 
#endif
  } /* if FTMPI_DDT_INIT not set */
}   
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
void ftmpi_ddt_remove_all_but_basic(void)
{
  int i;
  FTMPI_DDT_R_ITEM * rc = NULL,* rt = NULL;
  FTMPI_DDT_E_ITEM * ec = NULL,* et = NULL;


  for(i=0;i<FTMPI_DDT_HASH_SIZE;i++){
    rc = FTMPI_DDT_ROOT[i];
    while(rc != NULL){
      ec = rc->root_add->first_e;
      while(ec != NULL){
        et = ec->next;
        _FREE(ec);
        ec = et;
      }
      rt = rc->next;
      if(rc->root_add->i != NULL)_FREE(rc->root_add->i);
      if(rc->root_add->a != NULL)_FREE(rc->root_add->a);
      if(rc->root_add->d != NULL)_FREE(rc->root_add->d);
      _FREE(rc->root_add);
      _FREE(rc);
      rc = rt;
    }
    FTMPI_DDT_ROOT[i] = NULL;
  }
  FTMPI_DDT_NEXT_DATATYPE = ((FTMPI_DDT_NEXT_DATATYPE/100)+1)*100;
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
void ftmpi_ddt_finalize()
{
  int i;
  _FREE(FTMPI_DDT_MPI_BUFFER);
  
  ftmpi_ddt_remove_all_but_basic();
  _FREE(FTMPI_DDT_ROOT);

  for(i=0;i<FTMPI_DDT_B_DT_MAX;i++){
    _FREE(_ftmpi_btype[i]);
  }
}

/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
void ftmpi_ddt_reset_ref_counts()
{
  int i;
  FTMPI_DDT_R_ITEM * rc = NULL;

  for(i=0;i<FTMPI_DDT_HASH_SIZE;i++){
    rc = FTMPI_DDT_ROOT[i];
    while(rc != NULL){
      rc->uses = 1;
      rc = rc->next;
    }
  }
}
/***************************************************************************/
/***************************************************************************
****************************************************************************
**  writes a datatype to a buffer:
**  buffer - pointer to where data is copied into
**  buffer_size - size of an available buffer
**  datatype - datatype to be copied
**  count - number of datatypes to be copied
**  data - pointer from where data is being copied
**  spaces - 0 for now spaces , 1 for spaces
**  last_dt - used internaly, needs to be NULL to begin
**  NOT ABLE TO CONTINUE IF NOT ENOUGH BUFFER SUPPLIED TO COPY EVERYTHING
**  BUT VERY EFFICIENT.  Definitely done.
****************************************************************************
***************************************************************************/
int ftmpi_ddt_write_dt_to_buffer(char *buffer, int buffer_size, int dt,
				 int count, char *data, int spaces, 
				 int *last_dt, int bswap_var)
{
  int i,ret,last_dt_a[2],offset=0,j,k,l;
  FTMPI_DDT_R_ITEM * xtemp = NULL;
  FTMPI_DDT_E_ITEM * temp = NULL;
  int xlast;        /* Last item in the comp-struct of the current entry */
  FTMPI_DDT_mem_CP_BS_vars

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();
  
  if (count == 0 ) {
#ifdef DEBUG_DDT
    printf("write_dt_to_buf: called with count=0\n");
#endif
    return ( 0 );
  }

  if(last_dt == NULL){
    last_dt    = last_dt_a;
    last_dt[0] = 0;
    last_dt[1] = -1;
  }
   
  offset = last_dt[0];

  /* Check for basic datatype */
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    {
      if(_ftmpi_btype[dt]->size * count <= buffer_size)
	dt_size = ret = _ftmpi_btype[dt]->size * count;
      else {
	dt_size = (buffer_size/_ftmpi_btype[dt]->size)*
	  _ftmpi_btype[dt]->size;
	ret = -2;
      }
      
      dt_dest = buffer;
      dt_src  = data;

      if(bswap_var == 0)
	dt_type = 1;
      else 
	dt_type = _ftmpi_btype[dt]->size;
      
      FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
      return(ret);
    }

  /* Derived datatype section */
  xtemp = ftmpi_ddt_get_root(dt);
  if(xtemp != NULL){
#ifdef DEBUG_DDT
    printf("%d EXTENT == %d %d\n",count,xtemp->extent,xtemp->last);
#endif

    for(i=0;i<count;i++)
      {
	/* XTROOT is a macro for xtemp->root_add */
	if(XTROOT->t_comp == -1){
#ifdef DEBUG_DDT
	  printf("FTMPI: write_dt_to_buf: not using the comp-struct\n");
#endif
	  temp = ftmpi_ddt_get_element(xtemp,temp);
	  while(temp != NULL){
	    if(temp->dt < FTMPI_DDT_B_DATATYPE_TOTAL){
	      /* Element is a basic datatype */
	      if(spaces){
		if(last_dt[1] != temp->dt && last_dt[1] != -1)
		  last_dt[0] = ALIGN_TO(last_dt[0], int);
	      }  

	      /*************************************************************/
	      /* Determine dest, src and dt_type */
	      dt_dest = &buffer[last_dt[0]];
	      dt_src  = &data[temp->padding + (i*xtemp->extent)];

	      if(bswap_var == 0)
		dt_type = 1;
	      else 
		dt_type = _ftmpi_btype[temp->dt]->size;	      

	      /*************************************************************/
	      /* Do copy, size depending on whether the buffer is large enough */
	      if((last_dt[0] + (temp->size*temp->count)) <= buffer_size){
		dt_size = (temp->size * temp->count);
		
		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		last_dt[0] += dt_size;
	      }
	      else {
		dt_size = (temp->size - ((last_dt[0]+temp->size)-buffer_size));
		
		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		fprintf(stderr, "FTMPI: write_dt_to_buf: Buffer is full!\n");
		return(-2);
	      }
	      last_dt[1] = temp->dt;
	    }
	    else {
	      /* Element is a derived dt, we call this routine recursively */
	      ret = ftmpi_ddt_write_dt_to_buffer(buffer,buffer_size,temp->dt,
						 temp->count,
						 &data[xtemp->extent*i+temp->padding],
					       spaces,last_dt,bswap_var);
	      if(ret < 0) return (ret);
	    }
	    temp = ftmpi_ddt_get_element(xtemp,temp);
	  }
	} 
	else {
#ifdef DEBUG_DDT
	  printf("FTMPI: write_dt_to_buf: using comp-structure\n");
#endif
	  /* XTROOT is a macro for xtemp->root_add */
	  for(j=0;j<XTROOT->t_comp;j += XTROOT->comp[j].nitems){
	    xlast = j + XTROOT->comp[j].nitems-1;
	    for(k=0;k<XTROOT->comp[j].nrepeats;k++){
	      for(l=0;l<XTROOT->comp[j].nitems;l++){
		if(spaces){  
		  /* if there should be spaces */
		  if(XTROOT->comp[j+l-1].dt!=XTROOT->comp[j+l].dt &&(j-1)!=-1){
		    offset = ALIGN_TO(offset, int);
		  }
		}

		/*********************************************************/
		/* Determine dest, src and type */
		dt_dest = &buffer[offset];
		dt_src  = &data[XTROOT->comp[j+l].disp+(i*xtemp->extent)+
				(k*(XTROOT->comp[xlast].extent))];

		if(bswap_var == 0)
		  dt_type = 1;
		else 
		  dt_type = _ftmpi_btype[XTROOT->comp[j+l].dt]->size;
		  
		/*********************************************************/
		/* Do the copy, look at the size of the provided buffer! */
		if(offset+_ftmpi_btype[XTROOT->comp[j+l].dt]->size*
		   XTROOT->comp[j+l].count <= buffer_size)
		  {
		    dt_size = XTROOT->comp[j+l].count*
		      (_ftmpi_btype[XTROOT->comp[j+l].dt]->size);
		    
		    FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		    offset += dt_size;
		  }
		else 
		  {
		    dt_size = buffer_size - offset;
		    
		    FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		    fprintf(stderr, "FTMPI: write_dt_to_buf: Wx - Buffer is full!\n");
		    return(-2);
		  }
	      }
	    }
	  }
	  
	  last_dt[0] = offset;
	} 
      }
#ifdef DEBUG_DDT
    printf("FTMPI: write_dt_to_buf: RETURNING %d\n",last_dt[0]);
#endif
    return(last_dt[0]);
  }
  
  printf("FTMPI: write_dt_to_buf: datatype %d unknown\n",dt); 
  return(-1);
}
/***************************************************************************
****************************************************************************
**  reads a datatype from a buffer into a data:
**  buffer - pointer from where datatype is copied
**  buffer_size - size of an available buffer
**  datatype - datatype to be copied
**  count - number of datatypes to be copied
**  data - pointer to where data is being copied
**  spaces - 0 for now spaces , 1 for spaces
**  last_dt - used internaly, needs to be NULL to begin
**  NOT ABLE TO CONTINUE IF NOT ENOUGH BUFFER SUPPLIED TO COPY EVERYTHING
**  BUT VERY EFFICIENT. Definitely done.
****************************************************************************
***************************************************************************/
int ftmpi_ddt_read_dt_from_buffer(char *from_buffer, int buffer_size,
				  int dt, int count, char *to_buffer,
				  int spaces, int *last_dt,
				  FTMPI_DDT_CODE_INFO *code_info)
{
  int i,ret,last_dt_a[2],offset=0,j,k,l;
  int bdt_size;
  int xlast;                  /* Last item in the comp-struct of the current entry */
  FTMPI_DDT_R_ITEM * xtemp = NULL;
  FTMPI_DDT_E_ITEM * temp = NULL;
  FTMPI_DDT_mem_CP_BS_vars

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();
  
  
  if (count == 0 ) {
#ifdef DEBUG_DDT
    printf("read_dt_to_buf: called with count=0\n");
#endif
    return ( 0 );
  }

  if(last_dt == NULL){
    last_dt    = last_dt_a;
    last_dt[0] = 0;
    last_dt[1] = -1;
  }

  offset = last_dt[0];

  /* Check for basic datatypes */
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    {
      dt_mode  = ftmpi_ddt_dt_mode(dt,code_info);
      bdt_size = ftmpi_ddt_get_bdt_sizes(code_info,dt);
      
      if(bdt_size * count <= buffer_size)
	dt_size = ret = bdt_size * count;
      else {
	dt_size = (buffer_size/bdt_size)*bdt_size;
	ret = -2;
      }

      dt_dest = to_buffer;
      dt_src  = from_buffer;

      if(code_info->bs == 0) 
	dt_type = 1;
      else   
	dt_type = ftmpi_ddt_get_bdt_sizes(code_info,dt);
      
      FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
      return(ret);
    }

  /* Derived datatype section */
  xtemp = ftmpi_ddt_get_root(dt);

  if(xtemp != NULL){
    for(i=0;i<count;i++){
      /* XTROOT is a macro for xtemp->root_add */
      if(XTROOT->t_comp == -1)
	{
	  /* Do not use the comp-structure */
	  temp = ftmpi_ddt_get_element(xtemp,temp);
	  while(temp != NULL){
	    if(temp->dt < FTMPI_DDT_B_DATATYPE_TOTAL){
	      /* Element is a basic datatype */
	      if(spaces){
		if(last_dt[1] != temp->dt && last_dt[1] != -1)
		  last_dt[0] = ALIGN_TO(last_dt[0], int );
	      }  
	      
	      dt_mode = ftmpi_ddt_dt_mode(dt,code_info);
	      bdt_size = ftmpi_ddt_get_bdt_sizes(code_info,temp->dt);
	      
	      /*************************************************************/
	      /* determine src and type, for some reason dest was calculated
		 here differently for the two cases (buffer large enough or not
	      */
	      dt_src  = &from_buffer[last_dt[0]];
	      dt_dest = &to_buffer[temp->padding + (i*xtemp->extent)];
	      
	      if(code_info->bs == 0)
		dt_type = 1;
	      else 
		dt_type = bdt_size;
		
	      /**************************************************************/
	      /* move the data, but look at the size of the provided buffer */
	      if((last_dt[0] + (bdt_size * temp->count )) <= buffer_size){
		dt_size = (bdt_size * temp->count);
		
		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		last_dt[0] += dt_size;
	      }
	      else {
		/* dt_dest = &to_buffer[temp->padding]; */
		dt_size = bdt_size - ((last_dt[0] + bdt_size) - buffer_size);

		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
	        fprintf(stderr, "FTMPI: read_dt_from_buf: Buffer is full!\n");
		return(-2);
	      }
	      last_dt[1] = temp->dt;
	    }
	    else {
	      /* Element is a derived datatype, call this routine recursively */
	      ret = ftmpi_ddt_read_dt_from_buffer(from_buffer,buffer_size,
						  temp->dt,temp->count,
						  &to_buffer[xtemp->extent*i+temp->padding],
						  spaces,last_dt,code_info);
	      if(ret < 0) return (ret);            
	      offset = ret;
	    }
	    temp = ftmpi_ddt_get_element(xtemp,temp);
	  }
	} 
      else 
	{
	  /* We can use the comp-structure instead of the element-list */
	  for(j=0;j<XTROOT->t_comp;j += XTROOT->comp[j].nitems){
	    xlast = j + XTROOT->comp[j].nitems -1;
	    for(k=0;k<XTROOT->comp[j].nrepeats;k++){
	      for(l=0;l<XTROOT->comp[j].nitems;l++){
		if(spaces){  
		  /* if there should be spaces */
		  if(XTROOT->comp[j+l-1].dt != XTROOT->comp[j+l].dt && (j-1) != -1)
		    offset = ALIGN_TO(offset, int);
		}

		dt_mode = ftmpi_ddt_dt_mode(XTROOT->comp[j+l].dt,code_info);
		bdt_size = ftmpi_ddt_get_bdt_sizes(code_info,XTROOT->comp[j+l].dt);


		/***********************************************************/
		/* Set dest, src and type */
		dt_dest = &to_buffer[XTROOT->comp[j+l].disp + (i*xtemp->extent)+
				     (k*(XTROOT->comp[xlast].extent))];
		dt_src  = &from_buffer[offset];

		if(code_info->bs == 0) 
		  dt_type = 1;
		else
		  dt_type = bdt_size;
		

		if(offset + XTROOT->comp[j+l].count*bdt_size <= buffer_size){
		  /* Buffer is large enough to copy the whole data at once */
		  dt_size = XTROOT->comp[j+l].count*bdt_size;

		  FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		  offset += dt_size;
		}
		else {
		  /* Buffer too small, just copy what fits */
		  dt_size = buffer_size - offset;

		  FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		  fprintf(stderr, "FTMPI: read_to_from_buf: buffer is full ABC!\n");
		  return(-2);
		}
	      }
	    }
	  }
	  last_dt[0] = offset;
	} 
    }
    return(last_dt[0]);
  }

  printf("FTMPI: read_dt_from_buf: datatype %d does NOT exist\n",dt); 
  return(-1);
}
/***************************************************************************
****************************************************************************
** Same as write_dt_to_buffer but puts all basic_datatypes of the same kind together
**
**  NOT ABLE TO CONTINUE IF NOT ENOUGH BUFFER SUPPLIED TO COPY EVERYTHING
**  BUT VERY EFFICIENT.
**
**
**
** Definitely done.
****************************************************************************
***************************************************************************/
int ftmpi_ddt_write_dt_to_buffer_block(char *buffer,int buffer_size,
				       int dt, int count,char *data,
				       int spaces, int *dt_write,int bswap_var)
{
  int i,j,k,l,ret,retvar=0;
  int dt_write_new[FTMPI_DDT_B_DT_MAX],last_dt=-1,extent;
  int xlast;            /* Last item in the comp-struct of the current entry */
  FTMPI_DDT_R_ITEM * xtemp = NULL;
  FTMPI_DDT_E_ITEM * temp = NULL;
  FTMPI_DDT_mem_CP_BS_vars

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();
  

  /* Check for basic datatypes */
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    {
      if(_ftmpi_btype[dt]->size * count <= buffer_size)
	dt_size = ret = _ftmpi_btype[dt]->size * count;
      else {
	dt_size = (buffer_size/_ftmpi_btype[dt]->size)*
	  _ftmpi_btype[dt]->size;
	ret = -2;
      }

      dt_dest = buffer;
      dt_src  = data;
      
      if(bswap_var == 0)
	dt_type = 1;
      else 
	dt_type = _ftmpi_btype[dt]->size;

      FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
      return(ret);
    }

  /* Derived datatype section */
  xtemp = ftmpi_ddt_get_root(dt);

  if(xtemp != NULL){
    extent = xtemp->extent;
    if(dt_write == NULL){
      dt_write = dt_write_new;
      for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
        if(XTROOT->type_count[i] != 0){	  
          if(last_dt == -1){
            dt_write[i] = 0;
            last_dt     = i;
          }
          else {
            if(spaces) 
              dt_write[i] = ((dt_write[last_dt]+
			      ALIGN_TO((_ftmpi_btype[last_dt]->size * 
					XTROOT->type_count[last_dt]*count), int)));
	    else 
              dt_write[i] = dt_write[last_dt]+ (_ftmpi_btype[last_dt]->size * 
						XTROOT->type_count[last_dt] * count);
            last_dt = i;
          }
        }
      } 
    }

    for(i=0;i<count;i++){
      /* XTROOT is a macro for xtemp->root_add */
      if(XTROOT->t_comp == -1){
	/* Use the element list, not the comp-structure */
        temp = ftmpi_ddt_get_element(xtemp,temp);
        while(temp != NULL){
          if(temp->dt < FTMPI_DDT_B_DATATYPE_TOTAL)
	    {
	      /* Element is a basic datatype */
	      
	      /*****************************************************************/
	      /* Set dest, src and type */
	      dt_dest = &buffer[dt_write[temp->dt]];
	      dt_src  = &data[temp->padding+(extent*i)];
		
	      if(bswap_var == 0)
		dt_type = 1;
	      else 
		dt_type = _ftmpi_btype[temp->dt]->size;
		
	      if((dt_write[temp->dt] + (temp->size * temp->count)) <= buffer_size){
		/* Buffer is large enough to copy the while data at once */
		dt_size = temp->size * temp->count;
		
		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		dt_write[temp->dt] += dt_size;
		if(dt_write[temp->dt] > retvar) retvar = dt_write[temp->dt];
	      }
	      else {
		/****************************************************************/
		/* Buffer too small, just copy as much as you can */
		dt_size = (temp->size*temp->count)-
		  ((dt_write[temp->dt] + temp->size*temp->count)-buffer_size);
		
		FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		fprintf(stderr, "FTMPI: write_dt_to_buffer_block: buffer is full!\n");
		return(-2);
	      }
	    }
          else {
	    /***************************************************************/
	    /* Element is a derived datatype, call this routine recursively */
            ret = ftmpi_ddt_write_dt_to_buffer_block(buffer,buffer_size,
						     temp->dt,temp->count,
						     &data[temp->padding],
						     spaces,dt_write,bswap_var);
            if(ret < 0) return (ret);
            if(ret > retvar) retvar = ret;
          }
          temp = ftmpi_ddt_get_element(xtemp,temp);
        }
      } 
      else {
        for(j=0;j<XTROOT->t_comp;j += XTROOT->comp[j].nitems){
	  xlast = j + XTROOT->comp[j].nitems -1 ;
          for(k=0;k<XTROOT->comp[j].nrepeats;k++){
            for(l=0;l<XTROOT->comp[j].nitems;l++){


	      /***********************************************************/
	      /* Calculate dest, src and type */
	      dt_dest = &buffer[dt_write[XTROOT->comp[j+l].dt]];
	      dt_src  = &data[XTROOT->comp[j+l].disp +
			      (XTROOT->comp[xlast].extent*k) + (i*extent)];
	      
	      if(bswap_var == 0)
		dt_type = 1;
	      else 
		dt_type = _ftmpi_btype[XTROOT->comp[j+l].dt]->size;
	      
              if((dt_write[XTROOT->comp[j+l].dt] + 
		  (_ftmpi_btype[XTROOT->comp[j+l].dt]->size * 
		   XTROOT->comp[j+l].count)) <= buffer_size)
		{
		  /* Buffer large enough... */
		  dt_size = _ftmpi_btype[XTROOT->comp[j+l].dt]->size * 
		    XTROOT->comp[j+l].count;
		  
		  FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		  dt_write[XTROOT->comp[j+l].dt] += dt_size;
		  if(dt_write[XTROOT->comp[j+l].dt] > retvar)
		    retvar = dt_write[XTROOT->comp[j+l].dt];
                
		}
              else {
		/* Buffer too small, just copy as much as you can */
                dt_size = (_ftmpi_btype[XTROOT->comp[j+l].dt]->size * 
			   XTROOT->comp[j+l].count)- ((dt_write[XTROOT->comp[j+l].dt] + 
			   _ftmpi_btype[XTROOT->comp[j+l].dt]->size *
			   XTROOT->comp[j+l].count)-buffer_size);

                FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
                fprintf(stderr, "FTMPI: write_dt_to_buffer_block: buffer is full!\n");
                return(-2);
              }
            }
          }
        }
      } 
    }
    return(retvar);
  }

  printf("FTMPI: write_dt_to_buffer_block: datatype %d unknown\n",dt);
  return(-1);
}
/***************************************************************************
****************************************************************************
** Same as ftmpi_ddt_read_dt_from_buffer() but puts all basic_datatypes 
** of the same kind together
**
**  NOT ABLE TO CONTINUE IF NOT ENOUGH BUFFER SUPPLIED TO COPY EVERYTHING
**  BUT VERY EFFICIENT.
**
**
**
** Definitely done.
****************************************************************************
***************************************************************************/
int ftmpi_ddt_read_dt_from_buffer_block(char *buffer, int buffer_size, int dt,
					int count, char *data, int spaces, 
					int *dt_read, int bswap_var)
{
  int i,j,k,l,ret,retvar=0;
  int dt_read_new[FTMPI_DDT_B_DT_MAX],last_dt=-1,extent;
  int xlast;                  /* Last item in the comp-struct of the current entry */
  FTMPI_DDT_R_ITEM * xtemp = NULL;
  FTMPI_DDT_E_ITEM * temp = NULL;
  FTMPI_DDT_mem_CP_BS_vars

  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();

  /* Check for basic datatypes */
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    {
      if(_ftmpi_btype[dt]->size * count <= buffer_size)
	dt_size = ret = _ftmpi_btype[dt]->size * count;
      else {
	dt_size = (buffer_size/_ftmpi_btype[dt]->size)*
	  _ftmpi_btype[dt]->size;
	ret = -2;
      }

      dt_dest = data;
      dt_src  = buffer;

      if(bswap_var == 0)
	dt_type = 1;
      else 
	dt_type = _ftmpi_btype[dt]->size;
      
      FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
      return(ret);
    }

  /* Derived datatype section */
  xtemp = ftmpi_ddt_get_root(dt);

  if(xtemp != NULL){
    extent = xtemp->extent;
    if(dt_read == NULL){
      dt_read = dt_read_new;
      for(i = 1;i <FTMPI_DDT_B_DATATYPE_TOTAL;i++){
        if(XTROOT->type_count[i] != 0){
          if(last_dt == -1){
            dt_read[i] = 0;
            last_dt    = i;
          }
          else {
            if(spaces)
              dt_read[i] = ((dt_read[last_dt]+
			     ALIGN_TO((_ftmpi_btype[last_dt]->size * 
			      XTROOT->type_count[last_dt]*count), int)));
            else 
              dt_read[i] = dt_read[last_dt]+
		           (_ftmpi_btype[last_dt]->size * 
			    XTROOT->type_count[last_dt]*count);
            last_dt = i;
          }     
        }       
      }   
    }   

    for(i=0;i<count;i++){
      if(XTROOT->t_comp == -1){ 
	/* use element list instead of the comp-structure */
        temp = ftmpi_ddt_get_element(xtemp,temp);
        while(temp != NULL){
          if(temp->dt < FTMPI_DDT_B_DATATYPE_TOTAL){
	    /* element is a basic datatype */

	    /**************************************************************/
	    /* Set dest, src and type */
	    dt_dest = &data[temp->padding+(extent*i)];
	    dt_src  = &buffer[dt_read[temp->dt]];

	    if(bswap_var == 0)
	      dt_type = 1;
	    else 
	      dt_type = _ftmpi_btype[temp->dt]->size;
	    
	    if(dt_read[temp->dt] + (temp->size * temp->count) <= buffer_size){
	      /* Buffer is large enough to copy everything */
              dt_size = temp->size * temp->count;

              FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
	      dt_read[temp->dt] += dt_size;
              if(dt_read[temp->dt] > retvar) retvar = dt_read[temp->dt];
            }   
            else {
	      /* Buffer too small, just copy as much as you can */
              dt_size = (size_t)(buffer + buffer_size - dt_read[temp->dt]);

              FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
	      fprintf(stderr, "FTMPI: read_td_from_buffer_block: Buffer is full!\n");
              return(-2);
            }   
          }     
          else {
	    /*****************************************************************/
	    /* element is a derived datatype, call this routine recursively */
            ret = ftmpi_ddt_read_dt_from_buffer_block(buffer,buffer_size,
						      temp->dt,temp->count,
						      &data[temp->padding],
						      spaces,dt_read,bswap_var);  
            if(ret < 0) return (ret);
            if(ret > retvar) retvar = ret;
          }     
          temp = ftmpi_ddt_get_element(xtemp,temp);
        }       
      }         
      else 
	{
	  /* Use the comp-structure instead of the element list */
	  for(j=0;j<XTROOT->t_comp;j += XTROOT->comp[j].nitems){
	    xlast = j + XTROOT->comp[j].nitems - 1;
	    for(k=0;k<XTROOT->comp[j].nrepeats;k++){
	      for(l=0;l<XTROOT->comp[j].nitems;l++){
		
		/****************************************************************/
		/* Set dest and type, src is however different. (why ?) */
		dt_dest = &buffer[XTROOT->comp[j+l].disp +
				  (XTROOT->comp[xlast].extent * k)+(i*extent)];

		if(bswap_var == 0)
		  dt_type = 1;
		else 
		  dt_type = _ftmpi_btype[XTROOT->comp[j+l].dt]->size;
                                
		if(dt_read[XTROOT->comp[j+l].dt] + 
		   (_ftmpi_btype[XTROOT->comp[j+l].dt]->size * 
		    XTROOT->comp[j+l].count) <= buffer_size)
		  {         
		    /* You know probably already, buffer is large enough */
		    dt_src  = &data[dt_read[XTROOT->comp[j+l].dt]];
		    dt_size = _ftmpi_btype[XTROOT->comp[j+l].dt]->size * 
		      XTROOT->comp[j+l].count;

		    FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		    dt_read[XTROOT->comp[j+l].dt] += dt_size;
		    if(dt_read[XTROOT->comp[j+l].dt] > retvar)
		      retvar = dt_read[XTROOT->comp[j+l].dt];
		  } 
		else 
		  {
		    /* Buffer too small, just copy as much as you can */
		    dt_src  = &data[XTROOT->comp[j+l].disp+((extent/XTROOT->t_comp)*k)];
		    dt_size = (size_t)(buffer + buffer_size -
				       dt_read[XTROOT->comp[j+l].dt]);
		    
		    FTMPI_DDT_mem_COPY_BSWAP(dt_dest,dt_src,dt_size,dt_type,dt_mode)
		    fprintf(stderr,"FTMPI: read_dt_from_buffer_block:buffer is full\n");
		    return(-2);
		  } 
	      }   
	    }   
	  }   
	}   
    }   
    return(retvar);
  }             
  printf("DATATYPE %d does NOT exist\n",dt);
  return(-1);   
}               
/***************************************************************************/
/***************************************************************************
****************************************************************************
**
**
**
**
**
**
**
** Definitely done
****************************************************************************
***************************************************************************/
int ftmpi_ddt_xdr_encode_dt_to_buffer(char * from,char * to,char * tmp,
				      int dest_size,int cnt,int dt,
				      FTMPI_DDT_CODE_INFO * code_info)
{
  int offset=0,i;
  FTMPI_DDT_R_ITEM * xtemp=NULL;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }


  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL){
#ifdef DEBUG_DDT
    printf("ENCODE BASIC DDT %d\n",code_info->code_type); 
#endif

    if(code_info->code_type == FTMPI_XDR){
      ftmpi_ddt_xdr_code(to,from,dt,cnt,1);
      return(1);
    }
    else if(code_info->code_type == FTMPI_RSC){
      ftmpi_ddt_write_dt_to_buffer(to,dest_size,dt,cnt,from,0,NULL,0);
      return(1);
    }
    else {
      memcpy(to,from,dest_size);
      return(0);
    }
  }

  xtemp = ftmpi_ddt_get_root(dt);

  if(xtemp != NULL){
#ifdef DEBUG_DDT
    printf("ENCODE DDT\n"); 
#endif
    if(code_info->code_type == FTMPI_XDR){
      ftmpi_ddt_write_dt_to_buffer_block(tmp,dest_size,dt,cnt,from,1,NULL,0);
      for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
        if(XTROOT->type_count[i] >0){
          ftmpi_ddt_xdr_code(&to[offset],&tmp[offset],i,
			     XTROOT->type_count[i]*cnt,1);
          offset += (ALIGN_TO((XTROOT->type_count[i]*
			       _ftmpi_btype[i]->size*cnt), int));
        }
      }
      return(0);   /* it should now be in src */
    }
    else if(code_info->code_type == FTMPI_RSC){
      ftmpi_ddt_write_dt_to_buffer(to,dest_size,dt,cnt,from,0,NULL,0);
      return(1);
    }
    else {
      ftmpi_ddt_write_dt_to_buffer(to,dest_size,dt,cnt,from,0,NULL,0);
      return(1);
    }
  } 
  printf("DATATYPE %d does NOT exist\n",dt);
  return(-1);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
int ftmpi_ddt_xdr_decode_dt_to_buffer(char *from, char *to, char *tmp, 
				      int dest_size, int cnt, int dt,
				      FTMPI_DDT_CODE_INFO * code_info)
{
  int offset=0,i;
  FTMPI_DDT_R_ITEM *xtemp = NULL;
  int size;

  if(FTMPI_DDT_INIT == 0){
    ftmpi_ddt_init();
  }

  ftmpi_mpi_type_size(dt,&size);


  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL){
#ifdef DEBUG_DDT
    printf("DECODE BASIC DDT\n"); 
#endif
    if(code_info->code_type == FTMPI_XDR){
      ftmpi_ddt_xdr_code(to,from,dt,cnt,2);
      return(1);
    }
    else if(code_info->code_type == FTMPI_RSC){
      ftmpi_ddt_read_dt_from_buffer(from,dest_size,dt,cnt,to,0,NULL,code_info);
      return(1);
    }
    memcpy(to,from,dest_size);
    return(0);
  }

  xtemp = ftmpi_ddt_get_root(dt);

  if(xtemp != NULL){
#ifdef DEBUG_DDT
     printf("DECODE DDT\n"); 
#endif
     if(code_info->code_type == FTMPI_XDR){
      for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
        if(XTROOT->type_count[i] >0){
          ftmpi_ddt_xdr_code(&tmp[offset],&from[offset],i,
			     XTROOT->type_count[i]*cnt,2);
          offset += (ALIGN_TO((XTROOT->type_count[i]*
			       _ftmpi_btype[i]->size*cnt), int));
        }
      }  
      ftmpi_ddt_read_dt_from_buffer_block(to,dest_size,dt,cnt,tmp,1,NULL,0);
      return(0);
    }
    else{
      ftmpi_ddt_read_dt_from_buffer(from,dest_size,dt,cnt,to,0,NULL,code_info);
      return(1);
    }
  }  
  printf("DATATYPE %d does NOT exist\n",dt);
  return(-1);
}

/***************************************************************************/
/***************************************************************************
****************************************************************************
**  performs XDR encode or decode depending on XDR setup. At the moment will
**  not XDR CHARs as I have no idea why
**
**
**
**
**
**  Definitely done
****************************************************************************
***************************************************************************/
int ftmpi_ddt_xdr_vector_code(XDR * xdr,char * buffer,int count,int size,int datatype)
{
  if(datatype == MPI_SHORT || datatype == MPI_UNSIGNED_SHORT ){
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_short)){
      printf("%d %d - XDR ENCODE ERROR - MPI_SHORT\n",datatype,
	     _ftmpi_btype[datatype]->size);
      return(-1);
    }
  }  
  else if(datatype == MPI_INT ){
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_int)){
      printf("%d - XDR ENCODE ERROR - MPI_INT\n",datatype);
      return(-1);
    }
  }  
  else if(datatype == MPI_LONG || datatype == MPI_UNSIGNED_LONG){
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_long)){
      printf("%d - XDR ENCODE ERROR - MPI_LONG\n",datatype);
      return(-1);
    }
  }  
  else if(datatype == MPI_UNSIGNED ){
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_int)){
      printf("%d - XDR ENCODE ERROR -- MPI_UNSIGNED\n",datatype);
      return(-1);
    }
  } 
  else if(datatype == MPI_FLOAT ){
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_float)){
      printf("%d - XDR ENCODE ERROR - MPI_FLOAT\n",datatype);
      return(-1);
    }
  } 
  else {
    if(!xdr_vector(xdr,buffer,(u_int)count,(u_int)size,(xdrproc_t)xdr_double)){
      printf("%d %d - XDR ENCODE ERROR - DO NOT KNOW HOW TO ENOCODE THIS\n",
	     datatype,_ftmpi_btype[datatype]->size);
      return(-1);
    }
  } 
  return(1);
}   
/***************************************************************************
****************************************************************************
**  This will decide on its own whether to do xdr or byteswap
**
**  code - 1 for encode and 2 for decode
**
**
**
**
**  Definitely done
****************************************************************************
***************************************************************************/
int ftmpi_ddt_xdr_code(char *out, char *in, int dt, int count, int code)
{
  XDR xdr;
  int ret;

  /* code 1 = ENCODE;  code 2 = DECODE */

#ifdef DEBUG_DDT
      printf("Performing XDR %d %d %d\n",code,dt,count);
#endif

  if(code == 1){
    if(dt == MPI_CHAR || dt == MPI_UNSIGNED_CHAR || dt == MPI_BYTE){
      memcpy(out,in,_ftmpi_btype[dt]->size*count);
      return 0;
    }
    else {
      xdrmem_create(&xdr,out,(u_int)(_ftmpi_btype[dt]->size 
				     * count),XDR_ENCODE);
      ret = ftmpi_ddt_xdr_vector_code(&xdr,in,count,
				      _ftmpi_btype[dt]->size,dt);
    }
  }
  else{
    if(dt == MPI_CHAR || dt == MPI_UNSIGNED_CHAR || dt == MPI_BYTE){
      memcpy(out,in,_ftmpi_btype[dt]->size*count);
      return 0;
    }
    else {
      xdrmem_create(&xdr,in,(u_int)(_ftmpi_btype[dt]->size 
				    * count),XDR_DECODE);
      ret = ftmpi_ddt_xdr_vector_code(&xdr,out,count,
				      _ftmpi_btype[dt]->size,dt);
    }
  }
  return(ret);
}
/***************************************************************************
****************************************************************************
**    
**    
**
**
**
**
**
**   Definitely done   - MAYBE as far as I am aware
****************************************************************************
***************************************************************************/
/* #define DEBUG_DDT 1 */
int ftmpi_ddt_check_array(_ftmpi_comp_t *old,int * size)
{     
  int j;
  int ni=0,i,k;
  int t_cnt = 0;
  _ftmpi_comp_t new_a[FTMPI_DDT_COMP_MAX+4];
  long last_ext = 0;
  long l_ext = 0;
  long p_ext = 0;

#ifdef DEBUG_DDT 
  int prt_ln = 0;
  int tmp;
  int dt_cnt = 0;
#endif 
      
  if(*size < 1){
    return(0);
  }

     
      
#ifdef DEBUG_DDT 
  printf("--- ORG --\n");
  printf("--- ORG --\n");
  printf("--- ORG --\n");
  tmp = old[0].nitems-1;
  printf("----------------------------------- %d\n",*size);
  for(i=0;i<*size;i++)
    {
      printf("%-3d ",i);
      for(j=0;j<6;j++)
	printf("%4d",old[i+j].dt);      
      printf("\n");
      
      if( (i==tmp) && (prt_ln == 1) )
	printf("  -----------------------\n");
      
      tmp += old[i+j].nitems;
      if(old[i+j].nitems >1)
	prt_ln = 1;
      else 
	prt_ln = 0;
    } 
#endif
     

  /* If I understand this next part of the code correctly, we are
     basically unrolling the description of the datatype into new_a.
     Unrolling can be "trivial", e.g. if the number of items are
     larger than 1, we have it anyway each item in a separate entry of
     the comp-array.  Thus, for this case just the description of the
     first item is really changing.  
     We are however also unrolling datatypes containing repeatitions, 
     e.g. each repeatition will now have its own line in new_a.  
     Thus, this information is lost forever.
  */

  /*
    0 - datatype
    1 - count
    2 - displacement
    3 - extent
    4 - number of items
    5 - number of repeats
  */  
      
  for(j=0;j<(*size);j += old[j].nitems )
    {
      if(old[j].nrepeats > 1)
	{
#ifdef DEBUG_DDT
	  printf("FTMPI: check_array: no. of repeat is > 1");
#endif
	  if(old[j].nitems == 1)
	    {
#ifdef DEBUG_DDT
	      printf("FTMPI: check_array: no. of items is one");
#endif
	      if(t_cnt+1 == FTMPI_DDT_COMP_MAX)
		return(0);
	      
	      memcpy(&new_a[ni],&old[j],sizeof(_ftmpi_comp_t));
	      t_cnt++;
	      ni++;
	    }
	  else 
	    {
#ifdef DEBUG_DDT
	      printf("FTMPI: check_array: no. of items is %d",t_cnt);
#endif
	      if(old[j].dt == old[j+(old[j].nitems-1)].dt )
		{
#ifdef DEBUG_DDT
		  printf("FTMPI: check_array: same dt of the first and last item");
#endif
		  if(0 == (old[j].disp - last_ext) )
		    {
#ifdef DEBUG_DDT
		      printf("FTMPI: check_array: start disp is zero");
#endif
		      /* Check whether: extent == displacement + size  * count */
		      if(old[j+(old[j].nitems-1)].extent == 
			 (old[j+(old[j].nitems-1)].disp +
			  (_ftmpi_btype[old[j+(old[j].nitems-1)].dt]->size 
			   * old[j+(old[j].nitems-1)].count)) )
			{
			
#ifdef DEBUG_DDT
			  printf("FTMPI: check_array:extent = (disp + size * count)");
#endif
			  
			  /* make this into a three part DT */
			  
			  /*
			    0 - datatype            - stays the same
			    1 - count               - stays the same
			    2 - displacement        - disp - size
			    3 - extent              - extent - size;
			    4 - number of items     - number - 1;
			    5 - number of repeats   - number -1;
			  */
			
			  l_ext = (old[j+(old[j].nitems-1)].extent - p_ext)*
			    (old[j].nrepeats - 1 );
#ifdef DEBUG_DDT
			  printf(" %3d %3d ",l_ext,p_ext);
#endif
			  
			  if(t_cnt+1 == FTMPI_DDT_COMP_MAX)
			    return(0);
			
			  memcpy(&new_a[ni],&old[j],sizeof(_ftmpi_comp_t));
			
			  new_a[ni].nitems   = 1;
			  new_a[ni].nrepeats = 1;
			  t_cnt++;
			  ni++;
			  
			  i = 0;
			  
			  if((old[j].nitems-2) > 0)
			    {
#ifdef DEBUG_DDT
			      printf("FTMPI: meaningless comment: - THERE IS MIDDLE");
#endif
			      for(i=0;i<(old[j].nitems-2);i++)
				{
				  if(t_cnt == FTMPI_DDT_COMP_MAX)
				    return(0);
				  
				  memcpy(&new_a[ni],&old[j+i+1],sizeof(_ftmpi_comp_t));
				
				  if(new_a[ni].nrepeats > 2)
				    new_a[ni].nitems -= 1;
				  else 
				    new_a[ni].nitems = 1;
				  
				  new_a[ni].nrepeats -= 1;			
				  t_cnt++;
				  ni++;
				}
			    }
			  
			  if(t_cnt == FTMPI_DDT_COMP_MAX)
			    return(0);
			  
			  memcpy(&new_a[ni],&old[j+i+1],sizeof(_ftmpi_comp_t));
			  
			  new_a[ni].count += old[j].count;
			  new_a[ni].extent = new_a[ni].extent +
			    (old[j].count * _ftmpi_btype[old[j].dt]->size);
			  
			  if(new_a[ni].nrepeats>2)
			    new_a[ni].nitems -= 1;
			  else 
			    new_a[ni].nitems = 1;
			  
			  new_a[ni].nrepeats -= 1;
			  t_cnt++;
			  ni++;
			  
			  /*
			    0 - datatype            - stays the same
			    1 - count               - stays the same
			    2 - displacement        - disp - size
			    3 - extent              - extent - size;
			    4 - number of items     - number - 1;
			    5 - number of repeats   - number -1;
			  */
			  
			  if(old[j].nitems-2 > 0){
			    for(i=0;i<old[j].nitems-2;i++){
			      if(t_cnt == FTMPI_DDT_COMP_MAX)
				return(0);
			
			      memcpy(&new_a[ni],&old[j+(i+1)],sizeof(_ftmpi_comp_t));
			      
			      new_a[ni].disp    += l_ext;
			      new_a[ni].extent  += l_ext;
			      new_a[ni].nitems   = 1;
			      new_a[ni].nrepeats = 1;
			      
			      t_cnt++;
			      ni++;
			    }
			  }
			  
			  if(t_cnt == FTMPI_DDT_COMP_MAX)
			    return(0);
			  
			  memcpy(&new_a[ni],&old[j+(old[j].nitems-1)],
				 sizeof(_ftmpi_comp_t));
			
			  new_a[ni].disp    += l_ext;
			  new_a[ni].extent  += l_ext;
			  new_a[ni].nitems   = 1;
			  new_a[ni].nrepeats = 1;
			  
      
			  p_ext = old[j+(old[j].nitems-1)].extent*(old[j].nrepeats);
			
			  t_cnt++;
			  ni++;;
			}
		      else 
			{
#ifdef DEBUG_DDT
			  printf("FTMPI: check_array:  - END NOK");
#endif
			  for(i=0;i<old[j].nitems;i++){
			    if(t_cnt == FTMPI_DDT_COMP_MAX)
			      return(0);
			    
			    /*memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t)*2);*/
			    memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t));
			    t_cnt++;
			    ni++;
			  }
			}
		    }
		  else 
		    {
#ifdef DEBUG_DDT
		      printf("FTMPI: check_array: start disp not zero %d",t_cnt);
#endif
		      for(i=0;i<old[j].nitems;i++){
			if(t_cnt == FTMPI_DDT_COMP_MAX)
			  return(0);
			
			/* memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t)*2); */
			memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t));
      
			t_cnt++;
			ni++;
		      }
      
		    }
		}
	      else 
		{
#ifdef DEBUG_DDT
		  printf("FTMPI: check_array: DIFF %d %d",t_cnt,old[j].nitems);
#endif
		  for(i=0;i<old[j].nitems;i++){
		    if(t_cnt == FTMPI_DDT_COMP_MAX)
		      return(0);
		    
		    /*  memcpy(&new_a[ni],&old[j+(i*6)],sizeof(_ftmpi_comp_t)*2); */
		    memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t));
		    
		    t_cnt++;
		    ni++;
		  }
		}
	    }
	} 
      else 
	{
#ifdef DEBUG_DDT
	  printf("FTMPI: check_array: no repeatitions set\n");
	  if(j <0)
	    exit(1);
#endif
	  for(i=0;i<old[j].nitems;i++){
	    if(t_cnt == FTMPI_DDT_COMP_MAX)
	      return(0);
	    
	    memcpy(&new_a[ni],&old[j+i],sizeof(_ftmpi_comp_t));
	    t_cnt++;
	    ni++;
	  }
	} 
      
      /*
	0 - datatype            - stays the same
	1 - count               - stays the same
	2 - displacement        - disp - size
	3 - extent              - extent - size;
	4 - number of items     - number - 1;
	5 - number of repeats   - number -1;
      */
      
      last_ext += old[j+(old[j].nitems-1)].extent*old[j].nrepeats;
    }   
  
  
  
  
#ifdef DEBUG_DDT
  printf("--- NEW -- %d\n",t_cnt);
  printf("--- NEW --\n");
  printf("--- NEW --\n");
  tmp = old[0].nitems-1;
  for(i=0;i<t_cnt;i++){
    printf("%-3d ",i);
    printf("%4d %4d %p %ld %4d %4d\n",
	   old[i].dt, old[i].count, old[i].disp,
	   old[i].extent, old[i].nitems, old[i].nrepeat);
    
    if(i==tmp){
      if(prt_ln == 1){
	printf("  -----------------------\n");
      }
      tmp += old[i+1].nitems;
      if(old[i+1].nitems >1){
	prt_ln = 1;
      }
      else {
	prt_ln = 0;
      }
    } 
  }   
#endif
  
  
  k = 0;
  memcpy(&old[0],&new_a[0],sizeof(_ftmpi_comp_t));
  /*   last_end = 0; */
  /*   cur_cnt = old.nitems; */
  
  /*  
      0 - datatype            - stays the same
      1 - count               - stays the same
      2 - displacement        - disp - size
      3 - extent              - extent - size;
      4 - number of items     - number - 1;
      5 - number of repeats   - number -1;
  */  
  
  /* This loop checks, whether two blocks in the new array could
     be merged to a single one. Condition: 
     1. no of repeat has to be one
     2. both blocks have to have the same datatype
     3. the blocks have to form a contiguous peace of memory
  */
  
  for(i=1;i<t_cnt;i++)
    {
      if(new_a[i].nrepeats == 1)
	{
#ifdef DEBUG_DDT
	  printf("FTMPI: check_array: possible merger of two blocks\n");
#endif
	  if(new_a[i].dt == old[k].dt){
#ifdef DEBUG_DDT
	    printf("FTMPI: check_array datatypes match %d new %d\n",k,i);
#endif
	    if(old[k].disp+_ftmpi_btype[old[k].dt]->size*old[k].count == 
	       new_a[i].disp)
	      {
#ifdef DEBUG_DDT
		printf("FTMPI: check_array: blocks are contiguous\n");
#endif
		old[k].count  += new_a[i].count;
		old[k].extent += new_a[i].extent;
	      }
	    else 
	      {
#ifdef DEBUG_DDT
		printf("FTMPI: check_array: blocks are not contiguous\n");
#endif
		k++;
		memcpy(&old[k],&new_a[i],sizeof(_ftmpi_comp_t));
	      }
	  }
	  else 
	    {
#ifdef DEBUG_DDT
	      printf("FTMPI: check_array: datatypes do not match\n");
#endif
	      k++;
	      memcpy(&old[k],&new_a[i],sizeof(_ftmpi_comp_t));
	    }
	} 
      else 
	{
#ifdef DEBUG_DDT
	  printf("FTMPI: check_array: no. of repeat > 1\n");
#endif
	  k++;
	  memcpy(&old[k],&new_a[i],sizeof(_ftmpi_comp_t));
	} 
    }   
  k++;
  
  
  
  
#ifdef DEBUG_DDT
  printf("\n\n***********************************************\n\n");
  for(i=0;i<k;i++){
    printf("  [%d]",i);
    printf("%4d %4d %p %ld %4d %4d\n",
	   old[i].dt, old[i].count, old[i].disp,
	   old[i].extent, old[i].nitems, old[i].nrepeat);
    
  } 
  
  printf("\n**************************************************\n\n");
#endif
  *size = k;
  return(1);
}     


/***************************************************************************
****************************************************************************
**  NEW ADDITION ... to find out the exact size to be sent and received 
** 
**
**
**
**
**
**   Definitely done
****************************************************************************
***************************************************************************/
int ftmpi_ddt_write_read_size_det(int dt,int count,int * size,int code_type)
{
  int offset=0,i;
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  
  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();

  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL){
    *size = count * _ftmpi_btype[dt]->size;
    if(code_type != 0)
      return(1);

    return(0);
  }
  else {
    xtemp = ftmpi_ddt_get_root(dt);
    if(xtemp != NULL){
      if(code_type == 2){
	for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
	  if(XTROOT->type_count[i] >0)
	    offset += (ALIGN_TO(((XTROOT->type_count[i]*
				  _ftmpi_btype[i]->size)*count), int));
	}  
	*size = offset;
	return(2);
      }
      else if(code_type == 1){
	/* if needed we could shave off little bit more time here --- ASK TONE */
	/* will need extra argument */
	*size = xtemp->size*count;
	return(1);
      }
      else {
	if(xtemp->contig == 1){
	  *size = xtemp->size*count;
	  return(0);
	}
	*size = xtemp->size*count;
	return(1);
      }
    } 
    printf(">wr_rd DATATYPE %d does NOT exist\n",dt);
    return(-1);
  }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_ddt_bs_xdr_det(int size)
{
  if(size%4 != 0)
    return(1);     /* doing XDR */
  
  else{
    if(size == 4)
      return(4);   /* This will allow 4 byte swapping */
    else if(size == 8)
      return(8);   /* This will allow 8 byte swapping */
    else if(size == 12)
      return(12);   /* This will allow 12 byte swapping */
    else 
      return(1);   /* this will make us use the XDR method */
    
  }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_ddt_get_remote_size(FTMPI_DDT_R_ITEM * xtemp,
			      FTMPI_DDT_CODE_INFO * code_info)
{
  if(ftmpi_ddt_my_longdouble.ld_flag == code_info->ld_flag) return xtemp->size;
  return(xtemp->size + (XTROOT->type_count[MPI_LONG] * 
			code_info->size_l) + 
	 (XTROOT->type_count[MPI_LONG_DOUBLE] * code_info->size_ld) 
	 - (XTROOT->type_count[MPI_LONG] * 
	    _ftmpi_btype[MPI_LONG]->size) + 
	 (XTROOT->type_count[MPI_LONG_DOUBLE] * 
	  _ftmpi_btype[MPI_LONG_DOUBLE]->size));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_ddt_decode_size_det(int dt,int count,int * size,
			      FTMPI_DDT_CODE_INFO * code_info)
{
  int offset=0,i;
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  
  if(FTMPI_DDT_INIT == 0) ftmpi_ddt_init();
  
  if(dt == MPI_PACKED || dt == MPI_BYTE){
    *size = count;
    return(0);
  }
  
  if(code_info == NULL) {
    *size = -1;
    return(3);
  }
  
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL){
    if(code_info->code_type == FTMPI_XDR){
      *size = count * _ftmpi_btype[dt]->xdr_size;
      return 1;
    }
    else if(code_info->code_type == FTMPI_RSC){
      *size = count * ftmpi_ddt_get_bdt_sizes(code_info,dt);
      if(ftmpi_ddt_my_longdouble.ld_flag == code_info->ld_flag)
	return 0;
      return 1;
    }
    else if(code_info->code_type == FTMPI_SSC){
      printf("         !!!   WARNING   !!!\n\nSENDER-SIDE-CONVERSION "
	     "NOT IMPLEMENTED DEBDT\n\n         !!!   WARNING   !!!\n");
      return -1;
    }
    else {
      *size = count * _ftmpi_btype[dt]->size;
      if(ftmpi_ddt_my_longdouble.ld_flag == code_info->ld_flag)
	return 0;
      return 1;
    }
  }
    
  
  xtemp = ftmpi_ddt_get_root(dt);
  if(xtemp != NULL){
    if(code_info->code_type == FTMPI_XDR){
      for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
	if(XTROOT->type_count[i] >0)
	  offset += (ALIGN_TO(((XTROOT->type_count[i]*
				_ftmpi_btype[i]->xdr_size)*count), int));
      }
      *size = offset;
      return(2);
    }
    else if(code_info->code_type == FTMPI_RSC) {
      *size = ftmpi_ddt_get_remote_size(xtemp,code_info) * count;
      if(xtemp->contig == 1 && ftmpi_ddt_my_longdouble.ld_flag == code_info->ld_flag){
	return 0;
      }
      return 1;
    }
    else if(code_info->code_type == FTMPI_SSC) {
      printf("!!   WARNING \n SENDER-SIDE-CONVERSION NOT IMPLEMENTED !!\n");
      return -1;
    }
    else {
      *size = ftmpi_ddt_get_remote_size(xtemp,code_info) * count;
      if(xtemp->contig == 1 && ftmpi_ddt_my_longdouble.ld_flag == code_info->ld_flag){
	return 0;
      }
      return 1;
    }
  }

  printf(">wr_rd DATATYPE %d does NOT exist\n",dt);
  return(-1);
}
/***********************************************************************************/
/***********************************************************************************/
/***********************************************************************************/
int ftmpi_ddt_encode_size_det(int dt, int count, int *size,
			      FTMPI_DDT_CODE_INFO *code_info)
{
  int offset=0,i;
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  
  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();
  
  if(dt == MPI_PACKED || dt == MPI_BYTE){
    *size = count;
    return(0);
  }
  
  if(code_info == NULL){
    *size = -1;
    return(3);
  }
  
  
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL){
    /*     printf("ENCODE_SIZE %d\n",code_info->code_type); */
    if(code_info->code_type == FTMPI_XDR){
      *size = count * _ftmpi_btype[dt]->xdr_size;
      return(1);
    }
    else if(code_info->code_type == FTMPI_SSC){
      printf("WARNING! \n SENDER-SIDE-CONVERSION NOT IMPLEMENTED !\n");
      return(-1);
    }
    else if(code_info->code_type == FTMPI_RSC) {
      *size = count * _ftmpi_btype[dt]->size;
      return(0);
    }
    else{
      *size = count * _ftmpi_btype[dt]->size;
      return(0);
    }
  }
  
  xtemp = ftmpi_ddt_get_root(dt);
  if(xtemp != NULL){
    if(code_info->code_type == FTMPI_XDR){
      for(i=1;i<FTMPI_DDT_B_DATATYPE_TOTAL;i++){
	if(XTROOT->type_count[i] >0)
	  offset += (ALIGN_TO(((XTROOT->type_count[i]*
				_ftmpi_btype[i]->xdr_size)*count), int ));
      }
      *size = offset;
      return(2);
    }
    
    else if(code_info->code_type == FTMPI_RSC){
      *size = xtemp->size*count;
      if(xtemp->contig != 1) 
	return 1;
      return 0;
    }
    else if(code_info->code_type == FTMPI_SSC){
      printf("WARNING!\n SENDER-SIDE-CONVERSION NOT IMPLEMENTED !\n");
      return(-1);
    }
    else {
      *size = xtemp->size*count;
      if(xtemp->contig != 1) 
	return 1;
      return 0;
    }
  }
  printf("ENCODE_SIZE_DET - DATATYPE %d does NOT exist\n",dt);
  return(-1);
}
/***************************************************************************
****************************************************************************
**  NEW ADDITION ... to find the offset of the very first item ... hehehe
**  no thanx to the MPI_BOTTOM
**
**
**
**
**
**   Definitely done
****************************************************************************
***************************************************************************/
int ftmpi_ddt_get_first_offset(int dt)
{
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  
  if(dt < FTMPI_DDT_B_DATATYPE_TOTAL)
    return(0);

  xtemp = ftmpi_ddt_get_root(dt);
  if(xtemp != NULL)
    return(XTROOT->first_e->padding);
  
  printf("FTMPI: ddt_get_first_offset: ddt %d unknown\n",dt);
  return(MPI_ERR_TYPE);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
int ftmpi_ddt_copy_ddt_to_ddt(void * fbuf,int fddt,int fcnt,void * tbuf,
			      int tddt,int tcnt)
{
  int fsize,tsize,ret;
  
  if(FTMPI_DDT_INIT == 0)
    ftmpi_ddt_init();

  if ( ( fcnt == 0 ) || ( tcnt == 0 )) {
#ifdef DEBUG_DDT
    printf("copy_ddt_to_ddt: called with count zero\n");
#endif
    return ( 0 );
  }
  
  if((ftmpi_mpi_type_size(fddt,&fsize)) != MPI_SUCCESS) return(-3);
  fsize *= fcnt;
  
  if((ftmpi_mpi_type_size(tddt,&tsize)) != MPI_SUCCESS) return(-4);
  tsize *= tcnt;
  
  /* OK IF THE SIZE IS NOT SAME .... EXITING */
  if(fsize > tsize){
    printf("FROM ddt %d cnt %d = %d != TO ddt %d cnt %d = %d\n",
	   fddt,fcnt,fsize,tddt,tcnt,tsize);
    return ( -12);
  }
  else if ( fsize < tsize ) {
    if ( fddt == tddt )
      tcnt = fcnt;
    else {
      tsize = tsize/tcnt;
      tcnt = fsize/tsize;
    }
  }
  
  if(fsize > FTMPI_DDT_MPI_BUFFER_SIZE){
    _FREE(FTMPI_DDT_MPI_BUFFER);
    FTMPI_DDT_MPI_BUFFER = (char*)_MALLOC(((tsize/1024)+1)*1024);
    if(FTMPI_DDT_MPI_BUFFER == NULL){
      printf("ERROR: COULD NOT ALLOCATE MEM FOR FTMPI_DDT_MPI_BUFFER\n");
      exit(1);
    }
    FTMPI_DDT_MPI_BUFFER_SIZE = tsize;
  }
  
  ret = ftmpi_ddt_write_dt_to_buffer(FTMPI_DDT_MPI_BUFFER,fsize,fddt,fcnt,
				     fbuf, 0,NULL,0);  
  if(ret < 0) return(-13);
  
  ret = ftmpi_ddt_read_dt_from_buffer(FTMPI_DDT_MPI_BUFFER,fsize,tddt,tcnt,
				      tbuf,0,NULL,&ftmpi_ddt_my_longdouble);
  if(ret < 0) return(-14);
  return(0);
}
/***************************************************************************/
/***************************************************************************/
/***************************************************************************/
int ftmpi_ddt_get_element_count(int ddt, long rsize, int *cnt, 
				FTMPI_DDT_R_ITEM *ddt_ptr, int ddt_cnt)
{
  FTMPI_DDT_R_ITEM *xtemp=NULL; 
  FTMPI_DDT_E_ITEM *etemp=NULL;
  int i,j,k;
  int ret;
  int size;
  int e_cnt = 0;
  
  if(ddt_ptr == NULL){ 
    *cnt = 0;
    if(ftmpi_mpi_type_size(ddt,&size) != 0) return(-1);
    if(size == 0) return 0;
    if( (ddt <= FTMPI_DDT_B_DT_MAX) || ((ddt>500) &&
#ifdef HAVE_LONG_LONG
					(ddt <= 513)
#else
					(ddt <= 512)
#endif 
					)) {
      *cnt = rsize/size;
      return(0);
    }
    
    xtemp = ftmpi_ddt_get_root(ddt);
    
    /*** COUNT THE ELEMENTS IN THE CURRENT DDT ***/
    for(j=1;j<FTMPI_DDT_B_DATATYPE_TOTAL;j++)
      e_cnt += XTROOT->type_count[j];
    
    ret = rsize/size;
    *cnt = ret * e_cnt;
    
    ret = rsize%size;

    if(XTROOT->t_comp != -1){
      for(i=0;i<XTROOT->t_comp;i++){
	for(j=0;j<XTROOT->comp[i].nrepeats;j++){
	  for(k=i;k<i+XTROOT->comp[i].nitems;k++){
	    if(ret > _ftmpi_btype[XTROOT->comp[k].dt]->size * 
	       XTROOT->comp[k].count){
	      *cnt+=XTROOT->comp[k].count;
	      ret -= _ftmpi_btype[XTROOT->comp[k].dt]->size * 
		XTROOT->comp[k].count;
	    }
	    else {
	      *cnt+=ret/_ftmpi_btype[XTROOT->comp[k].dt]->size;
	      ret = 0;
	      return(ret);
	    } 
	  }
	}
      }
    }
    else {
      etemp = XTROOT->first_e;
      while(etemp!= NULL || ret > 0){
	if(etemp->dt <= FTMPI_DDT_B_DT_MAX){
	  if(etemp->count*etemp->size <= ret){
	    *cnt+= etemp->count;
	    ret -= etemp->count*etemp->size;
	  }
	  else {
	    *cnt += ret/etemp->size;
	    ret = ret%etemp->size;
	  }
	}
	else {
	  ret -= ftmpi_ddt_get_element_count(ddt,ret,cnt,
					     ftmpi_ddt_get_root(etemp->dt),
					     etemp->count);
	}
	etemp = etemp->next;
      }
    }
  }
  else {
    xtemp = ddt_ptr;
    if(ftmpi_mpi_type_size(xtemp->dt,&size) != 0) return(-1);
    if(size == 0) return 0;
    
    for(j=1;j<FTMPI_DDT_B_DATATYPE_TOTAL;j++){
      e_cnt += XTROOT->type_count[j];
    }
    
    /*** FITS IN COMPLETELY ***/
    if(size*ddt_cnt <= rsize){  
      *cnt +=e_cnt*ddt_cnt;
      return size*ddt_cnt;
    }
    
    ret = rsize;
    
    if(XTROOT->t_comp != -1){
      for(i=0;i<XTROOT->t_comp;i++){
	for(j=0;j<XTROOT->comp[i].nrepeats;j++){
	  for(k=i;k<i+XTROOT->comp[i].nitems;k++){
	    if(ret > _ftmpi_btype[XTROOT->comp[k].dt]->size * 
	       XTROOT->comp[k].count){
	      *cnt+=XTROOT->comp[k].count;
	      ret -= _ftmpi_btype[XTROOT->comp[k].dt]->size * 
		XTROOT->comp[k].count;
	    }
	    else {
	      *cnt+=ret/_ftmpi_btype[XTROOT->comp[k].dt]->size;
	      return(rsize-(ret%_ftmpi_btype[XTROOT->comp[k].dt]->size));
	    } 
	  }
	}
      }
    }
    else {
      etemp = XTROOT->first_e;
      while(etemp!= NULL || ret > 0){
        if(etemp->dt <= FTMPI_DDT_B_DT_MAX){
          if(etemp->count*etemp->size <= ret){
            *cnt += etemp->count;
            ret  -= etemp->count*etemp->size;
          }
          else {
            *cnt += ret/etemp->size;
            ret   = ret%etemp->size;
          }
        }
        else 
          ret -= ftmpi_ddt_get_element_count(ddt,ret,cnt,ftmpi_ddt_get_root(etemp->dt),
					     etemp->count);
        etemp = etemp->next;
      }
    }
  }
  return(rsize-ret);
}

/*************************************************************************************/
/* ftmpi_ddt_check_ddt() checks whether or not a ddt is a valid datatype*/
/*************************************************************************************/
int ftmpi_ddt_check_ddt(int ddt,int cmt)
{
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  if(ddt > 0 && ddt <= FTMPI_DDT_B_DT_MAX){
    return(1);
  }
  
  xtemp = ftmpi_ddt_get_root(ddt);
  if(xtemp == NULL)
    return 0;
  
  if(xtemp->commited == 0  && cmt == 1){
    return 0;
  }
  return 1;
}


void ftmpi_ddt_increment_dt_uses(int ddt)
{
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  if(ddt < 500) return;
  
  
  xtemp = ftmpi_ddt_get_root(ddt);
  if(xtemp != NULL){
    xtemp->uses++;
  }
}

void ftmpi_ddt_decrement_dt_uses(int ddt)
{
  FTMPI_DDT_R_ITEM * xtemp=NULL;
  if(ddt < 500) return;
  
  xtemp = ftmpi_ddt_get_root(ddt);
  if(xtemp != NULL){
    xtemp->uses--;
    if(xtemp->uses == 0){
      MPI_Type_free(&ddt);
    }
  }
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Attribute caching routines */
#ifdef GIVEITATRY
int ftmpi_type_set_attr ( MPI_Datatype type, int key )
{
  FTMPI_DDT_R_ITEM *typeptr;
  int tmp;
  
  /* allocate memory if required for the list of
     attributes for the new communicator */
  
  if(type < FTMPI_DDT_B_DT_MAX)
    typeptr = &_ftmpi_btype[type];
  else {
    typeptr = ftmpi_ddt_get_root(type);
    if(typeptr == NULL ){
      return ( MPI_ERR_TYPE );
    }
  }
  
  tmp = typeptr->nattr;
  
  if ( tmp > (typeptr->maxattr-1) ) 
    {
      if ( typeptr->attrs != NULL ) 
	{
	  /* there are already some elements set, we need to enlarge the buffer */
	  int *tmpArray, newSize = typeptr->maxattr;
	  
	  /* allocate a bigger array */
	  newSize = typeptr->maxattr + FTMPI_TYPE_ATTR_BLOCK_SIZE;
	  tmpArray = (int *)_MALLOC( newSize * sizeof(int));
	  if( tmpArray == NULL ) return ( MPI_ERR_INTERN );
	  
	  /* restore the old data into the new array and free the temporary buffer */
	  memcpy ( tmpArray, typeptr->attrs,
		   (size_t)( typeptr->maxattr * sizeof (int)));
	  _FREE( typeptr->attrs );
	  typeptr->attrs = tmpArray;
	  typeptr->maxattr = newSize;
	} 
      else 
	{
	  /* first element to be set, array is not yet allocated */
	  typeptr->maxattr += FTMPI_COMM_ATTR_BLOCK_SIZE;
	  typeptr->attrs = (int *)_MALLOC( typeptr->maxattr * sizeof(int));
	  if ( typeptr->attrs == NULL ) return ( MPI_ERR_INTERN );
	}
    }
  
  /* set now the element */
  typeptr->attrs[tmp] = key;
  typeptr->nattr++;
  
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_type_del_attr ( MPI_Datatype type, int key )
{
  int i;
  FTMPI_DDT_R_ITEM *typeptr;
  
  
  if(type < FTMPI_DDT_B_DT_MAX)
    typeptr = &_ftmpi_btype[type];
  else 
    {
      typeptr = ftmpi_ddt_get_root(type);
      if(typeptr == NULL ){
	return ( MPI_ERR_TYPE );
      }
    }
  
  /* find first the according element */
  for ( i = 0; i <typeptr->nattr; i++ )
    if ( typeptr->attrs[i] == key ) break;
  
  /* the impossible error: at this level we shouldn't have an invalid key */
  if ( i >= typeptr->nattr ) return ( MPI_ERR_INTERN );
  
  typeptr->nattr--;
  
  /* now we have to shrink the array. I hope memmove doesn't have a problem to move
     0 bytes, because this might happen....*/
  if ( typeptr->nattr > 0 )
    memmove ( &(typeptr->attrs[i]), &(typeptr->attrs[i+1]),
	      ((typeptr->nattr-i) * sizeof(int)));
  
  return ( MPI_SUCCESS );
}
#endif

/**********************************/
/*** ADVANCED ENCODING ROUTINES ***/
/**********************************/




/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ftmpi_ddt_dt_mode(int bdt,FTMPI_DDT_CODE_INFO * code_info)
{
  if(bdt == MPI_LONG){
    return code_info->mode_l;
  }
  if(bdt == MPI_LONG_DOUBLE  && ftmpi_ddt_my_longdouble.ld_flag!= code_info->ld_flag){
    return -2;
  }
  return -1;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ftmpi_ddt_get_bdt_sizes(FTMPI_DDT_CODE_INFO * code_info, int dt)
{
  if(dt == MPI_LONG && code_info->ld_flag != ftmpi_ddt_my_longdouble.ld_flag ){
    return code_info->size_l;
  }
  else if(dt == MPI_LONG_DOUBLE && 
	  code_info->ld_flag != ftmpi_ddt_my_longdouble.ld_flag){
    return code_info->size_ld;
  }
  return _ftmpi_btype[dt]->size;
  
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ftmpi_ddt_set_dt_mode(unsigned int me,unsigned int other,
			   FTMPI_DDT_CODE_INFO * code_info)
{
  int big1,big2,lng1,lng2;
  
  code_info->code_type = FTMPI_RSC;
  
#ifdef DO_FTMPI_NOE
  code_info->code_type = FTMPI_NOE;
#endif
#ifdef DO_FTMPI_SSC
  code_info->code_type = FTMPI_SSC;
#endif
#ifdef DO_FTMPI_XDR
  code_info->code_type = FTMPI_XDR;
#endif
  
  
  if(code_info->code_type == FTMPI_XDR) return;
  
  code_info->bs = 0;
  code_info->mode_l = -1;
  code_info->ld_flag = other;
  
  if(me == other){
    code_info->size_l = _ftmpi_btype[MPI_LONG]->size;
    code_info->size_ld = _ftmpi_btype[MPI_LONG_DOUBLE]->size;
    return;
  }
  
  /* this can happen for the blank mode */
  if ( other == 0 ) {
    code_info->size_l = _ftmpi_btype[MPI_LONG]->size;
    code_info->size_ld = _ftmpi_btype[MPI_LONG_DOUBLE]->size;
    return;
  }
  
  
  big1 = ftmpi_convinfo_checkmask(&me,FTMPI_ISBIGENDIAN);
  big2 = ftmpi_convinfo_checkmask(&other,FTMPI_ISBIGENDIAN);
  
  lng1 = ftmpi_convinfo_checkmask(&me,FTMPI_LONGIS64);
  lng2 = ftmpi_convinfo_checkmask(&other,FTMPI_LONGIS64);
    
  if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDEXPSIZEIS15 ))
    code_info->ld_expl = 15;
  else
    code_info->ld_expl = 11;
    
  if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDMANTDIGIS64))
    code_info->ld_sigl = 64;
  else if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDMANTDIGIS105))
    code_info->ld_sigl = 105;
  else if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDMANTDIGIS106))
    code_info->ld_sigl = 106;
  else if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDMANTDIGIS107))
    code_info->ld_sigl = 107;
  else if ( ftmpi_convinfo_checkmask (&other, FTMPI_LDMANTDIGIS113))
    code_info->ld_sigl = 113;
  else
    code_info->ld_sigl = 113;
  
  
  if(!ftmpi_convinfo_checkmask(&other,FTMPI_LDISINTEL)) code_info->ld_sigl--;
  
  if ( ftmpi_convinfo_checkmask (&other, FTMPI_LONGDOUBLEIS96))
    code_info->size_ld = 12;
  else if ( ftmpi_convinfo_checkmask (&other, FTMPI_LONGDOUBLEIS128))
    code_info->size_ld = 16;
  else
    code_info->size_ld = 8;
    
  code_info->size_l = 4;
  if(lng2 != 0) code_info->size_l = 8;
  
    
  /**********************************************/
  /******        HANDLING THE LONGS        ******/
  /**********************************************/
  if(lng2 == 0){  /* LONG IS 4 BYTES */
    if(lng1 == 0){  /* LONG IS 4 BYTES */
      if(big1 == big2){
        code_info->bs = 0;
        code_info->mode_l = -1;
      }
      else {
        code_info->bs = 1;
        code_info->mode_l = -1;
      }
    }
    else{                          /* LONG IS 8 BYTES */
      if(big1 == big2){
        code_info->bs = 0;
        if(big1 == 0) code_info->mode_l = 3; /* LITTLE ENDIAN */
        else          code_info->mode_l = 0; /* BIG ENDIAN */
      }
      else {
        code_info->bs = 1;
        if(big1 == 0) code_info->mode_l = 1; /* LITTLE ENDIAN */
        else          code_info->mode_l = 2; /* BIG ENDIAN */
      }
    }
  }
  else {                           /* LONG IS 8 BYTES */
    if(lng1 != 0){                 /* LONG IS 8 BYTES */
      if(big1 == big2){
        code_info->bs = 0;
        code_info->mode_l = -1;
      }
      else {
        code_info->bs = 1;
        code_info->mode_l = -1;
      }
    }
    else{                          /* LONG IS 4 BYTES */
      if(big1 == big2){
        code_info->bs = 0;
        if(big1 == 0) code_info->mode_l = 7; /* LITTLE ENDIAN */
        else          code_info->mode_l = 4; /* BIG ENDIAN */
      }
      else {
        code_info->bs = 1;
        if(big1 == 0) code_info->mode_l = 5; /* LITTLE ENDIAN */
        else          code_info->mode_l = 6; /* BIG ENDIAN */
          
      }
    }
  }

  /*****************************************************/
  /******        HANDLING THE LONG DOUBLES        ******/
  /*****************************************************/

  /* THIS WILL BE DONE LATER WHEN WE KNOW WHAT TO DO WITH THIS */


  return;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ftmpi_ddt_set_size ( MPI_Datatype type, int *size )
{
  FTMPI_DDT_R_ITEM * temp = NULL;
  *size = -1;
  switch(type){
    case MPI_CHAR:
      *size = sizeof(char);
      break;
    case MPI_UNSIGNED_CHAR:
      *size = sizeof(unsigned char);
      break;
    case MPI_PACKED:
    case MPI_BYTE:
      *size = sizeof(char);
      break;
    case MPI_SHORT:
      *size = sizeof(short);
      break;
    case MPI_UNSIGNED_SHORT:
      *size = sizeof(unsigned short);
      break;
    case MPI_LOGICAL:
    case MPI_INTEGER:
    case MPI_INT:
      *size = sizeof(int);
      break;
    case MPI_UNSIGNED:
      *size = sizeof(unsigned int);
      break;
    case MPI_LONG:
      *size = sizeof(long);
      break;
    case MPI_UNSIGNED_LONG:
      *size = sizeof(unsigned long);
      break;
    case MPI_REAL:
    case MPI_FLOAT:
      *size = sizeof(float);
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      *size = sizeof(double);
      break;
    case MPI_LONG_DOUBLE:
      *size = sizeof(long double);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG:
      *size = sizeof(long long);
      break;
#endif
    case MPI_UB:
      *size = 0;
      break;
    case MPI_LB:
      *size = 0;
      break;
    default:
      temp = ftmpi_ddt_get_root(type);
      if(temp == NULL){
        *size = -2;
        printf("NOT A VALID DATATYPE!! - S %d\n", type);
        return ( MPI_ERR_TYPE );
      }
      else {
        *size = temp->size;
      }
      break;
  } 
  if(*size == -2){
    printf("DDT NOT INITIALIZED -- %d --\n",type);
  } 
  return(MPI_SUCCESS);
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/

int ftmpi_ddt_set_xdr_size ( MPI_Datatype type, int *size )
{   
  *size = -1;
  switch(type){
    case MPI_CHAR:
    case MPI_UNSIGNED_CHAR:
    case MPI_BYTE:
    case MPI_PACKED:
      *size = 1;
      break;
    case MPI_SHORT:
    case MPI_UNSIGNED_SHORT:
      *size = 2;
      break;
    case MPI_LOGICAL:
    case MPI_INTEGER:
    case MPI_INT:
    case MPI_UNSIGNED:
      *size = 4;
      break;
    case MPI_LONG:
    case MPI_UNSIGNED_LONG:
      *size = 4;
      break;
    case MPI_REAL:
    case MPI_FLOAT:
      *size = 4;
      break;
    case MPI_DOUBLE_PRECISION:
    case MPI_DOUBLE:
      *size = 8;
      break;
    case MPI_LONG_DOUBLE:
      *size = sizeof(long double);
      break;
#ifdef HAVE_LONG_LONG
    case MPI_LONG_LONG:
      *size = sizeof(long long);
      break;
#endif
    case MPI_UB:
      *size = 0;
      break;
    case MPI_LB:
      *size = 0;
      break;
    default:
      printf("NOT A VALID DATATYPE!! - Sxdr %d\n", type);
      break;
  } 
  if(*size == -2){
    printf("DDT NOT INITIALIZED -- %d --\n",type);
  } 
  return(MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_mpi_type_xdr_size(MPI_Datatype type, int * size)
{
  FTMPI_DDT_R_ITEM * temp = NULL;

  if(type < FTMPI_DDT_B_DT_MAX)
    *size = _ftmpi_btype[type]->xdr_size;
  else {
    temp = ftmpi_ddt_get_root(type);
    if(temp == NULL ){
      return ( MPI_ERR_TYPE );
    }
    *size = temp->xdr_size;
  }
  return(MPI_SUCCESS);
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ftmpi_ddt_set_xdr_extent(MPI_Datatype type, MPI_Aint* extent)
{
  FTMPI_DDT_R_ITEM * temp = NULL;
  int size,ret_var;

  if(type < FTMPI_DDT_B_DT_MAX){
    ret_var = ftmpi_mpi_type_xdr_size(type,&size);
    if(ret_var == MPI_SUCCESS){
      *extent = (MPI_Aint)size;
      return(MPI_SUCCESS);
    }
    return ( MPI_ERR_TYPE );
  } 
  else { 
    temp = ftmpi_ddt_get_root(type);
    if(temp == NULL ){
      return ( MPI_ERR_TYPE );
    }
    else {
      *extent = temp->xdr_extent;
      return(MPI_SUCCESS);
    }
  }
}

/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ftmpi_ddt_set_type(int ddt,int type)
{
  FTMPI_DDT_R_ITEM * temp = NULL;

  temp = ftmpi_ddt_get_root(ddt);
  if(temp == NULL ){
    return;
  }
  TROOT->create_type = type;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ftmpi_ddt_set_args(int ddt,int ** i, MPI_Aint ** a,int * d,int type)
{
  FTMPI_DDT_R_ITEM * temp = NULL;
  int j, k;

  temp = ftmpi_ddt_get_root(ddt);
  if(temp == NULL ){
    return;
  }

  if(TROOT->i != NULL)_FREE(TROOT->i);
  if(TROOT->a != NULL)_FREE(TROOT->a);
  if(TROOT->d != NULL)_FREE(TROOT->d);

  switch(type){
    /******************************************************************/
    case MPI_COMBINER_DUP: /* DONE */
      TROOT->i = (int *)_MALLOC(sizeof(int));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);
     
      TROOT->i[0] = 0;

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    case MPI_COMBINER_CONTIGUOUS: /* DONE */
      TROOT->i = (int *)_MALLOC(sizeof(int)*2);
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      TROOT->i[0] = 1;
      TROOT->i[1] = i[0][0];

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    case MPI_COMBINER_VECTOR: /* DONE */
      TROOT->i = (int *)_MALLOC(sizeof(int)*4);
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      TROOT->i[0] = 3;
      TROOT->i[1] = i[0][0];
      TROOT->i[2] = i[1][0];
      TROOT->i[3] = i[2][0];

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    case MPI_COMBINER_HVECTOR_INTEGER: /* DONE */
    case MPI_COMBINER_HVECTOR:
      TROOT->i = (int *)_MALLOC(sizeof(int)*3);
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*2);
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      TROOT->i[0] = 2;
      TROOT->i[1] = i[0][0];
      TROOT->i[2] = i[1][0];

      TROOT->a[0] = 1;
      TROOT->a[1] = a[0][0];

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    case MPI_COMBINER_INDEXED: /* DONE */
      j = 0;
      TROOT->i = (int *)_MALLOC(sizeof(int)*((2*i[0][0])+2));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);


      TROOT->i[j++] = (2*i[0][0])+1;
      TROOT->i[j++] = i[0][0];     
      for(k=0;k<i[0][0];k++) TROOT->i[j++] = i[1][k];
      for(k=0;k<i[0][0];k++) TROOT->i[j++] = i[2][k];

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    case MPI_COMBINER_HINDEXED_INTEGER: /* DONE */
    case MPI_COMBINER_HINDEXED:
      TROOT->i = (int *)_MALLOC(sizeof(int)*(i[0][0]+2));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*(i[0][0]+1));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      j = 0;
      TROOT->i[j++] = i[0][0]+1;
      TROOT->i[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->i[j++] = i[1][k];

      j = 0;
      TROOT->a[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->a[j++] = a[0][k];

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];

      break;
    /******************************************************************/
    case MPI_COMBINER_INDEXED_BLOCK: /* DONE */
      TROOT->i = (int *)_MALLOC(sizeof(int)*(i[0][0]+3));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      j=0;
      TROOT->i[j++] = (i[0][0])+2;
      TROOT->i[j++] = i[0][0]; 
      TROOT->i[j++] = i[1][0];
      for(k=0;k<i[0][0]+1;k++) 
	TROOT->i[j++] = i[2][k];

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];

      break;
    /******************************************************************/
    case MPI_COMBINER_STRUCT_INTEGER: /* DONE */
    case MPI_COMBINER_STRUCT:
      TROOT->i = (int *)_MALLOC(sizeof(int)*(i[0][0]+2));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*(i[0][0]+1));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*(i[0][0]+1));

      j=0;
      TROOT->i[j++] = i[0][0]+1;
      TROOT->i[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->i[j++] = i[1][k];
     
      j = 0;
      TROOT->a[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->a[j++] = a[0][k];

      j = 0;
      TROOT->d[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->d[j++] = d[k];

      break;
    /******************************************************************/
    case MPI_COMBINER_SUBARRAY: /* DONE */
      TROOT->i = (int *)_MALLOC(sizeof(int)*((3*i[0][0])+3));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      j=0;
      TROOT->i[j++] = (3*i[0][0])+2;
      TROOT->i[j++] = i[0][0];
      for(k=0;k<i[0][0];k++) 
	TROOT->i[j++] = i[1][k];
      for(k=0;k<i[0][0];k++) 
	TROOT->i[j++] = i[2][k];
      for(k=0;k<i[0][0];k++) 
	TROOT->i[j++] = i[3][k];
      TROOT->i[j++] = i[4][0];

      TROOT->a[0] = 0;
 
      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];

      break;
    /******************************************************************/
    case MPI_COMBINER_DARRAY:
      TROOT->i = (int *)_MALLOC(sizeof(int)*((4*i[0][0])+5));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      j=0;
      TROOT->i[j++] = (4*i[0][0])+4;
      TROOT->i[j++] = i[0][0];
      TROOT->i[j++] = i[1][0];
      TROOT->i[j++] = i[2][0];

      for(k=0;k<i[2][0];k++) 
	TROOT->i[j++] = i[3][k];
      for(k=0;k<i[2][0];k++) 
	TROOT->i[j++] = i[4][k];
      for(k=0;k<i[2][0];k++) 
	TROOT->i[j++] = i[5][k];
      for(k=0;k<i[2][0];k++) 
	TROOT->i[j++] = i[6][k];
      TROOT->i[j++] = i[7][0];

      TROOT->a[0] = 0;

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];

      break;
    /******************************************************************/
    case MPI_COMBINER_F90_REAL:
    case MPI_COMBINER_F90_COMPLEX:
      TROOT->i = (int *)_MALLOC(sizeof(int)*3);
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype));

      TROOT->i[0] = 2;
      TROOT->i[1] = i[0][0];
      TROOT->i[2] = i[1][0];

      TROOT->a[0] = 0;

      TROOT->d[0] = 0;
      break;
    /******************************************************************/
    case MPI_COMBINER_F90_INTEGER:
      TROOT->i = (int *)_MALLOC(sizeof(int)*2);
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint));
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype));

      TROOT->i[0] = 1;
      TROOT->i[1] = i[0][0];

      TROOT->a[0] = 0;

      TROOT->d[0] = 0;
      break;
    /******************************************************************/
    case MPI_COMBINER_RESIZED:
      TROOT->i = (int *)_MALLOC(sizeof(int));
      TROOT->a = (MPI_Aint *)_MALLOC(sizeof(MPI_Aint)*3);
      TROOT->d = (MPI_Datatype *)_MALLOC(sizeof(MPI_Datatype)*2);

      TROOT->i[0] = 0;

      TROOT->a[0] = 2;
      TROOT->a[1] = a[0][0];
      TROOT->a[2] = a[1][0];

      TROOT->d[0] = 1;
      TROOT->d[1] = d[0];
      break;
    /******************************************************************/
    default:
    break;
  }

  TROOT->create_type = type;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ftmpi_ddt_get_args(MPI_Datatype ddt, int which,
		       int * ci, int * i,
		       int * ca, MPI_Aint * a,
		       int * cd, MPI_Datatype * d, int * type)
{
  FTMPI_DDT_R_ITEM * temp = NULL;
  int k;


  if(ddt < FTMPI_DDT_B_DATATYPE_TOTAL){
    switch(which){
      case 0:
        *ci = 0;
        *ca = 0;
        *cd = 0;
        *type = MPI_COMBINER_NAMED;
        break;
      default:
/*         printf("ERRROR --- INVALID OPTION  ON FTMPI_DDT_GET_ARGS\n"); */
        return MPI_ERR_INTERN;
        break;
    }
    return(MPI_SUCCESS);
  }

  temp = ftmpi_ddt_get_root(ddt);
  if(temp == NULL ){
    return MPI_ERR_TYPE;
  }

  switch(which){
    case 0:     /* GET THE LENGTHS */
      *ci = TROOT->i[0];
      *ca = TROOT->a[0];
      *cd = TROOT->d[0];
      *type = TROOT->create_type;
      break;
    case 1:     /* GET THE ARGUMENTS */
      if(*ci < TROOT->i[0] || *ca < TROOT->a[0] || *cd < TROOT->d[0]) 
	return MPI_ERR_ARG;

      for(k=1;k<TROOT->i[0]+1;k++) 
	i[k-1] = TROOT->i[k];
      for(k=1;k<TROOT->a[0]+1;k++) 
	a[k-1] = TROOT->a[k];
      for(k=1;k<TROOT->d[0]+1;k++) 
	d[k-1] = TROOT->d[k];

      break;
    default:
      printf("ERRROR --- INVALID OPTION  ON FTMPI_DDT_GET_ARGS\n");
      return MPI_ERR_INTERN;
      break;
  }
  return(MPI_SUCCESS);
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
    
/* The IEEE754 floating point representation.
** The following statement are for long double numbers and of course
** are not standards (as expected). But there are the exceptional numbers in
** the logical representation for the long double numbers:
** normal number (0<e<32767): (-1)Sign2 (exponent - 16383)1.f
** subnormal number (e=0, f!=0): (-1)Sign2 (-16382)0.f
** zero (e=0, f=0): (-1)Sign0.0
** signaling NaN s=u, e=32767(max); f=.0u-uu; at least one bit must be nonzero
** quiet NaN s=u, e=32767(max); f=.1uuu-uu
** Infinity s=u, e=32767(max); f=.0000-00 (all zeroes)
*********************************************************
** Now I find 2 different implementation: Sparc and Intel. The main
** difference (except the endianess problem) is that on
** the SPARC implementation the first digit of
** the significant is implicit, but not on Intel. So on all Intel
** architectures we lose one bit of precision...
**/

#ifndef MIN
#define MIN(A,B)  ((A)<(B) ? (A) : (B))
#endif  /* MIN */

int ftmpi_ddt_copy_bits( unsigned int* dest,
         unsigned int dbit,
         unsigned int* src,
         unsigned int sbit,
         unsigned int length,
         int direction,
         int implicit,
         int implicit_bit_value )
{
  int i, sindex = 0, dindex = 0;
  unsigned int dmask, smask;

  printf( "copy %d bits direction %d implicit = %d implicit_bit = %d\n",
	  length, direction, implicit, (implicit_bit_value ? 1 : 0) );
  dmask = (1 << dbit);
  smask = (1 << sbit);
  if( implicit != 0 ) {
    if( implicit == 1 ) {
      smask >>= 1;
      if( smask == 0 ) {
	sindex += direction;
	smask = (((unsigned int)1) << 31);
      }
    } else {
      dmask >>= 1;
      if( dmask == 0 ) {
	dindex += direction;
	dmask = (((unsigned int)1) << 31);
      }
      if( implicit_bit_value ) dest[dindex] |= dmask;
    }
    length--; /* done with one */
  }
  for( i = 0; i < length; i++ ) {
    dmask >>= 1;
    if( dmask == 0 ) {
      dindex += direction;
      dmask = (((unsigned int)1) << 31);
    }
    smask >>= 1;
    if( smask == 0 ) {
      sindex += direction;
      smask = (((unsigned int)1) << 31);
    }
    if( src[sindex] & smask )
      dest[dindex] |= dmask;
  }
  return 0;
}

/* #define eprintf printf */
#define eprintf my_empty_fct
void my_empty_fct( char* fmt, ... ) {}

static void dump_memory_order( unsigned char* pc, unsigned int length )
{
  unsigned int dumped = 0;
  
  while( length ) {
    printf( "%02x", *pc );
    dumped++;
    if( dumped == 4 ) {
      printf( " " ); dumped = 0;
    }
    length--; pc++;
  }
  printf( "\n" );
}

static void swap_bytes( unsigned int* pui )
{
  unsigned char *pc = (unsigned char*)pui, cswap;
  cswap = pc[0]; pc[0] = pc[3]; pc[3] = cswap;
  cswap = pc[1]; pc[1] = pc[2]; pc[2] = cswap;
}

/* This function convert between big endian and little endian
 * representations. Their only job is to put the number in memory
 * in the other representation (from big to little and of
 * course from little to big).
 */
static void swap_words( unsigned int* ptr, unsigned int length )
{
  unsigned int uitemp, i;

  length--; /* small trick to use length as the swapping index */
  /* just < to avoid double conversion for the middle word */
  for( i = 0; i < length; i++, length-- ) {
    swap_bytes( &(ptr[i]) );
    swap_bytes( &(ptr[length]) );
    uitemp = ptr[i];
    ptr[i] = ptr[length];
    ptr[length] = uitemp;
  }
  if( i == length ) swap_bytes( &(ptr[i]) );
}

#define isBigEndian(DDT_DECODE) (ftmpi_convinfo_checkmask(&((DDT_DECODE).ld_flag), FTMPI_ISBIGENDIAN))
#define useIntelRepresentation(DDT_DECODE) (ftmpi_convinfo_checkmask(&((DDT_DECODE).ld_flag), FTMPI_LDISINTEL))

int
ftmpi_ddt_convert_long_double( char* buffer,      /* source buffer */
			       long double* pld,  /* destination buffer */
			       int count,         /* number of long double */
			       FTMPI_DDT_CODE_INFO * remote_info ) /* remote info */
{
/*   ftmpi_ddt_my_longdouble -- LONG DOUBLE DEFINITION FOR MY ARCH */

  unsigned int* puir = (unsigned int*)buffer;
  unsigned int* puil = (unsigned int*)pld;
  unsigned int value, rsign, mask, lexp, rexp;
  unsigned int ldepl, rdepl;
  unsigned int rmax_exp, lmax_exp, shift_sign, i;
  int lindex, rindex, direction = 1, add_sign = 0;

  if( isBigEndian(*remote_info) != isBigEndian(ftmpi_ddt_my_longdouble) ) {
    /* if they have a different endianness */
    eprintf( "swap words => now both have the same endian\n" );
    swap_words( (unsigned int*)buffer, remote_info->size_ld / sizeof(unsigned int) );
  }
  /* check if we need to add the 1 before the significand.
   * add_sign will be 0 if we same similars architectures,
   * -1 if the remote architecture is a sparc and the local is an intel
   * and 1 if remote is intel and local sparc.
   */
  add_sign = (useIntelRepresentation(*remote_info) - 
	      useIntelRepresentation(ftmpi_ddt_my_longdouble));
  eprintf( "add_sign = %d %08x\n", add_sign, add_sign );
  dump_memory_order( (unsigned char*)buffer, remote_info->size_ld );
  /* lets compute local and remote index of exponent and shift */
  ldepl = (ftmpi_ddt_my_longdouble.ld_sigl % (sizeof(unsigned int) << 3));
  rdepl = (remote_info->ld_sigl % (sizeof(unsigned int) << 3));
  rindex = ((remote_info->size_ld << 3) - 
	    (remote_info->ld_sigl + remote_info->ld_expl)) >> 5;
  lindex = ((ftmpi_ddt_my_longdouble.size_ld << 3) - 
	    (ftmpi_ddt_my_longdouble.ld_sigl + ftmpi_ddt_my_longdouble.ld_expl)) >> 5;
  rmax_exp = (1 << (remote_info->ld_expl - 1)) - 1;
  lmax_exp = (1 << (ftmpi_ddt_my_longdouble.ld_expl - 1)) - 1;
  shift_sign = ldepl - (rdepl-1);
  if( !isBigEndian(ftmpi_ddt_my_longdouble) ) { /* if little endian */
    puir = puir + (remote_info->size_ld >> 2) - 1;
    puil = puil + (ftmpi_ddt_my_longdouble.size_ld >> 2) - 1;
   /*  rindex = -rindex; */
/*     lindex = -lindex; */
    direction = -1;
  }

  /* dump all those informations */
  eprintf( "Local  info: ldepl = %d, lindex = %d lmax_exp = %d\n", ldepl, lindex, lmax_exp );
  eprintf( "Remote info: rdepl = %d, rindex = %d rmax_exp = %d\n", rdepl, rindex, rmax_exp );

  /* where the remote exponent start */
  if( (rindex >> 5) != ((rindex - remote_info->ld_expl) >> 5) ) {
    eprintf( ">> error exponent on 2 words significand length %d bits\n",
	     remote_info->ld_sigl );
    eprintf( ">> index = %d exponent is %d bits\n", rindex, remote_info->ld_expl );
  }

  for( i = 0; i < count; i++ ) {
    /* lets compute the remote sign and exponent */
    value = puir[rindex] >> rdepl;
    mask = 1 << remote_info->ld_expl;
    rsign = value & mask; /* here we got the remote sign */
    mask--; /* create the mask for the exponent */
    eprintf( "initial data %08x mask = %08x  value = %08x (middle %x)\n",
	     puir[rindex], mask, value, rmax_exp );
    rexp = (value & mask) - rmax_exp;
    eprintf( "remote number is %s and has an exponent = %d\n",
	     (rsign ? "negatif" : "positif"), rexp );
    lexp = lmax_exp + rexp;
    eprintf( "local E = %d\n", lexp );
    /* now check if the local exp can be represented */
    /* lets copy the significand */
    /* if the local exponent is 0 we have a subnormal number so
     * we dont need to add the leading bit as it's already set to 0.
     */
    ftmpi_ddt_copy_bits( puil + lindex, ldepl,
			 puir + rindex, rdepl,
			 MIN( remote_info->ld_sigl, ftmpi_ddt_my_longdouble.ld_sigl ),
			 direction, add_sign, (lexp != 0) );
    /* lets put the sign along with the exponent */
    if( rsign ) lexp |= ( 1 << ftmpi_ddt_my_longdouble.ld_expl );
    /* now lets shitf the exponent to the right place */
    lexp <<= ldepl;
    /* and add it to the long double number in the right position */
    puil[lindex] |= lexp;
    buffer += remote_info->size_ld;
    pld += 1;
  }
  return 0;
}
