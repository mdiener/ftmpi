/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Antonin Bukovsky <tone@cs.utk.edu>
			Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include "ft-mpi-f2c-p2p.h"
#include "ft-mpi-fprot.h"

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Bsend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm); */
void mpi_bsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
	       int * comm,int * __ierr)
{
 *__ierr = MPI_Bsend((void *)ToPtr(buf),(*((int *)cnt)),
		     (MPI_Datatype)fdt2c((*((int *)ddt))),(*((int*)dest)),
		     (*((int*)tag)),(MPI_Comm)(*((int*) comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_send_(void * buf,int * count,int * datatype,int *dest,int *tag,
int * comm,int * __ierr )
{
  *__ierr = MPI_Send((void *)ToPtr(buf),(*((int *)count)), 
		     (MPI_Datatype)(*((int *)datatype)),(*((int *)dest)),
		     (*((int *)tag)), (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_recv_(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr)
{
  *__ierr = MPI_Recv((void *)ToPtr(buf),(*((int *)count)), 
		     (MPI_Datatype)(*((int *)datatype)),
		     (*((int *)source)),
		     (*((int *)tag)), (MPI_Comm)(*((int*)comm)),
		     (MPI_Status *)ToPtr(status));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mpi_sendrecv_(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr )
{
  
  *__ierr = MPI_Sendrecv((void*)ToPtr(sendbuf),(*((int *)sendcount)), 
			 (MPI_Datatype)(*((int*)sendtype)), 
			 (*((int*)dest)),(*((int*)sendtag)),
			 (void*)ToPtr(recvbuf),(*((int*)recvcount)),
			 (MPI_Datatype)(*((int*)recvtype)),
			 (*((int*)source)),
			 (*((int*)recvtag)), 
			 (MPI_Comm)ToPtr(*((int*)comm)),
			 (MPI_Status *)ToPtr(status)); 
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Probe(int source,int tag,MPI_Comm comm,MPI_Status * status) */
void mpi_probe_(int *source,int *tag,int *comm,int *status,int  *__ierr)
{
  *__ierr = MPI_Probe((*((int *)source)),(*((int *)tag)),
		      (MPI_Comm)(*((int *)comm)),
		      (MPI_Status *)ToPtr(status));
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Rsend(void * buf,int cnt,MPI_Datatype ddt, int dest,int tag,
   MPI_Comm comm); */
void mpi_rsend_(void * buf,int * cnt,int * ddt, int * dest,int * tag,
	       int * comm,int * __ierr)
{
 *__ierr = MPI_Rsend((void *)ToPtr(buf),(*((int *)cnt)),
		     (MPI_Datatype )fdt2c((*((int *)ddt))),
		     (*((int*)dest)),(*((int*)tag)),
		     (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Sendrecv_replace(void *buf,int cnt,MPI_Datatype ddt,int dest,
   int sendtag,int source,int recvtag,MPI_Comm comm,MPI_Status *status)*/
void mpi_sendrecv_replace_(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr)
{
  *__ierr = MPI_Sendrecv_replace((void *)ToPtr(buf),(*((int *)cnt)),
				 (MPI_Datatype)fdt2c((*((int *)ddt))),
				 (*((int *)dest)),(*((int *)sendtag)),
				 (*((int *)source)),(*((int *)recvtag)),
				 (MPI_Comm)(*((int *)comm)),
				 (MPI_Status *)ToPtr(status));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Ssend(void * buf,int cnt,MPI_Datatype ddt,int dest,int tag,
   MPI_Comm comm); */
void mpi_ssend_(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
	       int * __ierr)
{
  *__ierr = MPI_Ssend((void *)ToPtr(buf),(*((int *)cnt)),
		      (MPI_Datatype)fdt2c((*((int *)ddt))),
		      (*((int*)dest)),(*((int*)tag)),
		      (MPI_Comm)(*((int*)comm)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Buffer_attach(void * buffer,int size) */
void mpi_buffer_attach_(void * buffer,int * size,int * __ierr)
{
  *__ierr = MPI_Buffer_attach((void *)ToPtr(buffer),(*((int *)size)));
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* int MPI_Buffer_detach(void * buffer,int * size) */
void mpi_buffer_detach_(void * buffer,int * size,int * __ierr)
{
  *__ierr = MPI_Buffer_detach((void *)ToPtr(buffer),size);
}


