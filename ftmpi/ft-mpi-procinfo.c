
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* code for handling proc tables in the form of chained hash tables */

/* proc tables are used to store info that translates a proc gid to */
/* connection records, connecton state, proc state, etc etc */
/* i.e. much of what is currently in the com structures will end up here */

#include <stdio.h>

#include "ft-mpi-ddt-sys.h" 

/* local includes */
#ifndef LISTSH
#include "ft-mpi-list.h"
#endif

#ifndef _FT_MPI_PROCLIST_H
#include "ft-mpi-procinfo.h"
#endif

#include "debug.h"

/* storage is local to this module */

static ftmpi_procinfo_t** ftmpi_procinfo_table;
static int* ftmpi_procinfo_table_count;
static int ftmpi_procinfo_m;

/* local prototypes not to appear in the header file */
ftmpi_procinfo_t* ftmpi_procinfo_list_head (ftmpi_procinfo_t* pl_head);
void ftmpi_procinfo_dump_list (int index, int v);


/*
 * This routine creates a htable for the proc info
 * Each entry points to a proc info empty linked list
 *
 * 'm' is the hash height/width etc
 *
 */

int ftmpi_procinfo_init_htable (int m)
{
/* here we create the table. 
 * for each entry we create an actual linked list head and reset the count
 */

  int i;

  if (m<=32) ftmpi_procinfo_m = m;
  else
    if (m<=64) ftmpi_procinfo_m = 32;
    else
      ftmpi_procinfo_m = 128;
  
  /* malloc the front end table */
  ftmpi_procinfo_table = (ftmpi_procinfo_t**)_MALLOC
    (sizeof(ftmpi_procinfo_t*)*ftmpi_procinfo_m);
  
  /* the count of entries */
  ftmpi_procinfo_table_count = (int*)_MALLOC
    (sizeof(int)*ftmpi_procinfo_m);
  
  /* setup the link lists and counts */
  for (i=0;i<ftmpi_procinfo_m;i++) {
    ftmpi_procinfo_table_count[i] = 0;
    ftmpi_procinfo_table[i] = 
      (ftmpi_procinfo_t *)_MALLOC(sizeof(ftmpi_procinfo_t));
    ftmpi_procinfo_table[i]->pl_link = ftmpi_procinfo_table[i]->pl_rlink 
      = ftmpi_procinfo_table[i];
  }
  
  return (0);
}


/* this routine dumps debug procinfo */
int ftmpi_procinfo_debug (int v)
{
  int i;
  printf("ftmpi_procinfo_debug: table size %d\n", ftmpi_procinfo_m);
  if (v==0) printf("dumping counts only\n");
  if (v==1) printf("dumping counts and actual GIDs\n");
  if (v==2) printf("dumping counts and actual GID and CONVINFO pairs\n");
  
  for (i=0;i<ftmpi_procinfo_m;i++) {
    printf ("%d\t: count %d\t: ", i, ftmpi_procinfo_table_count[i]);
    /* dump gid list here */
    if (v) ftmpi_procinfo_dump_list (i, v);
    printf ("\n");
  }
  
  return (0);
}




/* linked list calls */

/* 
 * NOT NEEDED 
 *
ftmpi_procinfo_t* ftmpi_procinfo_list_tail (pl_head)
   ftmpi_procinfo_t *pl_head;
{
       if (pl_head->pl_rlink != pl_head) return (pl_head->pl_rlink);
	       else return ((struct ftmpi_procinfo_t *)0);
}
 */

/* this is used only locally for searches and frees etc etc */
ftmpi_procinfo_t* ftmpi_procinfo_list_head (ftmpi_procinfo_t* pl_head)
{
  if (pl_head->pl_link != pl_head) return (pl_head->pl_link);
  else return ((ftmpi_procinfo_t *)0);
}


/*
 * This routine creates a new procinfo_t and adds it to the correct location in the hashtable linked list 
 * For this to work we need to know the GID of the proc in advance
 */
ftmpi_procinfo_t* ftmpi_procinfo_new (int gid)
{
  int index;
  ftmpi_procinfo_t *plp;
  
  if (gid<0) return ((ftmpi_procinfo_t*)0);
  index = ftmpi_procinfo_index(gid);
  if (!(plp = TALLOC(1, ftmpi_procinfo_t, 0))) {
    fprintf(stderr, "procinfo_list_new() can't get memory\n");
    exit(1);
  }
  
  LISTPUTBEFORE(ftmpi_procinfo_table[index], plp, pl_link, pl_rlink);
  
  /* now set a few things for the user */
  plp->pl_gid = gid;	/* we know this */
  plp->pl_convinfo = 0;	/* but not these yet */
  plp->pl_mcw_rank =-1;
  /* plp->pl_conn =NULL; */
  plp->pl_conn_index =-1;
  
  /* update entry counts */
  ftmpi_procinfo_table_count[index]++;
  
  return plp;
}

/*
 * This routine takes a pointer to a procinfo element, and removes it from
 * the hash/table/list and frees it as well
 */
void ftmpi_procinfo_free (ftmpi_procinfo_t* plp)
{
  int index;

  index = ftmpi_procinfo_index(plp->pl_gid);	/* calc index before freeing! */
  LISTDELETE(plp, pl_link, pl_rlink);
  FREE(plp);
  
  /* update entry counts */
  if ((index>=0)&&(index<ftmpi_procinfo_m)) ftmpi_procinfo_table_count[index]--;
}

/*
 * This routine takes a GID, and removes it from
 * the hash/table/list and frees it as well
 */
void ftmpi_procinfo_free_by_gid (int id)
{
  int index;
  ftmpi_procinfo_t* plp;

  plp = ftmpi_procinfo_find_by_gid (id);
  if (!plp) return;
 
  LISTDELETE(plp, pl_link, pl_rlink);
  FREE(plp);
  
  /* update entry counts */
  index = ftmpi_procinfo_index(id);
  ftmpi_procinfo_table_count[index]--;
}



/* this routine destroys the WHOLE table and all contents */
/* after calling this you must call init again or really exit */
void ftmpi_procinfo_table_destroy ()
{
  ftmpi_procinfo_t *pl_head;
  ftmpi_procinfo_t *plp;
  int i;
  
  /* loop for each linked list */
  for(i=0;i<ftmpi_procinfo_m;i++) {
    /* get each head of linked list */
    pl_head = ftmpi_procinfo_table[i];
    /* loop taking the head and freeing it, when no head, throw empty */
    /* record place holder away as well and then return */
    while (pl_head) {
      plp = ftmpi_procinfo_list_head (pl_head);
      if (plp) ftmpi_procinfo_free (plp);
      else {
	_FREE((char *) pl_head);	/* free empty element at head */
	pl_head=(ftmpi_procinfo_t*)0;	/* break out of loop */
      }   
    } /* while a head of list */
  } /* for each list */
  
  /* free the table and table counts and then reset M value */
  _FREE(ftmpi_procinfo_table);
  _FREE(ftmpi_procinfo_table_count);
  ftmpi_procinfo_m = 0;
}


/*
 * This is the main search routine i.e. the point of this whole module
 */

ftmpi_procinfo_t* ftmpi_procinfo_find_by_gid (int id)
{
  ftmpi_procinfo_t *pl_head;
  ftmpi_procinfo_t *plp;

  if (id<0) return ((ftmpi_procinfo_t*)0);

  pl_head = ftmpi_procinfo_head(id);	/* get head of list */

  for (plp = pl_head->pl_link; plp != pl_head; plp = plp->pl_link)
    if((id==plp->pl_gid)) return( plp );

  /* if none of them, then ops! */
  return (ftmpi_procinfo_t*)0; /* no match, sorry */
}



/*
 * this dumps the contents of an individual list 
 */
void ftmpi_procinfo_dump_list (int index, int v)
{
  ftmpi_procinfo_t *pl_head;
  ftmpi_procinfo_t *plp;

  pl_head = ftmpi_procinfo_table[index];	/* get head of list */

  for (plp = pl_head->pl_link; plp != pl_head; plp = plp->pl_link) {
    if (v==1)
      printf("%d :",plp->pl_gid);
    if (v==2)
      printf("%d conv 0x%x : ",plp->pl_gid, plp->pl_convinfo);
  }
}



/*
 * this routine updates the ddt_decode info stored in the procinfo structure
 * it loops through the array of gids give (upto 'num' of them) setting
 * the DDT info values using the local myconv value 
 *
 * GEF July03
 */

int ftmpi_procinfo_set_ddt_code_info (int* gids, int num, unsigned int myconv)
{
  int i;
  int same = 1;
  ftmpi_procinfo_t *plp;

  for (i=0;i<num;i++) {
    plp = ftmpi_procinfo_find_by_gid (gids[i]);
    ftmpi_ddt_set_dt_mode(myconv,plp->pl_convinfo,&(plp->pl_code_info));
    if(myconv != plp->pl_convinfo) same = 0;
  }
#ifndef DO_FTMPI_NOE
  if(same == 1){
    for (i=0;i<num;i++) {
      plp = ftmpi_procinfo_find_by_gid (gids[i]);
      plp->pl_code_info.code_type = FTMPI_NOE;
    }
  }
#endif

  return (0);
}

/*
 * this routine updates the ddt_decode info stored in the procinfo structure
 * it loops through the array of gids give (upto 'num' of them) setting
 * the DDT info values using the local myconv value 
 *
 * GEF July03
 */
int ftmpi_procinfo_add_and_set_all_info (int* gids, int* gcaddr, int* gcport, 
	                      int* gsport, int* gconvs, int num, unsigned int myconv)
{
  int i;
  int same = 1;
  ftmpi_procinfo_t *plp;

  for (i=0;i<num;i++) {
    plp = ftmpi_procinfo_find_by_gid (gids[i]);
    if (plp) { /* make sure decode_info is set */
      ftmpi_ddt_set_dt_mode(myconv,plp->pl_convinfo,&(plp->pl_code_info));
      /* also the mcw rank might have changed */
      plp->pl_mcw_rank = i;
      if(myconv != plp->pl_convinfo) same = 0;
    }
    else { /* need to create a new record structure */
      plp = ftmpi_procinfo_new (gids[i]);
      plp->pl_conn_addr = gcaddr[i];
      plp->pl_conn_p = 	gcport[i];
      plp->pl_state_p = 	gsport[i];
      plp->pl_event_p = 0;
      plp->pl_convinfo = 	gconvs[i];
      ftmpi_ddt_set_dt_mode(myconv,plp->pl_convinfo,&(plp->pl_code_info));
      plp->pl_conn_index = -1;
      /* 		plp->pl_conn; */
      plp->pl_mcw_rank = i;
      if(myconv != plp->pl_convinfo) same = 0;
    }
  }
#ifndef DO_FTMPI_NOE
  if(same == 1){
    for (i=0;i<num;i++) {
      plp = ftmpi_procinfo_find_by_gid (gids[i]);
      plp->pl_code_info.code_type = FTMPI_NOE;
    }
  }
#endif

  return (0);
}

/*
 * This routine gets a pointer to a DDT decode info structure for a given gid 
 * if its not known it returns NULL/0
 *
 * GEF July03
 */
FTMPI_DDT_CODE_INFO* ftmpi_procinfo_get_ddt_code_info (int gid)
{
  ftmpi_procinfo_t *plp;
 
 plp = ftmpi_procinfo_find_by_gid (gid);
 if (!plp) return ((FTMPI_DDT_CODE_INFO*)NULL);
 else
   return (&(plp->pl_code_info));
}

