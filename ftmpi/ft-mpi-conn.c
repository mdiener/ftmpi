 
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS FT-MPI-CONN Connection management library

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Author:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* And not its not cut and pasted */

#include "mpi.h"
#include "ft-mpi-sys.h"
#include "snipe_lite.h"
#include "snipe2.h"
#include "notifier.h"
#include "msgbuf.h"
#include <stdio.h>
#include "ft-mpi-fifo.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-req-list.h"
#include "ft-mpi-nb.h"
#include "debug.h"

/* IMA_LINUX and need to block sigpipe or else I can die in the wrong place */
#ifdef IMA_LINUX
#include <signal.h>
#endif 

#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>

/* include our own prototypes and data structures */
#include "ft-mpi-conn.h"

#define	POLL 0 		/* poll in check_for_cons */
#define FOREVER 1	/* wait for a connection in check_for_cons */

conn_info_t conn [MAXPERAPP];
int connfree = MAXPERAPP;

/* Other globals */
static int myid, myaddr, mycs, myes;	/* my gid, addr, conn sock, error sock */
static int cons=0;	/* how many connections do I currently have */
static int outstanding=0; /* how many have not sent an XON/XOFF ? */
static int xons=0;	/* recd xons */
static int xoffs=0;/* recd xoffs */
static int sxons=0; /* how many xons have I sent */
static int sxoffs=0; /* how many xoffs have I sent */
static int allowed=0; /* how many can I send to */
static int recving=1;	/* Am I currently receving */

/* make err_d global so it can be checked without a function call */
int ftmpi_err_d=0;	/* error detected ? */

char tmpbuffer[1024];
int tmplen;

/* fairness var for mpi_any_source calls */
static int fairnext=0;

/* headers and other temp values */
msg_hdr_t	rhdr;	/* last recvd header */
msg_hdr_t	shdr;	/* last sent header */
msg_hdr_t	xhdr;	/* temp XON/XOFF/XALT header */

/* low level driver globals */
static int snipe2con_error;		/* connection for error detect */
static snipe2_msg_list_t *snipe2con_error_req; /* req to catch events */
static int snipe2con_newconn;	/* connection for new connection attempt */
static snipe2_msg_list_t *snipe2con_newconn_req; /* req to catch connects */

extern fifo_t* progress_queue;	/* from nb.c */

/* if we send an event on a connection failure */
static int conn_fail_sends_event = 0;	


/* callback functions must have prototypes */
int conn_snipe2_event_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_newconnection_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_recvdata_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_recvtrunc_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_recvhdr_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_connerror_callback (SNIPE2CALLBACKPROTO);
int conn_snipe2_closeconn_callback (SNIPE2CALLBACKPROTO);
int conn_sentflowcontrol_callback (SNIPE2CALLBACKPROTO);


int conn_enable_snipe2_messaging (int con);

/* start of the real code */



int ftmpi_conn_init () {
int i;

for(i=0;i<MAXPERAPP;i++) {
	conn_clr_entry (i);
	}

connfree = MAXPERAPP-1;	/* -1 as entry 0 is always kept */
conn[0].inuse = 1;

/* build the xhdr header */
make_hdr (xhdr, 0, ftmpi_sys_get_gid(), 0, 0, 0, 0, 0, 0, 0);
/* from now on we just fill in the fields as needed */
/* THIS no longer makes sense with the trans_hdr_ calls */

/* And now something completely different and bad */
/* Under Linux we have problems with a sudden death of a socket */
/* causing a signal SIGPIPE which we don't catch and it kills us */
/* so here we simply ignore it. GEF */
#ifdef IMA_LINUX
#ifdef VERBOSE
	printf("ftmpi_conn_init: blocking SIGPIPE\n");
#endif /* VERBOSE */
	(void)signal(SIGPIPE, SIG_IGN);
#endif /* IMA_LINUX */

return (0);
}

/* find one not in use, if none found returns 0 */
int find_free_conn () {
int i;

for(i=1;i<MAXPERAPP;i++) 
	if (!conn[i].inuse) return (i);
return (0);
}

int conn_clr_entry (int e)
{

conn[e].inuse = 0;
conn[e].gid = 0;
conn[e].addr = 0;
conn[e].ports[0] = 0;
conn[e].ports[1] = 0;
if (conn[e].sock>0) { closeconn (conn[e].sock); }	/* shouldnot happen */
conn[e].sock = 0;
conn[e].connected = 0;
conn[e].send_msgcnt = 0;
conn[e].recv_msgcnt = 0;
conn[e].sendallowed = 0;
conn[e].initialack = 0;
conn[e].error = 0;
conn[e].sentxon = 0;
conn[e].sentxoff = 0;

/* low level connections */
conn[e].snipe2chan = SNIPE2NULLCONN;
conn[e].recvreqptr = (snipe2_msg_list_t*) NULL;
conn[e].recvtruncreqptr = (snipe2_msg_list_t*) NULL;
conn[e].recvreq = 0;
conn[e].recvtruncreq = 0;
conn[e].recvtruncbufid = 0;
conn[e].recvstate=0;
conn[e].recvtruncstate=0;
conn[e].sendstate = 0;
conn[e].sendreqptr = (snipe2_msg_list_t*) NULL;
conn[e].sendreq = 0;

connfree --;
return (0);
}

int remove_conn_entry_by_gid (int id)
{
int i;

if (id==0) return (0);
for (i=1;i<MAXPERAPP;i++) 
	if (id==conn[i].gid) conn_clr_entry (i);	/* loop in case more match */ 
return (0);
}

int find_conn_entry_by_gid (int id)
{
int i;

for(i=1;i<MAXPERAPP;i++)
	if (id==conn[i].gid) return (i);

return (0);	/* NOT found */
}

int remove_conn_entry_not_in_list (int *ids, int sz)
{
int i;
int r;
int found[MAXPERAPP];

found[0] = 1;	/* we never remove 0 */
for(i=1;i<MAXPERAPP;i++) found[i]=0;	/* clr found list */

for (i=0;i<sz;i++) { /* search given list */
	if (ids[i]) {	/* if valid id */
		r = find_conn_entry_by_gid (ids[i]);
		if (r) found[r] = 1;	/* mark by conn index NOT the ids index */
		}
	}

/* OK, now only the conn entries that are found are kept, the other go */
for (i=1;i<MAXPERAPP;i++) 
	if ((!found[i])&&(conn[i].inuse)) conn_clr_entry (i);

return (0);
}


/* 
 This takes an ids, addr, ports (a, s) and a size
 and packs this info into conn DSs
 If a conn is not available, it makes one.
 If a conn is already made it adds to it updated info 
 */
int ftmpi_conn_fillin_conn_info (int *ids, int *pa, int *pc, int *ps, int ext)

{
    int i;
    int r;
    int chk=0;	/* debug chk flag, do not set for production codes */
    
    /*
      debugging Graham style */
    
    /* ** for out of order swapping of conn info testing **
     *for(i=0;i<ext;i++) { 
     *	r = find_conn_entry_by_gid (ids[i]);
     *	if (r>0) 
     *		printf("index %d gid %d naddr %x slot %d addr %x\n", i, ids[i], pa[i], r, conn[r].addr);
     *	else
     *		printf("index %d gid %d naddr %x slot %d\n", i, ids[i], pa[i], r);
     *}
     */
    
    for(i=0;i<ext;i++) 
	if (ids[i]>0) { /* for each valid ID */
	    r = find_conn_entry_by_gid (ids[i]);
	    if (r) { /* we have one so update it */
		if (conn[r].addr!=pa[i]) {
		    printf("changing address of existing GID %d, index %d, conn %d was %lx now %x\n", ids[i], i, r, conn[r].addr, pa[i]); 
		    chk=1;
		}
		conn[r].addr = pa[i];
		conn[r].ports[0] = pc[i];
		conn[r].ports[1] = ps[i];
	    }
	    else { /* we didnot find it, so get one */
		r = find_free_conn ();
		if (r) { /* found one */
		    conn[r].inuse = 1;
		    conn[r].gid = ids[i];
		    conn[r].addr = pa[i];
		    conn[r].ports[0] = pc[i];
		    conn[r].ports[1] = ps[i];
		}
		else {
		    printf("ftmpi_conn:could not find space for 0x%x entry\n",
			   ids[i]);
		}
	 } /* mk entry */	
	} /* Valid id */
    
    /* ** debugging do not uncomment **
     *for(i=0;i<ext;i++) { 
     *	r = find_conn_entry_by_gid (ids[i]);
     *	if (r>0) 
     *		printf("index %d gid %d naddr %x slot %d addr %x\n", i, ids[i], pa[i], r, conn[r].addr);
     *	else
     *		printf("index %d gid %d naddr %x slot %d\n", i, ids[i], pa[i], r);
     *}
     */
    
    
    if(chk) { 
	puts("sleeping so you can attach with a debugger...");
	fflush(stdout);
	fflush(stderr);
	sleep (100);
    }
    return 0;
}

int	conn_entry_dump (int r)
{
    printf("Conn entry dump %d\n", r);
    printf("Inuse %d GID 0x%x ADDR 0x%lx PC %d PS %d Sock %d\n", 
	   conn[r].inuse, conn[r].gid, conn[r].addr, conn[r].ports[0],
	   conn[r].ports[1], conn[r].sock);
    return 0;
}

/* ft-mpi-sys uses this to pass me my accept sockets! */
/* this is needed as he creates them! */
int ftmpi_conn_set_local (int me, int addr, int cas, int sas, int eas)
{
myid = me;
myaddr = addr;
mycs = cas;
myes = eas;

return (0);
}


/* this is an enquiry routine */
int conn_snipe2_getchan (int con, int *snipe2chan)
{
   if ((conn[con].connected)&&(conn[con].snipe2chan)) {
	if (snipe2chan) *snipe2chan = conn[con].snipe2chan;
	return (1);
   }
   else
	  return (0);
}


int conn_send_allowed (int con)
{
   if ((conn[con].connected)&&(conn[con].sendallowed)) 
	return (1);
   else
	  return (0);
}

int conn_connected (int con)
{
   if (conn[con].connected) 
	return (1);
   else
	return (0);
}

int conn_status (int con, int *connected, int *sendallowed)
{
	if (connected) *connected=conn[con].connected;
	if (sendallowed) *sendallowed=conn[con].sendallowed;

return (0);
}




/*
 This forces a connection to the target gid 
 There is no handshaking and all values get hardcoded
 This is for fully connected systems (i.e. small or testing)
 */
int conn_all_mkconn (int *gids, int ext)
{
int r=0;	/* remote conn rec */
int m=0;	/* my conn rec */
int p=0;
int cs=0;
int rc=0;
int tport=0;
int myindex=0;
int i;
int ingid=0;

for (i=0;i<ext;i++) {
   if (myid==gids[i]) {
	  myindex=i;
	  break;
   }
}

for (i=0;i<ext;i++) { /* we loop but we might not connect in order */
   /* so gids[i] might not connect when we want at iteration 'i' */

   r = 0; /* remote connection */
   rc=0;

   /* if my rank < i connect to them (make myrank-1 connections) */
   /* if my rank = i do nothing */
   /* if my rank > i allow (size-myrank connections) */

    if (gids[i]==myid) continue;

/* 	r = find_conn_entry_by_gid (gids[i]); */
	m = find_conn_entry_by_gid (myid);

/* 	if (!r) { */
/* 		printf("No conn info for 0x%x index %d\n", gids[i], i); */
/* 		exit (-1); */
/* 		} */

	if (myindex<i) { /* I allow them to connect to me */
	   cs = allowconn (mycs, 0, &tport);
/* 	   printf("Got Connection index %d on %d:%d\n", i, cs, tport); */
	   if (cs<0) {
		  printf("Failed on connection accept on index %d 0x%x -> %d\n", i, 
				gids[i], cs);
		  exit (-1);
	   }
	   rc = read (cs, &ingid, sizeof(int));
	   if (rc<0) {
		  printf("Failed on connection reading remote gid on index %d 0x%x -> %d\n", i, 
				gids[i], cs);
		  exit (-1);
	   }
	   else {
		  r = find_conn_entry_by_gid (ingid);
/* 		  printf("Connection on index %d was from GID 0x%x conn %d\n", */
/* 				i, ingid, r); */
	   }
	}
	else {	/* I connect to them */
	   /* for make connection, we DO know who we are connecting too! */
	   r = find_conn_entry_by_gid (gids[i]);

		p = conn[r].ports[0];
		cs = getconn_addr (conn[r].addr, &p, 0);
/* 	    printf("Made Connection index %d on %d:%d -> conn %d\n", i, cs, p, r);
 * 	    */
		if (cs<=0) {
		   printf("Cannot connect to 0x%x index %d conn %d\n", gids[i], i, r);
		   exit (-1);
		}
	   rc = write (cs, &myid, sizeof(int));
	   if (rc<0) {
		  printf("Failed on make connection sending my gid on index %d\n", i);
		  exit (-1);
	   }
	}

	/* we are already connected */
	/* if (conn[r].connected) return (1);	 */

	conn[r].sock = cs;
	conn[r].connected = 1;
	conn[r].send_msgcnt = 0;
	conn[r].recv_msgcnt = 0;
	conn[r].sendallowed = 1;	/* yes force it */
	conn[r].initialack = 1;
	conn[r].error = 0;	
	conn[r].sentxon = 1;
	conn[r].sentxoff = 0;
	conn[r].snipe2chan = 0; /* we still are not connected yet at device level */

	cons++;

	/* as we don't know about yes/no/alt yet */
/* 	outstanding++;	 */
	/* force it here to not change as we are disabling flow control */

	/* as we are keeping this one set the buffer size? */
	setsendrecvbufs (conn[r].sock, 128);

	/* formally try and open the connection by sending an XON request */
	/* ignore */

	/* make it snipe aware */
	conn_enable_snipe2_messaging (r);
	} /* for loop */

/* if we don't connect, we do nothing until we get an event? */
/* nope, set state now or tears later */
/* conn_fail (r); */
return (cons);
}


/*
 This makes a connection to the target gid 
 */
int conn_mkconn2 (int tgid, int startsnipe)
{
int r;	/* their conn rec */
int p;
int cs, rc;

r = find_conn_entry_by_gid (tgid);
/* m = find_conn_entry_by_gid (myid); */

if (!r) {
	printf("No conn info for 0x%x\n", tgid);
	return (-1);
	}

/* if (!m) { */
/* 	printf("No conn info for 0x%x\n", myid); */
/* 	return (-1); */
/* 	} */

if (conn[r].connected) return (1);	/* we are already connected */

/* else connect to them */
p = conn[r].ports[0];
cs = getconn_addr (conn[r].addr, &p, 0);

if (cs>0) {
	/* we setup the DS first, if it fails we let conn_fail clean up */
	/* if we don't set up first conn_send_req does not think we are connected */
	/* and thus doesnot send the XON to start it all.... GEF */
	conn[r].sock = cs;
	conn[r].connected = 1;
	conn[r].send_msgcnt = 0;
	conn[r].recv_msgcnt = 0;
	conn[r].sendallowed = 0;
	conn[r].initialack = 0;
	conn[r].error = 0;	
	conn[r].sentxon = 0;
	conn[r].sentxoff = 0;
	conn[r].snipe2chan = 0; /* we still are not connected yet at device level */

	cons++;
	outstanding++;	/* as we don't know about yes/no/alt yet */

	/* as we are keeping this one set the buffer size? */
	setsendrecvbufs (conn[r].sock, 128);

	/* formally try and open the connection by sending an XON request */
	rc = conn_send_req (XON, r);
	if (rc<0) {
		conn_fail (r);
		return (-1);
		}
	else {
		if (startsnipe) conn_enable_snipe2_messaging (r);
		return (1);	/* ok */
		}
	}

/* if we don't connect, we do nothing until we get an event? */
/* nope, set state now or tears later */
conn_fail (r);
return (-1);
}


/*
 This routine handles any connection attempts/requests
 This routine assumes the connection is not handled by the lowlevel driver yet
 If it gets new ones it returns the number of them
 If it gets an event, it returns -1 and sets ftmpi_err_d to 1

 we test even if ftmpi_err_d is set as we use this to flush requests 
 */
int	conn_handle_con_attempts (int nconnects, int allowdevice, int *activecons, int *tgids)
{
int cs, i, j;
int done=0;
/* int tags[3]; */
int id, fl, le;
int r;
int cnt=0;
int acp=0;
int rc;

#ifdef VERBOSE
printf("conn_handle_con_attempts: loop nconnects %d\n", nconnects);
#endif

/* blank out active con list if enabled */
if (activecons && (nconnects>0)) for(i=0;i<nconnects;i++) activecons[i]=0;
if (tgids && (nconnects>0)) for(i=0;i<nconnects;i++) tgids[i]=0;

while (!done) {
	acp=0;
	cs=0;
	/* if we got a request */
	if (!nconnects) cs = probeconn (mycs, 0, NULL); /* we got a request */
	else /* don't probe, wait? */
		cs = allowconn (mycs, 0, NULL); 
	if (cs>0) { /* we have a connection request */
		/* read it */
		#ifdef VERBOSE9
		printf("b readconn\n"); fflush(stdout);
		#endif
		j = readconn(cs, (char*)&rhdr, msg_hdr_size);

		/* after all reads we always translate the header */
		trans_hdr_from_network (rhdr);

		#ifdef VERBOSE9
		printf("a readconn\n"); fflush(stdout);
		#endif
		if (j==msg_hdr_size) { /* valid data */
			id = rhdr.fromgid;
			fl = rhdr.type;
			le = rhdr.datasize;
			r =find_conn_entry_by_gid (id); /* do we know them ? */ 
			if (r>0) { /* yes */
				if (conn[r].connected) { /* if already connected */
					/* we decide who connects to who */
					if (myid<id) { /* I will refuse the new */
					   /* why are we not using conn_send_req here ?? */
					   /* bacuse the conn[r].sock value is not right yet!! */
						xhdr.type = XALT;
						xhdr.fromgid = myid; /* already set? */
						xhdr.datasize=0;	 /* not needed but being safe */
		#ifdef VERBOSE5
						printf("%x req con but I %x have also, sending XALT\n",
								id, myid);
		#endif
						/* before sending make sure we convert header */
						trans_hdr_to_network (xhdr);

						writeconn(cs, (char*)&xhdr, msg_hdr_size);
						closeconn(cs);
						acp=0;

						/* I think it was a valid attempt so inc count */
						cnt++;
						}
					else { /* I give up the prev attempt already */
		#ifdef VERBOSE5
						printf("%x req con I %x closing previous attempt\n",
								id, myid);
		#endif

						   /* some lowlevel work to do */
						if (conn[r].snipe2chan>0) { 
		#ifdef VERBOSES2
						   printf("conn %d closing chan %d\n", r, conn[r].snipe2chan);
		#endif
						   /* if a recv is posted try and cancel it */
						   if (conn[r].recvreqptr) {
		#ifdef VERBOSES2
						   printf("conn %d closing chan %d cancelling recv hdr req 0x%x %d\n", r, conn[r].snipe2chan, conn[r].recvreqptr, conn[r].recvreq);
		#endif
							  snipe2_cancel_msg (conn[r].recvreqptr);
							  conn[r].recvreqptr = (snipe2_msg_list_t*) NULL;
							  conn[r].recvreq = 0;
							  conn[r].recvstate = 0;
							  conn[r].recvtruncstate = 0;
						   /* remove the low level device from this connection
							* */
		#ifdef VERBOSES2
						   	printf("conn %d removing chan %d\n", r, conn[r].snipe2chan);
		#endif
						   		snipe2_remove_chan (conn[r].snipe2chan);
						   		conn[r].snipe2chan = 0;
						   }
						}

						/* now do high level shutdown and reset counters */
						closeconn (conn[r].sock);	
						conn[r].connected = 0;	/* not any more */
						cons--;
						outstanding--;
/* 	printf("!\n"); */
						acp =1; /* yes accept is set so although we */
						/* kill the old connection we create a correct */
						/* replacement below! GEF03 */
						}
					} /* previously attempted to connect */	
				else 
					acp =1;	/* not previously connected so accpt */

				/* OK now try and handle it */
				if (acp) {
					cons++;
					outstanding++;	/* i.e. we don't know if its acked yet */
									/* if we don't do this, a head2head */
									/* send gets the outstanding count wrong */
					conn[r].connected = 1;
					conn[r].sock = cs;
					conn[r].gid = id;
					conn[r].send_msgcnt = 0;
					conn[r].recv_msgcnt = 0;
					conn[r].sendallowed = 0;
					conn[r].error = 0;
					conn[r].sentxon = 0;
					conn[r].sentxoff = 0;
					conn[r].snipe2chan = 0; /* we still are not connected yet at device level */

					/* as we are keeping this one set the buffer size? */
					setsendrecvbufs (conn[r].sock, 128);

				/* Now handle their message  */
				/* I.e. update my state stuff */
					rc = conn_handle_req (id, fl, r);

					/* Then we send reply */
					rc = conn_send_reply (fl, r);

					/* if allowed to active low level device */
					if (conn[r].sendallowed&&allowdevice) conn_enable_snipe2_messaging(r);

					/* if upper level needs a list of connections and gids that have become active */
					/* used to post pending sends onto low level device for example */
					if (conn[r].sendallowed&&activecons) activecons[cnt]=r;
					if (conn[r].sendallowed&&tgids) tgids[cnt]=id;

					cnt++;

					} /* if accept allowed */

				} /* if we know them */
			else {
		#ifdef VERBOSE5
			printf("Received connect req from unknown %x\n", id);
		#endif 
				}
			} /* if message of right size */
		else {
			printf("0x%x Badly formed message req recvd\n", myid);
			closeconn (cs);
			}

		/* we should always check the count if a connection attempt was made */
		if (cnt>=nconnects) done=1;
		} /* on connection request */

		else { /* if no connection req we are allowed to break out if we asked for none */
			if (!nconnects) done = 1;
			}

	/* Ok here we check for an event message on our ES */
	/* if we get one, we break out and go home */
#ifdef VERYOLD
	if (nconnects) cs = conn_check_for_event (0);
	else
		cs = conn_check_for_event (0);

	if (cs>0) { /* we have an event awaiting! */
		done = 1;
		cnt = -1;
		}
#endif /* VERYOLD */
	} /* While not done */

return (cnt);	
}



/* this routine checks for event messages arriving */
/* if one arrives it sets the ftmpi_err_d value */
/* and returns -1 */
/* if no event, returns 0 */
/* NOTE timeout in mSeconds not uSecs! */

int conn_check_for_event (int tout) 

{
int rc;


#ifdef VERBOSE
printf("conn_check_for_event: tout %d uSec\n", tout);
#endif

if (tout<0) tout = 10;	/* 10 is quite short but quite fast */

	rc = pollconn (myes, 0, tout); /* we got a event! */

	if (rc>0) { /* we have an event awaiting! */
		if (ftmpi_err_d==0) {	/* notify just the once */
#ifdef VERBOSE5
			printf("0x%x conn_check_for_event detected fault\n", myid);
#endif
			ftmpi_err_d = 1;
			}
		return (-1); /* yes */
		}
return (0);	/* not yet */
}

/*
	In this routine we handle state messages
	We update the state of a connection depending on flow/control msgs
	if we are opening a connection we 
		might also hand it off to the low level driver (todo)
	if we are closing a connection we my flush it first 
*/
int	conn_handle_req (int id, int fl, int r)
{
#ifdef VERBOSE
if (fl==XON) printf("conn_handle_req: XON me %d, them %d conn %d\n", myid, id, r);
if (fl==XOFF) printf("conn_handle_req: XOFF me %d, them %d conn %d\n", myid,
id, r);
if (fl==XALT) printf("conn_handle_req: XALT me %d, them %d conn %d\n", myid,
id, r);
#endif
if (fl==XON) {
	if (conn[r].sendallowed) {
#ifdef VERBOSE5 
		printf("%x sent XON when already XON?\n", id);
#endif 
		}
	else { /* change of state */
#ifdef VERBOSE5 
		printf("%x sent XON sends allowed\n", id);
#endif 
		conn[r].sendallowed = 1;
		xons++;
		}
	}
if (fl==XOFF) {
	if (!conn[r].sendallowed) {
#ifdef VERBOSE5 
		printf("%x sent XOFF when already XOFF?\n", id);
#endif 
		}
	else { /* change of state */
#ifdef VERBOSE5
		printf("%x sent XOFF sends disallowed\n", id);
#endif 
		conn[r].sendallowed = 0;
		xoffs++;
		}
	}
	
if (fl==XALT) { /* not really possible */
#ifdef VERBOSE5 
	printf("%x sent XALT???\n", id);
#endif 
	}

/*
 Initial ack helps me keep count of those outstanding 
*/
if ((!conn[r].initialack)&&((fl==XON)||(fl==XOFF))) {
	outstanding--;
#ifdef VERBOSE5 
	printf("IACK!\n");
#endif 
	conn[r].initialack = 1;
	}

return (0);
}



/*
 here I send a reply to someone 
 */

 /* fl is their message to ME */

int	conn_send_reply (int fl, int r)
{
int rc;

#ifdef VERBOSE
if (fl==XON) printf("conn_send_reply: XON conn %d\n", r);
if (fl==XOFF) printf("conn_send_reply: XOFF conn %d\n", r);
if (fl==XALT) printf("conn_send_reply: XALT conn %d\n", r);
#endif


if (ftmpi_err_d) { /* If I have detected an error */
			/* I always send an XOFF */
			/* Unless I have already */
	if (!conn[r].sentxoff) {
		rc = conn_send_req (XOFF, r);
		if (rc<0) conn_fail (r);	/* update counters */	
		}
/* 	else */
/* 		printf("0x%x auto reply on ftmpi_err_d XOFF already Ackd\n", myid); */
	
	} /* on ftmpi_err_d */

else { /* no ftmpi_err_d, i.e. normal replies */
	if (fl==XON) {
		if (conn[r].sentxon); /*printf("0x%x Got XON have already Ackd\n", myid);*/
		else { /* send XON back */
			rc = conn_send_req (XON, r);
			if (rc<0) conn_fail (r);	/* update counters */	
			}
		} /* XON */
	if (fl==XOFF) {
		if (conn[r].sentxoff) {
/* 		   printf("0x%x Got XOFF already Ackd\n", myid); */
		}
		else { /* send XOFF back */
			rc = conn_send_req (XOFF, r);
			if (rc<0) conn_fail (r);	/* update counters */	
			}
		} /* XOFF */
	} /* no ftmpi_err_d known */

return (0);
}


/*
 here I send a req to someone 
 this now uses the xhdr format which is the same for all headers
 */
int	conn_send_req (int fl, int r)
{
int j;
/* int tags[3]; */

#ifdef VERBOSE
if (fl==XON) printf("conn_send_req: XON me %d, conn %d\n", myid, r);
if (fl==XOFF) printf("conn_send_req: XOFF me %d, conn %d\n", myid, r);
if (fl==XALT) printf("conn_send_req: XALT me %d, conn %d\n", myid, r);
#endif


if (!conn[r].connected) return (0);	/* we are not already connected */

if (!conn[r].sock) return (0);	/* have no socket to send on */

/* if we are using a low level device and there are pending sends on */
/* that channel, then post the sends etc as we cannot do it here */
if ((conn[r].snipe2chan>0)&&(snipe2_pending_sends_on_chan (conn[r].snipe2chan))){
   if (conn[r].sendreqptr) {
	  fprintf(stderr,"conn %d chan %d cannot post Xxx of type %d as sendreq already pending\n", r, conn[r].snipe2chan, fl);
	  return (-1);
   }

   else { /* we can post it */
	  		/* build request */
	  fprintf(stderr,"conn %d chan %d pending send before flctrl %d = %d\n", 
			r, conn[r].snipe2chan, fl, snipe2_pending_sends_on_chan (conn[r].snipe2chan));

			conn[r].xhdr.type = fl;
			conn[r].xhdr.fromgid=myid;
			conn[r].xhdr.datasize=0; /* no payload */

			/* before sending must set the sendstate flag */
			conn[r].sendstate = fl;

			/* do low level send of msg */
			/* on callback return the channel number */
	  		conn[r].sendreq = snipe2_post_send_msg (
				conn[r].snipe2chan, 
				(char*)&conn[r].xhdr, msg_hdr_size, 
				(intfuncptr)(conn_sentflowcontrol_callback), 
				r, 1, &conn[r].sendreqptr);

			return (0); /* we return 0 as we do not need to update cnts yet */
   }
}

/* j = writeconn(conn[r].sock, (char*)tags, 3*4); */
xhdr.type = fl;
xhdr.fromgid=myid;
xhdr.datasize=0; /* no message */

trans_hdr_to_network (xhdr);	/* convert to network format */

/* if low level device but not any pending ops (i.e. override it) */
if (conn[r].snipe2chan>0) {  /* low-level device enabled */
   setblocking (conn[r].sock);
}
j = writeconn(conn[r].sock, (char*)&xhdr, msg_hdr_size);

if (conn[r].snipe2chan>0) {  /* low-level device enabled */
   setnonblocking (conn[r].sock);
}

if (j!=msg_hdr_size) { /* ops already? */
		conn_fail (r);
		return (-1);
		}

if (fl==XON)  { conn[r].sentxon=1;  conn[r].sentxoff=0; sxons++;  }
if (fl==XOFF) { conn[r].sentxoff=1; conn[r].sentxon=0;  sxoffs++; }

return (1); /* return of 1 means actually sent */
}


void conn_set_conn_fail_sends_events_flag (int sendeventonfail)
{
   conn_fail_sends_event = sendeventonfail;
}



/*
 Carefull routine that resets counts as failures are detected
*/
int conn_fail (int r)
{
if (!conn[r].error) {
		conn[r].error = 1;
		closeconn (conn[r].sock);
		conn[r].sock=0;
		ftmpi_err_d = 1;
		conn[r].send_msgcnt = 0;
		conn[r].recv_msgcnt = 0;

		if (conn[r].connected) {
			cons--;

			/* ONLY reset outstanding if I WAS connected */

			if (!conn[r].initialack) {
				outstanding--;
#ifdef VERBOSE5
				printf("!\n");
#endif
				}
			conn[r].initialack = 0;
			conn[r].connected = 0;
			}


		if (conn[r].sendallowed) xons--;	/* we had a xon now we dont */
		conn[r].sendallowed = 0;

		/* we cannot reset counters on this */
		/* as we don't know if they were recvd etc */
		/* but really on the its doesn't matter as they are dead */
		if (conn[r].sentxon)  sxons--;
		if (conn[r].sentxoff) sxoffs--;
		conn[r].sentxon = 0;
		conn[r].sentxoff = 0;

		/* ok, now the low level device stuff */
		if (conn[r].snipe2chan>0) {

#ifdef VERBOSES2
		   fprintf(stderr,"conn_fail: active snipe2 channel %d.\n", 
				 conn[r].snipe2chan);
		   fprintf(stderr,"conn_fail: should dequeue these for CONT here\n");
#endif

		   snipe2_remove_chan (conn[r].snipe2chan);
		   conn[r].snipe2chan = 0;

		} /* if snipe2 device active */

	} /* if not already marked as bad */

/* if we are allowed to send a death event about this connection dying?.. */
/* GEF */

/* first we check to see if its already dead and on our list */
/* if its not we inject a death event */

/* this is enables in ftmpi_sys_start_mpi and disabled in ftmpi_sys_leave */

if (conn_fail_sends_event) {	/* depends on flag */
   int rc;
   int runid;
   int myid;
   int theirid;
   int sig;

	runid = ftmpi_sys_get_runid();
	myid = ftmpi_sys_get_gid();
	theirid = conn[r].gid;
	sig = -900;
	rc = notifier_send_exit_event (myid, runid, theirid, sig);
	if (rc<0) {
	      fprintf(stderr,"Notice:ftmpi_conn:conn_fail: couldnot send event on runid 0x%x gid 0x%x code %d = %d\n",
					runid, theirid, sig, rc);
	}
#ifdef VERBOSERECOVERY
	else
	   fprintf(stderr,"ftmpi_conn:conn_fail: conn %d failed, sent death event about 0x%x\n", r, theirid);
#endif /*  VERBOSERECOVERY */

} /* send death event */

return(0);
}


int conn_dump_counters ( )
{
    printf("\nconn_dump_counters: me 0x%x\n", myid);
    printf("connections %d outstanding %d\n", cons, outstanding);
    printf("Sent xons %d xoffs %d Recvd xons %d xoffs %d\n", sxons, sxoffs, xons, xoffs);
    printf("Allowed to send to %d receiving y/n %d error_detect %d\n", allowed, recving, ftmpi_err_d);
    
    printf("\n");
    fflush(stdout);
    sleep (1);
    return 0;
}


/*
  conn_flush: this routine empties the socket for connection r
  if there is an error, it calls conn_fail etc etc
  BEWARE: it return 0 if all the data has been flushed or -1 otherwise
*/

int conn_flush (int r, int dlen, int flushhdr)
{
  int s; /* da socket */
  int dr, dexpect; /* data read */
  char *dustbin;	/* the place the unwanted data goes before being freed */
  
  s = conn[r].sock;	/* got the socket */
  
  dexpect = dlen;
  if( flushhdr ) dexpect += sizeof(msg_hdr_t);

  dustbin = (char*)_MALLOC( dexpect );
  if (!dustbin) { /* problem */
    return (-1);
  }
  
  /* now lets read ALL the useless data in just one step */
  dr = readconn( s, dustbin, dexpect );
  /* it's just dust isn't it */
  _FREE( dustbin );
  if( dr == dexpect ) return 0;
  /* something nasty happend */
  conn_fail( r );
  return -1;
}

/*
	conn_connection_event_matcher

	This flushes incomming events into the event loop 
	(i.e. a mix of libnotifier and ft-mpi-sys calls)
	Then it checks each GID of each open connection to see if they
	are in the event loop, if they are, it closes any connections to it

*/

int conn_connection_event_matcher ()
{
int i;
int rc;
int id;

/* this gets ALL pending events into the event loop */

rc = ftmpi_sys_check_for_events (&id, -1);

/* now loop from 1 to MAX */
for(i=1;i<MAXPERAPP;i++)
    if (conn[i].inuse) 
		if (conn[i].connected) {	/* valid record so check it */
			id = conn[i].gid;
			rc = find_event_by_gid (id);	/* in event loop? */
			if (rc) {
#ifdef VERBOSE
				printf("conn_connection_event_matcher: 0x%x found 0x%x in event loop, cleaning connection DB\n", myid, id);
#endif

				conn_fail (i);	/* mark it as dead */
				}
			}
	
return (0);
}

/*
 conn_restart_all_coms 

 This routine signals all other 'connected' parties that they can 
 communicate with me again.
 It updates counters as required.
 It also checks to see if anyone died since last time it was called.

 NOTE: Should only call after completing a RECOVERY mode.
*/

int	conn_restart_all_coms ( )
{
int i;
int rc;

/* first flush any newly failed tasks out */
rc =  conn_connection_event_matcher ();

/* Now we reset general counters */
cons = 0;
outstanding = 0;
xons = 0;
xoffs = 0;
sxons = 0;
sxoffs = 0;
allowed = 0;
recving = 1;	/* people can send to me now, I hope */

/* Reset error flag */
ftmpi_err_d = 0;

/* now reopen all communications with each opened connection by sending XONs */

/* now loop from 1 to MAX */
for(i=1;i<MAXPERAPP;i++)
    if (conn[i].inuse) 
		if (conn[i].connected) {	/* valid record so check it */
			/* send XON */
			rc = conn_send_req (XON, i);

			/* update counters */
			if (rc==1) { /* i.e. send XON worked */
				/* general counters */
				cons++;
				outstanding++;

				/* connection specific */
				conn[i].initialack = 0;
				conn[i].send_msgcnt = 0;
				conn[i].recv_msgcnt = 0;
				conn[i].sendallowed = 0;
				conn[i].initialack = 0;
				conn[i].error = 0;
				} /* if xon ok */
			if (rc==-1) {
				/* if xon failed.... */
				/* it is marked within conn_fail by the conn_send_req */
#ifdef VERBOSE
				printf("conn_restart_all_coms: 0x%x XON failed to 0x%x conn %d after recovery!\n", myid, conn[i].gid, i);
#endif
				}
            if (rc==0) {
#ifdef VERBOSE
                printf("conn_restart_all_coms: 0x%x POSTED XON to 0x%x conn %d\n", myid, conn[i].gid, i);
#endif
			    ftmpi_nb_progress (0);
			}

			}	/* for each valid conn record */

return (0);
}


/*
 conn_stop_all_coms 

 This routine signals all other 'connected' parties that they can NOT
 communicate with me until after a recovery has been completed.
 It updates counters as required.
 It also checks to see if anyone died since last time it was called.


 NOTE: Should only call before starting a RECOVERY mode.
*/

int	conn_stop_all_coms ()
{
int i;
int rc;

/* first flush any newly failed tasks out */
rc =  conn_connection_event_matcher ();

recving = 0;

/* now stop all communications with each opened connection by sending XOFFs */

/* now loop from 1 to MAX */
for(i=1;i<MAXPERAPP;i++)
    if (conn[i].inuse) 
		if (conn[i].connected) {	/* valid record so check it */
			/* send XOFF */
		    /* only if we haven't already! :) */
		    if (conn[i].sentxoff) {
#ifdef VERBOSE
			   printf("conn_stop_all_coms %d can not send XOFF to %d as already ACKD\n", myid, conn[i].gid);
#endif
			   continue; 
			}
			rc = conn_send_req (XOFF, i);

			/* update counters */
			if (rc==1) { /* i.e. send XOFF worked */
#ifdef VERBOSE
			   printf("conn_stop_all_coms %d sent XOFF to %d\n", myid, conn[i].gid);
#endif
				} /* if xoff ok */
			if (rc==-1) {
				/* if xoff failed.... */
				/* it is marked within conn_fail by the conn_send_req */
#ifdef VERBOSE
				printf("conn_stop_all_coms: 0x%x XOFF failed to 0x%x conn %d prior to  recovery!\n", myid, conn[i].gid, i);
#endif
				}
			if (rc==0) {
#ifdef VERBOSE
				printf("conn_stop_all_coms: 0x%x POSTED XOFF to 0x%x conn %d\n", myid, conn[i].gid, i);
#endif
				ftmpi_nb_progress (0);
			}

			}	/* for each valid conn record */


return (0);
}

/*

	conn_stop_and_flush ( )

	This routine is what its all about. It stops all communications
	using the above routines and then waits for all the XOFFs/connections
	etc to match up. i.e. at this stage all processes that I involve are 
	either flushed and ready for the rebuild/recovery or are dead.

	If this routine blocks then the game is over and we cannot recover
	hense it must check for all possible events continuous loop.

	This routine only works now when low level coms enabled 
	As we don't have a recv flow control call in here anymore

	Graham.

*/

int	conn_stop_and_flush ()
{
int i, rc;
int done = 0;

/* first flush any newly failed tasks out */
rc =  conn_connection_event_matcher ();

/* first thing we do is send all partners an XOFF */

conn_stop_all_coms ();

/* Warning */
/* this might later get replaced in the main loop to prevent deadlock from */
/* other processes not emptying their stream sockets fast enought */
/* GEF */

/* now the decision is based on how many xoffs we have received verses */
/* how many connections we have 'open' */
/* i.e. if xoffs = cons or cons=0 we are done */

done = 0;

#ifdef VERBOSE
printf("stop_and_flush cons %d xoffs %d\n", cons, xoffs);
#endif

while (!done) { /* while awaiting all cons to be complete */

#ifdef VERBOSE
printf("conn_stop_and_flush: open cons %d current xoffs %d awaiting %d\n",
		cons, xoffs, (cons-xoffs));
#endif

	/* any open connections */
	if (!cons) { done = 1; break; }

	/* have the right counter values */
	if (cons<= xoffs) { done = 1; break; }

/* ok, we have to handle connection attempts (to stop them) */
/* data messages which we drop until we get an xoff */
/* and finally check for errors so that any new errors reduce the cons cnt */

	conn_handle_con_attempts (0, 0, NULL, NULL);	/* poll do not wait */

	/* for checking incomming data streams */
	/* we have to check each possible connection */

	for(i=1;i<MAXPERAPP;i++)
   		 if (conn[i].inuse) 
			if (conn[i].connected) {	/* valid record so check it */
		 /* give each connection a chance to send/recv flow control msgs */
			   			ftmpi_nb_progress (0);
						/* check so we loop less */
						if ((cons<=xoffs)||(!cons)) {done = 1; break;}
				} /* connected */

	/* ok, now just check for any new failures/errors */
	rc =  conn_connection_event_matcher ();

	/* and now a short sleep until we recover to prevent spinning too much */
/* 	usleep (1000); */

	} /* while not done */

/* printf("stop_and_flush done : cons %d xoffs %d\n", cons, xoffs); */
/* fflush(stdout); */

return (0);
}


int conn_halt_all_coms ()
{
   int i;

/* if we are stopping the low level device do it now and switch to sync mode */
for(i=1;i<MAXPERAPP;i++)
   	if (conn[i].inuse && conn[i].connected && conn[i].snipe2chan) {
	   /* valid low level device.. so stop it!!! */
	   if (snipe2_pending_sends_on_chan(conn[i].snipe2chan)) {
#ifdef VERBOSERECOVERY
fprintf(stderr,"conn_stop_all_coms:disable low-level on chan %d has %d pending send still!\n", conn[i].snipe2chan, snipe2_pending_sends_on_chan(conn[i].snipe2chan));
#endif
		/* should dequeue here maybe? maybe not we are in finalize! I hope */
			}
	   /* remove channel and reset channel flags */
	   snipe2_remove_chan (conn[i].snipe2chan);
	   conn[i].snipe2chan = 0;
	}

/* now disable all the sockets */
for(i=1;i<MAXPERAPP;i++)
   	if (conn[i].inuse && conn[i].connected && conn[i].sock>0) {
	   closeconn (conn[i].sock);
	   conn[i].sock=0;
	   conn[i].inuse=0;
	   conn[i].connected=0;
	}

return (0);
}


/*
	ft-mpi-conn-any-error ( )
	This is a simple routine used by higher layers to find out if an error
	has occured.
	Basically it just returns ftmpi_err_d 
	(All other routine check the ft-mpi-sys/notifier event loop but
	 this is expensive in time, hense this is a fast check for previous error)
*/

int	ft_mpi_conn_any_error ()
{
return (ftmpi_err_d);
}







/* SNIPE2 and other low level device interfaces */

/* important note */
/* callbacks are not allowed to make calls that can cause ther callbacks */
/* this could lead to very recursive actions... */
/* keep it simple, even if it means just flipping globals. GEF Sep03 */

int conn_snipe2_setup_callbacks ()
{
/* after storing the information, we also setup SNIPE2 to watch these */
conn_snipe2_setup_event_callback (myes);
conn_snipe2_setup_newconnection_callback (mycs);

#ifdef VERBOSES2
snipe2_dump_all_chans ();
#endif /* SNIPE2 verbose */
return (0);
}

/* this removes all the callbacks that detect new connections and events */
int conn_snipe2_disable_newconnections_and_events ()
{
   /* first cancel recv */
   if (snipe2con_newconn_req) snipe2_cancel_msg (snipe2con_newconn_req);
   if (snipe2con_error_req) snipe2_cancel_msg (snipe2con_error_req);

   /* remove channel */
   snipe2_remove_chan (snipe2con_newconn);
   snipe2_remove_chan (snipe2con_error);

   return (0);
}

int conn_snipe2_setup_event_callback (int es)
{
   int req;
   snipe2con_error = snipe2_add_chan (es, SNIPE2POLLONLY, NULL, 0, 0);
   req = snipe2_post_recv_msg (snipe2con_error, NULL, 0, 
			       (intfuncptr)(conn_snipe2_event_callback), 1001, 0, 
			       &snipe2con_error_req); 
   /* req to catch events */
#ifdef VERBOSES2
   printf("conn_snipe2_setup_event_callback: on chan %d socket %d\n",
	  snipe2con_error, es); 
#endif /* SNIPE2 verbose */

   if (req<0) {
       fprintf(stderr,"? cannot setup event callback for SNIPE2\n");
   }
   return 0;
}

int conn_snipe2_setup_newconnection_callback (int cs)
{
   int req;

   snipe2con_newconn = snipe2_add_chan (cs, SNIPE2POLLONLY, NULL, 0, 0);
   req = snipe2_post_recv_msg (snipe2con_newconn, NULL, 0, 
		 (intfuncptr)(conn_snipe2_newconnection_callback), 2002, 0, 
		 &snipe2con_newconn_req); 
   /* req to catch new connection attempts */

#ifdef VERBOSES2
   printf("conn_snipe2_setup_newconnection_callback: on chan %d socket %d\n",
	  snipe2con_newconn, cs); 
   fflush(stdout);
#endif /* SNIPE2 verbose */

   if (req<0) {
       fprintf(stderr,"? cannot setup new connection callback for SNIPE2\n");
   }
   return 0;
}


int conn_snipe2_event_callback (SNIPE2CALLBACKPROTO)
{
#ifdef VERBOSES2
    printf("Callback on EVENT\n");
#endif
    if (userval==1001) {
	fifo_queue_add (progress_queue, PROGRESS_EVENT, NULL, NULL, NULL);
	ftmpi_err_d=1;
    }
    else
       printf("EVENT: Wrong callback. Got %ld expected %d\n", userval, 1001);
    snipe2_reset_poll_chan (onchan);
    
/*     sleep (1); */
    return 0;
}


int conn_snipe2_newconnection_callback (SNIPE2CALLBACKPROTO)
{
#ifdef VERBOSES2
   printf("Callback on NEWCONN\n");
#endif
   if (userval==2002) {
       fifo_queue_add (progress_queue, PROGRESS_NEWCONN, NULL, NULL, NULL);
   }
   else
       printf("NEWCONN: Wrong callback. Got %ld expected %d\n", userval, 2002);
   
   snipe2_reset_poll_chan (onchan);
   return 0;
}

int conn_sentflowcontrol_callback (SNIPE2CALLBACKPROTO)
{
   int fl, r;

   r = userval;
   fl = conn[r].sendstate;

#ifdef VERBOSES2
   printf("Callback on sent flow control for chan %d conn %d for flowctrl %d\n",
		 onchan, r, fl);
#endif

   /* ok, clean up conn record */
   conn[r].sendstate = 0;
   conn[r].sendreqptr = (snipe2_msg_list_t*) NULL;
   conn[r].sendreq = 0;

   /* depending on which type of flowctrl request we update counters */
   if (fl==XON) conn[r].sentxon=1;
   if (fl==XOFF) conn[r].sentxoff=1;

   return (0);
}

int conn_snipe2_recvhdr_callback (SNIPE2CALLBACKPROTO)
{
   int fl, id, r, lencheck;

#ifdef VERBOSES2
   printf("Callback on recvd header for chan %d\n", onchan);
#endif
   trans_hdr_from_network (conn[userval].recvhdr);
   conn[userval].recvstate=2;
   /* cache the list of headers received */
   /* onchan=chan and userval = conn */


   /* if it is a certain type of message handle it here */
   if (conn[userval].recvhdr.type!=XHDR) {
       fl = conn[userval].recvhdr.type;
       id = conn[userval].recvhdr.fromgid;
       lencheck = conn[userval].recvhdr.datasize; /* used for checking */
       
       
       /* check flow control message before handling it */
       r = find_conn_entry_by_gid(id);
       if ((r<0)||(lencheck!=0)) {
	   fprintf(stderr,"conn_snipe2_recvhdr_callback: recvd bad/misformed flowcontol msg from process 0x%x. Marking connection %ld as failed\n", id, userval);
	   /* what do I do now ? */
	   /* probably call conn fail ? */
	   /* note r should equal userval :) */
	   conn_fail (userval);
       }

#ifdef VERBOSES2
   printf("Callback on recvd header on chan %d is from 0x%x type %d conn %d\n", onchan, id, fl, r);
#endif

       conn_handle_req (id,fl,r);

       conn_send_reply (fl, r);

       conn_snipe2_post_recvhdr (r);

       if ((fl==XON)&&(conn[r].connected)&&(conn[r].sendallowed)) /* we have a new chan/conn ready for user data */
       		fifo_queue_add (progress_queue, PROGRESS_READYCONN, (void*)onchan, (void*)id, (void*)r);


       /* 	  int conn_send_reply (int fl, int r) */
       /* this routine will need to change to handle snipe2 correctly */
       
   }
   else { /* its a MPI header so process it */
       fifo_queue_add (progress_queue, PROGRESS_HDR_RECVD, 
		       (void*)onchan, (void*) userval, NULL);
   }
   return 0;
}


int conn_snipe2_recvdata_callback (SNIPE2CALLBACKPROTO)
{
#ifdef VERBOSES2
    printf("Callback on recvd data for conn %d\n", onchan);
#endif
    /* userval = connection for this callback */
    conn[userval].recvstate=4;
    /* onchan=chan and userval = conn */
    fifo_queue_add (progress_queue, PROGRESS_RECV_COMPLETED, 
		    (void*)onchan, (void*) userval, NULL);
    return 0;
}


int conn_snipe2_recvtrunc_callback (SNIPE2CALLBACKPROTO)
{
#ifdef VERBOSES2
    printf("Callback on recvd TRUNC data for conn %d\n", onchan);
#endif
    /* userval = connection for this callback */
    fifo_queue_add (progress_queue, PROGRESS_TRUNC_COMPLETED, 
		    (void*)onchan, (void*) userval, NULL);
    
    /* for use we should free up the trunc buffer here for this connection :) */
    /* TODO */
    free_msg_buf (conn[userval].recvtruncbufid);
    conn[userval].recvtruncstate = 0;
    conn[userval].recvtruncreq = 0;
    conn[userval].recvtruncreqptr = NULL;
    return 0;
}

int conn_snipe2_connerror_callback (SNIPE2CALLBACKPROTO)
{
   printf("Callback on CON ERROR chan %d con %ld Sock = %d\n", onchan, userval, conn[onchan].sock);
   ftmpi_err_d = 1;
   fifo_queue_add (progress_queue, PROGRESS_CONNERROR, 
		   (void*)onchan, (void*) userval, NULL);
   return 0;
}



int conn_snipe2_closeconn_callback (SNIPE2CALLBACKPROTO)
{
#ifdef VERBOSES2
    printf("Callback on CLOSECONN\n");
#endif
    /* this is triggered from a send */
    /* such as receiving an xoff/xalt etc when a connection has not be ackd */
    
    /* we should check that this is true etc BUT we posted the call that */
    /* allowed this callback :) */
    
    /* we could let the system store the to close con, but instead we will */
    /* just mark it for immediate deletion */
    conn[userval].connected = 2;
    
    /* then set a flag so the main progress loop will find it */
    fifo_queue_add (progress_queue, PROGRESS_CONNCLOSE, 
		    (void*)onchan, (void*) userval, NULL);
    return 0;
}


int conn_snipe2_post_recvhdr (int con)
{

    conn[con].recvstate=1; /* recv header posting */
    /*    conn[con].recvtruncstate = 0;  */
    /* there might be one already posted */
    conn[con].recvreq = snipe2_post_recv_msg (conn[con].snipe2chan,
					      (char*)&conn[con].recvhdr, msg_hdr_size,
					      (intfuncptr)(conn_snipe2_recvhdr_callback),
					      con, 1, &conn[con].recvreqptr);
    /* note the 1=autodelete after call back */
    /* callback userval = con */
    
    return 0;
}

int conn_snipe2_post_recvdata (int con, char* buf, long len, struct req_list *rlp, struct msg_list *mlp)
{

   conn[con].recvstate=3; /* recv data */
   conn[con].rlp=rlp; /* so we know which rlp it was if ANY */
   conn[con].mlp=mlp; /* so we know which mlp it was if ANY */
   conn[con].recvreq = snipe2_post_recv_msg (conn[con].snipe2chan,
	          (char*)buf, len,
	           (intfuncptr)(conn_snipe2_recvdata_callback),
	            con, 1, &conn[con].recvreqptr);
   				/* callback userval = con */
   				/* note the 1=autodelete after call back */

   return (0); 
}

int conn_snipe2_post_recvtrunc (int con, long len)
{
   char* bufptr;
   int blen;

   /* get a buffer for the truncated data */
   conn[con].recvtruncbufid = get_msg_buf_of_size (len, 1, 0);

   /* findout the ptr of the buffer */
   get_msg_buf_info (conn[con].recvtruncbufid, &bufptr, &blen);

   /* todo check blen == len ?? */

   conn[con].recvtruncstate = 1;			/* we have posted a trunc recv */

   conn[con].recvtruncreq = snipe2_post_recv_msg (conn[con].snipe2chan,
						  (char*)bufptr, len,
						  (intfuncptr)(conn_snipe2_recvtrunc_callback),
						  con, 1, &conn[con].recvtruncreqptr);
   /* note the 1=autodelete after call back */
   fprintf(stderr,"WARNING: %s:%d conn_snipe2_post_recvtrunc : posted a trunc recv on conn %d chan %d for %ld bytes under req %d\n\nProgram maybe erroreous.\n",
	  __FILE__, __LINE__, con, conn[con].snipe2chan, len, conn[con].recvtruncreq);
   return (conn[con].recvtruncreq);
}




int conn_enable_snipe2_messaging (int con)
{
    /* create the connection and also register a call back on it */
    /* if an error occurs it will call the callback with userval con */
    conn[con].snipe2chan = 
	snipe2_add_chan (conn[con].sock, SNIPE2DATA, 
			 (intfuncptr)(conn_snipe2_connerror_callback), con, 0);

    if (conn[con].snipe2chan<0) {
	fprintf(stderr,"conn_enable_snipe2_messaging: failed for ftmpi con %d with %d\n", con, conn[con].snipe2chan);
	return (conn[con].snipe2chan);
    }
    
    conn_snipe2_post_recvhdr (con);
    
#ifdef VERBOSES2
    printf("Con %d passed to SNIPE2 as device %d. Default recv header is %d\n",
	   con, conn[con].snipe2chan, conn[con].recvreq);
#endif /* SNIPE2 verbose */
    
    return (0);
}











msg_hdr_t* conn_get_recvhdr (int con)
{
   return (&conn[con].recvhdr);
}

int	conn_get_recvptrs (int con, struct req_list **rptr, struct msg_list **mptr)
{
   *rptr = conn[con].rlp;
   *mptr = conn[con].mlp;
   return (conn[con].snipe2chan);
}



/* debug routines */
int conn_debug_send (char* ptr, int len)
{
   int preq;
   snipe2_msg_list_t* rptr;

   preq = snipe2_post_send_msg (2, (char*)ptr, len, NULL, 100, 1, &rptr);
   return 0;
}

int conn_debug_recv (char* ptr, int len)
{
    int preq;
    snipe2_msg_list_t* rptr;
    
    preq = snipe2_post_recv_msg (2, (char*)ptr, len, NULL, 100, 1, &rptr);
    return 0;
}

int conn_hexdump (char *ptr, int len)
{
    int i;
    unsigned short v;
    char *p;
    p = ptr;
    
    printf("0x%lx:",(long)ptr);
    for (i=0;i<len;i++) {
	v = (unsigned short)*p++;
	if (v<16) 
	    printf("0%1x ",v);
	else
	    printf("%2x ",v);
    }
    printf("\n");
    return 0;
}

