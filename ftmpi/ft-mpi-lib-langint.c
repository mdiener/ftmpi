/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
                        Edgar Gabriel <egabriel@cs.utk.edu>
						Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include "mpi.h"
#include "ft-mpi-lib.h"

/* This file implements the language interoperability
   chapter of MPI-2. Since all object-references in FT-MPI
   are identical, these routines are dummy-functions. 
   EG, Jan. 28 2003 */


MPI_Datatype MPI_Type_f2c ( MPI_Fint ftype )
{
  return ( (MPI_Datatype) ftype );
}

MPI_Fint MPI_Type_c2f ( MPI_Datatype dtype )
{
  return ( (MPI_Fint) dtype );
}

MPI_Comm MPI_Comm_f2c ( MPI_Fint fcomm )
{
  return ( (MPI_Comm) fcomm );
}

MPI_Fint MPI_Comm_c2f ( MPI_Comm comm )
{
  return ( (MPI_Fint) comm );
}

MPI_Group MPI_Group_f2c ( MPI_Fint fgroup)
{
  return ( (MPI_Group) fgroup );
}

MPI_Fint MPI_Group_c2f ( MPI_Group group )
{
  return ( (MPI_Fint) group );
}

MPI_Op MPI_Op_f2c ( MPI_Fint fop)
{
  return ( (MPI_Op) fop );
}

MPI_Fint MPI_Op_c2f ( MPI_Op op)
{
  return ( (MPI_Fint) op );
}

MPI_Request MPI_Request_f2c ( MPI_Fint freq )
{
  return ( (MPI_Request) freq );
}

MPI_Fint  MPI_Request_c2f ( MPI_Request req )
{
  return ( (MPI_Fint) req );
}

/* Assuming that we might have one day 
   the operations requiring the MPI_Win,
   MPI_Info, and MPI_File objects, and 
   assuming again that their version
   in C and Fortran are identicall, I include
   already the code for their language interoperability
   functions .
   EG Jan. 28 2003 
*/

/*
MPI_Win MPI_Win_f2c ( MPI_Fint fwin )
{
  return ((MPI_Win) fwin );
}

MPI_Fint MPI_Win_c2f ( MPI_Win win )
{
  return ( (MPI_Fint) win );
}

MPI_File MPI_File_f2c ( MPI_Fint ffile )
{
  return ( (MPI_File) ffile );
}

MPI_Fint MPI_File_c2f ( MPI_Fint file )
{
  return ( (MPI_Fint) file );
}

MPI_Info MPI_Info_f2c ( MPI_Fint finfo )
{
  return ( (MPI_Info) finfo );
}

MPI_Fint MPI_Info_c2f ( MPI_Info info )
{
  return ( (MPI_Fint) info );
}
*/



