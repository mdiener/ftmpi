/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Edgar Gabriel <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/


#ifndef FT_MPI_FPROT_H
#define FT_MPI_FPROT_H


#define HDR     "FTMP:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif

/*  constants needed by Tone */

extern int _ATB_MAX_CFLAG_LEN;
extern int * _ATB_MAX_CFLAG;



MPI_Datatype fdt2c (int indt);
int cdt2f   (MPI_Datatype indt);
int fstr2c (char *in, char *out, int outmaxlen);

int check_ddt(int n,int * odt,int * ocnt,int * ndt,int * ncnt);
void uncheck_ddt(int flag,int n,int * cnt);



/* I don't see currently another solution than providing the 
   prototypes four times * 2 (profiling interface) */
#ifdef FORTRANCAPS
void MPI_ATTR_DELETE(int * comm,int * keyval,int * __ierr);
void MPI_ATTR_GET(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void MPI_ATTR_PUT(int *comm,int *keyval,int *attribute_val,int *__ierr);
void MPI_BSEND_INIT(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void MPI_BUFFER_ATTACH(void * buffer,int * size,int * __ierr);
void MPI_BUFFER_DETACH(void * buffer,int * size,int * __ierr);
void MPI_CANCEL(int *request,int *__ierr);
void MPI_CART_COORDS(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void MPI_CART_CREATE(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void MPI_CARTDIM_GET(int * comm,int * ndims,int * __ierr);
void MPI_CART_GET(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void MPI_CART_MAP(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void MPI_CART_RANK(int * comm,int * coords,int * rank,int * __ierr);
void MPI_CART_SHIFT(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void MPI_CART_SUB(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void MPI_DIMS_CREATE(int * nnodes,int * ndims,int * dims,int * __ierr);
void MPI_ERRHANDLER_CREATE(int * function,int * errhandler,int * __ierr);
void MPI_ERRHANDLER_FREE(int * errhandler,int * __ierr);
void MPI_ERRHANDLER_GET(int * comm,int *errhandler,int * __ierr);
void MPI_ERRHANDLER_SET(int * comm,int * errhandler,int * __ierr);
void MPI_ERROR_CLASS(int * errorcode,int * errorclass,int * __ierr);
void MPI_ERROR_STRING(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void MPI_GRAPH_CREATE(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void MPI_GRAPHDIMS_GET(int *comm,int *nnodes,int *nedges,int *__ierr);
void MPI_GRAPH_GET(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void MPI_GRAPH_MAP(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void MPI_GRAPH_NEIGHBORS_COUNT(int * comm,int * rank,int * neighbors,
				int * __ierr);
void MPI_GRAPH_NEIGHBORS(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void MPI_GROUP_COMPARE(int * group1,int *group2,int *result,int *__ierr);
void MPI_GROUP_DIFFERENCE(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void MPI_GROUP_EXCL(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void MPI_GROUP_FREE(int * group,int * __ierr);
void MPI_GROUP_INCL(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void MPI_GROUP_INTERSECTION(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void MPI_GROUP_RANK(int *group, int *rank, int *__ierr);
void MPI_GROUP_SIZE(int *group, int *size, int *__ierr);
void MPI_GROUP_RANGE_EXCL(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void MPI_GROUP_RANGE_INCL(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void MPI_GROUP_TRANSLATE_RANKS(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void MPI_GROUP_UNION(int *group1,int *group2,int *newgroup,int *__ierr);
void MPI_IBSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void MPI_IPROBE(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void MPI_IRECV(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void MPI_IRSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void MPI_ISEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void MPI_ISSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void MPI_KEYVAL_CREATE(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void MPI_KEYVAL_FREE(int * keyval,int * __ierr);
void MPI_NULL_COPY_FN(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void MPI_DUP_FN( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void MPI_NULL_DELETE_FN(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void MPI_OP_CREATE(int * function,int * commute, int * op,int * __ierr);
void MPI_OP_FREE(int * op,int * __ierr);
void MPI_PCONTROL(int *level,...);
void MPI_RECV_INIT(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void MPI_REQUEST_FREE(int * request,int * __ierr);
void MPI_RSEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void MPI_SEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void MPI_SSEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void MPI_STARTALL(int * cnt,int * array_of_requests,int * __ierr);
void MPI_START(int * request,int * __ierr);
void MPI_TEST( int* request, int* flag, int* status, int* __ierr );
void MPI_TESTALL(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void MPI_TESTANY(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void MPI_TEST_CANCELLED(int * status,int * flag,int * __ierr);
void MPI_TESTSOME(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void MPI_TOPO_TEST(int * comm,int * status,int * __ierr);
void MPI_WAITALL(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void MPI_WAITANY(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void MPI_WAIT(int * request,int * status,int * __ierr);
void MPI_WAITSOME(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double MPI_WTIME();
double MPI_WTICK();
void MPI_BARRIER(int *comm,int * __ierr );
void MPI_BCAST(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void MPI_REDUCE(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void MPI_REDUCE_SCATTER(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void MPI_ALLREDUCE(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void MPI_ALLGATHER(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void MPI_ALLGATHERV(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void MPI_ALLTOALL(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void MPI_ALLTOALLV(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void MPI_GATHER(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void MPI_GATHERV(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void MPI_SCATTER(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void MPI_SCATTERV(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void MPI_SCAN(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void MPI_ADDRESS(void * particle,int * disp,int * __ierr);
void MPI_GET_COUNT(int * status,int * ddt,int *cnt,int * __ierr);
void MPI_GET_ELEMENTS(int * status,int * ddt,int * cnt,int * __ierr);
void MPI_TYPE_FREE(int * ddt,int * __ierr);
void MPI_TYPE_SIZE(int * ddt,int *size,int * __ierr);
void MPI_TYPE_EXTENT(int * ddt,int *extent,int * __ierr);
void MPI_TYPE_STRUCT(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void MPI_TYPE_CONTIGUOUS(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void MPI_TYPE_VECTOR(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void MPI_TYPE_HVECTOR(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void MPI_TYPE_INDEXED(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void MPI_TYPE_HINDEXED(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void MPI_TYPE_LB(int * ddt,int * displacement,int * __ierr);
void MPI_TYPE_UB(int * ddt,int * displacement,int * __ierr);
void MPI_TYPE_COMMIT(int * ddt,int * __ierr);
void MPI_PACK_SIZE(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void MPI_PACK(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void MPI_UNPACK(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void MPI_BSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void MPI_SEND(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void MPI_RECV(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void MPI_SENDRECV(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void MPI_PROBE(int *source,int *tag,int *comm,int *status,int  *__ierr);
void MPI_RSEND(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void MPI_SENDRECV_REPLACE(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void MPI_SSEND(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void MPI_COMM_RANK(int *comm,int * rank,int * __ierr );
void MPI_COMM_SIZE(int *comm,int * size,int * __ierr );
void MPI_COMM_DUP(int * comm,int * newcomm,int * __ierr);
void MPI_COMM_CREATE(int * comm,int * group,int * newcomm,int * __ierr);
void MPI_COMM_FREE(int * comm,int * __ierr);
void MPI_COMM_GROUP(int * comm,int * group,int * __ierr);
void MPI_COMM_REMOTE_GROUP(int * comm,int * group,int * __ierr);
void MPI_COMM_REMOTE_SIZE(int * comm,int * size,int * __ierr);
void MPI_COMM_SPLIT(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void MPI_COMM_TEST_INTER(int * comm,int * flag,int * __ierr);
void MPI_COMM_COMPARE (int *comm1, int* comm2, int* result, int* __ierr);
void MPI_INTERCOMM_CREATE(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void MPI_INTERCOMM_MERGE(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void MPI_INIT( int *ierr );
void MPI_INITIALIZED(int *flag,int * __ierr) ;
void MPI_FINALIZE(int * __ierr);
void MPI_ABORT(int * comm,int * errorcode,int * __ierr);
void MPI_GET_PROCESSOR_NAME( char *name, int* len, int* __ierr);

void MPI_COMM_CREATE_KEYVAL(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void MPI_COMM_FREE_KEYVAL (int * keyval,int * __ierr);
void MPI_COMM_DELETE_ATTR(int * comm,int * keyval,int * __ierr);
void MPI_COMM_GET_ATTR(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void MPI_COMM_SET_ATTR(int *comm,int *keyval,int *attribute_val,int *__ierr);
void MPI_COMM_CREATE_ERRHANDLER(int * function,int * errhandler,int * __ierr);
void MPI_COMM_GET_ERRHANDLER(int * comm,int *errhandler,int * __ierr);
void MPI_COMM_SET_ERRHANDLER(int * comm,int * errhandler,int * __ierr);
void MPI_COMM_CALL_ERRHANDLER(int *comm, int* errcode, int * __ierr );
void MPI_ADD_ERROR_CLASS ( int *errorclass, int * __ierr );
void MPI_ADD_ERROR_CODE ( int* class, int *code, int* __ierr );
void MPI_ADD_ERROR_STRING ( int *code,  char *string, int* __ierr );

/* Profiling Interface */
void PMPI_ATTR_DELETE(int * comm,int * keyval,int * __ierr);
void PMPI_ATTR_GET(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void PMPI_ATTR_PUT(int *comm,int *keyval,int *attribute_val,int *__ierr);
void PMPI_BSEND_INIT(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void PMPI_BUFFER_ATTACH(void * buffer,int * size,int * __ierr);
void PMPI_BUFFER_DETACH(void * buffer,int * size,int * __ierr);
void PMPI_CANCEL(int *request,int *__ierr);
void PMPI_CART_COORDS(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void PMPI_CART_CREATE(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void PMPI_CARTDIM_GET(int * comm,int * ndims,int * __ierr);
void PMPI_CART_GET(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void PMPI_CART_MAP(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void PMPI_CART_RANK(int * comm,int * coords,int * rank,int * __ierr);
void PMPI_CART_SHIFT(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void PMPI_CART_SUB(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void PMPI_DIMS_CREATE(int * nnodes,int * ndims,int * dims,int * __ierr);
void PMPI_ERRHANDLER_CREATE(int * function,int * errhandler,int * __ierr);
void PMPI_ERRHANDLER_FREE(int * errhandler,int * __ierr);
void PMPI_ERRHANDLER_GET(int * comm,int *errhandler,int * __ierr);
void PMPI_ERRHANDLER_SET(int * comm,int * errhandler,int * __ierr);
void PMPI_ERROR_CLASS(int * errorcode,int * errorclass,int * __ierr);
void PMPI_ERROR_STRING(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void PMPI_GRAPH_CREATE(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void PMPI_GRAPHDIMS_GET(int *comm,int *nnodes,int *nedges,int *__ierr);
void PMPI_GRAPH_GET(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void PMPI_GRAPH_MAP(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void PMPI_GRAPH_NEIGHBORS_COUNT(int * comm,int * rank,int * neighbors,
				int * __ierr);
void PMPI_GRAPH_NEIGHBORS(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void PMPI_GROUP_COMPARE(int * group1,int *group2,int *result,int *__ierr);
void PMPI_GROUP_DIFFERENCE(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void PMPI_GROUP_EXCL(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void PMPI_GROUP_FREE(int * group,int * __ierr);
void PMPI_GROUP_INCL(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void PMPI_GROUP_INTERSECTION(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void PMPI_GROUP_RANK(int *group, int *rank, int *__ierr);
void PMPI_GROUP_SIZE(int *group, int *size, int *__ierr);
void PMPI_GROUP_RANGE_EXCL(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void PMPI_GROUP_RANGE_INCL(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void PMPI_GROUP_TRANSLATE_RANKS(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void PMPI_GROUP_UNION(int *group1,int *group2,int *newgroup,int *__ierr);
void PMPI_IBSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void PMPI_IPROBE(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void PMPI_IRECV(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void PMPI_IRSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void PMPI_ISEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void PMPI_ISSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void PMPI_KEYVAL_CREATE(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void PMPI_KEYVAL_FREE(int * keyval,int * __ierr);
void PMPI_NULL_COPY_FN(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void PMPI_DUP_FN( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void PMPI_NULL_DELETE_FN(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void PMPI_OP_CREATE(int * function,int * commute, int * op,int * __ierr);
void PMPI_OP_FREE(int * op,int * __ierr);
void PMPI_PCONTROL(int *level,...);
void PMPI_RECV_INIT(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void PMPI_REQUEST_FREE(int * request,int * __ierr);
void PMPI_RSEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void PMPI_SEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void PMPI_SSEND_INIT(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void PMPI_STARTALL(int * cnt,int * array_of_requests,int * __ierr);
void PMPI_START(int * request,int * __ierr);
vois PMPI_TEST( int* request, int* flag, int* status, int* __ierr );
void PMPI_TESTALL(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void PMPI_TESTANY(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void PMPI_TEST_CANCELLED(int * status,int * flag,int * __ierr);
void PMPI_TESTSOME(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void PMPI_TOPO_TEST(int * comm,int * status,int * __ierr);
void PMPI_WAITALL(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void PMPI_WAITANY(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void PMPI_WAIT(int * request,int * status,int * __ierr);
void PMPI_WAITSOME(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double PMPI_WTIME();
double PMPI_WTICK();
void PMPI_BARRIER(int *comm,int * __ierr );
void PMPI_BCAST(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void PMPI_REDUCE(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void PMPI_REDUCE_SCATTER(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void PMPI_ALLREDUCE(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void PMPI_ALLGATHER(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void PMPI_ALLGATHERV(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void PMPI_ALLTOALL(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void PMPI_ALLTOALLV(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void PMPI_GATHER(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void PMPI_GATHERV(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void PMPI_SCATTER(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void PMPI_SCATTERV(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void PMPI_SCAN(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void PMPI_ADDRESS(void * particle,int * disp,int * __ierr);
void PMPI_GET_COUNT(int * status,int * ddt,int *cnt,int * __ierr);
void PMPI_GET_ELEMENTS(int * status,int * ddt,int * cnt,int * __ierr);
void PMPI_TYPE_FREE(int * ddt,int * __ierr);
void PMPI_TYPE_SIZE(int * ddt,int *size,int * __ierr);
void PMPI_TYPE_EXTENT(int * ddt,int *extent,int * __ierr);
void PMPI_TYPE_STRUCT(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void PMPI_TYPE_CONTIGUOUS(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void PMPI_TYPE_VECTOR(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void PMPI_TYPE_HVECTOR(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void PMPI_TYPE_INDEXED(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void PMPI_TYPE_HINDEXED(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void PMPI_TYPE_LB(int * ddt,int * displacement,int * __ierr);
void PMPI_TYPE_UB(int * ddt,int * displacement,int * __ierr);
void PMPI_TYPE_COMMIT(int * ddt,int * __ierr);
void PMPI_PACK_SIZE(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void PMPI_PACK(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void PMPI_UNPACK(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void PMPI_BSEND(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void PMPI_SEND(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void PMPI_RECV(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void PMPI_SENDRECV(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void PMPI_PROBE(int *source,int *tag,int *comm,int *status,int  *__ierr);
void PMPI_RSEND(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void PMPI_SENDRECV_REPLACE(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void PMPI_SSEND(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void PMPI_COMM_RANK(int *comm,int * rank,int * __ierr );
void PMPI_COMM_SIZE(int *comm,int * size,int * __ierr );
void PMPI_COMM_DUP(int * comm,int * newcomm,int * __ierr);
void PMPI_COMM_CREATE(int * comm,int * group,int * newcomm,int * __ierr);
void PMPI_COMM_FREE(int * comm,int * __ierr);
void PMPI_COMM_GROUP(int * comm,int * group,int * __ierr);
void PMPI_COMM_REMOTE_GROUP(int * comm,int * group,int * __ierr);
void PMPI_COMM_REMOTE_SIZE(int * comm,int * size,int * __ierr);
void PMPI_COMM_SPLIT(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void PMPI_COMM_TEST_INTER(int * comm,int * flag,int * __ierr);
void PMPI_COMM_COMPARE (int *comm1, int* comm2, int* result, int* __ierr);
void PMPI_INTERCOMM_CREATE(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void PMPI_INTERCOMM_MERGE(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void PMPI_INIT( int *ierr );
void PMPI_INITIALIZED(int *flag,int * __ierr) ;
void PMPI_FINALIZE(int * __ierr);
void PMPI_ABORT(int * comm,int * errorcode,int * __ierr);
void PMPI_GET_PROCESSOR_NAME( char *name, int* len, int* __ierr);

void PMPI_COMM_CREATE_KEYVAL(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void PMPI_COMM_FREE_KEYVAL (int * keyval,int * __ierr);
void PMPI_COMM_DELETE_ATTR(int * comm,int * keyval,int * __ierr);
void PMPI_COMM_GET_ATTR(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void PMPI_COMM_SET_ATTR(int *comm,int *keyval,int *attribute_val,int *__ierr);
void PMPI_COMM_CREATE_ERRHANDLER(int * function,int * errhandler,int * __ierr);
void PMPI_COMM_GET_ERRHANDLER(int * comm,int *errhandler,int * __ierr);
void PMPI_COMM_SET_ERRHANDLER(int * comm,int * errhandler,int * __ierr);
void PMPI_COMM_CALL_ERRHANDLER(int *comm, int* errcode, int * __ierr );
void PMPI_ADD_ERROR_CLASS ( int *errorclass, int * __ierr );
void PMPI_ADD_ERROR_CODE ( int* class, int *code, int* __ierr );
void PMPI_ADD_ERROR_STRING ( int *code,  char *string, int* __ierr );

#elif defined(FORTRANDOUBLEUNDERSCORE)
void mpi_attr_delete__(int * comm,int * keyval,int * __ierr);
void mpi_attr_get__(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void mpi_attr_put__(int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_bsend_init__(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void mpi_buffer_attach__(void * buffer,int * size,int * __ierr);
void mpi_buffer_detach__(void * buffer,int * size,int * __ierr);
void mpi_cancel__(int *request,int *__ierr);
void mpi_cart_coords__(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void mpi_cart_create__(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void mpi_cartdim_get__(int * comm,int * ndims,int * __ierr);
void mpi_cart_get__(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void mpi_cart_map__(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void mpi_cart_rank__(int * comm,int * coords,int * rank,int * __ierr);
void mpi_cart_shift__(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void mpi_cart_sub__(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void mpi_dims_create__(int * nnodes,int * ndims,int * dims,int * __ierr);
void mpi_errhandler_create__(int * function,int * errhandler,int * __ierr);
void mpi_errhandler_free__(int * errhandler,int * __ierr);
void mpi_errhandler_get__(int * comm,int *errhandler,int * __ierr);
void mpi_errhandler_set__(int * comm,int * errhandler,int * __ierr);
void mpi_error_class__(int * errorcode,int * errorclass,int * __ierr);
void mpi_error_string__(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void mpi_graph_create__(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void mpi_graphdims_get__(int *comm,int *nnodes,int *nedges,int *__ierr);
void mpi_graph_get__(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void mpi_graph_map__(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void mpi_graph_neighbors_count__(int * comm,int * rank,int * neighbors,
				int * __ierr);
void mpi_graph_neighbors__(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void mpi_group_compare__(int * group1,int *group2,int *result,int *__ierr);
void mpi_group_difference__(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void mpi_group_excl__(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void mpi_group_free__(int * group,int * __ierr);
void mpi_group_incl__(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void mpi_group_intersection__(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void mpi_group_rank__(int *group, int *rank, int *__ierr);
void mpi_group_size__(int *group, int *size, int *__ierr);
void mpi_group_range_excl__(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void mpi_group_range_incl__(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void mpi_group_translate_ranks__(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void mpi_group_union__(int *group1,int *group2,int *newgroup,int *__ierr);
void mpi_ibsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_iprobe__(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void mpi_irecv__(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_irsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_isend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_issend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_keyval_create__(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void mpi_keyval_free__(int * keyval,int * __ierr);
void mpi_null_copy_fn__(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void mpi_dup_fn__( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void mpi_null_delete_fn__(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void mpi_op_create__(int * function,int * commute, int * op,int * __ierr);
void mpi_op_free__(int * op,int * __ierr);
void mpi_pcontrol__(int *level,...);
void mpi_recv_init__(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void mpi_request_free__(int * request,int * __ierr);
void mpi_rsend_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void mpi_send_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void mpi_ssend_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void mpi_startall__(int * cnt,int * array_of_requests,int * __ierr);
void mpi_start__(int * request,int * __ierr);
void mpi_test__( int* request, int* flag, int* status, int* __ierr );
void mpi_testall__(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void mpi_testany__(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void mpi_test_cancelled__(int * status,int * flag,int * __ierr);
void mpi_testsome__(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void mpi_topo_test__(int * comm,int * status,int * __ierr);
void mpi_waitall__(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void mpi_waitany__(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void mpi_wait__(int * request,int * status,int * __ierr);
void mpi_waitsome__(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double mpi_wtime__();
double mpi_wtick__();
void mpi_barrier__(int *comm,int * __ierr );
void mpi_bcast__(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void mpi_reduce__(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void mpi_reduce_scatter__(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void mpi_allreduce__(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void mpi_allgather__(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_allgatherv__(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void mpi_alltoall__(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_alltoallv__(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void mpi_gather__(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void mpi_gatherv__(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void mpi_scatter__(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void mpi_scatterv__(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void mpi_scan__(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void mpi_address__(void * particle,int * disp,int * __ierr);
void mpi_get_count__(int * status,int * ddt,int *cnt,int * __ierr);
void mpi_get_elements__(int * status,int * ddt,int * cnt,int * __ierr);
void mpi_type_free__(int * ddt,int * __ierr);
void mpi_type_size__(int * ddt,int *size,int * __ierr);
void mpi_type_extent__(int * ddt,int *extent,int * __ierr);
void mpi_type_struct__(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void mpi_type_contiguous__(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void mpi_type_vector__(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void mpi_type_hvector__(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void mpi_type_indexed__(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void mpi_type_hindexed__(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void mpi_type_lb__(int * ddt,int * displacement,int * __ierr);
void mpi_type_ub__(int * ddt,int * displacement,int * __ierr);
void mpi_type_commit__(int * ddt,int * __ierr);
void mpi_pack_size__(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void mpi_pack__(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void mpi_unpack__(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void mpi_bsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_send__(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void mpi_recv__(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void mpi_sendrecv__(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void mpi_probe__(int *source,int *tag,int *comm,int *status,int  *__ierr);
void mpi_rsend__(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_sendrecv_replace__(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void mpi_ssend__(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void mpi_comm_rank__(int *comm,int * rank,int * __ierr );
void mpi_comm_size__(int *comm,int * size,int * __ierr );
void mpi_comm_dup__(int * comm,int * newcomm,int * __ierr);
void mpi_comm_create__(int * comm,int * group,int * newcomm,int * __ierr);
void mpi_comm_free__(int * comm,int * __ierr);
void mpi_comm_group__(int * comm,int * group,int * __ierr);
void mpi_comm_remote_group__(int * comm,int * group,int * __ierr);
void mpi_comm_remote_size__(int * comm,int * size,int * __ierr);
void mpi_comm_split__(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void mpi_comm_test_inter__(int * comm,int * flag,int * __ierr);
void mpi_comm_compare__ (int *comm1, int* comm2, int* result, int* __ierr);
void mpi_intercomm_create__(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void mpi_intercomm_merge__(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void mpi_init__( int *ierr );
void mpi_initialized__(int *flag,int * __ierr) ;
void mpi_finalize__(int * __ierr);
void mpi_abort__(int * comm,int * errorcode,int * __ierr);
void mpi_get_processor_name__( char *name, int* len, int* __ierr);

void mpi_comm_create_keyval__(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void mpi_comm_free_keyval__ (int * keyval,int * __ierr);
void mpi_comm_delete_attr__(int * comm,int * keyval,int * __ierr);
void mpi_comm_get_attr__(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void mpi_comm_set_attr__(int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_comm_create_errhandler__(int * function,int * errhandler,int * __ierr);
void mpi_comm_get_errhandler__(int * comm,int *errhandler,int * __ierr);
void mpi_comm_set_errhandler__(int * comm,int * errhandler,int * __ierr);
void mpi_comm_call_errhandler__(int *comm, int* errcode, int * __ierr );
void mpi_add_error_class__ ( int *errorclass, int * __ierr );
void mpi_add_error_code__ ( int* class, int *code, int* __ierr );
void mpi_add_error_string__ ( int *code,  char *string, int* __ierr );


/* Profiling Interface */
void pmpi_attr_delete__(int * comm,int * keyval,int * __ierr);
void pmpi_attr_get__(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void pmpi_attr_put__(int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_bsend_init__(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void pmpi_buffer_attach__(void * buffer,int * size,int * __ierr);
void pmpi_buffer_detach__(void * buffer,int * size,int * __ierr);
void pmpi_cancel__(int *request,int *__ierr);
void pmpi_cart_coords__(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void pmpi_cart_create__(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void pmpi_cartdim_get__(int * comm,int * ndims,int * __ierr);
void pmpi_cart_get__(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void pmpi_cart_map__(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void pmpi_cart_rank__(int * comm,int * coords,int * rank,int * __ierr);
void pmpi_cart_shift__(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void pmpi_cart_sub__(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void pmpi_dims_create__(int * nnodes,int * ndims,int * dims,int * __ierr);
void pmpi_errhandler_create__(int * function,int * errhandler,int * __ierr);
void pmpi_errhandler_free__(int * errhandler,int * __ierr);
void pmpi_errhandler_get__(int * comm,int *errhandler,int * __ierr);
void pmpi_errhandler_set__(int * comm,int * errhandler,int * __ierr);
void pmpi_error_class__(int * errorcode,int * errorclass,int * __ierr);
void pmpi_error_string__(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void pmpi_graph_create__(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void pmpi_graphdims_get__(int *comm,int *nnodes,int *nedges,int *__ierr);
void pmpi_graph_get__(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void pmpi_graph_map__(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void pmpi_graph_neighbors_count__(int * comm,int * rank,int * neighbors,
				int * __ierr);
void pmpi_graph_neighbors__(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void pmpi_group_compare__(int * group1,int *group2,int *result,int *__ierr);
void pmpi_group_difference__(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void pmpi_group_excl__(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void pmpi_group_free__(int * group,int * __ierr);
void pmpi_group_incl__(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void pmpi_group_intersection__(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void pmpi_group_rank__(int *group, int *rank, int *__ierr);
void pmpi_group_size__(int *group, int *size, int *__ierr);
void pmpi_group_range_excl__(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void pmpi_group_range_incl__(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void pmpi_group_translate_ranks__(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void pmpi_group_union__(int *group1,int *group2,int *newgroup,int *__ierr);
void pmpi_ibsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_iprobe__(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void pmpi_irecv__(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_irsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_isend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_issend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_keyval_create__(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void pmpi_keyval_free__(int * keyval,int * __ierr);
void pmpi_null_copy_fn__(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void pmpi_dup_fn__( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void pmpi_null_delete_fn__(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void pmpi_op_create__(int * function,int * commute, int * op,int * __ierr);
void pmpi_op_free__(int * op,int * __ierr);
void pmpi_pcontrol__(int *level,...);
void pmpi_recv_init__(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void pmpi_request_free__(int * request,int * __ierr);
void pmpi_rsend_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void pmpi_send_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void pmpi_ssend_init__(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void pmpi_startall__(int * cnt,int * array_of_requests,int * __ierr);
void pmpi_start__(int * request,int * __ierr);
void pmpi_test( int* request, int* flag, int* status, int* __ierr );
void pmpi_testall__(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void pmpi_testany__(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void pmpi_test_cancelled__(int * status,int * flag,int * __ierr);
void pmpi_testsome__(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void pmpi_topo_test__(int * comm,int * status,int * __ierr);
void pmpi_waitall__(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void pmpi_waitany__(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void pmpi_wait__(int * request,int * status,int * __ierr);
void pmpi_waitsome__(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double pmpi_wtime__();
double pmpi_wtick__();
void pmpi_barrier__(int *comm,int * __ierr );
void pmpi_bcast__(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void pmpi_reduce__(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void pmpi_reduce_scatter__(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void pmpi_allreduce__(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void pmpi_allgather__(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_allgatherv__(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void pmpi_alltoall__(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_alltoallv__(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void pmpi_gather__(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void pmpi_gatherv__(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void pmpi_scatter__(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void pmpi_scatterv__(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void pmpi_scan__(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void pmpi_address__(void * particle,int * disp,int * __ierr);
void pmpi_get_count__(int * status,int * ddt,int *cnt,int * __ierr);
void pmpi_get_elements__(int * status,int * ddt,int * cnt,int * __ierr);
void pmpi_type_free__(int * ddt,int * __ierr);
void pmpi_type_size__(int * ddt,int *size,int * __ierr);
void pmpi_type_extent__(int * ddt,int *extent,int * __ierr);
void pmpi_type_struct__(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_contiguous__(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void pmpi_type_vector__(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void pmpi_type_hvector__(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void pmpi_type_indexed__(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_hindexed__(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_lb__(int * ddt,int * displacement,int * __ierr);
void pmpi_type_ub__(int * ddt,int * displacement,int * __ierr);
void pmpi_type_commit__(int * ddt,int * __ierr);
void pmpi_pack_size__(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void pmpi_pack__(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void pmpi_unpack__(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void pmpi_bsend__(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_send__(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void pmpi_recv__(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void pmpi_sendrecv__(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void pmpi_probe__(int *source,int *tag,int *comm,int *status,int  *__ierr);
void pmpi_rsend__(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_sendrecv_replace__(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void pmpi_ssend__(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void pmpi_comm_rank__(int *comm,int * rank,int * __ierr );
void pmpi_comm_size__(int *comm,int * size,int * __ierr );
void pmpi_comm_dup__(int * comm,int * newcomm,int * __ierr);
void pmpi_comm_create__(int * comm,int * group,int * newcomm,int * __ierr);
void pmpi_comm_free__(int * comm,int * __ierr);
void pmpi_comm_group__(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_group__(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_size__(int * comm,int * size,int * __ierr);
void pmpi_comm_split__(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void pmpi_comm_test_inter__(int * comm,int * flag,int * __ierr);
void pmpi_comm_compare__ (int *comm1, int* comm2, int* result, int* __ierr);
void pmpi_intercomm_create__(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void pmpi_intercomm_merge__(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void pmpi_init__( int *ierr );
void pmpi_initialized__(int *flag,int * __ierr) ;
void pmpi_finalize__(int * __ierr);
void pmpi_abort__(int * comm,int * errorcode,int * __ierr);
void pmpi_get_processor_name__( char *name, int* len, int* __ierr);

void pmpi_comm_create_keyval__(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void pmpi_comm_free_keyval__ (int * keyval,int * __ierr);
void pmpi_comm_delete_attr__(int * comm,int * keyval,int * __ierr);
void pmpi_comm_get_attr__(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void pmpi_comm_set_attr__(int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_comm_create_errhandler__(int * function,int * errhandler,int * __ierr);
void pmpi_comm_get_errhandler__(int * comm,int *errhandler,int * __ierr);
void pmpi_comm_set_errhandler__(int * comm,int * errhandler,int * __ierr);
void pmpi_comm_call_errhandler__(int *comm, int* errcode, int * __ierr );
void pmpi_add_error_class__ ( int *errorclass, int * __ierr );
void pmpi_add_error_code__ ( int* class, int *code, int* __ierr );
void pmpi_add_error_string__ ( int *code,  char *string, int* __ierr );


#elif defined(FORTRANNOUNDERSCORE)

void mpi_attr_delete(int * comm,int * keyval,int * __ierr);
void mpi_attr_get(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void mpi_attr_put(int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_bsend_init(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void mpi_buffer_attach(void * buffer,int * size,int * __ierr);
void mpi_buffer_detach(void * buffer,int * size,int * __ierr);
void mpi_cancel(int *request,int *__ierr);
void mpi_cart_coords(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void mpi_cart_create(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void mpi_cartdim_get(int * comm,int * ndims,int * __ierr);
void mpi_cart_get(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void mpi_cart_map(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void mpi_cart_rank(int * comm,int * coords,int * rank,int * __ierr);
void mpi_cart_shift(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void mpi_cart_sub(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void mpi_dims_create(int * nnodes,int * ndims,int * dims,int * __ierr);
void mpi_errhandler_create(int * function,int * errhandler,int * __ierr);
void mpi_errhandler_free(int * errhandler,int * __ierr);
void mpi_errhandler_get(int * comm,int *errhandler,int * __ierr);
void mpi_errhandler_set(int * comm,int * errhandler,int * __ierr);
void mpi_error_class(int * errorcode,int * errorclass,int * __ierr);
void mpi_error_string(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void mpi_graph_create(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void mpi_graphdims_get(int *comm,int *nnodes,int *nedges,int *__ierr);
void mpi_graph_get(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void mpi_graph_map(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void mpi_graph_neighbors_count(int * comm,int * rank,int * neighbors,
				int * __ierr);
void mpi_graph_neighbors(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void mpi_group_compare(int * group1,int *group2,int *result,int *__ierr);
void mpi_group_difference(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void mpi_group_excl(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void mpi_group_free(int * group,int * __ierr);
void mpi_group_incl(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void mpi_group_intersection(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void mpi_group_rank(int *group, int *rank, int *__ierr);
void mpi_group_size(int *group, int *size, int *__ierr);
void mpi_group_range_excl(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void mpi_group_range_incl(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void mpi_group_translate_ranks(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void mpi_group_union(int *group1,int *group2,int *newgroup,int *__ierr);
void mpi_ibsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_iprobe(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void mpi_irecv(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_irsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_isend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_issend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_keyval_create(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void mpi_keyval_free(int * keyval,int * __ierr);
void mpi_null_copy_fn(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void mpi_dup_fn( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void mpi_null_delete_fn(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void mpi_op_create(int * function,int * commute, int * op,int * __ierr);
void mpi_op_free(int * op,int * __ierr);
void mpi_pcontrol(int *level,...);
void mpi_recv_init(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void mpi_request_free(int * request,int * __ierr);
void mpi_rsend_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void mpi_send_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void mpi_ssend_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void mpi_startall(int * cnt,int * array_of_requests,int * __ierr);
void mpi_start(int * request,int * __ierr);
void mpi_test( int* request, int* flag, int* status, int* __ierr );
void mpi_testall(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void mpi_testany(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void mpi_test_cancelled(int * status,int * flag,int * __ierr);
void mpi_testsome(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void mpi_topo_test(int * comm,int * status,int * __ierr);
void mpi_waitall(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void mpi_waitany(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void mpi_wait(int * request,int * status,int * __ierr);
void mpi_waitsome(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double mpi_wtime();
double mpi_wtick();
void mpi_barrier(int *comm,int * __ierr );
void mpi_bcast(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void mpi_reduce(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void mpi_reduce_scatter(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void mpi_allreduce(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void mpi_allgather(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_allgatherv(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void mpi_alltoall(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_alltoallv(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void mpi_gather(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void mpi_gatherv(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void mpi_scatter(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void mpi_scatterv(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void mpi_scan(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void mpi_address(void * particle,int * disp,int * __ierr);
void mpi_get_count(int * status,int * ddt,int *cnt,int * __ierr);
void mpi_get_elements(int * status,int * ddt,int * cnt,int * __ierr);
void mpi_type_free(int * ddt,int * __ierr);
void mpi_type_size(int * ddt,int *size,int * __ierr);
void mpi_type_extent(int * ddt,int *extent,int * __ierr);
void mpi_type_struct(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void mpi_type_contiguous(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void mpi_type_vector(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void mpi_type_hvector(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void mpi_type_indexed(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void mpi_type_hindexed(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void mpi_type_lb(int * ddt,int * displacement,int * __ierr);
void mpi_type_ub(int * ddt,int * displacement,int * __ierr);
void mpi_type_commit(int * ddt,int * __ierr);
void mpi_pack_size(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void mpi_pack(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void mpi_unpack(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void mpi_bsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_send(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void mpi_recv(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void mpi_sendrecv(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void mpi_probe(int *source,int *tag,int *comm,int *status,int  *__ierr);
void mpi_rsend(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_sendrecv_replace(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void mpi_ssend(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void mpi_comm_rank(int *comm,int * rank,int * __ierr );
void mpi_comm_size(int *comm,int * size,int * __ierr );
void mpi_comm_dup(int * comm,int * newcomm,int * __ierr);
void mpi_comm_create(int * comm,int * group,int * newcomm,int * __ierr);
void mpi_comm_free(int * comm,int * __ierr);
void mpi_comm_group(int * comm,int * group,int * __ierr);
void mpi_comm_remote_group(int * comm,int * group,int * __ierr);
void mpi_comm_remote_size(int * comm,int * size,int * __ierr);
void mpi_comm_split(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void mpi_comm_test_inter(int * comm,int * flag,int * __ierr);
void mpi_comm_compare (int *comm1, int* comm2, int* result, int* __ierr);
void mpi_intercomm_create(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void mpi_intercomm_merge(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void mpi_init( int *ierr );
void mpi_initialized(int *flag,int * __ierr) ;
void mpi_finalize(int * __ierr);
void mpi_abort(int * comm,int * errorcode,int * __ierr);
void mpi_get_processor_name( char *name, int* len, int* __ierr);

void mpi_comm_create_keyval(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void mpi_comm_free_keyval (int * keyval,int * __ierr);
void mpi_comm_delete_attr(int * comm,int * keyval,int * __ierr);
void mpi_comm_get_attr(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void mpi_comm_set_attr(int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_comm_create_errhandler(int * function,int * errhandler,int * __ierr);
void mpi_comm_get_errhandler(int * comm,int *errhandler,int * __ierr);
void mpi_comm_set_errhandler(int * comm,int * errhandler,int * __ierr);
void mpi_comm_call_errhandler(int *comm, int* errcode, int * __ierr );
void mpi_add_error_class ( int *errorclass, int * __ierr );
void mpi_add_error_code ( int* class, int *code, int* __ierr );
void mpi_add_error_string ( int *code,  char *string, int* __ierr );


/* Profiling Interface */
void pmpi_attr_delete(int * comm,int * keyval,int * __ierr);
void pmpi_attr_get(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void pmpi_attr_put(int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_bsend_init(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void pmpi_buffer_attach(void * buffer,int * size,int * __ierr);
void pmpi_buffer_detach(void * buffer,int * size,int * __ierr);
void pmpi_cancel(int *request,int *__ierr);
void pmpi_cart_coords(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void pmpi_cart_create(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void pmpi_cartdim_get(int * comm,int * ndims,int * __ierr);
void pmpi_cart_get(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void pmpi_cart_map(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void pmpi_cart_rank(int * comm,int * coords,int * rank,int * __ierr);
void pmpi_cart_shift(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void pmpi_cart_sub(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void pmpi_dims_create(int * nnodes,int * ndims,int * dims,int * __ierr);
void pmpi_errhandler_create(int * function,int * errhandler,int * __ierr);
void pmpi_errhandler_free(int * errhandler,int * __ierr);
void pmpi_errhandler_get(int * comm,int *errhandler,int * __ierr);
void pmpi_errhandler_set(int * comm,int * errhandler,int * __ierr);
void pmpi_error_class(int * errorcode,int * errorclass,int * __ierr);
void pmpi_error_string(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void pmpi_graph_create(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void pmpi_graphdims_get(int *comm,int *nnodes,int *nedges,int *__ierr);
void pmpi_graph_get(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void pmpi_graph_map(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void pmpi_graph_neighbors_count(int * comm,int * rank,int * neighbors,
				int * __ierr);
void pmpi_graph_neighbors(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void pmpi_group_compare(int * group1,int *group2,int *result,int *__ierr);
void pmpi_group_difference(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void pmpi_group_excl(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void pmpi_group_free(int * group,int * __ierr);
void pmpi_group_incl(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void pmpi_group_intersection(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void pmpi_group_rank(int *group, int *rank, int *__ierr);
void pmpi_group_size(int *group, int *size, int *__ierr);
void pmpi_group_range_excl(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void pmpi_group_range_incl(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void pmpi_group_translate_ranks(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void pmpi_group_union(int *group1,int *group2,int *newgroup,int *__ierr);
void pmpi_ibsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_iprobe(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void pmpi_irecv(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_irsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_isend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_issend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_keyval_create(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void pmpi_keyval_free(int * keyval,int * __ierr);
void pmpi_null_copy_fn(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void pmpi_dup_fn( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void pmpi_null_delete_fn(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void pmpi_op_create(int * function,int * commute, int * op,int * __ierr);
void pmpi_op_free(int * op,int * __ierr);
void pmpi_pcontrol(int *level,...);
void pmpi_recv_init(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void pmpi_request_free(int * request,int * __ierr);
void pmpi_rsend_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void pmpi_send_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void pmpi_ssend_init(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void pmpi_startall(int * cnt,int * array_of_requests,int * __ierr);
void pmpi_start(int * request,int * __ierr);
void pmpi_test( int* request, int* flag, int* status, int* __ierr );
void pmpi_testall(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void pmpi_testany(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void pmpi_test_cancelled(int * status,int * flag,int * __ierr);
void pmpi_testsome(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void pmpi_topo_test(int * comm,int * status,int * __ierr);
void pmpi_waitall(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void pmpi_waitany(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void pmpi_wait(int * request,int * status,int * __ierr);
void pmpi_waitsome(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double pmpi_wtime();
double pmpi_wtick();
void pmpi_barrier(int *comm,int * __ierr );
void pmpi_bcast(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void pmpi_reduce(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void pmpi_reduce_scatter(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void pmpi_allreduce(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void pmpi_allgather(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_allgatherv(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void pmpi_alltoall(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_alltoallv(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void pmpi_gather(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void pmpi_gatherv(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void pmpi_scatter(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void pmpi_scatterv(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void pmpi_scan(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void pmpi_address(void * particle,int * disp,int * __ierr);
void pmpi_get_count(int * status,int * ddt,int *cnt,int * __ierr);
void pmpi_get_elements(int * status,int * ddt,int * cnt,int * __ierr);
void pmpi_type_free(int * ddt,int * __ierr);
void pmpi_type_size(int * ddt,int *size,int * __ierr);
void pmpi_type_extent(int * ddt,int *extent,int * __ierr);
void pmpi_type_struct(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_contiguous(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void pmpi_type_vector(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void pmpi_type_hvector(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void pmpi_type_indexed(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_hindexed(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_lb(int * ddt,int * displacement,int * __ierr);
void pmpi_type_ub(int * ddt,int * displacement,int * __ierr);
void pmpi_type_commit(int * ddt,int * __ierr);
void pmpi_pack_size(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void pmpi_pack(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void pmpi_unpack(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void pmpi_bsend(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_send(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void pmpi_recv(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void pmpi_sendrecv(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void pmpi_probe(int *source,int *tag,int *comm,int *status,int  *__ierr);
void pmpi_rsend(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_sendrecv_replace(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void pmpi_ssend(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void pmpi_comm_rank(int *comm,int * rank,int * __ierr );
void pmpi_comm_size(int *comm,int * size,int * __ierr );
void pmpi_comm_dup(int * comm,int * newcomm,int * __ierr);
void pmpi_comm_create(int * comm,int * group,int * newcomm,int * __ierr);
void pmpi_comm_free(int * comm,int * __ierr);
void pmpi_comm_group(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_group(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_size(int * comm,int * size,int * __ierr);
void pmpi_comm_split(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void pmpi_comm_test_inter(int * comm,int * flag,int * __ierr);
void pmpi_comm_compare (int *comm1, int* comm2, int* result, int* __ierr);
void pmpi_intercomm_create(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void pmpi_intercomm_merge(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void pmpi_init( int *ierr );
void pmpi_initialized(int *flag,int * __ierr) ;
void pmpi_finalize(int * __ierr);
void pmpi_abort(int * comm,int * errorcode,int * __ierr);
void pmpi_get_processor_name( char *name, int* len, int* __ierr);

void pmpi_comm_create_keyval(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void pmpi_comm_free_keyval (int * keyval,int * __ierr);
void pmpi_comm_delete_attr(int * comm,int * keyval,int * __ierr);
void pmpi_comm_get_attr(int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void pmpi_comm_set_attr(int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_comm_create_errhandler(int * function,int * errhandler,int * __ierr);
void pmpi_comm_get_errhandler(int * comm,int *errhandler,int * __ierr);
void pmpi_comm_set_errhandler(int * comm,int * errhandler,int * __ierr);
void pmpi_comm_call_errhandler(int *comm, int* errcode, int * __ierr );
void pmpi_add_error_class ( int *errorclass, int * __ierr );
void pmpi_add_error_code ( int* class, int *code, int* __ierr );
void pmpi_add_error_string ( int *code,  char *string, int* __ierr );


#else

void mpi_attr_delete_(int * comm,int * keyval,int * __ierr);
void mpi_attr_get_(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void mpi_attr_put_(int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_bsend_init_(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void mpi_buffer_attach_(void * buffer,int * size,int * __ierr);
void mpi_buffer_detach_(void * buffer,int * size,int * __ierr);
void mpi_cancel_(int *request,int *__ierr);
void mpi_cart_coords_(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void mpi_cart_create_(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void mpi_cartdim_get_(int * comm,int * ndims,int * __ierr);
void mpi_cart_get_(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void mpi_cart_map_(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void mpi_cart_rank_(int * comm,int * coords,int * rank,int * __ierr);
void mpi_cart_shift_(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void mpi_cart_sub_(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void mpi_dims_create_(int * nnodes,int * ndims,int * dims,int * __ierr);
void mpi_errhandler_create_(int * function,int * errhandler,int * __ierr);
void mpi_errhandler_free_(int * errhandler,int * __ierr);
void mpi_errhandler_get_(int * comm,int *errhandler,int * __ierr);
void mpi_errhandler_set_(int * comm,int * errhandler,int * __ierr);
void mpi_error_class_(int * errorcode,int * errorclass,int * __ierr);
void mpi_error_string_(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void mpi_graph_create_(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void mpi_graphdims_get_(int *comm,int *nnodes,int *nedges,int *__ierr);
void mpi_graph_get_(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void mpi_graph_map_(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void mpi_graph_neighbors_count_(int * comm,int * rank,int * neighbors,
				int * __ierr);
void mpi_graph_neighbors_(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void mpi_group_compare_(int * group1,int *group2,int *result,int *__ierr);
void mpi_group_difference_(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void mpi_group_excl_(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void mpi_group_free_(int * group,int * __ierr);
void mpi_group_incl_(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void mpi_group_intersection_(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void mpi_group_rank_(int *group, int *rank, int *__ierr);
void mpi_group_size_(int *group, int *size, int *__ierr);
void mpi_group_range_excl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void mpi_group_range_incl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void mpi_group_translate_ranks_(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void mpi_group_union_(int *group1,int *group2,int *newgroup,int *__ierr);
void mpi_ibsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_iprobe_(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void mpi_irecv_(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_irsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_isend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void mpi_issend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void mpi_keyval_create_(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void mpi_keyval_free_(int * keyval,int * __ierr);
void mpi_null_copy_fn_(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void mpi_dup_fn_( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void mpi_null_delete_fn_(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void mpi_op_create_(int * function,int * commute, int * op,int * __ierr);
void mpi_op_free_(int * op,int * __ierr);
void mpi_pcontrol_(int *level,...);
void mpi_recv_init_(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void mpi_request_free_(int * request,int * __ierr);
void mpi_rsend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void mpi_send_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void mpi_ssend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void mpi_startall_(int * cnt,int * array_of_requests,int * __ierr);
void mpi_start_(int * request,int * __ierr);
void mpi_test( int* request, int* flag, int* status, int* __ierr );
void mpi_testall_(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void mpi_testany_(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void mpi_test_cancelled_(int * status,int * flag,int * __ierr);
void mpi_testsome_(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void mpi_topo_test_(int * comm,int * status,int * __ierr);
void mpi_waitall_(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void mpi_waitany_(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void mpi_wait_(int * request,int * status,int * __ierr);
void mpi_waitsome_(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double mpi_wtime_();
double mpi_wtick_();
void mpi_barrier_ (int *comm,int * __ierr );
void mpi_bcast_(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void mpi_reduce_(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void mpi_reduce_scatter_(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void mpi_allreduce_(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void mpi_allgather_(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_allgatherv_(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void mpi_alltoall_(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void mpi_alltoallv_(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void mpi_gather_(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void mpi_gatherv_(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void mpi_scatter_(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void mpi_scatterv_(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void mpi_scan_(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void mpi_address_(void * particle,int * disp,int * __ierr);
void mpi_get_count_(int * status,int * ddt,int *cnt,int * __ierr);
void mpi_get_elements_(int * status,int * ddt,int * cnt,int * __ierr);
void mpi_type_free_(int * ddt,int * __ierr);
void mpi_type_size_(int * ddt,int *size,int * __ierr);
void mpi_type_extent_(int * ddt,int *extent,int * __ierr);
void mpi_type_struct_(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void mpi_type_contiguous_(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void mpi_type_vector_(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void mpi_type_hvector_(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void mpi_type_indexed_(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void mpi_type_hindexed_(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void mpi_type_lb_(int * ddt,int * displacement,int * __ierr);
void mpi_type_ub_(int * ddt,int * displacement,int * __ierr);
void mpi_type_commit_(int * ddt,int * __ierr);
void mpi_pack_size_(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void mpi_pack_(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void mpi_unpack_(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void mpi_bsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_send_(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void mpi_recv_(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void mpi_sendrecv_(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void mpi_probe_(int *source,int *tag,int *comm,int *status,int  *__ierr);
void mpi_rsend_(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void mpi_sendrecv_replace_(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void mpi_ssend_(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void mpi_comm_rank_(int *comm,int * rank,int * __ierr );
void mpi_comm_size_(int *comm,int * size,int * __ierr );
void mpi_comm_dup_(int * comm,int * newcomm,int * __ierr);
void mpi_comm_create_(int * comm,int * group,int * newcomm,int * __ierr);
void mpi_comm_free_(int * comm,int * __ierr);
void mpi_comm_group_(int * comm,int * group,int * __ierr);
void mpi_comm_remote_group_(int * comm,int * group,int * __ierr);
void mpi_comm_remote_size_(int * comm,int * size,int * __ierr);
void mpi_comm_split_(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void mpi_comm_test_inter_(int * comm,int * flag,int * __ierr);
void mpi_comm_compare_ (int *comm1, int* comm2, int* result, int* __ierr);
void mpi_intercomm_create_(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void mpi_intercomm_merge_(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void mpi_init_( int *ierr );
void mpi_initialized_(int *flag,int * __ierr) ;
void mpi_finalize_(int * __ierr);
void mpi_abort_(int * comm,int * errorcode,int * __ierr);
void mpi_get_processor_name_( char *name, int* len, int* __ierr);

void mpi_comm_create_keyval_(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void mpi_comm_free_keyval_ (int * keyval,int * __ierr);
void mpi_comm_delete_attr_(int * comm,int * keyval,int * __ierr);
void mpi_comm_get_attr_ (int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void mpi_comm_set_attr_ (int *comm,int *keyval,int *attribute_val,int *__ierr);
void mpi_comm_create_errhandler_ (int * function,int * errhandler,int * __ierr);
void mpi_comm_get_errhandler_ (int * comm,int *errhandler,int * __ierr);
void mpi_comm_set_errhandler_ (int * comm,int * errhandler,int * __ierr);
void mpi_comm_call_errhandler_ (int *comm, int* errcode, int * __ierr );
void mpi_add_error_class_ ( int *errorclass, int * __ierr );
void mpi_add_error_code_ ( int* class, int *code, int* __ierr );
void mpi_add_error_string_ ( int *code,  char *string, int* __ierr );


/* Profiling Interface */
void pmpi_attr_delete_(int * comm,int * keyval,int * __ierr);
void pmpi_attr_get_(int *comm,int *keyval,int *attribute_val,int *flag,
		   int *__ierr);
void pmpi_attr_put_(int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_bsend_init_(void *buf,int *cnt,int * ddt,int * dest,int * tag,
		     int *comm,int *request,int * __ierr);
void pmpi_buffer_attach_(void * buffer,int * size,int * __ierr);
void pmpi_buffer_detach_(void * buffer,int * size,int * __ierr);
void pmpi_cancel_(int *request,int *__ierr);
void pmpi_cart_coords_(int *comm,int *rank,int *maxdims,int *coords,
		      int *__ierr);
void pmpi_cart_create_(int * comm_old,int * ndims,int * dims,int * periods,
		      int * reorder,int * comm_cart,int * __ierr);
void pmpi_cartdim_get_(int * comm,int * ndims,int * __ierr);
void pmpi_cart_get_(int * comm,int * maxdims,int * dims,int * periods,
		   int * coords,int * __ierr);
void pmpi_cart_map_(int * comm,int * ndims,int * dims,int * periods,
		   int * newrank,int * __ierr);
void pmpi_cart_rank_(int * comm,int * coords,int * rank,int * __ierr);
void pmpi_cart_shift_(int *comm,int *direction,int *disp,
		     int *rank_source, int *rank_dest,int *__ierr);
void pmpi_cart_sub_(int * comm,int * remain_dims,int * newcomm,int *__ierr);
void pmpi_dims_create_(int * nnodes,int * ndims,int * dims,int * __ierr);
void pmpi_errhandler_create_(int * function,int * errhandler,int * __ierr);
void pmpi_errhandler_free_(int * errhandler,int * __ierr);
void pmpi_errhandler_get_(int * comm,int *errhandler,int * __ierr);
void pmpi_errhandler_set_(int * comm,int * errhandler,int * __ierr);
void pmpi_error_class_(int * errorcode,int * errorclass,int * __ierr);
void pmpi_error_string_(int * errorcode,char * string,int * resultlen,
		       int * __ierr);
void pmpi_graph_create_(int *comm_old,int *nnodes,int *index,int *edges,
		       int * reorder,int * comm_graph,int * __ierr);
void pmpi_graphdims_get_(int *comm,int *nnodes,int *nedges,int *__ierr);
void pmpi_graph_get_(int * comm,int * maxindex,int * maxedges,int *index,
		    int * edges,int * __ierr);
void pmpi_graph_map_(int * comm,int * nnodes,int * index,int * edges,
		    int * newrank,int * __ierr);
void pmpi_graph_neighbors_count_(int * comm,int * rank,int * neighbors,
				int * __ierr);
void pmpi_graph_neighbors_(int * comm,int * rank,int * maxneighbors,
			  int * neighbors,int * __ierr);
void pmpi_group_compare_(int * group1,int *group2,int *result,int *__ierr);
void pmpi_group_difference_(int *group1,int *group2,int *newgroup,
			   int * __ierr);
void pmpi_group_excl_(int * group,int * n,int * ranks,int * newgroup,
		     int * __ierr);
void pmpi_group_free_(int * group,int * __ierr);
void pmpi_group_incl_(int * group,int * n,int * ranks, int * newgroup,
		     int * __ierr);
void pmpi_group_intersection_(int * group1,int * group2,int * newgroup,
			     int * __ierr);
void pmpi_group_rank_(int *group, int *rank, int *__ierr);
void pmpi_group_size_(int *group, int *size, int *__ierr);
void pmpi_group_range_excl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr) ;
void pmpi_group_range_incl_(int *group,int *n,int ranges[][3],int *newgroup,
			   int * __ierr);
void pmpi_group_translate_ranks_(int * group1,int * n,int * ranks1,
				int * group2,int * ranks2,int * __ierr);
void pmpi_group_union_(int *group1,int *group2,int *newgroup,int *__ierr);
void pmpi_ibsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_iprobe_(int * source,int * tag,int * comm,int * flag,
		 int * status,int * __ierr);
void pmpi_irecv_(void * buf,int * cnt,int * ddt,int * source,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_irsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_isend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * request,int * __ierr);
void pmpi_issend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		 int * comm,int * request,int * __ierr);
void pmpi_keyval_create_(int * copy_fn,int * delete_fn,int * keyval,
			void * extra_state,int * __ierr);
void pmpi_keyval_free_(int * keyval,int * __ierr);
void pmpi_null_copy_fn_(int *comm,int *k,void *exs,void *ain,
		       void *aout, int *f ,int * __ierr);
void pmpi_dup_fn_( int *comm, int *k, int *exs, int *ain, int *aout, 
		   int *f, int *__ierr );
void pmpi_null_delete_fn_(int *comm ,int *k, void *attr_val, void *exs,
			 int *__ierr);
void pmpi_op_create_(int * function,int * commute, int * op,int * __ierr);
void pmpi_op_free_(int * op,int * __ierr);
void pmpi_pcontrol_(int *level,...);
void pmpi_recv_init_(void * buf,int *cnt,int *ddt,int *source,int *tag,
		    int * comm,int *request,int *__ierr);
void pmpi_request_free_(int * request,int * __ierr);
void pmpi_rsend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int * request,int * __ierr);
void pmpi_send_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		    int * comm,int * request,int * __ierr);
void pmpi_ssend_init_(void *buf,int *cnt,int *ddt,int *dest,int *tag,
		     int *comm,int *request,int *__ierr);
void pmpi_startall_(int * cnt,int * array_of_requests,int * __ierr);
void pmpi_start_(int * request,int * __ierr);
void pmpi_test( int* request, int* flag, int* status, int* __ierr );
void pmpi_testall_(int * cnt,int * array_of_requests,int * flag,
		  int * array_of_statuses,int * __ierr);
void pmpi_testany_(int * cnt,int * array_of_requests,int * index,
		  int * flag,int * status,int * __ierr);
void pmpi_test_cancelled_(int * status,int * flag,int * __ierr);
void pmpi_testsome_(int * incount,int * array_of_requests,int * outcount,
		   int * array_of_indices,int * array_of_statuses,
		   int * __ierr);
void pmpi_topo_test_(int * comm,int * status,int * __ierr);
void pmpi_waitall_(int * cnt,int * array_of_requests,
		  int * array_of_statuses,int * __ierr);
void pmpi_waitany_(int * cnt,int * array_of_requests,int * index,
		  int * status,int * __ierr);
void pmpi_wait_(int * request,int * status,int * __ierr);
void pmpi_waitsome_(int *incount,int *array_of_requests,int *outcount,
		   int *array_of_indices,int *array_of_statuses,
		   int *__ierr);
double pmpi_wtime_();
double pmpi_wtick_();
void pmpi_barrier_ (int *comm,int * __ierr );
void pmpi_bcast_(void * buf,int * cnt,int * ddt,int  * root,int * comm,
		int * __ierr) ;
void pmpi_reduce_(void * sbuf,void * rbuf,int *cnt,int * ddt,
		 int * op,int * root,int * comm,int * __ierr) ;
void pmpi_reduce_scatter_(void * sbuf,void * rbuf,int * cnt,int * ddt,
			 int * op,int * comm,int * __ierr);
void pmpi_allreduce_(void * sbuf,void * rbuf,int * cnt,int * ddt,
		    int * op,int * comm,int * __ierr) ;
void pmpi_allgather_(void *sbuf,int * scnt, int * sddt,void *rbuf,
		    int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_allgatherv_(void *sbuf,int * scnt,int * sddt,void *rbuf, 
		     int *rcnt,int *displs,int * rddt,int * comm,
		     int * __ierr);
void pmpi_alltoall_(void* sbuf,int * scnt,int * sddt,void * rbuf,
		   int * rcnt,int * rddt,int * comm,int * __ierr) ;
void pmpi_alltoallv_(void * sbuf,int * scnt,int * sdisp,int * sddt,
		    void * rbuf,int * rcnt,int * rdisp,int * rddt,
		    int * comm,int * __ierr) ;
void pmpi_gather_(void * sbuf,int * scnt,int * sddt,void *rbuf,
		 int * rcnt,int * rddt,int * root,int * comm,
		 int * __ierr) ;
void pmpi_gatherv_(void * sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int *rdisp,int *rddt,int *root,int *comm,int *__ierr) ;
void pmpi_scatter_(void *sbuf,int * scnt,int * sddt,void *rbuf,int *rcnt,
		  int * rddt,int * root,int * comm,int * __ierr);
void pmpi_scatterv_(void *sbuf,int * scnt,int * sdisp,int * sddt,
		   void *rbuf,int *rcnt,int *rddt,int *root,int *comm,
		   int * __ierr) ;
void pmpi_scan_(void * sbuf,void * rbuf,int * cnt,int * ddt,int *op,
	       int * comm,int * __ierr) ;
void pmpi_address_(void * particle,int * disp,int * __ierr);
void pmpi_get_count_(int * status,int * ddt,int *cnt,int * __ierr);
void pmpi_get_elements_(int * status,int * ddt,int * cnt,int * __ierr);
void pmpi_type_free_(int * ddt,int * __ierr);
void pmpi_type_size_(int * ddt,int *size,int * __ierr);
void pmpi_type_extent_(int * ddt,int *extent,int * __ierr);
void pmpi_type_struct_(int * cnt,int * array_block,int * array_disp,
		      int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_contiguous_(int *cnt,int *ddt,int *ret_handle,int *__ierr);
void pmpi_type_vector_(int * cnt,int *block_length,int *stride,int *ddt,
		      int * ret_handle,int * __ierr);
void pmpi_type_hvector_(int * cnt,int * block_length,int * stride,int *ddt,
		       int * ret_handle,int * __ierr);
void pmpi_type_indexed_(int * cnt,int * array_blocks,int * array_disp,
		       int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_hindexed_(int * cnt,int * array_blocks,int * array_disp,
			int * ddt,int * ret_handle,int * __ierr);
void pmpi_type_lb_(int * ddt,int * displacement,int * __ierr);
void pmpi_type_ub_(int * ddt,int * displacement,int * __ierr);
void pmpi_type_commit_(int * ddt,int * __ierr);
void pmpi_pack_size_(int * cnt,int *ddt,int *comm,int *size,int *__ierr);
void pmpi_pack_(void *inbuf,int *cnt, int *ddt,void *outbuf,int *out_size,
	       int * position,int * comm,int * __ierr);
void pmpi_unpack_(void * inbuf,int * insize,int * position,void * outbuf,
		 int * cnt,int *ddt,int *comm, int * __ierr);
void pmpi_bsend_(void * buf,int * cnt,int * ddt,int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_send_(void * buf,int * count,int * datatype,int *dest,int *tag,
	       int * comm,int * __ierr );
void pmpi_recv_(void * buf,int * count,int * datatype,int * source,
	       int * tag,int * comm,int * status,int * __ierr);
void pmpi_sendrecv_(void *sendbuf,int *sendcount,int *sendtype,int * dest,
		   int * sendtag,void * recvbuf, int *recvcount,
		   int * recvtype,int * source,int * recvtag,
		   int * comm,int * status, int * __ierr );
void pmpi_probe_(int *source,int *tag,int *comm,int *status,int  *__ierr);
void pmpi_rsend_(void * buf,int * cnt,int * ddt, int * dest,int * tag,
		int * comm,int * __ierr);
void pmpi_sendrecv_replace_(void * buf,int * cnt,int * ddt,int * dest,
			   int * sendtag,int * source,int * recvtag,
			   int * comm,int * status,int * __ierr);
void pmpi_ssend_(void *buf,int *cnt,int *ddt,int *dest,int *tag,int *comm,
		int * __ierr);
void pmpi_comm_rank_(int *comm,int * rank,int * __ierr );
void pmpi_comm_size_(int *comm,int * size,int * __ierr );
void pmpi_comm_dup_(int * comm,int * newcomm,int * __ierr);
void pmpi_comm_create_(int * comm,int * group,int * newcomm,int * __ierr);
void pmpi_comm_free_(int * comm,int * __ierr);
void pmpi_comm_group_(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_group_(int * comm,int * group,int * __ierr);
void pmpi_comm_remote_size_(int * comm,int * size,int * __ierr);
void pmpi_comm_split_(int * comm,int * color,int * key,int * newcomm,
		     int * __ierr);
void pmpi_comm_test_inter_(int * comm,int * flag,int * __ierr);
void pmpi_comm_compare_ (int *comm1, int* comm2, int* result, int* __ierr);
void pmpi_intercomm_create_(int *local_comm, int *local_leader,
			   int *peer_comm, int *remote_leader,int *tag,
			   int *newintercom, int *__ierr);
void pmpi_intercomm_merge_(int *intercomm,int * high,int *newintracom,
			  int *__ierr);
void pmpi_init_( int *ierr );
void pmpi_initialized_(int *flag,int * __ierr) ;
void pmpi_finalize_(int * __ierr);
void pmpi_abort_(int * comm,int * errorcode,int * __ierr);
void pmpi_get_processor_name_( char *name, int* len, int* __ierr);

void pmpi_comm_create_keyval_(int * copy_fn,int * delete_fn,int * keyval,
			     void * extra_state,int * __ierr);
void pmpi_comm_free_keyval_ (int * keyval,int * __ierr);
void pmpi_comm_delete_attr_(int * comm,int * keyval,int * __ierr);
void pmpi_comm_get_attr_ (int *comm,int *keyval,int *attribute_val,int *flag,
			int *__ierr);
void pmpi_comm_set_attr_ (int *comm,int *keyval,int *attribute_val,int *__ierr);
void pmpi_comm_create_errhandler_ (int * function,int * errhandler,int * __ierr);
void pmpi_comm_get_errhandler_ (int * comm,int *errhandler,int * __ierr);
void pmpi_comm_set_errhandler_ (int * comm,int * errhandler,int * __ierr);
void pmpi_comm_call_errhandler_ (int *comm, int* errcode, int * __ierr );
void pmpi_add_error_class_ ( int *errorclass, int * __ierr );
void pmpi_add_error_code_ ( int* class, int *code, int* __ierr );
void pmpi_add_error_string_ ( int *code,  char *string, int* __ierr );


#endif /*FORTRANCAPS etc. */


#endif
