/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
	     Edgar Gabriel <egabriel@cs.utk.edu>
	     Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*

	MPI API group calls
*/

#include <stdio.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-group.h"
#include "ft-mpi-com.h"
#include "ft-mpi-intercom.h"
#include "debug.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Comm_remote_group    = PMPI_Comm_remote_group
#    pragma weak  MPI_Comm_remote_size     = PMPI_Comm_remote_size
#    pragma weak  MPI_Comm_test_inter      = PMPI_Comm_test_inter
#    pragma weak  MPI_Intercomm_create     = PMPI_Intercomm_create
#    pragma weak  MPI_Intercomm_merge      = PMPI_Intercomm_merge

#endif

#    define  MPI_Comm_remote_group     PMPI_Comm_remote_group
#    define  MPI_Comm_remote_size      PMPI_Comm_remote_size
#    define  MPI_Comm_test_inter       PMPI_Comm_test_inter
#    define  MPI_Intercomm_create      PMPI_Intercomm_create
#    define  MPI_Intercomm_merge       PMPI_Intercomm_merge



/* Some remarks to this implementation of the inter-communicators:
   - this implementation just takes care of the MPI-1 functions
     regarding inter-communicators. All MPI-2 extentions will have
     to be handled later
   - the major goal for this implementation is to keep the changes
     in all real communication routines as small as possible
   EG Jan. 4 2003
*/


int MPI_Comm_remote_group (MPI_Comm comm, MPI_Group *group )
{
  int rc;
  MPI_Group grp;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  rc = ftmpi_com_test_inter ( comm );
  if (!rc )
    RETURNERR (comm, MPI_ERR_COMM );   /* Not an intercomm */
  
  grp = ftmpi_com_get_remote_group ( comm );
  if ( grp < 0 )
    RETURNERR (MPI_COMM_WORLD,  grp );

  rc = ftmpi_group_copy ( grp, group );
  if ( rc < 0 )
    RETURNERR ( comm, rc );

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_remote_size (MPI_Comm comm, int *size )
{
  int rc;
  int sz;

  CHECK_MPIINIT;
  CHECK_COM(comm);

  rc = ftmpi_com_test_inter ( comm );
  if (!rc ) RETURNERR (comm, MPI_ERR_COMM ); /* Not an intercomm */

  sz =ftmpi_com_remote_size ( comm );
  
  *size = sz;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Comm_test_inter ( MPI_Comm comm, int *flag )
{
  int rc;
  
  CHECK_MPIINIT;
  CHECK_COM(comm);

  rc = ftmpi_com_test_inter ( comm );

  *flag = rc;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Intercomm_create (MPI_Comm local_comm, int local_leader, 
			  MPI_Comm peer_comm, int remote_leader, 
			  int tag, MPI_Comm *newintercomm)
{
  int ret;
  int rc;
  int my_rank;
  int nc;
  /*  int  mygid; */
  MPI_Group org_grp, new_grp, r_grp;
  group_t *r_group_ptr;
  int lleader_gid, rleader_gid;
  int size;


  /*----------------------------------------------------------------------*/  
  /* Step 1: make sure, our input parameters are correct. This means:
     - local_comm is valid and an intra-comm
     - local_leader is a valid id in local_comm
     - peer_comm is valid and an intra-comm
     - remote_leader is a vlid id in peer_comm
  */

  CHECK_MPIINIT;
  CHECK_COM(local_comm);
  CHECK_FT(local_comm);

  size = ftmpi_com_size(local_comm);

  if ( (local_leader < 0 ) || (local_leader >=size)) 
    RETURNERR (local_comm, MPI_ERR_RANK);




  rc = ftmpi_com_test_inter ( local_comm );
  if ( rc ) 
    {
      printf("MPI_Intercomm_create: local_comm must not be an inter-"
	     "communicator ! \n");
      RETURNERR (local_comm, MPI_ERR_COMM );
    }

  /* This was a nasty bug to hunt down: since the peer communicator
     just hast to be valid at the local_leaders, the check for the peer_comm
     can cause an error on other, not participating processes.
     EG Feb. 13 2003 */

  my_rank = ftmpi_com_rank ( local_comm );
  if ( my_rank == local_leader )
    {
      CHECK_COM(peer_comm);
      size = ftmpi_com_size (peer_comm);

      rc = ftmpi_com_test_inter ( peer_comm );
      if ( rc ) 
	{
	  printf("MPI_Intercomm_create: peer_comm must not be an inter-"
		 "communicator ! \n");
	  RETURNERR (peer_comm, MPI_ERR_COMM);
	}

      if ( (remote_leader < 0 )||(remote_leader >=size))
	RETURNERR( local_comm, MPI_ERR_RANK );

    }

  /*----------------------------------------------------------------------*/  
  /* Step 2: the local_leaders exchange information about their groups */
  if ( my_rank == local_leader )
    {
      /* Hey, that's me! */
      r_group_ptr = ftmpi_com_exchange_groups (local_comm, peer_comm,
					       remote_leader,tag);
      rleader_gid = ftmpi_com_gid ( peer_comm, remote_leader );

    }
  else
    {
      /* just malloc memory for the group_structure */
      r_group_ptr = (group_t *)_MALLOC( sizeof (group_t));
      if ( r_group_ptr == NULL )
	RETURNERR (local_comm, MPI_ERR_INTERN );
    }

  /*----------------------------------------------------------------------*/  
  /* Step 3: dsitribute the information about the remote_group in the
     local communicator */
  ret = ftmpi_com_bcast_group ( r_group_ptr, local_comm, local_leader, 
				&remote_leader, &rleader_gid );

  /*----------------------------------------------------------------------*/  
  /* Step 4: create the new communicator and set the information about
     the remote_group */

  /* Two notes: 
     - here somewhere we have to execute the sys-recovery loop later
     - the id achieved by com_lib_next_com is *not* unique over
       both subgroups of the inter-communicator! Thats why we
       have a slightly modified version called com_lib_next_intercom
  */

  nc = ftmpi_com_lib_next_com ( FT_INTRA_INTER, local_leader, local_comm, 
				peer_comm, remote_leader, tag );
  rc = ftmpi_com_copy (local_comm, &nc);
  if (rc<0) RETURNERR (local_comm,rc);  

  /* we need to also update the com to group relationships... */
  org_grp = ftmpi_com_get_group (local_comm);	/* get original group */

  /* Set the local group, which is a copy of the group from local_comm */
  rc = ftmpi_group_copy (org_grp, &new_grp);
  if (rc<0) RETURNERR (local_comm,rc);
  
  rc = ftmpi_com_set_group (nc, new_grp);
  if (rc<0) RETURNERR (local_comm, rc);
  
  /* Set the remote group info */
  rc = ftmpi_group_get_free (&r_grp);
  if (rc<0) RETURNERR (local_comm,rc);

  /* build group */
  /*  mygid = ftmpi_com_my_gid (); */

  lleader_gid = ftmpi_com_gid ( local_comm, local_leader );
  /* The arguments in the next line are ugly! reiterate please! */
  rc = ftmpi_group_build (r_grp, MPI_UNDEFINED, 
			  r_group_ptr->gids, 
			  r_group_ptr->currentsize, 
			  r_group_ptr->currentsize);

  ret = ftmpi_com_build_intercom ( /* commid */        nc ,
				   /* local_comm */    local_comm,
				   /* remote_group */  r_grp,
				   /* local_leader */  local_leader,
				   /* lleader_gid */   lleader_gid,
				   /* remote_leader */ remote_leader,
				   /* rleader_gid */   rleader_gid );

  if ( ret != MPI_SUCCESS )
    RETURNERR ( local_comm, ret );

  /* Copy error handler from the local intercomm 
     (Clarification of MPI-1.2, which of the two 
     available communicators has to be used )
  */
  ftmpi_com_copy_errhandler ( local_comm, nc );

  /* Create shadow communicator
     For inter-communicators take nc+3 (instead of nc+1)
     since nc+1 is already the copy of the local_comm */
  rc = ftmpi_com_create_shadow ( nc, nc+3);
  if ( rc < 0 ) RETURNERR ( local_comm, rc );

  *newintercomm = nc;
  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Intercomm_merge (MPI_Comm intercomm, int high, MPI_Comm *newcomm)
{
  MPI_Group oldgrp, newgrp, group, r_group;
  MPI_Comm ocomm, nc;
  int ret, numprocs, mygid;
  int rc;
  int r_high;
  int tag, lleader, rleader;
  int size1, size2;
  int lgid, rgid;
  int *tempgids, *t;
  int *grp_gids, *r_grp_gids;
  int *first, *second;
  int firsts, seconds;
  
  CHECK_MPIINIT;
  CHECK_COM(intercomm);
  CHECK_FT(intercomm);
  
  rc = ftmpi_com_test_inter ( intercomm );
  if ( !rc )
    {
      printf("MPI_Intercomm_merge: communicator is not an inter-comm\n");
      RETURNERR (intercomm, MPI_ERR_COMM );
    }
  
  group   = ftmpi_com_get_group ( intercomm );
  r_group = ftmpi_com_get_remote_group ( intercomm );

  grp_gids   = ftmpi_group_get_gids_ptr ( group );
  r_grp_gids = ftmpi_group_get_gids_ptr ( r_group );

  lgid = ftmpi_com_get_lleader_gid ( intercomm );
  rgid = ftmpi_com_get_rleader_gid ( intercomm );

  size1 = ftmpi_group_size ( group );
  size2 = ftmpi_group_size ( r_group );

  /*--------------------------------------------------------------------*/
  /* Step 1: make sure, everybody from the other group has our "high"
     argument */

  ret = ftmpi_com_exchange_high ( intercomm, high, &r_high );
  if ( ret != MPI_SUCCESS ) RETURNERR ( intercomm, ret );

  /*--------------------------------------------------------------------*/
  /* Step 2: determine which group goes first in the new comm  */
  if ( (!high) && ( r_high))
    {
      first   = grp_gids;
      firsts  = size1;
      second  = r_grp_gids;
      seconds = size2;
    }
  else if ( (high) && (!r_high ) )
    {
      first   = r_grp_gids;
      firsts  = size2;
      second  = grp_gids;
      seconds = size1;
    }
  else 
    {
      /* User didn't tell us which group should go first in the 
	 new comm. We decide it ourselves */
      if ( lgid < rgid )
	{
	  first   = grp_gids;
	  firsts  = size1;
	  second  = r_grp_gids;
	  seconds = size2;
	}
      else
	{
	  first   = r_grp_gids;
	  firsts  = size2;
	  second  = grp_gids;
	  seconds = size1;
	}
    }

  /*--------------------------------------------------------------------*/
  /* Step 3: set up the gid-list */
  numprocs = size1 + size2;
  tempgids = (int *)_MALLOC( numprocs * sizeof ( int ));
  if (tempgids == NULL)
    RETURNERR (intercomm, MPI_ERR_INTERN );

  t = tempgids;
  memcpy ( t, first, firsts * sizeof(int) );
  t += firsts;
  memcpy ( t, second, seconds * sizeof(int) );

  /*--------------------------------------------------------------------*/
  /* Step 4: create the new communicator */
  ocomm   = ftmpi_com_get_local_comm ( intercomm );
  lleader = ftmpi_com_get_lleader ( intercomm );
  rleader = ftmpi_com_map_rank ( intercomm, rgid );
  tag = 528; /* should set to a later easy to recognize value */

  nc = ftmpi_com_lib_next_com (FT_INTER_INTRA, lleader, ocomm,
			       intercomm, rleader, tag);

  if ( nc < 0 ) RETURNERR (intercomm, nc );

  rc = ftmpi_com_copy_subset ( intercomm, &nc, tempgids, numprocs, numprocs );
  if ( rc < 0 ) RETURNERR (intercomm, rc );

  oldgrp = ftmpi_com_get_group ( intercomm );
  rc = ftmpi_group_copy ( oldgrp, &newgrp );
  if ( rc < 0 ) RETURNERR (intercomm, rc );

  mygid = ftmpi_com_my_gid ();
  rc = ftmpi_group_build ( newgrp, mygid, tempgids, numprocs, numprocs );

  _FREE( tempgids );

  /* Attach group to communicator */
  rc = ftmpi_com_set_group (nc, newgrp);
  if (rc<0) RETURNERR (intercomm, rc);

  /* Copy error handler */
  ftmpi_com_copy_errhandler ( intercomm, nc );
  
  /* Create shadow communicator */
  rc = ftmpi_com_create_shadow ( nc, nc+1);
  if ( rc < 0 ) RETURNERR ( intercomm, rc );

  *newcomm = nc;
  return ( MPI_SUCCESS );
}




