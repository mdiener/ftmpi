
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_GRAPH 
#define _FT_MPI_H_GRAPH

graph_info_t* ftmpi_graph_set ( int nnodes, int *index, int *edges );

void ftmpi_graph_free (graph_info_t *graph);

graph_info_t* ftmpi_graph_copy (graph_info_t *graph); 

int ftmpi_graph_compare (graph_info_t *graph1, graph_info_t *graph2);

int ftmpi_graph_get_nnodes ( graph_info_t *graph );

int ftmpi_graph_get_nedges ( graph_info_t *graph );

int* ftmpi_graph_get_index_ptr ( graph_info_t *graph );

int* ftmpi_graph_get_edges_ptr ( graph_info_t *graph );

int ftmpi_graph_calc_neighbors_count ( graph_info_t *graph, int rank);

int ftmpi_graph_calc_neighbors ( int* neighbors, int maxneighbors, int rank, graph_info_t *graph);


#endif /*  _FT_MPI_H_GRAPH  */

