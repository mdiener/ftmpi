
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef __FT_MPI_SYS_H__
#define __FT_MPI_SYS_H__

#define MAXPERAPP 512	/* currently 512 processes per application ? */
#define MAXCLUSTERS 10	/* allows you to be in 10 clusters at once */
#define MAXCOMS 2100		/* used until I get the ll stuff from NG */
#define MAXTREEFANOUT 10 /* tree fan out used for internal bcasts */

#include "pthread.h"




typedef struct {
  int nnodes;
  int ndims;
  int * dims;
  int * periods;
} cart_info_t;

typedef struct {
  int nnodes;
  int nedges;
  int *index;
  int *edges;
} graph_info_t;

typedef struct {
	MPI_Comm comm;
	MPI_Comm derived_from;
	MPI_Group group;
	int freed;			/* we may have been freed but still have pending coms */
	int nbref;			/* non-blocking coms ref count, when 0 can be freed */
	int maxsize;		/* extent of datastructure */
	int currentsize;	/* size we report back to the MPI API */
	int nprocs;		/* How many procs are alive in there */
	int my_rank;
	int my_gid;
	int ft_com_mode;
	int ft_msg_mode;
	int ft_com_state;
	int ft_epoch;

	/* operation counters for collective operations */
	int bar_cnt;	/* Barriers */
	int coll_cnt;	/* bcast/reduces etc */


	/* Data on each member */
/* 	int proc_gids[MAXPERAPP]; */
/* 	int proc_ft_states[MAXPERAPP]; */
	int gids[MAXPERAPP];
	int conn_entry[MAXPERAPP];

	/* Recv message list stuff */
	struct msg_list *msg_list_head;
	long    msg_list_count;

	/* non-blocking operations stuff */
	/* queues are now common to call communicators */

	/* send ops */
/* 	struct req_list *req_list_send_head; */
/* 	long   req_list_send_count; */

	/* recv ops */
/* 	struct req_list *req_list_recv_head; */
/* 	long   req_list_recv_count; */

	/* completed ops */
/* 	struct req_list *req_list_done_head; */
/* 	long   req_list_done_count; */

	/* simple low level topology info */
	/* used during collective operations etc */
	int tree_prev;
	int tree_next[MAXTREEFANOUT];
	int tree_nextsize;
	int bmtree_root;
	int bmtree_prev;
	int bmtree_next[MAXTREEFANOUT];
	int bmtree_nextsize;
	int ring_prev;
	int ring_next;

	/* Locking information */
	pthread_mutex_t comm_info_lock;
	int changed_since_last_lock;

  /* Information for the topology-functions */
  int topo_type;   /* MPI_UNDEFINED, MPI_CART, MPI_GRAPH */
  cart_info_t  * cart_ptr;
  graph_info_t * graph_ptr;

  /* Information for inter-communicators */
  MPI_Comm   local_comm;
  MPI_Group  remote_group;
  int        inter;         /* 0: intra-comm   1: inter-comm */
  int        lleader;
  int        rleader;
  int        lleader_gid;
  int        rleader_gid;

  /* Information for attribute caching */
  int        nattr; /* number of attributes attached to this communicator */
  int      maxattr; /* maximum number of attributes (current lenght of the array) */
  int       *attrs; /* array containing the list of */

  /* Information for error-handling */
  int                    errhandler; /* handle of the attached error handler */
  MPI_Comm_errhandler_fn     *errfn; /* pointer to the errhandler function */

  /* Information for shadow communicator (for collective operations) */
  MPI_Comm  shadow;
  int       am_shadow;  /* 0 : no; 1 = yes; */
  MPI_Comm  parent;     /* if shadow, this is my parent */


} comm_info_t;


#define FTMPI_COMM_WORLD_SHADOW 2
#define FTMPI_COMM_SELF_SHADOW  3

#ifdef DEFUNCT
typedef struct {
	int inuse; /* entry free? */

	int gid; /* of the other end */

	/* TCP socket comms info */
	unsigned long addr;
	int ports[2];	/* their ports, accept, state */
						/* we only use the first */
	int sock; /* socket */
	int connected;

	/* connect stats */
	int send_msgcnt;
	int recv_msgcnt;

	/* connection states */
	int sendallowed;
	int initialack;
	int error;

	/* basic flow control */
	int sentxon;
	int sentxoff;

	/* non-blocking flow control info */
	int last_acked_msg_id;					/* this ID is piggy backed */
	int data_sent_since_last_ack;			/* how much NB data have we sent */
	int send_nb_limit;						/* how much NB data can we send */

	/* nono-blocking temp storage info */
	/* this info is returned to the sender by the recver on the next send */
	int last_recvd_msg_id;					/* non 0 if not yet ackd */
	int	last_recvd_msg_len;					/* how big was it */

} conn_info_t;
#endif

#ifdef mthreadreq
typedef struct {
	int req_number;
	int req_type;
	pthread_mutex_t req_start_lock;
	pthread_cond_t req_start_cv;
	pthread_mutex_t req_done_lock;
	pthread_cond_t req_done_cv;
	int req_rc;
	int req_input[5];
	int req_output[5];
} req_t;
#endif /* mthreadreq */



typedef enum {
	USR_P2P,
	USR_COLL,
	USR_BAR,
	SYS_PROC_FAIL,
	SYS_NEW_COM,
	SYS_BAR
} msg_types_t;

/* Message tags */
#define SYS_MSG_TAG -9	/* used by system messages */
#define USR_MSG_TAG -10 /* used by user collective messages */
						/* Note this tags are not used to sort messages */
						/* We have to fill the msg header and these help */
						/* with debugging */

/* State message tags */
#define FTMPI_SYS_ACK	8001
#define FTMPI_SYS_SACK	8002
#define FTMPI_SYS_NACK	8003
#define FTMPI_SYS_ABORT	9009			/* abort message */
						
/* abort is listed here, so that an MPI abort can be trapped even within a */
/* recovery operation. If it was not trapped here then an abort would be */
/* missed and the aborting task could be restarted accidently */ 

/* currently we only send abort messages between tasks started with MPI_Spawn */
/* i.e. all within MCW should get the abort via the state records */

/* connection function return types */
#define FTMPI_CONN_FAULT			-1
#define FTMPI_CONN_NO_MPI_HEADER	-900

typedef enum {
	CTRL_REQ_QUIT,
	CTRL_REQ_ACCEPT,
	CTRL_REQ_CONNECT
} CTRL_REQ_TYPES;



typedef enum {
	SEND_REQ_SYS_MSG,
	SEND_REQ_SYS_NB_MSG,
	SEND_REQ_CANCEL_SYS_MSG,
	SEND_REQ_USER_MSG,
	SEND_REQ_USER_NB_MSG,
	SEND_REQ_CANCEL_USER_MSG,
	SEND_REQ_MSG_STATUS
} SEND_REQ_TYPES;



/* Not sure if these will stay like this... :) */
/* As recv thread just ermm... recvs and buffers */
/* or rejects and then lets sender send... */

typedef enum {
	RECV_REQ_SYS_MSG,
	RECV_REQ_SYS_NB_MSG,
	RECV_REQ_CANCEL_SYS_MSG,
	RECV_REQ_USER_MSG,
	RECV_REQ_USER_NB_MSG,
	RECV_REQ_CANCEL_USER_MSG,
	RECV_REQ_MSG_STATUS
} RECV_REQ_TYPES;


/* startup defaults */

/* we can have gextent -1 exit messages before we know the leader has failed */
#define FTMPI_WAITFOR_LEADER_LOOPS	(gextent-1)

/* we delay the nodes pushing the NS when starting larger jobs */
/* without this they hammer the NS waiting for the leader record etc */
/* default is 25ms per node and it staggers them based on rank */
/* the value will need changing if your on fast interface and faster node */
/* NOTE value is in uSec not milliSecs */
#define FTMPI_WAITFOR_LEADER_STARTUP_DELAY	(25000)


/* these are the modes for the function ft-mpi-lib-next-com 
   FT_INTRA_INTRA: used in Comm_create, etc. when an intra communicator
                is created from another intra-communicator.
   FT_INTRA_INTER: used in Intercomm_creat, when an inter-communicator
                is created from two intra-communicators.
   FT_INTER_INTRA: used in Intercomm_merge, when the groups of an inter-
                communicator are merged to an intra-communicator.
   FT_INTER_INTER: used when dup-ing an intercommunicator

*/
#define  FT_INTRA_INTRA  0
#define  FT_INTRA_INTER  1
#define  FT_INTER_INTRA  2
#define  FT_INTER_INTER  3

/* Prototypes */
int ftmpi_sys_init (int *argc, char **argv[]);
int ftmpi_sys_init_startup_state (int tout);
void ftmpi_sys_start_mpi ();  
int clearup_ns_records (int tgid);
int ftmpi_sys_get_runid ();
int ftmpi_sys_get_gid ();
int ftmpi_sys_get_grank ();
int ftmpi_sys_get_leader ();
int ftmpi_sys_getnextcomid ();
int ftmpi_sys_waitfor_new_state (int *newstate, int oldstate, int leader);
int ftmpi_sys_get_state (int *state, int *lleader, int *lepoch, 
			 int *lnproc, int* lextent, int** lpp,
			 int updatecinfo, int loops, int checkerr);
int ftmpi_sys_wait_state (int *state, int *lleader, int *lepoch, 
			  int *lnproc, int* lextent, int** lpp,
			  int updatecinfo, int loops, int checkerr);
int ftmpi_sys_get_conninfo (int tgid, int *values,  int loops, int checkerr);
int ftmpi_sys_waitfor_ack (int tid, int tepoch, int *val);
int  ftmpi_sys_check_for_ack (int *ids, int *caddr, int *cport, 
			      int *sport, int *convs, int *vals, int max, int tepoch);
void ftmpi_sys_check_for_mpi_abort ();
void ftmpi_sys_leave (int, int);

int ftmpi_sys_check_for_events (int *ids, int max);
int ftmpi_sys_elect_new_leader();
int ftmpi_sys_spawn ();
int ftmpi_sys_post_state (int newstate, int *ids, int ext);
int ftmpi_sys_send_leader_ack (int epoch);
int ftmpi_sys_send_gid_ack (int tid, int epoch, int val);
int ftmpi_sys_leader_waitfor_acks (int *glp, int *rlp, int s, int n, int allowdead, int updatecinfo);
int ftmpi_sys_ring_loop (int *lp, int s, int tepoch);
int ftmpi_any_in_list_died (int *lp, int s);
int ftmpi_sys_new_leader_first_time_only();
int ftmpi_sys_get_new_leader_first_time_only (int loops, int offset);
int ftmpi_sys_reorder_ids (int *new, int *old, int extent);
int ftmpi_sys_pack_ids ( int extent );
int ftmpi_sys_recovery_loop ();
int ftmpi_sys_recovery_leader_loop (int new);
int ftmpi_sys_recovery_peon_loop ();
int ftmpi_sys_find_monitor ();
int ftmpi_sys_monitor_post_mcw (int firsttime);
int ftmpi_sys_create_failedlist ( int osize, int ogids[], int nsize, int ngids[] );

#endif /* __FT_MPI_SYS_H__ */
