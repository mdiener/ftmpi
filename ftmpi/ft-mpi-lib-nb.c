/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			George Bosilca <bosilca@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/
/* #include <assert.h> */

#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-ddt-sys.h"

#include "ft-mpi-req-list.h"

#include "ft-mpi-lib-nb.h"
#include "ft-mpi-lib-p2p.h"

#include "ft-mpi-nb.h"
#include "ft-mpi-p2p.h"

#include "ft-mpi-com.h"
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"

/* #include "status.macro" */


/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Cancel               = PMPI_Cancel
#    pragma weak  MPI_Isend                = PMPI_Isend
#    pragma weak  MPI_Irsend               = PMPI_Irsend
#    pragma weak  MPI_Issend               = PMPI_Issend
#    pragma weak  MPI_Irecv                = PMPI_Irecv 
#    pragma weak  MPI_Request_free         = PMPI_Request_free
#    pragma weak  MPI_Test                 = PMPI_Test
#    pragma weak  MPI_Testall              = PMPI_Testall
#    pragma weak  MPI_Testany              = PMPI_Testany
#    pragma weak  MPI_Testsome             = PMPI_Testsome
#    pragma weak  MPI_Test_cancelled       = PMPI_Test_cancelled
#    pragma weak  MPI_Wait                 = PMPI_Wait
#    pragma weak  MPI_Waitall              = PMPI_Waitall
#    pragma weak  MPI_Waitany              = PMPI_Waitany
#    pragma weak  MPI_Waitsome             = PMPI_Waitsome

#endif

#    define  MPI_Cancel                PMPI_Cancel
#    define  MPI_Isend                 PMPI_Isend
#    define  MPI_Irsend                PMPI_Irsend
#    define  MPI_Issend                PMPI_Issend
#    define  MPI_Irecv                 PMPI_Irecv 
#    define  MPI_Request_free          PMPI_Request_free
#    define  MPI_Test                  PMPI_Test
#    define  MPI_Testall               PMPI_Testall
#    define  MPI_Testany               PMPI_Testany
#    define  MPI_Testsome              PMPI_Testsome
#    define  MPI_Test_cancelled        PMPI_Test_cancelled
#    define  MPI_Wait                  PMPI_Wait
#    define  MPI_Waitall               PMPI_Waitall
#    define  MPI_Waitany               PMPI_Waitany
#    define  MPI_Waitsome              PMPI_Waitsome





/* ********************************************************************* */
/*                            basic send-recv operations                 */
/* ********************************************************************* */

int MPI_Irecv(void* buf,int count,MPI_Datatype datatype,int source,int tag,
	      MPI_Comm comm,MPI_Request *request)
{
  int es, rc, size;

  CHECK_MPIINIT
  CHECK_COM (comm);
  GET_PPT_SIZE(comm, size);

  CHECK_REQ_P (comm, request);
  CHECK_TAG_R (comm, tag);
  CHECK_DDT(comm,datatype,1);

  /* lets start with the request set to MPI_REQUEST_NULL */
  *request = (MPI_Request)MPI_REQUEST_NULL;

  if ( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );
  if( source == MPI_PROC_NULL ) return ftmpi_nb_handle_proc_null( comm, request );
  if ( (source < 0) && (source!=MPI_ANY_SOURCE) ) RETURNERR (comm, MPI_ERR_RANK);
  if ( source >= size ) RETURNERR (comm, MPI_ERR_RANK );

  /* check for failure */
  es = ft_mpi_conn_any_error ();
  if (es) RETURNERR (comm,MPI_ERR_OTHER);

  rc = ftmpi_mpi_irecv(buf,count,datatype,source,tag,comm,request);


  if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
  RETURNERR (comm, rc);
}

int MPI_Isend(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  int es;
  int rc;
  int size;
 
  CHECK_MPIINIT
  CHECK_COM(comm);
  GET_PPT_SIZE(comm,size);

  CHECK_REQ_P(comm,request);
  CHECK_TAG_S(comm,tag);

  CHECK_DDT(comm,datatype,1);

  /* lets start with the request set to MPI_REQUEST_NULL */
  *request = (MPI_Request)MPI_REQUEST_NULL;

  if( count < 0 ) RETURNERR (comm, MPI_ERR_COUNT );
  if( dest == MPI_PROC_NULL ) return ftmpi_nb_handle_proc_null( comm, request );
  if( (dest < 0) || (dest >= size) ) RETURNERR (comm, MPI_ERR_RANK);

  /* check for failure */
  es = ft_mpi_conn_any_error ();
  if (es) {
    *request = MPI_REQUEST_NULL;
    RETURNERR (comm,MPI_ERR_OTHER);
  }
  
  /* ok do the actual isend */
  rc = ftmpi_mpi_isend (buf,count,datatype,dest,tag,comm,request);
  
  
  if (rc==0) return (MPI_SUCCESS);
  RETURNERR (comm, rc)
}

/* These functions do the MPI_I ops without additional error checking */
/* they can be used by internal error functions as they DO not call the */
/* error handlers but instead return errors to the above layer */

int ftmpi_mpi_irecv (void* buf,int count,MPI_Datatype datatype,int source,
		     int tag,MPI_Comm comm,MPI_Request *request)
{
  int ret;
  MPI_Request req;
  int match;
  int sgid;

  *request = MPI_REQUEST_NULL;

  /* check for failure */
  CHECK_FT_NOE;

  /* handle dead procs for the blank mode */
  if ( source != MPI_ANY_SOURCE ) {
    sgid = ftmpi_com_gid ( comm, source );
    if ( sgid == MPI_PROC_NULL )
      source = MPI_PROC_NULL;
  }
  
  
  /* NB stuff */
  /* First find free req handle */
  req = ftmpi_nb_get_freehandle ();

  
  if (req<0) {
    *request = MPI_REQUEST_NULL;
    return (MPI_ERR_INTERN);
  }
  
  *request = req;	/* set request value */
  
  ret = ftmpi_nb_set_req (buf,count, datatype, source, tag, comm, REQ_IRECV, req);
  if (ret<0) return (ret);
  
  /* quick check to see if it is a proc_null op */
  if (source==MPI_PROC_NULL) { /* it is do mark it done */
    ret = ftmpi_nb_req_add2_done_list (req, 1);
    return (ret);
  }
  
  /* at this point update the DDT ref count */
  /* we inc refcnt when creating a request always */
  /* the count is decd when we free the req up */
  /* unless it was MPI_PROC_NULL request */
  ftmpi_ddt_increment_dt_uses (datatype);
  
  
  /* if a match it does the recv, and moves the req to the completed/done list */ 
  match =  ftmpi_nb_check_msglist_for_matching_req_and_get (req);
  /* match =  0 ;  */
  
  if (!match) { /* we just put it on the queue */
    ret = ftmpi_nb_req_add2_recv_list (req, 0);
    /* do not update ref count as this is already done by the set_req op */
	if (ret<0) return (ret);

	/* but we can now check for an incomplete unexp msg matching */
	/* we will then get removed from the recv queue so that */
	/* a second unexp msg will not match us again... */
	/* the unexpected msg will keep track of us and move us to done when */
	/* it completes */

	ret = ftmpi_nb_check_incomplete_msglist_for_matching_req_mark_unlink (req);

    return (ret);
  }

  if (match<0) return (match);
  return (ret);	/* this must be here in case we do match */
}


int ftmpi_mpi_isend(void* buf,int count,MPI_Datatype datatype,int dest,
	  				int tag,MPI_Comm comm,MPI_Request *request)
{
  int ret;
  MPI_Request req;
  int self;
  int dgid;
  int tmp;

#ifdef SEND_VERBOSE
  /*   printf("Send from rank [%d] to [%d] com mode = %d stat %d remote stat %d\n", coms[0].my_rank,
       dest, comm_mode, comm_state, remote_state); */
#endif
  
  *request = MPI_REQUEST_NULL;
  
  /* check for failure */
  CHECK_FT_NOE;
  
  /* handle dead procs for the blank mode */
  dgid = ftmpi_com_gid ( comm, dest );
  if ( dgid == MPI_PROC_NULL )
    dest = MPI_PROC_NULL;

  
  /* NB stuff */
  
  /* First find free req handle */
  req = ftmpi_nb_get_freehandle ();
  
  if (req<0) {
    *request = MPI_REQUEST_NULL;
    return (MPI_ERR_INTERN);
  }
  
  *request = req;	/* set request value */
  
  /* at this point update the DDT ref count */
  ftmpi_ddt_increment_dt_uses (datatype);
  
  ret = ftmpi_nb_add2_send_list (buf, count, datatype, dest, tag, comm, req);
  
  /* ok its now a fully build request, here we can check for sends to self */
  /* or proc_null or very short immediate - eager sends */
  
  self = ftmpi_nb_send2_self_test_and_do (req);
  /* printf("self = %d\n", self); */
  if( self != 0 ) {
    if( self > 0 ) return (ret);	/* sent to self (matched nb or unexpected list) */
    return (self); /* error so call errorhandler and return */
  }
  /* if self = 0 which means not to self */
 

  ret = ftmpi_nb_post_send (req);

/* printf("Isend on req %d returns %d at post\n", req, ret); */

  ftmpi_nb_progress (0);

  tmp = ftmpi_nb_req_isdone (req);

/* printf("Isend on req %d after post isdone 0x%x\n", req, tmp); */

  return (ret);
}


/* these are currently just wrappers used to get this off the ground */
/* GEF */

int MPI_Issend(void* buf,int count,MPI_Datatype datatype, int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  int rc;

  rc = MPI_Isend (buf, count, datatype, dest, tag, comm, request);
  return (rc); /* returnerr is handled within the isend call */
}

int MPI_Irsend(void* buf,int count,MPI_Datatype datatype,int dest,int tag,MPI_Comm comm,MPI_Request *request)
{
  int rc;

  rc = MPI_Isend (buf, count, datatype, dest, tag, comm, request);
  return (rc); /* returnerr is handled within the isend call */
}



/* ********************************************************************* */
/*                            TEST operations                            */
/* ********************************************************************* */



int MPI_Test(MPI_Request *request,int *flag, MPI_Status *status)
{
  int rc;
  /* default checks */

  *flag = 0;  /* always set it to ZERO */

  CHECK_MPIINIT
  CHECK_REQ_P (MPI_COMM_WORLD, request);
  CHECK_FLAG_P (MPI_COMM_WORLD, flag);
  CHECK_STATUS_P (MPI_COMM_WORLD, status);
  
  if (*request!=MPI_REQUEST_NULL) {
    /* should check for invalid request handle? */
    rc = ftmpi_nb_req_ok (*request);
    if (!rc) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
  }
  
  rc = ftmpi_mpi_test (request, flag, status);
  
  if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
  else
    RETURNERR (MPI_COMM_WORLD, rc);
} 


/* this is the less checked version of mpi_test */
/* this routine also does not call the errorhandler but passes errors back */
/* so that the lib level PMPI_ or MPI_ call can call the errorhandler */
/* just ONCE */

int ftmpi_mpi_test (MPI_Request *request,int *flag, MPI_Status *status)
{
  MPI_Request req;
  MPI_Status lstatus, *plstatus = &lstatus;
  int rc;
  int type;
  
  req = *request; /* cache request */
  
  *flag = 0;  /* always set it to ZERO */

  if( status!=MPI_STATUS_IGNORE ) plstatus = status;
  /* do the request_null check */
  if (req==MPI_REQUEST_NULL) {
    /* this is a test so return empty status if possible */
    ftmpi_pp_set_emptystatus( plstatus );
    /* as TEST flag is set */
    *flag=1;
    return plstatus->MPI_ERROR;
  }

  /* should check for invalid request handle? */
  rc = ftmpi_nb_req_ok (req);
  if (!rc) return (MPI_ERR_REQUEST);
  
  /* try and find if its done */
  /* check once and then probes and then checks again if needed */
  /* but only if the op is a RECV type op */
  rc = ftmpi_nb_req_isdone(req);

  if( rc == 0 ) { /* not yet done, not cancelled or errored  */
      
      rc = ftmpi_nb_progress (0);	 /* make progress */
      if (rc<0) return rc;

    /* check it again as the above returns for many reasons */
    rc = ftmpi_nb_req_isdone(req);
  }
  
  if (rc==0) { /* if after checking, and progress its still not done */
    return (MPI_SUCCESS);	/* even if not done */
  }

  if (rc<0) return rc;
  
  /* now search the completed list match to this request */
  /* if we find a req we set the status */
  /* we set the '1' so that a match also removes the req is possible */
  
  rc = ftmpi_nb_find_completed (req, plstatus, 1);
  
  if (rc==2) {	/* if found/matched/and deleted */
    ftmpi_nb_free_req_handle (req);	
    *request=MPI_REQUEST_NULL;
    *flag=1;
    return plstatus->MPI_ERROR;
  }
  if (rc==1) { /* found BUT request still in use */
    /* Please, this flag still has to be 1, since the operation is finished,
       but the request not removed (e.g. persistent request) */
    *flag = 1;
    printf("ftmpi_mpi_test: rc = 1 for TAG %d ERROR %d msglength = %d\n",
	   plstatus->MPI_TAG, plstatus->MPI_ERROR, plstatus->msglength);
    return plstatus->MPI_ERROR;
  }
  
  /* else not matched. Check still for an inactive (persistent) request*/
  rc = ftmpi_nb_get_req_refcnt ( req );
  type = ftmpi_nb_get_req_type ( req );
  /* Check for inactive persistent requests. We rely here on refcount
     to be correct ! */
  if( (type & REQ_PERSISTENT_MASK) && (rc == 1) ) {
    ftmpi_pp_set_emptystatus ( status );
    *flag = 1;
    return ( MPI_SUCCESS);
  }
  
  return (MPI_SUCCESS);	/* even if not found */
}

int MPI_Testany(int count,MPI_Request *array_of_requests, int *index,
		int *flag,MPI_Status *status)
{
  int rc;
  int i;
  int invalid_requests=0;
  int type;
  
  CHECK_MPIINIT;
  CHECK_INDEX_P (MPI_COMM_WORLD, index);
  CHECK_FLAG_P (MPI_COMM_WORLD, flag);
  CHECK_STATUS_P (MPI_COMM_WORLD, status);

  if ( count == 0 )
    {
      *index = MPI_UNDEFINED;
      *flag  = 1;
      ftmpi_pp_set_emptystatus ( status );
      
      return ( MPI_SUCCESS );
    }

  if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);

  for( i = 0; i < count; i++) 
    {
      if ( array_of_requests[i] == MPI_REQUEST_NULL )
	{
	  invalid_requests++;
	  continue;
	}
      
      rc = ftmpi_nb_req_ok (array_of_requests[i]);
      if (!rc) 
	{
	  *index = i; /* set the index of the faulty request */
	  /* and initialize the status */
	  if( status != MPI_STATUSES_IGNORE ) {
	    ftmpi_pp_set_emptystatus ( status );
	    status->MPI_ERROR = MPI_ERR_REQUEST;
	  }
	  /* I am not sure whether we really have to return an error... */
	  RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
	}

      rc = ftmpi_nb_get_req_refcnt ( array_of_requests[i] );
      type = ftmpi_nb_get_req_type ( array_of_requests[i]);
      /* Check for inactive persistent requests. We rely here on refcount
	 to be correct ! */
      if( (type & REQ_PERSISTENT_MASK) && (rc == 1) )
	{
	  invalid_requests++;
	  continue;
	}
     
      rc = ftmpi_mpi_test ( &array_of_requests[i], flag, status );
      if ( *flag ) 
	{
	  *index = i;
/* 	  printf("MPI_TestAny status error %d return error %d\n", status->MPI_ERROR, rc ); */
	  return (rc);
	}
    }

  /* This is point is not reached if any of the requests
     has finished with a positiv flag */
  ftmpi_pp_set_emptystatus ( status );
  if ( invalid_requests == count )
    {
      *flag  = 1;
      *index = MPI_UNDEFINED;
    }
  else
    {
      *flag  = 0;
      *index = MPI_UNDEFINED;
    }

  return (MPI_SUCCESS);	
} 

/*  If an error is detected the outcount, array_of_statuses and array_of_indices will be adjusted to
 * reflect the completion of all requests that have completed or failed.
 */
int MPI_Testsome(int incount,MPI_Request *array_of_requests, int *outcount,
		 int *array_of_indices,MPI_Status *array_of_statuses)
{
  int rc;
  int i;
  int invalid_requests=0;
  int lindex = 0;
  int flag=0;
  int type;
  int returnval = MPI_SUCCESS;

  CHECK_MPIINIT;
  if (!outcount) RETURNERR (MPI_COMM_WORLD,MPI_ERR_ARG);

  if ( incount == 0 )
    {
      *outcount = MPI_UNDEFINED;
      return ( MPI_SUCCESS );
    }

  if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
  if (!array_of_statuses) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  if( array_of_statuses == MPI_STATUSES_IGNORE ) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  for( i = 0; i < incount; i++) {
    if ( array_of_requests[i] == MPI_REQUEST_NULL ) {
      invalid_requests++;
      continue;
    }

    rc = ftmpi_nb_req_ok (array_of_requests[i]);
    if (!rc) {
      array_of_statuses[i].MPI_ERROR = rc;
      CALL_ERRHANDLER( MPI_COMM_WORLD, MPI_ERR_REQUEST );
      return MPI_ERR_IN_STATUS;
    }
    
    rc = ftmpi_nb_get_req_refcnt ( array_of_requests[i] );
    type = ftmpi_nb_get_req_type ( array_of_requests[i]);
    /* Check for inactive persistent requests. We rely here on refcount
       to be correct ! */
    if( (type & REQ_PERSISTENT_MASK) && (rc == 1) ) {
	invalid_requests++;
	continue;
    }

    /* always set the status */
    rc = ftmpi_mpi_test ( &array_of_requests[i], &flag, &(array_of_statuses[lindex]) );
    if( flag || (rc != MPI_SUCCESS) ) {
	if( rc != MPI_SUCCESS ) returnval = MPI_ERR_IN_STATUS;
	array_of_indices[lindex]  = i;
	lindex++;
    }
  }

  if ( invalid_requests == incount )
    *outcount = MPI_UNDEFINED;
  else
    *outcount = lindex;
  
  return returnval;
}


/* From the MPI-1.1 standard:
 * flag is set to true if all the requests are completed. If flag == false no request is modified
 * and the values of the status entries are undefined. That allow us to modify the statuses, as the user should
 * not expect any correct value inside.
 */
int MPI_Testall( int count,MPI_Request *array_of_requests,int *flag,
		 MPI_Status *array_of_statuses)
{
    MPI_Request req;
    MPI_Status *plstatus = (MPI_Status*)MPI_STATUS_IGNORE;
    int rc;
    int i;
    int error = MPI_SUCCESS;
    
    CHECK_MPIINIT
    CHECK_FLAG_P (MPI_COMM_WORLD,flag);
    
    
    if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
    if (!array_of_statuses) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

    /* make some progress attempt then check to see if we succeeded */
    rc = ftmpi_nb_progress (0); /* 0 = nonblocking */
    /* should be more carefull on the error handling */
    if (rc<0) return (rc);

    /* let's suppose that all the requests are completed */
    *flag = 1;
    
    /* check if all the requests are completed or NULL */
    for( i = 0; i < count; i++ ) {
	/* should check for invalid request handle? */
	req = array_of_requests[i]; /* cache request */
	if( req == MPI_REQUEST_NULL ) continue;

	rc = ftmpi_nb_req_ok (req);
	if (!rc) {
	    /* detected an error, will fill the status on the second pass */
	    error = MPI_ERR_REQUEST;
	    continue;
	}
	
	rc = ftmpi_nb_req_isdone (req); 
	if (rc == 0) {	/* not yet completed */
	    *flag = 0;
	}
    } /* for each request in the array */
    
    /* if there are uncompleted requests */
    if( *flag == 0 ) {
	if( error == MPI_SUCCESS ) return MPI_SUCCESS;
	RETURNERR( MPI_COMM_WORLD, error );
    }

    /* all the requests are completed we will detect the error on the second pass */
    for( i = 0; i < count; i++ ) {
	
	if( array_of_statuses != MPI_STATUSES_IGNORE )
	    plstatus = &(array_of_statuses[i]);

	/* should check for invalid request handle? */
	req = array_of_requests[i]; /* cache request */
	
	if( req == MPI_REQUEST_NULL ) {
	    if( plstatus != (MPI_Status*)MPI_STATUS_IGNORE )
		ftmpi_pp_set_emptystatus( plstatus );
	    continue;
	}

	rc = ftmpi_nb_req_ok (req);
	if (!rc) {
	    /* detected an error, empty the status and set the error to MPI_ERROR_REQUEST */
	    if( plstatus != (MPI_Status*)MPI_STATUS_IGNORE ) {
		ftmpi_pp_set_emptystatus( plstatus );
		plstatus->MPI_ERROR = MPI_ERR_REQUEST;
	    }
	    continue;
	}
	/* now search the completed list of the com lists for a match to this request */
	/* if we find a req we set the status, indices and inc the cnt etc */
	
	rc = ftmpi_nb_find_completed (req, plstatus, 1);
	
	if (rc==2) {    /* if found/matched/and deleted */
	    ftmpi_nb_free_req_handle (req);
	    array_of_requests[i]=MPI_REQUEST_NULL;
	} else if (rc==1) { /* found BUT request still in use */
	    /* do nothing */
	} else {
	    printf( "%s:%d error on request in MPI_Testall\n", __FILE__, __LINE__ );
	}
    } /* for each request in the array */
    
    return (MPI_SUCCESS);
}

#ifdef OLD_CODE
int MPI_Testall( int count,MPI_Request *array_of_requests,int *flag,
		 MPI_Status *array_of_statuses)
{
    MPI_Request req;
    int rc;
    int i;
    int fcnt=0; /* first count of completed */
    int nulls=0, local_error = MPI_SUCCESS;
    
    CHECK_MPIINIT
    CHECK_FLAG_P (MPI_COMM_WORLD,flag);
    
    
    if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
    if (!array_of_statuses) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

    /* set the initial flag to zero */
    *flag = 0;
    
    /* make some progress attempt then check to see if we succeeded */
    rc = ftmpi_nb_progress (0); /* 0 = nonblocking */
    /* should be more carefull on the error handling */
    if (rc<0) return (rc);

    /* first loop we just count the number of completed ops */
    /* and NULL ops */
    /* we also check for errors here */
    
    for(i=0;i<count;i++) {
	
	/* should check for invalid request handle? */
	req = array_of_requests[i]; /* cache request */
	/* prepare the status */
	if( (int)array_of_statuses != MPI_STATUSES_IGNORE )
	    ftmpi_pp_set_emptystatus( &(array_of_statuses[i]) );
	if(req==MPI_REQUEST_NULL) nulls++;
	else {
	    rc = ftmpi_nb_req_ok (req);
	    if (!rc) {
		/* now set the right information. Now the point here is that on the first
		 * error we stop filling the statuses ... *TODO*
		 */
		if( (int)array_of_statuses != MPI_STATUSES_IGNORE ) {
		    array_of_statuses[i].MPI_ERROR = MPI_ERR_REQUEST;
		    RETURNERR (MPI_COMM_WORLD, MPI_ERR_IN_STATUS);
		} else
		    RETURNERR( MPI_COMM_WORLD, MPI_ERR_REQUEST );
	    }
	    
	    /* now search the completed list of the com lists for a match to this request */
	    /* if we find a req JUST increment the count */
	    
	    /* note the procnull ops are marked done in irecv/isend */
	    rc = ftmpi_nb_req_isdone (req); 
	    if (rc>=1) {	/* if found/matched */
		fcnt++;
	    }
	} /* if not a request_null but a real request */
	
    } /* for each request in the array */
    
    /* if we don't have all the tests completed but have no other errors */
    /* its a success, but we set flag to false (0) */
    
    if ((fcnt+nulls)!=count) {
	*flag=0;
	return (MPI_SUCCESS);
    }
    
    /* ok, we do the loop again, this time we actually update any status handles */
    /* free requests etc etc */
    
    for(i=0;i<count;i++) {
	
	/* should check for invalid request handle? */
	req = array_of_requests[i]; /* cache request */
	
	if( req == MPI_REQUEST_NULL ) continue;
	
	/* less checks on the dt stuctures */
	
	/* now search the completed list of the com lists for a match to this request */
	/* if we find a req we set the status, indices and inc the cnt etc */
	
	if ((int)array_of_statuses != MPI_STATUSES_IGNORE) 
	    rc = ftmpi_nb_find_completed (req, &array_of_statuses[i], 1);
	else
	    rc = ftmpi_nb_find_completed (req, (MPI_Status *)MPI_STATUS_IGNORE, 1);
	
	if (rc==2) {    /* if found/matched/and deleted */
	    ftmpi_nb_free_req_handle (req);
	    array_of_requests[i]=MPI_REQUEST_NULL;
	}
	if (rc==1) { /* found BUT request still in use */
	    /* do nothing */
	}
	
    } /* for each request in the array */
    
    *flag=1;
    return (MPI_SUCCESS);
}
#endif  /* OLD_CODE */

/* ********************************************************************* */
/*                          Cancel operations                            */
/* ********************************************************************* */


int MPI_Cancel(MPI_Request *request)
{
    MPI_Request req;
    int rc;
    int type; 

    CHECK_MPIINIT
    CHECK_REQ_P (MPI_COMM_WORLD, request);
    
    req = *request;

    rc = ftmpi_nb_req_ok (req);
    if (!rc) {
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
    }
    
    /* Check for inactive requests */ 
    type = ftmpi_nb_get_req_type ( req );
    if ( type < 0 ) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
    
    if( type & REQ_PERSISTENT_MASK ) {
	/* TODO remove them */
	/*     if ( (type==REQ_PRECV) || (type==REQ_PSEND) || (type==REQ_PSSEND) || */
	/* 	 (type==REQ_PRSEND) || (type==REQ_PBSEND)) { */
	/* Has this request already bin used in Start without a Wait ? */
	rc = ftmpi_nb_get_req_refcnt ( req );
	if ( rc <= 1 ) RETURNERR ( MPI_COMM_WORLD, MPI_ERR_REQUEST);
    }
    
    rc = ftmpi_nb_req_cancel (req);
    
    
    if (rc<0) 
	RETURNERR (MPI_COMM_WORLD, rc);
    
    return (rc);
}



int MPI_Test_cancelled(MPI_Status * status,int *flag)
{
  CHECK_MPIINIT
  CHECK_FLAG_P (MPI_COMM_WORLD, flag);

  /* check status ptr */
  if (status) { /* assume its ok */
    *flag = (status->flags & FTMPI_REQ_CANCELLED ? 1 : 0);
  }

  return (MPI_SUCCESS);
}


/* ********************************************************************* */
/*                           Request operations                          */
/* ********************************************************************* */


int MPI_Request_free(MPI_Request *request)
{
MPI_Request req;
int rc;

CHECK_MPIINIT
CHECK_REQ_P (MPI_COMM_WORLD, request);

req = *request; /* cache request */
rc = ftmpi_nb_req_ok (req);
if (!rc) {
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST)
	}

/* ok if the request is on the completed list then we can actually free it */

rc = ftmpi_nb_req_decref (req);	/* attempt to free it */

/* a rc = 0 means we just decremented, 1 = we actually freed it then */

/* if the request is ok, then we can mark it as freed */

/* to be finished after checking persistant NB tests? */
/* nope, if we call free we force the handle to be lost */
if (rc>=0) {
	*request=MPI_REQUEST_NULL;
	ftmpi_nb_free_req_handle (req);	
	return (MPI_SUCCESS);
	}
else
	RETURNERR (MPI_COMM_WORLD, rc) /* if a problem return error code */
}



int MPI_Request_get_status (MPI_Request request, int *flag, MPI_Status *status)
{
MPI_Request req;
int rc;

CHECK_MPIINIT
CHECK_FLAG_P (MPI_COMM_WORLD, flag);
CHECK_STATUS_P (MPI_COMM_WORLD, status);

/* should check for invalid request handle? */
req = request; /* cache request, note its not a pointer */
rc = ftmpi_nb_req_ok (req);
if (!rc) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST)

/* now search the completed list of the com lists for a match to this request */
/* if we find a req we set the status */

rc = ftmpi_nb_find_completed (req, status, 0);	/* NOTE the 0 !!! */

if (rc==1) {	/* if found/matched */
	/* we do not reset the request to null, just set the flag */
	*flag=1;
	return (MPI_SUCCESS);
}
/* else not matched */
*flag=0;
return (MPI_SUCCESS);	/* even if not found */
} 




/* ********************************************************************* */
/*                            WAIT operations                            */
/* ********************************************************************* */

int MPI_Wait(MPI_Request *request,MPI_Status *status)
{
    int rc;

    /* default checks */

    CHECK_MPIINIT
    CHECK_REQ_P (MPI_COMM_WORLD, request);
    CHECK_STATUS_P (MPI_COMM_WORLD, status);

    if (*request!=MPI_REQUEST_NULL) {
	rc = ftmpi_nb_req_ok (*request);
	if (!rc) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST)
		     }
    
    rc = ftmpi_mpi_wait (request, status);
    
    if (rc==MPI_SUCCESS) return (MPI_SUCCESS);
    RETURNERR (MPI_COMM_WORLD, rc);
}


/* real wait routine which does limited error checking */
/* also returns error to above layer without calling the errorhandler */
/* routines (above layer should do that */

int ftmpi_mpi_wait (MPI_Request *request,MPI_Status *status)
{
    MPI_Request req;
    MPI_Status lstatus, *plstatus = &lstatus;
    int rc;
    int type=0;
    
    /* should check for invalid request handle? */
    req = *request; /* cache request */
    
    /* do the request_null check */
    if (req==MPI_REQUEST_NULL) {
	/* this is a WAIT so return empty status if possible */
	ftmpi_pp_set_emptystatus(status);
	/* as WAIT return */
	return (MPI_SUCCESS);
    }
    
    if( status != MPI_STATUS_IGNORE ) plstatus = status;
    
    rc = ftmpi_nb_req_ok (req);
    if (!rc) {
	ftmpi_pp_set_emptystatus( plstatus );
	plstatus->MPI_ERROR = MPI_ERR_REQUEST;
	return plstatus->MPI_ERROR;
    }
    
    /* now search the completed list of the com lists for a match to this request */
    /* if we find a req we set the status */
    /* we could just its done value.. */
    
    rc = ftmpi_nb_find_completed (req, plstatus, 1);
    
    if (rc==2) {    /* if found/matched/and deleted */
	ftmpi_nb_free_req_handle (req);
	*request=MPI_REQUEST_NULL;
	return plstatus->MPI_ERROR;
    }
    if (rc==1) { /* found BUT request still in use */
/* 	printf( "%s:%d should the request be set to MPI_REQUEST_NULL ???\n", __FILE__, __LINE__ ); */
	return plstatus->MPI_ERROR;
    }
    
    /* here we use a helper routine to loop for us */
    rc = ftmpi_nb_complete_upto_req (req);
    
    if( rc < 0 ) {
	ftmpi_nb_find_completed (req, status, 1);
	status->MPI_ERROR = rc;
	return (rc);
    }
    
    /* we call find_completed to fill in the status object and free the request */
    rc = ftmpi_nb_find_completed (req, plstatus, 1);
    
    if (rc==2) {    /* if found/matched/and deleted */
	ftmpi_nb_free_req_handle (req);
	*request=MPI_REQUEST_NULL;
	return plstatus->MPI_ERROR;
    }
    if (rc==1) { /* found BUT request still in use */
	return  plstatus->MPI_ERROR;
    }
    
    /* else not matched ? it might still be an inactive request */
    
    rc = ftmpi_nb_get_req_refcnt ( req );
    type = ftmpi_nb_get_req_type ( req );
    /* Check for inactive persistent requests. We rely here on refcount
       to be correct ! */
    if( (type & REQ_PERSISTENT_MASK) && (rc == 1) ) {
	return plstatus->MPI_ERROR;
    }
    
    return (MPI_ERR_INTERN);	/* how can we even get here ? */
}



int MPI_Waitany(int count,MPI_Request *array_of_requests,int *index,
		MPI_Status * status)
{
  int ret=MPI_SUCCESS;
  int i;
  int flag;
  int realcount;
  int type, rc;

  CHECK_MPIINIT;
  CHECK_INDEX_P (MPI_COMM_WORLD, index);
  CHECK_STATUS_P (MPI_COMM_WORLD, status);
  
  if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);

  if ( count == 0 )
    {
      *index = MPI_UNDEFINED;
      return ( MPI_SUCCESS );
    }

  for ( i = 0, realcount = 0; i < count; i++ )
    if (array_of_requests[i] != MPI_REQUEST_NULL ) 
      {
	rc = ftmpi_nb_get_req_refcnt ( array_of_requests[i] );
	type = ftmpi_nb_get_req_type ( array_of_requests[i] );
	/* Check for inactive persistent requests. We rely here on refcount
	   to be correct ! */
	if( !((type & REQ_PERSISTENT_MASK) && (rc == 1)) )
	  realcount++;
      }
  
  if ( realcount == 0 )
    {
      *index = MPI_UNDEFINED;
      ftmpi_pp_set_emptystatus ( status );
      return ( MPI_SUCCESS );
    }
  
  
  flag = 0;
  i = 0;
  /* I am not checking in this loop for the case
     that all requests are invalid, since I think I 
     handle this case already with the realcount-loop
     above */
  while ( !flag )
    {
      if ( i >= count ) i = 0;
     
      if (array_of_requests[i] == MPI_REQUEST_NULL )
	{
	  i++;
	  continue;
	}

      /* Check for inactive persistent requests. We rely here on refcount
	 to be correct ! */
      rc = ftmpi_nb_get_req_refcnt ( array_of_requests[i] );
      type = ftmpi_nb_get_req_type ( array_of_requests[i] );
      if( (type & REQ_PERSISTENT_MASK) && (rc == 1) )
	{
	  i++;
	  continue;
	}
	    
 
      ret = ftmpi_mpi_test ( &array_of_requests[i], &flag, status );
      if( ret != MPI_SUCCESS )
	RETURNERR( MPI_COMM_WORLD, ret );  /* TODO */
      if ( flag ) 
	{
	  *index = i;
	  break;
	}
      i++;
    }
  
  return (ret);	
} 


int MPI_Waitsome(int incount,MPI_Request *array_of_requests, int *outcount,
		 int * array_of_indices,MPI_Status *array_of_statuses)
{
  int rc;

  CHECK_MPIINIT;
  if (!outcount) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
  
  if ( incount == 0 )
    {
      *outcount = MPI_UNDEFINED;
      return ( MPI_SUCCESS );
    }

  if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
  if (!array_of_statuses) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);

  while  ( 1 )
    {
      rc = MPI_Testsome (incount, array_of_requests, outcount, 
			 array_of_indices, array_of_statuses );
      if( rc != MPI_SUCCESS ) return rc;
      if ( *outcount == MPI_UNDEFINED )
	break;
	  
      if ( *outcount > 0 )
	break;
    }

  return (MPI_SUCCESS);
}


/* TODO: call the correct error handler in the request communicator not in the
 * MPI_COMM_WORLD.
 */
int MPI_Waitall( int count,MPI_Request *array_of_requests,
		 MPI_Status *array_of_statuses)
{
    MPI_Request req;
    MPI_Status status; /* locally used use to avoid multiple ifs */
    MPI_Status *pstatus = &status;
    int rc=MPI_SUCCESS;
    int i;
    
    CHECK_MPIINIT
	
    if (!array_of_requests) RETURNERR (MPI_COMM_WORLD, MPI_ERR_REQUEST);
    if (!array_of_statuses) RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
    if (count==0) return (MPI_SUCCESS);
    
    for(i=0;i<count;i++) {
	/* should check for invalid request handle? */
	req = array_of_requests[i]; /* cache request */
	
	/* set the pstatus */
	if( array_of_statuses != MPI_STATUSES_IGNORE ) {
	    pstatus = &(array_of_statuses[i]);
	}
	
	if (req!=MPI_REQUEST_NULL) {
	    
	    /* less checks on the dt stuctures */
	    
	    /* now search the completed list of the com lists for a match to this request */
	    /* if we find a req we set the status, indices and inc the cnt etc */
	    
	    rc = ftmpi_nb_find_completed (req, pstatus, 1);
	    
	    if (rc==2) {	/* if found/matched */
		ftmpi_nb_free_req_handle (req);
		array_of_requests[i]=MPI_REQUEST_NULL;
	    }
	    else if (rc==1) {	/* if found/matched but req still in use */
		/* do nothing */
	    }
	    else if (rc==0) { /* did not match therefore force a wait */
		rc = ftmpi_mpi_wait( &(array_of_requests[i]), pstatus );
		if (rc<0) break;
	    }
	    if( pstatus->MPI_ERROR != MPI_SUCCESS ) {
		rc = pstatus->MPI_ERROR;
		break;
	    }
	} /* if not a NULL request */
	else { /* it is a NULL request */
	    ftmpi_pp_set_emptystatus( pstatus );
	} 
	
    } /* for each request in the array */
    
    /* all non yet tested requests got a MPI_ERROR field in the status equal to MPI_ERR_PENDING */
    if( array_of_statuses != MPI_STATUSES_IGNORE )
	for( i++; i < count; i++ )
	    array_of_statuses[i].MPI_ERROR = MPI_ERR_PENDING;

    /* generic error handler */
    if( rc < 0 ) {
	/* here is more tricky. we should call the error handler with the actual
	 * error code but this function should return with MPI_ERR_IN_STATUS, of
	 * course except in the case the user call us with MPI_STATUS_IGNORE...
	 */
	if( array_of_statuses != MPI_STATUSES_IGNORE ) {
	    CALL_ERRHANDLER(MPI_COMM_WORLD, rc); /* if a problem return error code */
	    return MPI_ERR_IN_STATUS;
	}
	RETURNERR( MPI_COMM_WORLD, rc );
    }
    return (MPI_SUCCESS);
}



