/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors:	
  Graham E Fagg <fagg@cs.utk.edu>
  Edgar Gabriel  <egabriel@cs.utk.edu>
  Antonin Bukovsky <tone@cs.utk.edu>
  Jeremy Millar <millar@cs.utk.edu>
  George Bosilca <bosilca@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

/*
Collective communication calls implemented as 
binomial trees.

Available currently:

MPI_Bcast
MPI_Reduce
MPI_Allreduce :     bmtree Reduce + bmtree Bcast
MPI_Allgather:      linear Gather + bmtree Bcast
MPI_Allgatherv:     linear Gatherv + bmtree Bcast
MPI_Reduce_scatter: bmtree Reduce + linear Scatterv

*/

#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-com.h"
#include "ft-mpi-op.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-top.h"
#include "ft-mpi-p2p.h"
#include "ft-mpi-nb.h"

#include "msgbuf.h"
#include "debug.h"

extern comm_info_t _ftmpi_comm[MAXCOMS];
extern int _FTMPI_OP_ERRNO;
extern int _ATB_MPI_OP_B_CNT;

/* all functions have a simple description as follows */
/*
TYPE	NAME

	Topology Used:		ring/tree (wt fanout)/linear
  static = from top DT in communicator
  per invocation (PI) = on each call recalc
  dynamic = can change during call
  Topology handling:	does the topology change on failure
  Data handling:		whole message / [static/dynamic/conn/length] 
  [or latency controlled] segmented 
  Buffer requirements:	all message, x2, etc or per segment
  if allocated via msgbuf, alloc etc
  On failure:		what it does if a failure occurs 
  during execution.. best effort,
  report errors only etc
  Description:		if needed
*/

/* Collectiives Implmentation Template */
/*
TYPE 	NAME
Topology Used:
Topology handling:
Data handling:
Buffer requirements:
On failure:
Description:
*/

/*
BASIC Barrier
Topology Used:		PI /  ring
Topology handling:	none
Data handling:		n/a
Buffer requirements:	n/a
On failure:		reports error
Description:		simple ring.
*/


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Broadcast
Topology Used:	        BMtree
Topology handling:	
Data handling:		n/a
Buffer requirements:	
On failure:		report Error
Description:		
*/

int ftmpi_coll_intra_bcast_bmtree ( void* buffer, int count, 
				    MPI_Datatype datatype, 
				    int root, MPI_Comm comm, 
				    int segsize )
{
  int err=0;
  int size, rank;
  int i, j;
  int segcount;       /* Number of elements sent with each segment */
  int num_segments;   /* Number of segmenets */
  int recvcount;      /* the same like segcount, except for the last segment */ 
  char *tmpbuf;
  MPI_Request* recv_request= NULL;
  MPI_Status recv_status;
  int typelng;
  
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
  
  tmpbuf = (char * ) buffer;


  /* Set up the correct elements of the bmtree */
  top_build_bmtree ( comm, root );

  /* -------------------------------------------------- */
  /* Determine number of segments and number of elements
     sent per operation  */
  err = ftmpi_mpi_type_size(datatype, &typelng);
  if ( err != MPI_SUCCESS ) return ( err );

  if ( segsize > 0 )
    {
      segcount     = segsize / typelng;
      num_segments = count / segcount;
    }
  else
    {
      segcount     = count;
      num_segments = 1;
    }

  /* ----------------------------------------------------- */
  /* Post Irecv if not root-node */
  if( ( _ftmpi_comm[comm].bmtree_prev != -1   ) &&
      ( _ftmpi_comm[comm].bmtree_prev != rank ) )
    {
      /* has a parent. need to receive before sending */
      recv_request = (MPI_Request*)_MALLOC( sizeof(MPI_Request) * 
					    num_segments );

      for( i = 0; i < num_segments; i++)
	{
	  /* printf("%d post src: %d\n", proc_rank_me, 
	     _ftmpi_comm[comm].bmtree_prev); */
	  if ( i == (num_segments -1) )
	    recvcount = count - (segcount * i);
	  else
	    recvcount = segcount;
	  
	  err = ftmpi_mpi_irecv(tmpbuf+i*segcount*typelng, recvcount, datatype,
			  _ftmpi_comm[comm].bmtree_prev, USR_COLL_BCAST, 
			  comm, &recv_request[i]);
	  if (err != MPI_SUCCESS ) return ( err );
	}
    }

  /* ---------------------------------------------- */
  /* If leaf node, just finish the receive */
  if(_ftmpi_comm[comm].bmtree_next[0] == -1 || 
     _ftmpi_comm[comm].bmtree_next[0] == rank)
    {
      if(recv_request != NULL)
	{
	  for(i=0; i<num_segments; i++){ 
	    err = ftmpi_mpi_wait(&recv_request[i], &recv_status);
	    if ( err != MPI_SUCCESS ) return ( err );
	  }
	}
    }
  else
    {
      /* ------------------------------------------ */
      /* root or intermediate node */      
      for( i = 0; i < num_segments; i++)
	{
	  if(recv_request != NULL)
	    {
	      /* intermediate nodes have to wait for the completion of
		 the corresponding receive */
	      /* printf("%d wait\n", rank); */
	      err = ftmpi_mpi_wait(&recv_request[i], &recv_status);
	      if ( err != MPI_SUCCESS ) return ( err );

	    }

	  for ( j = 0; j < _ftmpi_comm[comm].bmtree_nextsize; j++)
	    {
	      if( (_ftmpi_comm[comm].bmtree_next[j] < size) &&
		  (_ftmpi_comm[comm].bmtree_next[j] != rank)&&
		  (_ftmpi_comm[comm].bmtree_next[j] != -1 ))
		{
		  if ( i == ( num_segments - 1 ))
		    recvcount = count - ( segcount * i);
		  else
		    recvcount = segcount;

		  err = ftmpi_mpi_send(tmpbuf+i*segcount*typelng, recvcount, 
				 datatype, _ftmpi_comm[comm].bmtree_next[j], 
				 USR_COLL_BCAST, comm );
		  if( err != MPI_SUCCESS ) return ( err );

		} /* if ( rank == valid ) */
	    } /* for ( j = 0; j < bmtree_nextsize; j++) */
	} /* for ( i = 0; i < num_segments; i++) */
    }

  if(recv_request != NULL)
    _FREE(recv_request);

  return (MPI_SUCCESS);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Reduce
Topology Used:		BMtree
Topology handling:	
Data handling:		
Buffer requirements:	root xN 
On failure:		report error
Description:		simple blocking recv fan-in
depending on 'op' we might not need all 
the buffering we use.
*/

/* Attention: this version of the reduce operations does not
   work for:
   - non-commutative operations
   - MPI_IN_PLACE option 
   - segment sizes which are not multiplies of the extent of the datatype
*/
int ftmpi_coll_intra_reduce_bmtree (void *sendbuf,void *recvbuf,int count,
				   MPI_Datatype datatype, MPI_Op op,
				   int root, MPI_Comm comm, int segsize)
{
  int ret;
  int i, j;
  int rank;
  int size;
  char *buffer = NULL;
  MPI_Status status;
  MPI_User_function * func;
  int recvcount=0;
  int num_segments;
  char* sendtmpbuf;
  char* recvtmpbuf;
  MPI_Request* recv_request=NULL;
  int typelng;
  int segcount;
  int bufid=0, len;
  MPI_Aint ext;

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
  
  sendtmpbuf = (char*)sendbuf;
  recvtmpbuf = (char*)recvbuf;

  /* Get op-function */
  func = (MPI_User_function *) _atb_op_get(op);
  if(func == NULL) return ( MPI_ERR_INTERN);

  /* Check for invalid combinations of op and datatype */
  if ( op < _ATB_MPI_OP_B_CNT ) {
    (func)(NULL,NULL,&recvcount,&datatype);
    if ( _FTMPI_OP_ERRNO != MPI_SUCCESS ) 
      return ( _FTMPI_OP_ERRNO );
  }

  /* Set up the correct elements of the bmtree */
  top_build_bmtree ( comm, root );


  /* -------------------------------------------------- */
  /* Determine number of segments and number of elements
     sent per operation  */
  ret = ftmpi_mpi_type_size(datatype, &typelng);
  if ( ret != MPI_SUCCESS ) return ( ret );
  if ( segsize > 0 )
    {
      segcount     = segsize / typelng;
      num_segments = count / segcount;
    }
  else
    {
      segcount     = count;
      num_segments = 1;
    }

  /* ----------------------------------------------------------- */
  /* if *not* leaf node:
     - allocate request-array
     - get operations 
     - get additional buffer 
     - copy sendbuffer into recvbuffer
  */
  if ( ( _ftmpi_comm[comm].bmtree_next[0] != rank ) && 
       ( _ftmpi_comm[comm].bmtree_next[0] != -1   ) &&
       ( _ftmpi_comm[comm].bmtree_next[0] < size  ) ) 
    {
      recv_request = (MPI_Request*)_MALLOC(sizeof(MPI_Request)*num_segments);
      if ( recv_request == NULL ) return ( MPI_ERR_INTERN );

      ret = ftmpi_mpi_type_extent(datatype,&ext);
      if ( ret != MPI_SUCCESS ) return ( ret );
    
      bufid = get_msg_buf_of_size(count*size*ext,1,0);
      if ( bufid == -1) return ( bufid );
      get_msg_buf_info(bufid,(char **)&buffer,&len);

      ftmpi_ddt_copy_ddt_to_ddt(sendbuf,datatype,count,
			   recvbuf,datatype,count);
    }

  /* ----------------------------------------------------------- */
  /* receive the data from the children and combine it
     with the data in the recvbuf */


  for ( i = 0; i < _ftmpi_comm[comm].bmtree_nextsize; i++)
    {
      if ( ( _ftmpi_comm[comm].bmtree_next[i] != rank ) && 
	   ( _ftmpi_comm[comm].bmtree_next[i] != -1   ) &&
	   ( _ftmpi_comm[comm].bmtree_next[i] < size  )  )
	{
	  for( j = 0; j < num_segments; j++)
	    {
	      if ( j == (num_segments-1) )
		recvcount = count - ( segcount * j);		
	      else
		recvcount = segcount;
	      
	      ret = ftmpi_mpi_irecv(buffer+j*segcount*typelng, recvcount, datatype, 
				    _ftmpi_comm[comm].bmtree_next[i], USR_COLL_REDUCE, 
				    comm, &recv_request[j]);
	      if ( ret != MPI_SUCCESS ) return ( ret );
	    }
	  
	  for( j = 0; j < num_segments; j++) {	    
	    ret = ftmpi_mpi_wait(recv_request+j, &status);
	    if ( ret != MPI_SUCCESS ) return ( ret );
	  }

	}

      if (func != NULL) 
	(func)(buffer,recvbuf,&count,&datatype);
    } 
     
  /* ----------------------------------------------------------- */
  /* if not root node, send the data to the parent. For intermediate
     notes, send the recvbuf, for leaf nodes send the sendbuf
  */
  if( ( _ftmpi_comm[comm].bmtree_prev != rank ) && 
      ( _ftmpi_comm[comm].bmtree_prev != -1   ) )
    {
      if ( ( _ftmpi_comm[comm].bmtree_next[0] != rank ) && 
	   ( _ftmpi_comm[comm].bmtree_next[0] != -1   ) &&
	   ( _ftmpi_comm[comm].bmtree_next[0] < size  ) ) 
	{
	  for( j = 0; j < num_segments; j++)
	    {
	      if ( j == num_segments -1 )
		recvcount = count - ( segcount * j);
	      else
		recvcount = segcount;

	      ret = ftmpi_mpi_send(recvtmpbuf+j*segcount*typelng, recvcount, datatype, 
				   _ftmpi_comm[ comm].bmtree_prev, USR_COLL_REDUCE, 
				   comm );
	      if ( ret != MPI_SUCCESS ) return ( ret );
	    }
	}
      else
	{
	  /* this section is just for the leaf-nodes */
	  for( j = 0; j < num_segments; j++)
	    {
	      if ( j == num_segments -1 )
		recvcount = count - ( segcount * j);
	      else
		recvcount = segcount;

	      ret = ftmpi_mpi_send(sendtmpbuf+j*segcount*typelng, recvcount, datatype, 
				   _ftmpi_comm[comm].bmtree_prev, USR_COLL_REDUCE, 
				   comm );
	      if ( ret != MPI_SUCCESS ) return ( ret );
	    }
	}
    }

  /* --------------------------------------------------------- */
  /* Free the allocated memory and the request list */
  if ( ( _ftmpi_comm[comm].bmtree_next[0] != rank ) && 
       ( _ftmpi_comm[comm].bmtree_next[0] != -1   ) &&
       ( _ftmpi_comm[comm].bmtree_next[0] < size  ) ) 
    {
      if ( recv_request != NULL )
	_FREE(recv_request);
      
      free_msg_buf(bufid);
    }      
  
  return ( MPI_SUCCESS );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Allreduce
Topology Used:		BMtree reduce/bcast
Topology handling:	
Data handling:		
Buffer requirements:	
On failure:		report error
Description:		simple.. Reduce and then bcast
Must be replaced asap.
*/

int ftmpi_coll_intra_allreduce_bmtree(void *sendbuf,void *recvbuf,int count,
				      MPI_Datatype datatype, MPI_Op op,
				      MPI_Comm comm, int segsize)
{
  int rc;

  rc =  ftmpi_coll_intra_reduce_bmtree(sendbuf,recvbuf,count,datatype,op,0, comm, segsize);
  if (rc<0) return (rc);

  rc = ftmpi_coll_intra_bcast_bmtree(recvbuf,count,datatype,0,comm, segsize);
  return(rc);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Allgather
Topology Used:			PI / linear fan-in fan-out
Topology handling:		none
Data handling:			full message no segmentation
Buffer requirements:		root = xN
On failure:			report error
Description:			linear gather/bmtree bcast
gather/bcast. No extra err checking.
*/

int ftmpi_coll_intra_allgather_bmtree (void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf,int rcnt,MPI_Datatype rddt,
				      MPI_Comm comm, int segsize)
{
  int ret;
  int root = 0;
  int size;
  
  size = ftmpi_com_size ( comm );
  if ( size < 0 ) return ( size );
  
  ret = ftmpi_coll_intra_gather_linear(sbuf,scnt,sddt,rbuf,rcnt,rddt,root,
				       comm,segsize);
  if(ret < 0) return(ret);
  
  ret = ftmpi_coll_intra_bcast_bmtree(rbuf,rcnt*size,rddt,root,comm,segsize);
  return(ret);
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
TYPE 	Allgatherv
Topology Used:			PI fan-in / fan out uses GV & bcast
Topology handling:
Data handling:
Buffer requirements:		at root.. xN
On failure:			returns error code
Description:	Basic / untuned

*/

int ftmpi_coll_intra_allgatherv_bmtree(void *sendbuf,int sendcount,
				       MPI_Datatype sendtype, void *recvbuf, 
				       int *recvcounts, int *displs,
				       MPI_Datatype recvtype, MPI_Comm comm, 
				       int segsize)
{
  int root = 0;
  int size;
  int ret;
  MPI_Datatype newtype;
  int tsize;
  MPI_Aint text;
  

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );

  ret = ftmpi_coll_intra_gatherv_linear(sendbuf,sendcount,sendtype,
					recvbuf,recvcounts,
					displs,recvtype,root,comm,segsize);
  if(ret != MPI_SUCCESS){
    printf("PROBLEM WITHIN MPI_ALLGATHERV WITH MPI_Gatherv %d\n",ret);
    return ret;
  }

  ret = ftmpi_mpi_type_indexed(size,recvcounts,displs,recvtype,&newtype);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_commit(&newtype);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_size(newtype,&tsize);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_extent(newtype,&text);
  if ( ret != MPI_SUCCESS ) return ( ret );
  
  ret = ftmpi_coll_intra_bcast_bmtree(recvbuf,1,newtype,root,comm,segsize);
  if(ret != MPI_SUCCESS){
    printf("PROBLEM WITHIN MPI_ALLGATHERV WITH MPI_Bcast\n");
    return ret;
  }

  ret = ftmpi_mpi_type_free(&newtype);

  return ret;
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
TYPE 	Reduce_scatter
Topology Used: 
Topology handling:
Data handling:
Buffer requirements:
On failure:
Description:   mbtree reduce/linear scatterv
*/
int ftmpi_coll_intra_reducescatter_bmtree(void * sbuf,void * rbuf,int *counts,
					  MPI_Datatype ddt,MPI_Op op, 
					  MPI_Comm comm, int segsize)
{
  int size;
  int rank;
  int sum = 0;
  int ret;
  int i;
  int disp_id,len;
  int * disp = NULL;
  MPI_Aint ext;
  char * tmp_buf;
  int totalcount = 0;

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );

  ret = ftmpi_mpi_type_extent ( ddt, &ext );
  if ( ret != MPI_SUCCESS ) return ( ret );

  for ( i = 0; i < size; i ++ )
    totalcount += counts[i];

  
  tmp_buf = ( char *)_MALLOC( totalcount * ext );
  if ( tmp_buf == NULL ) return ( MPI_ERR_INTERN);

  ret = ftmpi_coll_intra_reduce_bmtree (sbuf,tmp_buf,totalcount,
					ddt,op,0,comm,segsize );
  if(ret == MPI_SUCCESS){
    disp_id = get_msg_buf_of_size(size*sizeof(int),1,0);
    if(disp_id == -1){
      return(disp_id);
    }    
    get_msg_buf_info(disp_id,(char **)&disp,&len);

    sum = counts[0]; 
    disp[0] = 0;
    for(i=1;i<size;i++){
      disp[i] = sum;
      sum += counts[i];
      }

    ret = ftmpi_coll_intra_scatterv_linear (tmp_buf, counts, disp, ddt, 
					    rbuf, counts[rank],
					    ddt,0,comm,segsize);
    
    free_msg_buf(disp_id);
    if ( rank == 0 )
      _FREE( tmp_buf );

  }

  return(ret);
}
