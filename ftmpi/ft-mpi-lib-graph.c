
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Edgar Gabriel    <egabriel@cs.utk.edu>
			Graham E Fagg    <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* NOTE: The implementation of this file is based on the implementation
   of the general graph-topologies in PACX-MPI. */

#include <stdio.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-group.h"
#include "ft-mpi-graph.h"
#include "ft-mpi-com.h"
#include "ft-mpi-intercom.h"
#include "debug.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Graph_create         = PMPI_Graph_create
#    pragma weak  MPI_Graph_get            = PMPI_Graph_get
#    pragma weak  MPI_Graph_map            = PMPI_Graph_map
#    pragma weak  MPI_Graph_neighbors      = PMPI_Graph_neighbors
#    pragma weak  MPI_Graph_neighbors_count = PMPI_Graph_neighbors_count
#    pragma weak  MPI_Graphdims_get        = PMPI_Graphdims_get

#endif 

#    define  MPI_Graph_create          PMPI_Graph_create
#    define  MPI_Graph_get             PMPI_Graph_get
#    define  MPI_Graph_map             PMPI_Graph_map
#    define  MPI_Graph_neighbors       PMPI_Graph_neighbors
#    define  MPI_Graph_neighbors_count PMPI_Graph_neighbors_count
#    define  MPI_Graphdims_get         PMPI_Graphdims_get




/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Graph_create (MPI_Comm comm, int nnodes, int *index, int *edges,
		      int reorder, MPI_Comm *newcomm )
{

  int ret;
  int *ranks=NULL;
  int n_ranks;
  int i, j;
  MPI_Group group, newgroup;
  graph_info_t *graph;
  int size;
  int lb, ub;

  /* Check for valid arguments  */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  ret = ftmpi_com_test_inter ( comm ) ;
  if ( ret ) RETURNERR ( comm, MPI_ERR_COMM);

  size = ftmpi_com_size ( comm );
  
  if ( (nnodes < 0 ) || (index == NULL) || (edges == NULL ))
    RETURNERR ( comm, MPI_ERR_ARG );
  if ( nnodes > size ) RETURNERR ( comm, MPI_ERR_ARG );

  if ( nnodes == 0 )
    {
      *newcomm = MPI_COMM_NULL;
      return ( MPI_SUCCESS );
    }
  
  ranks = (int*)_MALLOC( nnodes * sizeof(int));
  if ( ranks == NULL )
    RETURNERR (comm, MPI_ERR_INTERN);

  for ( i = 0; i < index[nnodes-1]; i++) 
    if ( (edges[i]<0) || (edges[i]>=size)) 
      RETURNERR (comm, MPI_ERR_ARG );
  
 
  /* Check for edge to self. We need this to pass
     in the Intel testsuite test MPI_Graph_create_err5,
     but I did not find in the MPI-spec anywhere a sentence
     that this is really forbidden! */
  for ( i = 0; i < nnodes; i++ ) {
    (i == 0) ? (lb = 0) : (lb = index[i-1]);
    ub = index[i];

    for ( j = lb; j < ub; j++) 
      if ( edges[j] == i ) 
	RETURNERR (comm, MPI_ERR_ARG);
  }

  n_ranks = 0;
  if ( index[0] ) ranks[n_ranks++] = 0;

  for ( i = 1; i < nnodes; i++)
    if ( index[i] > index[i-1] )
      ranks[n_ranks++] = i;


  group = ftmpi_com_get_group ( comm);
  ret = ftmpi_mpi_group_incl ( group, n_ranks, ranks, &newgroup );
  if ( ret != MPI_SUCCESS) RETURNERR ( comm, ret );

  ret = ftmpi_mpi_comm_create ( comm, newgroup, newcomm );
  if ( ret != MPI_SUCCESS) RETURNERR ( comm, ret );
  

  _FREE(ranks);

  /* Need not free 'group' if not using the MPI-interface */

  ret = ftmpi_group_free ( &newgroup );
  if ( ret != MPI_SUCCESS) RETURNERR ( comm, ret );

  if ( *newcomm != MPI_COMM_NULL )
    {
      graph = ftmpi_graph_set ( nnodes, index, edges );
      ftmpi_comm_attach_graph(*newcomm, graph); 
    }

  return ( ret );
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Graphdims_get ( MPI_Comm comm, int *nnodes, int *nedges )
{
  graph_info_t *graph;

  /* Check communicator */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  graph = (graph_info_t *) ftmpi_comm_get_graph_ptr ( comm );
  if ( graph == NULL )
    RETURNERR ( comm, MPI_ERR_TOPOLOGY );
       
  *nnodes = ftmpi_graph_get_nnodes(graph);
  *nedges = ftmpi_graph_get_nedges(graph);

  return ( MPI_SUCCESS );
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Graph_get ( MPI_Comm comm, int maxindex, int maxedges, 
		    int *index, int *edges ) 
{
  graph_info_t *graph;
  int nnodes, nedges;
  int *iptr, *eptr;
  int i;

  /* Check communicator */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  graph = (graph_info_t *) ftmpi_comm_get_graph_ptr ( comm );
  if ( graph == NULL )
    RETURNERR ( comm, MPI_ERR_TOPOLOGY );
      
  nnodes = ftmpi_graph_get_nnodes( graph );
  nedges = ftmpi_graph_get_nedges( graph );

  /* Copy index array */
  if ( maxindex > nnodes ) 
    maxindex = nnodes;

  iptr = ftmpi_graph_get_index_ptr ( graph );
  for ( i = 0 ; i < maxindex; i++ )
    index[i] = iptr[i];

  /* Copy edges */
  if ( maxedges > nedges )
    maxedges = nedges;
  
  eptr = ftmpi_graph_get_edges_ptr ( graph );
  for ( i = 0; i < maxedges; i++ )
    edges[i] = eptr[i];

  return ( MPI_SUCCESS );
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Graph_neighbors ( MPI_Comm comm, int rank, int maxneighbors,
			  int *neighbors )
{
  graph_info_t *graph;

  /* Check communicator */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  graph = (graph_info_t *) ftmpi_comm_get_graph_ptr ( comm );
  if ( graph == NULL )
      RETURNERR ( comm, MPI_ERR_TOPOLOGY );

  ftmpi_graph_calc_neighbors ( neighbors, maxneighbors, rank, graph );


  return ( MPI_SUCCESS );
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Graph_neighbors_count ( MPI_Comm comm, int rank, int *neighbors )
{
  graph_info_t *graph;
  
  /* Check communicator */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  graph = (graph_info_t *) ftmpi_comm_get_graph_ptr ( comm );
  if ( graph == NULL )
    RETURNERR ( comm, MPI_ERR_TOPOLOGY );

  *neighbors = ftmpi_graph_calc_neighbors_count ( graph, rank );

  return ( MPI_SUCCESS );
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Graph_map ( MPI_Comm comm, int nnodes, int *index, int *edges,
		   int *newrank )
{
  int size;

  /* COMMENT: According to the MPI-1 standard, it is a legal
     implementation of this function to return 
        newrank = rank
     This is what we are doing in this implementation. However,
     for later research, an optimization of this function would
     be great.  EG Jan. 31 2003 */

  /* Algorithm: 
     - check input parameters. comm need *not* have a 
       any topology!!
     - determine own rank and return it
  */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );

  if ( (nnodes > size) || (nnodes <= 0 ) ) RETURNERR ( comm, MPI_ERR_ARG);

  *newrank = ftmpi_com_rank ( comm );
  return ( MPI_SUCCESS );
}

