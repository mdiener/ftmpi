
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_DB_TYPES
#define _FT_MPI_H_DB_TYPES


#define MAX_DB_NAME_LEN	256	/* short? */

#define DB_OK	0
#define DB_NO_RECORD	-1
#define DB_BAD_ARGS	-2
#define DB_BAD_CONN	-3
#define	DB_FAILURE_DETECT -4

/* This is used for getting/waiting on a record */
#define DB_ANY	-9


typedef enum {
	DB_FT_MPI_STATE,
	DB_FT_MPI_STARTUP,
	DB_FT_MPI_CONN,
	DB_FT_MPI_ONFAIL,
	DB_FT_MPI_RECOVER,
	DB_FT_MPI_RECOVERED,
	DB_FT_MPI_LEADER, 
	DB_FT_MPI_HANDLER_INFO, 
	DB_FT_MPI_LAST_RECORD
	}	db_ft_mpi_record_t;

typedef struct {
	int	db_record_type;				/* its type # */
	char	db_access_name[MAX_DB_NAME_LEN];	/* the string used to access it */
	int	db_arg_count;				/* extra args to above string */
	long	db_retry_interval;			/* default retry time in uSecs */
	int     db_callback_tag;                        /* callback tag */
	int     db_callback_sockfd;                     /* callback sockfd */
	int     db_callback_addr;                       /* callback addr */
	int     db_callback_port;                       /* callback port */
	}	db_record_access_t;


#endif /*  _FT_MPI_H_DB_TYPES */

