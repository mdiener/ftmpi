
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
                        Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg    <fagg@cs.utk.edu>
			Edgar Gabriel    <egabriel@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



#include <stdio.h>
#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-cart.h"
#include "ft-mpi-com.h"
#include "ft-mpi-group.h"
#include "ft-mpi-intercom.h"
#include "debug.h"

/* definitions for the profiling interface */
#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Cartdim_get          = PMPI_Cartdim_get
#    pragma weak  MPI_Cart_coords          = PMPI_Cart_coords
#    pragma weak  MPI_Cart_create          = PMPI_Cart_create
#    pragma weak  MPI_Cart_get             = PMPI_Cart_get
#    pragma weak  MPI_Cart_map             = PMPI_Cart_map
#    pragma weak  MPI_Cart_rank            = PMPI_Cart_rank
#    pragma weak  MPI_Cart_shift           = PMPI_Cart_shift
#    pragma weak  MPI_Cart_sub             = PMPI_Cart_sub

#endif 

#    define  MPI_Cartdim_get           PMPI_Cartdim_get
#    define  MPI_Cart_coords           PMPI_Cart_coords
#    define  MPI_Cart_create           PMPI_Cart_create
#    define  MPI_Cart_get              PMPI_Cart_get
#    define  MPI_Cart_map              PMPI_Cart_map
#    define  MPI_Cart_rank             PMPI_Cart_rank
#    define  MPI_Cart_shift            PMPI_Cart_shift
#    define  MPI_Cart_sub              PMPI_Cart_sub



/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int MPI_Cart_create(MPI_Comm comm_old, int ndims, int *dims, 
		    int *periods,int reorder, MPI_Comm *comm_cart )
{
  int range[1][3];
  MPI_Group group_old, group;
  cart_info_t * cart = NULL;
  int size;
  int ret;
  int i;
  int num_ranks = 1;

  /* Check for valid arguments  */

  CHECK_MPIINIT;
  CHECK_COM(comm_old);
  CHECK_FT(comm_old);

  ret = ftmpi_com_test_inter ( comm_old ) ;
  if ( ret ) RETURNERR ( comm_old, MPI_ERR_COMM);

  if (ndims < 1 || dims == (int *)0)
    RETURNERR (comm_old, MPI_ERR_DIMS);

  /* Determine number of ranks in topology */
  for (i=0;i<ndims;i++){
    num_ranks *= (dims[i]>0)?dims[i]:-dims[i];
  }
  if(num_ranks<1){
    (*comm_cart) = MPI_COMM_NULL;
    printf("MPI_Cart_create: invalid array of dimensions, total sum "
	   "less than one\n");
    RETURNERR (comm_old,MPI_ERR_TOPOLOGY);
  }

  size = ftmpi_com_size (comm_old);
  if(num_ranks > size) {
    printf("MPI_Cart_create: invalid array of dimensions, total sum: "
	   "%d exceeds number of processes %d \n",num_ranks,size);
    RETURNERR ( comm_old,MPI_ERR_ARG);
  }
  
  range[0][0]=0; 
  range[0][1]=num_ranks-1; 
  range[0][2]=1;


  group_old = ftmpi_com_get_group(comm_old );
  ret = ftmpi_mpi_group_range_incl(group_old,1,range,&group);
  if ( ret != MPI_SUCCESS) RETURNERR ( comm_old, ret );

  ret = ftmpi_mpi_comm_create(comm_old,group,comm_cart);
  if ( ret != MPI_SUCCESS) RETURNERR ( comm_old, ret );

  ret = ftmpi_group_free(&group);
  if ( ret != MPI_SUCCESS) RETURNERR ( comm_old, ret );

  /* Need not free group_old if not using the MPI-interface */

  if (  *comm_cart != MPI_COMM_NULL )
    {
      cart = ftmpi_cart_set ( ndims, num_ranks, dims, periods );
      ftmpi_comm_attach_cart(*comm_cart,cart); 
    }

  return (ret);
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Cart_rank(MPI_Comm comm, int * coords,int * rank)
{
  int ret;
  cart_info_t * cart = NULL;

  /*Check for valid arguments*/
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  if(rank == NULL)
    RETURNERR(comm, MPI_ERR_ARG);
  
  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
    RETURNERR(comm, MPI_ERR_TOPOLOGY);

  *rank = ftmpi_cart_calc_rank ( cart, coords );
  if ( *rank == MPI_PROC_NULL )
    {
      ret = MPI_ERR_ARG;
      RETURNERR (comm, ret );
    }
  else
    ret = MPI_SUCCESS;

  return(ret);
}
/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Cartdim_get ( MPI_Comm comm, int *ndims ) 
{
  cart_info_t *cart=NULL;

  /*Check for valid arguments*/
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
    RETURNERR(comm, MPI_ERR_TOPOLOGY);

  *ndims = ftmpi_cart_get_ndims (cart);
  if (*ndims < 0 )
    RETURNERR (comm, MPI_ERR_INTERN );

  return ( MPI_SUCCESS);
}

/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Cart_shift ( MPI_Comm comm, int direction, int disp, int *r_source,
		     int *r_dest )
{
  cart_info_t *cart;
  int ret;
  int ndims;
  int rank;
  int tmp_disp;

  /* Check calling arguments;
     - comm (including topology)
     - direction shouldn't be higher than cart->ndims
   */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
    RETURNERR(comm, MPI_ERR_TOPOLOGY);

  rank = ftmpi_com_rank (comm);
  ndims = ftmpi_cart_get_ndims ( cart);
  if ( (direction > ndims ) || (direction < 0 ))
    RETURNERR (comm,  MPI_ERR_ARG );

  if ( disp == 0 ) RETURNERR(comm, MPI_ERR_ARG);

  if ( disp < 0 )
    tmp_disp = -disp;
  else
    tmp_disp = disp;
     
  ret = ftmpi_cart_calc_neighbors ( direction, tmp_disp, r_source, r_dest,
				    cart, rank );
  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  if ( disp < 0 )
    {
      /* Swap source and dest */
      tmp_disp  = *r_source;
      *r_source = *r_dest;
      *r_dest   = tmp_disp;
    }

  return ( ret );
}

/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Cart_sub ( MPI_Comm comm, int *rem_dims, MPI_Comm *newcomm)
{
  cart_info_t *cart, *newcart;
  int ret;
  int i;
  int rank, newsize;
  int *dptr, *pptr;
  int key=0, color=0;
  int num_remain_dims;
  int *tmp_coords;
  int *dims, *periods;
  int ndims;

  /* Algorithm:
     - check input parameters 
     - determine my rank and coords
     - determine key and color using coords
     - split the communicator using MPI_Comm_split
     - determine the correct dims and periods array
     - set the cart-structure with ftmpi_cart_set
     - attach cart-structure with ftmpi_comm_cart_attach
  */

  /* Check input parameters */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  ret = ftmpi_com_test_inter ( comm ) ;
  if ( ret ) RETURNERR ( comm, MPI_ERR_COMM);

  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
    RETURNERR(comm, MPI_ERR_TOPOLOGY);

  /* Determine rank and coords */
  rank = ftmpi_com_rank (comm);
  ndims = ftmpi_cart_get_ndims (cart);
  tmp_coords = (int *)_MALLOC( sizeof(int) * ndims );
  if ( tmp_coords == NULL )
    RETURNERR (comm, MPI_ERR_INTERN);
  
  dptr = ftmpi_cart_get_dims_ptr (cart);
  ftmpi_cart_calc_coords ( tmp_coords, dptr, rank, ndims );

  /* determine key and color and create new communicator */
  for ( i = 0; i < ndims; i++ )
    {
      if ( rem_dims[i] )
	key = (key*dptr[i]) + tmp_coords[i];
      else
	color = (color*dptr[i]) + tmp_coords[i];
    }

  ret = ftmpi_mpi_comm_split (comm, color, key, newcomm );
  if ( ret != MPI_SUCCESS) RETURNERR ( comm, ret );
  

  /* Determine new dims and periods */
  dims    = (int *)_MALLOC(sizeof(int) * ndims);
  periods = (int *)_MALLOC(sizeof(int) * ndims);
  if ( (dims == NULL ) || (periods == NULL ))
    RETURNERR (comm, MPI_ERR_INTERN);
  
  pptr = ftmpi_cart_get_periods_ptr (cart);
  for ( num_remain_dims=0, i = 0; i < ndims; i++ )
    {
      if ( rem_dims[i] )
	{
	  dims[num_remain_dims]    = dptr[i];
	  periods[num_remain_dims] = pptr[i];
	  num_remain_dims++;
	}
    }

  newsize = ftmpi_com_size (*newcomm);
  newcart = ftmpi_cart_set (num_remain_dims, newsize, dims, periods);
  ftmpi_comm_attach_cart ( *newcomm, newcart);

  /* free temporary arrays */
  _FREE(tmp_coords);
  _FREE(dims);
  _FREE(periods);
  
  return (ret);
}

/********************************************************************/
/********************************************************************/
/********************************************************************/
int MPI_Cart_map ( MPI_Comm comm, int ndims, int *dims, int *periods,
		   int *newrank )
{
  int i, sum=1;
  int size;

  /* COMMENT: According to the MPI-1 standard, it is a legal
     implementation of this function to return 
        newrank = rank
     This is what we are doing in this implementation. However,
     for later research, an optimization of this function would
     be great.  EG Jan. 30 2003 */

  /* Algorithm: 
     - check input parameters. comm need *not* have a 
       cartesian topology!!
     - determine own rank and return it
  */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  size = ftmpi_com_size ( comm );

  if ( ndims <= 0 ) RETURNERR (comm, MPI_ERR_DIMS);
  if ( dims == NULL ) RETURNERR ( comm, MPI_ERR_ARG);

  /* Determine number of ranks in topology */
  for (i=0;i<ndims;i++){
    sum *= (dims[i]>0)?dims[i]:-dims[i];
  }

  if ( sum > size ) RETURNERR (comm, MPI_ERR_DIMS);
  

  *newrank = ftmpi_com_rank ( comm );
 
  return ( MPI_SUCCESS );
}
/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
int MPI_Cart_get ( MPI_Comm comm, int maxdims, int *dims, int *periods, 
		   int *coords )
{
  int *dptr=NULL;
  int *pptr=NULL;
  int ret;
  cart_info_t *cart;
  int rank;
  int ndims;
  int i;

  /* CHeck for input arguments:
     - comm has to have a cartesian structure
     - maxdims should not be larger than ndims in the cart structure
  */
  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
    RETURNERR(comm, MPI_ERR_TOPOLOGY);

  if(maxdims<0)
    RETURNERR(comm, MPI_ERR_DIMS);

  ndims = ftmpi_cart_get_ndims (cart);
  if (ndims < 0 )
    RETURNERR (comm, MPI_ERR_INTERN );

  if ( maxdims > ndims )
    {
      maxdims = ndims;
      /*      printf ("MPI_Cart_get: input parameter maxdims exceeds dimension of"
	      " cartesian structure\n");
      RETURNERR (comm, MPI_ERR_TOPOLOGY);
      */
    }

  /* Copy dims and periods into user buffer */
  dptr = ftmpi_cart_get_dims_ptr (cart);
  pptr = ftmpi_cart_get_periods_ptr (cart);
  for ( i = 0; i < maxdims; i++)
    {
      dims[i]    = dptr[i];
      periods[i] = pptr[i];
    }

  rank = ftmpi_com_rank (comm);
  ret = ftmpi_cart_calc_coords ( coords, dims, rank, maxdims );

  if ( ret != MPI_SUCCESS )
    RETURNERR ( comm, ret );

  return (ret );
}
/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
int MPI_Cart_coords ( MPI_Comm comm, int rank, int maxdims, int *coords )
{
  cart_info_t * cart = NULL;
  int ndims;
  int *dptr;

  CHECK_MPIINIT;
  CHECK_COM(comm);
  CHECK_FT(comm);

  /* Get topology information from the communicator */
  cart = (cart_info_t *) ftmpi_comm_get_cart_ptr(comm);
  if(cart == NULL)
      RETURNERR(comm, MPI_ERR_TOPOLOGY);

  if(rank < 0)
    RETURNERR(comm, MPI_ERR_RANK);
  if(maxdims<1)
    RETURNERR (comm, MPI_ERR_DIMS);
  if(coords == NULL)
    RETURNERR(comm, MPI_ERR_ARG);

  ndims = ftmpi_cart_get_ndims (cart);
  if (ndims < 0 )
    RETURNERR (comm, MPI_ERR_INTERN );
  if ( maxdims > ndims )
    {
      /*printf ("MPI_Cart_coords: input parameter maxdims exceeds dimension of"
	" cartesian structure\n");
	RETURNERR (comm, MPI_ERR_TOPOLOGY);
      */
    }
  dptr = ftmpi_cart_get_dims_ptr (cart);
  return ftmpi_cart_calc_coords ( coords, dptr, rank, ndims );
}
