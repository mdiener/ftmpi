
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This is the macro defs for simple checks of library API args

 If an arg is used in a non standard way, then we do the check the long way

 always look here to make sure we handle the test and rc the way you want
 before using these macros. <exit soap box>

GEF UTK03
 */

#ifndef _FT_MPI_CHECK_H
#define _FT_MPI_CHECK_H

#include "ft-mpi-lib.h"

/* Checks used by almost every lib routine */
/* we don't go through the error handler as we cannot have one setup! */

#define	CHECK_MPIINIT	{ if (!MPI_INITIALISED) return (MPI_ERR_COMM); }


/* checks on communicators */

#define CHECK_COM(comm)	{int _ret; _ret=ftmpi_com_ok (comm);if (_ret<0)RETURNERR (MPI_COMM_WORLD, MPI_ERR_COMM); }
/* checks on communicators */

#define CHECK_GROUP(comm,group)	{int _ret; _ret=ftmpi_group_ok (group);if (_ret<0)RETURNERR (group, _ret); }

/* checks on p2p ops */

/* tag checks depend on if its a send or recv op */

#define CHECK_TAG_R(comm,tag)		{if((tag>MPI_TAG_UB)||((tag<0)&&(tag!=MPI_ANY_TAG)))RETURNERR(comm, MPI_ERR_TAG); }

#define CHECK_TAG_S(comm,tag)		{if((tag>MPI_TAG_UB)||(tag<0))RETURNERR (comm, MPI_ERR_TAG); }


/* checks on non-blocking coms */

#define CHECK_REQ_P(comm,request)	{ if (!request) RETURNERR (comm,MPI_ERR_REQUEST); }

#define CHECK_FLAG_P(comm,flag)	{ if (!flag) RETURNERR (comm,MPI_ERR_ARG); }

#define CHECK_STATUS_P(comm,status)	{ if (!status) RETURNERR (comm,MPI_ERR_ARG); }

/* misc checks */

#define CHECK_INDEX_P(comm,index)	{ if (!index) RETURNERR (comm,MPI_ERR_ARG); }

/* checks for failures */
/* this causes the system to call an error handler and then return */
/* MPI_ERR_OTHER */

#define CHECK_FT(comm) {if (ft_mpi_conn_any_error()) RETURNERR(comm,MPI_ERR_OTHER);}

/* the same like CHECK_FT, but does not call the error-handler */
#define CHECK_FT_NOE {if (ft_mpi_conn_any_error()) return(MPI_ERR_OTHER);}

#define CHECK_DDT(comm,ddt,cmt) { if(ftmpi_ddt_check_ddt(ddt,cmt) == 0) RETURNERR(comm,MPI_ERR_TYPE);}

#define GET_PPT_SIZE(comm, size) { int _inter; _inter = ftmpi_com_test_inter (comm); if ( _inter ) size = ftmpi_com_remote_size(comm);  else size = ftmpi_com_size (comm);}

#define CHECK_BUF(buf,ddt,count) { if(buf == NULL && count > 0 && ddt > 0 && ddt < FTMPI_DDT_B_DATATYPE_TOTAL) RETURNERR(comm,MPI_ERR_BUFFER);}

#endif /* _FT_MPI_CHECK_H */

