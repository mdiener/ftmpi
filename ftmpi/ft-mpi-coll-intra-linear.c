/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

  --------------------------------------------------------------------------

  Authors:	
  Graham E Fagg <fagg@cs.utk.edu>
  Antonin Bukovsky <tone@cs.utk.edu>
  Jeremy Millar <millar@cs.utk.edu>

  --------------------------------------------------------------------------

  NOTICE

  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.

  Neither the University of Tennessee nor the Authors make any
  representations about the suitability of this software for any
  purpose.  This software is provided ``as is'' without express or
  implied warranty.

  HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
  U.S. Department of Energy.

*/

/*
Collective communication calls
*/

#include "mpi.h"
#include "ft-mpi-lib.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-op.h"
#include "ft-mpi-ddt-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-p2p.h"

#include "debug.h"
#include "msgbuf.h"

extern int _FTMPI_OP_ERRNO;
extern int _ATB_MPI_OP_B_CNT;

/* Order */

/* 
MPI_Barrier
MPI_Bcast
MPI_Reduce
MPI_Allreduce
MPI_Allgather
MPI_Allgatherv
MPI_Alltoall
MPI_Alltoallv
MPI_Gather
MPI_Gatherv
MPI_Scatter
MPI_Scatterv
MPI_Scan
*/

/* all functions have a simple description as follows */
/*
TYPE	NAME

	Topology Used:		ring/tree (wt fanout)/linear
  static = from top DT in communicator
  per invocation (PI) = on each call recalc
  dynamic = can change during call
  Topology handling:	does the topology change on failure
  Data handling:		whole message / [static/dynamic/conn/length] 
  [or latency controlled] segmented 
  Buffer requirements:	all message, x2, etc or per segment
  if allocated via msgbuf, alloc etc
  On failure:		what it does if a failure occurs 
  during execution.. best effort,
  report errors only etc
  Description:		if needed
*/

/* Collectiives Implmentation Template */
/*
TYPE 	NAME
Topology Used:
Topology handling:
Data handling:
Buffer requirements:
On failure:
Description:
*/

/*
BASIC Barrier
Topology Used:		PI /  ring
Topology handling:	none
Data handling:		n/a
Buffer requirements:	n/a
On failure:		reports error
Description:		simple ring.
*/


int ftmpi_coll_intra_barrier_linear ( MPI_Comm comm )
{
  int check;
  int size, rank;
  int rc;
  int err=0;
  int tag;
  MPI_Status status;
  int next, prev, i;
  
#ifdef COLL_VERBOSE
  printf("[%d] barrier count %d l[%d]r[%d] root[%d]\n", coms[comm].my_rank, cnt, left, right,
  root); 
#endif

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
    
  tag = USR_COLL_BAR;
  
  next = (rank+1)%size;
  prev = rank-1;
  if (prev<0) prev=size-1;
 
  if (!rank) {
    for( i = 0; i < 2; i++ ) {
      rc = ftmpi_mpi_send (&check, 1, MPI_INT, next, tag, comm);
#ifndef NOSTOPCOLL
      if (rc<0) return (rc);
#else
      if ( rc < 0 ) err = 1;
#endif
      rc = ftmpi_mpi_recv (&check, 1, MPI_INT, prev, tag, comm, &status);
#ifndef NOSTOPCOLL
      if (rc<0) return (rc);
#else
      if ( rc < 0 ) err = 1;
#endif
	}
  }
  else {
    for( i = 0; i < 2; i++ ) {
      rc = ftmpi_mpi_recv (&check, 1, MPI_INT, prev, tag, comm, &status);
#ifndef NOSTOPCOLL
      if (rc<0) return (rc);
#else
      if ( rc < 0 ) err = 1;
#endif
      rc = ftmpi_mpi_send (&check, 1, MPI_INT, next, tag, comm);
#ifndef NOSTOPCOLL
      if (rc<0) return (rc);
#else
      if ( rc < 0 ) err = 1;
#endif
    }
  }
  
  if ( err ) 
    rc = MPI_ERR_OTHER;
  else
    rc = MPI_SUCCESS;

  return ( rc );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Broadcast
Topology Used:		PI/ linear
Topology handling:	none
Data handling:		n/a
Buffer requirements:	wholex2
On failure:		report Error
Description:		slowest bcast in world
*/

int ftmpi_coll_intra_bcast_linear ( void* buffer, int count, 
				    MPI_Datatype datatype, 
				    int root, MPI_Comm comm, 
				    int segsize )
{
  int tag, i, rc;
  int err=0;
  MPI_Status status;
  int size, rank;
  
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
  
  tag = USR_COLL_BCAST;
  
#ifdef COLL_VERBOSE
   printf("[%d] of [%d] entered bcast\n", rank, size); fflush(stdout); 
#endif

  if (rank==root) {
    for (i=0;i<size;i++) 
      if (i!=rank) {
        rc = ftmpi_mpi_send (buffer, count, datatype, i, tag, comm);
        if (rc<0) err = 1; 
#ifndef NOSTOPCOLL
        if (rc<0) return (rc); 
#endif
      }
  }
  
  if (rank!=root) {
    rc = ftmpi_mpi_recv (buffer, count, datatype, root, tag, comm, &status);
    if ( rc < 0 ) err = 1;
#ifndef NOSTOPCOLL
    if (rc < 0) return (rc);
#endif
  }
  
#ifndef NOSTOPCOLL
  if ( err ) 
    rc = MPI_ERR_OTHER;
  else
#endif
    rc = MPI_SUCCESS;
  
  return ( rc );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Reduce
Topology Used:		PI / linear fan-in
Topology handling:	none
Data handling:		non segmented
Buffer requirements:	root xN 
On failure:		report error
Description:		simple blocking recv fan-in
depending on 'op' we might not need all 
the buffering we use.
*/
int ftmpi_coll_intra_reduce_linear (void *sendbuf,void *recvbuf,int count,
				   MPI_Datatype datatype, MPI_Op op,
				   int root, MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int size;
  int tag;
  int ret;
  int err = 0;
  int bufid,len;
  char * buffer = NULL;
  MPI_Status status;
  MPI_User_function * func;
  int commute;
  int recvcount=0;
 
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );

  tag = USR_COLL_REDUCE;

  /* Get op-function */
  func = (MPI_User_function *) _atb_op_get(op);
  if(func == NULL) return ( MPI_ERR_INTERN);

  /* Check for invalid combinations of op and datatype */
  if ( op <_ATB_MPI_OP_B_CNT ) {
    (func)(NULL,NULL,&recvcount,&datatype);
    if ( _FTMPI_OP_ERRNO != MPI_SUCCESS ) 
      return ( _FTMPI_OP_ERRNO );
  }
  
  if(root == rank){
    commute = _atb_op_get_commute(op);

    ret = ftmpi_mpi_type_extent(datatype,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );

    bufid = get_msg_buf_of_size(count*size*ext,1,0);
    if(bufid == -1){
      return(bufid);
    }
    get_msg_buf_info(bufid,&buffer,&len);
    
    for(i=0;i<size;i++)
      {
	if(i!= root)
	  {
	    if ( i == 0 )
	      ret = ftmpi_mpi_recv(recvbuf,count,datatype,i,tag,comm,&status);
	    else
	      ret = ftmpi_mpi_recv(buffer,count,datatype,i,tag,comm,&status);
	    if(ret < 0) err = 1;
#ifndef NOSTOPCOLL
	    if(ret<0) {
	      free_msg_buf(bufid);
	      return(ret);
	    }
#endif
	  }
	else
	  {
	    if(sendbuf != MPI_IN_PLACE)
	      {
		if ( i == 0 )
		  ret = ftmpi_ddt_copy_ddt_to_ddt(sendbuf,datatype,count,
					     recvbuf,datatype,count);
		else
		  ret = ftmpi_ddt_copy_ddt_to_ddt(sendbuf,datatype,count,
					     buffer,datatype,count);

		if(ret < 0){
		  printf("[%d]: FTMPI error copying data:%d \n",root, ret);
		  err = 1;
#ifndef NOSTOPCOLL
		  free_msg_buf(bufid);
		  return(ret);
#endif
		}
	      }
	  }
	
	if( (func != NULL) && ( i != 0 ))
	  {
	    if (commute )
	      (func)(buffer,recvbuf,&count,&datatype);
	    else {
	      func (recvbuf, buffer, &count, &datatype);
	      ftmpi_ddt_copy_ddt_to_ddt(buffer,datatype,count,
				   recvbuf,datatype,count);
	    }
	  }
      }
    
    free_msg_buf(bufid);
  }
  else
    {
      if(sendbuf != MPI_IN_PLACE){
	ret = ftmpi_mpi_send(sendbuf,count,datatype,root,tag,comm);
      }
      else {
	ret = ftmpi_mpi_send(recvbuf,count,datatype,root,tag,comm);
      }
    if(ret < 0) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif 
    }
    }
  
#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else    
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Allreduce
Topology Used:		PI / linear fan-in fan-out
Topology handling:	none
Data handling:		non segmented
Buffer requirements:	root xN 
On failure:		report error
Description:		simple.. Reduce and then bcast
Must be replaced asap.
*/

int ftmpi_coll_intra_allreduce_linear(void *sendbuf,void *recvbuf,int count,
				     MPI_Datatype datatype, MPI_Op op,
				     MPI_Comm comm, int segsize)
{
  int rc;

  
  rc =  ftmpi_coll_intra_reduce_linear(sendbuf,recvbuf,count,datatype,op,0,
				       comm, segsize);
  if (rc<0) return (rc);

  rc = ftmpi_coll_intra_bcast_linear(recvbuf,count,datatype,0,
				     comm, segsize);
  return(rc);
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Allgather
Topology Used:			PI / linear fan-in fan-out
Topology handling:		none
Data handling:			full message no segmentation
Buffer requirements:		root = xN
On failure:			report error
Description:			very simple minded built on 
gather/bcast. No extra err checking.
*/

int ftmpi_coll_intra_allgather_linear (void *sbuf,int scnt,MPI_Datatype sddt,
				      void *rbuf,int rcnt,MPI_Datatype rddt,
				      MPI_Comm comm, int segsize)
{
  int ret;
  int root = 0;
  int size;
  
  size = ftmpi_com_size ( comm );
  if ( size < 0 ) return ( size );
  
  ret = ftmpi_coll_intra_gather_linear(sbuf,scnt,sddt,rbuf,rcnt,rddt,root,
				       comm,segsize);
  if(ret < 0) return(ret);
  
  ret = ftmpi_coll_intra_bcast_linear(rbuf,rcnt*size,rddt,root,comm,segsize);
  return(ret);
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
TYPE 	Allgatherv
Topology Used:			PI fan-in / fan out uses GV & bcast
Topology handling:
Data handling:
Buffer requirements:		at root.. xN
On failure:			returns error code
Description:	Basic / untuned

*/

int ftmpi_coll_intra_allgatherv_linear(void *sendbuf,int sendcount,
				       MPI_Datatype sendtype, void *recvbuf, 
				       int *recvcounts, int *displs,
				       MPI_Datatype recvtype, MPI_Comm comm, 
				       int segsize)
{
  int root = 0;
  int size;
  int ret;
  MPI_Datatype newtype;
  int tsize;
  MPI_Aint text;
  

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );

  ret = ftmpi_coll_intra_gatherv_linear(sendbuf,sendcount,sendtype,recvbuf,recvcounts,
					displs,recvtype,root,comm,segsize);
  if(ret != MPI_SUCCESS){
    printf("PROBLEM WITHIN MPI_ALLGATHERV WITH MPI_Gatherv %d\n",ret);
    return ret;
  }

  ret = ftmpi_mpi_type_indexed(size,recvcounts,displs,recvtype,&newtype);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_commit(&newtype);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_size(newtype,&tsize);
  if ( ret != MPI_SUCCESS ) return ( ret );
  ret = ftmpi_mpi_type_extent(newtype,&text);
  if ( ret != MPI_SUCCESS ) return ( ret );
  
  ret = ftmpi_coll_intra_bcast_linear(recvbuf,1,newtype,root,comm,segsize);
  if(ret != MPI_SUCCESS){
    printf("PROBLEM WITHIN MPI_ALLGATHERV WITH MPI_Bcast\n");
    return ret;
  }
  ret = ftmpi_mpi_type_free(&newtype);

  return ret;
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* The missing routines return */
/* were in a previous update and missed the CVS boat */

int ftmpi_coll_intra_alltoall_linear(void* sbuf,int scnt,MPI_Datatype sddt,
				    void* rbuf,int rcnt,MPI_Datatype rddt,
				    MPI_Comm comm, int segsize)
{
  int ret;
  int size,rank;
  int i,j;
  MPI_Aint exts,extr;
  int tag;
  int err = 0;
  MPI_Status status;
  
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
    
  ret = ftmpi_mpi_type_extent(rddt,&extr);
  if ( ret != MPI_SUCCESS ) return ( ret );

  ret = ftmpi_mpi_type_extent(sddt,&exts);
  if ( ret != MPI_SUCCESS ) return ( ret );
  
  for(i=0;i<size;i++)
    {
      if(i == rank)
	{
	  for(j=0;j<size;j++)
	    {
	      if(j != rank)
		{
		  tag = USR_COLL_ALLTOALL;
		  ret = ftmpi_mpi_send((char *)sbuf+(j*scnt*exts),scnt,sddt,
				       j,tag,comm);
		  if(ret < 0) {
		    err = 1;
#ifndef NOSTOPCOLL
		    return(ret);
#endif
		  }
		}
	      else 
		{
		  ret = ftmpi_ddt_copy_ddt_to_ddt((char *)sbuf+(j*scnt*exts),sddt,
					     scnt,(char *)rbuf+(i*rcnt*extr),
					     rddt,rcnt);
		  if(ret < 0){
		    printf("INTRA_ALLREDUCE_LINEAR: ddt_copy returned %d \n",ret);
		    err = 1;
#ifndef NOSTOPCOLL
		    return(ret);
#endif
		  }
		}
	    }
	}
      else {  /* RECEIVE FROM THE CURRENT PROCESS */
	tag = USR_COLL_ALLTOALL;
	ret = ftmpi_mpi_recv((char *)rbuf+(i*rcnt*extr),rcnt,rddt,i,
			     tag,comm,&status);
	if(ret < 0) { 
	  err = 1;
#ifndef NOSTOPCOLL
	  return ( ret );
#endif
	}
      }
  }
  
#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
TYPE 	NAME
Topology Used:
Topology handling:
Data handling:
Buffer requirements:
On failure:
Description:
*/
int ftmpi_coll_intra_alltoallv_linear(void* sbuf,int *scnt,int *sdisp,
				     MPI_Datatype sddt, void* rbuf,int *rcnt,
				     int *rdisp,MPI_Datatype rddt, 
				     MPI_Comm comm, int segsize)
{
  int ret;
  int size,rank;
  int i,j;
  MPI_Aint exts,extr;
  int tag;
  int err = 0;
  MPI_Status status;
  
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
  
  ret = ftmpi_mpi_type_extent(rddt,&extr);
  if ( ret != MPI_SUCCESS ) return ( ret );

  ret = ftmpi_mpi_type_extent(sddt,&exts);
  if ( ret != MPI_SUCCESS ) return ( ret );
  
  for(i=0;i<size;i++){
    if(i == rank){           /* IT IS MY TURN AND I NEED TO SEND TO ALL */
      for(j=0;j<size;j++){
        if(j != rank){
          tag = USR_COLL_ALLTOALLV;
          ret = ftmpi_mpi_send((char *)sbuf+(sdisp[j]*exts),scnt[j],sddt,j,tag,comm);
          if(ret < 0) {
	    err = 1;
#ifndef NOSTOPCOLL
	    return(ret);
#endif
	  }
	}
	else {
	  ret = ftmpi_ddt_copy_ddt_to_ddt((char *)sbuf+(sdisp[j]*exts),sddt,
					  scnt[j],(char *)rbuf+(rdisp[j]*extr),
					  rddt,rcnt[j]);
	  if(ret < 0){
	    printf("ALLTOALLV_LINEAR: ddt_copy returned: %d \n",ret);
	    err = 1;
#ifndef NOSTOPCOLL
	    return(ret);
#endif
	  }
	}
      }
    }
    else {
      tag = USR_COLL_ALLTOALLV;
      ret = ftmpi_mpi_recv((char *)rbuf+(rdisp[i]*extr),rcnt[i],rddt,i,tag,
			   comm,&status);
      if(ret < 0) {
	err = 1;
#ifndef NOSTOPCOLL
	return(ret);
#endif
      }
    }
  }
 
#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER ;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Gather
Topology Used:			PI / linear fan-in
Topology handling:		none
Data handling:			no segmenting
Buffer requirements:		non root MS, root (N-1)MS
On failure:			report error
Description:			Simple fan-in 
*/
int ftmpi_coll_intra_gather_linear (void *sbuf, int scnt, MPI_Datatype sddt, 
				   void *rbuf, int rcnt, MPI_Datatype rddt, 
				   int root, MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int size;
  int tag;
  int ret;
  int err = 0;
  MPI_Status status;
  
  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
  
  if(root == rank){
    ret = ftmpi_mpi_type_extent(rddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );

    for(i=0;i<size;i++){
      if(i!= root){
        tag = USR_COLL_GATHER+i; 
        ret = ftmpi_mpi_recv((char *)rbuf+(i*rcnt*ext),rcnt,rddt,i,tag,comm,&status);
        if(ret < 0) err = 1;
#ifndef NOSTOPCOLL
        if(ret<0) return(ret);
#endif
      }
      else {
        if(sbuf != MPI_IN_PLACE){
          ret = ftmpi_ddt_copy_ddt_to_ddt(sbuf,sddt,scnt,(char *)rbuf+(i*rcnt*ext),
				     rddt,rcnt);
          if(ret < 0){
            printf("GATHER_LINEAR: ddt_copy returned :%d \n",ret);
	    err = 1;
#ifndef NOSTOPCOLL
            return(ret);
#endif
          }
        }
      }
    }
    /*    if(err)return(MPI_ERR_OTHER); */
  }
  else{
    /* tag = USR_COLL + rank; */
    tag = USR_COLL_GATHER+rank; 
    if(sbuf != MPI_IN_PLACE){
      ret = ftmpi_mpi_send(sbuf,scnt,sddt,root,tag,comm);
    }
    else {
      ret = ftmpi_mpi_send(rbuf,rcnt,rddt,root,tag,comm);
    }
    if ( ret < 0 ) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif
    }
  }

#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Gatherv
Topology Used:		PI / linear fan-in
Topology handling:	nope	
Data handling:		non segmented
Buffer requirements:	xN-1 for root
On failure:		report error
Description:		simple minded V version of gather above
*/
int ftmpi_coll_intra_gatherv_linear (void *sbuf,int scnt, MPI_Datatype sddt,
				    void *rbuf, int *rcnt, int *rdisp, 
				    MPI_Datatype rddt,int root,
				    MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int size;
  int tag;
  int ret;
  int err = 0;
  MPI_Status status;

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
    
  if(root == rank){
    ret = ftmpi_mpi_type_extent(rddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );

    for(i=0;i<size;i++){
      if(i!= root){
        tag = USR_COLL_GATHER;
        ret = ftmpi_mpi_recv((char *)rbuf+(rdisp[i]*ext),rcnt[i],rddt,i,tag,
			     comm,&status);
        if(ret < 0) {
	  err =1;
#ifndef NOSTOPCOLL
	  return(ret);
#endif
	}
      }
      
      else {
        if(sbuf != MPI_IN_PLACE){
          ret = ftmpi_ddt_copy_ddt_to_ddt(sbuf,sddt,scnt,
				     (char *)rbuf+(rdisp[i]*ext),rddt,
				     rcnt[i]);
          if(ret < 0){
            printf("GATHERV_LINEAR: ddt_copy returned %d \n",ret);
	    err = 1;
#ifndef NOSTOPCOLL
            return(ret);
#endif
          }
        }
      }
    }
  }
  else{
    tag = USR_COLL_GATHER;
    ret = ftmpi_mpi_type_extent(rddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );

    if(sbuf != MPI_IN_PLACE){
      ret = ftmpi_mpi_send(sbuf,scnt,sddt,root,tag,comm);
    }
    else {
      ret = ftmpi_mpi_send((char *)rbuf+(rdisp[rank]*ext),rcnt[rank],rddt,root,
		     tag,comm);
    }
    if ( ret < 0 ) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif
    }
  }

#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
TYPE 	Reduce_scatter
Topology Used: 
Topology handling:
Data handling:
Buffer requirements:
On failure:
Description:
*/
int ftmpi_coll_intra_reducescatter_linear(void * sbuf,void * rbuf,int *counts,
					 MPI_Datatype ddt,MPI_Op op, 
					 MPI_Comm comm, int segsize)
{
  int size;
  int rank;
  int sum = 0;
  int ret;
  int i;
  int disp_id,len;
  int * disp = NULL;
  MPI_Aint ext;
  char * tmp_buf=NULL;
  int totalcount = 0;


  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );

  ret = ftmpi_mpi_type_extent ( ddt, &ext );
  if ( ret != MPI_SUCCESS ) return ( ret );

  for ( i = 0; i < size; i ++ )
    totalcount += counts[i];

  if ( rank == 0 )
    {
      tmp_buf = ( char *)_MALLOC( totalcount * ext );
      if ( tmp_buf == NULL ) return ( MPI_ERR_INTERN );
    }

  ret = ftmpi_coll_intra_reduce_linear (sbuf,tmp_buf,totalcount,
					ddt,op,0,comm,segsize );
  if(ret == MPI_SUCCESS){
    disp_id = get_msg_buf_of_size(size*sizeof(int),1,0);
    if(disp_id == -1){
      return(disp_id);
    }
    get_msg_buf_info(disp_id,(char **) &disp,&len);

    sum = counts[0]; 
    disp[0] = 0;
    for(i=1;i<size;i++){
      disp[i] = sum;
      sum += counts[i];
      }

    ret = ftmpi_coll_intra_scatterv_linear (tmp_buf, counts, disp, ddt, rbuf, 
					    counts[rank], ddt,0,comm,segsize);
    
    free_msg_buf(disp_id);
    if ( rank == 0 )
      _FREE( tmp_buf );
  }

  return(ret);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Scatter
Topology Used:			PI / Linear fan-out
Topology handling:		none
Data handling:			non segmented
Buffer requirements:		
On failure:			report error
Description:			single fan-out of data using blocking 
sends. no optimisations
*/
int ftmpi_coll_intra_scatter_linear (void *sbuf,int scnt,MPI_Datatype sddt,
				    void *rbuf,	int rcnt,MPI_Datatype rddt,
				    int root,MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int tsize;
  int size;
  int tag;
  int ret;
  int err = 0;
  MPI_Status status;

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
    
  if(root == rank){
    ret = ftmpi_mpi_type_extent(sddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );
  
    ret = ftmpi_mpi_type_size(sddt,&tsize);
    if ( ret != MPI_SUCCESS ) return ( ret );

    for(i=0;i<size;i++){
      if(i!= root){
        tag = USR_COLL_SCATTER;
        ret = ftmpi_mpi_send((char *)sbuf+(i*scnt*ext),scnt,sddt,i,tag,comm);
        if(ret < 0) {
	  err =1;
#ifndef NOSTOPCOLL
	  return(ret);
#endif
	}
      }
      else {
        if(rbuf != MPI_IN_PLACE){
          ret = ftmpi_ddt_copy_ddt_to_ddt((char *)sbuf+(i*scnt*ext),sddt,scnt,
				     rbuf,rddt,rcnt);
          if(ret < 0){
            printf("SCATTER_LINEAR: ddt_copy returned %d\n",ret);
	    err = 1;
#ifndef NOSTOPCOLL
            return(ret);
#endif
          }
        }
      }
    }
  }
  else{
    tag = USR_COLL_SCATTER;
    ret = ftmpi_mpi_recv(rbuf,rcnt,rddt,root,tag,comm,&status);
    if ( ret < 0 ) {
      err = 1;
#ifndef NOSTOPCOLL
    return(ret);
#endif
    }
  }

#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Scatterv
Topology Used:			PI / Linear fan-out
Topology handling:		none
Data handling:			non segmented
Buffer requirements:		
On failure:			report error
Description:			single fan-out of V data using blocking 
sends. no optimisations
*/
int ftmpi_coll_intra_scatterv_linear (void *sbuf,int * scnt,int * sdisp,
				     MPI_Datatype sddt, void *rbuf,int rcnt,
				     MPI_Datatype rddt,int root, 
				     MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int size;
  int tag;
  int ret;
  int err = 0;
  MPI_Status status;


  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );
    
  if(root == rank){
    ret = ftmpi_mpi_type_extent(sddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );

    for(i=0;i<size;i++){
      if(i!= root){
        tag = USR_COLL_SCATTER;
        ret = ftmpi_mpi_send((char *)sbuf+(sdisp[i]*ext),scnt[i],sddt,i,tag,comm);
        if(ret < 0) {
	  err =1;
#ifndef NOSTOPCOLL
          return(ret);
#endif
	}
      }
      else {
        if(rbuf != MPI_IN_PLACE){
          ret = ftmpi_ddt_copy_ddt_to_ddt((char *)sbuf+(sdisp[i]*ext),sddt,
				     scnt[i],rbuf,rddt,rcnt);
          if(ret < 0){
            printf("SCATTERV_LINEAR: ddt_copy returned %d \n",ret);
#ifndef NOSTOPCOLL
            return(ret);
#endif
          }
        }
      }
    }
  }
  else{
    tag = USR_COLL_SCATTER;
    ret = ftmpi_mpi_recv(rbuf,rcnt,rddt,root,tag,comm,&status);
    if ( ret <  0 ) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif
    }
  }

#ifndef NOSTOPCOLL
  if ( err ) 
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/*
BASIC 	Scan
Topology Used:			PI static / linear fan-in fan-out
Topology handling:		none
Data handling:			non segmented
Buffer requirements:		cnt*size*ext (xN basically)
On failure:			report error
Description:			needs replacing
*/
int ftmpi_coll_intra_scan_linear (void * sbuf,void * rbuf,int cnt,
				 MPI_Datatype ddt, MPI_Op op,
				 MPI_Comm comm, int segsize)
{
  int i;
  int rank;
  MPI_Aint ext;
  int size;
  int tag;
  int ret;
  int err = 0;
  int root = 0;
  int bufidA,lenA;
  int bufidB,lenB;
  char * bufferA = NULL;
  char * bufferB = NULL;
  MPI_Status status;
  MPI_User_function * func;
  int recvcount = 0;

  size = ftmpi_com_size (comm);
  if ( size < 0 ) return ( size );
  rank = ftmpi_com_rank ( comm );
  if ( rank < 0 )  return ( rank );


  /* Get op-function */
  func = (MPI_User_function *) _atb_op_get(op);
  if(func == NULL) return ( MPI_ERR_INTERN);

  /* Check for invalid combinations of op and datatype */
  if ( op < _ATB_MPI_OP_B_CNT ) {
    (func)(NULL,NULL,&recvcount,&ddt);
    if ( _FTMPI_OP_ERRNO != MPI_SUCCESS ) 
      return ( _FTMPI_OP_ERRNO );
  }

  if(root == rank){
    ret = ftmpi_mpi_type_extent(ddt,&ext);
    if ( ret != MPI_SUCCESS ) return ( ret );
    
    bufidA = get_msg_buf_of_size(cnt*size*ext,1,0);
    bufidB = get_msg_buf_of_size(cnt*size*ext,1,0);
    
    if(bufidA == -1){
      return(bufidA);
    }
    if(bufidB == -1){
      return(bufidB);
    }
    get_msg_buf_info(bufidA,&bufferA,&lenA);
    get_msg_buf_info(bufidB,&bufferB,&lenB);
    
    if(sbuf != MPI_IN_PLACE){
      ret = ftmpi_ddt_copy_ddt_to_ddt(sbuf,ddt,cnt,rbuf,ddt,cnt);
      if(ret < 0){
        printf("SCAN LINEAR: ddt_copy returned %d \n",ret);
	err = 1;
#ifndef NOSTOPCOLL
	free_msg_buf(bufidA);
	free_msg_buf(bufidB);
        return(ret);
#endif
      }
    }
    
    /*     memcpy(bufferB,rbuf,cnt*ext); */
    ret = ftmpi_ddt_copy_ddt_to_ddt(rbuf,ddt,cnt,bufferB,ddt,cnt);
    if(ret < 0){
      printf("SCAN LINEAR: ddt_copy returned %d \n",ret);
      err = 1;
#ifndef NOSTOPCOLL
      free_msg_buf(bufidA);
      free_msg_buf(bufidB);
      return(ret);
#endif
    }
    for(i=0;i<size;i++){
      if(i!= root){
        tag = USR_COLL_SCAN;
        ret = ftmpi_mpi_recv(bufferA,cnt,ddt,i,tag,comm,&status);
	
        if(ret < 0) {
	  err = 1;
#ifndef NOSTOPCOLL
          free_msg_buf(bufidA);
          free_msg_buf(bufidB);
          return(ret);
#endif
        }
        if(func != NULL){
          (func)(bufferB,bufferA,&cnt,&ddt);
          ret = ftmpi_mpi_send(bufferA,cnt,ddt,i,tag,comm);
	  if ( ret < 0 ) {
	    err = 1;
#ifndef NOSTOPCOLL
	    free_msg_buf(bufidA);
	    free_msg_buf(bufidB);
	    return ( ret );
#endif
	  }
 
	  ret = ftmpi_ddt_copy_ddt_to_ddt(bufferA,ddt,cnt,bufferB,ddt,cnt);
	  if(ret < 0){
	    printf("SCAN LINEAR: ddt_copy returned %d \n",ret);
	    err = 1;
#ifndef NOSTOPCOLL
	    free_msg_buf(bufidA);
	    free_msg_buf(bufidB);
	    return(ret);
#endif
	  }
        }
      }
    }
    free_msg_buf(bufidA);
    free_msg_buf(bufidB);
  }
  else{
    tag = USR_COLL_SCAN;
    if(sbuf != MPI_IN_PLACE){
      ret = ftmpi_mpi_send(sbuf,cnt,ddt,root,tag,comm);
    }
    else {
      ret = ftmpi_mpi_send(rbuf,cnt,ddt,root,tag,comm);
    }
    if(ret < 0) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif
    }

    ret = ftmpi_mpi_recv(rbuf,cnt,ddt,root,tag,comm,&status);
    if(ret < 0) {
      err = 1;
#ifndef NOSTOPCOLL
      return(ret);
#endif
    }
  }

#ifndef NOSTOPCOLL
  if ( err )
    ret = MPI_ERR_OTHER;
  else
#endif
    ret = MPI_SUCCESS;
  
  return ( ret );
}




