/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This is a list of misc function calls for those little used but still
 needed MPI calls.

 Graham Stuttgart 2002
*/

#include <unistd.h>	/* needed by gethostname */
#include <string.h>	/* needed for strncpy */
#include <time.h>	/* needed by clock_getres */

#include <math.h>	
/* an eg idea */

#include "ft-mpi-lib.h"

#ifdef HAVE_PRAGMA_WEAK

#    pragma weak  MPI_Get_processor_name   = PMPI_Get_processor_name
#    pragma weak  MPI_Wtime                = PMPI_Wtime
#    pragma weak  MPI_Wtick                = PMPI_Wtick
#    pragma weak  MPI_Get_version          = PMPI_Get_version
#    pragma weak  MPI_Pcontrol             = PMPI_Pcontrol
#endif

#    define MPI_Get_processor_name    PMPI_Get_processor_name
#    define MPI_Wtime                 PMPI_Wtime
#    define MPI_Wtick                 PMPI_Wtick
#    define MPI_Get_version           PMPI_Get_version
#    define MPI_Pcontrol              PMPI_Pcontrol


/* ************************************************************* */

/* Get processor name just returns hostname in this version */

int MPI_Get_processor_name(char *name, int *resultlen)
{
/* ok we must have a name upto length MPI_MAX_PROCESSOR_NAME */
char tmp[MPI_MAX_PROCESSOR_NAME];
int rl; /* real length */
 int i;

if (!name) {
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
	}

if (!resultlen) {
	RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
	}

i = gethostname (tmp, MPI_MAX_PROCESSOR_NAME);

rl = strlen (tmp);

strncpy (name, tmp, rl);

if (rl<MPI_MAX_PROCESSOR_NAME) {
	*resultlen = rl;
	name[rl]='\0';
	return (MPI_SUCCESS);
	}
else { /* no room to put 'null str term' */
	*resultlen = rl-1;
	name[rl-1]='\0';
	return (MPI_SUCCESS);
	}	
}

/*
MPI_Get_version
*/

int MPI_Get_version(
               int *version,
               int *subversion )
{
if (!version) {
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
		}
else 
	*version = MPI_VERSION;

if (!subversion) {
		RETURNERR (MPI_COMM_WORLD, MPI_ERR_ARG);
		}
else
	*subversion = MPI_SUBVERSION;

return (MPI_SUCCESS);
}

/* 
MPI_Wtime
*/

double MPI_Wtime ()
{
struct timeval tp;
struct timezone tzp;
double sec=0.0;
double psec=0.0;

gettimeofday (&tp, &tzp);
sec = (double)tp.tv_sec;
/* psec = ((double)tp.tv_usec)/1000000.0; */
psec = ((double)tp.tv_usec)/((double)1000000.0);

return (sec+psec);
}


/*
MPI_Wtick

here we rely on posix calls being available
Which might be bad. GEF STR 2002
*/
double MPI_Wtick()
{
#ifdef CLOCK_GETRES 
  struct timespec res;
  int rc;
  double v;

  rc = clock_getres (CLOCK_REALTIME, &res);
/* get res as timespec */

  /* convert to seconds */
  v = ((double)res.tv_sec) + (((double)res.tv_nsec)/((double)1000000000.0));
  return (v);

#else
  return (0.0001); /* bad, just to keep it working */
  
#endif /* CLOCK_GETRES */
}


int MPI_Pcontrol( const int level, ... )
{
  return (MPI_SUCCESS);
}

