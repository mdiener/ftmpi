
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:	
              Edgar Gabriel <egabriel@cs.utk.edu>
	      George Bosilca <bosilca@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/
/*
  This file implements all routines needed for determining the own
  data representation, publishing it and check settings for other processes.
*/

#include <stdio.h>
#include <float.h>
#include <string.h>
#include "mpi.h"
#include "ft-mpi-ddt-sys.h" 
#include "ft-mpi-convinfo.h"


int ftmpi_convinfo_mysettings ( unsigned int *me )
{
  ftmpi_convinfo_initialize_var ( me); 
  if ( sizeof(long) == 8 )
    ftmpi_convinfo_setmask (me, FTMPI_LONGIS64);
  
  if ( sizeof(long double) == 12 )
    ftmpi_convinfo_setmask ( me, FTMPI_LONGDOUBLEIS96);
  else if ( sizeof(long double) == 16 )
    ftmpi_convinfo_setmask ( me, FTMPI_LONGDOUBLEIS128);

  if ( ftmpi_convinfo_isbigendian ())
    ftmpi_convinfo_setmask ( me, FTMPI_ISBIGENDIAN);


  if ( LDBL_MAX_EXP == 16384 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDEXPSIZEIS15 );


  if ( LDBL_MANT_DIG == 64 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDMANTDIGIS64);
  else if ( LDBL_MANT_DIG == 105 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDMANTDIGIS105); 
  else if ( LDBL_MANT_DIG == 106 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDMANTDIGIS106); 
  else if ( LDBL_MANT_DIG == 107 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDMANTDIGIS107); 
  else if ( LDBL_MANT_DIG == 113 )
    ftmpi_convinfo_setmask ( me, FTMPI_LDMANTDIGIS113); 

  if ( ftmpi_convinfo_ldisintel () )
    ftmpi_convinfo_setmask ( me, FTMPI_LDISINTEL );

  /*   ftmpi_convinfo_bitdump ("me", (char *) me, sizeof(me));  */


  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_convinfo_setabit ( unsigned int* var, int pos )
{
  unsigned int mask=0x01;

  mask  = mask << (pos-1);
  *var |= mask;
  
  /*  bitdump ( "var", (char *) var, sizeof (*var));  */

  return ( MPI_SUCCESS );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_convinfo_setmask ( unsigned int *var, unsigned int mask)
{
  *var |= mask;
  return ( 0 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Check whether a certain flag/mask is set in the integer */
int ftmpi_convinfo_checkmask ( unsigned int *var, unsigned int mask )
{
  unsigned int tmpvar;

  /* Check whether the headers are set correctly,
     or whether this is an erroneous integer */
  if ( !(*var&FTMPI_HEADERMASK) )
    {
      if ( *var&FTMPI_HEADERMASK2 )
	{
	   FTMPI_DDT_mem_CP_BS_vars 
	    /* Both ends of this integer have the wrong settings,
               maybe its just the wrong endian-representation. Try
               to swap it and check again. If it looks now correct,
	       keep this version of the variable 
	    */

	  /* printf("both ends are wrong, lets try to shift it\n"); */
	  dt_dest = (char *) &tmpvar;
	  dt_src  = (char *) var;
	  dt_type = 4;
	  dt_size = 4;
	  dt_mode = -1;
	  FTMPI_DDT_mem_COPY_BSWAP (dt_dest, dt_src, dt_size, dt_type,dt_mode);
	  if ( (tmpvar&FTMPI_HEADERMASK) && (!(tmpvar&FTMPI_HEADERMASK2)) ) 
	    {
	      *var = tmpvar;
	      printf("swapping variable to have proper ordering\n");
	    }
	  else
	    {
	      printf("FTMPI CONVINFO CHECK MASK integer does not contain correct headers\n");
	      MPI_Abort ( MPI_COMM_WORLD, 1 );
	      } 
	}
      else
	{
	  printf("integer does not contain correct headers\n");
	  exit ( -1 );
	}
    }
    
  /* Here is the real evaluation of the bitmask */

  if ( (*var&mask) == mask )
    return ( 1 );
  else
    return ( 0 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Initialize a variable setting the header and
   the unused elements of the integer 
*/
int ftmpi_convinfo_initialize_var ( unsigned int *var)
{
  *var = 0x0;
  *var |= FTMPI_HEADERMASK; 
  *var |= FTMPI_UNUSEDMASK; 

  return ( 0 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Debug routine for see every single bit */
void ftmpi_convinfo_bitdump ( char * msg, char *c_ptr, int len )
{
  unsigned char mask=0x01;
  unsigned cbyte, i;

  printf ( "\n  bitdump for %s\n", msg );
   
  while ( len > 0 )
    {
      mask = 0x01;

      cbyte = (unsigned char) *c_ptr;;
      for ( i = 0; i < 8; i++)
	{
	  if ( mask & cbyte )
	    printf("1");
	  else
	    printf("0");
      
	  mask = mask << 1;
	}
      printf (" ");
      c_ptr++;
      len --;
    }
  printf("\n");
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void ftmpi_convinfo_hexdump ( char *msg,  char *c_ptr, int len )
{
   printf ( "\n  hexdump for %s\n", msg );
   while ( len > 0 )
     {
       printf ( " %2.2x", (0xff&(int)*c_ptr++));
       len--;
     }
   printf ( "\n" );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_convinfo_isbigendian ( void )
{
  const unsigned int value=0x12345678;
  const char *ptr = (char*)&value;
  int x=0;

  if ( sizeof(int) == 8 )
    x = 4;
      
  if ( ptr[x] == 0x12)
    return ( 1 ); /* big endian, true */
  else if ( ptr[x] == 0x78 )
    return ( 0 ); /* little endian, false */
  else
    printf ("Unkwon endia type ! What shall I ?\n");
  
  
  return ( -1 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* we must find what representation of long double is used
  * intel or sparc. For that first set the least significant bit
  * from the exponent to zero and then check for the first bit
  * from the declared significand length. On intel architectures
  * we should find the explicit bit of the mantissa (that should be
  * 1 as we have well choose the long double number). On sparc we
  * find the last significand bit from the exponent (and it's 0
  * because we have set it to 0 in the exponenet). QED
  */
int ftmpi_convinfo_ldisintel ()
{
 long double ld = 2.0;
 int i, j;
 unsigned int* pui = (unsigned int *) &ld;

 j = LDBL_MANT_DIG / 32;
 i = (LDBL_MANT_DIG % 32) - 1;
 if( ftmpi_convinfo_isbigendian() ) { /* big endian */
   j = (sizeof(long double) / sizeof(unsigned int)) - j;
   if( i < 0 ) {
     i = 31;
     j = j+1;
   }
 } else {
   if( i < 0 ) {
     i = 31;
     j = j-1;
   }
 }
 return (pui[j] & (1 << i) ? 1 : 0);
}

