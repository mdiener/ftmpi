
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
            Antonin Bukovsky <tone@cs.utk.edu>
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 Original original code was the comm_info X11 GUI based on the MPE wrappers

 This is a Tone modified example, that Graham has now turned into a library 
 that CAN be called from within the FT-MPI RTS.
*/

/*
 Although the notifier library would be the ideal place to handle the GUI
 update messages, it does not know anything about ranks! only global IDs
 (afterall its a HARNESS plug-in and is meant to be simple).

 So this library should only be called by the 'leader' from ft-mpi-sys during
 the following:

 (1) startup
 (2) failure detection
 (3) respawning
 (4) reordering of a communicator
 (5) finalise
 (6) MPI_Abort

 Graham Oct-Nov 2002 UTK
*/




#include "ft-mpi-cinfo.h"
#include "snipe_lite.h"
#include "debug.h"


int cinfo_s = -1;
int cinfohost = -1;
int cinfoport = -1;

/**********************************************************************************/
#ifdef wheniwasatstandalonedemobytone
int main(int argc,char ** argv)
{
  int i;
  int commid1 = 123;
  int commid2 = 124;
  int numprocs = 128;
  int active = 1;
  int state = 0;
  char hostname[MAXHOSTNAMELEN];
  int * pid = NULL;
  int * pstate = NULL;

  sprintf(hostname,"MY APPLICATION");


  pid = (int *)_MALLOC(sizeof(int)*numprocs*2);
  pstate = (int *)_MALLOC(sizeof(int)*numprocs*2);
  for(i=0;i<numprocs*2;i++){
    pid[i] = 100+i;
    pstate[i] = 0;
  }

  printf("COMMINFO_TEST is running \n");
  if(ft_mpi_cinfo_find() == 0){
    for(i=123;i<130;i++){
      req_finalize_comm(i);
    }
  
    req_new_comm(commid1,numprocs,active,state,hostname,pid,pstate);
    req_new_comm(commid2,numprocs*2,active,state,hostname,pid,pstate);
    sleep(3);
    for(i=0;i<numprocs;i++){
      req_proc_state(commid1,i,i%4);
    }
    sleep(5);
    req_finalize_comm(commid1);
    req_finalize_comm(commid2);
  }
  else 
    printf("\n!!! COMMINFO GUI does not exist !!!\n\n");

  printf("COMMINFO_TEST is exiting \n");
  return 1;
}
#endif
/**********************************************************************************/

int ft_mpi_cinfo_find()
{
  int ret;
  int gid;
  int nmembers;
  int mid;
  char gname[] = "FT-MPI:Monitor:comminfo";

  init_msg_bufs();



  ns_init(getenv("HARNESS_NS_HOST"),atoi(getenv("HARNESS_NS_HOST_PORT")));
  ret = ns_info(gname,1,&gid,&nmembers,&mid,&cinfohost,&cinfoport);
  ns_close();
  if(ret != 1){
    return(-1);
  }
  printf("FOUND COMMINFO ON %x %d\n",cinfohost,cinfoport);
  /* OK for test from home */
  /* cinfohost = machine at kevins */
  /* cinfohost=0xE3E6D618; */


  return 0;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
int ft_mpi_cinfo_mkconn()
{
  int max_try = 10;
  cinfo_s = -1;
  if(cinfohost != -1 && cinfoport != -1){
    while(cinfo_s <= 0 && max_try > 0){
      cinfo_s = getconn_addr(cinfohost,&cinfoport,1);
    }
    return cinfo_s;
  }
  return 0;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_close_conn()
{
  closeconn(cinfo_s);
  cinfo_s = -1;
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_req_new_comm(int commid,int numprocs,int active,int state,char * hostname,int * pid,int * pstate)
{
  int i;
  int buf_id;
  int from;
  int ntag = 1;
  int tag = NEWCOMM;
  int ret;

  if(ft_mpi_cinfo_mkconn() > 0){
    buf_id = get_msg_buf(1);
    printf("PROCESSING NEWCOM() %d\n",numprocs);
    pk_int32(buf_id, &commid, 1);
    pk_int32(buf_id, &numprocs, 1);
    pk_int32(buf_id, &active, 1);
    pk_int32(buf_id, &state , 1);
    pk_string(buf_id,hostname);
  
    for (i=0;i<numprocs;i++) {
      pk_int32(buf_id, &pid[i], 1);
      pk_int32(buf_id, &pstate[i], 1);
    }

    ret = send_pkmesg(cinfo_s,123,1,&tag,buf_id,1);
    ret = recv_pkmesg(cinfo_s,&from,&ntag,&tag,&buf_id);
  }
  ft_mpi_cinfo_close_conn();
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_req_com_state(int commid,int numprocs,int active,int state)
{
  int buf_id;
  int from;
  int ntag = 1;
  int tag = COMSTATE;
  int ret;

  if(ft_mpi_cinfo_mkconn() > 0){
    buf_id = get_msg_buf(1);
    printf("PROCESSING COMSTATE()\n");

    pk_int32 (buf_id, &commid, 1);
    pk_int32 (buf_id, &numprocs, 1);
    pk_int32 (buf_id, &active , 1);
    pk_int32 (buf_id, &state , 1);

    ret = send_pkmesg(cinfo_s,123,1,&tag,buf_id,1);
    ret = recv_pkmesg(cinfo_s,&from,&ntag,&tag,&buf_id);
  }
  ft_mpi_cinfo_close_conn();
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_req_proc_state(int commid,int rank,int pstate)
{
  int buf_id;
  int from;
  int ntag = 1;
  int tag = PROCSTATE;
  int ret;

  if(ft_mpi_cinfo_mkconn() > 0){
    buf_id = get_msg_buf(1);
    printf("PROCESSING PROCSTATE()\n");

    pk_int32 (buf_id, &commid, 1);
    pk_int32 (buf_id, &rank, 1);
    pk_int32 (buf_id, &pstate, 1);

    ret = send_pkmesg(cinfo_s,123,1,&tag,buf_id,1);
    ret = recv_pkmesg(cinfo_s,&from,&ntag,&tag,&buf_id);
  }
  ft_mpi_cinfo_close_conn();
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_req_update_com(int commid,int numprocs,int active,int state,int * pid,int * pstate)
{
  int i;
  int buf_id;
  int from;
  int ntag = 1;
  int tag = UPDATECOM;
  int ret;

  if(ft_mpi_cinfo_mkconn() > 0){
    buf_id = get_msg_buf(1);
    printf("PROCESSING UPDATECOM()\n");

    pk_int32 (buf_id, &commid, 1);
    pk_int32 (buf_id, &numprocs, 1);
    pk_int32 (buf_id, &active, 1);
    pk_int32 (buf_id, &state , 1);


    for (i=0;i<numprocs;i++) {
      pk_int32 (buf_id, &pid, 1);
      pk_int32 (buf_id, &pstate, 1);
    }



    ret = send_pkmesg(cinfo_s,123,1,&tag,buf_id,1);
    ret = recv_pkmesg(cinfo_s,&from,&ntag,&tag,&buf_id);
  }
  ft_mpi_cinfo_close_conn();
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/
void ft_mpi_cinfo_req_finalize_comm(int commid)
{
  int buf_id;
  int from;
  int ntag = 1;
  int tag = FINALIZE;
  int ret;

  if(ft_mpi_cinfo_mkconn() > 0){
    buf_id = get_msg_buf(1);
    printf("PROCESSING FINALIZE()\n");

    pk_int32 (buf_id, &commid, 1);

    ret = send_pkmesg(cinfo_s,123,1,&tag,buf_id,1);
    ret = recv_pkmesg(cinfo_s,&from,&ntag,&tag,&buf_id);
  }
  ft_mpi_cinfo_close_conn();
}
/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/

/**********************************************************************************/
/**********************************************************************************/
/**********************************************************************************/

