/*
  HARNESS G_HCORE
  HARNESS FT_MPI

  Innovative Computer Laboratory,
  University of Tennessee,
  Knoxville, TN, USA.

  harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
      Graham E Fagg    <fagg@cs.utk.edu>
      Antonin Bukovsky <tone@cs.utk.edu>
      Edgar Gabriel    <egabriel@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_F2C_DDT
#define _FT_MPI_H_F2C_DDT

#include "../include/mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"


#define HDR     "FTMP:f2c:"
#define NOPHDR  "FTMPI:f2c:NOP:"


/* defs that we might need one day (on a Cray somewhere probably) */
#ifdef POINTER_64_BITS
extern void *ToPtr();
extern int FromPtr();
extern void RmPtr();
#else
#define ToPtr(a) (a)
#define FromPtr(a) (int)(a)
#define RmPtr(a)
#endif

/* Modification to handle the Profiling Interface
   January 8 2003, EG */

#ifdef HAVE_PRAGMA_WEAK

#  ifdef FORTRANCAPS

#    pragma weak MPI_PACK                  = mpi_pack_
#    pragma weak PMPI_PACK                 = mpi_pack_ 
#    pragma weak MPI_PACK_SIZE             = mpi_pack_size_
#    pragma weak PMPI_PACK_SIZE            = mpi_pack_size_ 
#    pragma weak MPI_UNPACK                = mpi_unpack_
#    pragma weak PMPI_UNPACK               = mpi_unpack_ 
#    pragma weak MPI_ADDRESS               = mpi_address_
#    pragma weak PMPI_ADDRESS              = mpi_address_ 
#    pragma weak MPI_GET_COUNT             = mpi_get_count_
#    pragma weak PMPI_GET_COUNT            = mpi_get_count_ 
#    pragma weak MPI_GET_ELEMENTS          = mpi_get_elements_
#    pragma weak PMPI_GET_ELEMENTS         = mpi_get_elements_ 
#    pragma weak MPI_TYPE_LB               = mpi_type_lb_	
#    pragma weak PMPI_TYPE_LB              = mpi_type_lb_
#    pragma weak MPI_TYPE_UB               = mpi_type_ub_	
#    pragma weak PMPI_TYPE_UB              = mpi_type_ub_
#    pragma weak MPI_TYPE_FREE             = mpi_type_free_
#    pragma weak PMPI_TYPE_FREE            = mpi_type_free_ 
#    pragma weak MPI_TYPE_SIZE             = mpi_type_size_
#    pragma weak PMPI_TYPE_SIZE            = mpi_type_size_ 
#    pragma weak MPI_TYPE_EXTENT           = mpi_type_extent_
#    pragma weak PMPI_TYPE_EXTENT          = mpi_type_extent_ 
#    pragma weak MPI_TYPE_STRUCT           = mpi_type_struct_
#    pragma weak PMPI_TYPE_STRUCT          = mpi_type_struct_ 
#    pragma weak MPI_TYPE_CONTIGUOUS       = mpi_type_contiguous_
#    pragma weak PMPI_TYPE_CONTIGUOUS      = mpi_type_contiguous_ 
#    pragma weak MPI_TYPE_VECTOR           = mpi_type_vector_
#    pragma weak PMPI_TYPE_VECTOR          = mpi_type_vector_ 
#    pragma weak MPI_TYPE_HVECTOR          = mpi_type_hvector_
#    pragma weak PMPI_TYPE_HVECTOR         = mpi_type_hvector_ 
#    pragma weak MPI_TYPE_INDEXED          = mpi_type_indexed_
#    pragma weak PMPI_TYPE_INDEXED         = mpi_type_indexed_ 
#    pragma weak MPI_TYPE_HINDEXED         = mpi_type_hindexed_
#    pragma weak PMPI_TYPE_HINDEXED        = mpi_type_hindexed_ 
#    pragma weak MPI_TYPE_COMMIT           = mpi_type_commit_
#    pragma weak PMPI_TYPE_COMMIT          = mpi_type_commit_ 
#    pragma weak MPI_TYPE_CREATE_DARRAY    = mpi_type_create_darray_	
#    pragma weak PMPI_TYPE_CREATE_DARRAY   = mpi_type_create_darray_
#    pragma weak MPI_TYPE_CREATE_SUBARRAY  = mpi_type_create_subarray_	
#    pragma weak PMPI_TYPE_CREATE_SUBARRAY = mpi_type_create_subarray_
#    pragma weak MPI_TYPE_GET_CONTENTS     = mpi_type_get_contents_	
#    pragma weak PMPI_TYPE_GET_CONTENTS    = mpi_type_get_contents_
#    pragma weak MPI_TYPE_GET_ENVELOPE     = mpi_type_get_envelope_	
#    pragma weak PMPI_TYPE_GET_ENVELOPE    = mpi_type_get_envelope_

#  elif defined(FORTRANDOUBLEUNDERSCORE)

#    pragma weak mpi_pack__                  = mpi_pack_
#    pragma weak pmpi_pack__                 = mpi_pack_
#    pragma weak mpi_pack_size__             = mpi_pack_size_
#    pragma weak pmpi_pack_size__            = mpi_pack_size_
#    pragma weak mpi_unpack__                = mpi_unpack_
#    pragma weak pmpi_unpack__               = mpi_unpack_
#    pragma weak mpi_address__               = mpi_address_
#    pragma weak pmpi_address__              = mpi_address_
#    pragma weak mpi_get_count__             = mpi_get_count_
#    pragma weak pmpi_get_count__            = mpi_get_count_
#    pragma weak mpi_get_elements__          = mpi_get_elements_
#    pragma weak pmpi_get_elements__         = mpi_get_elements_
#    pragma weak mpi_type_lb__               = mpi_type_lb_	
#    pragma weak pmpi_type_lb__              = mpi_type_lb_
#    pragma weak mpi_type_ub__               = mpi_type_ub_	
#    pragma weak pmpi_type_ub__              = mpi_type_ub_
#    pragma weak mpi_type_free__             = mpi_type_free_
#    pragma weak pmpi_type_free__            = mpi_type_free_
#    pragma weak mpi_type_size__             = mpi_type_size_
#    pragma weak pmpi_type_size__            = mpi_type_size_
#    pragma weak mpi_type_extent__           = mpi_type_extent_
#    pragma weak pmpi_type_extent__          = mpi_type_extent_
#    pragma weak mpi_type_struct__           = mpi_type_struct_
#    pragma weak pmpi_type_struct__          = mpi_type_struct_
#    pragma weak mpi_type_contiguous__       = mpi_type_contiguous_
#    pragma weak pmpi_type_contiguous__      = mpi_type_contiguous_
#    pragma weak mpi_type_vector__           = mpi_type_vector_
#    pragma weak pmpi_type_vector__          = mpi_type_vector_
#    pragma weak mpi_type_hvector__          = mpi_type_hvector_
#    pragma weak pmpi_type_hvector__         = mpi_type_hvector_
#    pragma weak mpi_type_indexed__          = mpi_type_indexed_
#    pragma weak pmpi_type_indexed__         = mpi_type_indexed_
#    pragma weak mpi_type_hindexed__         = mpi_type_hindexed_
#    pragma weak pmpi_type_hindexed__        = mpi_type_hindexed_
#    pragma weak mpi_type_commit__           = mpi_type_commit_
#    pragma weak pmpi_type_commit__          = mpi_type_commit_
#    pragma weak mpi_type_create_darray__    = mpi_type_create_darray_
#    pragma weak pmpi_type_create_darray__   = mpi_type_create_darray_
#    pragma weak mpi_type_create_subarray__  = mpi_type_create_subarray_
#    pragma weak pmpi_type_create_subarray__ = mpi_type_create_subarray_
#    pragma weak mpi_type_get_contents__     = mpi_type_get_contents_
#    pragma weak pmpi_type_get_contents__    = mpi_type_get_contents_
#    pragma weak mpi_type_get_envelope__     = mpi_type_get_envelope_
#    pragma weak pmpi_type_get_envelope__    = mpi_type_get_envelope_


#  elif defined(FORTRANNOUNDERSCORE)

#    pragma weak mpi_pack                  = mpi_pack_
#    pragma weak pmpi_pack                 = mpi_pack_
#    pragma weak mpi_pack_size             = mpi_pack_size_
#    pragma weak pmpi_pack_size            = mpi_pack_size_
#    pragma weak mpi_unpack                = mpi_unpack_
#    pragma weak pmpi_unpack               = mpi_unpack_
#    pragma weak mpi_address               = mpi_address_
#    pragma weak pmpi_address              = mpi_address_
#    pragma weak mpi_get_count             = mpi_get_count_
#    pragma weak pmpi_get_count            = mpi_get_count_
#    pragma weak mpi_get_elements          = mpi_get_elements_
#    pragma weak pmpi_get_elements         = mpi_get_elements_
#    pragma weak mpi_type_lb               = mpi_type_lb_	
#    pragma weak pmpi_type_lb              = mpi_type_lb_
#    pragma weak mpi_type_ub               = mpi_type_ub_	
#    pragma weak pmpi_type_ub              = mpi_type_ub_
#    pragma weak mpi_type_free             = mpi_type_free_
#    pragma weak pmpi_type_free            = mpi_type_free_
#    pragma weak mpi_type_size             = mpi_type_size_
#    pragma weak pmpi_type_size            = mpi_type_size_
#    pragma weak mpi_type_extent           = mpi_type_extent_
#    pragma weak pmpi_type_extent          = mpi_type_extent_
#    pragma weak mpi_type_struct           = mpi_type_struct_
#    pragma weak pmpi_type_struct          = mpi_type_struct_
#    pragma weak mpi_type_contiguous       = mpi_type_contiguous_
#    pragma weak pmpi_type_contiguous      = mpi_type_contiguous_
#    pragma weak mpi_type_vector           = mpi_type_vector_
#    pragma weak pmpi_type_vector          = mpi_type_vector_
#    pragma weak mpi_type_hvector          = mpi_type_hvector_
#    pragma weak pmpi_type_hvector         = mpi_type_hvector_
#    pragma weak mpi_type_indexed          = mpi_type_indexed_
#    pragma weak pmpi_type_indexed         = mpi_type_indexed_
#    pragma weak mpi_type_hindexed         = mpi_type_hindexed_
#    pragma weak pmpi_type_hindexed        = mpi_type_hindexed_
#    pragma weak mpi_type_commit           = mpi_type_commit_
#    pragma weak pmpi_type_commit          = mpi_type_commit_
#    pragma weak mpi_type_create_darray    = mpi_type_create_darray_
#    pragma weak pmpi_type_create_darray   = mpi_type_create_darray_
#    pragma weak mpi_type_create_subarray  = mpi_type_create_subarray_
#    pragma weak pmpi_type_create_subarray = mpi_type_create_subarray_
#    pragma weak mpi_type_get_contents     = mpi_type_get_contents_
#    pragma weak pmpi_type_get_contents    = mpi_type_get_contents_
#    pragma weak mpi_type_get_envelope     = mpi_type_get_envelope_
#    pragma weak pmpi_type_get_envelope    = mpi_type_get_envelope_


#  else
/* fortran names are lower case and single underscore. we still
   need the weak symbols statements. EG  */

#    pragma weak pmpi_pack_                 = mpi_pack_
#    pragma weak pmpi_pack_size_            = mpi_pack_size_
#    pragma weak pmpi_unpack_               = mpi_unpack_
#    pragma weak pmpi_address_              = mpi_address_
#    pragma weak pmpi_get_count_            = mpi_get_count_
#    pragma weak pmpi_get_elements_         = mpi_get_elements_
#    pragma weak pmpi_type_lb_              = mpi_type_lb_
#    pragma weak pmpi_type_ub_              = mpi_type_ub_
#    pragma weak pmpi_type_free_            = mpi_type_free_
#    pragma weak pmpi_type_size_            = mpi_type_size_
#    pragma weak pmpi_type_extent_          = mpi_type_extent_
#    pragma weak pmpi_type_struct_          = mpi_type_struct_
#    pragma weak pmpi_type_contiguous_      = mpi_type_contiguous_
#    pragma weak pmpi_type_vector_          = mpi_type_vector_
#    pragma weak pmpi_type_hvector_         = mpi_type_hvector_
#    pragma weak pmpi_type_indexed_         = mpi_type_indexed_
#    pragma weak pmpi_type_hindexed_        = mpi_type_hindexed_
#    pragma weak pmpi_type_commit_          = mpi_type_commit_
#    pragma weak pmpi_type_create_darray_   = mpi_type_create_darray_
#    pragma weak pmpi_type_create_subarray_ = mpi_type_create_subarray_
#    pragma weak pmpi_type_get_contents_    = mpi_type_get_contents_
#    pragma weak pmpi_type_get_envelope_    = mpi_type_get_envelope_

#  endif /*FORTRANCAPS, etc. */
#else
/* now we work with the define statements. 
   Each routine containing an f2c interface
   will have to be compiled twice: once
   without the COMPILE_FORTRAN_INTERFACE
   flag, which will generate the regular
   fortran-interface, one with, generating
   the profiling interface 
   EG Jan. 14 2003 */

#  ifdef COMPILE_FORTRAN_PROFILING_INTERFACE

#    ifdef FORTRANCAPS

#      define mpi_type_lb_               PMPI_TYPE_LB   
#      define mpi_type_ub_               PMPI_TYPE_UB   
#      define mpi_get_count_             PMPI_GET_COUNT 
#      define mpi_get_elements_          PMPI_GET_ELEMENTS 
#      define mpi_get_processor_name_    PMPI_GET_PROCESSOR_NAME 
#      define mpi_pack_                  PMPI_PACK 
#      define mpi_pack_size_             PMPI_PACK_SIZE 
#      define mpi_type_free_             PMPI_TYPE_FREE 
#      define mpi_type_size_             PMPI_TYPE_SIZE 
#      define mpi_type_extent_           PMPI_TYPE_EXTENT 
#      define mpi_type_struct_           PMPI_TYPE_STRUCT 
#      define mpi_type_contiguous_       PMPI_TYPE_CONTIGUOUS 
#      define mpi_type_vector_           PMPI_TYPE_VECTOR 
#      define mpi_type_hvector_          PMPI_TYPE_HVECTOR 
#      define mpi_type_indexed_          PMPI_TYPE_INDEXED 
#      define mpi_type_hindexed_         PMPI_TYPE_HINDEXED 
#      define mpi_type_commit_           PMPI_TYPE_COMMIT 
#      define mpi_unpack_                PMPI_UNPACK 
#      define mpi_address_               PMPI_ADDRESS 
#      define mpi_type_create_darray_    PMPI_TYPE_CREATE_DARRAY   
#      define mpi_type_create_subarray_  PMPI_TYPE_CREATE_SUBARRAY   
#      define mpi_type_get_contents_     PMPI_TYPE_GET_CONTENTS   
#      define mpi_type_get_envelope_     PMPI_TYPE_GET_ENVELOPE   

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define mpi_type_lb_              pmpi_type_lb__ 
#      define mpi_type_ub_              pmpi_type_ub__ 
#      define mpi_get_count_            pmpi_get_count__ 
#      define mpi_get_elements_         pmpi_get_elements__ 
#      define mpi_pack_ 	        pmpi_pack__ 
#      define mpi_pack_size_ 	        pmpi_pack_size__ 
#      define mpi_unpack_ 	        pmpi_unpack__ 
#      define mpi_type_free_            pmpi_type_free__ 
#      define mpi_type_size_            pmpi_type_size__ 
#      define mpi_type_extent_          pmpi_type_extent__ 
#      define mpi_type_struct_          pmpi_type_struct__ 
#      define mpi_type_contiguous_      pmpi_type_contiguous__ 
#      define mpi_type_vector_          pmpi_type_vector__ 
#      define mpi_type_hvector_         pmpi_type_hvector__ 
#      define mpi_type_indexed_         pmpi_type_indexed__ 
#      define mpi_type_hindexed_        pmpi_type_hindexed__ 
#      define mpi_type_commit_          pmpi_type_commit__ 
#      define mpi_address_              pmpi_address__ 
#      define mpi_type_create_darray_   pmpi_type_create_darray__ 
#      define mpi_type_create_subarray_ pmpi_type_create_subarray__ 
#      define mpi_type_get_contents_    pmpi_type_get_contents__ 
#      define mpi_type_get_envelope_    pmpi_type_get_envelope__ 


#    elif defined(FORTRANNOUNDERSCORE)

#      define mpi_type_lb_              pmpi_type_lb 
#      define mpi_type_ub_              pmpi_type_ub 
#      define mpi_get_count_            pmpi_get_count 
#      define mpi_get_elements_         pmpi_get_elements 
#      define mpi_pack_ 	        pmpi_pack 
#      define mpi_pack_size_ 	        pmpi_pack_size 
#      define mpi_unpack_ 	        pmpi_unpack 
#      define mpi_type_free_            pmpi_type_free 
#      define mpi_type_size_            pmpi_type_size 
#      define mpi_type_extent_          pmpi_type_extent 
#      define mpi_type_struct_          pmpi_type_struct 
#      define mpi_type_contiguous_      pmpi_type_contiguous 
#      define mpi_type_vector_          pmpi_type_vector 
#      define mpi_type_hvector_         pmpi_type_hvector 
#      define mpi_type_indexed_         pmpi_type_indexed 
#      define mpi_type_hindexed_        pmpi_type_hindexed 
#      define mpi_type_commit_          pmpi_type_commit 
#      define mpi_address_              pmpi_address 
#      define mpi_type_create_darray_   pmpi_type_create_darray 
#      define mpi_type_create_subarray_ pmpi_type_create_subarray 
#      define mpi_type_get_contents_    pmpi_type_get_contents 
#      define mpi_type_get_envelope_    pmpi_type_get_envelope 

#    else
/* for the profiling interface we have to do the
   redefinitions, although the name-mangling from
   fortran to C is already correct
   EG Jan. 14 2003 */

#      define mpi_type_lb_              pmpi_type_lb_ 
#      define mpi_type_ub_              pmpi_type_ub_ 
#      define mpi_get_count_            pmpi_get_count_ 
#      define mpi_get_elements_         pmpi_get_elements_ 
#      define mpi_pack_ 	        pmpi_pack_ 
#      define mpi_pack_size_ 	        pmpi_pack_size_ 
#      define mpi_unpack_ 	        pmpi_unpack_ 
#      define mpi_type_free_            pmpi_type_free_ 
#      define mpi_type_size_            pmpi_type_size_ 
#      define mpi_type_extent_          pmpi_type_extent_ 
#      define mpi_type_struct_          pmpi_type_struct_ 
#      define mpi_type_contiguous_      pmpi_type_contiguous_ 
#      define mpi_type_vector_          pmpi_type_vector_ 
#      define mpi_type_hvector_         pmpi_type_hvector_ 
#      define mpi_type_indexed_         pmpi_type_indexed_ 
#      define mpi_type_hindexed_        pmpi_type_hindexed_ 
#      define mpi_type_commit_          pmpi_type_commit_ 
#      define mpi_address_              pmpi_address_ 
#      define mpi_type_create_darray_   pmpi_type_create_darray_ 
#      define mpi_type_create_subarray_ pmpi_type_create_subarray_ 
#      define mpi_type_get_contents_    pmpi_type_get_contents_ 
#      define mpi_type_get_envelope_    pmpi_type_get_envelope_ 
#    endif

#  else /* COMPILING_FORTRAN_PROFILING_INTERFACE */

#    ifdef FORTRANCAPS

#      define mpi_type_lb_               MPI_TYPE_LB   
#      define mpi_type_ub_               MPI_TYPE_UB   
#      define mpi_get_count_             MPI_GET_COUNT 
#      define mpi_get_elements_          MPI_GET_ELEMENTS 
#      define mpi_get_processor_name_    MPI_GET_PROCESSOR_NAME 
#      define mpi_pack_                  MPI_PACK 
#      define mpi_pack_size_             MPI_PACK_SIZE 
#      define mpi_type_free_             MPI_TYPE_FREE 
#      define mpi_type_size_             MPI_TYPE_SIZE 
#      define mpi_type_extent_           MPI_TYPE_EXTENT 
#      define mpi_type_struct_           MPI_TYPE_STRUCT 
#      define mpi_type_contiguous_       MPI_TYPE_CONTIGUOUS 
#      define mpi_type_vector_           MPI_TYPE_VECTOR 
#      define mpi_type_hvector_          MPI_TYPE_HVECTOR 
#      define mpi_type_indexed_          MPI_TYPE_INDEXED 
#      define mpi_type_hindexed_         MPI_TYPE_HINDEXED 
#      define mpi_type_commit_           MPI_TYPE_COMMIT 
#      define mpi_unpack_                MPI_UNPACK 
#      define mpi_address_               MPI_ADDRESS 
#      define mpi_type_create_darray_    MPI_TYPE_CREATE_DARRAY   
#      define mpi_type_create_subarray_  MPI_TYPE_CREATE_SUBARRAY   
#      define mpi_type_get_contents_     MPI_TYPE_GET_CONTENTS   
#      define mpi_type_get_envelope_     MPI_TYPE_GET_ENVELOPE   
 

#    elif defined(FORTRANDOUBLEUNDERSCORE)

#      define mpi_type_lb_              mpi_type_lb__ 
#      define mpi_type_ub_              mpi_type_ub__ 
#      define mpi_get_count_            mpi_get_count__ 
#      define mpi_get_elements_         mpi_get_elements__ 
#      define mpi_pack_ 	        mpi_pack__ 
#      define mpi_pack_size_ 	        mpi_pack_size__ 
#      define mpi_unpack_ 	        mpi_unpack__ 
#      define mpi_type_free_            mpi_type_free__ 
#      define mpi_type_size_            mpi_type_size__ 
#      define mpi_type_extent_          mpi_type_extent__ 
#      define mpi_type_struct_          mpi_type_struct__ 
#      define mpi_type_contiguous_      mpi_type_contiguous__ 
#      define mpi_type_vector_          mpi_type_vector__ 
#      define mpi_type_hvector_         mpi_type_hvector__ 
#      define mpi_type_indexed_         mpi_type_indexed__ 
#      define mpi_type_hindexed_        mpi_type_hindexed__ 
#      define mpi_type_commit_          mpi_type_commit__ 
#      define mpi_address_              mpi_address__ 
#      define mpi_type_create_darray_   mpi_type_create_darray__ 
#      define mpi_type_create_subarray_ mpi_type_create_subarray__ 
#      define mpi_type_get_contents_    mpi_type_get_contents__ 
#      define mpi_type_get_envelope_    mpi_type_get_envelope__ 

#    elif defined(FORTRANNOUNDERSCORE)

#      define mpi_type_lb_              mpi_type_lb 
#      define mpi_type_ub_              mpi_type_ub 
#      define mpi_get_count_            mpi_get_count 
#      define mpi_get_elements_         mpi_get_elements 
#      define mpi_pack_ 	        mpi_pack 
#      define mpi_pack_size_ 	        mpi_pack_size 
#      define mpi_unpack_ 	        mpi_unpack 
#      define mpi_type_free_            mpi_type_free 
#      define mpi_type_size_            mpi_type_size 
#      define mpi_type_extent_          mpi_type_extent 
#      define mpi_type_struct_          mpi_type_struct 
#      define mpi_type_contiguous_      mpi_type_contiguous 
#      define mpi_type_vector_          mpi_type_vector 
#      define mpi_type_hvector_         mpi_type_hvector 
#      define mpi_type_indexed_         mpi_type_indexed 
#      define mpi_type_hindexed_        mpi_type_hindexed 
#      define mpi_type_commit_          mpi_type_commit 
#      define mpi_address_              mpi_address 
#      define mpi_type_create_darray_   mpi_type_create_darray 
#      define mpi_type_create_subarray_ mpi_type_create_subarray 
#      define mpi_type_get_contents_    mpi_type_get_contents 
#      define mpi_type_get_envelope_    mpi_type_get_envelope 


#    else
/* fortran names are lower case and single underscore. 
   for the regular fortran interface nothing has to be done
   EG Jan. 14 2003 */
#  endif /* FORTRANCAPS etc. */
#  endif /* COMPILING_FORTRAN_PROFILING_INTERFACE */


#endif /* HAVE_PRAGMA_WEAK */
#endif
