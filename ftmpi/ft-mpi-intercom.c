/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors of this file:
                        Edgar Gabriel <egabriel@cs.utk.edu>
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-com.h"
#include "ft-mpi-group.h"
#include "ft-mpi-coll.h"
#include "ft-mpi-intercom.h"
#include "ft-mpi-msg-list.h"
#include "ft-mpi-p2p.h"
#include "debug.h"

/* The following functions have to be implemented here.
   Management functions:
     - ftmpi_com_test_inter
     - ftmpi_com_get_remote_group
     - ftmpi_com_get_remote_group_ptr
     - ftmpi_com_get_lleader
     - ftmpi_com_get_lleader_gid
     - ftmpi_com_get_rleader_gid
     - ftmpi_com_get_local_comm
     - ftmpi_com_build_intercom
   Functions for abstracting some communication patterns in 
   Intercomm_create:
     - ftmpi_com_exchange_group
     - ftmpi_com_bcast_group
   Functions for abstracting some communication patterns in
   Intercomm_merge:
     - ftmpi_com_exchange_high
     - ftmpi_com_copy_intercom
*/

extern comm_info_t _ftmpi_comm[MAXCOMS];	/* just for now a fixed array */
extern int _ftmpi_comfree;
   

int ftmpi_com_test_inter ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].inter );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/

MPI_Group ftmpi_com_get_remote_group (MPI_Comm com)
{
  return ( _ftmpi_comm[com].remote_group );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_remote_size ( MPI_Comm com)
{
  MPI_Group grp;

  grp = ftmpi_com_get_remote_group ( com );
  if ( grp < 0 ) return (MPI_ERR_COMM);
  
  return (ftmpi_group_size ( grp ));
}
  
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
group_t *ftmpi_com_get_remote_group_ptr ( MPI_Comm com )
{
  group_t * grp_ptr;

  grp_ptr = (group_t *) ftmpi_group_get_ptr ( _ftmpi_comm[com].remote_group );
  return ( grp_ptr );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_lleader ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].lleader );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_lleader_gid ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].lleader_gid );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_get_rleader_gid ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].rleader_gid );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
MPI_Comm ftmpi_com_get_local_comm ( MPI_Comm com )
{
  return ( _ftmpi_comm[com].local_comm );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* This function sets for a ALREADY ALLOCATED communicator all
   settings realted to intercommunicators */
int ftmpi_com_build_intercom ( MPI_Comm com, MPI_Comm local_comm,
			       MPI_Group r_group, int lleader, int llgid,
			       int rleader, int rlgid )
{
  int ret;
  MPI_Comm nc;

  /* create a copy of the local communicator for later use 
     (the user might free local_comm, which would thus not be
     available anymore ) */
  ret = MPI_Comm_dup ( local_comm, &nc );

  _ftmpi_comm[com].local_comm = nc;

  _ftmpi_comm[com].inter   = 1;
  _ftmpi_comm[com].lleader = lleader;
  _ftmpi_comm[com].rleader = rleader;
  _ftmpi_comm[com].lleader_gid = llgid;
  _ftmpi_comm[com].rleader_gid = rlgid;
  _ftmpi_comm[com].remote_group = r_group;

  ftmpi_group_refinc ( r_group);

  return ( ret );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
group_t *ftmpi_com_exchange_groups (MPI_Comm local_comm,
				    MPI_Comm peer_comm, 
				    int remote_leader,
				    int tag)
{
  group_t *grp_ptr, *r_grp_ptr;
  MPI_Group grp;
  int g_rank;
  MPI_Status status;
  int ret;

  grp = ftmpi_com_get_group (local_comm);
  grp_ptr = (group_t *) ftmpi_group_get_ptr ( grp );

  r_grp_ptr = ( group_t *) _MALLOC( sizeof (group_t ) );
  if ( r_grp_ptr == NULL )
    return ( NULL );
  
  /* So which data is now relevant to exchange between the two 
     "leaders ?" I think currently its maxsize (resp. currentsize),
     and the array gids.
     
     For the communication, I can think of several possibilities:
     - send each element separatly ( which I am doing now)
     - pack the required information
     - use a derived datatype describinb a group structure (would
       work currently with the fixed gids-array, as soon as
       we change that to a flexible array its getting more
       difficult 
     - send sizeof(group_t) MPI_CHARS (bad in heterogeneous
       environments) 
  */

  g_rank = ftmpi_com_rank ( peer_comm );
  if ( g_rank > remote_leader )
    {
      ret = ftmpi_mpi_send ( &(grp_ptr->currentsize), 1, MPI_INT, remote_leader,
			     tag, peer_comm );
      if ( ret != MPI_SUCCESS ) return ( NULL );

      ret = ftmpi_mpi_send ( grp_ptr->gids, grp_ptr->currentsize, MPI_INT, 
			     remote_leader, tag, peer_comm );
      if ( ret != MPI_SUCCESS ) return ( NULL );

      ret = ftmpi_mpi_recv ( &(r_grp_ptr->currentsize), 1, MPI_INT, remote_leader,
			     tag, peer_comm, &status );
      if ( ret != MPI_SUCCESS ) return ( NULL );
      ret = ftmpi_mpi_recv ( r_grp_ptr->gids, r_grp_ptr->currentsize, MPI_INT, 
			     remote_leader, tag, peer_comm, &status );
      if ( ret != MPI_SUCCESS ) return ( NULL );
    }
  else
    {
      ret = ftmpi_mpi_recv ( &(r_grp_ptr->currentsize), 1, MPI_INT, remote_leader,
			     tag, peer_comm, &status );
      if ( ret != MPI_SUCCESS ) return ( NULL );
      ret = ftmpi_mpi_recv ( r_grp_ptr->gids, r_grp_ptr->currentsize, MPI_INT, 
			     remote_leader, tag, peer_comm, &status );
      if ( ret != MPI_SUCCESS ) return ( NULL );
      ret = ftmpi_mpi_send ( &(grp_ptr->currentsize), 1, MPI_INT, remote_leader,
			     tag, peer_comm );
      if ( ret != MPI_SUCCESS ) return ( NULL );
      ret = ftmpi_mpi_send ( grp_ptr->gids, grp_ptr->currentsize, MPI_INT, 
			     remote_leader, tag, peer_comm );
      if ( ret != MPI_SUCCESS ) return ( NULL );
    }

  return ( r_grp_ptr );
}

/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_bcast_group ( group_t *grp_ptr, MPI_Comm comm, 
			    int root, int *remote_leader, int *rleader_gid )
{
  int ret;
  MPI_Comm shadow;

  shadow = ftmpi_com_get_shadow ( comm );
  if ( shadow == MPI_COMM_NULL ) return ( MPI_ERR_INTERN);

  ret = ftmpi_coll_intra_bcast_bmtree ( &(grp_ptr->currentsize), 1, MPI_INT, 
					root, shadow, 0);
  if ( ret != MPI_SUCCESS ) return ( ret );

  ret = ftmpi_coll_intra_bcast_bmtree ( grp_ptr->gids, grp_ptr->currentsize, 
					MPI_INT, root, shadow, 0 );
  if ( ret != MPI_SUCCESS ) return ( ret );

  /* OK, this looks silly, but since the MPI - standard doesn't force
     the user, that all members have to provide the same argument for remote_leader,
     I am distributing this argument to all members in the group.
     EG Feb. 13 2003 */

  ret = ftmpi_coll_intra_bcast_bmtree ( remote_leader, 1, MPI_INT, root, 
					shadow, 0 );
  if ( ret != MPI_SUCCESS ) return ( ret );

  ret = ftmpi_coll_intra_bcast_bmtree ( rleader_gid, 1, MPI_INT, root, 
					shadow, 0 );
  
  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
int ftmpi_com_exchange_high ( MPI_Comm com, int high, int *r_high )
{
  /* Remark: don't forget, that comm is an inter-communicator,
     therefore local_comm is the copy of the original local group
     presented at Intercomm_create */

  MPI_Comm local_comm;
  MPI_Status status;
  int ret, lrank;
  MPI_Comm shadow;
  int hmax=0,hmin=0;

  local_comm = _ftmpi_comm[com].local_comm;
  lrank = ftmpi_com_rank ( com );

  shadow = ftmpi_com_get_shadow ( local_comm );
  if ( shadow == MPI_COMM_NULL ) return ( MPI_ERR_INTERN);

  /* Check first, whether the high value is identical on all procs
     in each group. For this, we do two Allreduce operations. */
  ret = ftmpi_coll_intra_allreduce_bmtree (&high, &hmax, 1, MPI_INT, 
					   MPI_MAX, shadow, 0);
  if ( ret != MPI_SUCCESS ) return (ret);
  ret = ftmpi_coll_intra_allreduce_bmtree (&high, &hmin, 1, MPI_INT, 
					   MPI_MIN, shadow, 0);
  if ( ret != MPI_SUCCESS ) return (ret);
  if ( (hmax != high) || (hmin != high))
    return ( MPI_ERR_ARG);

  /* We are not using lleader and rleader, since this would require 
     one additional mapping step. However we are using their gid's
     to determine who is sending resp. receiving first, to avoid
     a deadlock */
  if ( lrank == 0 ) 
    {		   
      if ( _ftmpi_comm[com].rleader_gid > _ftmpi_comm[com].lleader_gid )
	{
	  ret = ftmpi_mpi_send ( &high, 1, MPI_INT, 0, 123, com );
	  if ( ret != MPI_SUCCESS ) return ( ret );

	  ret = ftmpi_mpi_recv ( r_high, 1, MPI_INT, 0, 124, com, &status );
	  if ( ret != MPI_SUCCESS ) return ( ret );
	}
      else
	{
	  ret = ftmpi_mpi_recv ( r_high, 1, MPI_INT, 0, 123, com, &status );
	  if ( ret != MPI_SUCCESS ) return ( ret );
	  ret = ftmpi_mpi_send ( &high, 1, MPI_INT, 0, 124, com );
	  if ( ret != MPI_SUCCESS ) return ( ret );
	}
    }

  ret = ftmpi_coll_intra_bcast_bmtree ( r_high, 1, MPI_INT, 0, 
					shadow, 0 );

  return ( ret );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Copy all relevant information for an intercommunicator.
   Should just be called from MPI_Comm_dup */

int ftmpi_com_copy_intercom ( MPI_Comm com, MPI_Comm newcom )
{
  int ret=MPI_SUCCESS;

  _ftmpi_comm[newcom].inter       = _ftmpi_comm[com].inter;
  _ftmpi_comm[newcom].rleader     = _ftmpi_comm[com].rleader ;
  _ftmpi_comm[newcom].lleader     = _ftmpi_comm[com].lleader ;
  _ftmpi_comm[newcom].rleader_gid = _ftmpi_comm[com].rleader_gid ;
  _ftmpi_comm[newcom].lleader_gid = _ftmpi_comm[com].lleader_gid ;

  if ( !_ftmpi_comm[newcom].am_shadow )  
    {
      ret = ftmpi_mpi_comm_dup ( _ftmpi_comm[com].local_comm, 
				 &(_ftmpi_comm[newcom].local_comm) );
      if ( ret != MPI_SUCCESS ) return ( ret );
    }

  ftmpi_group_copy (_ftmpi_comm[com].remote_group, 
		    &(_ftmpi_comm[newcom].remote_group) );
  ftmpi_group_refinc(_ftmpi_comm[newcom].remote_group);

  return ( ret );
}
















