#include <stdio.h>
#include <stdlib.h>
#include <sys/poll.h>
#include <errno.h>
#include <unistd.h>
/* extern int errno; */

#include "snipe_lite.h"
#include "snipe2.h"
/* #include "snipe2-msg-list.h" */


#define POLLTOUT -1

#define ERREVENTS (POLLERR | POLLHUP | POLLNVAL)
#define READEVENT (POLLIN)
#define WRITEEVENT (POLLOUT)


#define FREELISTSTARTLEN	5
#define FREELISTMAXLEN		10


/* maxchans is not a variable, hense we can resize the chans DS whenever we need
 * to. GEF Sep03.
 */
static int MAXCHANS;


/* data definition of internal channel type */

typedef struct {
	/* basic info */
	int state; 	/* 0 not connected, 1 connected, -1 for error */
	int fd;		/* actual socket */

	int type;	/* is this a normal channel or a channel/accept sock */

	/* poll data structure (so we don't have to rebuild it all the time */
	struct pollfd* ppfd;

	/* on error callback */
	intfuncptr errorcallback;	/* function to call */
	long erroruserval;			/* value to return on error for this chan */
	int	autocancel;		/* if we should cancel all req on error */
								/* 0 = no, 1 = yes, 2 = yes with callback */

	/* counters */
	int next_msg_id; /* next message id to be issued */

	/* not if the below send/recv values are set, then these elements */
	/* are no longer on the send and recv queues */

	/* send queue info */
	int pending_sends;	/* # how many incl current */
	struct snipe2_msg_list*	send_head;

	/* current send op */
	struct snipe2_msg_list* sendcurrent;
	int sendreqid;
	long datasentcnt;
	long datatosendcnt;
	char *sendbuf;
	intfuncptr sendcallback;

	/* recv queue info */
	int pending_recvs;	/* # how many incl current */
	struct snipe2_msg_list*	recv_head;

	/* current recv op */
	struct snipe2_msg_list* recvcurrent;
	int recvreqid;
	long datarecvcnt;
	long datatorecvcnt;
	char *recvbuf;
	intfuncptr recvcallback;

	/* stats info */
	long stat_total_sends;
    long stat_total_recvs;
	long stat_send_polls;
	long stat_recv_polls;
	long stat_send_polls_suc;
	long stat_recv_polls_suc;

} chan2_t;

/* local prototypes not called from else where ? */
int     snipe2_clr_chan (chan2_t *chan, int clrlists);  /* internal */
int     snipe2_set_next_send (int chan);
int     snipe2_set_next_recv (int chan);

int     snipe2_do_send_on_all (int flags, int autonext);
int     snipe2_do_recv_on_all (int flags, int autonext);
int     snipe2_do_send (int chan, int autonext, int immediate);
int     snipe2_do_recv (int chan, int autonext, int immediate);

int 	snipe2_set_next_send_blank (int c);
int 	snipe2_set_next_recv_blank (int c);

int		snipe2_check_for_error_and_mark (int chan, int lastrc);
int		snipe2_do_check_for_errors ();



/* data structure that holds everything */
static chan2_t* chans;

static int chan_top=0;	/* 	top channel, i.e. 0..top-1 in use */

/* structure that holds pollfd ptrs to the event lists within the */
/* channel structures themselves */
static struct pollfd* chan2_poll_list;

/* done list */
struct snipe2_msg_list *snipe2_done_list=NULL;
int    snipe2_done_cnt=0;

/* free list */
/* internal to the snipe2_msg_list code now */
/* but we get a ref just we can display it for debugging */
extern struct snipe2_msg_list * snipe2_msg_free_list;


static int snipe2_total_pending_sends = 0;
static int snipe2_total_pending_recvs = 0;
static int snipe2_next_req = 1;				/* 1 as a 0 is a blank request */

static int snipe2_error_checking=0;

/* arrays that hold the list of completed sends/recvs where another is pending
 * this is used when we complete one op and might be able to complete another
 * without having to poll again
 */
int snipe2_repeat_send_cnt;
int snipe2_repeat_recv_cnt;
int *snipe2_repeat_sends;
int *snipe2_repeat_recvs;

int	snipe2_init (int initialchans)
{
	int i;

	chans = (chan2_t*) malloc (initialchans*sizeof(chan2_t));

	if (!chans) {
	   fprintf(stderr,"snipe2_init: failure allocating memory for channels\n");
	   return (-1);
	}

	chan2_poll_list = (struct pollfd*) malloc 
	   				(initialchans*sizeof(struct pollfd));
	if (!chan2_poll_list) {
	   fprintf(stderr,"snipe2_init: failure allocating memory for poll data structures\n");
	   return (-1);
	}

	snipe2_repeat_sends = (int*) malloc (initialchans*sizeof(int));
	snipe2_repeat_recvs = (int*) malloc (initialchans*sizeof(int));

	if ((!snipe2_repeat_sends)||(!snipe2_repeat_recvs)) {
	   fprintf(stderr,"snipe2_init: failure allocating memory for repeat send/recv flag data structures\n");
	   return (-1);
	}
	   

	MAXCHANS = initialchans;


	for (i=0;i<MAXCHANS;i++) {
		chans[i].ppfd = &chan2_poll_list[i];	/* link the poll fd to the real pfd */
		snipe2_clr_chan (&chans[i], 0);	
	}

	/* create done list */
	snipe2_done_list = snipe2_msg_list_init();

	/* create free list */
/* 	snipe2_free_list = snipe2_msg_list_init(); */
	/* create some free list elements */
/* 	for (i=0;i<FREELISTSTARTLEN;i++) { */
/*	struct snipe2_msg_list* mlp = snipe2_msg_list_new (snipe2_free_list); */
/* 		snipe2_free_cnt++; */
/* 	} */

	snipe2_msg_list_init_freelist (FREELISTSTARTLEN,FREELISTMAXLEN);


	snipe2_total_pending_sends = 0;
	snipe2_total_pending_recvs = 0;
	snipe2_next_req = 1;

	/* debug */
/* 	snipe2_dump_other_lists (); */
	return 0;
}

int snipe2_end ()
{
   /* should clear all message lists, done and free lists */
   free (snipe2_repeat_sends);
   free (snipe2_repeat_recvs);
   free (chan2_poll_list);
   free (chans);
return (0);
}

int	snipe2_clr_chan (chan2_t *chan, int clrlists)
{
	chan->state=0;				/* not in use yet */
	chan->fd=0;					/* socket */
	chan->type=0;				/* blank type */

	chan->errorcallback=NULL;	/* error call back */
	chan->erroruserval=0;		/* error return value */
	chan->autocancel=0;			/* cancel on error */

	chan->next_msg_id=0;		/* msg counter per channel */

	chan->pending_sends=0;		/* how many */

	/* send low level info */
	chan->datasentcnt=0;
	chan->datatosendcnt=0;
	chan->sendcurrent=NULL;
	chan->sendbuf=NULL;

	chan->pending_recvs=0;

	/* recv low level info */
	chan->datarecvcnt=0;
	chan->datatorecvcnt=0;
	chan->recvcurrent=NULL;
	chan->recvbuf=NULL;

	/* setting up the msg lists for this channel */
	if (clrlists) {
		/* empty the lists */
		if (chan->send_head) snipe2_msg_list_destroy (chan->send_head);
		if (chan->recv_head) snipe2_msg_list_destroy (chan->recv_head);
		chan->send_head = NULL;
		chan->recv_head = NULL;
	}
	else {
		chan->send_head = NULL;
		chan->recv_head = NULL;
	}

	/* reset callbacks here */
	chan->sendcallback = NULL;
	chan->recvcallback = NULL;

	/* poll event list */
	chan->ppfd->fd = 0;				/* poll fd */
	chan->ppfd->events = 0;			/* poll event list */
	chan->ppfd->revents = 0;		/* poll return event list */

	/* statistics for the channel */
	/* if they are ment to be kept */
	chan->stat_total_sends=0;
	chan->stat_total_recvs=0;
	chan->stat_send_polls=0;
	chan->stat_recv_polls=0;
	chan->stat_send_polls_suc=0;
	chan->stat_recv_polls_suc=0;
	return 0;
}

/*
 * find a free channel
 */

int	snipe2_add_chan (int fd, int type, intfuncptr errorcallback, long errval, int autocancel) 
{
	int i;

	for (i=0;i<MAXCHANS;i++) 

		if (chans[i].state==0) { /* if found free */

			chans[i].state = 1;
			chans[i].type = type;

			chans[i].errorcallback=errorcallback;   /* error call back */
		    chans[i].erroruserval=errval;   		/* error return value */
			chans[i].autocancel = autocancel;		/* autocancel reqs on err */

			chans[i].fd = fd;
			chans[i].ppfd->fd = fd;

			chans[i].ppfd->events = 0;	/* clear all events */
			chans[i].ppfd->revents = 0;	/* clear all events */

			/* send recv buffers upto 128KBytes */
			setsendrecvbufs (chans[i].fd, 128); 

			/* DATA channel work on nonblocking sockets */
			/* non data (i.e. pollonly) we do not modify */
			/* also we want error events on a DATA channel */
			if (type==SNIPE2DATA) {
			  /*Removed since setnonblocking is a void funct
			    rc = */ 
			  setnonblocking (fd);		/* set non blocking */
/* 				printf("nonblocking 0x%x\n", rc); */
				/* chans[i].ppfd->events = ERREVENTS;	*/
				/* all channels detect errors */
				snipe2_error_checking++;
			}


			/* create new msg lists */
			chans[i].send_head = snipe2_msg_list_init ();
			chans[i].recv_head = snipe2_msg_list_init ();

			/* must blank these out before use */
			snipe2_set_next_send_blank (i);
			snipe2_set_next_recv_blank (i);

			/* some internal counters updates */
			if ((i+1)>chan_top) chan_top=i+1; /* note the +1 */
#ifdef DEBUG9
			printf("snipe2_add_chan top=%d\n", chan_top);
#endif

			return (i);
		}
	/* if here no channel available */
	return (-1);
}


int	snipe2_remove_chan (int chan)
{
	int i;
	int j;

	if ((chan<0)||(chan>=MAXCHANS)) return (-1);
	if (chans[chan].state==0) return (-1);

	i =chans[chan].fd;
	if (i>0) setblocking (i);

	/* before clr chan we need to unset any fd flags and update any counters */
	/* first counters if we are just throwing ops away!! */
	/* should really cancel them all first ? TODO */
	if (chans[chan].pending_sends>0) {
		snipe2_total_pending_sends -= chans[chan].pending_sends;
		if (snipe2_total_pending_sends<0) {
			fprintf(stderr,"snipe2_remove_chan on %d neg total pending sends?\n", chan);
		}
		chans[chan].ppfd->events =  chans[chan].ppfd->events & (~WRITEEVENT);
		/* reset poll flags */
	}
	if (chans[chan].pending_recvs>0) {
		snipe2_total_pending_recvs -= chans[chan].pending_recvs;
		if (snipe2_total_pending_recvs<0) {
			fprintf(stderr,"snipe2_remove_chan on %d neg total pending recvs?\n", chan);
		}
		chans[chan].ppfd->events =  chans[chan].ppfd->events & (~READEVENT);
		/* reset poll flags */
	}

	snipe2_clr_chan (&chans[chan], 1); /* 1 = free any pending message lists */

	/* some internal counters updates */
	/* if top channel removed, recheck the top channel value */
	if (chan==(chan_top-1)) {
	   for (j=chan_top-1;j>=0;j--) 
		  if (chans[j].state) {
			 chan_top = j+1; /* need +1 here as chan_top is the next highest */
			 break;
		  }
	   if (j==-1) chan_top = 0; /* empty list of channels so set top = 0 */

#ifdef DEBUG9
	printf("snipe2_remove_chan top=%d\n", chan_top);
#endif
	}

	return (0);
}

/*
 * this routine cancels a request as long as it hasn't already started
 */

int     snipe2_cancel_msg (snipe2_msg_list_t *reqptr)
{
   int chan;
   int type;
   int currentsend=0;
   int currentrecv=0;

   /* first check if its possible to cancel the request */
   /* is req valid */
   if (!reqptr) return (0);	

   if (!reqptr->sml_msg_reqid) return (0); /* not in use anymore */

   /* has req already been done? */
   if (reqptr->sml_msg_done) return (0);	

   /* ok, valid request, not done, so find its channel */
   /* we need to know if its in the current req of any */
   /* as well as its type for updating counters */
   chan = reqptr->sml_msg_chan;
   type = reqptr->sml_msg_type;

   /* if it is the current operation we need to do extra checks */
   if (chans[chan].sendcurrent==reqptr) {
	  currentsend=1;
	  if (chans[chan].datasentcnt) return (0); /* already started */
   }
   if (chans[chan].recvcurrent==reqptr) {
	  currentrecv=1;
	  if (chans[chan].datarecvcnt) return (0); /* already started */
   }

   /* ok, we can cancel it */

   /* unlink it, move to done and then free it */
   /* unlink only if not the current op for either send or recv as they */
   /* already are unlinked! */
   if ((!currentrecv)&&(!currentsend)) snipe2_msg_list_unlink (reqptr);
   /* we add to done as free can only handle reqs on a list not floating */
   snipe2_msg_list_add_to_head (snipe2_done_list, reqptr);
   snipe2_msg_list_free (reqptr);

   /* update general counters */
   if (type==1) {
   		chans[chan].pending_recvs--;
   		snipe2_total_pending_recvs--;
   }
   if (type==2) {
   		chans[chan].pending_sends--;
   		snipe2_total_pending_sends--;
   }
   /* update next if needed */
   if (currentrecv) snipe2_set_next_recv (chan);
   if (currentsend) snipe2_set_next_send (chan);

   return (1); /* we freed it */
}


/*
 * this routine adds a send request to a channel 
 */
int     snipe2_post_send_msg (int chan, char *msg, long msglen, intfuncptr callback, long uservalue, int autodelete, snipe2_msg_list_t** reqptr)
{
	int rc=0;

	struct snipe2_msg_list* mlp;

	if ((chan<0)||(chan>=MAXCHANS)) return (-1);
	if (chans[chan].state==0) return (-1);

#ifdef DEBUG
printf("snipe2_post_send_msg:chan%d msg0x%x len %d callback 0x%x userval %d AD%d\n", chan, (long)msg, msglen, (long)callback, uservalue, autodelete);
fflush(stdout);
#endif /* debug */

	/* get an element */
	mlp = snipe2_msg_list_new (chans[chan].send_head);

	if (!mlp) return (-1);	/* PANIC !? */

	/* else its a valid message block, so fill it up */

	mlp->sml_msg_id = chans[chan].next_msg_id++;
	mlp->sml_msg_reqid = snipe2_next_req++;
	mlp->sml_msg_priority = 0; 	/* normal */

	mlp->sml_msg_chan = chan;
	mlp->sml_msg_type = 2; /* send type */

	mlp->sml_msg_buf = msg;
	mlp->sml_msg_length = msglen;

	if (callback) {
	   mlp->sml_callbackfunction = callback;
	   mlp->sml_uservalue = uservalue;
	}
	else mlp->sml_callbackfunction = NULL;

	mlp->sml_autodelete = autodelete;

	/* update general counters for the channel and system */
	
	chans[chan].pending_sends++;	/* inc counts */
	snipe2_total_pending_sends++;


	if (reqptr) *reqptr = mlp; /* return mlp before possible callback */

	/* check to see if we need to set this as our current send */
	/* i.e. are we the first on the list */
	if (!chans[chan].sendcurrent) {
	   snipe2_set_next_send (chan);
	   /* as we are the first on the list we could attempt to send */
	   /* immediately (this potentially reduces latency) */
	   /* but make sure the callback does not cause problems */
	   rc = snipe2_do_send (chan, 1, 1);
/* 	   printf("IS %1d ", rc); */
	}

	return (mlp->sml_msg_reqid);	/* return request ID right now.. */
}

/*
 * this routine adds a recv request to a channel 
 */
int     snipe2_post_recv_msg (int chan, char *msg, long msglen, intfuncptr callback, long uservalue, int autodelete, snipe2_msg_list_t** reqptr)
{
	struct snipe2_msg_list* mlp;
	int rc;

	if ((chan<0)||(chan>=MAXCHANS)) return (-1);
	if (chans[chan].state==0) return (-1);

#ifdef DEBUG
printf("snipe2_post_recv_msg:chan%d msg0x%x len %d callback 0x%x userval %d AD%d\n", chan, (long)msg, msglen, (long)callback, uservalue, autodelete);
fflush(stdout);
#endif /* debug */

	/* get an element */
	/* malloc one and add it to a linked list */
	mlp = snipe2_msg_list_new (chans[chan].recv_head);

	if (!mlp) return (-1);	/* PANIC !? */

	/* else its a valid message block, so fill it up */

	mlp->sml_msg_id = -1;	/* not known yet */
	mlp->sml_msg_reqid = snipe2_next_req++; 
	mlp->sml_msg_priority = 0; 	/* normal */

	mlp->sml_msg_chan = chan;
	mlp->sml_msg_type = 1; /* recv type */

	mlp->sml_msg_buf = msg;
	mlp->sml_msg_length = msglen;

	if (callback) {
	   mlp->sml_callbackfunction = callback;
	   mlp->sml_uservalue = uservalue;
	}
	else mlp->sml_callbackfunction = NULL;

	mlp->sml_autodelete = autodelete;

	/* update general counters for the channel and system */
	chans[chan].pending_recvs++;	
	snipe2_total_pending_recvs++;

	if (reqptr) *reqptr = mlp;	/* return mlp before possible callback */


	/* check to see if we need to set this as our current send */
	if (!chans[chan].recvcurrent) {
	   snipe2_set_next_recv (chan);
	  /* as we are the first on the list we could attempt to recv */
	  /* immediately (this potentially reduces latency) */
	  /* but make sure the callback does not cause problems */
	  rc = snipe2_do_recv (chan, 1, 1);
/* 	  printf("IR %1d ", rc); */
	}

	return (mlp->sml_msg_reqid);
}



int     snipe2_reqptr_2_reqid (snipe2_msg_list_t* req)
{
   if (!req) return (0);
   return (req->sml_msg_reqid);
}


snipe2_msg_list_t* snipe2_reqid_2_reqptr (int reqid)   /* warning slow */
{
   /* search the lists to find this request */
   /* check done, then send, recv */
   return (NULL);
}

int     snipe2_isdone (snipe2_msg_list_t* req, int deleteifdone)
{
   int rc;
/*    printf("isdone reqptr 0x%x\n", req); */
   if (!req) return (0);

/*    printf("isdone reqptr 0x%x reqid %d done %d\n",  */
/* 		 req, req->sml_msg_reqid, req->sml_msg_done); */

   rc = req->sml_msg_done;
   /* done =1 other values mean not done or error cannot do */
   if ((rc==1)&&(deleteifdone)) {
	  snipe2_msg_list_free(req);
   }
   return (rc);
}


int     snipe2_isdone_reqid (int reqid, int deleteifdone) /* slower */
{
   snipe2_msg_list_t* reqptr;
   
   reqptr = snipe2_reqid_2_reqptr (reqid);
   if (!reqptr) return (0);

   return (snipe2_isdone (reqptr, deleteifdone));
}


/* resets the poll/done flag for this channel */
int     snipe2_reset_poll_chan (int chan)
{
   snipe2_msg_list_t* reqptr;

   if (chans[chan].type!=SNIPE2POLLONLY) return (-1);

   reqptr = chans[chan].recvcurrent;
   if (!reqptr) return (-1);

   reqptr->sml_msg_done = 0;

   return (0);
}



/*
 * This routine makes progress on the pending send/recv queues
 *
 * if tchan = SNIPE2ALL then on all channels pending
 * flags = 0 -> just push/pull what you can
 * flags>0 -> complete this many pending ops
 * flags = SNIPE2ALLSENDS complete all sends
 * flags = SNIPE2ALLRECVS complete all recvs
 * flags = SNIPE2ALL complete all ops
 */
int     snipe2_progress (int tchan, int flags)
{
	int c;
	int tout=POLLTOUT;
	int done=0;
	int sautonext=1;	/* send auto next */
	int rautonext=1;	/* recv auto next */
	int scompleted=0;
	int rcompleted=0;
	int tcompleted=0;
	int fscompleted=0;
	int frcompleted=0;
	int failed=0;
	int rc;

	if (!flags) { tout=0; } /* just one loop, poll not wait */

	while (!done) {
#ifdef VERBOSE
	printf("total pending sends %d recvs %d\n", snipe2_total_pending_sends, snipe2_total_pending_recvs);
	snipe2_dump_poll_flags ();
#endif

	for(c=0;c<chan_top;c++) {
	   chan2_poll_list[c].revents = 0; /* reset them all before poll? */
#ifdef FASTREPEAT	
	   snipe2_repeat_sends[c] = 0;
	   snipe2_repeat_recvs[c] = 0;
#endif
	}
	snipe2_repeat_send_cnt = 0;
	snipe2_repeat_recv_cnt = 0;

	rc = poll (chan2_poll_list, chan_top, tout);
#ifdef VERBOSE
	printf ("poll returns %d\n", rc);
	snipe2_dump_poll_result ();
#endif
    chans[c].stat_send_polls++;
    chans[c].stat_recv_polls++;

if (snipe2_total_pending_sends) {
   scompleted += snipe2_do_send_on_all (0, sautonext);
}

if (snipe2_total_pending_recvs) {
   rcompleted += snipe2_do_recv_on_all (0, rautonext);
}

if (snipe2_error_checking) {
   failed += snipe2_do_check_for_errors ();
}

#ifdef FASTREPEAT	
/* fast send/recv test */
/* they are all immediate as this stops them adding to the repeat lists! */
if (snipe2_repeat_send_cnt) {
   for (i=0;i<snipe2_repeat_send_cnt;i++) 
   		if (snipe2_repeat_sends[i]) {
   			fscompleted += snipe2_do_send (snipe2_repeat_sends[i], sautonext, 1);
			/* 1 = immediate */
/* 			printf("fs%d\n", fscompleted); */
		}
   snipe2_repeat_send_cnt=0; /* paranoid */
}

if (snipe2_repeat_recv_cnt) {
   for (i=0;i<snipe2_repeat_recv_cnt;i++) 
   		if (snipe2_repeat_recvs[i])  {
   			frcompleted += snipe2_do_recv (snipe2_repeat_recvs[i], rautonext, 1);
			/* 1 = immediate */
/* 			printf("fr%d\n", frcompleted); */
		}
   snipe2_repeat_recv_cnt=0; /* paranoid */
}
#endif /* FASTREPEAT */


	tcompleted =  (scompleted+rcompleted+failed+fscompleted+frcompleted);

	if ((flags==SNIPE2TEST)||(flags==SNIPE2PROGRESS)) done = 1;
	if ((flags==SNIPE2SOME)&&(tcompleted)) done = 1;
/* 	if ((flags==SNIPE2ALLSENDS)&&(!snipe2_total_pending_sends)) done = 1; */
/* 	if ((flags==SNIPE2SOMESEND)&&( scompleted)) done = 1; */
/* 	if ((flags==SNIPE2ALLRECVS)&&(!snipe2_total_pending_recvs)) done = 1; */
/* 	if ((flags==SNIPE2SOMERECV)&&( rcompleted)) done = 1; */
	/* above modes might break the fast send/recv calls */

	} /* while not done */


return (tcompleted);
}



int	snipe2_pending ()
{
	return (snipe2_total_pending_sends+snipe2_total_pending_recvs);
}

int snipe2_pending_sends_on_chan (int chan)
{
   return (chans[chan].pending_sends);
}

int snipe2_pending_recvs_on_chan (int chan)
{
   return (chans[chan].pending_recvs);
}





/* this routine updates all the counters that indicate how the next send */
/* on channel chan works */
int	snipe2_set_next_send (int chan)
{
	struct snipe2_msg_list* mlp;


	if (chans[chan].pending_sends){		/* if any sends pending */

		mlp = snipe2_msg_list_head (chans[chan].send_head);
		if (!mlp) return (-1); /* panic no message on queue? */

		/* unlink it from the send queue */
		snipe2_msg_list_unlink (mlp);

		/* send low level info */
		chans[chan].sendcurrent=mlp;
		chans[chan].sendreqid=mlp->sml_msg_reqid;
		chans[chan].datatosendcnt=mlp->sml_msg_length;
		chans[chan].sendbuf=mlp->sml_msg_buf;
		chans[chan].sendcallback=mlp->sml_callbackfunction;
		chans[chan].datasentcnt=0;
		chans[chan].ppfd->events = chans[chan].ppfd->events | WRITEEVENT;	/* update poll flags */
		return (chans[chan].sendreqid);
	}
	else {
		chans[chan].datasentcnt=-1;
		chans[chan].datatosendcnt=-1;
		chans[chan].sendreqid=0;
		chans[chan].sendcurrent=NULL;
		chans[chan].sendbuf=NULL;
		chans[chan].sendcallback=NULL;
		chans[chan].ppfd->events =  chans[chan].ppfd->events & (~WRITEEVENT); /* reset poll flags */
		return (0);
	}
}


int snipe2_set_next_send_blank (int chan)
{
		chans[chan].datasentcnt=-1;
		chans[chan].datatosendcnt=-1;
		chans[chan].sendreqid=0;
		chans[chan].sendcurrent=NULL;
		chans[chan].sendbuf=NULL;
		chans[chan].sendcallback=NULL;
		chans[chan].ppfd->events =  chans[chan].ppfd->events & (~WRITEEVENT); /* reset poll flags */
		return (0);
}


int	snipe2_set_next_recv (int chan)
{
	struct snipe2_msg_list* mlp;

	if (chans[chan].pending_recvs){		/* if any recvs pending */

		mlp = snipe2_msg_list_head (chans[chan].recv_head);
		if (!mlp) return (-1); /* panic no message on queue? */

		/* unlink it from the recv queue */
		snipe2_msg_list_unlink (mlp);

		/* recv low level info */
		chans[chan].recvcurrent=mlp;
		chans[chan].recvreqid=mlp->sml_msg_reqid;
		chans[chan].datatorecvcnt=mlp->sml_msg_length;
		chans[chan].recvbuf=mlp->sml_msg_buf;
		chans[chan].recvcallback=mlp->sml_callbackfunction;
		chans[chan].datarecvcnt=0;
		chans[chan].ppfd->events = chans[chan].ppfd->events | READEVENT;	/* update poll flags */
		return (chans[chan].recvreqid);
	}
	else {
		chans[chan].datarecvcnt=-1;
		chans[chan].datatorecvcnt=-1;
		chans[chan].recvreqid=0;
		chans[chan].recvcurrent=NULL;
		chans[chan].recvbuf=NULL;
		chans[chan].recvcallback=NULL;
		chans[chan].ppfd->events = chans[chan].ppfd->events & (~READEVENT);	/* reset poll flags */
		return (0);
	}
}

int snipe2_set_next_recv_blank (int chan)
{
		chans[chan].datarecvcnt=-1;
		chans[chan].datatorecvcnt=-1;
		chans[chan].recvreqid=0;
		chans[chan].recvcurrent=NULL;
		chans[chan].recvbuf=NULL;
		chans[chan].recvcallback=NULL;
		chans[chan].ppfd->events = chans[chan].ppfd->events & (~READEVENT);	/* reset poll flags */
		return (0);
}




int	snipe2_do_send_on_all (int flags, int autonext)
{
	int c;
	int completed=0;

	if (!snipe2_total_pending_sends) return (0);

	/* step through all open chans */
	for (c=0;c<chan_top;c++) 
		if ((chans[c].state==1)&&(chans[c].pending_sends)&&(chan2_poll_list[c].revents&WRITEEVENT)) {
#ifdef VERBOSE
			printf("%d> ", c); 
#endif
		    chans[c].stat_send_polls_suc++;
			completed += snipe2_do_send (c, autonext, 0);
		}
#ifdef VERBOSE
	printf("\n");
#endif
	return (completed);
}


int	snipe2_do_send (int c, int autonext, int immediate)
{
	long sent;
	int completed=0;
	int done=0;
	int cbackrc;
	int err=0;

	while (!done) {

	   if ((chans[c].state==1)&&(chans[c].pending_sends)) {
		   sent = write (chans[c].fd, chans[c].sendbuf,
						   chans[c].datatosendcnt);
#ifdef VERYVERBOSE
printf("sent %d ", sent);
#endif
		   if (sent>0) {
			   chans[c].datatosendcnt-=sent;
			   if (chans[c].datatosendcnt) {
				   chans[c].datasentcnt+=sent;
				   chans[c].sendbuf+=sent;
			   }
			   else {
#ifdef VERBOSE
				   printf("Scomp %d req %d ", c, chans[c].sendreqid);
#endif
				  chans[c].sendcurrent->sml_msg_done = 1;
				  snipe2_msg_list_add_to_head (snipe2_done_list, chans[c].sendcurrent);
				  snipe2_done_cnt++;
				   chans[c].pending_sends--;
				   snipe2_total_pending_sends--;
				   completed++;
				   chans[c].stat_total_sends++;

					/* if call back function setup activate it */
				   if (chans[c].sendcallback) {
					  cbackrc = (chans[c].sendcallback)(chans[c].sendreqid, chans[c].sendcurrent, c, chans[c].sendcurrent->sml_uservalue);
					  /* if done ok and called callback, delete automatically */
				   }

				   if (chans[c].sendcurrent->sml_autodelete)
					  snipe2_isdone (chans[c].sendcurrent, 1);

				   /* only after updating counters do we get the next send op */
				   if (autonext)
				   	snipe2_set_next_send (c);
				   else
					snipe2_set_next_send_blank (c);

#ifdef FASTREPEAT
				   /* experimental queued send fast step operation */
				   if ((autonext)&&(chans[c].pending_sends)&&(!immediate)) {
#ifdef VERBOSE
					  fprintf(stderr,"snipe2_do_send: fast send on next\n");
#endif
				   		snipe2_repeat_sends [snipe2_repeat_send_cnt]=c;
						snipe2_repeat_send_cnt++;
				   }
#endif /* FASTREPEAT */


			   }
		   } /* if sent any data */
		   else {
			  if (!immediate) {
			  	printf(" s! %d ", c); /* no data sent and poll told use it could?? */
				err = snipe2_check_for_error_and_mark (c, sent);
				printf(" %1d ", err);
			  	sleep (1);
			  }
			  else {
				 /* immediate sends can get away with not sending data */
			  }
		   }
	   }

		return (completed);

	} /* while not done */
	return -1;  /* this one should never be reached */
}


int	snipe2_do_recv_on_all (int flags, int autonext)
{
	int c;
	int completed=0;
	int cbackrc;

	if (!snipe2_total_pending_recvs) return (0);

	/* step through all open chans */
	for (c=0;c<chan_top;c++) {
		if ((chans[c].state==1)&&(chans[c].pending_recvs)&&(chan2_poll_list[c].revents&READEVENT)&&(chans[c].type)) {
#ifdef VERBOSE
			printf(">%d ", c); 
#endif
		    chans[c].stat_recv_polls_suc++;
			completed += snipe2_do_recv (c, autonext, 0);
		}
		if ((chans[c].state)&&(chans[c].pending_recvs)&&(chan2_poll_list[c].revents&READEVENT)&&(!chans[c].type)) {
		  chans[c].recvcurrent->sml_msg_done++;
		  completed++;
			/* if call back function setup activate it */
		   if (chans[c].recvcallback) 
			  cbackrc = (chans[c].recvcallback)(chans[c].recvreqid, chans[c].recvcurrent, c, chans[c].recvcurrent->sml_uservalue);
		}
	} /* for loop */
#ifdef VERBOSE
	printf("\n");
#endif
	return (completed);
}

int	snipe2_do_recv (int c, int autonext, int immediate)
{
	long recvd;
	int completed=0;
	int done=0;
	int cbackrc;


	while (!done) {

		if ((chans[c].state==1)&&(chans[c].pending_recvs)) {
			/* printf("c %d ", c); */
			recvd = read (chans[c].fd, chans[c].recvbuf,
						chans[c].datatorecvcnt);
#ifdef VERYVERBOSE
printf("recvd %d ", recvd);
#endif
			if (recvd>0) {
				chans[c].datatorecvcnt-=recvd;
				if (chans[c].datatorecvcnt) {
					chans[c].datarecvcnt+=recvd;
					chans[c].recvbuf+=recvd;
				}
				else {
#ifdef VERBOSE
				    printf("Rcomp %d req %d ", c, chans[c].recvreqid);
#endif
					chans[c].recvcurrent->sml_msg_done = 1;
					snipe2_msg_list_add_to_head (snipe2_done_list, chans[c].recvcurrent);
					snipe2_done_cnt++;
					chans[c].pending_recvs--;
					snipe2_total_pending_recvs--;
					completed++;
				    chans[c].stat_total_recvs++;
					/* only after updating counters do we get the next recv op */
					/* if call back function setup activate it */
				   if (chans[c].recvcallback) {
			  		  cbackrc = (chans[c].recvcallback)(chans[c].recvreqid, chans[c].recvcurrent, c, chans[c].recvcurrent->sml_uservalue);
				   }

				   if (chans[c].recvcurrent->sml_autodelete) {
					  snipe2_isdone (chans[c].recvcurrent, 1);
				   }


				   if (autonext)
					snipe2_set_next_recv (c);
				   else
					snipe2_set_next_recv_blank (c);

#ifdef FASTREPEAT
				   /* experimental queued recv fast step operation */
				   if ((autonext)&&(chans[c].pending_recvs)&&(!immediate)) {
#ifdef VERBOSE
					  fprintf(stderr,"snipe2_do_recv: fast recv on next\n");
#endif
					  	snipe2_repeat_recvs[snipe2_repeat_recv_cnt] = c;
						snipe2_repeat_recv_cnt++;
				   }
#endif /*  FASTREPEAT */

				}
			} /* if recvd any data */
		   else {
			  if (!immediate) {
				(void)snipe2_check_for_error_and_mark (c, recvd);
				/* no data recvd after poll said there was?? */
/* 			  	printf(" r! %d  ", c);  */
/* 				printf(" %1d ", err); */
/* 			  	sleep (1); */
			  }
			  else {
				 /* failed to recv on immediate recv is ok */
			  }
		   }

		} /* if a valid channel and pending recvs */

	   	return (completed);

	} /* while not done */
	return -1; /* this one should never be reached */
}


/* error handling routines */


int		snipe2_check_for_error_and_mark (int chan, int lastrc)
{
  int copyerrno = errno;
  /* here we just the errno */
  
  if ((copyerrno==EINTR)||(copyerrno==EAGAIN)) return (0);
  /* ok, its a real error */
#ifdef VERBOSE
  if (copyerrno) perror("snipe2_check_for_error_and_mark");
  printf("snipe2_check_for_error_and_mark: chan %d lastrc %d errno %d\n", 
	 chan, lastrc, errno);
#endif /* VERBOSE */
  /* so we cheat and flip a POLL flag :) */
  /* not neat but the check for errors loop in progress will catch it */
  chans[chan].ppfd->revents = chans[chan].ppfd->revents | POLLERR;

  return (1);
}

int     snipe2_do_check_for_errors ()
{
  int c;
  int errors=0;

  if (!snipe2_error_checking) return (0);

  /* step through all open chans */
  for (c=0;c<chan_top;c++) {
    /* check each active channel that is of type DATA for an ERR event */
    if ((chans[c].state==1)&&(chans[c].type)&&  
	(chan2_poll_list[c].revents & (POLLERR|POLLHUP|POLLNVAL))) {
      fprintf(stderr,"Error event set for channel %d\n", c);
      
      /* the state of the channel is now changed to in ERROR */
      chans[c].state = -1;
      errors++;
      
      /* remove all the POLL event flags so we only catch the error */
      /* *** ONCE *** */
      chans[c].ppfd->events=0;
      
      /* if user set a call back on this channel */
      if (chans[c].errorcallback) {
	(void)(chans[c].errorcallback)(c,NULL,c,chans[c].erroruserval);
      }
      
    }
  }
  
  return (errors);
}

/*
 * dump chantents of channel 
 */

int	snipe2_dump_chan (int chan, int flags)
{
  if ((chan<0)||(chan>=MAXCHANS)) return (-1);
  if (chans[chan].state==0) {
    printf("dump: chan %d not in use.\n", chan);
    return (-1);
  }
  
  printf("Con %d on socket %d pending sends %d recvs %d\n",
	 chan, chans[chan].fd, chans[chan].pending_sends,
	 chans[chan].pending_recvs);
  printf("current send req %d bytes sent %ld tosend %ld\n",
	 chans[chan].sendreqid,
	 chans[chan].datasentcnt,
	 chans[chan].datatosendcnt);
  printf("current recv req %d bytes recvd %ld torecv %ld\n",
	 chans[chan].recvreqid,
	 chans[chan].datarecvcnt,
	 chans[chan].datatorecvcnt);
  
  printf("Overall stats. Totals sends completed %ld recvs completed %ld\n",
	 chans[chan].stat_total_sends, chans[chan].stat_total_recvs);
  printf("Total send polls %ld positive %ld blank %ld\n",
	 chans[chan].stat_send_polls, chans[chan].stat_send_polls_suc, 
	 chans[chan].stat_send_polls-chans[chan].stat_send_polls_suc);
  printf("Total recv polls %ld positive %ld blank %ld\n", 
	 chans[chan].stat_recv_polls, chans[chan].stat_recv_polls_suc, 
	 chans[chan].stat_recv_polls-chans[chan].stat_recv_polls_suc);
  return 0;
}


int	snipe2_dump_all_chans ()
{
  int i;
  
  printf("dump_all. Total pending sends %d total pending recvs %d\n",
	 snipe2_total_pending_sends, snipe2_total_pending_recvs);
  
  for (i=0;i<MAXCHANS;i++)
    if (chans[i].state) snipe2_dump_chan (i, 0);
  return (0);
}

int snipe2_dump_poll_flags ()
{
  int i;
  int r=0;
  int w=0;
  
  for (i=0;i<chan_top;i++) {
    if (chan2_poll_list[i].events & WRITEEVENT) {
      printf("W");
      w++;
    }
    else
      printf(" ");
  }
  printf("\n");
  for (i=0;i<chan_top;i++) {
    if (chan2_poll_list[i].events & READEVENT) {
      printf("R");
      r++;
    }
    else
      printf(" ");
  }
  printf("\n");

  /* others flags are illegal on the event field */  
  
  printf("POLL FLAGS write %d read %d active %d/%d\n", w, r, chan_top, MAXCHANS);
  return 0;
}

int snipe2_dump_poll_result ()
{
   int i;
   int r=0;
   int w=0;
   int e=0;

   for (i=0;i<chan_top;i++) {
	  if (chan2_poll_list[i].revents & WRITEEVENT) {
		 printf("W");
		 w++;
	  }
	  else
		 printf(" ");
   }
   printf("\n");
   for (i=0;i<chan_top;i++) {
	  if (chan2_poll_list[i].revents & READEVENT) {
		 printf("R");
		 r++;
	  }
	  else
		 printf(" ");
   }
   printf("\n");
   for (i=0;i<chan_top;i++) {
	  if ( chan2_poll_list[i].revents & (POLLERR|POLLHUP|POLLNVAL) ) {
		 printf("E");
		 e++;
	  }
	  else
		 printf(" ");
   }
   printf("\n");

   printf("POLL writeable %d readable %d on error %d active %d/%d\n", w, r, e,  chan_top,  MAXCHANS);
   return 0;
}

void snipe2_dump_other_lists ()
{
   printf("dumping other lists\n");

   printf("Done list\n");
   snipe2_msg_list_dump(snipe2_done_list);

   printf("Free list\n");
   snipe2_msg_list_dump(snipe2_msg_free_list);
}


