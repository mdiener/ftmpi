
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	          Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
    SNIPE_LITE_S is secure version of the sniple_lite using SSL
*/

#ifdef USE_OPENSSL

#include <openssl/ssl.h>
#include "snipe_lite.h"

#define RETRIES 2

int closeconn_s (SSL *ssl, int s)
{
   int err;
   err=SSL_shutdown(ssl);
   switch(err) {
      case 1: /* complete shutdown */
      case 0: /* incomplete shutdown */
      case -1: /* error */
      default: /* oops */
   }
   close (s);
}


/* sends stream data. I.e. no header/seq stuff */	

int writeconn_s (SSL *s,char * data,int len) 
{
   /* We don't need to loop because the semantics of SSL_write() are all or nothing */ 
    return SSL_write(s,data,len);
}

int readconn_s (SSL *s,char * data,int len)/* reads stream data. I.e. no header/seq stuff */	
/*                  return is read length or error */
/* int	s;          socket conn is on */
/* char *data;      raw data buffer */
/* int len;         length of data tobe read */
{
int i,j,k;
int n;
char bb;                        /* Bit Bucket character */
unsigned int toget;  			/* note type */
int got, fluffed;               /* loop variables */


toget = (unsigned) len;
fluffed = 0;

#ifdef DB9
printf("read on [%d] to get msg length [%d]\n", s, toget);
#endif

for (i=0;toget>0;) {
#ifdef DB9
    printf("Reading on [%d] amount [%d]\n", s, toget);
#endif /* DB9 */

    got = SSL_read (s, &data[i], toget);

#ifdef DB9
    printf("reading on [%d] got [%d] bytes.\n", s, got);
#endif /* DB9 */

    if (got<=0) {
        fluffed++;
        if (fluffed==RETRIES) {
            fprintf(stderr,"Problem, connection on socket [%d] has failed.\nThis connect has been closed.\n", s); fflush(stderr);
            close (s);
            return (i); /* how much we got in the end... */
                        /* upto the user app to realise the error. */
        } /* if fluffed */
    }
    else { /* i.e. we got */
        i+= got;    /* total got and index ptr update */
        toget -= got;
        fluffed = 0;    /* fluffed reset counter */
    } /* if got */
} /* for look on toget */

/* ok all done... now return amount read */
return (i);



}

#endif

/* The foo function is declare to prevent segmentation fault
 *  * while compiling this file of some compiler */
void snipe_foo(void)
{

}
