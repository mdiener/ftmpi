
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Thara Angskun	 <angskun@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



/**
 * Basic functions to create a daemon.
 *
 * @file daemon.c
 * @see UNP book
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/param.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>

#ifdef	SIGTSTP		/* true if BSD or LINUX system */
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#endif


extern int errno; /**< error number */

short ignsigcld = 1; /**< ignore SIGCHLD */

void err_sys(char* err_string);
void sigchild_handler(int i);


/**
 * Initialize daemon.
 *
 * @retval 0 success
 * @retval otherwise error
 * @note file descriptor 0-2 are not closed
 * @warning ignore SIGCHLD is default
 */
int daemon_init()
{
	register int	childpid, fd;
	int fds = 0;

	/*
	 * If we were started by init (process 1) from the /etc/inittab file
	 * there's no need to detach.
	 * This test is unreliable due to an unavoidable ambiguity
	 * if the process is started by some other process and orphaned
	 * (i.e., if the parent process terminates before we are started).
	 */

	if (getppid() == 1) {}
		/*goto out;*/

	/*
	 * Ignore the terminal stop signals (BSD, Linux).
	 */

#ifdef SIGTTOU
	signal(SIGTTOU, SIG_IGN);
#endif
#ifdef SIGTTIN
	signal(SIGTTIN, SIG_IGN);
#endif
#ifdef SIGTSTP
	signal(SIGTSTP, SIG_IGN);
#endif

	/*
	 * If we were not started in the background, fork and
	 * let the parent exit.  This also guarantees the first child
	 * is not a process group leader.
	 */

	if ( (childpid = fork()) < 0)
		err_sys("daemon : can't fork first child");
	else if (childpid > 0)
		exit(0);	/* parent */

	/*
	 * First child process.
	 *
	 * Disassociate from controlling terminal and process group.
	 * Ensure the process can't reacquire a new controlling terminal.
	 */

#ifdef NO_SETSID
#ifdef	SYSV  /* System V */

	if (setpgrp() == -1)
		err_sys("can't change process group");

	signal(SIGHUP, SIG_IGN);	/* immune from pgrp leader death */

	if ( (childpid = fork()) < 0)
		err_sys("can't fork second child");
	else if (childpid > 0)
		exit(0);	/* first child */

	/* second child */

#elif defined(BSD)||defined(__linux__) /* BSD or Linux*/

	if (setpgrp() == -1)
		err_sys("can't change process group");

	if ( (fd = open("/dev/tty", O_RDWR)) >= 0) {
		ioctl(fd, TIOCNOTTY, (char *)NULL); /* lose controlling tty */
		close(fd);
	} /*if*/

#endif
#else
	if (setsid() == -1)
		err_sys("can't create new session");
#endif

	/* FIXME : Add some pid file open session here */

out:
	/*
	 * Close any open files descriptors.
	 */

#ifndef HAVE_GETDTABLESIZE
	fds = NOFILE;
#else
	fds = getdtablesize();
#endif
	for (fd = 0; fd < fds; fd++)
		close(fd);

#ifdef _PATH_DEVNULL
	freopen(_PATH_DEVNULL, "w", stderr);
#endif

	errno = 0;		/* probably got set to EBADF from a close */

	/*
	 * Move the current directory to root, to make sure we
	 * aren't on a mounted filesystem.
	 */

	/*
	chdir("/");
	*/

	/*
	 * Clear any inherited file mode creation mask.
	 */

	/*
	umask(0);
	*/
	umask(022);

	/*
	 * See if the caller isn't interested in the exit status of its
	 * children, and doesn't want to have them become zombies and
	 * clog up the system.
	 * With System V all we need do is ignore the signal.
	 * With BSD, however, we have to catch each signal
	 * and execute the wait3() system call.
	 */

	if (ignsigcld) {
#ifdef	SYSV
		signal(SIGCLD, SIG_IGN);	/* System V */
#elif defined(BSD)||defined(__linux__)
		signal(SIGCLD, sigchild_handler);	/* BSD and Linux*/
#endif
	}

	return 0;
}

/**
 * Fatal error handler
 *
 * @internal
 */
void err_sys(char* err_string) {
   fputs(err_string, stderr);
   fputs("\n", stderr);
   exit(1);
}

/**
 * SIGCHLD handler
 */
void sigchild_handler(int i) {
#if defined(BSD)||defined(__linux__)
	/*
	 * Use the wait3() system call with the WNOHANG option.
	 */

	int	pid, status;

	while ( (pid = wait3(&status, WNOHANG, NULL)) > 0);
#endif
} 
