
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* some code was borrowed from PVM, thanks to Robert Manchek */

/* local includes */
#ifndef MSGLISTH
#include "snipe2-msg-list.h"
#endif


#ifndef LISTSH
#define LISTSH


/* MACROS required to manipulate the list structures */
/* These macros are originaly from a {broken} PVM3.3 tasker demo. */ 
/* Used with thanks to Bob Manchek, who made PVM what it is today ;) */

#define LISTPUTAFTER(o,n,f,r) \
  { (n)->f=(o)->f; (n)->r=o; (o)->f->r=n; (o)->f=n; }
#define LISTPUTBEFORE(o,n,f,r) \
    { (n)->r=(o)->r; (n)->f=o; (o)->r->f=n; (o)->r=n; }
#define LISTDELETE(e,f,r) \
	  { (e)->f->r=(e)->r; (e)->r->f=(e)->f; (e)->r=(e)->f=0; }

#define TALLOC(n,t,g) (t*)malloc((n)*sizeof(t))
#define FREE(p) free((char *)p)
#define STRALLOC(s)    strcpy(TALLOC(strlen(s)+1,char,"str"),s)

#endif /* LISTSH */




/* Typical PVM setup */
/*#ifdef HASSTDLIB */
#include <stdlib.h>
/*#endif */

#if defined(SYSVBFUNC)
#include <memory.h>
#define BZERO(d,n)      memset(d,0,n)
#define BCMP(s,d,n)     memcmp(d,s,n)
#define BCOPY(s,d,n)    memcpy(d,s,n)

#else
#define BZERO(d,n)      bzero(d,n)
#define BCMP(s,d,n)     bcmp(s,d,n)
#define BCOPY(s,d,n)    bcopy(s,d,n)
#endif

#include <stdio.h>	/* just for the dump instructions */
#include <string.h> /* needed for strcmp when checking local,remote names */

/* struct must be of this form */
/*
 * struct type {
 *		struct type *t_link, *t_rlink;
 *		int t_data0;
 *		int t_data1;
 *        .
 *		int t_dataN;
 * };
 */

/* start data lists off this way */
/* struct snipe2_msg_list *sml_head = 0; */

/* the structures used here, contain an empty first element that links to */
/* itself. */


/* we implement a free list */
/* free list stuff */
struct snipe2_msg_list * snipe2_msg_free_list=NULL;
/* was static but now exported to snipe2.c */
static long   msgfreecnt=0;
static long   maxmsgfree=0;


/* we need to return this first element so that it can be added to the */
/* communicator (com_list) that owns it */

struct snipe2_msg_list *
snipe2_msg_list_init()
{
struct snipe2_msg_list *first;

	first = TALLOC(1, struct snipe2_msg_list, 0); 
	first->sml_link = first->sml_rlink = first; /* all points to me */
	return (first);
}


/* 
 *  This creates a free list of msg requests of length INITREQS 
 */
void snipe2_msg_list_init_freelist(int initelements, int maxelements)
{
int i;
struct snipe2_msg_list *smlp;

snipe2_msg_free_list = snipe2_msg_list_init();
msgfreecnt = 0;
maxmsgfree = maxelements;

for(i=0;i<initelements;i++)  {
   /* make a new * entry */ 

   /* cannot use new here as it will get them off the free list DUD! */ 
   /*          smlp = *          msg_list_new(snipe2_msg_free_list); *          */

	if (!(smlp = TALLOC(1, struct snipe2_msg_list, 0))) {
	    fprintf(stderr, "snipe2_msg_list_init_freelist() can't get memory\n");
   		exit(-1);
		}
	LISTPUTBEFORE(snipe2_msg_free_list, smlp, sml_link, sml_rlink);
																		             /* make sure item knows it free */
	smlp->sml_msg_reqid = 0;
	smlp->sml_msg_id = 0;
	smlp->sml_msg_priority = 0;
	smlp->sml_msg_chan = 0;
	smlp->sml_msg_type = 0;
	smlp->sml_msg_done = 0;
	smlp->sml_msg_buf = NULL;
	smlp->sml_msg_length = 0;
	smlp->sml_callbackfunction = NULL;
	smlp->sml_uservalue = 0;

	msgfreecnt++;
	}
}



struct snipe2_msg_list *
snipe2_msg_list_tail (sml_head)
struct snipe2_msg_list *sml_head;
{
	if (sml_head->sml_rlink != sml_head) return (sml_head->sml_rlink);
	else return ((struct snipe2_msg_list *)0);
}

struct snipe2_msg_list *
snipe2_msg_list_head (sml_head)
struct snipe2_msg_list *sml_head;
{
	if (sml_head->sml_link != sml_head) return (sml_head->sml_link);
	else return ((struct snipe2_msg_list *)0);
}


/*	snipe2_msg_list_new()
*
*	Make a new snipe2_msg_list descriptor, adds to list at the tail.
* This makes sure that message order is preserved.
* Returns the pointer to the new item so that it can be updated/filled in 
* Gets it from the free list if possible
*
*
*/

struct snipe2_msg_list *
snipe2_msg_list_new (sml_head)
struct snipe2_msg_list *sml_head;
{
	struct snipe2_msg_list *smlp;

	if (msgfreecnt) {
		/* get of tail of free list */
		smlp = snipe2_msg_list_tail (snipe2_msg_free_list);  

        if (!smlp) return (smlp);

		LISTDELETE(smlp, sml_link, sml_rlink); /* detaches from free list */
	    msgfreecnt--;

		LISTPUTBEFORE(sml_head, smlp, sml_link, sml_rlink); /* adds to new tail */
	    return (smlp);
    }

	if (!(smlp = TALLOC(1, struct snipe2_msg_list, 0))) {
		fprintf(stderr, "snipe2_msg_list_new() can't get memory\n");
		exit(1);
	}
	smlp->sml_msg_reqid = 0;
	smlp->sml_msg_id = 0;
	smlp->sml_msg_priority = 0;
	smlp->sml_msg_chan = 0;
	smlp->sml_msg_type = 0;
	smlp->sml_msg_done = 0;
	smlp->sml_msg_buf = NULL;
	smlp->sml_msg_length = 0;
	smlp->sml_callbackfunction = NULL;
	smlp->sml_uservalue = 0;

	LISTPUTBEFORE(sml_head, smlp, sml_link, sml_rlink);

	return smlp;
}


/*
* Note here we don't change the msgs in list counter. This has to be done by
* the calling routine as it will have access to this (being as its in the
* conn_list structures anyway).
*
*/
void
snipe2_msg_list_free(smlp)
	struct snipe2_msg_list *smlp;
{
	LISTDELETE(smlp, sml_link, sml_rlink); /* detach it */
    if (msgfreecnt<maxmsgfree) { /* we can add it back to the free list */
		LISTPUTBEFORE(snipe2_msg_free_list, smlp, sml_link, sml_rlink);
	   smlp->sml_msg_reqid = 0;
	   smlp->sml_msg_id = 0;
	   smlp->sml_msg_priority = 0;
	   smlp->sml_msg_chan = 0;
	   smlp->sml_msg_type = 0;
	   smlp->sml_msg_done = 0;
	   smlp->sml_msg_buf = NULL;
	   smlp->sml_msg_length = 0;
	   smlp->sml_callbackfunction = NULL;
	   smlp->sml_uservalue = 0;

	    msgfreecnt++;
	}
    else
		FREE(smlp);
}

/*
 * the following just unlinks a element.. its upto the user to make
 * sure its freed or released or whatever and not lost...
 */

void
snipe2_msg_list_unlink(smlp)
	struct snipe2_msg_list *smlp;
{
	LISTDELETE(smlp, sml_link, sml_rlink);
}

/*
 * this routine takes the head off one list and puts it on the other lists tail
 * It does not change any counters, but it does return the handle to what
 * got moved if anything
 */

struct snipe2_msg_list* 
snipe2_msg_list_movehead_to_tail (fromsmlp, tosmlp)
	struct snipe2_msg_list *fromsmlp;
	struct snipe2_msg_list *tosmlp;
{
	struct snipe2_msg_list *smlp;

	smlp = snipe2_msg_list_head (fromsmlp); /* get the element/head */

	if (!smlp) return (smlp); /* no entry to move */

	snipe2_msg_list_unlink (smlp); /* disconnect it */

	LISTPUTAFTER(tosmlp, smlp, sml_link, sml_rlink);	/* add to tail */
	return (smlp);
}


/* does not create, just adds to a list */
void 
snipe2_msg_list_add_to_tail (head, smlp)
	struct snipe2_msg_list *head;
	struct snipe2_msg_list *smlp;
{
	if (!smlp) return; /* no entry to move */

	LISTPUTBEFORE(head, smlp, sml_link, sml_rlink);	/* add to tail */
	return;
}

/* does not create, just adds to a list */
void 
snipe2_msg_list_add_to_head (head, smlp)
	struct snipe2_msg_list *head;
	struct snipe2_msg_list *smlp;
{
	if (!smlp) return; /* no entry to move */

	LISTPUTAFTER(head, smlp, sml_link, sml_rlink);	/* add to head */
	return;
}








void snipe2_msg_list_destroy (sml_head)
struct snipe2_msg_list *sml_head;
{
struct snipe2_msg_list *smlp;

if (!sml_head) return;	/* if bad list return rather than deadlock */

/* loop taking the head and freeing it, when no head, throw empty */
/* record place holder away as well and then return */
while (1) {
	smlp = snipe2_msg_list_head (sml_head);
	if (smlp) snipe2_msg_list_free (smlp);
	else {
		free ((char *) sml_head);
		return;
		}	
	}

}





/*
* This routine is used to find messages by their msg_id.
* Cancelling messages is an example of this routines use.
*/
struct snipe2_msg_list *
snipe2_msg_list_find_by_msg_id (sml_head, req_id)
struct snipe2_msg_list * sml_head;
	int req_id;
{
	struct snipe2_msg_list *smlp;
	
	for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
		if(req_id==smlp->sml_msg_id)
			return( smlp );

	/* if none of them, then ops! */	
	return (struct snipe2_msg_list*)0;	/* no match, sorry */
}

/*
* This routine is used to find messages by their msg request.
*/
struct snipe2_msg_list *
snipe2_msg_list_find_by_request (sml_head, req)
struct snipe2_msg_list * sml_head;
	int req;
{
	struct snipe2_msg_list *smlp;
	
	for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
		if(req==smlp->sml_msg_reqid)
			return( smlp );

	/* if none of them, then ops! */	
	return (struct snipe2_msg_list*)0;	/* no match, sorry */
}


/*
* This routine is used to find the next message by their msg priority.
*/
struct snipe2_msg_list *
snipe2_msg_list_find_by_priority (sml_head, pri)
struct snipe2_msg_list * sml_head;
	int pri;
{
	struct snipe2_msg_list *smlp;
	
	for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
		if(pri==smlp->sml_msg_priority)
			return( smlp );

	/* if none of them, then ops! */	
	return (struct snipe2_msg_list*)0;	/* no match, sorry */
}

void
snipe2_msg_list_dump(sml_head)
struct snipe2_msg_list * sml_head;
{
	struct snipe2_msg_list *smlp;

	for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link) {
		printf("Req %d msg_id %d Priority %d Buf 0x%p Msg_Len %ld Done %1d ",
				smlp->sml_msg_reqid, 
				smlp->sml_msg_id, 
				smlp->sml_msg_priority,
				smlp->sml_msg_buf, smlp->sml_msg_length,
				smlp->sml_msg_done);
		if (smlp->sml_callbackfunction) printf("Cback Val %ld\n", smlp->sml_uservalue);
		else
		   printf("No CB\n");
		}

}





































#ifdef for_a_rainy_day_spare_code
/* fast function that just return true if it matches the header */

int msg_hdr_match (req_rank, req_tag, sender, tag) 
int req_rank, req_tag, sender, tag;

{

	if((req_tag==MPI_ANY_TAG)&&(req_rank==MPI_ANY_SOURCE)) return(1);

	if((req_tag==MPI_ANY_TAG)&&(req_rank==sender)) return (1);

	if((req_rank==MPI_ANY_SOURCE)&&(req_tag==tag)) return (1);

	if((req_rank==sender)&&(req_tag==tag)) return (1);

	return (0);
}
	
/*	snipe2_msg_list_find_by_header (head, rank, tag)
*
*	Find a msg by the recv tag and senders rank as required.
* Handles wildcards correctly. (Although this should be done higher up
* to save on stack push/pulls and a few ifs + cache misses etc etc)
*
*/

struct snipe2_msg_list *
snipe2_msg_list_find_by_header (sml_head, req_rank, req_tag)
struct snipe2_msg_list * sml_head;
	int req_rank;
	int req_tag;
{
	struct snipe2_msg_list *smlp;
	
	if((req_tag==MPI_ANY_TAG)&&(req_rank==MPI_ANY_SOURCE)) 	/* take off head */
		return( snipe2_msg_list_head(sml_head) );

	if(req_tag==MPI_ANY_TAG) 	/* then search my source/rank */
		for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
				if (smlp->sml_senders_rank==req_rank) return smlp;

	if(req_rank==MPI_ANY_SOURCE) 	/* then search my tag */
		for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
				if (smlp->sml_mpi_tag==req_tag) return smlp;

	/* else just pattern match on both.. this should be neater TODO[] */
	for (smlp = sml_head->sml_link; smlp != sml_head; smlp = smlp->sml_link)
		if ((smlp->sml_mpi_tag==req_tag)&&(smlp->sml_senders_rank==req_rank))
				return smlp;
	
	/* if none of them, then ops! */	
	return (struct snipe2_msg_list*)0;	/* no match, sorry */
}
#endif
