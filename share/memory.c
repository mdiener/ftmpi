#if !defined(NDEBUG)
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct __mem_allocator smemAllocator_t;

#define VALIDATOR 0xdeadbeaf

struct __mem_allocator {
  smemAllocator_t* prev;
  smemAllocator_t* next;
  unsigned int lineno;
  unsigned int validator;
  char* pFileName;
  unsigned int length;
  int index;
};

static int __alloc_index = 0;
static smemAllocator_t* pdllmem = NULL;

void* ftmpi_malloc( size_t size, char* file, int lineno )
{
    size_t totalLength = 0;
    smemAllocator_t* pTemp;

    totalLength = sizeof( smemAllocator_t ) + size;
    pTemp = (smemAllocator_t*)malloc( totalLength );
    if( pTemp == NULL ) return NULL;
    pTemp->length = size;
    pTemp->validator = VALIDATOR;
    pTemp->pFileName = file;
    pTemp->lineno = lineno;
    pTemp->index = __alloc_index++;

    if( pdllmem == NULL ) {
      pdllmem = pTemp;
      pTemp->next = pTemp;
      pTemp->prev = pTemp;
    } else {
      pdllmem->prev->next = pTemp;
      pTemp->prev = pdllmem->prev;
      pTemp->next = pdllmem;
      pdllmem->prev = pTemp;
    }
    return (void*)&pTemp[1];
}

void* ftmpi_calloc( size_t number, size_t size, char* file, int lineno )
{
    size_t totalLength = 0;
    smemAllocator_t* pTemp;

    totalLength = sizeof( smemAllocator_t ) + number * size;
    pTemp = (smemAllocator_t*)calloc( 1, totalLength );
    pTemp->lineno = lineno;
    pTemp->validator = VALIDATOR;
    pTemp->length = size;
    pTemp->pFileName = file;

    if( pdllmem == NULL ) {
      pdllmem = pTemp;
      pTemp->next = pTemp;
      pTemp->prev = pTemp;
    } else {
      pdllmem->prev->next = pTemp;
      pTemp->prev = pdllmem->prev;
      pTemp->next = pdllmem;
      pdllmem->prev = pTemp;
    }
    return (void*)&pTemp[1];
}

void* ftmpi_realloc( void* ptr, size_t size, char* file, int lineno )
{
  size_t totalLength = 0;
  smemAllocator_t* pTemp = (smemAllocator_t*)((char*)ptr - sizeof(smemAllocator_t));
  smemAllocator_t *prev, *next, *old;
  
  prev = pTemp->prev;
  next = pTemp->next;
  old = pTemp;
  
  totalLength = sizeof( smemAllocator_t ) + size;
  pTemp = (smemAllocator_t*)realloc( pTemp, totalLength );
  pTemp->length = size;
  pTemp->validator = VALIDATOR;
  pTemp->pFileName = file;
  pTemp->lineno = lineno;

  if( pdllmem == old ) {
    if( prev == old ) {
      assert( next == old );
      prev = next = pTemp;
    }
    pdllmem = pTemp;
  }
  prev->next = pTemp;
  next->prev = pTemp;
  pTemp->prev = prev;
  pTemp->next = next;
  return (void*)&pTemp[1];
}

void ftmpi_free( void* ptr, char* file, int lineno )
{
  smemAllocator_t* pTemp = (smemAllocator_t*)((char*)ptr - sizeof(smemAllocator_t));

  /* remove it from the allocated memory */
  assert( pTemp->prev->next == pTemp );
  assert( pTemp->next->prev == pTemp );
  pTemp->prev->next = pTemp->next;
  pTemp->next->prev = pTemp->prev;
  if( pTemp == pdllmem ) {
    if( pTemp->next == pTemp ) {
      assert( pTemp->prev == pTemp );
      pdllmem = NULL;
    } else {
      pdllmem = pTemp->next;
    }
  }
  assert( pTemp->validator == VALIDATOR );
  pTemp->validator = 0;
  
  /* And now really free the memory */
  free( pTemp );
}

void ftmpi_display_memory_usage( void )
{
  smemAllocator_t* pTemp = pdllmem;
  int totalSize = 0, chunks = 0;

  if( pTemp == NULL ) return;
  printf( ">> BEGIN MEMORY USAGE and TRACE\n" );
  do {
    printf( "Allocate %d bytes in file %s at line %d pointer %p\n",
	    pTemp->length, pTemp->pFileName, pTemp->lineno,
	    ((char*)pTemp + sizeof(smemAllocator_t)) );
    chunks++;
    totalSize += pTemp->length;
    pTemp = pTemp->next;
  } while( pTemp != pdllmem );
  printf( "Allocated %d chunks of memory with the total size of %d bytes\n",
	  chunks, totalSize );
  printf( ">> END MEMORY USAGE and TRACE\n" );
}

#endif  /* NDEBUG */
