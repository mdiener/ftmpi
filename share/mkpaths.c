
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/





/* this code makes a path structure if it doesn't already exist */

/* two versions of this */
/* one that is just passed the full path name of the directory */

/* the other is passed the full path and file name and just makes */
/* the directory so that the file can be created */

/* G.E.Fagg Nov 2000. */
/* fagg@hlrs.de */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

int	check_mk_dir (char *pname)
{
struct stat sbuf;
int cc;
/* int errno; */

printf("check_mk_dir on [%s]\n", pname);

if ((cc = stat (pname, &sbuf)) <0) {
	if (errno==EACCES) {
		perror ("stat");
		printf("[%s] cannot be accessed\n", pname); 
		return (-1);
		}
	if (errno==ENOENT) {	/* does not exist */
		perror ("stat");
		printf("[%s] does not exist. Making.\n", pname);


		/* first we need a mode mask value */


		/* now we can attempt to make a directory... */

		cc = mkdir (pname, (mode_t) -1);
		if (cc<0) {	/* if we didn't make it... */
			perror ("mkdir");
			if (errno==EACCES) {
				printf("[%s] access denied on create.\n", pname); 
				return (-1);
				}
			else {
				printf("[%s] unknown reason for non-creation.\n", pname);
				return (-1);
				}
			}
		/* we made it */
		return (0);
		}

	printf("[**%d**]\n", errno);
	perror ("stat");
	printf("[%s] could not be checked by stat.\n", pname);		
	return (-1);	/* cannot check it, cannot make or break it */
}

return (0);	/* path exists and is accessable */

}


int	mk_path (char *fpath,int all)
/* if all = 0 then last subpart of fpath is a filename not a directory */
/* if all = 1 then fpath is just a pathname */
{
int i, j;
int len;
int cnt;
int last;
char tchar;
char subpath[4096];	/* bad fixed path length !! */

/* do it the lazy way.... */
/* break the path down into individual sub-paths and build each */
/* could do it recursively, but that would make me look like a CS person */


/* first check is for null and non full paths */

if (!fpath) return (-1);

len = strlen (fpath);
if (len==0) return (-2);

#ifdef WIN32
	if (fpath[1]!=':') return (-2);	/* as in X:\ */
#else
	if (fpath[0]!='/') return (-2);
#endif

/* ok full path maybe.. count the number of parts */

/* 	tchar = SEPCHAR	 */
	tchar = '/';	

len = strlen (fpath);

cnt = 0;
last=0;
for (i=0;i<len;i++) 
	if (fpath[i]==tchar) {
		cnt++;	
		last=i;
		}

if (!cnt) return (-2);	/* shouldn't happen here except for win32 case */

if (!all) {	/* if the last part is a filename then we only */
			/* want the cnt-1 parts of the path :) */
			cnt--;

			if (!cnt) return (-9);	/* i.e. no directory other than / ? */

			/* ok reset the length of the string for the below search */
			len = last;

	}



printf("subpath parts %d\n", cnt);

/* build paths */
subpath[0]='\0';
last = 0;
for (i=0;i<cnt;i++)	{ /* for each path */
	if (last>=len) break;
	for(j=last+1;j<len;j++)	/* search for the end of the [sub]path */
		if (fpath[j]==tchar) break;	
	/* so the sub path is the last path built + fpath[last->j-1] */

/*  	printf("subpath was [%s], last %d j %d len %d\n", subpath, last, j, len); */


	strncat (subpath, (char*)(fpath+last), (j-last));

/* 	printf("new subpath [%s]\n", subpath); */

	i = check_mk_dir (subpath);

	if (i<0) { 	/* i.e. a path could not be made */
		printf("The path [%s] does not exist and cannot be made.\n", subpath);
		return (-3);	
		}

	last = j;
	}

return (0);


}


int	check_path (char *pname)
{
  struct stat sbuf;
  /* int errno; */

  if (!pname) return (-1);	/* NULL check */

  printf("check_path on [%s]\n", pname);

  if (stat (pname, &sbuf) <0) {
     return (-1);
  }
  return (0);
}

