#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include "debug.h"

typedef struct __log_struct {
  int fd;
  int fPos;
  FILE* fp;
  size_t bufSize;
  int pos;
  char* memory;
  char* name;
} log_t;

int log_printf( log_t* pLog, const char* fmt, ... );
int log_flush( log_t* pLog );

static log_t* __log_create( size_t bufSize )
{
  log_t* pLog = (log_t*)_CALLOC(1, sizeof(log_t) );

  pLog->bufSize = bufSize;
  pLog->pos = 0;
  pLog->fPos = 0;
  
  /* now we have the file. Lets get the buffer in the memory */
  if( bufSize != 0 ) {
    pLog->memory = (char*)_MALLOC( bufSize );
    if( pLog->memory == NULL ) pLog->bufSize = 0;
  }
  return pLog;
}

static void __log_destroy( log_t* pLog )
{
  if( pLog ==  NULL ) return;
  if( pLog->memory != NULL ) _FREE( pLog->memory );
  if( pLog->name != NULL ) _FREE( pLog->name );
}

/* inline log_t* log_attach( FILE* f, size_t bufSize ) */
log_t* log_attach( FILE* f, size_t bufSize )
{
  log_t* pLog = __log_create( bufSize );

  pLog->fp = f;
  return pLog;
}

log_t* log_open( char* fname, size_t bufSize )
{
  log_t* pLog;
  FILE* f;

  if( (f = fopen( fname, "w" )) == NULL ) {
    /* redirect the log to the stderr */
    pLog = log_attach( stderr, bufSize );
    log_printf( pLog, "Unable to open the requested log file %s\n", fname );
    return pLog;
  }
  pLog = log_attach( f, bufSize );
  pLog->name = _CALLOC( 1, strlen(fname) + 1 );
  strcpy( pLog->name, fname );
  return pLog;
}

int log_close( log_t* pLog )
{
  log_flush( pLog );
  if( pLog->name != NULL )
    if( fclose(pLog->fp) == EOF ) perror( "log_close" );
  pLog->fp = NULL;
  __log_destroy( pLog );
  return 0;
}

int log_flush( log_t* pLog )
{
  size_t length;
  
  if( pLog->bufSize == 0 ) { /* direct write in the file */
    return fflush( pLog->fp );
  }
  if( pLog->pos == 0 ) return 0; /* dont have to sync anything */
  if( (length = fwrite( pLog->memory, 1, pLog->pos, pLog->fp )) != pLog->pos ) {
    perror( "log_flush" );
    assert(0);
  } /* something wrong ? the changes will be discarded */
    /* now flush the file */
  fflush( pLog->fp );
  /* and move file position in the log struct */
  pLog->fPos += pLog->pos;
  pLog->pos = 0;
  return 0;
}

/* inline int log_vprintf( log_t* pLog, const char* fmt, va_list vlist ) */
int log_vprintf( log_t* pLog, const char* fmt, va_list vlist )
{
  int wbytes;
  
 begin_write:
  if( pLog->bufSize != 0 ) {
    wbytes = vsnprintf( pLog->memory + pLog->pos, (pLog->bufSize - pLog->pos), fmt, vlist );
    if( pLog->bufSize > (wbytes + pLog->pos) ) {
      pLog->pos += wbytes;
    } else {
      assert( wbytes < pLog->bufSize );
      log_flush( pLog );
      goto begin_write;
    }
  } else { /* write directly to the file */
    wbytes = vfprintf( pLog->fp, fmt, vlist );
    pLog->fPos += wbytes;
  }
  return wbytes;
}

int log_printf( log_t* pLog, const char* fmt, ... )
{
  va_list vlist;
  int wbytes;
  
  va_start( vlist, fmt );
  wbytes = log_vprintf( pLog, fmt, vlist );
  va_end( vlist );
  return wbytes;
}
