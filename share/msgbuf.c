
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* msg buffer handling routines */

/*
	message packing/unpacking routines

*/


#include <stdlib.h>
#include <stdio.h>
#include<string.h>

typedef struct {

	int	msg_buff_id;	/* ID of this buffer */

	void*	base_ptr;	/* start of my memory */
	void*	data_ptr;	/* location of where next data will go */
	void*	from_ptr;	/* location of where to get the next data from */

	/* counters */

	long	size;		/* total size of this buffer */
	long	len;		/* total amount already packed */
	long	space;		/* how much space we have left */
						/* yep, size=len+space */

	long	toend;		/* how many bytes till the end when unpacking :) */

	/* specialised counters */

	int		in_use;		/* zero if free, else 1 */
	long	times_sent;	/* inc each time we send with it, not used? */	
	long	times_freed; /* how many times has this buffer been freed */
	long	times_resized; /* how many times has this buffer been resized */
	int		resizable;	/* whether we are allowed to remalloc if needed */

}	msg_cb_t;



/* how many buffers */
#define 	MAX_MSG_BUFS	20

/* how many of above are above minimal size */
#define 	LARGER_MSG_BUFS	15

/* how many of above are resizable */
#define		RESIZE_MSG_BUFS	20

/* the buffer control blocks */
msg_cb_t	msg_buf	[MAX_MSG_BUFS];

int	msg_convert_type;

/* initial size */
#define 	MSG_BUF_INIT_SIZE	64

/* after resizing, limit at which we must resize back */
#define MSG_BUF_RESIZE_LIMIT (1024*128)

/* when creating lots of buffers the inital limit on buffer sizes */
#define MSG_BUF_INIT_SIZE_LIMIT (1024*128)

int	msg_bufs_free;
int msg_buf_last_freed; 	/* used to allow for better caching when looking for a free buffer */

int msg_buf_init_called = 0;

int check_buf (int buf);
int	_msg_downsize_buf (int buf );
int	_msg_resize_buf (int buf,long incsize );


/* call once */
int init_msg_bufs ()
{
int i;
int j;
long s;
void *p;

if (msg_buf_init_called) return (MAX_MSG_BUFS);	/* silly goose */
else
	msg_buf_init_called = 1;

for (i=0;i<MAX_MSG_BUFS;i++) {

	if (i<(MAX_MSG_BUFS-LARGER_MSG_BUFS)) s = MSG_BUF_INIT_SIZE;
	else /* make various size buffers */
	   		/* make one resizable of smallest size */
	   if (i==(MAX_MSG_BUFS-LARGER_MSG_BUFS)) s = MSG_BUF_INIT_SIZE;
		else s *= 2;
/* 		s = MSG_BUF_INIT_SIZE * ((i-(MAX_MSG_BUFS-LARGER_MSG_BUFS))+1); */

	if (s>MSG_BUF_INIT_SIZE_LIMIT) s = MSG_BUF_INIT_SIZE_LIMIT;

	p = malloc (s);
	if (!p) break;	/* escape so we can continue */

	msg_buf[i].msg_buff_id = i;
	msg_buf[i].base_ptr = p;
	msg_buf[i].data_ptr = p;
	msg_buf[i].from_ptr = p;
	msg_buf[i].size = s;
	msg_buf[i].len = 0;
	msg_buf[i].space = s;
	msg_buf[i].toend = 0;
	msg_buf[i].in_use = 0;
	msg_buf[i].times_sent = 0;
	msg_buf[i].times_freed = 0;
	msg_buf[i].times_resized = 0;

	/* we set RESIZE_MSG_BUFS buffers are changable, the rest stay small */
	if (i < (MAX_MSG_BUFS-RESIZE_MSG_BUFS)) msg_buf[i].resizable = 0;
	else 						msg_buf[i].resizable = 1;
	}

msg_bufs_free = i;
msg_buf_last_freed = 0;		/* ok, its not freed yet but it is free :) */


if (i!=MAX_MSG_BUFS) /* then we have some that couldn't be allocated */
	for(j=i;j<MAX_MSG_BUFS;j++) {
		msg_buf[j].msg_buff_id = -1;
		msg_buf[j].base_ptr = NULL;
		msg_buf[j].data_ptr = NULL;
		msg_buf[j].from_ptr = NULL;
		msg_buf[j].size = 0;
		msg_buf[j].len = 0;
		msg_buf[j].space = 0;
		msg_buf[j].toend = 0;
		msg_buf[j].in_use = -1;
		msg_buf[j].times_sent = 0;
		msg_buf[j].times_freed = 0;
		msg_buf[j].times_resized = 0;
		}

/* ok, now to set myself up so that I know if I need to convert stuff or not */

msg_convert_type	= 1;	/* force by default.... */

/* now unforce by type */
#if defined (IMA_SUN4) || defined (IMA_SUN4SOL2) || defined (IMA_JAVA) || defined (IMA_AIX4) || defined (IMA_RS6K) || defined (IMA_SGI6) || defined (IMA_SP2)
	msg_convert_type = 0;
#endif
	

return (i);
}

int dump_msg_bufs () 
{
int i;

printf("----------------------------------------\n");
printf("Internal Message Buffer Information Dump\n");
printf("----------------------------------------\n");
	printf("ID\tBase\tSize\tPacked\tSpace\tToEnd\tInUse\t");
	printf("#Sent\t#Freed\tResized\n");

for(i=0;i<MAX_MSG_BUFS;i++) {
    printf("%2d\t",msg_buf[i].msg_buff_id);
    printf("%p\t",msg_buf[i].base_ptr);
    printf("%ld\t",msg_buf[i].size);
    printf("%ld\t",msg_buf[i].len);
    printf("%ld\t",msg_buf[i].space);
    printf("%ld\t",msg_buf[i].toend);
    printf("%d\t",msg_buf[i].in_use);
    printf("%ld\t",msg_buf[i].times_sent);
    printf("%ld\t",msg_buf[i].times_freed);
    printf("%ld",msg_buf[i].times_resized);

	if (msg_buf[i].resizable) printf("*");
	printf("\n");
	}

printf("----------------------------------------\n");
printf("Msg bufs free: %d Max bufs %d Last freed %d\n", 
		msg_bufs_free, MAX_MSG_BUFS, msg_buf_last_freed);
printf("----------------------------------------\n");
return 0;
}

/* used to get buffer info when sending for example */
int	get_msg_buf_info (int buf,char ** base,int * len)
{
/* return address check */
if (!base) return (0);
if (!len) return (0);

/* buf id value range check */
if (buf<0) return (0);
if (buf>=MAX_MSG_BUFS) return (0);

/* buf id matches buf internal value */
if (msg_buf[buf].msg_buff_id != buf) return (0);

/* buffer is in use */
if (msg_buf[buf].in_use != 1) return (0);

/* ok all is well return values */
*base = (char*) msg_buf[buf].base_ptr;
*len  = msg_buf[buf].len;

return (1);

}

/* help */
int set_unpksize (int buf,int reqsize) 
{

if (!check_buf(buf)) return -1;
 msg_buf[buf].toend = reqsize;
 return 0;
}

/* get a free buf of a certain size */
/* if the resize flag is set, then we can resize the buffer memory */
/* note when we free the buffer up, if it is above the MSG_BUF_RESIZE_LIMIT */
/* it will be resized back down to the MSG_BUF_INIT_SIZE */

/* setunpksize is non zero if we need to set the buffer toend value */
/* this is used by receive calls that copy directly back into the buffer */
/* usually the toend is set only when packing. */

int	get_msg_buf_of_size (unsigned long reqsize,int resize,int setunpksize)
{
int i;
int found = 0;			/* init straight search found=0/1 not the ID! */
int largestsize = 0;
char * p;

if (!msg_bufs_free) return (-1);	/* no free bufs return error */

if (msg_buf_last_freed!=-1) {
	if (msg_buf[msg_buf_last_freed].size >= reqsize)
		{
		found = 1;
		i = msg_buf_last_freed;
		msg_buf_last_freed = -1;
		}
	}

	/* search for one */
if (!found)
	for (i=0;i<MAX_MSG_BUFS;i++) {
		if (msg_buf[i].msg_buff_id==i) 
			if (msg_buf[i].in_use == 0)	
				if (msg_buf[i].size >= reqsize)
				{	
				found = 1; 
				break; 
				}
		} /* for */

if (found) {
	msg_buf[i].data_ptr = msg_buf[i].base_ptr; 
	msg_buf[i].len = 0;
	msg_buf[i].space = msg_buf[i].size;
	if (setunpksize) 	msg_buf[i].toend = reqsize;
	else 				msg_buf[i].toend = 0;
	msg_buf[i].in_use = 1;

	return (i);
	}

/* ok we did not find one of the correct length, so if the resize flag */
/* is set, take the largest un-used buffer and resize it */

if (!resize) return (-1);

/* printf("Hunting down a buffer of size %d\n", reqsize); */

	for (i=0;i<MAX_MSG_BUFS;i++) {
		if (msg_buf[i].msg_buff_id==i) 
			if (msg_buf[i].in_use == 0)	
				if (msg_buf[i].resizable==1)
					if (msg_buf[i].size > largestsize)
					{	
						largestsize = msg_buf[i].size;
/* 						found = i;  */
						/* NO, i=ID, found goes to 1 !*/
						found = 1;
/* 						printf("* i%d size%d \n", i, largestsize); */
						break;
					}
		}

if (found) { /* then we have one that we can resize */

	p = (char *)malloc (reqsize);
	if (!p) {
		/* ops, we have a problem houston */
		return (-1);
		}


	/* else we have the memory, so lets do it */

	free (msg_buf[i].base_ptr);	/* free the old memory first */

	msg_buf[i].base_ptr = p;
	msg_buf[i].data_ptr = p;
	msg_buf[i].from_ptr = p;
	msg_buf[i].size = reqsize;
	msg_buf[i].len = 0;
	msg_buf[i].space = reqsize;
	if (setunpksize) 	msg_buf[i].toend = reqsize;
	else 				msg_buf[i].toend = 0;
	msg_buf[i].in_use = 1;
	msg_buf[i].times_resized++;

	return (i);
	}


return (-1);
}

/* get a free buf */
int	get_msg_buf (int resizable )
/* int resizable;	 if we want a resizable buffer or not */
{
int i;
int found = 0;

/* Again, found = 1 when we find on. i=the mb index! */
/* 1:40am in stuttgart and what a bug to find! */

if (!msg_bufs_free) return (-1);	/* no free bufs return error */

if (!resizable)	/* we can then check the last buf used */
	if (msg_buf_last_freed!=-1) {
		found = 1;
		i = msg_buf_last_freed;
		msg_buf_last_freed = -1;
		}

if (!found)	/* search for one */

	for (i=0;i<MAX_MSG_BUFS;i++) {
		if (msg_buf[i].msg_buff_id==i) 
			if (msg_buf[i].in_use == 0)
				if ((!resizable) || (resizable && msg_buf[i].resizable))
					{	found = 1; break; }
		}

if (found) {
	msg_buf[i].data_ptr = msg_buf[i].base_ptr; 
	msg_buf[i].len = 0;
	msg_buf[i].space = msg_buf[i].size;
	msg_buf[i].toend = 0;
	msg_buf[i].in_use = 1;

	return (i);
	}

return (-1);
}


/* free a buf */
int free_msg_buf (int buf)
{
if (buf<0) return (0);
if (buf>=MAX_MSG_BUFS) return (0);

/* buf id matches buf internal value */
if (msg_buf[buf].msg_buff_id != buf) return (0);

/* if the buffer is too big and resizable then resize it down */
if (msg_buf[buf].size >= MSG_BUF_RESIZE_LIMIT) _msg_downsize_buf (buf);
	

    msg_buf[buf].data_ptr = msg_buf[buf].base_ptr;
    msg_buf[buf].from_ptr = msg_buf[buf].base_ptr;
    msg_buf[buf].len = 0;
	msg_buf[buf].space = msg_buf[buf].size;
    msg_buf[buf].toend = 0;
    msg_buf[buf].in_use = 0;	/* reset in use flag! */
	msg_buf[buf].times_freed++;	/* incrememnt freed count */

	/* note if we remember a buffer that is resized we end up thrashing */
	/* i.e. we should not change the msg_buf_last_freed flag to a resizable */
	/* buffer */

	if (!msg_buf[buf].resizable) msg_buf_last_freed = buf;	

return (buf);
}

/* end_msg_buf, i.e. call at end to release all memory (keeps purify happy) */
int	end_msg_buf ()
{
int i;

for (i=0;i<MAX_MSG_BUFS;i++) {
	free_msg_buf (i);
	}

/* this assumes that the above sets free bufs correctly :) */
for (i=0;i<msg_bufs_free;i++) {
	free (msg_buf[i].base_ptr);
	msg_buf[i].msg_buff_id = -1;	/* blocks its use */
	msg_buf[i].in_use = -1;			/* twice :) */
	}
return 0;
}



int check_buf (int buf)
{
/* buf id value range check */
if (buf<0) return (0);
if (buf>=MAX_MSG_BUFS) return (0);

/* buf id matches buf internal value */
if (msg_buf[buf].msg_buff_id != buf) return (0);

/* buffer is in use */
if (msg_buf[buf].in_use != 1) return (0);

/* ok, its a valid buffer and its in use, so use it :) */

return (1);
}


int	pk_int8 (int buf,void * ptr,long n) 
{
int i;
char *p;
char *q;
int s;

s = 1*n; /* yep 8bit number */

if (!check_buf(buf)) return (0);

if (msg_buf[buf].space < s) { /* not enough space */
	if (!msg_buf[buf].resizable) return (0);	/* not enough space */ 
												/* cannot resize it */

	i = _msg_resize_buf (buf, s);			/* request a resize on it */

	if (i<0) return (0); /* i.e. it failed to resize, so no data packed */
	/* else we just continue as normal */
	}

p = (char *) msg_buf[buf].data_ptr;
q = (char *) ptr;

/* ok have space.. so do it */
/* No msg_convert_type check for bytes */
	
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */	
								/* which is n=s ! */

msg_buf[buf].data_ptr = (void *) p;
msg_buf[buf].len += s;
msg_buf[buf].space -= s;
msg_buf[buf].toend += s;

return (s);

}



int	pk_int16 (int buf,void * ptr,long n)
{
int i;
char *p;
char *q;
int s;

s = 2*n;	/* two byte INTs */

if (!check_buf(buf)) return (0);

if (msg_buf[buf].space < s) { /* not enough space */
	if (!msg_buf[buf].resizable) return (0);	/* not enough space */ 
												/* cannot resize it */

	i = _msg_resize_buf (buf, s);			/* request a resize on it */

	if (i<0) return (0); /* i.e. it failed to resize, so no data packed */
	/* else we just continue as normal */
	}

p = (char *) msg_buf[buf].data_ptr;
q = (char *) ptr;

/* ok have space.. so do it */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* Note we step through here 'n' times not 's' times */
		*p++ = *(q+1);
		*p++ = *q;
		q+=2;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}

msg_buf[buf].data_ptr = (void *) p;
msg_buf[buf].len += s;
msg_buf[buf].space -= s;
msg_buf[buf].toend += s;

return (s);

}

int	pk_int32 (int buf,void * ptr,long n)
{
int i;
char *p;
char *q;
int s;

/* s = sizeof(int)*n; */
/* IT MAY NOT BE an INT */
s = 4*n;

if (!check_buf(buf)) return (0);

if (msg_buf[buf].space < s) { /* not enough space */
	if (!msg_buf[buf].resizable) return (0);	/* not enough space */ 
												/* cannot resize it */

	i = _msg_resize_buf (buf, s);			/* request a resize on it */

	if (i<0) return (0); /* i.e. it failed to resize, so no data packed */
	/* else we just continue as normal */
	}

p = (char *) msg_buf[buf].data_ptr;
q = (char *) ptr;

/* ok have space.. so do it */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* Note we step through here 'n' times not 's' times */
		*p++ = *(q+3);
		*p++ = *(q+2);
		*p++ = *(q+1);
		*p++ = *q;
		q+=4;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}

msg_buf[buf].data_ptr = (void *) p;
msg_buf[buf].len += s;
msg_buf[buf].space -= s;
msg_buf[buf].toend += s;

return (s);
}


/* this is the same as the pk_int32 except it does not convert data */
/* i.e. when handling IP addresses etc we do not convert the values */
int	pk_raw32 (int buf,void * ptr,long n)
{
int i;
char *p;
char *q;
int s;

/* s = sizeof(int)*n; */
/* IT MAY NOT BE an INT */
s = 4*n;

if (!check_buf(buf)) return (0);

if (msg_buf[buf].space < s) { /* not enough space */
	if (!msg_buf[buf].resizable) return (0);	/* not enough space */ 
												/* cannot resize it */

	i = _msg_resize_buf (buf, s);			/* request a resize on it */

	if (i<0) return (0); /* i.e. it failed to resize, so no data packed */
	/* else we just continue as normal */
	}

p = (char *) msg_buf[buf].data_ptr;
q = (char *) ptr;

/* note we step through here 's' times not 'n' times */
for(i=0;i<s;i++) *p++=*q++;	

msg_buf[buf].data_ptr = (void *) p;
msg_buf[buf].len += s;
msg_buf[buf].space -= s;
msg_buf[buf].toend += s;

return (s);
}

int	pk_int64 (int buf,void * ptr,long n) 
{
  int i;
  char *p;
  char *q;
  int s;
  s = 8*n;	/* yep 64 bits */

  if (!check_buf(buf)){
    return (0);
  }
   
  if (msg_buf[buf].space < s) {
    if (!msg_buf[buf].resizable){
      return (0);
    }
     
    i = _msg_resize_buf (buf, s);

    if (i<0){
      return (0);
    }
  }  
   
  p = (char *) msg_buf[buf].data_ptr;
  q = (char *) ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) {
      *p++ = *(q+7);
      *p++ = *(q+6);
      *p++ = *(q+5);
      *p++ = *(q+4);
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=8;
    }
  }  
  else {
    for(i=0;i<s;i++){
      *p++=*q++;
    }
  }  
   
  msg_buf[buf].data_ptr = (void *) p;
  msg_buf[buf].len += s;
  msg_buf[buf].space -= s;
  msg_buf[buf].toend += s;

  return (s);

}

int	pk_int128 (int buf,void * ptr,long n) 
{
  int i;
  char *p;
  char *q;
  int s;
  s = 16*n;	/* 128 bits */

  if (!check_buf(buf)){
    return (0);
  }
   
  if (msg_buf[buf].space < s) {
    if (!msg_buf[buf].resizable){
      return (0);
    }
     
    i = _msg_resize_buf (buf, s);

    if (i<0){
      return (0);
    }
  }  
   
  p = (char *) msg_buf[buf].data_ptr;
  q = (char *) ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) {
      *p++ = *(q+15);
      *p++ = *(q+14);
      *p++ = *(q+13);
      *p++ = *(q+12);
      *p++ = *(q+11);
      *p++ = *(q+10);
      *p++ = *(q+9);
      *p++ = *(q+8);
      *p++ = *(q+7);
      *p++ = *(q+6);
      *p++ = *(q+5);
      *p++ = *(q+4);
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=16;
    }
  }  
  else {
    for(i=0;i<s;i++){
      *p++=*q++;
    }
  }  
   
  msg_buf[buf].data_ptr = (void *) p;
  msg_buf[buf].len += s;
  msg_buf[buf].space -= s;
  msg_buf[buf].toend += s;


  return (s);
}

int pk_real32 (int buf,void * ptr,long n)
{
  int i;
  char *p;
  char *q;
  int s;
  s = 4*n;

  if (!check_buf(buf)){
    return (0);
  }
   
  if (msg_buf[buf].space < s) {
    if (!msg_buf[buf].resizable){
      return (0);
    }
     
    i = _msg_resize_buf (buf, s);

    if (i<0){
      return (0);
    }
  }  
   
  p = (char *) msg_buf[buf].data_ptr;
  q = (char *) ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) {
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=4;
    }
  }  
  else {
    for(i=0;i<s;i++){
      *p++=*q++;
    }
  }  
   
  msg_buf[buf].data_ptr = (void *) p;
  msg_buf[buf].len += s;
  msg_buf[buf].space -= s;
  msg_buf[buf].toend += s;

  return (s);
 }

int pk_real64 (int buf,void * ptr,long n)
{
  int i;
  char *p;
  char *q;
  int s;
  s = 8*n;

  if (!check_buf(buf)){
    return (0);
  }
   
  if (msg_buf[buf].space < s) {
    if (!msg_buf[buf].resizable){
      return (0);
    }
     
    i = _msg_resize_buf (buf, s);

    if (i<0){
      return (0);
    }
  }  
   
  p = (char *) msg_buf[buf].data_ptr;
  q = (char *) ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) {
      *p++ = *(q+7);
      *p++ = *(q+6);
      *p++ = *(q+5);
      *p++ = *(q+4);
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=8;
    }
  }  
  else {
    for(i=0;i<s;i++){
      *p++=*q++;
    }
  }  
   
  msg_buf[buf].data_ptr = (void *) p;
  msg_buf[buf].len += s;
  msg_buf[buf].space -= s;
  msg_buf[buf].toend += s;

  return (s);
 }

int pk_byte(int buf,void * ptr,long n)
{
  int i;
  char *p;
  char *q;

  if (!check_buf(buf)){
    return (0);
  }
  
  if (msg_buf[buf].space < n) {
    if (!msg_buf[buf].resizable){
      return (0);
    }

    i = _msg_resize_buf (buf, n);

    if (i<0){
      return (0);
    }
  }
  
  p = (char *) msg_buf[buf].data_ptr;
  q = (char *) ptr;

  for(i=0;i<n;i++){
    *p++=*q++;
  }
  
  msg_buf[buf].data_ptr = (void *) p;
  msg_buf[buf].len += n;
  msg_buf[buf].space -= n;
  msg_buf[buf].toend += n;

  return (n);

}

int	pk_string (int buf,char *strptr) 
{
int i;
int s;
char *p;
char *q;

if (!check_buf(buf)) return (0);

s = strlen (strptr);

/* check for enough space (str+strlen) */ 

  if (msg_buf[buf].space < (s+4)) {
    if (!msg_buf[buf].resizable){
      return (0);
    }

    i = _msg_resize_buf (buf, (s+4));

    if (i<0){
      return (0);
    }
  }

pk_int32 (buf, &s, 1);	/* yep I did just do what you think I did */

p = (char *) msg_buf[buf].data_ptr;
q = (char *) strptr;

/* memcopy oneday */
for(i=0;i<s;i++) *p++ = *q++;

/* note we don't update the buf by s+4 as the +4 has been done by the pkint strlen */
msg_buf[buf].data_ptr = (void *) p;
msg_buf[buf].len += s;
msg_buf[buf].space -= s;
msg_buf[buf].toend += s;

/* but we did pack (s+4) bytes */
return (s+4);
}




int	upk_int8 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


s = sizeof(int)*n;

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* note we step through here 'n' times only */
		*p++ = *(q+3);
		*p++ = *(q+2);
		*p++ = *(q+1);
		*p++ = *q;
		q+=4;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}



/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);
}



int	upk_int16 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


s = 2*n;	/* two bytes */

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* note we step through here 'n' times only */
		*p++ = *(q+1);
		*p++ = *q;
		q+=2;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}



/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);
}

int	upk_int32 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


/* No, an int might not be 4 bytes */
/* s = sizeof(int)*n; */

s = 4*n;	/* 32 bit or 4bytes */

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* note we step through here 'n' times only */
		*p++ = *(q+3);
		*p++ = *(q+2);
		*p++ = *(q+1);
		*p++ = *q;
		q+=4;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}



/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);

}

/* same as above but does not convert/byte swap in any way ever */
/* used for binary data such as IP address etc */
int	upk_raw32 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


/* No, an int might not be 4 bytes */
/* s = sizeof(int)*n; */

s = 4*n;	/* 32 bit or 4bytes */

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
/* note we step through here 's' times not 'n' times */
for(i=0;i<s;i++) *p++=*q++;	


/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);
}


int	upk_int64 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


s = 8*n; /* 64 bit or 8 byte */

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* note we step through here 'n' times only */
      	*p++ = *(q+7);
      	*p++ = *(q+6);
      	*p++ = *(q+5);
      	*p++ = *(q+4);
		*p++ = *(q+3);
		*p++ = *(q+2);
		*p++ = *(q+1);
		*p++ = *q;
		q+=8;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}



/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);
}

int	upk_int128 (int buf,void * ptr,long n) 
{
int i;
int s;
char *p;
char *q;


s = 16*n; /* 128 bit or 16 byte */
		  /* this is good for key exchange stuff :) */

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

p = (char *) ptr;
q = (char *) msg_buf[buf].from_ptr;

/* ok unpack it.. make it so */
if (msg_convert_type) {
	for (i=0;i<n;i++) {	/* le duff device el GEF style */
						/* note we step through here 'n' times only */
      	*p++ = *(q+15);
      	*p++ = *(q+14);
      	*p++ = *(q+13);
      	*p++ = *(q+12);
      	*p++ = *(q+11);
      	*p++ = *(q+10);
      	*p++ = *(q+9);
      	*p++ = *(q+8);
      	*p++ = *(q+7);
      	*p++ = *(q+6);
      	*p++ = *(q+5);
      	*p++ = *(q+4);
		*p++ = *(q+3);
		*p++ = *(q+2);
		*p++ = *(q+1);
		*p++ = *q;
		q+=16;
		}
	}

else {
	for(i=0;i<s;i++) *p++=*q++;	/* note we step through here 's' times not 'n' times */
	}



/* update buf info */
p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (s);
}

int upk_real32 (int buf,void * ptr,long n)
{
  int i;
  int s;
  char *p;
  char *q;
  s = 4*n;

  if (!check_buf(buf)) {
    return (0);
  }

  if (msg_buf[buf].toend < (s)){
    return (0);
  }

  p = (char *) ptr;
  q = (char *) msg_buf[buf].from_ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) { 
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=4;
    }
  }
  else {
    for(i=0;i<s;i++) {
      *p++=*q++;
    }
  }
  p = (char *) msg_buf[buf].from_ptr;
  p+= s;

  msg_buf[buf].from_ptr = p;
  msg_buf[buf].toend -= s;
 return (s);
}



int upk_real64 (int buf,void * ptr,long n)
{
  int i;
  int s;
  char *p;
  char *q;
  s = 8*n;

  if (!check_buf(buf)) {
    return (0);
  }

  if (msg_buf[buf].toend < (s)){
    return (0);
  }

  p = (char *) ptr;
  q = (char *) msg_buf[buf].from_ptr;

  if (msg_convert_type) {
    for (i=0;i<n;i++) { 
      *p++ = *(q+7);
      *p++ = *(q+6);
      *p++ = *(q+5);
      *p++ = *(q+4);
      *p++ = *(q+3);
      *p++ = *(q+2);
      *p++ = *(q+1);
      *p++ = *q;
      q+=8;
    }
  }
  else {
    for(i=0;i<s;i++) {
      *p++=*q++;
    }
  }
  p = (char *) msg_buf[buf].from_ptr;
  p+= s;

  msg_buf[buf].from_ptr = p;
  msg_buf[buf].toend -= s;
return (s);
}



int upk_byte(int buf,void * ptr,long n)
{
  int i;
  char *p;
  char *q;

  if (!check_buf(buf)) {
    return (0);
  }
   
  if (msg_buf[buf].toend < (n)){
    return (0);
  }
   
  p = (char *) ptr;
  q = (char *) msg_buf[buf].from_ptr;

  for(i=0;i<n;i++) {
    *p++=*q++;
  }
  p = (char *) msg_buf[buf].from_ptr;
  p+= n;

  msg_buf[buf].from_ptr = p;
  msg_buf[buf].toend -= n;
 return (n);
}


int	upk_string (int buf,void * strptr,long maxlen) 
/* int buf; */
/* void *strptr; */
/* long maxlen;	 maximum length of my target buffer */
{
int i;
int s;
int t;
char *p;
char *q;

if (!check_buf(buf)) return (0);

/* get the size of the string in the buffer */

upk_int32 (buf, &s, 1); /* yep I did just do what you think I did */

if (msg_buf[buf].toend < (s)) return (0);	/* not enough space */ 
											/* this cannot ever happen as we are not allowed to */
											/* pack unless we have all the space! */

/* BUT we can not have enough space to unpack in... */

if ((s+1)>maxlen)	{	/* the +1 and -1 are for the NULL characters we will terminate the string with */
	t = maxlen-1;	
	}
else {
	/* s stays the same */
	t = s;	/* no truncated message to chop off later */
	}

p = (char *) strptr;
q = (char *) msg_buf[buf].from_ptr;

/* memcopy oneday */
for(i=0;i<t;i++) *p++ = *q++;

/* now terminate the string with a null character */
*p = '\0';

p = (char *) msg_buf[buf].from_ptr;
p+= s;

msg_buf[buf].from_ptr = p;
msg_buf[buf].toend -= s;

return (t);

}


int	_msg_resize_buf (int buf,long incsize )
{

char *p;
char *q;
char *oldp;
long oldsize;
long oldlen;
long diffsize;	/* how much more space */
long difffrom;	/* if this has been read from at all, whats its offset */
int i;
long reqsize;


	if (!msg_buf[buf].resizable) return (-1);


	/* remember what we are doing */
	oldp = (char*)msg_buf[buf].base_ptr;
	oldsize = msg_buf[buf].size;
	oldlen = msg_buf[buf].len;
	difffrom  = ((char*)msg_buf[buf].from_ptr - (char*)msg_buf[buf].base_ptr);

	/* ok now we can attempt to get more memory */

	if (incsize<=MSG_BUF_INIT_SIZE)
		reqsize = oldsize + MSG_BUF_INIT_SIZE;
	else 
		{
		if (incsize>oldsize)
			reqsize = oldsize + incsize;
		else
			reqsize = oldsize * 2;	/* double it ! */
									/* should use a better rule */
		}

	diffsize = reqsize - oldsize;	/* remember the difference */

    p = (char*) malloc (reqsize);
    if (!p) {
        /* ops, we have a problem houston */
        return (-1);
        }

    /* else we have the memory, so lets do it */

	/* first copy the data over */

	q = p;	/* make a copy of the new start pointer */

	for (i=0;i<oldlen;i++)	/* loop it */
		*q++ = *oldp++;

	/* note, q = location of next free location in the new object */

    free (msg_buf[buf].base_ptr); /* free the old memory first */

    msg_buf[buf].base_ptr = p;
    msg_buf[buf].data_ptr = q;
    msg_buf[buf].from_ptr = (char*)msg_buf[buf].base_ptr + difffrom;
    msg_buf[buf].size = reqsize;
    msg_buf[buf].space += diffsize;
    msg_buf[buf].times_resized++;

    return (buf);
}


/* this is like resize buf but it works only on freed buffers */
int	_msg_downsize_buf (int buf )
{
char *p;

	if (!msg_buf[buf].resizable) return (-1);

	/* ok now we can attempt to get more memory */

    p = (char*) malloc (MSG_BUF_INIT_SIZE);
    if (!p) {
        /* ops, we have a problem houston */
        return (-1);
        }

    /* else we have the memory, so lets do it */

	/* we don't need to copy the data over, as its a freed buffer */

    free (msg_buf[buf].base_ptr); /* free the old memory first */

    msg_buf[buf].base_ptr = p;
    msg_buf[buf].data_ptr = p;
    msg_buf[buf].from_ptr = p;
    msg_buf[buf].size = MSG_BUF_INIT_SIZE;
    msg_buf[buf].space = MSG_BUF_INIT_SIZE;
    msg_buf[buf].len = 0;
    msg_buf[buf].times_resized++;

    return (buf);
}


/* int upk_double(int buf,void * ptr,long n) */
/* This is removed as a double was assumed to be 64 bit when its not always */

