
export HARNESS_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export HARNESS_LIB_DIR=$HARNESS_ROOT/lib
export HARNESS_BIN_DIR=$HARNESS_ROOT/bin
export HARNESS_CACHE_DIR=$HARNESS_ROOT/tmp

export HARNESS_SVC_DIR=$HARNESS_ROOT/svc

export HARNESS_ARCH=`${HARNESS_ROOT}/conf/pvmgetarch`

# The default port used by G_HCORE_D
export HARNESS_GHCORED_PORT=1945

# The default Harness top level NAME SERVICE
export HARNESS_NS_HOST=$(hostname)
export HARNESS_NS_HOST_PORT=1812

# The above service will point to the lower service

# The default HARNESS REGISTRATION metadata-ring SERVICE
export HARNESS_RS_HOST=$(hostname)
export HARNESS_RS_HOST_PORT=1970

# Set path up

export PATH=$PATH:$HARNESS_ROOT/bin/$HARNESS_ARCH

