/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	Fast MPI tester by Graham, Stuttgart March 2002 
*/



#include <stdio.h>
#include "mpi.h"
#include "snipe2.h"

#define OPS	10
#define MIN	0
#define MAX 1048576

#define CNT	30

#define HSTAG 1776

double	st[CNT];		/* start */
double	et[CNT];		/* stop */
double	dt[CNT];		/* diff */
double	bw[CNT];		/* bandwidth */
long	sz[CNT];		/* size */

char	out[MAX*OPS];
char	in[MAX*OPS];



main (argc, argv)
int argc;
char *argv[];
{
int rc;
int rank, size;
int i, tag;
int j;
MPI_Status status;
int rs;
int numt;	/* number of tests */
int tmp;
double t, t2;
char *p;
char *q;
int output=0;	/* 0 = all, 1 = latency, 2 = BW */


/* printf("sleeping..."); fflush(stdout); */
/* sleep (30); */
/* printf("done sleeping..."); fflush(stdout); */
/*
 args? */
rc = MPI_Init (&argc, &argv);

if (rc==MPI_INIT_RESTARTED_NODE) {
        printf("MPI_Init says I am a restart :)\n"); fflush(stdout);
        rs = 1;
        }
else {
        printf("MPI_Init [%d]\n", rc); fflush(stdout);
        rs = 0;
        }

rc = MPI_Comm_rank (MPI_COMM_WORLD, &rank);
rc = MPI_Comm_size (MPI_COMM_WORLD, &size);

if (size!=2) {
	fprintf(stderr,"ping pong needs two procs.. exiting nicely bye..\n");
	MPI_Finalize ();
	exit(0);
}

if (argc==2) {
   output = atoi (argv[1]);
   if (rank==0){
	  if ((output<0)||(output>3)) { printf("Bad output option [0-3] %d\n", output); output = 0; }
	  if (output==0) printf("All details: size bytes time mSec BW MBytes/Sec\n");
	  if (output==1) printf("Latency only: size bytes time mSec\n");
	  if (output==2) printf("BW only: size bytes BW MBytes/Sec\n");
	  if (output==3) printf("Latency & BW only: size bytes time in mSec BW MBytes/Sec\n");
   }
}

/* build test values */
j = MIN;
for (i=0;j<=MAX;i++) {	/* note the i,j combo here */
	st[i] = 0.0;
	et[i] = 0.0;
	dt[i] = 0.0;
	bw[i] = 0.0;

	sz[i] = j;
	if (j==0) j=1;	/* i.e. test 0 length and then 1 byte length */
	else
		j *= 2;		/* else we do 2, 4, 8, 16... MAX bytes */

	}

numt = i;

t = MPI_Wtime ();
/* if (rank==0)  */
/* if (rank==1)  */
   sleep (1);
t2 = MPI_Wtime ();	/* warm it up for the real deal */


printf("t %lf t2 %lf diff %lf\n", t, t2, t2-t);

if (rank==0) {
	printf("leader: test sizes as followes for %d iterations:\t", numt);
	for (i=0;i<numt;i++) printf("%d ", sz[i]);
	printf("\n");

	printf("leader: doing a handshake then doing benchmark\n");
    fflush(stdout);

	/* handshake is the numt value */
    rc = MPI_Send (&numt, 1, MPI_INT, 1, HSTAG, MPI_COMM_WORLD);
    rc = MPI_Recv (&tmp, 1, MPI_INT, 1, HSTAG, MPI_COMM_WORLD, &status);
	
	if (numt!=tmp) {
		fprintf(stderr,"Handshake was %d NOT %d\n Exiting gracefully\n",
				tmp, numt);
		MPI_Finalize ();
		exit(0);
		}

	tag = 1000;	/* just so we can tell */

    for (i=0;i<numt;i++) {	/* for each test size */
		p = (char*) out;
		q = (char*) in;

		st[i] = MPI_Wtime ();

		for (j=0;j<OPS;j++) {	
#ifndef FAST
        	rc = MPI_Send (p, sz[i], MPI_BYTE, 1, tag, MPI_COMM_WORLD);
    		rc = MPI_Recv (q, sz[i], MPI_BYTE, 1, tag, MPI_COMM_WORLD, &status);
#else
        	rc = ftmpi_mpi_send (p, sz[i], MPI_BYTE, 1, tag, MPI_COMM_WORLD);
    		rc = ftmpi_mpi_recv (q, sz[i], MPI_BYTE, 1, tag, MPI_COMM_WORLD, &status);
#endif /* RAW, i.e. using non DDT calls to check for overheads of DDTs */

			p+= sz[i];		/* creep through array to avoid cache effects */
			q+= sz[i];

			tag++;			/* so we creep through the tags as well */
			}

		et[i] = MPI_Wtime ();

        }	/* for each test size */
	}

else { /* follower */
   /* unmatched message just to test head to head send xalt protocol */
/*     rc = MPI_Send (&numt, 1, MPI_INT, 0, HSTAG+1000, MPI_COMM_WORLD); */
	printf("follower getting handshake\n");
    rc = MPI_Recv (&tmp, 1, MPI_INT, 0, HSTAG, MPI_COMM_WORLD, &status);
    rc = MPI_Send (&numt, 1, MPI_INT, 0, HSTAG, MPI_COMM_WORLD);
	if (numt!=tmp) {
		fprintf(stderr,"Handshake was %d NOT %d\n Exiting gracefully\n",
				tmp, numt);
		MPI_Finalize ();
		exit(0);
		}

	tag = 1000;	/* just so we can tell */

    for (i=0;i<numt;i++) {	/* for each test size */
		p = (char*) out;
		q = (char*) in;

		st[i] = MPI_Wtime ();

		for (j=0;j<OPS;j++) {	
#ifndef FAST
    		rc = MPI_Recv (q, sz[i], MPI_BYTE, 0, tag, MPI_COMM_WORLD, &status);
        	rc = MPI_Send (p, sz[i], MPI_BYTE, 0, tag, MPI_COMM_WORLD);
#else
    		rc = ftmpi_mpi_recv (q, sz[i], MPI_BYTE, 0, tag, MPI_COMM_WORLD, &status);
        	rc = ftmpi_mpi_send (p, sz[i], MPI_BYTE, 0, tag, MPI_COMM_WORLD);
#endif /* RAW, i.e. using non DDT calls to check for overheads of DDTs */

			p+= sz[i];		/* creep through array to avoid cache effects */
			q+= sz[i];

			tag++;			/* so we creep through the tags as well */
			}

		et[i] = MPI_Wtime ();

        }	/* for each test size */
	}

if (rank==0) { /* do the results */
	printf("\n\n");
	for (i=0;i<numt;i++) {

		dt[i] = et[i]-st[i];	/* diff time */

		dt[i] = dt[i] / (double) 2.0; /* round trip time vs one way */

		dt[i] = dt[i] / ((double)OPS);	/* time per op avg/mean */

		bw[i] = ((double)sz[i]) / dt[i];
		bw[i] = bw[i] / (double) 1048576.0;

		dt[i] = dt[i] * (double) 1000.0; /* round trip time in mSecs */


		if (output==0)
		printf("size %7d bytes   time %3.4lf mSec    bw %3.4lf MB/Sec\n", sz[i], dt[i], bw[i]);
		if (output==1)
		printf("%7d %3.4lf\n", sz[i], dt[i]);
		if (output==2)
		printf("%7d %3.4lf\n", sz[i], bw[i]);
		if (output==3)
		printf("%7d %3.4lf %3.4lf\n", sz[i], dt[i], bw[i]);
		}
	printf("\n\n");
	fflush (stdout);
	}

/* TEST */
/* snipe2_progress (SNIPE2ALL,SNIPE2TEST); */
/* TEST */

sleep (1);
MPI_Finalize ();
exit(0);

}

