
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include<stdio.h>
#include<stdlib.h>
#include<stdlib.h>
#include"snipe_lite.h"

/* #include "names.h" */

/* lib */
#include "hlib.h"

/* local f()s */
int print_count();
int print_back ();
int print_who  ();



main () {
int i; 
int j;

int package;
int ncalls;

int rc;
int s;

int funct_id;
int funct_id2;
int funct_id3;

int totalcores;			/* how many are their really */

coreinfo_t cores[3];	/* only get info on 3? */

rc = hlib_find_cores ("dvm", 1, 3, &totalcores, cores); 

if (rc<1) {
	puts("Not enough cores to do a test on!");
	exit (-1);
	}

printf("Got info on %d cores\n", rc); fflush(stdout);

/* from now on we go tests on the first core only */

hlib_print_core_info (&cores[0], 1);

rc = hlib_load_package (&cores[0], "system", "test", &package, &ncalls);

printf("Package %d ncalls %d\n", package, ncalls);

if (rc<0) {
	printf("Load package on %s fails with rc %d\n", cores[0], rc);
	exit (-1);
	}

rc = hlib_get_function (&cores[0], "count", package, &funct_id); 
if (rc<0) {
	printf("get function %s failed with rc %d\n", "count", rc);
	exit (-1);
	}

rc = hlib_get_function (&cores[0], "backwards", package, &funct_id2); 
if (rc<0) {
	printf("get function %s failed with rc %d\n", "backwards", rc);
	exit (-1);
	}

rc = hlib_get_function (&cores[0], "whoami", package, &funct_id3); 
if (rc<0) {
	printf("get function %s failed with rc %d\n", "whoami", rc);
	exit (-1);
	}


/* now the NOT neat stuff */

rc = hlib_call_function (&cores[0], funct_id, &s);
print_count (s);

rc = hlib_call_function (&cores[0], funct_id, &s);
print_count (s);

rc = hlib_call_function (&cores[0], funct_id, &s);
print_count (s);


rc = hlib_call_function (&cores[0], funct_id2, &s);
print_back (s);

rc = hlib_call_function (&cores[0], funct_id2, &s);
print_back (s);


rc = hlib_call_function (&cores[0], funct_id3, &s);
print_who (s);

rc = hlib_call_function (&cores[0], funct_id3, &s);
print_who (s);

rc = hlib_end ();

}



/* support functions to make example easier to understand */

print_count (s)
int s;
{
int i0, i1;


/* get the results */
readconn (s, (char *)&i0, sizeof(int));

i1 = 0;
if (i0<0) {
	readconn (s,(char *) &i1, sizeof(int));
}

usleep (1000);
closeconn (s);

printf("Count returned %d\n", i0);
}

print_back (s)
int s;
{
int i0, i1;


/* get the results */
readconn (s,(char *) &i0, sizeof(int));

i1 = 0;
if (i0<0) {
	readconn (s,(char *) &i1, sizeof(int));
}

usleep (1000);
closeconn (s);

printf("Backwards returned %d\n", i0);
}


print_who (s)
int s;
{
int i0, i1;
char tempstr[1024];


/* get the results */
readconn (s,(char *) &i0, sizeof(int));

i1 = 0;
if (i0>0) {
	readconn (s, tempstr, i0);
	tempstr[i0]='\0';
	}

usleep (1000);
closeconn (s);

printf("Whoami returned [%s]\n", tempstr);
}




