#include "mpi.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define MCW MPI_COMM_WORLD 

/* void main (int argc, char *argv[]) */

int main (argc, argv)
int argc;
char *argv[];
{

	int rank, size, i;
	char **ptr;
	char hname[255];	
	int pid;
	char idstr[300];

	/* build unique id str */
	gethostname (hname, sizeof(hname));
	pid = (int) getpid();

	sprintf(idstr, "[%s:%5d]  ", hname, pid);

	printf("\n%s MPI version dependant startup information\n\n",idstr);
	ptr=argv;
	printf("%s Argc = %d and first argv is %s\n",idstr, argc, *argv);
	for(i=0;i<argc;i++) {
		printf("%s %d %s \n",idstr,i, *ptr);
		ptr ++;
		}


	fflush(stdout);
	fflush(stderr);

	sleep (30);
		
	MPI_Init(&argc, &argv);


	MPI_Comm_size (MCW, &size);
	MPI_Comm_rank (MCW, &rank);


	printf("\n%s MPI independant information\n",idstr);
	printf("\n%s Host [%s] Size of %d, my rank in that size %d\n\n",idstr, 
						hname, size, rank);

	/* maybe see the output sometime ? */
	fflush(stdout);
	fflush(stderr);
	sleep (1);
	MPI_Barrier (MCW);
	fflush(stdout);
	fflush(stderr);
	sleep (1);

	MPI_Finalize(); 
	sleep (1);
}
