
      subroutine  slave ( myrank )
      
      implicit none
      include "mpif.h"
      include "fsolvergen.inc"


      integer rc, myrank
      integer howmanydone
      integer todo
      double precision result
      double precision width
      double precision x, y
      integer status(MPI_STATUS_SIZE)


      comm = MPI_COMM_WORLD
      mystate = AVAILABLE

C     for the calculation I will do 
      width = 1.0 / slices
  
C     status 	
      howmanydone = 0

  
C     all I do is get work, do a calculation and then return an answer 
 10    continue
C     -------------------------------------------------------
C     get work 
      if ( mystate .eq. AVAILABLE ) then
         call MPI_Recv ( todo, 1, MPI_INTEGER, masterrank, WORK_TAG, 
     &        comm, status, rc )
         call slave_advance_state (todo)
      end if

C     -------------------------------------------------------
C     calculate 
      if ( mystate .eq. RECEIVED ) then
         howmanydone = howmanydone + 1
	      
C        calculate pi 
         x = width *  todo
         y = 4.0 / (1.0 + x*x)
         result = y * width
         
C        check for delay here 
         if (delay .gt. 0 ) then
            call dosleep (delay)
         end if
         call slave_advance_state(NULLWORKID)
      endif
      
C     -------------------------------------------------------
      if ( mystate .eq. WORKING ) then
C     return work 
         call MPI_Send ( result, 1, MPI_DOUBLE_PRECISION, 
     &        masterrank, RES_TAG, comm, rc )
         call slave_advance_state(NULLWORKID)
      endif

C     -------------------------------------------------------
      if ( mystate .eq. FINISHED ) then
         goto 100
      end if
      
C     -------------------------------------------------------
      goto 10  ! main loop
C     -------------------------------------------------------

 100  continue
  
      write(*,*) "I am slave [", myrank, "] and I did [", howmanydone,
     &     "] operations"
      
      return 
      end
C********************************************************************
C********************************************************************
C********************************************************************
      subroutine slave_advance_state ( thisworkid )

      implicit none
      include "mpif.h"
      include "fsolvergen.inc"

      integer rc, thisworkid
      
      if ( mystate .eq. AVAILABLE) then
         if ( thisworkid .eq. NULLWORKID ) then
C           do nothing, master has died and we need to keep the
C	    Available state 
         else if ( thisworkid .eq. FINISH ) then
            mystate = FINISHED
         else 
            mystate = RECEIVED
         endif
      else if ( mystate .eq. RECEIVED ) then
         mystate = WORKING
      else if ( mystate .eq. WORKING) then
         mystate = AVAILABLE
      else if ( mystate .eq. SEND_FAILED ) then
         mystate = WORKING
      else if ( mystate .eq. RECV_FAILED) then
         mystate = AVAILABLE
      else if ( mystate .eq. DEAD .or. mystate .eq. FINISHED ) then
C     Not really defined for the slave 
      else
         write(*,*)"Invalid state"
         call MPI_Abort ( MPI_COMM_WORLD, 1 , rc)
      endif

      end
C********************************************************************
C********************************************************************
C********************************************************************
      subroutine slave_mark_error ()

      implicit none
      include "mpif.h"
      include "fsolvergen.inc"

      if ( mystate .eq. AVAILABLE )  then
         mystate = RECV_FAILED
      else if ( mystate .eq. WORKING )  then
         mystate = SEND_FAILED
      endif

      end


      subroutine slave_mark_available ()

      implicit none
      include "mpif.h"
      include "fsolvergen.inc"

      mystate = AVAILABLE

      end
C********************************************************************
C********************************************************************
C********************************************************************
      subroutine recover_slave ( rcomm, err)

      implicit none
      include "mpif.h"
      include "fsolvergen.inc"
      
      integer rcomm, err
      integer oldcomm, newcomm
      integer rc
      integer size, newrank
      integer tmparr(MAXSIZE), i

      if ( err .eq. MPI_ERR_OTHER ) then
         oldcomm = MPI_COMM_WORLD
         newcomm = FT_MPI_CHECK_RECOVER

         call MPI_Comm_dup (oldcomm, newcomm, rc)
         call MPI_Comm_rank (MPI_COMM_WORLD, newrank, rc)
         call MPI_Comm_size (MPI_COMM_WORLD, size, rc)
         write(*,*)"[", newrank,"]: AFTER Recovery size: ", size,
     &        "ret: ",rc
      
         call checkwhodied ( wasalivebefore, size, rc, tmparr )
         call slave_mark_error ()
         if ( rc .gt. 0  ) then 
            do 10 i = 1, rc
               if ( tmparr(i) .eq. masterrank ) then
C     Master has died, throw our current work
C     away, and wait for new portions 
C     This call overrides the mark_fault call from
C     three lines before 
                  call slave_mark_available()              
               endif      
 10         continue
         endif
      end if
      
      end
C********************************************************************
C********************************************************************
C********************************************************************
      subroutine dosleep ( del )
      
      implicit none

      integer del
      integer i, j

      j = 0
      do 100 i = 1, del
         j = j*2 +1
         call dosomethingelse ( j )
 100  continue

      return
      end
C**********************************************************************
C**********************************************************************
C**********************************************************************
      subroutine dosomethingelse ( del )

      implicit none

      integer del
      integer kj, i

C     the only goal of this routine is to avoid that the compiler
C     to optimize the loog in dosleep away

      kj  = (del*2) - 1
      del = (kj + 1) / 2

      do 110 i = 1, 100
         kj = kj -1 
 110  continue

      return
      end












