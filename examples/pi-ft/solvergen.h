
/* Message Tags */

#define WORK_TAG	100
#define RES_TAG		200

/* contents of message the indicates no more work (i.e. FINISHed) */

#define	FINISH		-999

/* delays to display stuff better */

#define delay		500000

/* sizes */

#define MAXSIZE 50
#define slices  250


/* worker state */

typedef enum {
  AVAILABLE,
  WORKING,
  SEND_FAILED,
  RECEIVED,
  RECV_FAILED,
  FINISHED,
  DEAD,
  INVALID
} worker_state_e;


typedef enum {
  SENT,
  RECV,
  ERROR,
  RECOVER,
  OK,
  DONE,
  DEATH
} events_e;


typedef enum {
  WNOTDONE,
  WINPROGRESS,
  WDONE
} work_state_e;

typedef struct {
  int workid;
  int rank;
  work_state_e workstate;
} _work_t;

typedef _work_t work_t;
work_t work[slices];

typedef struct {
  int rank;
  int currentwork;
  worker_state_e state;
} _proc_t;

typedef _proc_t proc_t;
proc_t procs[MAXSIZE];

/* Some constants */
#define NULLWORKID -1


/* distributed work */
extern int wasalivebefore;

/* Prototypes */
int slave ( int myrank );
void recover_master ( MPI_Comm *, int *, ... );
void recover_slave ( MPI_Comm *, int *, ... );
int panic (int rank);
int master (void);
void init_states (int max);
void init_work ();
void mark_error (int r);
void mark_dead (int r);
void advance_state ( int r, long workid);

void slave_advance_state (long workid);
void slave_mark_error (void);
void slave_mark_available (void);

long getnextwork ( void );
int *checkwhodied ( int, int, int*);


