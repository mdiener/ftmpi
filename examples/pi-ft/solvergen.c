/*
 original code purely by Graham
 Oct-Nov 2000

 adopted to the new semantics of FTMPI by Edgar
 July 2003

 This program shows how to do a master-slave framework with FT-MPI. We
 introduce structures for the work and for each proc. This is
 necessary, since we are designing this example now to work with
 REBUILD, BLANK and SHRINK modes.

 Additionally, this example makes use of the two additional
 attributes of FT-MPI, which indicate who has died and how
 many have died. By activating the COLLECTIVE_CHECKWHODIED flag, 
 a more portable (however also more expensive) version of the 
 checkwhodied routines is invoked. Attention: the collective 
 version of checkwhodied just works for the REBUILD mode!

 Graham Fagg 2000(c)
 Edgar Gabriel 2003(c)

*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"
#include "solvergen.h"

#define MCW	             MPI_COMM_WORLD
#define MASTER               0
#define SPECIALRECOVERYVALUE -1.0

long	nextwork;
MPI_Comm comm;

int j;
int maxworkers;
int wasalivebefore=0;

int main ( int argc, char **argv)
{
  int rank, size;
  int i;
  int rc;
  int mode;
  MPI_Errhandler errh;
  int numrespawned;

  double recval=SPECIALRECOVERYVALUE;

  comm = MCW;	/* our communicator is MCW */
  
  rc = MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);
  printf("I am [%d] of [%d]\n", rank, size);

#ifdef COLLECTIVE_CHECKWHODIED
  checkwhodied ( wasalivebefore, size, &numrespawned);
  wasalivebefore = 1;
#endif

  maxworkers = size;
  if (rank==0) 
    {
      MPI_Errhandler_create ( recover_master, &errh);
      MPI_Errhandler_set ( MPI_COMM_WORLD, errh);
      master ();
    }
  else
    {
      MPI_Errhandler_create ( recover_slave, &errh );
      MPI_Errhandler_set ( MPI_COMM_WORLD, errh );
      slave ( rank );
    }


  MPI_Finalize ();
  return ( 0 );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* the master code */
int master (void)
{
  /* general stuff */
  int i;
  int rc;
  int done=0;
  
  /* result stuff */
  double result, deltaresult;
  long slicestodo;
  MPI_Status status;

  /* work distribution */
  int	currentworkers;
  long next;
  
  /* recovery */
  int failed, reclaimed;
  int isfinished=0;
  
  /* startup */
  slicestodo  = slices;
  result      = 0.0;
  deltaresult = 0.0;
  nextwork    = 0;

  /* init worker states */
  init_states (maxworkers);
  init_work ();

  /* loop until we have done all the work or lost all my workers 
     The loop contains of two parts: distribute work, and 
     collect the results. In case a worker dies, his work is
     marked as 'not done' and redistributed. 
  */
  while (!done)
    {
      /* distribute work */
      /***************************************************/      
      for (j=1;j<maxworkers;j++)  
	{
	  if (procs[j].state == AVAILABLE) 
	    {
	      next = getnextwork ();
	      MPI_Send (&next, 1, MPI_LONG, j, WORK_TAG, comm);
	      advance_state (j, next);
	    } 
	} 
      
      /* get results */
      /***************************************************/      
      for (isfinished=0, j=1;j<maxworkers;j++)  
	{
	  if (procs[j].state == WORKING) 
	    {
	      MPI_Recv (&deltaresult, 1, MPI_DOUBLE, j, 
			RES_TAG, comm, &status);
	      advance_state(j, NULLWORKID);
	    }

	  else if (procs[j].state==FINISHED || procs[j].state == DEAD )
	    isfinished++;


	  if ( procs[j].state == RECEIVED )
	    {
	      result += deltaresult;
	      slicestodo--;
	      advance_state (j, NULLWORKID);
	    } 
	} 
      
      /* if no more work to be done AND everybody received
	 the FINISHED-msg, exit
      */
      if (!slicestodo && (isfinished == (maxworkers - 1))) break;
      
    } /* main loop */
  
  if (slicestodo) printf("There are %d slices left to calculate\n",
			 slicestodo);
  
  
  printf("Root has finished, result found = [%lf]\n", result);
  fflush(stdout);

  return (0);
}


/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void init_states (int max)
{
  int i;
  
  /* note: 0 is me.. and I don't work */
  for (i=1;i<max;i++) {
    procs[i].rank        = i;
    procs[i].currentwork = -1;
    procs[i].state       = AVAILABLE;
  }
}


void init_work ()
{  
  int i;
  /* note: slice is from 0 to slices-1.. so if = slices then n/a */
  for (i=0;i<slices;i++) {
    work[i].workid    = i;
    work[i].rank      = MPI_UNDEFINED;
    work[i].workstate = WNOTDONE;
  }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* Comment: the second argument workid is just used in a single case */
void advance_state ( int r,  long workid)
{
  int tmp;

  switch( procs[r].state )
    {
    case (AVAILABLE):
      {
	if ( workid == NULLWORKID ) {
	  printf("Invalid workid %d for proc: %d \n", (int) workid, r);
	  MPI_Abort ( MPI_COMM_WORLD, 1 );
	}
	else if ( workid == FINISH ) {
	  procs[r].state       = FINISHED;
	  procs[r].currentwork = slices; /* empty tag */
	  printf("FINISHED:[%d]\n", r);
	}
	else { 
	  procs[r].state       = WORKING;
	  procs[r].currentwork = workid;
      
	  printf("SENT WORK:[%d] work [%ld]\n", r, workid);

	  /* mark work as 'in progress' */
	  work[workid].workstate = WINPROGRESS;
	  work[workid].rank      = r;
	}
	break;
      }
    case (WORKING):
      {
	procs[r].state = RECEIVED;
	break;
      }
    case (RECEIVED):
      {
	/* mark work as finished */
	tmp = procs[r].currentwork;
	work[tmp].workstate = WDONE;

	procs[r].state       = AVAILABLE;
	procs[r].currentwork = slices;	/* i.e. empty tag */
	printf("DONE WORK:[%d] work [%ld]\n", r, tmp);  

	break;
      }
    case ( SEND_FAILED ):
      {
	procs[r].state = AVAILABLE;
	break;
      }
    case ( RECV_FAILED):
      {
	procs[r].state = WORKING;
	break;
      }
    case (DEAD):
      {
	/* State of process not updated, since done or dead */
#if !defined(USE_BLANK_MODE) && !defined(USE_SHRINK_MODE) 	
	procs[r].state = AVAILABLE;
	procs[r].currentwork = slices; /* empty tag */
	printf("MASTER: rank %d was reset to AVAILABLE\n", r );
#endif
	break;
      }
    case (FINISHED):
      {
	/* do nothing anymore */
	break;
      }
    default:
      {
	printf("Invalid state of proc %d\n", r );
	MPI_Abort ( MPI_COMM_WORLD, 1 );

	break;
      }
    }

}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void mark_error ( int r )
{
  int tmp;

  if ( procs[r].state == AVAILABLE ) 
    procs[r].state = SEND_FAILED;
  else if ( procs[r].state == WORKING )
    procs[r].state = RECV_FAILED;
}

void mark_dead (int r)
{
  int tmp;

#if !defined(USE_BLANK_MODE) && !defined(USE_SHRINK_MODE) 	
  if ( (procs[r].state == SEND_FAILED ) || 
       (procs[r].state == RECV_FAILED ) )
#endif
    procs[r].state = DEAD;
#if !defined(USE_BLANK_MODE) && !defined(USE_SHRINK_MODE) 	
  else
    procs[r].state = AVAILABLE;
#endif

  /* mark its current work as not yet done again */
  tmp                 = procs[r].currentwork;
  work[tmp].workstate = WNOTDONE;
  work[tmp].rank      = MPI_UNDEFINED;
  
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
/* getnextwork */
long getnextwork ( void )
{
  long slice=FINISH;
  int i;
  
   for(i=0;i<slices;i++) {
     if ( work[i].workstate == WNOTDONE ) {
       slice = work[i].workid;
       break;
     }
   }
   
   return ( slice );
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
#ifdef COLLECTIVE_CHECKWHODIED
/* This routine is used to check, who and how many processes are
   respawned. It is called by all processes after MPI_Init and
   in the recovery function. The way how to determine who is respawned
   is done by using a static, global  variable, which is set to
   1 after checkwhodied has been called the first time. Therefore,
   we have three possibilities:
   - the sum of the global variables is zero: all processes are
     freshly started
   - the sum of the global variables is: 1<sum<size-1 : Using an
     allgather, we can even determine who is respawned
   - sum > size-1: something went wrong, abort:

   ATTENTION: I think this routine does not survive a failure
              during its execution /I have to think about this.

	      This routines does not work for the SHRINK mode

   EG July 13 2003
*/
   
int* checkwhodied ( int wasalivebefore, int size, int *numrespawned)
{
  int rc;
  int *totalarr, *respawnedarr;
  int i, j;
  int sum;
  
  rc = MPI_Allreduce (&wasalivebefore, &sum, 1, MPI_INT, MPI_SUM, 
		      MPI_COMM_WORLD );
  
  if ( sum == 0 )
    {
      /* All processes are freshly started */
      *numrespawned = 0;
      return ( NULL );
    }
  else if (sum > 0) 
    {
      totalarr     = (int *)malloc ( sizeof(int) * size);
      respawnedarr = (int *)malloc ( sizeof(int) * (size-sum));
      if ( (totalarr == NULL) || (respawnedarr == NULL) ) 
	MPI_Abort (MPI_COMM_WORLD,1 );
      
      memset ( totalarr, 0, (sizeof(int) * size ));
      
      rc = MPI_Allgather ( &wasalivebefore, 1, MPI_INT, totalarr,
			   1, MPI_INT, MPI_COMM_WORLD );
      
      for ( j = 0, i = 0; i < size ; i++)
	if ( totalarr[i] == 0 ) 
	  respawnedarr[j++] = i;
      
      free ( totalarr );
      *numrespawned = (size - sum );
      return ( respawnedarr );
    }

  /* If we are here, sum was > size-1, which means, something
     went completly wrong.... */
  
  MPI_Abort ( MPI_COMM_WORLD, 1);
  return ( NULL );
}

#else
/* This routine is used to check, who and how many processes 
   have failed. It uses two differen attributes to determine
   how many procs have failed (FTMPI_NUM_FAILED_PROCS) and
   who. The latter one is hidden in the Error string returned
   by the Attribute FTMPI_ERRCODE_FAILED (which is in fact
   an error code).
   
   In contrary to the previous version this
   routine is purely local, and does not involve any communication.
 
   EG August 6 2003
*/
   
int* checkwhodied ( int wasalivebefore, int size, int *numfailed)
{
  int *failedarr;
  int *valp1;
  int errcode;
  int flag;

  char headerstring[128];
  char column;
  int offset=0;
  char errstring[MPI_MAX_ERROR_STRING], tmpstring[MPI_MAX_ERROR_STRING];
  int k=0, i;

  MPI_Comm_get_attr ( comm, FTMPI_ERROR_FAILURE, (void **)&valp1, &flag );
  errcode = (int) *valp1;

  MPI_Comm_get_attr ( comm, FTMPI_NUM_FAILED_PROCS, (void **)&valp1, &flag);
  *numfailed = (int ) *valp1;

  failedarr = (int *) malloc ( sizeof(int) * (*numfailed));
  if ( failedarr == NULL )
    MPI_Abort ( MPI_COMM_WORLD, 1 );

  /* Get error string and parse it */
  MPI_Error_string ( errcode, errstring, &flag);

  if ( !strncmp( errstring, "No error", MPI_MAX_ERROR_STRING) ) {
    printf("Parsing of the string returns: %s \n", errstring);
    return ( NULL );
  }
  
  /* Header */
  sscanf ( errstring, "%[^:] %c  %[^\n]", headerstring,  &column, tmpstring);
  /* List of ranks */
  for ( i = 0; i < *numfailed; i ++) {
    sscanf ( tmpstring+offset, "%d", &flag );
    /* 5 is the result of the 4 digit representation
       and 1 blank */
    offset += 5;
    failedarr[k++] = flag;
  }

  return ( failedarr );
}
#endif
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void recover_master ( MPI_Comm *comm, int *err, ...)
{

  MPI_Comm oldcomm, newcomm;
  int rc;
  int size, i, rank;
  int *tmparr;
#ifdef USE_SHRINK_MODE
  proc_t copyproc;
  int most, k;
#endif

  if ( *err == MPI_ERR_OTHER ) { 
    /* Mark the communication to the procs. which didn't succeed
       as erroneous  */
    mark_error ( j ) ;
    
    oldcomm = MPI_COMM_WORLD;
    newcomm = FT_MPI_CHECK_RECOVER;
    
    rc = MPI_Comm_dup (oldcomm, &newcomm);
    rc = MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    rc = MPI_Comm_size (MPI_COMM_WORLD, &size);
    printf("[%d]: AFTER Recovery size : %d ret : %d\n", 
	   rank, size, rc); fflush(stdout);

    tmparr = checkwhodied ( wasalivebefore, size, &rc );
    if ( tmparr != NULL )
      {
	printf("MASTER: %d procs has/have failed \n", rc );
	for ( i = 0; i < rc; i++)
	  {
	    printf("MASTER: rank %d failed\n",
		   tmparr[i]);
	    mark_dead ( tmparr[i] );
	  }
	free ( tmparr );
      }

    for ( i = 0; i < size; i++ )
      printf("MASTER: state of rank %d is %d \n", i, procs[i].state );

#ifdef USE_SHRINK_MODE 
    /* Shrink the procs-list */
    most = size + rc;
    for ( i = 1, k = 1; i < most; i++ ) {
      copyproc = procs[i];
      if ( i != k ) { 
	procs[i].rank        = MPI_UNDEFINED;
	procs[i].currentwork = MPI_UNDEFINED;
	procs[i].state       = MPI_UNDEFINED;
	
	procs[k] = copyproc ;
      }
      if ( procs[i].state != DEAD)
	k++;
    }
    maxworkers = size;
 #endif
    } 
  else
    {
      printf("MASTER: Error handler called with error code %d\n",
	     *err );
      sleep ( 30 );
    }
}
