#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "mpi.h"
#include "solvergen.h"

int masterrank = 0;
static int cprank = -1;

static int mystate=AVAILABLE;

int slave ( int myrank )
{
  int done;
  int rc;
  long howmanydone;
  long todo;
  double result;
  double width;
  double x, y;
  MPI_Status status;
  MPI_Comm comm = MPI_COMM_WORLD;

  cprank = myrank;
  
  /* for the calculation I will do */
  width = (double) 1.0 / ((double) slices);
  
  /* status */	
  done = 0;
  howmanydone = 0;

  
  /* all I do is get work, do a calculation and then return an answer */
  while (!done) 
    {
      /* get work */
      if ( mystate == AVAILABLE )
	{
	  rc = MPI_Recv ( &todo, 1, MPI_LONG, masterrank, WORK_TAG, comm, &status );
	  slave_advance_state (todo);
	}

      if ( mystate == RECEIVED )
	{
	  /* calculate */
	  howmanydone++;
	      
	  /* pi */
	  x = width * ((double) todo);
	  y = 4.0 / (1.0 + x*x);
	  result = y * width;
	      
	  /* check for delay here */
	  if (delay) usleep (delay);
	  slave_advance_state(NULLWORKID);
	}
      
      if ( mystate == WORKING )
	{
	  /* return work */
	  rc = MPI_Send ( &result, 1, MPI_DOUBLE, masterrank, RES_TAG, comm );
	  slave_advance_state(NULLWORKID);
	}

      if ( mystate == FINISHED ) 
	break;
    
    } /* while not done.. */
  
  printf("I am slave [%d] and I did [%u] operations\n", myrank, howmanydone);
  
  return (0);
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void slave_advance_state ( long workid )
{

  switch( mystate )
    {
    case (AVAILABLE):
      {
	if ( workid == NULLWORKID ) {
	  /* do nothing, master has died and we need to keep the
	     Available state */
	}
	else if ( workid == FINISH ) {
	  mystate = FINISHED;
	}
	else {
	  mystate = RECEIVED;
	}

	break;
      }
    case (RECEIVED):
      {
	mystate = WORKING;
	break;
      }
    case (WORKING):
      {
	mystate = AVAILABLE;
	break;
      }
    case ( SEND_FAILED ):
      {
	mystate = WORKING;
	break;
      }
    case ( RECV_FAILED):
      {
	mystate = AVAILABLE;
	break;
      }
    case (DEAD):
    case (FINISHED):
      {
	/* Not really defined for the slave */
	break;
      }
    default:
      {
	printf("Invalid state \n");
	MPI_Abort ( MPI_COMM_WORLD, 1 );

	break;
      }
    }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void slave_mark_error ()
{
  if ( mystate == AVAILABLE ) 
    mystate = RECV_FAILED;
  else if ( mystate == WORKING ) 
    mystate = SEND_FAILED;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void slave_mark_available ()
{
  mystate = AVAILABLE;
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/
void recover_slave ( MPI_Comm *comm, int *err, ... )
{

  MPI_Comm oldcomm, newcomm;
  int rc;
  int size, rank;
  int *tmparr, i;

 if ( *err == MPI_ERR_OTHER ) { 
    oldcomm = MPI_COMM_WORLD;
    newcomm = FT_MPI_CHECK_RECOVER;
    
    rc = MPI_Comm_dup (oldcomm, &newcomm);
    rc = MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    rc = MPI_Comm_size (MPI_COMM_WORLD, &size);
    printf("[%d]: AFTER Recovery size: %d ret: %d\n", 
	   rank, size, rc); fflush(stdout);
    
    
    tmparr = checkwhodied ( wasalivebefore, size, &rc );
    slave_mark_error ();
    if (tmparr != NULL )    
      {
	for ( i = 0; i < rc; i++)
	  if ( tmparr[i] == masterrank )
	    {
	      /* Master has died, throw our current work
		 away, and wait for new portions */
	      /* This call overrides the mark_fault call from
		 three lines before */
	      slave_mark_available();
	    }      
	free ( tmparr );
      }
    }
 else
   {
     printf("%d: Error handler called with error code %d\n",
	    cprank, *err );
     sleep ( 30 );
   }
}
/**********************************************************************/
/**********************************************************************/
/**********************************************************************/













