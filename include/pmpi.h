
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This file contains the PMPI_ defs for the MPI functions
 Without this there are no prototypes etc etc etc 
 and everything just returns an int... like Wtime! which is wrong

 GEF UTK/Jan03
 */


#ifndef _FT_PMPI_H
#define _FT_PMPI_H


/* A.2 prototypes for user defined functions (C) */

/* no proto needed? */

/* END A.2 prototypes for user defined functions (C) */


/* A.3 C binding for Point-to-point Communication */

int PMPI_Send(void*, int, MPI_Datatype, int, int, MPI_Comm);
int PMPI_Recv(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Status *);
int PMPI_Get_count(MPI_Status *, MPI_Datatype, int *);
int PMPI_Bsend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int PMPI_Ssend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int PMPI_Rsend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int PMPI_Buffer_attach( void*, int);
int PMPI_Buffer_detach( void*, int*);
int PMPI_Isend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Ibsend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Issend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Irsend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Irecv(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Wait(MPI_Request *, MPI_Status *);
int PMPI_Test(MPI_Request *, int *, MPI_Status *);
int PMPI_Request_free(MPI_Request *);
int PMPI_Request_get_status (MPI_Request request, int *flag, MPI_Status *status);
int PMPI_Waitany(int, MPI_Request *, int *, MPI_Status *);
int PMPI_Testany(int, MPI_Request *, int *, int *, MPI_Status *);
int PMPI_Waitall(int, MPI_Request *, MPI_Status *);
int PMPI_Testall(int, MPI_Request *, int *, MPI_Status *);
int PMPI_Waitsome(int, MPI_Request *, int *, int *, MPI_Status *);
int PMPI_Testsome(int, MPI_Request *, int *, int *, MPI_Status *);
int PMPI_Iprobe(int, int, MPI_Comm, int *flag, MPI_Status *);
int PMPI_Probe(int, int, MPI_Comm, MPI_Status *);
int PMPI_Cancel(MPI_Request *);
int PMPI_Test_cancelled(MPI_Status *, int *);
int PMPI_Send_init(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int PMPI_Bsend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int PMPI_Ssend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int PMPI_Rsend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int PMPI_Recv_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int PMPI_Start(MPI_Request *);
int PMPI_Startall(int, MPI_Request *);
int PMPI_Sendrecv(void *, int, MPI_Datatype,int, int, void *, int, MPI_Datatype,
int, int, MPI_Comm, MPI_Status *);
int PMPI_Sendrecv_replace(void*, int, MPI_Datatype, int, int, int, int,
MPI_Comm, MPI_Status *);
int PMPI_Type_contiguous(int, MPI_Datatype, MPI_Datatype *);
int PMPI_Type_vector(int, int, int, MPI_Datatype, MPI_Datatype *);
int PMPI_Type_hvector(int, int, MPI_Aint, MPI_Datatype, MPI_Datatype *);
int PMPI_Type_indexed(int, int *, int *, MPI_Datatype, MPI_Datatype *);
int PMPI_Type_hindexed(int, int *, MPI_Aint *, MPI_Datatype, MPI_Datatype *);
int PMPI_Type_struct(int, int *, MPI_Aint *, MPI_Datatype *, MPI_Datatype *);
int PMPI_Type_get_envelope( MPI_Datatype ddt, int * num_int, int * num_addr,
			    int * num_ddt,int * combiner);
int PMPI_Type_get_contents( MPI_Datatype ddt, int max_int, int max_addr, 
			    int max_ddt, int * array_int, MPI_Aint * array_addr,
			    MPI_Datatype * array_ddt );
int PMPI_Type_create_darray( int size, int rank, int ndims,
			     int *array_of_gsizes, int *array_of_distribs,
			     int *array_of_dargs, int *array_of_psizes,
			     int order, MPI_Datatype oldtype,
			     MPI_Datatype *newtype);

int PMPI_Type_create_subarray( int ndims,int *array_of_sizes,
			       int *array_of_subsizes,int *array_of_starts,
			       int order,MPI_Datatype oldtype,
			       MPI_Datatype *newtype);
int PMPI_Address(void*, MPI_Aint *);
int PMPI_Type_extent(MPI_Datatype, MPI_Aint *);
int PMPI_Type_size(MPI_Datatype, int *);
int PMPI_Type_lb(MPI_Datatype, MPI_Aint*);
int PMPI_Type_ub(MPI_Datatype, MPI_Aint*);
int PMPI_Type_commit(MPI_Datatype *);
int PMPI_Type_free(MPI_Datatype *);
int PMPI_Get_elements(MPI_Status *, MPI_Datatype, int *);
int PMPI_Pack(void*, int, MPI_Datatype, void *, int, int *,  MPI_Comm);
int PMPI_Unpack(void*, int, int *, void *, int, MPI_Datatype, MPI_Comm);
int PMPI_Pack_size(int, MPI_Datatype, MPI_Comm, int *);

/* END A.3 C binding for Point-to-point Communication */

/* type count? */

/* A.3 C binding for Collective Communication */

int PMPI_Barrier(MPI_Comm );
int PMPI_Bcast(void*, int, MPI_Datatype, int, MPI_Comm );
int PMPI_Gather(void* , int, MPI_Datatype, void*, int, MPI_Datatype, int,
MPI_Comm);
int PMPI_Gatherv(void* , int, MPI_Datatype, void*, int *, int *, MPI_Datatype,
int, MPI_Comm);
int PMPI_Scatter(void* , int, MPI_Datatype, void*, int, MPI_Datatype, int,
MPI_Comm);
int PMPI_Scatterv(void* , int *, int *,  MPI_Datatype, void*, int, MPI_Datatype,
int, MPI_Comm);
int PMPI_Allgather(void* , int, MPI_Datatype, void*, int, MPI_Datatype,
MPI_Comm);
int PMPI_Allgatherv(void* , int, MPI_Datatype, void*, int *, int *,
MPI_Datatype, MPI_Comm);
int PMPI_Alltoall(void* , int, MPI_Datatype, void*, int, MPI_Datatype,
MPI_Comm);
int PMPI_Alltoallv(void* , int *, int *, MPI_Datatype, void*, int *, int *,
MPI_Datatype, MPI_Comm);
int PMPI_Reduce(void* , void*, int, MPI_Datatype, MPI_Op, int, MPI_Comm);
int PMPI_Op_create(MPI_User_function *, int, MPI_Op *);
int PMPI_Op_free( MPI_Op *);
int PMPI_Allreduce(void* , void*, int, MPI_Datatype, MPI_Op, MPI_Comm);
int PMPI_Allreduce_a(void* , void*,void *, int, MPI_Datatype, MPI_Op, MPI_Comm);
int PMPI_Reduce_scatter(void* , void*, int *, MPI_Datatype, MPI_Op, MPI_Comm);
int PMPI_Scan(void* , void*, int, MPI_Datatype, MPI_Op, MPI_Comm );

/* END A.3 C binding for Collective Communication */


/* A.3 C binding for Groups, Contexts and Communicators */

int PMPI_Group_size(MPI_Group group, int *);
int PMPI_Group_rank(MPI_Group group, int *);
int PMPI_Group_translate_ranks (MPI_Group, int, int *, MPI_Group, int *);
int PMPI_Group_compare(MPI_Group, MPI_Group, int *);
int PMPI_Comm_group(MPI_Comm, MPI_Group *);
int PMPI_Group_union(MPI_Group, MPI_Group, MPI_Group *);
int PMPI_Group_intersection(MPI_Group, MPI_Group, MPI_Group *);
int PMPI_Group_difference(MPI_Group, MPI_Group, MPI_Group *);
int PMPI_Group_incl(MPI_Group group, int, int *, MPI_Group *);
int PMPI_Group_excl(MPI_Group group, int, int *, MPI_Group *);
int PMPI_Group_range_incl(MPI_Group group, int, int [][3], MPI_Group *);
int PMPI_Group_range_excl(MPI_Group group, int, int [][3], MPI_Group *);
int PMPI_Group_free(MPI_Group *);

int PMPI_Comm_size(MPI_Comm, int *);
int PMPI_Comm_rank(MPI_Comm, int *);
int PMPI_Comm_compare(MPI_Comm, MPI_Comm, int *);
int PMPI_Comm_dup(MPI_Comm, MPI_Comm *);
int PMPI_Comm_create(MPI_Comm, MPI_Group, MPI_Comm *);
int PMPI_Comm_split(MPI_Comm, int, int, MPI_Comm *);
int PMPI_Comm_free(MPI_Comm *);
int PMPI_Comm_test_inter(MPI_Comm, int *);
int PMPI_Comm_remote_size(MPI_Comm, int *);
int PMPI_Comm_remote_group(MPI_Comm, MPI_Group *);

int PMPI_Intercomm_create(MPI_Comm, int, MPI_Comm, int, int, MPI_Comm * );
int PMPI_Intercomm_merge(MPI_Comm, int, MPI_Comm *);

int PMPI_Keyval_create(MPI_Copy_function *, MPI_Delete_function *, int *,
void*);
int PMPI_Keyval_free(int *);

int PMPI_Attr_put(MPI_Comm, int, void*);
int PMPI_Attr_get(MPI_Comm, int, void *, int *);
int PMPI_Attr_delete(MPI_Comm, int);

/* END A.3 C binding for Groups, Contexts and Communicators */


/* A.3 C binding for Process Topologies */

int PMPI_Cart_create(MPI_Comm, int, int *, int *, int, MPI_Comm *);
int PMPI_Dims_create(int, int, int *);
int PMPI_Graph_create(MPI_Comm, int, int *, int *, int, MPI_Comm *);
int PMPI_Topo_test(MPI_Comm, int *);
int PMPI_Graphdims_get(MPI_Comm, int *, int *);
int PMPI_Graph_get(MPI_Comm, int, int, int *, int *);
int PMPI_Cartdim_get(MPI_Comm, int *);
int PMPI_Cart_get(MPI_Comm, int, int *, int *, int *);
int PMPI_Cart_rank(MPI_Comm, int *, int *);
int PMPI_Cart_coords(MPI_Comm, int, int, int *);
int PMPI_Graph_neighbors_count(MPI_Comm, int, int *);
int PMPI_Graph_neighbors(MPI_Comm, int, int, int *);
int PMPI_Cart_shift(MPI_Comm, int, int, int *, int *);
int PMPI_Cart_sub(MPI_Comm, int *, MPI_Comm *);
int PMPI_Cart_map(MPI_Comm, int, int *, int *, int *);
int PMPI_Graph_map(MPI_Comm, int, int *, int *, int *);


/* END A.3 C binding for Process Topologies */


/* A.3 C binding for Environmental Inquiry */

int PMPI_Get_processor_name(char *, int *);
int PMPI_Errhandler_create(MPI_Handler_function *, MPI_Errhandler *);
int PMPI_Errhandler_get(MPI_Comm, MPI_Errhandler *);
int PMPI_Errhandler_set(MPI_Comm, MPI_Errhandler);
int PMPI_Errhandler_free(MPI_Errhandler *);
int PMPI_Error_string(int, char *, int *);
int PMPI_Error_class(int, int *);
int PMPI_Get_version(int *version, int *subversion );

double PMPI_Wtime(void);
double PMPI_Wtick(void);

int PMPI_Init(int *, char ***);
int PMPI_Finalize(void);
int PMPI_Initialized(int *);
int PMPI_Abort(MPI_Comm, int);
int MPI_Pcontrol (int level, ...);

/* END A.3 C binding for Environmental Inquiry */

/* ***
int PMPI_Pack(void * ,int , MPI_Datatype ,void * ,int ,int * ,MPI_Comm );
int PMPI_Unpack(void * ,int ,int * ,void * ,int ,MPI_Datatype ,MPI_Comm );

*** */

/* *** keep these incase int verse MPI_Datatype causes prototype errors 
int  PMPI_Type_commit(int*);
int  PMPI_Type_contiguous(int,int,int *);
int  PMPI_Type_extent(int,int *);
int  PMPI_Type_free(int*);
int  PMPI_Type_hindexed(int,int *,int *,int,int *);
int  PMPI_Type_hvector(int,int,int,int,int *);
int  PMPI_Type_indexed(int,int *,int *,int,int *);
int  PMPI_Type_size(int,int *);
int  PMPI_Type_struct(int,int *,int *,int *,int *);
int  PMPI_Type_vector(int,int,int,int,int *);

*** */

/* missed stuff */
int PMPI_NULL_COPY_FN(MPI_Comm a, int b, void * c, void * d, void * f, int * h
);
int PMPI_NULL_DELETE_FN(MPI_Comm a, int b, void * c, void * d );

/* Some MPI-2 functions */

int PMPI_Comm_create_keyval ( MPI_Comm_copy_attr_function *copy_fn,
			     MPI_Comm_delete_attr_function *del_fn,
			     int *keyval, void *extra_state );
int PMPI_Comm_free_keyval ( int *keyval );
int PMPI_Comm_set_attr ( MPI_Comm comm, int keyval, void *attribute_val );
int PMPI_Comm_get_attr ( MPI_Comm comm, int keyval, void *attribute_val, 
			int *flag);
int PMPI_Comm_delete_attr ( MPI_Comm comm, int keyval );
int PMPI_Comm_create_errhandler ( MPI_Comm_errhandler_fn *fn, MPI_Errhandler *errhandler );
int PMPI_Comm_set_errhandler    ( MPI_Comm comm, MPI_Errhandler errhandler );
int PMPI_Comm_get_errhandler    ( MPI_Comm comm, MPI_Errhandler *errhandler );
int PMPI_Add_error_class  ( int *errorclass );
int PMPI_Add_error_code   ( int errorclass, int *errorcode );
int PMPI_Add_error_string ( int errorcode, char *string );
int PMPI_Comm_call_errhandler ( MPI_Comm comm, int errorcode );
MPI_Datatype PMPI_Type_f2c ( MPI_Fint ftype );
MPI_Fint PMPI_Type_c2f ( MPI_Datatype dtype );
MPI_Comm PMPI_Comm_f2c ( MPI_Fint fcomm );
MPI_Fint PMPI_Comm_c2f ( MPI_Comm comm );
MPI_Group PMPI_Group_f2c ( MPI_Fint fgroup);
MPI_Fint PMPI_Group_c2f ( MPI_Group group );
MPI_Op PMPI_Op_f2c ( MPI_Fint fop);
MPI_Fint PMPI_Op_c2f ( MPI_Op op);
MPI_Request PMPI_Request_f2c ( MPI_Fint freq );
MPI_Fint  PMPI_Request_c2f ( MPI_Request req );
MPI_Win PMPI_Win_f2c ( MPI_Fint fwin );
MPI_Fint PMPI_Win_c2f ( MPI_Win win );

#ifdef DECLARE_MPIIO
MPI_File PMPI_File_f2c ( MPI_Fint ffile );
MPI_Fint PMPI_File_c2f ( MPI_Fint file );
MPI_Info PMPI_Info_f2c ( MPI_Fint finfo );
MPI_Fint PMPI_Info_c2f ( MPI_Info info );
#endif  /* DECLARE_MPIIO */

#endif /* _FT_PMPI_H */
/* End of profiled prototypes :) */
