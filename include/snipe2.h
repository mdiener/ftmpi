/*
 * snipe 2
 *
 * nonblocking medium level coms library
 * this library creates a list of communications and completes them
 *
 * all channels and actual send/recv is done by the snipe library
 */

/* #include <sys/poll.h> */

#include "snipe2-msg-list.h"

/* shorthand / macros */

/* prototype of callback functions */
#define SNIPE2CALLBACKPROTO int req, snipe2_msg_list_t* reqptr, int onchan, long userval
/* all call back functions should use this */
/* req & reqptr indicate the request completed/failed on channel chan */
/* userval is the value given in the original postcall */


/* 
 * prototypes 
 */

/* must be called */
int     snipe2_init ();

/* create a channel path & enable */
int snipe2_add_chan (int fd, int type, intfuncptr errorcallback, long errval, int autocancel);


/* free up pending messages and remove channel path */
int     snipe2_remove_chan (int chan);

/* reset accept / chanect done flag on a channel */
int		snipe2_reset_poll_chan (int chan);

/* control sending on a channel path */
int		snipe2_suspendsending_chan (int chan);
int		snipe2_resumesending_chan (int chan);

/* call back functions must have the following form */
/* int fback (int reqid, snipe2_msg_list_t* reqptr, userdefinedvalue) */
/* on callback, all the values will be set before calling back */

int		snipe2_cancel_msg (snipe2_msg_list_t *reqptr);

int 	snipe2_post_send_msg (int chan, char *msg, long msglen, intfuncptr callback, long uservalue, int autodelete, snipe2_msg_list_t** reqptr);
int 	snipe2_post_recv_msg (int chan, char *msg, long msglen, intfuncptr callback, long uservalue, int autodelete, snipe2_msg_list_t** reqptr);

int		snipe2_reqptr_2_reqid (snipe2_msg_list_t* req);
snipe2_msg_list_t* snipe2_reqid_2_reqptr (int reqid);	/* warning slow */

int		snipe2_isdone (snipe2_msg_list_t* req, int deleteifdone);
int		snipe2_isdone_reqid (int reqid, int deleteifdone); /* slower */

int		snipe2_progress (int tchan, int flags);


int     snipe2_pending (); /* total pending on all queues */

int snipe2_pending_sends_on_chan (int chan);
int snipe2_pending_recvs_on_chan (int chan);


/* debugging functions */
int     snipe2_dump_chan (int chan, int flags);
int     snipe2_dump_all_chans ();
int		snipe2_dump_poll_flags ();
int		snipe2_dump_poll_result (); /* internal / debugging */

/* chanstants for calls */

#define SNIPE2NULLCONN 0

/* add channel calls */
#define SNIPE2POLLONLY		0
#define SNIPE2DATA			1


/* progress call */
/* tchan on progress */
#define SNIPE2ALL 0 			/* all channels [default] */


/* progress call */
/* flags on progress */

#define SNIPE2TEST 0			/* do what we can (probe/test) */
								/* but returns as soon as possible */

#define SNIPE2PROGRESS 1		/* do what we can (test+) */
								/* return only after something has happened */
								/* not quite a 'some/any' as it does not */
								/* wait for completion but does block */


/* these loop until a chandition is met */

#define SNIPE2SOME 2			/* return after completing something! */

#define SNIPE2ALLSENDS 4 		/* complete all pending sends */
#define SNIPE2SOMESEND 8 		/* complete some pending sends */

#define SNIPE2ALLRECVS 16 		/* complete all pending recvs */
#define SNIPE2SOMERECV 32 		/* complete any/some pending recv  */

/* special cases */

/* this is bad as if the req is a send, it might rely on new recvs to be posted
 * which cannot happen as it is in a tight loop!
 */
/* #define SNIPE2UNTILREQ 128		 */
/* run until given is completed */

/* not fully implemented yet */

/* #define SNIPE2ALLCURRENTSENDS 256  */
/* run until given is completed */


