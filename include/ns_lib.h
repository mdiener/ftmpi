
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

	The Name Service code is derived from the Name Service
	Code that was developed for the benchmark run of linpeak(tm)
	on the ASCI Pacific Blue machine using MPI_Connect.

	The MPI_Connect project was supported in part by grants from 
	the DoD under the DOD MOD office PET program.

*/

#ifndef _GHCORE_NSLIB_H
#define _GHCORE_NSLIBG_H  1


/*
int ns_init (hname, port)
char *hname;
int port;
*/

int ns_init (char * hname,int port);

/* returns 0 if a server found, or -1 if not */
int ns_open (void);

/* shuts connection down */
void ns_close (void);

/* simple get unique id function */
/* this can be used in place of the mkid plugin */
/* this routine makes an id up as needed, its args */
/* indicate which name space is used */
/* the lib caches the id, and returns this every time asked */
int	ns_gid (int index, int range, int *gids);

#define NS_ID_CORE	0
#define NS_ID_OTHER	1
#define NS_ID_FTMPI	2
#define NS_ID_FTMPI_COMS	3
#define _NS_ID_END  4



/* dumps the name service contents AT the name server */
/* used a diag tool for debugging things. */
/* the users client library should not expect to get to this data directly and should */
/* use the ns_info calls instead */
/* returns number of groups/names dumped. Each unique name is counted just once */
int	ns_dump (void);

/* adds me to the group named by gname */
/* int ns_add (gname, info1, info2, group, member) */
/* char *gname; */
/* int info1, info2; */
/* int *group; */
/* int *member; */

int	ns_add (char * gname,int info1,int info2,int * group,int * member);

/* remove me for the group named by gname */
/* int ns_del (gname, group, member) */
/* char *gname; */
/* int group; */
/* int member; */

int	ns_del (char *gname,int group,int member);

/* get info for the group named by gname */
/* return me upto rsize element details */
/* return code is the number of entries returned or -1 for error */
/* if you allocated no space, then 0 is a valid return */
/* hense we return nmembers if you want to try again :) */
/* entries, info1 and 2 can be NULL, the others cannot */
/* int ns_info (gname, rsize, group, nmembers, entries, info1s, info2s) */
/* char *gname; */
/* int rsize; */
/* int *group; */
/* int *nmembers; */
/* int *entries; */
/* int *info1s; */
/* int *info2s; */

int	ns_info (char *gname,int rsize,int * group,int * nmembers,int * entries,int * info1s,int * info2s);

int ns_echo (double *timeresult);
/*-- Data functions that do more than host/port info */
/* these all use the msgbuf routines */

int	ns_record_put (int me,char * rname,int rmb,int flags,int reqslot,int * record,int * slot,int delaftersend);		/* put a record in the db */
int	ns_record_info (int me,char * rname,int rsize,int * record,int * nmembers,int * entries,int * rlens);		/* get info about records (but not the records) */
int	ns_record_get (int me,char * rname,int flags,int reqslot,int * record,int * slot,int * rmb,int *gencount);		/* get the record data */
/*int ns_record_delete ();*/	/* delete a record (if you can!) */
int ns_record_del(char * rname);
int	ns_record_swap (int me,char * rname,int rmb,int flags,int reqslot,int previous_owner,int * record,int * slot,int delaftersend);		/* do an atomic swap of info, i.e. a combo r/w */

int ns_ismember(char *gname,int ip,int port);
int ns_dumpall (void);
int ns_record_callback_register (int flag, int addr, int port, char *recordName,int slot,int tag,int genCount );
int ns_record_callback_unregister (int addr, int port, char *recordName,int tag);
int ns_halt (void);

/* flag for ns_record_callback_register */
#define DEFAULT 0         /* do nothing */
#define AUTOUNREG 1       /* auto unregister */
#define EXIST 2           /* got data back if the record exist */  
#define ANYSLOT -1



/* now for the record access flags */
/* Note these are very very simple */

/* Used for put/get/swap */
#define RECORD_DB_DEFAULT		0

/* for put this means first available, RW */
/* for swap same */
/* for get first available that is readable, non private */
/* indexes are ignored */
/* you cannot use this flag with others */

/* Used for put/swap */
#define RECORD_DB_SINGLEONLY	1

/* Short hand for index=0 RW, non private, IFEMPTY. There can be only one! */
/* get, doesn't need this as it can use INDEXED with an index of 0 */

/* Used for put */
#define RECORD_DB_READONLY		2

/* only matching ID/key can change this entry */

/* used for get */
#define RECORD_DB_REMOVE		4

/* Like a tuple space read. There can be only one reader (good for tokens) */

/* used for put/swap/get */
#define RECORD_DB_INDEXED		8

/* operation is on this index (entry/slot #). If index does not match, err */

/* used for put (effects get/info/swap etc) */
#define RECORD_DB_PRIVATE		16

/* used for put */
#define RECORD_DB_IFEMPTY		32

/* this is a poor mans test and set, used to do a swap style operation */
/* Note SINGLEONLY is a short hand version of this */


/* Now if users want to put/get records they need the msg header */
/* we include it here for them to use so they don't have to remember to */
/* include it... GEF */

#ifndef _GHCORE_MSG_H
#include "msg.h"
#endif

#endif /* _GHCORE_NSLIB_H */
