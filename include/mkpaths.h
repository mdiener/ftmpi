
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#ifndef _GHCORE_H_MKPATH
#define _GHCORE_H_MKPATH 1

/*
	G.E.Fagg Nov2000
	mkpath and check path calls
	used for maintaining dir structures 
*/


/*
	Makes the path if possible 
	all = 0 then last part of the path is assumed to be a filename
	all = 1 then fpath is really the full path 
*/


int mk_path (char *fpath, int all);
/*
	Makes a directory pname if possible
	does NOT make the intermmediate directories 
	hense an internal function 
*/

int check_mk_dir (char *pname);

/*
	Returns 0 if pname exists and is accessable (file or dir)
	-1 if not
	General function can be used everywhere.
*/

int check_path (char *pname);

#endif /*  _GHCORE_H_MKPATH */
