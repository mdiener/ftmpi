!/*
!	HARNESS G_HCORE
!	HARNESS FT_MPI
!
!	Innovative Computer Laboratory,
!	University of Tennessee,
!	Knoxville, TN, USA.
!
!
! suffer:	Graham E Fagg <fagg@hlrs.de>
!
!
!                              NOTICE
!
! Permission to use, copy, modify, and distribute this software and
! its documentation for any purpose and without fee is hereby granted
! provided that the above copyright notice appear in all copies and
! that both the copyright notice and this permission notice appear in
! supporting documentation.
!
! Neither the University of Tennessee nor the Authors make any
! representations about the suitability of this software for any
! purpose.  This software is provided ``as is'' without express or
! implied warranty.
!
! HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
! U.S. Department of Energy.
!
!*/
!

! this code has mutated way back from the US DOE funded PVMPI/MPI_Connect project

! /* All the functions we need for the inital FT-MPI versions */



!        implicit none 

!/* versions */
!#define MPI_VERSION 1
!#define MPI_SUBVERSION  2

	INTEGER MPI_VERSION, MPI_SUBVERSION
	PARAMETER (MPI_VERSION=1,MPI_SUBVERSION=2)
	

!/* Major.minor.patch.release_status */
!#define FTMPI_VERSION   "0.90.4.b"  


!/* A.2 Defined Constants for C and Fortran */

!/* A.2 return codes */

!#define MPI_SUCCESS 0

	INTEGER MPI_SUCCESS
	PARAMETER(MPI_SUCCESS = 0)

!/* NORMAL MPI ERROR CODES */
!#define MPI_ERR_ARG -10
!#define MPI_ERR_BUFFER -11
!#define MPI_ERR_COMM -12
!#define MPI_ERR_COUNT -13
!#define MPI_ERR_DIMS -14
!#define MPI_ERR_GROUP -15
!#define MPI_ERR_IN_STATUS -16
!#define MPI_ERR_INTERN -17
!#define MPI_ERR_OP -18
!#define MPI_ERR_OTHER -19
!#define MPI_ERR_PENDING -20
!#define MPI_ERR_RANK -21
!#define MPI_ERR_REQUEST -22
!#define MPI_ERR_ROOT -23
!#define MPI_ERR_TAG -24
!#define MPI_ERR_TOPOLOGY -25
!#define MPI_ERR_TRUNCATE -26
!#define MPI_ERR_TYPE -27
!#define MPI_ERR_UNKNOWN -28
!#define MPI_ERR_KEYVAL -29
!#define MPI_ERR_LASTCODE -30

	INTEGER MPI_ERR_ARG, MPI_ERR_BUFFER, MPI_ERR_COMM, MPI_ERR_COUNT
	INTEGER MPI_ERR_DIMS, MPI_ERR_GROUP, MPI_ERR_IN_STATUS
	INTEGER MPI_ERR_INTERN, MPI_ERR_OP, MPI_ERR_OTHER
        INTEGER MPI_ERR_PENDING
	INTEGER MPI_ERR_RANK, MPI_ERR_REQUEST, MPI_ERR_ROOT, MPI_ERR_TAG
	INTEGER MPI_ERR_TOPOLOGY, MPI_ERR_TRUNCATE, MPI_ERR_TYPE
	INTEGER MPI_ERR_UNKNOWN,  MPI_ERR_KEYVAL, MPI_ERR_LASTCODE

	PARAMETER (MPI_ERR_ARG=-10, MPI_ERR_BUFFER=-11,MPI_ERR_COMM=-12)
	PARAMETER (MPI_ERR_COUNT=-13,MPI_ERR_DIMS=-14,MPI_ERR_GROUP=-15)
	PARAMETER (MPI_ERR_IN_STATUS=-16, MPI_ERR_INTERN=-17)
        PARAMETER (MPI_ERR_OP=-18)
	PARAMETER (MPI_ERR_OTHER=-19, MPI_ERR_PENDING=-20 )
        PARAMETER (MPI_ERR_RANK=-21)
	PARAMETER (MPI_ERR_REQUEST=-22,MPI_ERR_ROOT=-23,MPI_ERR_TAG=-24)
	PARAMETER (MPI_ERR_TOPOLOGY=-25, MPI_ERR_TRUNCATE=-26) 
        PARAMETER (MPI_ERR_TYPE=-27)
	PARAMETER (MPI_ERR_UNKNOWN=-28, MPI_ERR_KEYVAL=-29)
        PARAMETER (MPI_ERR_LASTCODE=-30)

!/* *** The following are unique to FTMPI only *** */

!/* This error is returned for each failed operation */
!#define MPI_ERR_NODE_FAILED -30

	INTEGER MPI_ERR_NODE_FAILED
	PARAMETER(MPI_ERR_NODE_FAILED=-30)

!/* This error is returned when in CONT mode and your operation succeeded but */
!/* some other process in this communicator has failed */
!#define MPI_ERR_NODE_FAILED_COMM_OK -31 

	INTEGER MPI_ERR_NODE_FAILED_COMM_OK
	PARAMETER(MPI_ERR_NODE_FAILED_COMM_OK=-31)

!/* The MPI_ERR_NODE_FAILED_COMM_OK can be replaced by MPI_ERR_NODE_FAILED */
!/* but with the status varibles set to possitive values. */
!/* although this only helps with RECV calls */

!#define MPI_INIT_PATHALOGICAL  -32
!#define MPI_ERR_something -33
!#define MPI_ERR_ABORT_CALLED -34
!#define FTMPI_ERR_LASTCODE -35


	INTEGER MPI_INIT_PATHALOGICAL,MPI_ERR_something
	INTEGER MPI_ERR_ABORT_CALLED,FTMPI_ERR_LASTCODE

	PARAMETER( MPI_INIT_PATHALOGICAL=-32,MPI_ERR_something=-33)
	PARAMETER( MPI_ERR_ABORT_CALLED=-34,FTMPI_ERR_LASTCODE=-35)

!/* *** END of FT-MPI specific section *** */

!/* END A.2 return codes */


!/* A.2 assorted constants */

!#define MPI_BOTTOM (void*)0   /* ?? */
!#define MPI_PROC_NULL -31
!#define MPI_ANY_SOURCE -1 
!#define MPI_ANY_TAG -2
!#define MPI_UNDEFINED -99
!#define MPI_BSEND_OVERHEAD 0
!#define MPI_KEYVAL_INVALID -1 /* shouldnot be 0 */


	INTEGER MPI_BOTTOM,MPI_PROC_NULL,MPI_ANY_SOURCE,MPI_ANY_TAG
	INTEGER MPI_UNDEFINED,MPI_BSEND_OVERHEAD,MPI_KEYVAL_INVALID

	PARAMETER (MPI_BOTTOM=0,MPI_PROC_NULL=-31,MPI_ANY_SOURCE=-1)
        PARAMETER (MPI_ANY_TAG=-2)
	PARAMETER (MPI_UNDEFINED=-99,MPI_BSEND_OVERHEAD=0)
        PARAMETER (MPI_KEYVAL_INVALID=-1)

!/* NOTE */
!/* MPI_PROC_NULL AND ANY_SOURCE MUST BE DIFFERENT ! */

!#define MPI_STATUS_IGNORE 1
!#define MPI_STATUSES_IGNORE 1

	INTEGER MPI_STATUS_IGNORE,MPI_STATUSES_IGNORE
	PARAMETER( MPI_STATUS_IGNORE=1,MPI_STATUSES_IGNORE=1)
!/* END A.2 assorted constants */


!/* A.2 Error-handling specifiers */

!#define MPI_ERRORS_ARE_FATAL  300
!#define MPI_ERRORS_RETURN 301

	INTEGER MPI_ERRORS_ARE_FATAL,MPI_ERRORS_RETURN
	PARAMETER( MPI_ERRORS_ARE_FATAL=300,MPI_ERRORS_RETURN=301)

!/* END A.2 Error-handling specifiers */

!/* A.2 Maximum sizes for strings */

!#define MPI_MAX_ERROR_STRING  1024
!#define MPI_MAX_PROCESSOR_NAME 255    /* follows UNIX hostname conv */

	INTEGER MPI_MAX_ERROR_STRING,MPI_MAX_PROCESSOR_NAME
	PARAMETER( MPI_MAX_ERROR_STRING=1024,MPI_MAX_PROCESSOR_NAME=255)

!/* END A.2 Maximum sizes for strings */


!/* A.2 elementary datatypes (C) */
! I GUESS THESE ARE NOT NEEDED

!#define MPI_CHAR           1
!#define MPI_SHORT          4
!#define MPI_INT            6
!#define MPI_LONG           8
!#define MPI_UNSIGNED_CHAR  2
!#define MPI_UNSIGNED_SHORT 5
!#define MPI_UNSIGNED       7
!#define MPI_UNSIGNED_LONG  9
!#define MPI_FLOAT          10
!#define MPI_DOUBLE         11
!#define MPI_LONG_DOUBLE    12
!#define MPI_BYTE           3
!#define MPI_LONG_LONG      20
!#define MPI_LONG_LONG_INT  21
!#define MPI_PACKED         13

        INTEGER MPI_BYTE, MPI_PACKED
	PARAMETER (MPI_BYTE=3, MPI_PACKED=13)

!/* END A.2  elementary datatypes (C) */


!/* A.2 elementary datatypes (FORTRAN) */

!#define MPI_INTEGER           14
!#define MPI_REAL              15
!#define MPI_DOUBLE_PRECISION  16
!#define MPI_COMPLEX           509
!#define MPI_DOUBLE_COMPLEX    510
!#define MPI_LOGICAL           17
!#define MPI_CHARACTER         1

	INTEGER MPI_INTEGER,MPI_REAL,MPI_DOUBLE_PRECISION,MPI_COMPLEX
	INTEGER MPI_DOUBLE_COMPLEX,MPI_LOGICAL,MPI_CHARACTER

	PARAMETER (MPI_INTEGER=14,MPI_REAL=15,MPI_DOUBLE_PRECISION=16)
        PARAMETER (MPI_COMPLEX=509)
	PARAMETER (MPI_DOUBLE_COMPLEX=510,MPI_LOGICAL=17,MPI_CHARACTER=1)

!/* END A.2  elementary datatypes (FORTRAN) */


!/* A.2 datatypes for reduction functions (C) */
! I GUESS THESE ARE NOT NEEDED

!#define MPI_FLOAT_INT         500
!#define MPI_DOUBLE_INT        501
!#define MPI_LONG_INT          502
!#define MPI_2INT              503
!#define MPI_SHORT_INT         504
!#define MPI_LONG_DOUBLE_INT   505

!/* END A.2 datatypes for reduction functions (C) */


!/* A.2 datatypes for reduction functions (FORTRAN) */

!#define MPI_2REAL             506
!#define MPI_2DOUBLE_PRECISION 507
!#define MPI_2INTEGER          508

	INTEGER MPI_2REAL,MPI_2DOUBLE_PRECISION,MPI_2INTEGER
	PARAMETER(MPI_2REAL=506,MPI_2DOUBLE_PRECISION=507)
        PARAMETER(MPI_2INTEGER=508)

!/* END A.2  datatypes for reduction functions (FORTRAN) */


!/* A.2 optional datatypes (FORTRAN) */
!/* they are in the mpif.h file :) */
!/* END A.2 optional datatypes (FORTRAN) */


!/* A.2 optional datatypes (C) */

!/* END A.2 optional datatypes (C) */
! I GUESS THESE ARE NOT NEEDED

!#define MPI_UB             18
!#define MPI_LB             19

	INTEGER MPI_UB,MPI_LB
	PARAMETER(MPI_UB=18,MPI_LB=19)
!/* END A.2 special datatypes for constructing derived datatypes */

!/* *** additional optional datatypes *** */

!#define MPI_2COMPLEX          511
!#define MPI_2DOUBLE_COMPLEX   512

	INTEGER MPI_2COMPLEX,MPI_2DOUBLE_COMPLEX
	PARAMETER(MPI_2COMPLEX=511,MPI_2DOUBLE_COMPLEX=512)

!/* END *** additional optional datatypes *** */


!/* A.2 reserved communicators (C & FORTRAN) */

!#define MPI_COMM_WORLD 0
!#define MPI_COMM_SELF 1

	INTEGER MPI_COMM_WORLD,MPI_COMM_SELF
	PARAMETER(MPI_COMM_WORLD=0,MPI_COMM_SELF=1)

!/* END A.2  reserved communicators (C & FORTRAN) */


!/* A.2 results of communicator and group comparisons */

!#define MPI_IDENT 100
!#define MPI_CONGRUENT 101
!#define MPI_SIMILAR 102
!#define MPI_UNEQUAL 103

	INTEGER MPI_IDENT,MPI_CONGRUENT,MPI_SIMILAR,MPI_UNEQUAL
	PARAMETER(MPI_IDENT=100,MPI_CONGRUENT=101,MPI_SIMILAR=102)
        PARAMETER(MPI_UNEQUAL=103)

!/* END A.2 results of communicator and group comparisons */


!/* A.2 environmental inquiry keys (C & FORTRAN) */

!#define MPI_TAG_UB  0x8000
!#define MPI_IO    0x8001
!#define MPI_HOST  0x8002
!#define MPI_WTIME_IS_GLOBAL 0x8003

      INTEGER MPI_TAG_UB,MPI_IO,MPI_HOST,MPI_WTIME_IS_GLOBAL
      INTEGER MPI_LASTUSEDCODE, FTMPI_ERROR_FAILURE
      INTEGER FTMPI_NUM_FAILED_PROCS
      PARAMETER(MPI_TAG_UB=32768,MPI_IO=32769,MPI_HOST=32770)
      PARAMETER(MPI_WTIME_IS_GLOBAL=32771, MPI_LASTUSEDCODE=32772)
      PARAMETER(FTMPI_ERROR_FAILURE=32773, FTMPI_NUM_FAILED_PROCS=32774)

!/* END A.2 environmental inquiry keys (C & FORTRAN) */


!/* A.2 collective operations (C & FORTRAN) */

!#define MPI_MAX 0
!#define MPI_MIN 1
!#define MPI_SUM 2
!#define MPI_PROD 3
!#define MPI_LAND 4
!#define MPI_BAND 5
!#define MPI_LOR 6
!#define MPI_BOR 7
!#define MPI_LXOR 8
!#define MPI_BXOR 9
!#define MPI_MAXLOC 10
!#define MPI_MINLOC 11

	INTEGER MPI_MAX, MPI_MIN, MPI_SUM, MPI_PROD
	INTEGER MPI_LAND, MPI_BAND, MPI_LOR, MPI_BOR
	INTEGER MPI_LXOR, MPI_BXOR, MPI_MAXLOC, MPI_MINLOC

	PARAMETER (MPI_MAX=0, MPI_MIN=1, MPI_SUM=2, MPI_PROD=3)
	PARAMETER (MPI_LAND=4, MPI_BAND=5, MPI_LOR=6, MPI_BOR=7)
	PARAMETER (MPI_LXOR=8, MPI_BXOR=9, MPI_MAXLOC=10, MPI_MINLOC=11)

!/* END A.2 collective operations (C & FORTRAN) */


!/* A.2 Null handles */

!/* negative so that a random '0' will still be caught */
!#define MPI_GROUP_NULL -2
!#define MPI_COMM_NULL -3
!#define MPI_DATATYPE_NULL -12
!#define MPI_REQUEST_NULL -4
!#define MPI_OP_NULL -5
!#define MPI_ERRHANDLER_NULL -6  

	INTEGER MPI_GROUP_NULL,MPI_COMM_NULL,MPI_DATATYPE_NULL
	INTEGER MPI_REQUEST_NULL,MPI_OP_NULL,MPI_ERRHANDLER_NULL

	PARAMETER (MPI_GROUP_NULL=-2,MPI_COMM_NULL=-3)
        PARAMETER (MPI_DATATYPE_NULL=-12)
	PARAMETER (MPI_REQUEST_NULL=-4,MPI_OP_NULL=-5)
        PARAMETER (MPI_ERRHANDLER_NULL=-6)
!/* END A.2 Null handles */


!/* A.2 Empty group */

!#define MPI_GROUP_EMPTY -1

	INTEGER MPI_GROUP_EMPTY
	PARAMETER(MPI_GROUP_EMPTY=-1)

!/* END A.2 Empty group */


!/* A.2 topologies (C & FORTRAN) */

!#define MPI_GRAPH 100
!#define MPI_CART  200

	INTEGER MPI_GRAPH,MPI_CART
	PARAMETER(MPI_GRAPH=100,MPI_CART=200)

!/* END A.2 topologies (C & FORTRAN) */


!/* Error handler functions */
        EXTERNAL MPI_NULL_COPY_FN, MPI_NULL_DELETE_FN, MPI_DUP_FN      


!/* *** MISC FT-MPI related stuff *** */

!/* Return from an MPI_INIT */
!/* Usually only MPI_SUCCESS or MPI_ERR_OTHER can be returned */
!/* This code means that Init was successfull and that this is a RESTARTED node */

!/* Note its positive to indicate its not normal BUT not an error! */
!#define MPI_INIT_RESTARTED_NODE 20

	INTEGER MPI_INIT_RESTARTED_NODE
	PARAMETER(MPI_INIT_RESTARTED_NODE=20)

!#define FT_MPI_CHECK_RECOVER    0xEFFF

	INTEGER FT_MPI_CHECK_RECOVER
	PARAMETER (FT_MPI_CHECK_RECOVER=61439)


!/* temporary ones for Tone */
!/* this one is not really allowed */
!#define MPI_NOT_INITIALISED     -100
!#define MPI_ERR_UNSUPPORTED_DATAREP     43

	INTEGER MPI_NOT_INITIALISED,MPI_ERR_UNSUPPORTED_DATAREP
	PARAMETER (MPI_NOT_INITIALISED=-100)
        PARAMETER (MPI_ERR_UNSUPPORTED_DATAREP=43)

!/* END *** MISC FT-MPI related stuff *** */




!/* *** MISC FT-MPI related stuff *** */

!/* should be in section A.2 */
!#define MPI_IN_PLACE (void *)-3

	INTEGER MPI_IN_PLACE
	PARAMETER(MPI_IN_PLACE=-3)


!#define MPI_ORDER_C              56
!#define MPI_ORDER_FORTRAN        57
!#define MPI_DISTRIBUTE_BLOCK    121
!#define MPI_DISTRIBUTE_CYCLIC   122
!#define MPI_DISTRIBUTE_NONE     123
!#define MPI_DISTRIBUTE_DFLT_DARG -49767

	INTEGER MPI_ORDER_C,MPI_ORDER_FORTRAN,MPI_DISTRIBUTE_BLOCK
	INTEGER MPI_DISTRIBUTE_CYCLIC,MPI_DISTRIBUTE_NONE
        INTEGER MPI_DISTRIBUTE_DFLT_DARG

	PARAMETER (MPI_ORDER_C=56,MPI_ORDER_FORTRAN=57)
        PARAMETER (MPI_DISTRIBUTE_BLOCK=121)
	PARAMETER (MPI_DISTRIBUTE_CYCLIC=122,MPI_DISTRIBUTE_NONE=123)
	PARAMETER (MPI_DISTRIBUTE_DFLT_DARG=-49767)





! OTHER STUFF ALREADY PRESET IN MPIF.H AND NOT IN MPI.H











































!typedef struct {
!	int MPI_SOURCE;
!	int MPI_TAG;
!	int MPI_ERROR;
! Extra stuff added so that get count works
!	long message_length
!	int datatype	
!	int count of that datatype
!	int cancelled (if a nb op)
!} MPI_Status;

	INTEGER MPI_STATUS_SIZE
	INTEGER MPI_SOURCE
	INTEGER MPI_TAG, MPI_ERROR

	PARAMETER (MPI_STATUS_SIZE=7)
	PARAMETER (MPI_SOURCE=1, MPI_TAG=2, MPI_ERROR=3)
!
! mpi_status_size could be 8 if a long is long...
! eg


!/* temp data structure used for Async message operations */
!/* Subject to change soon (S2CS) */
!typedef struct {
!	int handle;
!} MPI_Request;
! ??


! Need to decide which of the following is true...

	INTEGER MPI_REAL4, MPI_REAL8

	PARAMETER (MPI_REAL4=-12, MPI_REAL8=-12)
!	PARAMETER (MPI_REAL4=MPI_REAL, MPI_REAL8=MPI_DOUBLE_PRECISION)


! Maybe it will work.
! G.E.Fagg Sep 1999-2002. (c) fagg@cs.utk.edu/Graham@Fagg.com
! Im wasted doing code like this. Someone, quick give me a real job.

! above comment was really in the pvmpi code

		DOUBLE PRECISION MPI_WTIME
	  	EXTERNAL MPI_WTIME

		DOUBLE PRECISION MPI_WTICK
	  	EXTERNAL MPI_WTICK
