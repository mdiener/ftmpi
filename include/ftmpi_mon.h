/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _FT_MPI_H_MON
#define _FT_MPI_H_MON 1

#define FTMPI_MON_CONNECT 2001
#define FTMPI_MON_NEWCOMM 2002
#define FTMPI_MON_FREECOM 2003
#define FTMPI_MON_COPYCOM 2004
#define FTMPI_MON_COMSTATE 2005
#define FTMPI_MON_PROCSTATE 2006
#define FTMPI_MON_CONNSTATE 2007
#define FTMPI_MON_UPDATECOM 2008
#define FTMPI_MON_FINALIZE 2009

#endif /* _FT_MPI_H_MON */
