
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _GHCORE_HLIB_H
#define _GHCORE_HLIB_H 1

/* some other includes that we haven't got already ? */
#include "../src/names.h"
#include "../src/finit.h"

/* User inspectable data structures */
/* these must be defined before use in prototypes in Solaris */

typedef struct {
	int rank;			/* what it thinks its ranking is */
	int	core_id;		/* its unique ID */
	char vmname[256];	/* just in case you forgot */
	char hostname[256];	/* maybe locals or FQDN */
	long addr;			/* address (32bit) IPV4 */
	int port;			/* contact port */
	int verified;		/* have we contacted it yet? */
	int status;			/* status of the core */
	double rtt;			/* our last round-trip time (msecs) */
	int harch_type;		/* harness arch type */
	int hcore_ver;		/* version of the hcore running */
	/* usage info for this core */
	int npackages;		/* number of packages loaded */
	int nthreads;		/* number of threads in use */
	int nthreadsfree;	/* number of threads free */
	int nservices;		/* how many services defined and running */
	/* system perfomance and scheduling info */
	int cpus;			/* how many cpus */
	float cpuloads[3];	/* current, 5 and 15 minute load (os depend) */
	float maxflops;		/* maximum flop rating per cpu */
	float maxmips;		/* maximum mip rating  per cpu*/
	} coreinfo_t;

/* User callable functions */

int hlib_init (void);
int hlib_end (void);

int   hlib_find_cores (char *vmname,int verify,int maxresults,int *totalcores,
					coreinfo_t * results);
int   hlib_core_ping (coreinfo_t *core_ptr,int timeit, int getinfo);
void  hlib_print_core_info (coreinfo_t *core_ptr,int count);

int hlib_load_package (coreinfo_t *core_idp,char * pname,char * cname,
						int * id,int * nf);
int hlib_get_function (coreinfo_t *core_idp,char * fname,int package,
						int * fid);
int hlib_call_function (coreinfo_t * core_idp,int fid,int * s);

int hlib_get_function_multiple (void);
int hlib_call_function_multiple (void);

/* global defines */

#define COREINFO_UNKNOWN	0
#define COREINFO_OK	1
#define COREINFO_FAILED	-1
#define COREINFO_STALEINFO	-2

#define COREINFO_RTT_UNKNOWN	99999.9


/* these are used as the getinfo flags (or'd together) */
#define COREINFO_PING_ONLY	0			/* ping fast only */
#define COREINFO_PING_GET_STATS	1		/* get packages loads etc */
#define COREINFO_PING_GET_NAMES	2		/* get hostname, dvm name */


#endif /* _GHCORE_HLIB_H */
