
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#ifndef _FT_MPI_H
#define _FT_MPI_H

/* Keep C++ compilers from getting confused */
#if defined(__cplusplus)
extern "C" {
#endif


/* 
 MPI Version and implementation info
*/

/*
 Most items are now defined in the order they appear in the original 
 MPI Spec 1.1
 Each section has the original Annex title unless it is from a different spec 
 Extra section are marked clearly

 This includes Prototype definitions
 */

#define MPI_VERSION	1
#define MPI_SUBVERSION	2

/* Major.minor.patch.release_status */
#define FTMPI_VERSION 	"0.90.4.b"	


/* A.2 Defined Constants for C and Fortran */

/* A.2 return codes */

#define MPI_SUCCESS 0

/* NORMAL MPI ERROR CODES */
#define MPI_ERR_ARG -10
#define MPI_ERR_BUFFER -11
#define MPI_ERR_COMM -12
#define MPI_ERR_COUNT -13
#define MPI_ERR_DIMS -14
#define MPI_ERR_GROUP -15
#define MPI_ERR_IN_STATUS -16
#define MPI_ERR_INTERN -17
#define MPI_ERR_OP -18
#define MPI_ERR_OTHER -19
#define MPI_ERR_PENDING -20
#define MPI_ERR_RANK -21
#define MPI_ERR_REQUEST -22
#define MPI_ERR_ROOT -23
#define MPI_ERR_TAG -24
#define MPI_ERR_TOPOLOGY -25
#define MPI_ERR_TRUNCATE -26
#define MPI_ERR_TYPE -27
#define MPI_ERR_UNKNOWN -28
#define MPI_ERR_KEYVAL  -29
#define MPI_ERR_FILE    -30
#define MPI_ERR_IO      -31
#define MPI_ERR_UNSUPPORTED_OPERATION -32
#define MPI_ERR_AMODE -33
#define MPI_ERR_LASTCODE -34

/* *** The following are unique to FTMPI only *** */

/* This error is returned for each failed operation */
#define MPI_ERR_NODE_FAILED -35

/* This error is returned when in CONT mode and your operation succeeded but */
/* some other process in this communicator has failed */
#define MPI_ERR_NODE_FAILED_COMM_OK -36

/* The MPI_ERR_NODE_FAILED_COMM_OK can be replaced by MPI_ERR_NODE_FAILED */
/* but with the status varibles set to possitive values. */
/* although this only helps with RECV calls */

#define MPI_INIT_PATHALOGICAL  -37
#define MPI_ERR_something -38
#define MPI_ERR_ABORT_CALLED -39
#define FTMPI_ERR_LASTCODE -40

/* *** END of FT-MPI specific section *** */

/* END A.2 return codes */


/* A.2 assorted constants */

#define MPI_BOTTOM (void*)0		/* ?? */
#define MPI_PROC_NULL	-31
#define MPI_ANY_SOURCE -1 
#define MPI_ANY_TAG -2
#define MPI_UNDEFINED -99
#define MPI_BSEND_OVERHEAD 0
#define MPI_KEYVAL_INVALID -1	/* shouldnot be 0 */

#define MPI_FILE_NULL    -33

/* NOTE */
/* MPI_PROC_NULL AND ANY_SOURCE MUST BE DIFFERENT ! */

#define MPI_STATUS_IGNORE   (MPI_Status*)1
#define MPI_STATUSES_IGNORE (MPI_Status*)1

/* END A.2 assorted constants */


/* A.2 Error-handling specifiers */

#define MPI_ERRORS_ARE_FATAL 	300
#define MPI_ERRORS_RETURN	301

/* END A.2 Error-handling specifiers */


/* A.2 Maximum sizes for strings */

#define MPI_MAX_ERROR_STRING	1024
#define MPI_MAX_PROCESSOR_NAME 255		/* follows UNIX hostname conv */

/* END A.2 Maximum sizes for strings */


/* A.2 elementary datatypes (C) */

#define MPI_CHAR           1
#define MPI_UNSIGNED_CHAR  2
#define MPI_BYTE           3
#define MPI_SHORT          4
#define MPI_UNSIGNED_SHORT 5
#define MPI_INT            6
#define MPI_UNSIGNED       7
#define MPI_LONG           8
#define MPI_UNSIGNED_LONG  9
#define MPI_FLOAT          10
#define MPI_DOUBLE         11
#define MPI_LONG_DOUBLE    12
#define MPI_PACKED         13
#define MPI_LONG_LONG      20
#define MPI_LONG_LONG_INT  20  /* WE ARE MISSING MPI_LONG_LONG FOR THIS */


/* END A.2  elementary datatypes (C) */


/* A.2 elementary datatypes (FORTRAN) */

#define MPI_INTEGER           14
#define MPI_REAL              15
#define MPI_DOUBLE_PRECISION  16
#define MPI_LOGICAL           17

#define MPI_COMPLEX           509
#define MPI_DOUBLE_COMPLEX    510


#define MPI_CHARACTER	   1

/* END A.2  elementary datatypes (FORTRAN) */


/* A.2 datatypes for reduction functions (C) */

#define MPI_FLOAT_INT         500
#define MPI_DOUBLE_INT        501
#define MPI_LONG_INT          502
#define MPI_2INT              503
#define MPI_SHORT_INT         504
#define MPI_LONG_DOUBLE_INT   505


/* END A.2 datatypes for reduction functions (C) */


/* A.2 datatypes for reduction functions (FORTRAN) */

#define MPI_2REAL             506
#define MPI_2DOUBLE_PRECISION 507
#define MPI_2INTEGER          508

/* END A.2  datatypes for reduction functions (FORTRAN) */


/* A.2 optional datatypes (FORTRAN) */
/* they are in the mpif.h file :) */
/* END A.2 optional datatypes (FORTRAN) */


/* A.2 optional datatypes (C) */


/* END A.2 optional datatypes (C) */


/* A.2 special datatypes for constructing derived datatypes */

#define MPI_UB             18
#define MPI_LB             19

/* END A.2 special datatypes for constructing derived datatypes */


/* *** additional optional datatypes *** */

#define MPI_2COMPLEX          511
#define MPI_2DOUBLE_COMPLEX   512

/* END *** additional optional datatypes *** */


/************************/
/* COMBINER DEFINITIONS */
/************************/

#define MPI_COMBINER_NAMED             61
#define MPI_COMBINER_DUP               62
#define MPI_COMBINER_CONTIGUOUS        63
#define MPI_COMBINER_VECTOR            64
#define MPI_COMBINER_HVECTOR_INTEGER   65
#define MPI_COMBINER_HVECTOR           66
#define MPI_COMBINER_INDEXED           67
#define MPI_COMBINER_HINDEXED_INTEGER  68
#define MPI_COMBINER_HINDEXED          69
#define MPI_COMBINER_INDEXED_BLOCK     70
#define MPI_COMBINER_STRUCT_INTEGER    71
#define MPI_COMBINER_STRUCT            72
#define MPI_COMBINER_SUBARRAY          73
#define MPI_COMBINER_DARRAY            74
#define MPI_COMBINER_F90_REAL          75
#define MPI_COMBINER_F90_COMPLEX       76
#define MPI_COMBINER_F90_INTEGER       77
#define MPI_COMBINER_RESIZED           78

/************************/
/* COMBINER DEFINITIONS */
/************************/














/* A.2 reserved communicators (C & FORTRAN) */

#define MPI_COMM_WORLD 0
#define MPI_COMM_SELF 1

/* END A.2  reserved communicators (C & FORTRAN) */


/* A.2 results of communicator and group comparisons */

#define MPI_IDENT	100
#define MPI_CONGRUENT	101
#define	MPI_SIMILAR	102
#define MPI_UNEQUAL	103

/* END A.2 results of communicator and group comparisons */


/* A.2 environmental inquiry keys (C & FORTRAN) */

#define MPI_TAG_UB	0x8000
#define MPI_IO		0x8001
#define MPI_HOST	0x8002
#define MPI_WTIME_IS_GLOBAL	0x8003
/* new in MPI-2 */
#define MPI_LASTUSEDCODE 0x8004

/* new in FT-MPI */
#define FTMPI_ERROR_FAILURE 0x8005
#define FTMPI_NUM_FAILED_PROCS    0x8006

/* END A.2 environmental inquiry keys (C & FORTRAN) */


/* A.2 collective operations (C & FORTRAN) */

#define MPI_MAX 0
#define MPI_MIN 1
#define MPI_SUM 2
#define MPI_PROD 3
#define MPI_LAND 4
#define MPI_BAND 5
#define MPI_LOR 6
#define MPI_BOR 7
#define MPI_LXOR 8
#define MPI_BXOR 9
#define MPI_MAXLOC 10
#define MPI_MINLOC 11

/* END A.2 collective operations (C & FORTRAN) */


/* A.2 Null handles */

/* negative so that a random '0' will still be caught */
#define MPI_GROUP_NULL -2
#define MPI_COMM_NULL -3
#define MPI_DATATYPE_NULL -12
#define MPI_REQUEST_NULL -4
#define MPI_OP_NULL	-5
#define MPI_ERRHANDLER_NULL -6	

/* END A.2 Null handles */


/* A.2 Empty group */

#define MPI_GROUP_EMPTY -1

/* END A.2 Empty group */


/* A.2 topologies (C & FORTRAN) */

#define MPI_GRAPH	100
#define MPI_CART	200

/* END A.2 topologies (C & FORTRAN) */


/* A.2 Opaque types (C) */

typedef long MPI_Aint;

typedef struct {
  int MPI_SOURCE;
  int MPI_TAG;
  int MPI_ERROR;
  /* NOTE these should really move as they are not part of the normal USER API */ /* and are internal (diff between C & F) */
  int msglength;		/* message length in bytes */
  int dt;			/* data type received */
  int elements;		/* how many of them (decoded) */
  unsigned int flags;	/* flags to keep trace of the actual request status */
} MPI_Status;

/* END A.2 Opaque types (C) */


/* A.2 handles to assorted structures (C) */
typedef int MPI_Group;
typedef int MPI_Comm;
typedef int MPI_Datatype;
typedef int MPI_Request;
typedef int MPI_Op;

/* END A.2 handles to assorted structures (C) */


/* A.2 prototypes for user defined functions (C) */

typedef int  MPI_Copy_function (MPI_Comm oldcomm, int keyval, 
				void *extra_state,
				void *attribute_val_in, 
				void *attribute_val_out, int *flag);

typedef int  MPI_Delete_function (MPI_Comm comm, int keyval, 
				  void *attribute_val, void *extra_state);

#define MPI_NULL_COPY_FN   ftmpi_comm_null_copy_fn
#define MPI_DUP_FN         ftmpi_comm_dup_fn
#define MPI_NULL_DELETE_FN ftmpi_comm_null_delete_fn

typedef void (MPI_Handler_function)(MPI_Comm *, int *, ...);

typedef void (MPI_User_function)(void*,void*,int *,int *);

/* *** not listed in the spec section A.2 */
typedef int MPI_Errhandler;
/* *** */

/* END A.2 prototypes for user defined functions (C) */


/* *** MISC FT-MPI related stuff *** */

/* Return from an MPI_INIT */
/* Usually only MPI_SUCCESS or MPI_ERR_OTHER can be returned */
/* This code means that Init was successfull and that this is a RESTARTED node */

/* Note its positive to indicate its not normal BUT not an error! */
#define MPI_INIT_RESTARTED_NODE 20

/* temporary ones for Tone */
/* this one is not really allowed */
#define MPI_NOT_INITIALISED  		-100

/* if this value is set in the newcom of a com dup (and soon com_create) */
/* then the operation checks for a failure and does a full recovery */
/* orginal version did this always but is too slow for BLACS on large tests */
#define FT_MPI_CHECK_RECOVER 0xEFFF
#define MPI_ERR_UNSUPPORTED_DATAREP   	43

/* END *** MISC FT-MPI related stuff *** */


/* A.3 C binding for Point-to-point Communication */

int MPI_Send(void*, int, MPI_Datatype, int, int, MPI_Comm);
int MPI_Recv(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Status *);
int MPI_Get_count(MPI_Status *, MPI_Datatype, int *);
int MPI_Bsend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int MPI_Ssend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int MPI_Rsend(void*, int, MPI_Datatype, int, int, MPI_Comm);
int MPI_Buffer_attach( void*, int);
int MPI_Buffer_detach( void*, int*);
int MPI_Isend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Ibsend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Issend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Irsend(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Irecv(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Wait(MPI_Request *, MPI_Status *);
int MPI_Test(MPI_Request *, int *, MPI_Status *);
int MPI_Request_free(MPI_Request *);
int MPI_Request_get_status (MPI_Request request, int *flag, MPI_Status *status);
int MPI_Waitany(int, MPI_Request *, int *, MPI_Status *);
int MPI_Testany(int, MPI_Request *, int *, int *, MPI_Status *);
int MPI_Waitall(int, MPI_Request *, MPI_Status *);
int MPI_Testall(int, MPI_Request *, int *, MPI_Status *);
int MPI_Waitsome(int, MPI_Request *, int *, int *, MPI_Status *);
int MPI_Testsome(int, MPI_Request *, int *, int *, MPI_Status *);
int MPI_Iprobe(int, int, MPI_Comm, int *flag, MPI_Status *);
int MPI_Probe(int, int, MPI_Comm, MPI_Status *);
int MPI_Cancel(MPI_Request *);
int MPI_Test_cancelled(MPI_Status *, int *);
int MPI_Send_init(void*, int, MPI_Datatype, int, int, MPI_Comm, MPI_Request *);
int MPI_Bsend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int MPI_Ssend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int MPI_Rsend_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int MPI_Recv_init(void*, int, MPI_Datatype, int,int, MPI_Comm, MPI_Request *);
int MPI_Start(MPI_Request *);
int MPI_Startall(int, MPI_Request *);
int MPI_Sendrecv(void *, int, MPI_Datatype,int, int, void *, int, MPI_Datatype,
int, int, MPI_Comm, MPI_Status *);
int MPI_Sendrecv_replace(void*, int, MPI_Datatype, int, int, int, int,
MPI_Comm, MPI_Status *);
int MPI_Type_contiguous(int, MPI_Datatype, MPI_Datatype *);
int MPI_Type_vector(int, int, int, MPI_Datatype, MPI_Datatype *);
int MPI_Type_hvector(int, int, MPI_Aint, MPI_Datatype, MPI_Datatype *);
int MPI_Type_indexed(int, int *, int *, MPI_Datatype, MPI_Datatype *);
int MPI_Type_hindexed(int, int *, MPI_Aint *, MPI_Datatype, MPI_Datatype *);
int MPI_Type_struct(int, int *, MPI_Aint *, MPI_Datatype *, MPI_Datatype *);
int MPI_Type_get_envelope( MPI_Datatype ddt, int * num_int, int * num_addr,
			   int * num_ddt,int * combiner);
int MPI_Type_get_contents( MPI_Datatype ddt, int max_int, int max_addr, 
			   int max_ddt, int * array_int, MPI_Aint * array_addr,
			   MPI_Datatype * array_ddt );
int MPI_Type_create_darray( int size, int rank, int ndims,
			    int *array_of_gsizes, int *array_of_distribs,
			    int *array_of_dargs, int *array_of_psizes,
			    int order, MPI_Datatype oldtype,
			    MPI_Datatype *newtype);
int MPI_Type_create_subarray( int ndims,int *array_of_sizes,
			      int *array_of_subsizes,int *array_of_starts,
			      int order,MPI_Datatype oldtype,
			      MPI_Datatype *newtype);
int MPI_Address(void*, MPI_Aint *);
int MPI_Type_extent(MPI_Datatype, MPI_Aint *);
int MPI_Type_size(MPI_Datatype, int *);
int MPI_Type_lb(MPI_Datatype, MPI_Aint*);
int MPI_Type_ub(MPI_Datatype, MPI_Aint*);
int MPI_Type_commit(MPI_Datatype *);
int MPI_Type_free(MPI_Datatype *);
int MPI_Get_elements(MPI_Status *, MPI_Datatype, int *);
int MPI_Pack(void*, int, MPI_Datatype, void *, int, int *,  MPI_Comm);
int MPI_Unpack(void*, int, int *, void *, int, MPI_Datatype, MPI_Comm);
int MPI_Pack_size(int, MPI_Datatype, MPI_Comm, int *);

/* END A.3 C binding for Point-to-point Communication */

/* type count? */

/* A.3 C binding for Collective Communication */

int MPI_Barrier(MPI_Comm );
int MPI_Bcast(void*, int, MPI_Datatype, int, MPI_Comm );
int MPI_Gather(void* , int, MPI_Datatype, void*, int, MPI_Datatype, int,
MPI_Comm);
int MPI_Gatherv(void* , int, MPI_Datatype, void*, int *, int *, MPI_Datatype,
int, MPI_Comm);
int MPI_Scatter(void* , int, MPI_Datatype, void*, int, MPI_Datatype, int,
MPI_Comm);
int MPI_Scatterv(void* , int *, int *,  MPI_Datatype, void*, int, MPI_Datatype,
int, MPI_Comm);
int MPI_Allgather(void* , int, MPI_Datatype, void*, int, MPI_Datatype,
MPI_Comm);
int MPI_Allgatherv(void* , int, MPI_Datatype, void*, int *, int *,
MPI_Datatype, MPI_Comm);
int MPI_Alltoall(void* , int, MPI_Datatype, void*, int, MPI_Datatype,
MPI_Comm);
int MPI_Alltoallv(void* , int *, int *, MPI_Datatype, void*, int *, int *,
MPI_Datatype, MPI_Comm);
int MPI_Reduce(void* , void*, int, MPI_Datatype, MPI_Op, int, MPI_Comm);
int MPI_Op_create(MPI_User_function *, int, MPI_Op *);
int MPI_Op_free( MPI_Op *);
int MPI_Allreduce(void* , void*, int, MPI_Datatype, MPI_Op, MPI_Comm);
int MPI_Allreduce_a(void* , void*,void *, int, MPI_Datatype, MPI_Op, MPI_Comm);
int MPI_Reduce_scatter(void* , void*, int *, MPI_Datatype, MPI_Op, MPI_Comm);
int MPI_Scan(void* , void*, int, MPI_Datatype, MPI_Op, MPI_Comm );

/* END A.3 C binding for Collective Communication */


/* A.3 C binding for Groups, Contexts and Communicators */

int MPI_Group_size(MPI_Group group, int *);
int MPI_Group_rank(MPI_Group group, int *);
int MPI_Group_translate_ranks (MPI_Group, int, int *, MPI_Group, int *);
int MPI_Group_compare(MPI_Group, MPI_Group, int *);
int MPI_Comm_group(MPI_Comm, MPI_Group *);
int MPI_Group_union(MPI_Group, MPI_Group, MPI_Group *);
int MPI_Group_intersection(MPI_Group, MPI_Group, MPI_Group *);
int MPI_Group_difference(MPI_Group, MPI_Group, MPI_Group *);
int MPI_Group_incl(MPI_Group group, int, int *, MPI_Group *);
int MPI_Group_excl(MPI_Group group, int, int *, MPI_Group *);
int MPI_Group_range_incl(MPI_Group group, int, int [][3], MPI_Group *);
int MPI_Group_range_excl(MPI_Group group, int, int [][3], MPI_Group *);
int MPI_Group_free(MPI_Group *);

int MPI_Comm_size(MPI_Comm, int *);
int MPI_Comm_rank(MPI_Comm, int *);
int MPI_Comm_compare(MPI_Comm, MPI_Comm, int *);
int MPI_Comm_dup(MPI_Comm, MPI_Comm *);
int MPI_Comm_create(MPI_Comm, MPI_Group, MPI_Comm *);
int MPI_Comm_split(MPI_Comm, int, int, MPI_Comm *);
int MPI_Comm_free(MPI_Comm *);
int MPI_Comm_test_inter(MPI_Comm, int *);
int MPI_Comm_remote_size(MPI_Comm, int *);
int MPI_Comm_remote_group(MPI_Comm, MPI_Group *);

int MPI_Intercomm_create(MPI_Comm, int, MPI_Comm, int, int, MPI_Comm * );
int MPI_Intercomm_merge(MPI_Comm, int, MPI_Comm *);

int MPI_Keyval_create(MPI_Copy_function *, MPI_Delete_function *, int *,
void*);
int MPI_Keyval_free(int *);

int MPI_Attr_put(MPI_Comm, int, void*);
int MPI_Attr_get(MPI_Comm, int, void *, int *);
int MPI_Attr_delete(MPI_Comm, int);

/* END A.3 C binding for Groups, Contexts and Communicators */


/* A.3 C binding for Process Topologies */

int MPI_Cart_create(MPI_Comm, int, int *, int *, int, MPI_Comm *);
int MPI_Dims_create(int, int, int *);
int MPI_Graph_create(MPI_Comm, int, int *, int *, int, MPI_Comm *);
int MPI_Topo_test(MPI_Comm, int *);
int MPI_Graphdims_get(MPI_Comm, int *, int *);
int MPI_Graph_get(MPI_Comm, int, int, int *, int *);
int MPI_Cartdim_get(MPI_Comm, int *);
int MPI_Cart_get(MPI_Comm, int, int *, int *, int *);
int MPI_Cart_rank(MPI_Comm, int *, int *);
int MPI_Cart_coords(MPI_Comm, int, int, int *);
int MPI_Graph_neighbors_count(MPI_Comm, int, int *);
int MPI_Graph_neighbors(MPI_Comm, int, int, int *);
int MPI_Cart_shift(MPI_Comm, int, int, int *, int *);
int MPI_Cart_sub(MPI_Comm, int *, MPI_Comm *);
int MPI_Cart_map(MPI_Comm, int, int *, int *, int *);
int MPI_Graph_map(MPI_Comm, int, int *, int *, int *);


/* END A.3 C binding for Process Topologies */


/* A.3 C binding for Environmental Inquiry */

int MPI_Get_processor_name(char *, int *);
int MPI_Errhandler_create(MPI_Handler_function *, MPI_Errhandler *);
int MPI_Errhandler_set(MPI_Comm, MPI_Errhandler);
int MPI_Errhandler_get(MPI_Comm, MPI_Errhandler *);
int MPI_Errhandler_free(MPI_Errhandler *);
int MPI_Error_string(int, char *, int *);
int MPI_Error_class(int, int *);
int MPI_Get_version(int *version, int *subversion );

double MPI_Wtime(void);
double MPI_Wtick(void);

int MPI_Init(int *, char ***);
int MPI_Finalize(void);
int MPI_Initialized(int *);
int MPI_Abort(MPI_Comm, int);
int MPI_Pcontrol (int level, ...);

/* END A.3 C binding for Environmental Inquiry */

/* ***
int MPI_Pack(void * ,int , MPI_Datatype ,void * ,int ,int * ,MPI_Comm );
int MPI_Unpack(void * ,int ,int * ,void * ,int ,MPI_Datatype ,MPI_Comm );

*** */

/* *** keep these incase int verse MPI_Datatype causes prototype errors 
int  MPI_Type_commit(int*);
int  MPI_Type_contiguous(int,int,int *);
int  MPI_Type_extent(int,int *);
int  MPI_Type_free(int*);
int  MPI_Type_hindexed(int,int *,int *,int,int *);
int  MPI_Type_hvector(int,int,int,int,int *);
int  MPI_Type_indexed(int,int *,int *,int,int *);
int  MPI_Type_size(int,int *);
int  MPI_Type_struct(int,int *,int *,int *,int *);
int  MPI_Type_vector(int,int,int,int,int *);

*** */


/* should be in section A.2 */
#define MPI_IN_PLACE (void *)-3

typedef int MPI_Fint;



#define MPI_ORDER_C              56
#define MPI_ORDER_FORTRAN        57
#define MPI_DISTRIBUTE_BLOCK    121
#define MPI_DISTRIBUTE_CYCLIC   122
#define MPI_DISTRIBUTE_NONE     123
#define MPI_DISTRIBUTE_DFLT_DARG -49767


/* Some MPI-2 prototypes */

typedef int  MPI_Comm_copy_attr_function (MPI_Comm comm, 
					  int keyval, void *extra_state,
					  void *attribute_val_in, 
					  void *attribute_val_out, 
					  int *flag);

typedef int  MPI_Comm_delete_attr_function (MPI_Comm comm, int keyval, 
					    void *attribute_val, 
					    void *extra_state);

#define MPI_COMM_NULL_COPY_FN   ftmpi_comm_null_copy_fn
#define MPI_COMM_DUP_FN         ftmpi_comm_dup_fn
#define MPI_COMM_NULL_DELETE_FN ftmpi_comm_null_delete_fn

typedef int  MPI_Type_copy_attr_function (MPI_Datatype type, 
					  int keyval, 
					  void *extra_state,
					  void *attribute_val_in, 
					  void *attribute_val_out, 
					  int *flag);

typedef int  MPI_Type_delete_attr_function (MPI_Datatype type, int keyval, 
					    void *attribute_val, 
					    void *extra_state);

#define MPI_TYPE_NULL_COPY_FN   ftmpi_type_null_copy_fn
#define MPI_TYPE_DUP_FN         ftmpi_type_dup_fn
#define MPI_TYPE_NULL_DELETE_FN ftmpi_type_null_delete_fn

/* just for the current purpose */
typedef int MPI_Win;

typedef int  MPI_Win_copy_attr_function (MPI_Win win, int keyval, 
					 void *extra_state,
					 void *attribute_val_in, 
					 void *attribute_val_out, 
					 int *flag);

typedef int  MPI_Win_delete_attr_function (MPI_Win win, int keyval, 
					   void *attribute_val, 
					   void *extra_state);


#define MPI_WIN_NULL_COPY_FN   ftmpi_win_null_copy_fn
#define MPI_WIN_DUP_FN         ftmpi_win_dup_fn
#define MPI_WIN_NULL_DELETE_FN ftmpi_win_null_delete_fn

/* These functions prototypes have to be visible in the user-code, since they
   are accessed through MPI_NULL_COPY_FN etc, which are define statements to
   these routines. Therefore, they have to be in mpi.h and can't be in an
   internal routine */

int ftmpi_comm_null_copy_fn (MPI_Comm c, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_comm_dup_fn (MPI_Comm c, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_comm_null_delete_fn (MPI_Comm c, int k, void* attr_val, void *exs);
int ftmpi_type_null_copy_fn (MPI_Datatype t, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_type_dup_fn (MPI_Datatype t, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_type_delete_fn (MPI_Datatype t, int k, void* attr_val, void *exs);
int ftmpi_win_null_copy_fn (MPI_Win w, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_win_dup_fn (MPI_Win w, int k, void *exs, void *ain, void *aout, int *f);
int ftmpi_win_delete_fn (MPI_Win w, int k, void* attr_val, void *exs);


/* just for now */
#ifdef DECLARE_MPIIO

#define MPI_INFO_NULL    -32
#define MPI_MAX_INFO_VAL 128
#define MPI_MAX_INFO_KEY 128

typedef int MPI_File;
typedef int MPI_Info;
typedef void (MPI_File_errhandler_fn)(MPI_File *, int *, ...);
MPI_File MPI_File_f2c ( MPI_Fint ffile );
MPI_Fint MPI_File_c2f ( MPI_Fint file );
MPI_Info MPI_Info_f2c ( MPI_Fint finfo );
MPI_Fint MPI_Info_c2f ( MPI_Info info );

#endif  /* DECLARE_MPIIO */


typedef void (MPI_Comm_errhandler_fn)(MPI_Comm *, int *, ...);
typedef void (MPI_Win_errhandler_fn) (MPI_Win *, int *, ...);

int MPI_Comm_create_keyval ( MPI_Comm_copy_attr_function *copy_fn,
			     MPI_Comm_delete_attr_function *del_fn,
			     int *keyval, void *extra_state );
int MPI_Comm_free_keyval ( int *keyval );
int MPI_Comm_set_attr ( MPI_Comm comm, int keyval, void *attribute_val );
int MPI_Comm_get_attr ( MPI_Comm comm, int keyval, void *attribute_val, 
			int *flag);
int MPI_Comm_delete_attr ( MPI_Comm comm, int keyval );
int MPI_Comm_create_errhandler ( MPI_Comm_errhandler_fn *fn, MPI_Errhandler *errhandler );
int MPI_Comm_set_errhandler    ( MPI_Comm comm, MPI_Errhandler errhandler );
int MPI_Comm_get_errhandler    ( MPI_Comm comm, MPI_Errhandler *errhandler );
int MPI_Add_error_class  ( int *errorclass );
int MPI_Add_error_code   ( int errorclass, int *errorcode );
int MPI_Add_error_string ( int errorcode, char *string );
int MPI_Comm_call_errhandler ( MPI_Comm comm, int errorcode );
MPI_Datatype MPI_Type_f2c ( MPI_Fint ftype );
MPI_Fint MPI_Type_c2f ( MPI_Datatype dtype );
MPI_Comm MPI_Comm_f2c ( MPI_Fint fcomm );
MPI_Fint MPI_Comm_c2f ( MPI_Comm comm );
MPI_Group MPI_Group_f2c ( MPI_Fint fgroup);
MPI_Fint MPI_Group_c2f ( MPI_Group group );
MPI_Op MPI_Op_f2c ( MPI_Fint fop);
MPI_Fint MPI_Op_c2f ( MPI_Op op);
MPI_Request MPI_Request_f2c ( MPI_Fint freq );
MPI_Fint  MPI_Request_c2f ( MPI_Request req );
MPI_Win MPI_Win_f2c ( MPI_Fint fwin );
MPI_Fint MPI_Win_c2f ( MPI_Win win );


/* Now to make sure the profiling interface works right */
#include "pmpi.h"
/* Hope that works :) GEF UTK Jan03 */

/* Keep C++ compilers from getting confused */
#if defined(__cplusplus)
}
#endif


#endif /* _FT_MPI_H */

