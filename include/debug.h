#ifndef DEBUG_H_HAS_BEEN_INCLUDED
#define DEBUG_H_HAS_BEEN_INCLUDED

/* AIX requires this to be the first thing in the file.  */
#ifdef HAVE_ALLOCA
#  ifndef __GNUC__
#    ifdef _MSC_VER
#      include <malloc.h>
#      define alloca _alloca
#    else
#      ifdef HAVE_ALLOCA_H
#        include <alloca.h>
#      else
#        ifdef _AIX
#pragma alloca
#        else
#          ifndef alloca /* predefined by HP cc +Olibcalls */
char *alloca ();
#          endif
#        endif  /* _AIX */
#      endif  /* HAVE_ALLOCA_H */
#    endif  /* _MSC_VER */
#  endif  /* __GNUC__ */
#endif  /* HAVE_ALLOCA */

#if !defined(NDEBUG)
#if !defined(__FILE__)
#define __FILE__ "unsupported"
#endif  /* __FILE */
#if !defined(__LINE__)
#define __LINE__ -1
#endif  /* __LINE__ */
extern void* ftmpi_malloc( size_t, char*, int );
extern void* ftmpi_calloc( size_t, size_t, char*, int );
extern void* ftmpi_realloc( void*, size_t, char*, int );
extern void  ftmpi_free( void*, char*, int );

#define _MALLOC(size)           ftmpi_malloc( (size), __FILE__, __LINE__ )
#define _CALLOC(nb, size)       ftmpi_calloc( (nb), (size), __FILE__, __LINE__ )
#define _REALLOC(ptr, size)     ftmpi_realloc( (ptr), (size), __FILE__, __LINE__ )
#define _FREE(ptr)              ftmpi_free( (ptr), __FILE__, __LINE__ )
#define DUMP_ALLOCATED_MEMORY() ftmpi_display_memory_usage()
#else  /* !defined(NDEBUG) */
#include <stdlib.h>
#define _MALLOC(size)            malloc((size))
#define _CALLOC(nb, size)        calloc((nb), (size))
#define _REALLOC(ptr, size)      realloc( (ptr), (size) )
#define _FREE(ptr)               free((ptr))
#define DUMP_ALLOCATED_MEMORY()
#endif  /* NDEBUG */

#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
extern int errno;
#define DUMP_SOCKET_ERROR(SOCK, ERRNO) \
do { \
  struct sockaddr_in sockname; \
  unsigned int length = sizeof(struct sockaddr); \
 \
  if( getpeername( (SOCK), (struct sockaddr*)&sockname, &length ) == 0 ) { \
    printf( "%s:%d sock %d [%s:%d] generate error %d %s\n", \
            __FILE__, __LINE__, \
            (SOCK), inet_ntoa(sockname.sin_addr), sockname.sin_port, \
	    (ERRNO), strerror((ERRNO)) ); \
  } else { \
    printf( "%s:%d unable to get the peer name on socket %d. error %d %s\n", \
            __FILE__, __LINE__, (SOCK), errno, strerror(errno) ); \
  } \
} while (0)

#define DUMP_SOCKET_INFO(SOCK, SSTR, ESTR) \
do { \
  struct sockaddr_in sockname; \
  unsigned int length = sizeof(struct sockaddr); \
 \
  if( getpeername( (SOCK), (struct sockaddr*)&sockname, &length ) == 0 ) { \
    printf( "%s:%d %s sock %d [%s:%d] %s", __FILE__, __LINE__, (SSTR), \
            (SOCK), inet_ntoa(sockname.sin_addr), sockname.sin_port, (ESTR) ); \
  } else { \
    printf( "%s:%d unable to get the peer name on socket %d. error %d %s\n", \
            __FILE__, __LINE__, (SOCK), errno, strerror(errno) ); \
  } \
} while (0)

#endif  /* DEBUG_H_HAS_BEEN_INCLUDED */
