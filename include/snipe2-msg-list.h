
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	Originally part of PVMPI/MPI_Connect by Graham 
	Some linked list code borrowed from PVM 3.3
*/


/* needed for lowlevel msg headers? */
/* #include "ft-mpi-conn.h"	*/


#ifndef SNIPEMSGLISTH
#define SNIPEMSGLISTH

/*
 *  * call back function pointer 
 *   */
typedef int (*intfuncptr)();


/* Message list, used by receive handler routines. */
typedef struct snipe2_msg_list {
  struct snipe2_msg_list *sml_link, *sml_rlink;  /* linked list stuff */

  /* our low level coms info */
  int sml_msg_reqid;              /* message request number */
  int sml_msg_id;               /* message id number */
  int sml_msg_chan;					/* which channel are we posted on? */
  int sml_msg_type;					/* what type of op is it? 1=recv 2=send */
  short sml_msg_priority;       /* message priority */
  short sml_msg_done;			/* low level done flag */			

  /* message information */
  char *sml_msg_buf;		/* pointer to the actual data */
  long sml_msg_length;		/* in bytes raw */

  /* call back function to call on completion */
  intfuncptr sml_callbackfunction;	
  long		sml_uservalue;	/* user defined value to return */

  /* auto deletion after completion flag */
  /* we only delete if the request completed WITHOUT errors */
  /* after a callback if any */
  int		sml_autodelete;
} snipe2_msg_list_t;


struct snipe2_msg_list* snipe2_msg_list_init(); /* called only once */
void snipe2_msg_list_init_freelist(int initelements, int maxelements);

void                    snipe2_msg_list_free(); /* unlink entry and free */
void                    snipe2_msg_list_unlink(); /* unlink entry from list */
void                    snipe2_msg_list_destroy(struct snipe2_msg_list*); /* destroys list */
struct snipe2_msg_list* snipe2_msg_list_head(struct snipe2_msg_list*);  
struct snipe2_msg_list* snipe2_msg_list_tail(struct snipe2_msg_list*); 

void snipe2_msg_list_add_to_head (struct snipe2_msg_list*,struct snipe2_msg_list*);
void snipe2_msg_list_add_to_tail (struct snipe2_msg_list*,struct snipe2_msg_list*);


struct snipe2_msg_list* snipe2_msg_list_movehead_to_tail (struct snipe2_msg_list*frommlp, struct snipe2_msg_list* tomlp);

void                    snipe2_msg_list_dump(struct snipe2_msg_list*); /* dumps contents of list */
struct snipe2_msg_list* snipe2_msg_list_new(struct snipe2_msg_list*);  /* makes new and adds to tail */
struct snipe2_msg_list* snipe2_msg_list_find_by_request(struct snipe2_msg_list*, int); /* search by req */ 
struct snipe2_msg_list* snipe2_msg_list_find_by_msg_id(struct snipe2_msg_list*, int); /* search by msg_id */
struct snipe2_msg_list* snipe2_msg_list_find_by_priority(struct snipe2_msg_list*, int); /* search by priority */

#endif /* SNIPEMSGLISTH */


