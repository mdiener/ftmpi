

/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

	The Name Service code is derived from the Name Service
	Code that was developed for the benchmark run of linpeak(tm)
	on the ASCI Pacific Blue machine using MPI_Connect.

	The MPI_Connect project was supported in part by grants from 
	the DoD under the DOD MOD office PET program.

*/


/* 
	Name service info and data structures 

*/

/* Port ranges and time outs etc etc */
#define NS_PORT_DEFAULT 1812 /* honest, its not against the french */
#define NS_PORT_RANGE 100
/* #define NS_POLLWAIT 10000 */
#define NS_POLLWAIT 0

/* This is how many attempts we make at findin the NS initially */
/* i.e we scan the whole port range doing this */
/* this helps avoid failues when the listen backlog of the NS is too short */
#define	NS_INIT_SEARCH_ATTEMPTS	2

/* ditto for the ns_open call, except here we back off NS_POLLWAIT uSec */
#define NS_OPEN_ATTEMPTS 10

/* maxgroups groups allowed to be connected to me */
#define ns_maxgroups 256
/* maxgroups MPI processes or hcores per group */
#define ns_maxprocs 256

/* ns_maxrecords is the number of different types of packed records allowed */
#define ns_maxrecords	128
/* maxattributes is the number of attributes under a single record type */
#define ns_maxentries 128

/* this structure is being taken out as it allowed me to make a mess with MPI collective operations */
/* as these are operated on contiguous addresses not ones that were separated by some random ammount */
/* when sending all addresses and then all port numbers */

/* struct member_entry_s { */
/*     int address;             */
	/* works for IP/addr and TID :) */
/*     int port; */
    /* int rank; */
        /* not really needed, but just for indexing */
        /* taken out until used */
/* }; */

struct group_entry_s {
    char groupname [256];
	int  gnamelen;
    int  num_members;
	int	 lowestslotfree;
/*     struct member_entry_s member[ns_maxprocs]; */
    int address [ns_maxprocs];
    int port [ns_maxprocs];
	int free [ns_maxprocs];	
    int nreaders;
    int nwriters;
    int gencount;
	/* added so we can use addr/port records for other things. values CANNOT be negative though*/
};

/* How big is the biggest single data entry allowed to be ? */
/* stops me filling up memory.. maybe */
#define NS_MAX_RECORD_SIZE		(16*1024)

struct callback_info {
    int flag;
    int addr;
    int port;
    int tag;
    int gencount;
    int slot;
};

struct record_entry_s {	/* this is used to store packed records */
	char recordname [256];		/* common name */
	int  rnamelen;				/* name len needed by disk IO routines */
	int num_entries;			/* how many active */
	int	 lowestslotfree;
	int	status[ns_maxentries];		/* status, 0=NOT IN USE */
	long  lengths[ns_maxentries];	/* how long each packed record */
	char* recordptr[ns_maxentries];	/* pointer to the actual data  */
										/* yes we do malloc/free */
	int owners[ns_maxentries];		/* who owns/created it */
	int flags[ns_maxentries];		/* access/perms flags */
    int nreaders;
    int nwriters;
    int gencount;
    
    int ncallback;
    int lowestcallback;
    struct callback_info callback[ns_maxprocs];
};


/* message formats to NS */

/*
4bytes = message type
4bytes = message length outstanding, i.e. 0 for EOM or 4 for an int etc.. 
<message dependent>...
*/

/*
Message types
0 PING/ECHO
-> 0, 0	Ping / Echo expects reply
<- 0, 0

1 JOIN/REGISTER
1, group_entry_s 
<- 0, 1, entry-point integer (4 bytes), location slot (4 bytes)

2 GET INFO/FIND OTHER GROUP
->2, name_len, name (CHAR)
<-2, size, group_entry_s x 1 with only the members_entries that are full

3 LEAVE
->3, name_len+4bytes, entry point, slot, name (CHAR)   (name used to verify)
<-3, 1, rc (rc = 0 for OK and -X for errors) 

*/

/* New message types based on packed message to solve byte ordering/protocol errs */

/* do an echo/ping test */
#define NS_PROTO_ECHO	88	

/* host/addr pair type operations */
#define NS_PROTO_JOIN	101		
#define NS_PROTO_INFO	102
#define NS_PROTO_RMLV	103
#define NS_PROTO_ISMEMBER	104	/* check an existence of member */

/* packed data type operations on the record database (RDB) */
#define NS_PROTO_RDB_PUT	201		
#define NS_PROTO_RDB_INFO	202		/* gets attribute/record info only */
#define NS_PROTO_RDB_GET	203
#define NS_PROTO_RDB_DELETE	204
#define NS_PROTO_RDB_SWAP	205		/* swaps based on certain info */
#define NS_PROTO_RDB_CALLBACK_REG	206	/* register callback */
#define NS_PROTO_RDB_CALLBACK_UNREG	207	/* un-register callback */
#define NS_PROTO_RDB_UPDATE_INFO	208	/* update information to callback */

/* management operations */
#define NS_PROTO_DUMP	301
#define NS_PROTO_DUMPALL	302

/* special call back routines */
#define NS_PROTO_WAIT	401

/* getid routine */
#define NS_PROTO_GID	411

/* close connection nicely (rather than just close connection) */
#define NS_PROTO_GBYE	909

/* request (yes ask) for the service to halt */
#define NS_PROTO_HALT	999

/* internal catch all and debug */
#define NS_PROTO_WHAT	-1


/* echo is non zero now as mismatched messages sometime come out as an zero... */

/* Below are default IDs for anon access where sender/recvr ID are not put in msg hdrs */
#define NS_ID_SERVER_ANON	1010
#define NS_ID_CLIENT_ANON	2020

/* return and error codes etc */
#define ns_ok			 0

#define ns_err_bad_arg		-1
#define ns_err_other		-2
#define ns_err_dup			-3
#define ns_err_no_space		-4
#define ns_err_none			-5
#define ns_err_denied		-6

#define ns_err_panic	-999

