
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

	The Name Service code is derived from the Name Service
	Code that was developed for the benchmark run of linpeak(tm)
	on the ASCI Pacific Blue machine using MPI_Connect.

	The MPI_Connect project was supported in part by grants from 
	the DoD under the DOD MOD office PET program.

*/

/*
	This is the third major version of this name service code
	The first was for MPI_Connect,
	the second was for Harness and used the ns_lib for the client
	this is the third.

	It used the msg/msgbuf routines to handle messages more like self contained packets rather
	than individual read/writes on streams. 
	It is easier to read and is not architecture byte order sensitive as the other versions.

	It also now supports more than just host:port address pairs and can now store 
	arbitary data upto NS_MAX_RECORD_SIZE in the form of a packed message.

	That means it can be used to implement a single point PVM style mbox.

	The message tags are now defined in name_service.h and have changed so that
	it cannot interoperate with v2, i.e. users will have to relink to the new version.

	GEF Stuttgart dec2001.

	Updated the disk IO routines

	GEF Jan 2002.

*/

/*
	30th dec 2001
	Added GID and dump to disk routine 
*/


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<signal.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>	/* need to swap bytes */
#include <arpa/inet.h>
#include <netdb.h>
#include"snipe_lite.h"
#include"name_service.h"
#include"msg.h"
#include"msgbuf.h"
#include"ns_lib.h"	/* so I can get the record flags */
#include"daemon.h"	/* so I can get the record flags */

#include "ns_id.h"	/* used for the IDs base values and ranges */

#ifdef DISKBACKUP
#include "mkpaths.h"
#endif

/* GLOBAL */
int debug;
int changestate;
int restart;
int daemon_mode;

/* END GLOBAL */

/* GLOBAL */
/* will alloc this later maybe */
/* struct group_entry_s grouptable[ns_maxgroups]; */

struct group_entry_s grouptable[ns_maxgroups];

struct record_entry_s recordtable[ns_maxrecords];

/* number of active groups already */
int ngroups;	

/* lowest group_entry_s available so we don't need to search always */
int lowest_group_entry_free=-1;

/* lowest record_entry_s available so we don't need to search always */
int lowest_record_entry_free=-1;

/* number of active records already */
int nrecords;	

/* a few more... */
int group_socket[ns_maxgroups];					/* socket used by each entry */
char group_wakeup_list[ns_maxgroups][256];	/* list of group names we wait on */
						/* to wait you must register yourself */

int	gidsnext[_NS_ID_END+1];	/* next gids to hand out */
int	basegids[_NS_ID_END+1];	/* start gids */
int	endgids[_NS_ID_END+1];	/* top of gids at which we roll over */

int c[ns_maxgroups];	/* connection info */

#ifdef DISKBACKUP
char fbname[256];	/* file system base name (dir name basically) */
char gindexname[512];	/* GROUP file name of the index file */
char gdataname[512];		/* start of the GROUP data file names */
char rindexname[512];	/* Record file name of the index file */
char rdataname[512];		/* start of the RECORD data file names */
char gidfname[512];		/* GID file */
FILE *fpgindex;
FILE *fprindex;
FILE *fpgid;	/* persistant file pointers */
#endif /*  DISKBACKUP */

/* END GLOBAL */

/* Functions that do IDs */
void init_gids ();
int next_gid (int index, int range, int socket);

/* Functions that work on the tables etc */
char *ns_get_hostname(int addr);
void init_tables (); 
void dump_tables ();
void dumpall_tables ();
void dump_group_entry (int e);
void dump_record_entry (int e);
void dumpall_group_entry (int e);
void dumpall_record_entry (int e);

/* groups (i.e. the addr:port kind of groups) */
int	group_join (int mbuf, int *outslot);
int	group_leave (int mbuf,int * outslot);
int	group_info (int mbuf,int s);
int	group_ismember (int mbuf);

/* records (i.e. packed message records) */
int	record_put (int mbuf,int  who,int * outslot);
int	record_info (int mbuf,int who,int s);
int	record_get (int mbuf,int who,int s);
int	record_swap (int mbuf,int who,int * outslot);
int     record_callback_register (int mbuf, int sockfd);
int     record_callback_unregister (int mbuf);
int     record_callback_clean(int entry, int slot);
int     record_delete (int mbuf);

/* call back functions */
/* int	 group_wait (); */


int main(int argc,char ** argv)
{
int ip, p;
int i;
int s;
int finished=0;
int pid=0;
int c_tmp=0;
int ncon=0;
int next=0;
/* int debug=0; */
int rc;	/* rc for pack/snd/recv calls */
int mb;	/* current message buffer */

daemon_mode=0;

for(i=0;i<ns_maxgroups;i++) c[i]=0;


if(argc>4) {
	fprintf(stderr,"Name Service can be started with a user given port addr and optionally a debug level from 0-9, i.e.\n%s port [debug] [RESTART]\n", argv[0]); 
	fflush(stderr);
	exit (-1);
}

ip = 0;	

if (argc <= 1 || (argc>1 && !strncmp(argv[1],"-d",2))) {
    ip = NS_PORT_DEFAULT;			/* if not default it and THEN check for ENV override */
    if (getenv("HARNESS_NS_HOST_PORT")) ip = atoi(getenv("HARNESS_NS_HOST_PORT"));
    if(argc>1) {
        if (!strncmp(argv[1],"-d",2)) {
             daemon_mode=1;  
             /**** IMPORTANT: Don't print/put anything to stdout/stderr while we are in daemon mode !!!! ***/
             /**** Because those file descriptor (1-stdout, 2-stderr) will interface with socket file descriptor ***/
             daemon_init();
        }
    }
} else {
    ip = atoi (argv[1]);	/* port on the command line ? */
}

if (ip<1024) { /* are we root yet? or just haven't set this port correctly */
	fprintf(stderr,"PORT number set to %d?\n", ip);
	fprintf(stderr,"Either set port on command line, via ENV VAR HARNESS_NS_HOST_PORT or compile time NS_PORT_DEFAULT.\n");
	exit (-2);
}

if (argc>2) debug = atoi (argv[2]);
else debug = 0;

if (argc>3) restart = atoi (argv[3]);
else restart = 0;

if(!daemon_mode) {
    printf("debug level set to [%d]\n", debug);
}
#ifdef DISKBACKUP
if (restart && (!daemon_mode) ) printf("Will attempt to load a restart from disk\n");
#else
if (restart && (!daemon_mode) ) printf("Requested RESTART, but was not compiled with -DDISKBACKUP option. Please recompile.\n");
#endif

if(!daemon_mode) {
   if (debug) printf("Group table = [%d]\n", sizeof(grouptable));
   if (debug) printf("Record table = [%d]\n", sizeof(recordtable));
}

p = setportconn(&s, ip, NS_PORT_RANGE);
if (s<0) { printf("Initial socket init failed.\n"); exit (s); }

signal(SIGPIPE,SIG_IGN); /* just in case */

/* passed so set up member entries */
init_tables();

init_gids(); /* start ID count */

i = init_msg_bufs ();
if (i>0) {
	if (debug && (!daemon_mode)) printf("init bufs ok with %d\n", i);
	}
else { 
	fprintf(stderr,"init bufs bad\n"); 
	exit (-2); 
	}


#ifdef DISKBACKUP
if (!restart) { /* prepare to store DB info on disk */
	init_files ();
	/* if we are not reading in old ones */
	init_index_files ();
	}

else { /* get data from DISK if it is there */
	err = restore_db_from_disk (restart);
	if (err) {
		fprintf(stderr,"Unable to restore name service DB from Disk.\n");
		fprintf(stderr,"Try again without the RESTART option set or check parameters DB directory\n");
		exit (-1);
		}
	else
           if(!daemon_mode) printf(stderr,"NS BD recovered from disk\n");

	} /* restarting from DISK */
#endif /* DISKBACKUP */

/* all passed to tell the world. */

if(!daemon_mode) {
   fprintf(stderr,"%s running on port [%d]\n", argv[0], p);
   if (ip!=p) fprintf(stderr, "Note:\nRequested port was %d, so there might be a name service already running or one in FINWAIT etc.\n", ip);
}

/* main loop */


finished = 0; /* go until we get a halt! */
while (!finished) {

/* allowing connections */
/* if re have connects, do it in a non block way */
if (ncon)
  c_tmp = probeconn (s, 0, &pid);
else {
  /* we have no current connects so block on a new connection */
  c_tmp = allowconn (s, 0, &pid);
}

/* probeconn returns a 0 on no message if it is acting on a socket
   that is performing a listen() */

if (c_tmp<0) { 
    if(!daemon_mode) {
        printf("Didnot allow connection on [%d]. Failed with [%d]\n", s, c_tmp); 
	perror ("conn:accept");
    }
}


if (c_tmp>0) {
	/* Ok connection was allowed */
	if (debug && (!daemon_mode)) printf("End-point established on socket [%d] for port [%d]\n", s, p);
	c[next] = c_tmp;	/* connection alive */
	next++;
	if (next==ns_maxgroups) next=0;	/* i.e. wrap around (more todo) */
	ncon++;			/* number connected increases */
}

/* puts("M");  */
if (ncon) { /* i.e. some sockets to poll() */

	for(i=0;i<ns_maxgroups;i++) {
		int len;
		int mtype;
/* 		int l1, l2, l3; */
/* 		int rc1, rc2, rc3; */
		int l[3];	/* l1, l2, l3 (message list replacements */
		int mrc[5];     /* message reply codes (replaces rc1, rc2, rc3) */
		int from;	/* who sent this to me */
		int ntag;	/* number of tags sent in message header */
		int j;	        /* general tmp vars */
		int base;	/* address of buffer base (debug info only) */
		int result;	/* some functions return a simple result to be sent to the client */


		c_tmp = 0;
		if (c[i]>0) c_tmp = pollconn(c[i],0,10); 
/* 		printf("Checking [%d] = [%d] which gives us [%d]\n", i, c[i], c_tmp);
*/
/* 		fflush(stdout); fflush(stderr); */
		if (c_tmp) { /* i.e. we have a message */

		/* we originally did two separate reads. Now we use msgbuf to do packed send/recvs */

/* 			l1 = readconn(c[i],&mtype, sizeof(mtype)); */
/* 			l2 = readconn(c[i],&mlen, sizeof(mlen)); */

		ntag = 3;					/* we only need the message type and a few other args */
		for(j=0;j<ntag;j++) l[j]=0; /* clear out our buffer */

		rc = recv_pkmesg (c[i], &from, &ntag, l, &mb); /* do the recv */

		if (debug && (!daemon_mode) ) {
   		     printf("rc=%d, from=%d, ntag=%d l0=%d l1=%d l2=%d buf_id = %d\n", 
			 			rc, from, ntag, l[0], l[1], l[2], mb);
		     j = get_msg_buf_info (mb, (void *)&base, &len);
			    if(!daemon_mode) printf("buf info returns %d base 0x%x of len %d\n", j, base, len);
			}	

		if (rc==MSG_BAD_HEADER && (!daemon_mode) ) {
			fprintf(stderr,"Ops bad message header!\nCould be an older(wrong) client library talking to talk to me. Update and try again.\n");
			}

		if (rc<0) mtype = -2;	/* drop to drop bad message at end of switch */

			/* extract message type from tag list */
			mtype = l[0];
			

		if (debug && (!daemon_mode) )
			printf("i=[%d] mtype=[%d]?\n", i, mtype);

			switch (mtype) {
				case NS_PROTO_ECHO:
					if (debug && (!daemon_mode) ) puts("Echo");

					upk_int32 (mb, &result, 1); /* message payload */

					if (debug && (!daemon_mode) ) printf("echo payload = %d 0x%x\n",
										result, result);

					/* finished with buffer so free it */
					free_msg_buf (mb);

					mrc[0] = NS_PROTO_ECHO;
					mrc[1] = result;		/* send payload back as a tag */
									/* that way they know its my ping :) */
					
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	

					if (debug && (!daemon_mode) )
						printf("Echo returned on connection [%d] socket [%d] rc's[%d]\n",
							i, c[i], mrc[0]);
					/* should we disconnect here? */
					/* c_tmp = -1; */
					/* not for now... */
					break;
				case NS_PROTO_JOIN:
					if (debug && (!daemon_mode) ) puts("Join");
					/* call function that takes appart the join request */
					mrc[0] = NS_PROTO_JOIN;
					mrc[1] = group_join (mb, &result);
					mrc[2] = result;
					/* finished with buffer so free it */
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
					break;

                                case NS_PROTO_ISMEMBER:
					if (debug && (!daemon_mode) ) puts("Is member");
					mrc[0] = NS_PROTO_ISMEMBER;
					mrc[1] = group_ismember (mb);
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					break;

				case NS_PROTO_INFO:
					if (debug && (!daemon_mode) ) puts("Info");
						/* returns NS_PROTO_INFO, entry point, #members */

					mrc[0] = NS_PROTO_INFO;
					mrc[1] = group_info (mb, c[i]);

					/* finished with buffer so free it */
					free_msg_buf (mb);

					/* group_info replies internally if all is well ONLY */
					/* i.e we reply here if there is an error only */
					if (mrc[1]<0) { /* i.e. error codes being sent back here */
						mrc[2]=0;	/* number of matching entries = 0! */
						send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
					}	
					break;
				case NS_PROTO_RMLV:
					if (debug && (!daemon_mode) )
						puts("Remove/Leave"); /* i.e. delete this entry from the system */

					mrc[0] = NS_PROTO_RMLV;
					mrc[1] = group_leave (mb, &result);
					mrc[2] = result;

					/* finished with buffer so free it */
					free_msg_buf (mb);

					send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_RDB_PUT:
					if (debug && (!daemon_mode) ) puts("RecordDB put");
					/* call function that takes appart the put request */
					mrc[0] = NS_PROTO_RDB_PUT;
					mrc[1] = record_put (mb, from, &result);
					mrc[2] = result;
					/* finished with buffer so free it */
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_RDB_CALLBACK_REG:
					if (debug && (!daemon_mode) ) puts("Register Callback");
					/* call function that takes appart the put request */
					mrc[0] = NS_PROTO_RDB_CALLBACK_REG;
					mrc[1] = record_callback_register (mb,c[i]);
					/* we free buffer in the function */

					if (mrc[1] != ns_err_other) { /* ns_err_other mean we reply internal  */
					    send_pkmesg (c[i], NS_ID_SERVER_ANON, 5, mrc, EMPTYMSGBUF, 0);	
					}
					break;
				case NS_PROTO_RDB_CALLBACK_UNREG:
					if (debug && (!daemon_mode) ) puts("Un-register Callback");
					/* call function that takes appart the put request */
					mrc[0] = NS_PROTO_RDB_CALLBACK_UNREG;
					mrc[1] = record_callback_unregister (mb);
					/* finished with buffer so free it */
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_RDB_INFO:
					if (debug && (!daemon_mode) ) puts("RecordDB Info");
						/* returns NS_PROTO_RDB_INFO, entry point, #members */

					mrc[0] = NS_PROTO_RDB_INFO;
					mrc[1] = record_info (mb, from, c[i]);

					/* finished with buffer so free it */
					free_msg_buf (mb);

					/* record_info replies internally if all is well ONLY */
					/* i.e we reply here if there is an error only */
					if (mrc[1]<0) { /* i.e. error codes being sent back here */
						mrc[2]=0;	/* number of matching entries = 0! */
						send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
						}
					break;
				case NS_PROTO_RDB_GET:
					if (debug && (!daemon_mode) ) puts("RecordDB Get");
						/* returns NS_PROTO_RDB_GET, entry point */

					mrc[0] = NS_PROTO_RDB_GET;
					mrc[1] = record_get (mb, from, c[i]);

					/* finished with buffer so free it */
					free_msg_buf (mb);

					/* record_get replies internally if all is well ONLY */
					/* i.e we reply here if there is an error only */
					if (mrc[1]<0) { /* i.e. error codes being sent back here */
						mrc[2]=-1;	/* slot is bad */
						send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
						}	
					break;
				case NS_PROTO_RDB_SWAP:
					if (debug && (!daemon_mode) ) puts("RecordDB SWAP");
					/* call function that takes apart the SWAP request */
					mrc[0] = NS_PROTO_RDB_SWAP;
					mrc[1] = record_swap (mb, from, &result);
					mrc[2] = result;
					/* finished with buffer so free it */
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 3, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_RDB_DELETE:
					if (debug && (!daemon_mode) ) puts("RecordDB Delete");
					/* call function that takes appart the join request */
					mrc[0] = NS_PROTO_RDB_DELETE;
					mrc[1] = record_delete (mb);
					free_msg_buf (mb);
					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_GID:
					if (debug && (!daemon_mode) ) puts("Get GlobalID");

					/* finished with buffer so free it */
					free_msg_buf (mb);

					mrc[1] = next_gid (l[1],l[2],c[i]);

					/* next_gid replies internally if all is well only */
					/* i.e we reply here if there is an error only */
					if(mrc[1]<0 ) { 
					        mrc[0] = NS_PROTO_GID;
						send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					}
					break;
				case NS_PROTO_DUMP:
					if (debug && (!daemon_mode) ) puts("Dump");

						dump_tables();

					mrc[0] = NS_PROTO_DUMP;
					mrc[1] = ngroups + nrecords; /* hack, maybe */

					/* finished with buffer so free it */
					free_msg_buf (mb);

					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					break;
				case NS_PROTO_DUMPALL:
					if (debug && (!daemon_mode) ) puts("DumpAll");

						dumpall_tables(); 

					mrc[0] = NS_PROTO_DUMPALL;
					mrc[1] = ngroups + nrecords; /* hack, maybe */

					/* finished with buffer so free it */
					free_msg_buf (mb);

					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					break;
#ifdef FORLATER
				case NS_PROTO_WAIT:
					if (debug && (!daemon_mode) ) puts("Wait");
						/* returns NS_PROTO_WAIT, entry point, # members */

					mrc[0] = NS_PROTO_WAIT;
					mrc[1] = group_wait (mb, c[i]);

					/* finished with buffer so free it */
					free_msg_buf (mb);

					/* group_wait replies if all is well or else */
					/* when someone joins or when the service is shut down */
					if (mrc[1]<0) { /* i.e. error codes being sent back here */
						send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	
					}	
					break;
#endif /* FOR LATER */
				case NS_PROTO_GBYE: /* happy exit */
					if (debug && (!daemon_mode) ) puts ("Good bye.");

					/* finished with buffer so free it */
					free_msg_buf (mb);

					mrc[0] = NS_PROTO_GBYE;
					mrc[1] = ns_ok; 

					send_pkmesg (c[i], NS_ID_SERVER_ANON, 2, mrc, EMPTYMSGBUF, 0);	

					usleep (NS_POLLWAIT);	/* give it time to get there */
					c_tmp = -1; /* kill the connection */
					break;
				case NS_PROTO_HALT:
					if(!daemon_mode) puts("Server shut down request ?\n");
					/* finished with buffer so free it */
					free_msg_buf (mb);
					exit(1);
					break;
				case -1:
                                        if(!daemon_mode) printf("Connection disconnect received.\n");
					/* finished with buffer so free it */
					free_msg_buf (mb);
					c_tmp = -1;
					break;
				default:
					if(!daemon_mode) printf("Unknown message of type [%d] received.\n Connection to this service will be dropped.\n", mtype);
					/* finished with buffer so free it */
					free_msg_buf (mb);
					c_tmp = -1;
					break;

			} /* end of switch */

			if (c_tmp==-1) { /* i.e. it exited */
				if (debug && (!daemon_mode) ) printf("Connection shutdown in a clean way for connection [%d] on socket [%d]\n", i, c[i]);
				closeconn(c[i]);
/* 				c[i] = -1; */
				c[i] = 0;
				ncon--;
				}
			} /* we have a message */
		} /* for each socket open */
	usleep (NS_POLLWAIT);	/* sleep after checking sockets */
/* BIG SLEEP */
/* usleep (10000); */
/* printf("ncon = [%d]\n", ncon); */
	} /* if there was some to check */
else  { /* nothing happening */
	if(!daemon_mode) puts("Sleep");
/* 	usleep (100000); */
/* 	sleep (1); */
	if(!daemon_mode) printf("ncon = [%d] after sleep\n", ncon);
	}
/* puts("E"); */
} /* main loop */

/* shut down connection.. this is a little messy but trys a clean disconnect */

for(i=0;i<ns_maxgroups;i++) {
		if (c[i]>0) {
			if(!daemon_mode) printf("Forcing close of connection [%d] socket [%d]\n", i, c[i]);
			closeconn(c[i]);
			c[i] = 0; 
		}
}
if(!daemon_mode) printf("Server shutdown in a clean way.\n");
closeconn(s);
return 0;
}
/* ------------------------------------------------------------------------------------------- */


/* */
/* */
/* */

/* real functions that do something other than handle connects and messages */

/* */
/* */
/* */
/* ------------------------------------------------------------------------------------------- */

void init_tables () {
	int g, m;	/* group, member */
	int r, a;	/* record, attribute */

	ngroups = 0;	/* set group count to zero */

	for (g=0;g<ns_maxgroups;g++) {
		grouptable[g].groupname[0] = '\0';	
		grouptable[g].gnamelen = 0;
		grouptable[g].num_members = 0;		/* i.e. empty */
		grouptable[g].lowestslotfree = 0;	/* new lowest free */
		group_socket[g] = -1;				/* no socket */
                grouptable[g].nreaders=0;
                grouptable[g].nwriters=0;
                grouptable[g].gencount=0;
		group_wakeup_list[g][0] = '\0';  	/* not waiting on anyone! */
		for (m=0;m<ns_maxprocs;m++) {
			grouptable[g].address[m] = 0;
			grouptable[g].port[m] = 0;
			grouptable[g].free[m] = 1;	/* 1 = free 0=in use */
		}		
	}
	lowest_group_entry_free = 0; /* lowest free entry is now 0 */

	if (debug && (!daemon_mode) ) printf("Group table init complete\n");

	/* now do the records table */

	nrecords = 0;	/* set record count to zero */

	for (r=0;r<ns_maxrecords;r++) {

		recordtable[r].recordname[0] = '\0';	
		recordtable[r].rnamelen = 0;	/* record with no name */
		recordtable[r].num_entries = 0;	/* i.e. empty */
		recordtable[r].lowestslotfree = 0;	/* new lowest free */
                recordtable[r].nreaders=0;
                recordtable[r].nwriters=0;
                recordtable[r].gencount=0;

		for (a=0;a<ns_maxprocs;a++) {
		    recordtable[r].callback[a].flag = -1;
		    recordtable[r].callback[a].addr = -1;
		    recordtable[r].callback[a].port = -1;
		    recordtable[r].callback[a].tag = -1;
		    recordtable[r].callback[a].gencount = -1;
		    recordtable[r].callback[a].slot = -1;
		}		

                recordtable[r].ncallback=0;
		recordtable[r].lowestcallback=0;

		for (a=0;a<ns_maxentries;a++) {

			recordtable[r].status[a] = 0;	/* NOT IN USE / no status */
			recordtable[r].lengths[a] = 0;	/* not in use */
			recordtable[r].recordptr[a] = NULL;	/* no record data */
			recordtable[r].owners[a] = -1;	/* no ownwer */
			recordtable[r].flags[a] = 0;	/* no flags */

			}		
		}

	lowest_record_entry_free = 0; /* lowest free entry is now 0 */

	if (debug && (!daemon_mode) ) printf("Record table init complete\n");
}
/* ------------------------------------------------------------------------------------------- */

void dump_group_entry (int e)
{
    int m;

    if(!daemon_mode) {
	printf("Entry [%d]\n" ,e);
	printf("Group name [%s]\n", grouptable[e].groupname);
	printf("Number of entries in group [%d]\n", grouptable[e].num_members);
        printf("Number of read is %d. Number of write is %d Generation counter is %d\n",grouptable[e].nreaders,grouptable[e].nwriters,grouptable[e].gencount);  
	printf("---------------------------------------\n");
/*     for (m=0;m<grouptable[e].num_members;m++) { */
    for (m=0;m<ns_maxprocs;m++) 
		if (!grouptable[e].free[m]) {
		printf("[%d] \taddress [0x%x] \tport [%d]\n", 
		 	m, grouptable[e].address[m], grouptable[e].port[m]);
        }
		printf("Next free slot is %d\n", grouptable[e].lowestslotfree);
	puts("\n");
     } 
}
/* ------------------------------------------------------------------------------------------- */

void dump_record_entry (int e)
{
    int a;	/* attribute */

    if(!daemon_mode) {

	printf("Entry [%d]\n" ,e);
	printf("Record name [%s]\n", recordtable[e].recordname);
	printf("Number of entries in record [%d]\n", recordtable[e].num_entries);
        printf("Number of read is %d. Number of write is %d Generation counter is %d\n",recordtable[e].nreaders,recordtable[e].nwriters,recordtable[e].gencount);  
	printf("---------------------------------------\n");
        for (a=0;a<ns_maxentries;a++) 
		if (recordtable[e].status[a]) {
		printf("[%d] \towner [0x%x]\tstatus:%d\tlen [%ld]\tflags [0x%x]\n", 
		 	a, recordtable[e].owners[a], recordtable[e].status[a],
			recordtable[e].lengths[a], recordtable[e].flags[a]);
        }
	printf("Ncallback [%d], LowestCallback [%d] \n",recordtable[e].ncallback,recordtable[e].lowestcallback);
        for (a=0;a<ns_maxprocs;a++) {
	    if (recordtable[e].callback[a].addr!=-1) {
                printf("[%d] \t addr [0x%x]\t port [%d]\t tag [%d]\t gencount [%d]\n", a, ns_get_hostname(recordtable[e].callback[a].addr), recordtable[e].callback[a].port, recordtable[e].callback[a].tag, recordtable[e].callback[a].gencount);
            }
        }
	printf("Next record free slot is %d\n", recordtable[e].lowestslotfree);
	puts("\n");
   }
}
/* ------------------------------------------------------------------------------------------- */

void dump_tables () {
	int i;

    if(!daemon_mode) {
	printf("Table entry information\n");
	printf("Group information\n");

	if (!ngroups) {
		printf("No current groups\n");
		}
	else {

		printf("Number of groups [%d]\n", ngroups);

		for (i=0;i<ns_maxgroups;i++) 
			if (grouptable[i].num_members) 
					printf("[%d]=[%s]\t", i, grouptable[i].groupname);

		printf("\nFull listing\n");

		for (i=0;i<ns_maxgroups;i++) 
			if (grouptable[i].num_members) dump_group_entry(i);

		printf("\n"); 
		if (lowest_group_entry_free!=-1)
			printf("Next lowest free is %d\n", lowest_group_entry_free);
		else
			printf("No next lowest free\n");
		} /* if groups */


	printf("Record information\n");

	if (!nrecords) {
		printf("No current record (types) stored\n");
	}
	else {

		printf("Number of records [%d]\n", nrecords);

		for (i=0;i<ns_maxrecords;i++) 
			if (recordtable[i].num_entries) 
					printf("[%d]=[%s]\t", i, recordtable[i].recordname);

		printf("\nFull details\n");

		for (i=0;i<ns_maxrecords;i++) 
			if (recordtable[i].num_entries!=0 || recordtable[i].ncallback!=0) dump_record_entry(i);

		printf("\n"); 
		if (lowest_record_entry_free!=-1)
			printf("Next record lowest free is %d\n", lowest_record_entry_free);
		else
			printf("No next record lowest free\n");
		}	/* if records */

	/* List IDs as well */
	printf("Generate ID counters\n");
	for (i=0;i<_NS_ID_END;i++) 
		printf("index %d next 0x%x\t", i, gidsnext[i]);
	printf("\n\n");
       fflush(stdout);
    }
}



/* ------------------------------------------------------------------------------------------- */

/* old proto */
/* int	group_join (s, mlen, outslot) */

int	group_join (int mbuf, int *outslot)
/* int mbuf;	 message buffer with requestion in it */
/* int *outslot;	 result var for result */
{
int i;
int name_len;
char gname[256];
int entry=-1;

int gslot;


/* handle message parts first */

/* readconn (s, &name_len, (sizeof(int))); */
/* readconn (s, (char*) gname, name_len); */
/* gname[name_len] = '\0'; */

name_len = upk_string (mbuf, gname, sizeof (gname));

if (debug && (!daemon_mode) ) printf("Gname is [%s] len[%d]\n", gname, name_len);


/* find slot for the gname, i.e. also check to see if it in use etc */
entry = -1;
for (i=0;i<ns_maxgroups;i++) {
	if (!strcmp(gname, grouptable[i].groupname)) {
		/* already in table */
/* 		printf("[%s] already in table at [%d]\n", gname, i); */
/* 		return (ns_err_dup); */
		/* not an error any more */
		entry = i; break;
	}
}	
/* not in table already */


if (entry==-1) {
	/* check for space */
	if (ngroups==ns_maxgroups) {
			/* no room at the inn */
            if(!daemon_mode) {
			printf("No room in the inn left for [%s]. Have [%d] entries already\n",
					gname, ngroups);
            }
			return (ns_err_no_space);
	}

	/* find free space for the entry */

        if (lowest_group_entry_free!=-1) {
	    entry=lowest_group_entry_free;
	    /* find next free slot */
	    lowest_group_entry_free=-1; /* in case we don't find one */
	    if (ngroups!=(ns_maxgroups-1)) { /* we can search */
		if ((entry+1)<ns_maxgroups) /* if we can search */
		    for(i=entry+1;i<ns_maxgroups;i++)
			if (!grouptable[i].num_members) {
			     lowest_group_entry_free = i; 
			     break;
			}
	    } /* if we search for a new free slot */
        } else {
	     for (i=0;i<ns_maxgroups;i++) {
		if (!grouptable[i].num_members) { entry=i; break; }
	     }
        }



}	/* if not in table and find space */

/* paranoid check */
if (entry==-1) {
       if(!daemon_mode) {
		printf("Panic, entry table messed up. Could not add [%s] even though we have only [%d] entries out of [%d] already??????\n",
				gname, ngroups, ns_maxgroups);
       }
		return (ns_err_panic);
}

/* ok have entry, so lets fill up the data structures :) */

/* this is different now from MPI_Connects use of groups */

/* now we can add entries one by one rather than all at once (atomic) */

/* is there a slot first */
if (grouptable[entry].num_members == ns_maxprocs) { /* no */
	/* no room at the inn */
       if(!daemon_mode) {
	printf("No room left in the inn for new entry in [%s]. Have [%d] entries already\n",
					gname, grouptable[entry].num_members);
       }
	return (ns_err_no_space);
}

/* ok find a slot */
gslot = -1;

if (grouptable[entry].lowestslotfree!=-1) {
	gslot=grouptable[entry].lowestslotfree;
	/* find next free slot */
	grouptable[entry].lowestslotfree=-1; /* in case we don't find one */
	if (grouptable[entry].num_members!=(ns_maxprocs-1)) { /* we can search */
		if ((gslot+1)<ns_maxprocs) /* if we can search */
			for(i=gslot+1;i<ns_maxprocs;i++)
				if (grouptable[entry].free[i]) {
					grouptable[entry].lowestslotfree = i; 
					break;
				}
		} /* if we search for a new free slot */
} /* a lowest slot pre known */
else {
	for (i=0;i<ns_maxprocs;i++) 
		if (grouptable[entry].free[i]) { gslot = i; break; }
}

if (gslot==-1) { 
	/* no room at the inn */
       if(!daemon_mode) {
	printf("No slot left for new entry in [%s]. Have [%d] entries already???\n",
					gname, grouptable[entry].num_members);
       }
	return (ns_err_no_space);
}

/* update the group_entry_s structure */

if (!grouptable[entry].num_members) { /* its a new entry */
	strcpy(grouptable[entry].groupname, gname);	/* name first of all */
	grouptable[entry].gnamelen = name_len;	/* remember length for IO use */
	ngroups ++;

	/* if doing FILE IO, update entry */
	#ifdef DISKBACKUP
		new_gdata_file (entry);
	#endif /* DISKBACKUP */
}

/* 		readconn (s, &(grouptable[entry].address[gslot]), sizeof(int)); */
/* 		readconn (s, &(grouptable[entry].port[gslot]), sizeof(int)); */

/* IP address is in the network class order, i.e. do a RAW read not a conv */
/* upk_int32 (mbuf, &(grouptable[entry].address[gslot]), 1); */
upk_raw32 (mbuf, &(grouptable[entry].address[gslot]), 1);
upk_int32 (mbuf, &(grouptable[entry].port[gslot]), 1);

grouptable[entry].free[gslot]=0;	/* no longer free */

/* ok update entry #s */

grouptable[entry].num_members++;
grouptable[entry].nwriters++;
grouptable[entry].gencount++;

/* dump new entry infomation to the console */
if (debug) dump_group_entry (entry);

/* if doing FILE IO, update entry */
#ifdef DISKBACKUP
	update_gdata_file (entry, gslot);
#endif /* DISKBACKUP */

/* return the entry number */
*outslot = gslot;
return (entry);
}
/* ------------------------------------------------------------------------------------------- */

/*
group_ismember - check an existence of member of the group
*/
int group_ismember(int mbuf)
{
   int name_len;
   char gname[256];
   int ip,port;
   int entry;
   int i;

   entry=ns_err_none;

   name_len = upk_string (mbuf, gname, sizeof (gname));	/* get the name */
   upk_raw32 (mbuf, &ip, 1);							
   upk_int32 (mbuf, &port, 1);							

   if(!daemon_mode) {
      printf("\n\nGNAME is %s\n",gname);
   }
   /* find slot used for the gname */
   for (i=0;i<ns_maxgroups;i++) {
       if (!strncmp(gname, grouptable[i].groupname, name_len)) {
           /* already in table */
           if (debug && (!daemon_mode) ) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, gname, i);
           if (!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, gname, i);
           entry = i;
           break;
       }
   }	
   if(entry==ns_err_none) {
       return 0; /* not found */
   }
   for (i=0;i<grouptable[entry].num_members;i++) {
       if((grouptable[entry].address[i]==ip) && (grouptable[entry].port[i]==port)) {
           return 1;
       }
   }
   return 0;
}

/*
 This is modified from MPI_connect to return only full entries hense 
 the index numbers as well as the contact info 
*/

int	group_info (int mbuf,int s)
/* int mbuf;	 message buffer of request */
/* int s;		 socket we reply to if all is well only */
{
int i;
int name_len;
int num_entries;
char gname[256];
int entry=-1;
int rmbuf;	/* reply message buffer */
int rents;	/* requested the first rents entries */
int npack;	/* how many entry details have we packed back to them */
int tosend;	/* how many are we going to send back */
int mrc[3];	/* my header for my reply */


/* handle message parts first */
name_len = upk_string (mbuf, gname, sizeof (gname));	/* get the name */
upk_int32 (mbuf, &rents, 1);							/* get the entry count THEY want */

if (debug && (!daemon_mode) ) printf("INFO: Gname is [%s] number of max requested entries %d\n", gname, rents);

if (!ngroups) {
	if(!daemon_mode) printf("No groups in table to search for [%s]\n", gname);
	return (ns_err_none);
}

/* find the gname */
for (i=0;i<ns_maxgroups;i++) {
	if (!strncmp(gname, grouptable[i].groupname, name_len)) {
		/* already in table */
		if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, gname, i);
		entry = i;
		break;
	}
}	

/* found check */
if (entry==-1) {
		if(!daemon_mode) printf("Could not find [%s] out of [%d] entries out of a maximum of [%d]?\n",
				gname, ngroups, ns_maxgroups);
		return (ns_err_panic);
}

/* ok have entry, so lets sent out the data structures :) */

num_entries = grouptable[entry].num_members;
grouptable[entry].nreaders++;

if (debug)
	if(!daemon_mode) printf("Sending data about entry [%d] group [%s] of [%d] entries\n",
			entry, gname, num_entries);

mrc[0] = NS_PROTO_INFO;
/* mrc[1] is the ok or fail code */
mrc[2] = num_entries;		/* how many are there? */

rmbuf = get_msg_buf (1);	/* resizable as it might be large */

if (num_entries > rents)
	tosend = rents;						/* how many do we send back, upto rents */
else
	tosend= num_entries;				/* how many do we send back, all! */


pk_int32 (rmbuf, &entry, 1);		/* location */
pk_int32 (rmbuf, &tosend, 1);		/* how many do we send back */

/* how many in this group */

npack = 0;
for(i=0;i<ns_maxprocs;i++) {
		if (npack==tosend) break;	/* we have sent all we can! */
									/* we have the test here to catch tosend=0 ! */
	/* send each entry in order */
	/* only if it exists */
	if (!grouptable[entry].free[i]) {
		pk_int32 (rmbuf, &i, 1);
		pk_raw32 (rmbuf, &(grouptable[entry].address[i]), 1); /* note raw */
		pk_int32 (rmbuf, &(grouptable[entry].port[i]), 1);	/* normal pk */
		npack++;
		if (debug &&(!daemon_mode)) printf("Packing %d 0x%x %d\n", i, grouptable[entry].address[i],
										grouptable[entry].port[i]);
		if (npack==tosend) break;	/* we have sent all we can! */
		} /* if it is a valid slot */
	} /* for each member */

/* ok message packed, create header and kick it in the bum */
/* if its ok that is */

/* paranoid check */
if (npack!=tosend) {
	if (debug && (!daemon_mode) ) 
		printf("Info: I should have packed %d but only packed %d instead? Cancelling reply\n",
				tosend, npack);
	free_msg_buf (rmbuf);	/* as it contains tainted data */
	return (ns_err_panic);
}

/* note we delete buffer when we send. */

mrc[1] = ns_ok;	/* as we are finally sure ? */
send_pkmesg (s, NS_ID_SERVER_ANON, 3, mrc, rmbuf, 1);


/* return zero for ok! */
return (ns_ok);
}
/* ------------------------------------------------------------------------------------------- */

int	group_leave (int mbuf,int * outslot)
/* int mbuf; */
/* int *outslot;	 slot that was deleted? */
{
int i;
int name_len;
int num_entries;
char gname[256];
int entry=ns_err_none;	/* default return (none found) */
int gslot;	/* location in entry */
int submitted_entry=ns_err_none;	/* entry sent by sender */
									/* compared so that a badly formed */
									/* message won't out a group by */
									/* accident */

/* this form of checking will be replaced later on by something a little */
/* more secure, like encripted tokens / certs */

/* handle message parts first */
name_len = upk_string (mbuf, gname, sizeof (gname));	/* get the name */
upk_int32 (mbuf, &submitted_entry, 1);							
upk_int32 (mbuf, &gslot, 1);							

if (debug && (!daemon_mode) )
	printf("Gname is [%s] len[%d] in possible entry [%d] at [%d]\n", 
				gname, name_len, submitted_entry, gslot);

/* find slot used for the gname */
for (i=0;i<ns_maxgroups;i++) {
	if (!strcmp(gname, grouptable[i].groupname)) {
		/* already in table */
		if (debug && (!daemon_mode)) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, gname, i);
		entry = i;
		break;
	}
}	

/* if not in table already */
if (entry<0) {
	if (debug && (!daemon_mode))
		printf("Group [%s] is not in the grouptable of [%d] entries. So cannot remove.\n", gname, ngroups);
	*outslot=-1;
	return (ns_err_none);
}

/* ok check entry for a match with submitted entry */
if (submitted_entry!=entry) {
     if(!daemon_mode) {
	fprintf(stderr,"Group remove on [%s:%d] was attempted by group at [%d]?",
			gname, entry, submitted_entry);
	fprintf(stderr,"Operation cancelled.\n");
      }
		*outslot=-1;
	return (ns_err_bad_arg);
}

/* is the location gslot a valid entry ? */
if (grouptable[entry].free[gslot]) {
     if(!daemon_mode) {
	fprintf(stderr,"Memory remove on [%s:%d:%d] was attempted on entry location\n",
			gname, entry, submitted_entry);
	fprintf(stderr,"Operation cancelled.\n");
      }
		*outslot=-1;
	return (ns_err_bad_arg);
}

/* Ok, free it up baby! */
num_entries = grouptable[entry].num_members; /* how many, first make copy */

grouptable[entry].address[gslot] = 0;
grouptable[entry].port[gslot] = 0;
grouptable[entry].free[gslot] = 1;
grouptable[entry].num_members--;

/* it might now be the lowest gslot :) */

if (grouptable[entry].lowestslotfree > gslot) 
	grouptable[entry].lowestslotfree = gslot;

if (num_entries==1) { /* its the last.... arrrggg */
	strcpy(grouptable[entry].groupname, "\0");	/* name first of all */
	grouptable[entry].gnamelen=0;
	grouptable[entry].num_members = 0;	/* nuke the number of members */
	grouptable[entry].nreaders=0;
	grouptable[entry].nwriters=0;
	grouptable[entry].gencount=0;

	/* update the count of groups */
	ngroups --;

        if (lowest_group_entry_free > entry) {
            lowest_group_entry_free = entry; 
        }
	if(ngroups == 0) { /* we are last group */
            lowest_group_entry_free = 0;
	}

	/* ok no lowest slot now... the first again */
	grouptable[entry].lowestslotfree = 0;

	/* if doing FILE IO, remove entry */
	#ifdef DISKBACKUP
		clear_gfile (entry);
	#endif /* DISKBACKUP */
}

else {	/* if its not the last entry update it */

	/* if doing FILE IO, remove entry */
	#ifdef DISKBACKUP
		update_gdata_file (entry, gslot);
	#endif /* DISKBACKUP */
}


/* return the entry number that was nuked/removed */
*outslot=gslot;
return (entry);
}
/* ------------------------------------------------------------------------------------------- */



/* 
 register that a group is waiting for another named group 
 When that group appears, wake it up
 The calling group still needs to call group_info() to get details though
*/

#ifdef FORLATER
int	group_wait (int s,int mlen)
{
int i, j, k;
int name_len;
int num_entries;
char gname[256];
int me=-1;
it mtype;


/* handle message parts first */

readconn (s, &me, (sizeof(int)));
readconn (s, &name_len, (sizeof(int)));
readconn (s, (char*) gname, name_len);

gname[name_len] = '\0';

if(!daemon_mode) printf("Entry [%d] [%s] is looking for Gname [%s]\n", 
			me, grouptable[me].groupname, gname);

/* first check to see if there is a need to search for the gname */

for (i=0;i<ns_maxgroups;i++) {
	if (!strcmp(gname, grouptable[i].groupname)) {
		/* already in table */
		if(!daemon_mode)  printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, gname, i);
		/* so build wakeup message */
		mtype = 505;
		writeconn (s, &mtype, (sizeof(int)));	/* message type */
		writeconn (s, &i, (sizeof(int)));		/* where it was found */
		return (ns_ok);
	}
}	
/* not in table already */

/* so add it to the wakeup list. */
strcpy (&group_wakeup_list[me][0], gname);

/* return the entry number */
return (ns_ok);
}
#endif /* FOR LATER */
/* yes will use this again later */


/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------------------------- */




/* record_send_update_info - send update information
@param entry record entry  
@param slot slot in that record entry
@oaram operation the operation that cause this update
*/
int record_send_update_info(int entry,int slot, int operation)
{
    int i,j;  
    int found;
    int s;
    int mrc[5];
    int rmbuf;
    int npack;
    int ncallback;

    found=0;

/* The update protocol

<<<-----------------------------  HEADER ---------------------------------------------------------->>> | 
------------------------------------------------------------------------------------------------------------------------------------------------
| NS_PRODO_RDB_UPDATE_INFO | User defined tag | operation | new generation counter | number of entries | length1 | data1 | length2 | data2 | ... 
------------------------------------------------------------------------------------------------------------------------------------------------

*/

    /* we have to save this value. because ncallback can be decrease in the loop */
    ncallback=recordtable[entry].ncallback;
   
/* if(!daemon_mode)   printf("\n\nAAAAAAAAA someone call update entry %d slot %d op %d\n\n\n",entry,slot,operation);  */

    for (i=0;i<ns_maxprocs;i++) {
        if(recordtable[entry].callback[i].addr!=-1) {
	    if ( (recordtable[entry].callback[i].gencount <= recordtable[entry].gencount) &&
                 (recordtable[entry].callback[i].slot==slot || recordtable[entry].callback[i].slot==ANYSLOT)) {

/*  if(!daemon_mode) printf("\n\n[1;32mAAAAAAAAAA UPDATE entry [%d] to host 0x%x port %d tag is %d[0m\n\n\n",entry,recordtable[entry].callback[i].addr,recordtable[entry].callback[i].port,recordtable[entry].callback[i].tag);  fflush(stdout); */

                  s=getconn_addr(recordtable[entry].callback[i].addr,&recordtable[entry].callback[i].port,0);
		  if(s<0) {    /* You are already dead */
		      record_callback_clean(entry, i);
                      found++;
		      continue;
		  }
                  mrc[0] = NS_PROTO_RDB_UPDATE_INFO;
                  mrc[1] = recordtable[entry].callback[i].tag; 
                  mrc[2] = operation;	                        
                  mrc[3] = recordtable[entry].gencount;	
                  mrc[4] = recordtable[entry].num_entries;	

                  rmbuf = get_msg_buf (1);
                  if(recordtable[entry].callback[i].slot==ANYSLOT) {
                      npack = 0;
                      for(j=0;j<ns_maxentries;j++) {
                         if (npack>=recordtable[entry].num_entries) break;	
                         if (recordtable[entry].status[j]) {
                          /*  pk_int32 (rmbuf, &(recordtable[entry].lengths[j]), 1); */
                            pk_byte (rmbuf, recordtable[entry].recordptr[j], recordtable[entry].lengths[j]);
                            npack++;
                            if (npack==recordtable[entry].num_entries) break;	
   	                 }
                      }
                  } else {
                      if(recordtable[entry].callback[i].slot==slot) {
                         if (recordtable[entry].status[slot]) { /* i.e. We will not send data on deleted slot */
                           /* pk_int32 (rmbuf, &(recordtable[entry].lengths[slot]), 1); Don't send it */ 
                            pk_byte (rmbuf, recordtable[entry].recordptr[slot], recordtable[entry].lengths[slot]);
	                 }
                      }
                  }
                  if (send_pkmesg (s, NS_ID_SERVER_ANON, 5, mrc, rmbuf, 1) <= 0) { 
		      /* You are already dead!! */
                      

/*  if(!daemon_mode) printf("\n\n[1;31mAAAAAAAAAA You are dead !! [host 0x%x port %d entry %d] [0m\n\n\n",recordtable[entry].callback[i].addr,recordtable[entry].callback[i].port,entry);  fflush(stdout); */

		      record_callback_clean(entry, i);
                      found++;
		      continue;
		  }
                  closeconn(s);
                  if((recordtable[entry].callback[i].flag & AUTOUNREG) == AUTOUNREG) {  
                      record_callback_clean(entry,i);
                  }
	    }
            found++;
        }
	if(found>=ncallback) break;
    }
    return 0;
}

int record_put (int mbuf,int  who,int * outslot)
/* int mbuf;	 message buffer with request in it */
/* int who;	 who called me? */
/* int *outslot;	 result var for result */
{
int i;
int name_len;
char rname[256];
int entry=-1;
int flag;
int rlen;		/* new record lengths in bytes */
int rslot;
int reqslot;	/* requested slot they want to use */
char* rptr;		/* pointer to the new records data lengths */
int change;    /* set this variable when update data of existing entry in the record */


/* handle message parts first */
change = 0;
name_len = upk_string (mbuf, rname, sizeof (rname));

if (debug && (!daemon_mode) ) printf("Rname is [%s] len[%d]\n", rname, name_len);

upk_int32 (mbuf, &flag, 1);
if (
    (!flag&RECORD_DB_DEFAULT)&&(!flag&RECORD_DB_SINGLEONLY)&&
    (!flag&RECORD_DB_READONLY)&&(!flag&RECORD_DB_PRIVATE)&&
    (!flag&RECORD_DB_INDEXED)&&(!flag&RECORD_DB_IFEMPTY)
    ) {
	if(!daemon_mode) printf("Record_put: request was with a bad flag arg of %d\n", flag);
	*outslot = -1;
	return (ns_err_bad_arg);
	}

/* now for the requested slot */
upk_int32 (mbuf, &reqslot, 1);

if (((reqslot<0)||(reqslot>=ns_maxentries))&&(flag&RECORD_DB_INDEXED)) {
	if(!daemon_mode) printf("Record_put: request was with a bad index value of %d\n", reqslot);
	*outslot = -1;
	return (ns_err_bad_arg);
	}

/* find slot for the rname, i.e. also check to see if it in use etc */
entry = -1;
for (i=0;i<ns_maxrecords;i++) {
	if (!strcmp(rname, recordtable[i].recordname)) {
		/* already in table */
		/* as it must have at least one arg */
		if (flag==RECORD_DB_SINGLEONLY && recordtable[i].num_entries!=0) { /* we fail as it already exists */
			if(!daemon_mode) printf("[%s] already in tables first slot at [%d]\n", rname, i);
			*outslot = -1;
			return (ns_err_dup);
			}
		entry = i; break;
	}
}	
/* not in table already */

if (entry==-1) {
	/* check for space */
	if (nrecords==ns_maxrecords) {
			/* no room at the inn */
			if(!daemon_mode) printf("No room in the inn left for [%s]. Have [%d] entries already\n",
					rname, nrecords);
			return (ns_err_no_space);
	}

	/* find free space for the entry */

        if (lowest_record_entry_free!=-1) {
	    entry=lowest_record_entry_free;
	    /* find next free slot */
	    lowest_record_entry_free=-1; /* in case we don't find one */
	    if (nrecords!=(ns_maxrecords-1)) { /* we can search */
		if ((entry+1)<ns_maxrecords) /* if we can search */
		    for(i=entry+1;i<ns_maxrecords;i++)
			if (!recordtable[i].num_entries) {
			     lowest_record_entry_free = i; 
			     break;
			}
	    } /* if we search for a new free slot */
        } else {
	     for (i=0;i<ns_maxrecords;i++) {
		if (!recordtable[i].num_entries) { entry=i; break; }
	     }
        }

}	/* if not in table and find space */

/* paranoid check */
if (entry==-1) {
		if(!daemon_mode) printf("Panic, entry table messed up. Could not add [%s] even though we have only [%d] entries out of [%d] already??????\n",
				rname, nrecords, ns_maxrecords);
		return (ns_err_panic);
}

/* ok have entry, so lets fill up the data structures :) */

/* is there a slot first */


/* ok find a slot */
if (flag&RECORD_DB_INDEXED) { /* ok they have a particular slot in mind */
	if (recordtable[entry].flags[reqslot]&RECORD_DB_READONLY) {
		if(!daemon_mode) printf("Record_put: requested index of %d in use READONLY\n", reqslot);
		*outslot = -1;
		return (ns_err_denied);
		}
	if ((flag&RECORD_DB_IFEMPTY)&&(recordtable[entry].status[reqslot])) {
		if(!daemon_mode) printf("Record_put: requested index of %d not empty.\n", reqslot);
		*outslot = -1;
		return (ns_err_dup);
		}
	if(recordtable[entry].status[reqslot]!=0) {
             change = 1;  
	}
	rslot = reqslot;	/* ok lets risk it */
	} /* if requested slot */

else {	/* ok find a free slot */
	rslot=-1;


/** Add caching **/

	if (recordtable[entry].num_entries == ns_maxentries) { /* no */
		/* no room at the inn */
		if(!daemon_mode) printf("No slot left for new entry in record [%s]. Have [%d] entries already???\n",
						rname, recordtable[entry].num_entries);
		return (ns_err_no_space);
		}


if (recordtable[entry].lowestslotfree!=-1) {
	rslot=recordtable[entry].lowestslotfree;
	/* find next free slot */
	recordtable[entry].lowestslotfree=-1; /* in case we don't find one */
	if (recordtable[i].num_entries!=(ns_maxentries-1)) { /* we can search */
		if ((rslot+1)<ns_maxentries)  { /* if we can search */
			for(i=rslot+1;i<ns_maxentries;i++) {
				if (recordtable[entry].status[i]==0) {
					recordtable[entry].lowestslotfree = i; 
					break;
				}
			}
		}
	} /* if we search for a new free slot */
} /* a lowest slot pre known */
else {
	for (i=0;i<ns_maxentries;i++) 
		if (!recordtable[entry].status[i]) { rslot = i; break; }

}

	if (rslot==-1) { 
		/* no room at the inn */
		if(!daemon_mode) printf("No slot left for new entry in record [%s]. Have [%d] entries already???\n",
						rname, recordtable[entry].num_entries);
		return (ns_err_no_space);
		}
	} /* find a slot */

/* check data to be stored before storing it */

rlen = 0;
upk_int32 (mbuf, &rlen, 1);

if (rlen>NS_MAX_RECORD_SIZE) {
	if(!daemon_mode) {
        printf("Record to be stored is %dbytes. Current limit is %d\n", rlen, NS_MAX_RECORD_SIZE);
	printf("Rather than truncating this record, am discarding instead.\n");
	printf("Update 'NS_MAX_RECORD_SIZE' in the name service header if you need more storage.\n");
        }
	return (ns_err_no_space);
	}

if (rlen<0) {	/* should be caught at the client? maybe a bad request exchange */
	if(!daemon_mode) printf("Record lengths is %d?? Ignoring.\n", rlen);
	return (ns_err_bad_arg);
	}

/* if all is well, store it and update structures */

rptr = (char*) malloc (rlen);
if (!rptr) { /* problem ! */
	if(!daemon_mode) printf("Internal error allocating %d bytes for record storage!\nDropping request!\n",rlen);
	return (ns_err_panic);
	}

upk_byte (mbuf, rptr, rlen);	/* get data from buffer and store it in binary format */

free_msg_buf (mbuf);	/* just in case (to stop any memory leaks) */

/* update the record_entry_s structure */

if (!recordtable[entry].num_entries) { /* its a new entry */
	strcpy(recordtable[entry].recordname, rname);	/* name first of all */
	recordtable[entry].rnamelen = name_len;
	if(recordtable[entry].ncallback==0) {
	     nrecords ++;
	}
}

recordtable[entry].status[rslot]=1;			/* the record is here */
recordtable[entry].lengths[rslot]=rlen;		/* remember lengths */
recordtable[entry].recordptr[rslot]=rptr;	/* get handle/ref to data */
recordtable[entry].owners[rslot]=who;		/* who owns me */
recordtable[entry].flags[rslot]=flag;		/* record flags */
recordtable[entry].nwriters++;
recordtable[entry].gencount++;

/* ok update entry #s */

if(change==0) {
recordtable[entry].num_entries++;
}

/* if doing FILE IO, update entry */
#ifdef DISKBACKUP
new_rdata_file (entry, rslot);
#endif /* DISKBACKUP */

/* dump new entry infomation to the console */
if (debug) dump_record_entry (entry);

/* send information to callback */
record_send_update_info(entry,rslot,NS_PROTO_RDB_PUT);

/* return the entry number */
*outslot = rslot;
return (entry);
}
/* ------------------------------------------------------------------------------------------- */


/*
 This returns both the number of matching record entries, their indices and sizes.
 Note. Private records are not returned unless ids match!
*/

int	record_info (int mbuf,int who,int s)
/* int mbuf;	 message buffer of request */
/* int who; */
/* int s;		 socket we reply to if all is well only */
{
int i;
int name_len;
int num_entries;
char rname[256];
int entry=-1;
int rmbuf;	/* reply message buffer */
int rents;	/* requested the first rents entries */
int npack;	/* how many entry details have we packed back to them */
int tosend;	/* how many are we going to send back */
int mrc[3];	/* my header for my reply */


/* handle message parts first */
name_len = upk_string (mbuf, rname, sizeof (rname));	/* get the name */
upk_int32 (mbuf, &rents, 1);							/* get the entry count THEY want */

free_msg_buf (mbuf);	/* just in case (to stop any memory leaks) */

if (debug && (!daemon_mode) ) printf("record_info: Rname is [%s] number of max requested entries %d\n", rname, rents);

if (!nrecords) {
	if(!daemon_mode) printf("No records in table to search for [%s]\n", rname);
	return (ns_err_none);
}

/* find the rname */
for (i=0;i<ns_maxrecords;i++) {
	if (!strncmp(rname, recordtable[i].recordname, name_len)) {
		/* already in table */
		if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, rname, i);
		entry = i;
		break;
	}
}	

/* found check */
if (entry==-1) {
		if(!daemon_mode) printf("Could not find [%s] out of [%d] entries out of a maximum of [%d]?\n",
				rname, nrecords, ns_maxrecords);
		return (ns_err_panic);
}

/* SHOULD CHECK HERE FOR PRIVICY FLAGS */
/* TODO XXX GEF */

/* ok have entry, so lets sent out the data structures :) */

num_entries = recordtable[entry].num_entries;
recordtable[entry].nreaders++;

if (debug && (!daemon_mode) )
	printf("Sending data about record entry [%d] record [%s] of [%d] entries\n",
			entry, rname, num_entries);

mrc[0] = NS_PROTO_RDB_INFO;
/* mrc[1] is the ok or fail code */
mrc[2] = num_entries;		/* how many are there? */

rmbuf = get_msg_buf (1);	/* resizable as it might be large */

if (num_entries > rents)
	tosend = rents;						/* how many do we send back, upto rents */
else
	tosend= num_entries;				/* how many do we send back, all! */


pk_int32 (rmbuf, &entry, 1);		/* location */
pk_int32 (rmbuf, &tosend, 1);		/* how many do we send back */

/* how many in this record */

npack = 0;
for(i=0;i<ns_maxentries;i++) {
		if (npack==tosend) break;	/* we have sent all we can! */
									/* we have the test here to catch tosend=0 ! */
	/* send each entry in order */

	if (recordtable[entry].status[i]) {
		pk_int32 (rmbuf, &i, 1);
		pk_int32 (rmbuf, &(recordtable[entry].lengths[i]), 1);
		npack++;
		if (debug && (!daemon_mode)) printf("Packing %d len %ld\n", i, recordtable[entry].lengths[i]);
		if (npack==tosend) break;	/* we have sent all we can! */
		} /* if it is a valid slot */
	} /* for each member */

/* ok message packed, create header and kick it in the bum */
/* if its ok that is */

/* paranoid check */
if (npack!=tosend) {
	if (debug && (!daemon_mode)) 
		printf("Info: I should have packed %d but only packed %d instead? Cancelling reply\n",
				tosend, npack);
	free_msg_buf (rmbuf);	/* as it contains tainted data */
	return (ns_err_panic);
}

/* note we delete buffer when we send. */

mrc[1] = ns_ok;	/* as we are finally sure ? */
send_pkmesg (s, NS_ID_SERVER_ANON, 3, mrc, rmbuf, 1);


/* return zero for ok! */
return (ns_ok);
}

/* ------------------------------------------------------------------------------------------- */

/*
 This returns a matching record entry, it index and lengths.
 Note. Private records are not returned unless ids match!
*/

int	record_get (int mbuf,int who,int s)
/* int mbuf;	 message buffer of request */
/* int who; */
/* int s;		 socket we reply to if all is well only */
{
int i;
int name_len;
int num_entries;
char rname[256];
int entry=-1;
int rslot;	/* did we find the one we wanted then? */
int rmbuf;	/* reply message buffer */
int mrc[4];	/* my header for my reply */
int flag;	/* op flags */
int reqslot;	/* the index of the item they want */


/* handle message parts first */
name_len = upk_string (mbuf, rname, sizeof (rname));	/* get the name */
upk_int32 (mbuf, &flag, 1);
upk_int32 (mbuf, &reqslot, 1);

free_msg_buf (mbuf);	/* just in case (to stop any memory leaks) */

if (debug && (!daemon_mode) ) printf("record_get: Rname is [%s] flags 0x%x @ %d\n", rname, flag, reqslot);

if (
    (!flag&RECORD_DB_DEFAULT)&&(!flag&RECORD_DB_SINGLEONLY)&&
    (!flag&RECORD_DB_READONLY)&&(!flag&RECORD_DB_PRIVATE)&&
    (!flag&RECORD_DB_INDEXED)&&(!flag&RECORD_DB_REMOVE)
    ) {
	if(!daemon_mode) printf("Record_get: request was with a bad flag arg of %d\n", flag);
	return (ns_err_bad_arg);
	}

/* now for the requested slot */

if (((reqslot<0)||(reqslot>=ns_maxentries))&&(flag&RECORD_DB_INDEXED)) {
	if(!daemon_mode) printf("Record_get: request was with a bad index value of %d\n", reqslot);
	return (ns_err_bad_arg);
	}

if (!nrecords) {
	if(!daemon_mode) printf("Record_get: No records in table to search for [%s]\n", rname);
	return (ns_err_none);
	}

/* find the rname */
for (i=0;i<ns_maxrecords;i++) {
	if (!strncmp(rname, recordtable[i].recordname, name_len)) { /* its in table */
		if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, rname, i);
		entry = i;
		break;
		}
	}	

/* found check */
if (entry==-1) {
		if(!daemon_mode) printf("Could not find [%s] out of [%d] entries out of a maximum of [%d]?\n",
				rname, nrecords, ns_maxrecords);
		return (ns_err_none);
		}

/* ok have entry, so lets examine the data structures :) */
/* we only read with caution (and delete even more so) */

rslot = -1;	/* start value */

/* first the is it active check */
if (flag&RECORD_DB_INDEXED) { /* for the indexed versions */

	if (!recordtable[entry].status[reqslot]) { /* is it alive? */
		if(!daemon_mode) printf("Record_get: requested index of %d is not in use\n", reqslot);
		return (ns_err_none);
		}

	/* it exists so check perms */

	if ((flag&RECORD_DB_REMOVE)&&(recordtable[entry].flags[reqslot]&RECORD_DB_READONLY)) {
		if(!daemon_mode) printf("Record_get: requested index of %d is READONLY. Use delete not RECORD_DB_REMOVE\n",
			reqslot);
		return (ns_err_denied);
		}

	if ((flag&RECORD_DB_REMOVE)&&(recordtable[entry].flags[reqslot]&RECORD_DB_PRIVATE)&&
		(who!=recordtable[entry].owners[reqslot])) {
		if(!daemon_mode) printf("Record_get: requested index of %d is PRIVATE and your not the owner!\n",
			reqslot);
		return (ns_err_denied);
		}

	/* then is it allowed? */
	rslot = reqslot;
	} /* indexed checks */

else { /* we probably have to search for the first available record */
	rslot=-1;
	for (i=0;i<ns_maxentries;i++) 
		if (recordtable[entry].status[i]) { /* found one, now check for PRIVATE & owner */ 
			if ((!(recordtable[entry].flags[i]&RECORD_DB_PRIVATE))||
					(recordtable[entry].owners[i]==who)) {
				rslot = i; 
				break; 
				}
			} /* found active */

	if (rslot==-1) { 	/* this shouldn't happen, but can with PRIVATE records in use */
		if(!daemon_mode) printf("No active [readable] record found for record named [%s]?\n", rname);
		return (ns_err_none);
		}
	} /* find a slot */

mrc[0] = NS_PROTO_RDB_GET;
/* mrc[1] is the ok or fail code */
mrc[2] = rslot;		/* which slot? */

/* No error at this point, update nreaders */
recordtable[entry].nreaders++;

/* resizable as I might get this wrong... :) But I won't */
rmbuf = get_msg_buf_of_size (((sizeof(int)*4)+recordtable[entry].lengths[rslot]), 1, 0);	

if (rmbuf<0) {
	if(!daemon_mode) printf("record_get: memory problem building reply for message of size %ld\n",
			(sizeof(int)*4)+recordtable[entry].lengths[rslot]);
	return (ns_err_panic);
	}

if (debug && (!daemon_mode)) printf("record_get: sending [%s]@[%d:%d] via buffer %d of record length %ld\n",
					rname, entry, rslot, rmbuf, recordtable[entry].lengths[rslot] );

/* I was going to pack owner, flags, etc but who needs it? maybe later */

pk_int32 (rmbuf, &(recordtable[entry].lengths[rslot]), 1);	/* lengths */
pk_byte (rmbuf, recordtable[entry].recordptr[rslot], recordtable[entry].lengths[rslot]);	/* the data */

if (debug && (!daemon_mode)) 
	printf("record_get: packed a record of lengths %ld\n", recordtable[entry].lengths[rslot]);

/* ok message packed, create header and kick it in the bum */

mrc[1] = entry;	/* as we are finally sure ? */
mrc[3] = recordtable[entry].gencount;	/* as we are finally sure ? */
send_pkmesg (s, NS_ID_SERVER_ANON, 4, mrc, rmbuf, 1);	/* 1= rm buffer when done */

/* Now for some homework. */
/* Here we have to delete the message if it was originally requested */

if (flag&RECORD_DB_REMOVE) { /* ok we have to do some deleting here */
	num_entries = recordtable[entry].num_entries; /* how many, first make copy */

	recordtable[entry].status[rslot] = 0;	/* out of action */
	recordtable[entry].owners[rslot] = 0;	
	recordtable[entry].flags[rslot] = 0;	
	if (recordtable[entry].lengths[rslot]) { /* we have some memory to free up */
		if (recordtable[entry].recordptr[rslot]) free (recordtable[entry].recordptr[rslot]);
		recordtable[entry].lengths[rslot] = 0; /* it is now */
		}
	recordtable[entry].num_entries--;

	#ifdef DISKBACKUP
	clear_rfile (entry, rslot);
	#endif /* DISKBACKUP */
	
	/* but what happens if we have no more? */

        /* it might now be the lowest rslot :) */
        if (recordtable[entry].lowestslotfree > rslot) 
	     recordtable[entry].lowestslotfree = rslot;

        record_send_update_info(entry,rslot,NS_PROTO_RDB_GET);
	if (num_entries==1 && recordtable[entry].ncallback==0) { /* its the last.... arrrggg */
		strcpy(recordtable[entry].recordname, "\0");	/* name first of all */
		recordtable[entry].rnamelen=0;
		recordtable[entry].num_entries = 0;	/* nuke the number of members */

	        recordtable[entry].lowestslotfree = 0;
		recordtable[entry].nreaders =0;
		recordtable[entry].nwriters =0;
		recordtable[entry].gencount =0;
		/* update the count of records */
		nrecords --;
                if (lowest_record_entry_free > entry) {
                     lowest_record_entry_free = entry; 
                }
	        if(nrecords == 0) { /* we are last group */
                     lowest_record_entry_free = 0;
	        }
	}
if (debug && (!daemon_mode) ) printf("record_get: removed record [%s] at [%d:%d] after get completed.\n",
					rname, entry, rslot);

	} /* if removing record after send */

/* return zero for ok! */
return (ns_ok);
}

/* 
 This does an atomic swap operation
 If you can match the original poster of the record in you arg list
 */

int	record_swap (int mbuf,int who,int * outslot)
/* int mbuf;	 message buffer with request in it */
/* int who;	 who called me? */
/* int *outslot;	 result var for result */
{
int i;
int name_len;
char rname[256];
int entry=-1;
int flag;
int rlen;		/* new record lengths in bytes */
int reqslot;	/* requested slot they want to use */
char* rptr;		/* pointer to the new records data lengths */
int oldowner;	/* the old owner of the record */


/* handle message parts first */

name_len = upk_string (mbuf, rname, sizeof (rname));

if (debug && (!daemon_mode) ) printf("Rname is [%s] len[%d]\n", rname, name_len);

upk_int32 (mbuf, &flag, 1);
if (flag!=RECORD_DB_INDEXED)
    {
	if(!daemon_mode) printf("Record_swap: request was with a bad flag arg of %d\n", flag);
	*outslot = -1;
	return (ns_err_bad_arg);
	}

/* now for the requested slot */
upk_int32 (mbuf, &reqslot, 1);

if (((reqslot<0)||(reqslot>=ns_maxentries))&&(flag&RECORD_DB_INDEXED)) {
	if(!daemon_mode) printf("Record_swap: request was with a bad index value of %d\n", reqslot);
	*outslot = -1;
	return (ns_err_bad_arg);
	}

/* find slot for the rname, i.e. also check to see if it in use etc */
entry = -1;
for (i=0;i<ns_maxrecords;i++) {
	if (!strcmp(rname, recordtable[i].recordname)) {
		/* already in table */
		entry = i; break;
		}
	}	

/* not in table already */
if (entry==-1) {
	/* return error */
	*outslot = -1;
	return (ns_err_none);
	}


/* there a slot but it it in use or empty */
if (!recordtable[entry].status[reqslot]) {	/* empty? */
	/* return error */
	*outslot = -1;
	recordtable[entry].nreaders++;
	return (ns_err_none);
	}

upk_int32 (mbuf, &oldowner, 1); /* read in the oldowner */

/* so the record exists and is valid */
/* NOW we swap if we know the original owner only */
if (recordtable[entry].owners[reqslot]!=oldowner) {
	/* return error */
	*outslot = -1;
	recordtable[entry].nreaders++;
	return (ns_err_denied);
	}

/* OK we got the owner right */
/* We now swap out the old and insert the new */


rlen = 0;
upk_int32 (mbuf, &rlen, 1);

if (rlen>NS_MAX_RECORD_SIZE) {
    if(!daemon_mode) {
	printf("Record to be stored is %dbytes. Current limit is %d\n", rlen, NS_MAX_RECORD_SIZE);
	printf("Rather than truncating this record, am discarding instead.\n");
	printf("Update 'NS_MAX_RECORD_SIZE' in the name service header if you need more storage.\n");
    }
	recordtable[entry].nreaders++;
	return (ns_err_no_space);
	}

if (rlen<0) {	/* should be caught at the client? maybe a bad request exchange */
	if(!daemon_mode) printf("Record lengths is %d?? Ignoring.\n", rlen);
	recordtable[entry].nreaders++;
	return (ns_err_bad_arg);
	}

/* if all is well, store it and update structures */


rptr = (char*) malloc (rlen);
if (!rptr) { /* problem ! */
	if(!daemon_mode) printf("Internal error allocating %d bytes for record storage!\nDropping request!\n",rlen);
	recordtable[entry].nreaders++;
	return (ns_err_panic);
	}

/* No more errors possible so NOW and only NOW do I free up the original Record
*/
free (recordtable[entry].recordptr[reqslot]);	/* free up the old memory */

upk_byte (mbuf, rptr, rlen);	/* get data from buffer and store it in binary format */

free_msg_buf (mbuf);	/* just in case (to stop any memory leaks) */

/* update the record_entry_s structure */

recordtable[entry].status[reqslot]=1;			/* the record is here */
recordtable[entry].lengths[reqslot]=rlen;		/* remember lengths */
recordtable[entry].recordptr[reqslot]=rptr;	/* get handle/ref to data */
recordtable[entry].owners[reqslot]=who;		/* who owns me */
recordtable[entry].flags[reqslot]=flag;		/* record flags */

/* ok update entry #s */

/* if doing FILE IO, update entry */
#ifdef DISKBACKUP
new_rdata_file (entry, reqslot);
#endif /* DISKBACKUP */


/* send information to callback */
recordtable[entry].nwriters++;
recordtable[entry].nreaders++;
recordtable[entry].gencount++;
record_send_update_info(entry,reqslot,NS_PROTO_RDB_SWAP);

/* return the entry number */
*outslot = reqslot;

/* dump new entry infomation to the console */
if (debug) dump_record_entry (entry);

return (entry);
}

/* ------------------------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------------------------- */

/* GID routines */

void init_gids ()
{

basegids[NS_ID_CORE] = NS_ID_CORE_BASE;
basegids[NS_ID_FTMPI] = NS_ID_FTMPI_BASE;
basegids[NS_ID_OTHER] = NS_ID_OTHER_BASE;
basegids[NS_ID_FTMPI_COMS] = NS_ID_FTMPI_COMS_BASE;
basegids[_NS_ID_END] = _NS_ID_END_BASE;

gidsnext[NS_ID_CORE] = NS_ID_CORE_BASE;
gidsnext[NS_ID_FTMPI] = NS_ID_FTMPI_BASE;
gidsnext[NS_ID_OTHER] = NS_ID_OTHER_BASE;
gidsnext[NS_ID_FTMPI_COMS] = NS_ID_FTMPI_COMS_BASE;
gidsnext[_NS_ID_END] = -1;

endgids[NS_ID_CORE] = NS_ID_CORE_END;
endgids[NS_ID_FTMPI] = NS_ID_FTMPI_END;
endgids[NS_ID_OTHER] = NS_ID_OTHER_END;
endgids[NS_ID_FTMPI_COMS] = NS_ID_FTMPI_COMS_END;
endgids[_NS_ID_END] = -1;
return;
}

/* ------------------------------------------------------------------------------------------- */

int next_gid (int index, int range, int socket)
{
   int start, end;
   int rmbuf;
   int mrc[2];

   if ((index<0)||(index>=_NS_ID_END) || (range<1) || (range >(endgids[index]-basegids[index])) ) return (-1);

   start=gidsnext[index];

   if((gidsnext[index] + range) > endgids[index]) { /* roll over next */
      gidsnext[index] = basegids[index]+((range-(endgids[index]-gidsnext[index]))-1) ;
   } else {	
      gidsnext[index]=gidsnext[index]+range;
   }

   if(gidsnext[index]==basegids[index]) {
       end=gidsnext[index]-1;
   } else {
       end=endgids[index];
   }

/* wrap around check */
/* this old check made sure all the ranges couldn't over lap */
/* this was nice, but our new method means we can restrict ids better */
/* if (gidsnext[index]==basegids[index+1]) gidsnext[index] = basegids[index];
*/

   mrc[0] = NS_PROTO_GID;
   mrc[1] = range;

   rmbuf = get_msg_buf (1);	/* resizable as it might be large */
   pk_int32 (rmbuf, &start, 1);
   pk_int32 (rmbuf, &end, 1);
   if(end < start ) { /* roll over */
      pk_int32 (rmbuf, &basegids[index], 1);		
      pk_int32 (rmbuf, &endgids[index], 1);	
   }
   send_pkmesg (socket, NS_ID_SERVER_ANON, 2, mrc, rmbuf, 1);

   return range;
}

/* ------------------------------------------------------------------------------------------- */

/*
 If the system is compiled with DISKBACKUP and the correct args are passed
 during init time, then the name service can copy its contents to DISK!!
 If the disk is in /tmp then the loss of that node will probably kill us
 If the disk is NFS mounted we are better off.
 This mod is brought to you be Graham awaiting the ring from Tone still

 This system supports two versions.
 init files for each group and record type
 single file MMapped to actual memory (faster)

 (but the data is network byte order to give it a chance)

 Graham Jan/Feb 2002 RUS-Stuttgart/HLRS
 */
/* ------------------------------------------------------------------------------------------- */

#ifdef DISKBACKUP

int init_files ()
{
int i;
FILE *fp;
char tname[512];
int id;

id = (int) getuid ();

#ifdef NSBASEDIR
	sprintf(fbname, "%s/NS_DB_%d\0", NSBASEDIR, id);
#else
	sprintf(fbname, "/tmp/NS_DB_%d\0", id);
#endif

sprintf(gindexname, "%s/group_index.db\0", fbname);
sprintf(gdataname, "%s/group_data.db\0", fbname);
sprintf(rindexname, "%s/record_index.db\0", fbname);
sprintf(rdataname, "%s/record_data.db\0", fbname);
sprintf(gidfname, "%s/GID.db\0", fbname);

if(!daemon_mode) printf("Base [%s] index [%s] datahdr [%s] index [%s] datahdr [%s] GID [%s]\n", 
			fbname, gindexname, gdataname, rindexname, rdataname, gidfname);


}


int	init_index_files ()
{
int i, err;
int g, m;	/* group, member */
int r, a;	/* record, attribute */
int v;

/* First we make sure we can build the base directory name/path */
/* if it doesn't exist create it or attempt to create it */

err = check_mk_dir (fbname);
if (err<0) {
   if(!daemon_mode) {
	fprintf(stderr,"Cannot create the base path [%s] needed for the DB records\n", fbname);
	fprintf(stderr,"Either fix this, OR restart the NS without DB backup.\n");
	fprintf(stderr,"If the NS is required to backup and cannot IT WILL exit\n");
    }
	exit (-1);
	}

/* OK have the directory */
/* Clear out any pre-existing files */
clear_index_files ();
clear_data_files ();

/* OK we can now create new files and remember their file pointers */

fpgindex = fopen (gindexname, "w");
fprindex = fopen (rindexname, "w");

/* GID counts as an 'index' file */
fpgid = fopen (gidfname, "w");

if ((!fpgindex)||(!fprindex)||(!fpgid)) {
	if(!daemon_mode) fprintf(stderr,"Problem opening index files %s & %s\n.Halting.\n",
			gindexname, rindexname);
	exit (-1);
	}

v = 0;
v = htonl (v);
fwrite(&v, sizeof(int), 1, fpgindex);	/* # of groups */

for (g=0;g<ns_maxgroups;g++) {
	fwrite(&v, sizeof(int), 1, fpgindex);	/* entries per group */
	}

fwrite(&v, sizeof(int), 1, fprindex);	/* # of records */

for (r=0;r<ns_maxrecords;r++) {
	fwrite(&v, sizeof(int), 1, fprindex);	/* entries per record */
	}



fflush (fpgindex);
fflush (fprindex);

}


int clear_index_files ( )
{

/* First close any files and clear pointers */

if (fpgindex) fclose (fpgindex);
fpgindex = NULL;

if (fprindex) fclose (fprindex);
fprindex = NULL;

if (fpgid) fclose (fpgid);
fpgid = NULL;


/* Now we unlick (delete) the index files if required */
/* always making sure we have valid filenames first :) */

if (gindexname) unlink (gindexname);
if (rindexname) unlink (rindexname);
if (gidfname) unlink (gidfname);

return (0);
}

int clear_data_files ( )
{
/* Clear data files */
int g, m;	/* group, member */
int r, a;	/* record, attribute */
char tname[512];

for (g=0;g<ns_maxgroups;g++) clear_gfile (g);
	
for (r=0;r<ns_maxrecords;r++) {
		for (a=0;a<ns_maxentries;a++) {
			clear_rfile (r,a);
		}
	}


return (0);
}

int clear_gfile (int g)
{
char tname[512];
sprintf(tname,"%s.%d\0", gdataname, g);
return (unlink (tname));
}



int update_index_file ()
{
}

/* This creates a new group data file */
int new_gdata_file (int g)
{
char tname[512];
FILE *fp;
int sl;
int v;
int i, j;

sprintf(tname,"%s.%d\0", gdataname, g);
fp = fopen (tname, "w");
if (!fp) return (-1);	/* problem, we cannot do anything, so we just return */

/* write record */
/* struct { #entries, {addr,port} repear for entries, strlen, str } */

v = htonl (ns_maxprocs);	/* max # of entries / slots */
fwrite (&v, sizeof(v), 1, fp);

v = 0;
for (i=0;i<ns_maxprocs;i++) {
	fwrite (&v, sizeof(v), 1, fp);	/* address */
	fwrite (&v, sizeof(v), 1, fp);	/* port */
	}

v = htonl (grouptable[g].gnamelen);
fwrite (&v, sizeof(v), 1, fp);

fwrite (grouptable[g].groupname, grouptable[g].gnamelen, 1, fp);

fclose (fp);
}

/* This creates a new group data file */
int update_gdata_file (int g, int m)
{
char tname[512];
FILE *fp;
int v;
long off;
int rc;

sprintf(tname,"%s.%d\0", gdataname, g);
fp = fopen (tname, "r+");
if (!fp) return (-1);	/* problem, we cannot do anything, so we just return */

off = sizeof(int) + (m*(sizeof(int)*2));

rc = fseek (fp, off, SEEK_SET);

/* paranoid check for trunction */
if (rc<0) {
	fclose (fp);
	return (rc);
	}

v = htonl (grouptable[g].address[m]);
fwrite (&v, sizeof(v), 1, fp);

v = htonl (grouptable[g].port[m]);
fwrite (&v, sizeof(v), 1, fp);

fclose (fp);
}

/* This creates a new record data file */
int new_rdata_file (int r, int m)
{
char tname[512];
FILE *fp;
int sl;
int v;
int i, j;

sprintf(tname,"%s.%d.%d\0", rdataname, r, m);
fp = fopen (tname, "w");
if (!fp) return (-1);	/* problem, we cannot do anything, so we just return */

/* write record */
/* struct { strlen, str, reclen, record } */

v = htonl (recordtable[r].rnamelen);	/* record name len */
fwrite (&v, sizeof(v), 1, fp);

fwrite (recordtable[r].recordname, recordtable[r].rnamelen, 1, fp);


v = htonl (recordtable[r].owners[m]);	/* record owners */
fwrite (&v, sizeof(v), 1, fp);

v = htonl (recordtable[r].flags[m]);	/* record flags */
fwrite (&v, sizeof(v), 1, fp);

v = htonl (recordtable[r].lengths[m]);	/* record length */
fwrite (&v, sizeof(v), 1, fp);

fwrite (recordtable[r].recordptr[m], recordtable[r].lengths[m], 1, fp); /* record data */

fclose (fp);
}

int clear_rfile (int r, int m)
{
char tname[512];
sprintf(tname,"%s.%d.%d\0", rdataname, r, m);
return (unlink (tname));
}

/* ------------------------------------------------------------------------------------------- */

/* 
 these are the routines needed to restore a previous NS servers DB from disk in the event of a
 crash.
	GEF Stuttgart 2002
 ------------------------------------------------------------------------------------------- */

int restore_db_from_disk (int rval)
{
int i;

return (0);

}


#endif /* DISKBACKUP */

/* ------------------------------------------------------------------------------------------- */



void dumpall_tables () {
	int i;
	char next[128];
	char base[128];
	char end[128];

        if(daemon_mode) return;

	puts("\n============= Begin Dump All ================\n");
	printf("---------- GROUP INFORMATION ----------\n");

	if (!ngroups) {
		printf("No current groups\n\n");
	} else {
		if (lowest_group_entry_free!=-1) {
		    printf("Number of groups [%d], Next group [%d], Max group [%d]\n\n\n", ngroups, lowest_group_entry_free, ns_maxgroups);
		} else {
		    printf("Number of groups [%d], No group free\n\n\n", ngroups);
		}

		for (i=0;i<ns_maxgroups;i++) 
			if (grouptable[i].num_members) dumpall_group_entry(i);

	} 
	printf("---------- RECORD INFORMATION ---------\n");

	if (!nrecords) {
	       printf("No current records\n\n");
	} else {
		if (lowest_record_entry_free!=-1) {
			printf("Number of records [%d], Next record [%d], Max record [%d]\n\n\n", nrecords, lowest_record_entry_free, ns_maxrecords);
		} else {
			printf("Number of records [%d], No record free\n\n\n", nrecords);
		}

		for (i=0;i<ns_maxrecords;i++) 
			if (recordtable[i].num_entries!=0 || recordtable[i].ncallback!=0) dumpall_record_entry(i);
	} 

	/* List IDs as well */
	printf("---------- ID INFORMATION ---------\n");
	for (i=0;i<_NS_ID_END;i++) {
		sprintf(next,"[0x%x]",gidsnext[i]);
		sprintf(base,"[0x%x]",basegids[i]);
		sprintf(end,"[0x%x]",endgids[i]);
	        switch(i) {
			case NS_ID_CORE:       printf("Core:       next %-10.10s base %-10.10s end %-10.10s\n",next,base,end);   break;
			case NS_ID_OTHER:      printf("Other:      next %-10.10s base %-10.10s end %-10.10s\n",next,base,end);   break;
			case NS_ID_FTMPI:      printf("Ftmpi:      next %-10.10s base %-10.10s end %-10.10s\n",next,base,end);   break;
			case NS_ID_FTMPI_COMS: printf("Ftmpi_coms: next %-10.10s base %-10.10s end %-10.10s\n",next,base,end);   break;
                        default: printf("next UNKNOWN ID\n"); break;
		};	
	}
	printf("\n\n");
	puts("=============== End Dump All ================\n");

fflush(stdout);
}

void dumpall_group_entry (int e)
{
    int m;
    char id[128]; 
    char addr[128]; 
    char port[128]; 
 
        if(daemon_mode) return;

	printf("Group [%d]=[%s]\n",e, grouptable[e].groupname);
	printf("Number of entries [%d], Next entries [%d], Max entries [%d]\n", grouptable[e].num_members, grouptable[e].lowestslotfree, ns_maxprocs);
	printf("Number of read [%d], Number of write [%d], Generation counter [%d]\n", grouptable[e].nreaders, grouptable[e].nwriters, grouptable[e].gencount);
	printf("---------------------------------------\n");
        for (m=0;m<ns_maxprocs;m++) {
	   if (!grouptable[e].free[m]) {
                sprintf(id,"[%d]", m);
                sprintf(addr,"address [%s]", ns_get_hostname(grouptable[e].address[m]));
                sprintf(port,"port [%d]", grouptable[e].port[m]);
		printf("%-6.6s %-30.30s %-20.20s\n",id,addr,port);
           }
	}
	puts("\n");
}
/* ------------------------------------------------------------------------------------------- */

void dumpall_record_entry (int e)
{
    int a;	/* attribute */
    int first = 1;
    int flag;
    char id[128];
    char owner[128];
    char length[128];

    char saddr[128];
    char sport[128];
    char stag[128];
    char sgencount[128];
    char sflag[128];
    char sslot[128];
    int found;

        found=0;	
        if(daemon_mode) return;

	printf("Record [%d]=[%s]\n", e,recordtable[e].recordname);
	printf("Number of entries [%d], Next entries [%d], Max entries [%d]\n", recordtable[e].num_entries,recordtable[e].lowestslotfree,ns_maxentries);
	printf("Number of read [%d], Number of write [%d], Generation count [%d]\n", recordtable[e].nreaders, recordtable[e].nwriters, recordtable[e].gencount);
	printf("---------------------------------------\n");
        for (a=0;a<ns_maxentries;a++) {
	    if (recordtable[e].status[a]) {
                sprintf(id,"[%d]", a);
                sprintf(owner,"owner [0x%x]", recordtable[e].owners[a]);
                sprintf(length,"len [%ld]", recordtable[e].lengths[a]);
		printf("%-6.6s %-15.15s %-15.15s",id,owner,length);

		printf("flags["); 
		flag=recordtable[e].flags[a];
		if(flag&RECORD_DB_SINGLEONLY) {
                    if(first) { printf("single only"); first=0; }  else printf(",single only");
		}
		if(flag&RECORD_DB_READONLY) {
                    if(first) { printf("read only"); first=0; }  else printf(",read only"); 
		}
		if(flag&RECORD_DB_REMOVE) {
                    if(first) { printf("remove"); first=0; }  else printf(",remove"); 
		}
		if(flag&RECORD_DB_INDEXED) {
                    if(first) { printf("indexed"); first=0; }  else printf(",indexed"); 
		}
		if(flag&RECORD_DB_PRIVATE) {
                    if(first) { printf("private"); first=0; }  else printf(",private"); 
		}
		if(flag&RECORD_DB_IFEMPTY) {
                    if(first) { printf("if empty"); first=0; }  else printf(",if empty"); 
		}
		if(first) {
		    if(flag==RECORD_DB_DEFAULT) printf("default"); else printf("INVALID");
		}
	        puts("]");
		found++;
            }
	    if(found>=recordtable[e].num_entries) break;
	}
        found=0;	
	printf("*** Ncallback [%d], LowestCallback [%d] ***\n",recordtable[e].ncallback,recordtable[e].lowestcallback);
        for (a=0;a<ns_maxprocs;a++) {
	    if (recordtable[e].callback[a].addr!=-1) {
                sprintf(id,"[%d]", a);
                sprintf(saddr,"address [%s]", ns_get_hostname(recordtable[e].callback[a].addr));
                sprintf(sport,"port [%d]", recordtable[e].callback[a].port);
                sprintf(stag,"tag [%d]", recordtable[e].callback[a].tag);
                sprintf(sgencount,"gencount [%d]", recordtable[e].callback[a].gencount);
		if(recordtable[e].callback[a].flag==0) {
                    sprintf(sflag,"  [DEFAULT]");
		} else {
                    sprintf(sflag,"[AUTOUNREG]");
		}
		if(recordtable[e].callback[a].slot==ANYSLOT) {
                    sprintf(sslot,"slot [ANY]");
		} else {
                    sprintf(sslot,"slot [%d]", recordtable[e].callback[a].slot);
		}
		printf("%-6.6s %-30.30s %-12.12s %-12.12s %-12.12s %-12.12s %-10.10s\n",id,saddr,sport,stag,sgencount,sflag,sslot);
		found++;
            }
	    if(found>=recordtable[e].ncallback) break;
        }
	puts("\n");
}
/* ------------------------------------------------------------------------------------------- */


/**
ns_getHostname - getHostname from IP(v4)
@param addr address
@return hostname
*/
char * ns_get_hostname(int addr)
{
    struct hostent *hp;
    struct in_addr saddr;
    saddr.s_addr=addr;

    hp = gethostbyaddr((char *) &addr, sizeof(addr), AF_INET);
    if (hp==NULL) {
       return inet_ntoa(saddr);
    } else {

#ifdef ENABLE_ALIASES    /* Enable alias hostname */
       if(hp->h_aliases[0]!=NULL) {
           return hp->h_aliases[0];
       } 
#endif
       if(hp->h_name !=NULL) {
           return hp->h_name;
       } else {
           return inet_ntoa(saddr);
       }
   }
}


/* record_callback_register - register callback
@param mbuf message buffer
@param sockfd reply sockfd
*/
int record_callback_register (int mbuf, int sockfd)
{
    int name_len;
    char rname[256];
    int i;
    int entry;
    int addr;
    int port;
    int tag;
    int gencount;
    int slot;
    int reqslot;
    int flag;
    int rmbuf;
    int mrc[5];

    entry = -1;

    name_len = upk_string (mbuf, rname, sizeof (rname));

    /* find the rname */
    if(nrecords>0) {
        for (i=0;i<ns_maxrecords;i++) {
	   if (!strcmp(rname, recordtable[i].recordname)) {
		/* already in table */
		if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, rname, i);
		entry = i;
		break;
	   }
        }	
    }

    /* found check */
    if (entry==-1) {
        if (recordtable[entry].ncallback == ns_maxprocs) { /* no room at the inn */
	     if(!daemon_mode) printf("No room left in the inn for new entry in [%s]. Have [%d] entries already\n", rname, recordtable[entry].ncallback);
	     return (ns_err_no_space);
        }


	/* find free space for the entry */

        if (lowest_record_entry_free!=-1) {
	    entry=lowest_record_entry_free;
	    /* find next free slot */
	    lowest_record_entry_free=-1; /* in case we don't find one */
	    if (nrecords!=(ns_maxrecords-1)) { /* we can search */
		if ((entry+1)<ns_maxrecords) /* if we can search */
		    for(i=entry+1;i<ns_maxrecords;i++)
			if (!recordtable[i].num_entries) {
			     lowest_record_entry_free = i; 
			     break;
			}
	    } /* if we search for a new free slot */
        } else {
	     for (i=0;i<ns_maxrecords;i++) {
		if (!recordtable[i].num_entries) { entry=i; break; }
	     }
        }
        if (recordtable[entry].num_entries==0) {              
	     strcpy(recordtable[entry].recordname, rname);
	     recordtable[entry].rnamelen = name_len;
	     nrecords ++;
	}
    }

    slot = -1;

    if (recordtable[entry].lowestcallback!=-1) {
	slot=recordtable[entry].lowestcallback;
	recordtable[entry].lowestcallback=-1; /* in case we don't find one */
	if (recordtable[entry].ncallback!=(ns_maxprocs-1)) { /* we can search */
	     if ((slot+1)<ns_maxprocs) {
		for(i=slot+1;i<ns_maxprocs;i++) {
		    if (recordtable[entry].callback[i].addr==-1) {
			recordtable[entry].lowestcallback = i; 
			break;
		    }
		} 
	     }
	}
    } else {
	for (i=0;i<ns_maxprocs;i++)  {
	    if (recordtable[entry].callback[i].addr==-1) { slot = i; break; }
	}
    }

    if (slot==-1) { 
	/* no room at the inn */
	if(!daemon_mode) printf("No slot left for new entry in [%s]. Have [%d] entries already???\n", rname, recordtable[entry].ncallback);
	return (ns_err_no_space);
    }

    upk_int32 (mbuf, &flag, 1);
    upk_raw32 (mbuf, &addr, 1);
    upk_int32 (mbuf, &port, 1);
    upk_int32 (mbuf, &tag, 1);
    upk_int32 (mbuf, &gencount, 1);
    upk_int32 (mbuf, &reqslot, 1);

    recordtable[entry].callback[slot].flag=flag;
    recordtable[entry].callback[slot].addr=addr;
    recordtable[entry].callback[slot].port=port;
    recordtable[entry].callback[slot].tag=tag;
    recordtable[entry].callback[slot].gencount=gencount;
    recordtable[entry].callback[slot].slot=reqslot;
    recordtable[entry].ncallback++;


    free_msg_buf(mbuf);

    if( (flag & EXIST) == EXIST) {

        if(reqslot!=ANYSLOT) { 

		/*
               printf("[1;33mREQ gencount is %d TABLE gencount is %d\n[0m",recordtable[entry].callback[slot].gencount,recordtable[entry].gencount); 
	       fflush(stdout);
	       */


            if (recordtable[entry].status[reqslot] && (recordtable[entry].callback[slot].gencount <= recordtable[entry].gencount) ) {


               recordtable[entry].nreaders++;
               mrc[0] = NS_PROTO_RDB_UPDATE_INFO;
               mrc[1] = tag; 
               mrc[2] = NS_PROTO_RDB_PUT;    /* operation */	                        
               mrc[3] = recordtable[entry].gencount;	
               mrc[4] = recordtable[entry].num_entries;	
               /* if we success reply internal, we return ns_err_other. */
               rmbuf = get_msg_buf (1);
               pk_byte (rmbuf, recordtable[entry].recordptr[reqslot], recordtable[entry].lengths[reqslot]);
               send_pkmesg (sockfd, NS_ID_SERVER_ANON, 5, mrc, rmbuf, 1);

	       if(!daemon_mode) { 

		       /*
                   printf("\n\n\n[1;32mBBBBB We report existing record data [entry %d slot %d] to host 0x%x port %d [0m \n\n",entry,reqslot,recordtable[entry].callback[slot].addr,recordtable[entry].callback[slot].port );
	           fflush(stdout); 
		   */

               }

               if((recordtable[entry].callback[slot].flag & AUTOUNREG) == AUTOUNREG) {  
                      record_callback_clean(entry,slot);
               }
               return ns_err_other;
	    } else { /* The record doesn't exist */
               return ns_err_none;
	    }
	} else {
            /* I will implement it later (in case of someone set exist flag and want information of all slot) */



	}
    }
    return ns_ok;
}

int record_callback_clean(int entry, int slot)
{
    recordtable[entry].callback[slot].flag = -1;
    recordtable[entry].callback[slot].addr = -1;
    recordtable[entry].callback[slot].port = -1;
    recordtable[entry].callback[slot].tag = -1;
    recordtable[entry].callback[slot].gencount = -1;
    recordtable[entry].callback[slot].slot = -1;
    recordtable[entry].ncallback--;

    if (recordtable[entry].lowestcallback > slot) {
        recordtable[entry].lowestcallback = slot; 
    }
    
    if (recordtable[entry].ncallback == 0) {
        recordtable[entry].lowestcallback = 0; 
    }

    if(recordtable[entry].num_entries==0) { /* No one in this room, return key to the inn */
	strcpy(recordtable[entry].recordname, "\0");	/* name first of all */
	recordtable[entry].rnamelen=0;
	recordtable[entry].num_entries = 0;	/* nuke the number of members */
	recordtable[entry].lowestslotfree = 0;
	recordtable[entry].nreaders =0;
	recordtable[entry].nwriters =0;
	recordtable[entry].gencount =0;
	/* update the count of records */
	nrecords --;
        if (lowest_record_entry_free > entry) {
            lowest_record_entry_free = entry; 
        }
        if(nrecords == 0) { /* we are last group */
            lowest_record_entry_free = 0;
        }
    }
    return ns_ok;
}


/* record_callback_unregister - register callback
@param mbuf message buffer
*/
int record_callback_unregister (int mbuf)
{
    int name_len;
    char rname[256];
    int i;
    int entry;
    int addr;
    int port;
    int tag;
    int slot;

    entry = -1;

    name_len = upk_string (mbuf, rname, sizeof (rname));

    if (!nrecords) {
	if(!daemon_mode) printf("No records in table to search for [%s]\n", rname);
	return (ns_err_none);
    }

    /* find the rname */
    for (i=0;i<ns_maxrecords;i++) {
	if (!strncmp(rname, recordtable[i].recordname, name_len)) {
		/* already in table */
		if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, rname, i);
		entry = i;
		break;
	}
    }	

    /* found check */
    if (entry==-1) {
	if(!daemon_mode) printf("Could not find [%s] out of [%d] entries out of a maximum of [%d]?\n", rname, nrecords, ns_maxrecords);
	return (ns_err_panic);
    }

    upk_raw32 (mbuf, &addr, 1);
    upk_int32 (mbuf, &port, 1);
    upk_int32 (mbuf, &tag, 1);

    slot=-1;
    
    for (i=0;i<ns_maxprocs;i++)  {
        if (recordtable[entry].callback[i].addr==addr &&
	    recordtable[entry].callback[i].port==port &&
            recordtable[entry].callback[i].tag==tag  ) 
	    { slot = i; break; }
    }

    if(slot==-1) {
	if(!daemon_mode) printf("Could not find callback addr[%s], port[%d], tag[%d]\n",ns_get_hostname(addr),port,tag);
	return (ns_err_panic);
    }

    record_callback_clean(entry,slot);
    return ns_ok;
}

/* record_delete - delete record
@param mbuf message buffer
*/
int record_delete (int mbuf)
{
    int entry;
    int i,j;
    char rname[256];

    entry = -1; 
    upk_string (mbuf, rname, sizeof (rname));

    if (!nrecords) {
       if(!daemon_mode) printf("Record_get: No records in table to search for [%s]\n", rname);
       return (ns_err_none);
     }

     /* find the rname */
     for (i=0;i<ns_maxrecords;i++) {
         if (!strcmp(rname, recordtable[i].recordname)) { /* its in table */
	    if(!daemon_mode) printf("%s:%d [%s] found in table at [%d]\n", __FILE__, __LINE__, rname, i);
	    entry = i;
	    break;
	 }
     }	

     if (entry==-1) {
	 if(!daemon_mode) printf("Could not find [%s] out of [%d] entries out of a maximum of [%d]?\n",
			rname, nrecords, ns_maxrecords);
	 return (ns_err_none);
     }

     /* Delete data & callback of the record  */
     for(i=0;i<recordtable[entry].num_entries;i++) {

	recordtable[entry].status[i] = 0;
	recordtable[entry].owners[i] = 0;	
	recordtable[entry].flags[i] = 0;	
	if (recordtable[entry].lengths[i]) { /* we have some memory to free up */
            if (recordtable[entry].recordptr[i]) {
		free (recordtable[entry].recordptr[i]);
	    }
	    recordtable[entry].lengths[i] = 0; /* it is now */
        } 
        
        for(j=0;j<recordtable[entry].ncallback;j++) {
            recordtable[entry].callback[j].flag = -1;
            recordtable[entry].callback[j].addr = -1;
            recordtable[entry].callback[j].port = -1;
            recordtable[entry].callback[j].tag = -1;
            recordtable[entry].callback[j].gencount = -1;
            recordtable[entry].callback[j].slot = -1;
	}
    }

     /* cleanup record */
     strcpy(recordtable[entry].recordname, "\0");	/* name first of all */
     recordtable[entry].rnamelen=0;
     recordtable[entry].num_entries = 0;	
     recordtable[entry].lowestslotfree = 0;
     recordtable[entry].nreaders =0;
     recordtable[entry].nwriters =0;
     recordtable[entry].ncallback= 0;
     recordtable[entry].lowestcallback = 0; 
     recordtable[entry].gencount =0;
     /* update the count of records */
     nrecords --;
     if (lowest_record_entry_free > entry) {
        lowest_record_entry_free = entry; 
     }
     if(nrecords == 0) { /* we are last group */
        lowest_record_entry_free = 0;
     }
     return ns_ok;
}
