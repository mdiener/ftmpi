

/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

	The Name Service code is derived from the Name Service
	Code that was developed for the benchmark run of linpeak(tm)
	on the ASCI Pacific Blue machine using MPI_Connect.

	The MPI_Connect project was supported in part by grants from 
	the DoD under the DOD MOD office PET program.

*/

#include<stdio.h>
#include<stdlib.h>
#include<stdlib.h>
#include "ns_lib.h" /* should have all we need right in here */


int main(int argc,char **argv)
{
int p;
int i;
char line[256];
char back[256];
int s;
int finished=0;
int num; 
int r, rc;	/* result codes */

char junk[1024*1024];
int count=0;
double d2, d1;

int i0, i1, i2, i3, i4, i5;

if (argc!=3) {
        printf("usage %s host port\n", argv[0] );
        exit (-1);
        }

    p = atoi(argv[2]);

printf ("ns_init %d\n", ns_init (argv[1], p));

r = ns_dump ( );/* get the server to dump all */

ns_close ();
exit (0);

  return 0;
}


