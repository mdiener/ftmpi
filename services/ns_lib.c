


/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

	The Name Service code is derived from the Name Service
	Code that was developed for the benchmark run of linpeak(tm)
	on the ASCI Pacific Blue machine using MPI_Connect.

	The MPI_Connect project was supported in part by grants from 
	the DoD under the DOD MOD office PET program.

*/

/*
    This is the second version of the 'nice' name service client side library.
    This version uses packed messages to make changes to the name service DB
    The name service now stores more than just host:port numbers.
    It can also store arbitary 'binary' data as well. This makes it a simple mbox.

    This library will only work with version 3 of the name service.

    GEF Stuttgart Dec 2001.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
#include"wincomm.h"
#else
#include<unistd.h>
#include"snipe_lite.h"
#endif
#include"msg.h"
#include"msgbuf.h"
#include"ns_lib.h"
#include"name_service.h"	/* for message hdr tags etc */


int ns_socket=0;
int ns_port;
char ns_host[256];


/* find and connect to the name service for the 'first' time */

int ns_init (char * hname,int port) 
{
/* store potential target info */
int s, p;
int i;
int j;


if (ns_socket) { /* we have it already open... err... */
	ns_close();
	ns_socket=0;
}

for (j=0;j<NS_INIT_SEARCH_ATTEMPTS;j++) {
	p = port;	/* always start with the indicated port */

	/* attempt to open connection with NS */
	s = getconn(hname, &p, NS_PORT_RANGE);

	if (s>0) { 
		/* else we have a NS server available */

		ns_socket=s;
		ns_port=p;
		strcpy (ns_host, hname);

		/* init message buffers used for sending and receiving */
		/* info from name service DB */
		i = init_msg_bufs ();
		if (i<=0) {
		   	puts ("init bufs bad"); 
			exit (-1); 
		}

		return (0);	/* found it! */
	} /* if opened */

	else 	/* we cannot find the NS so we loop as indicated */
#ifdef WIN32
    Sleep(1);
#else
		sleep (1);
#endif
		
} /* for attempts to get in contact with the NS service */

#ifdef VERBOSE
	printf("ns_init: connect failed.\n"); 
#endif /* VERBOSE */
ns_socket = 0;
ns_port = 0;
strcpy (ns_host, "none");
return (-1);
}

/* returns 0 if a server found, or -1 if not */
int	ns_open () 
{
int s; /* local socket */
int p;
int j;

if (ns_socket) return (0);	/* we are already connected */

p = ns_port;	/* same port I hope */

for (j=0;j<NS_OPEN_ATTEMPTS;j++) {
	s = getconn(ns_host, &p, 1);

	if (s>0) { 
		ns_socket=s;
		ns_port=p;
		return (0);
	}
#ifndef WIN32
	else /* no connection? back off a little and try again.. */
    usleep (NS_POLLWAIT);
#endif
}

#ifdef VERBOSE
	printf("ns_open: connect failed.\n"); 
#endif /* VERBOSE */
ns_socket = 0;
ns_port = 0;
return (-1);

}

/* shuts connection down */
void ns_close ()
{
int i0;
int ntag;
int from;
int rc[3];
if (ns_socket) {
	/* kill the connection */

/* Make message/request header */
i0 = NS_PROTO_GBYE;
/* no payload */

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, EMPTYMSGBUF, 0);

/* get the reply back */
ntag = 2;   /* we expect */
i0 = recv_pkmesg (ns_socket, &from, &ntag, rc, NULL); /* do the recv */

/* we should check the return values but what is the point? */
/* we are disconnecting anyway? */

#ifndef WIN32
usleep(NS_POLLWAIT); /* give TCP time fo FIN_WAIT etc */
#endif
closeconn(ns_socket);
ns_socket = 0;			/* forget the socket we used */
/* 	ns_port = 0; */
} /* if ns_socket */
}



/* adds me to the group named by gname */
int	ns_add (char * gname,int info1,int info2,int * group,int * member)
{
int mb;
int i0;		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

#ifdef VERBOSE
printf("Going to attempt to add group [%s] i1 0x%x i2 %d\n",
		gname, info1, info2);
#endif


/* Make message/request header */
i0 = NS_PROTO_JOIN;

mb = get_msg_buf (0);

if (mb<0) {
		fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	        dump_msg_bufs();
		*group = -1;
    	*member = -1;
    	return (-1);
}

pk_string (mb, gname);
pk_raw32 (mb, &info1, 1);	/* info1 is usually IP and NOT convertable */
pk_int32 (mb, &info2, 1);

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);	
/* end 1 = release buf after send */


/* get the reply back */
ntag = 3;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d at entry %d slot %d\n", rc[0], rc[1], rc[2]);
#endif

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);



if (ntag!=3) { /* bad message reply ? */
	*group = -1;
	*member = -1;
	return (-1);
	}

if (rc[0]!=NS_PROTO_JOIN) {	/* wrong reply? */
    return (-9);
	}


if (rc[1]>=0) { /* worked */
	*group = rc[1];
	*member = rc[2];
	return (0);
	}
else {	/* failed */
	*group = -1;
	*member = -1;
	return (-1);
	}

}

/* remove me for the group named by gname */
int	ns_del (char *gname,int group,int member)
{
int mbuf;
int r[3];	/* request tags */
int rc[3];  /* return codes */
int ntag;   /* # of tags we send/recv */
int from;   /* who we are talking too */



if (!ns_socket) return (-1);	/* not connected */


#ifdef VERBOSE
	printf("Going to attempt to delete from group [%s], entry %d slot %d\n",
			gname, group, member);
#endif


/* Make message/request header */
r[0] = NS_PROTO_RMLV;

/* get a message buffer */
mbuf = get_msg_buf (0);

if (mbuf<0) {
	fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mbuf);
	dump_msg_bufs();
    return (-1);
	}

/* Now form message */
pk_string (mbuf, gname);
pk_int32 (mbuf, &group, 1);
pk_int32 (mbuf, &member, 1);

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, r, mbuf, 1);
/* end 1 = release buf after send. We can now reuse mbuf */

/* get the reply back */
ntag = 3;   /* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mbuf); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d at entry %d slot %d\n", rc[0], rc[1], rc[2]);
#endif 

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mbuf>=0) free_msg_buf (mbuf);

if (ntag!=3) { /* bad message reply ? */
    return (-1);
	}

if (rc[0]!=NS_PROTO_RMLV) {	/* wrong reply? */
    return (-9);
	}

if ((rc[1]>=0)&&(rc[2]>=0)) { /* worked */
	return (0);
	}
else {	/* failed */
	return (-1);
	}

}

/* get info for the group named by gname */
/* return me upto rsize element details */
/* return code is the number of entries returned or -1 for error */
/* if you allocated no space, then 0 is a valid return */
/* entries, info1 and 2 can be NULL, the others cannot */
int	ns_info (char *gname,int rsize,int * group,int * nmembers,int * entries,int * info1s,int * info2s)
{
int i;
int e;
int ne;
int toreturn;
int succ;
int tmp;
int i0;
int mb;
int ntag;
int rc[3];
int from;


if (!ns_socket) return (-1);	/* not connected */

#ifdef VERBOSE
	printf("Going to attempt to get group info [%s] upto %d entries\n",
			gname, rsize);
#endif


/* Make message/request header */
i0 = NS_PROTO_INFO;

mb = get_msg_buf (0);

if (mb<0) {
	fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
        *nmembers = -1;
        return (-1);
}

/* Now form message */
pk_string (mb, gname);		/* they just need the group name and */
pk_int32 (mb, &rsize, 1);	/* how many entries we want back */

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);
/* end 1 = release buf after send */

/* get the reply back */
ntag = 3;   /* we expect */
i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d proto %d with %d entries\n", rc[0], rc[1], rc[2]);
#endif


if (ntag!=3) { /* bad message reply ? */
    *nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (-1);
    }

/* other paranoid checks */
/* rc[0] = proto tag, rc[1] = ok/failed and rc[2] = n entries sent to us */

if (rc[0]!=NS_PROTO_INFO) { /* wrong message tag ? */
    *nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (-1);
    }


succ = rc[1]; 
ne   = rc[2];

if ((succ<0)||(ne<0)) { /* check for fail, neg returns */
	*nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
	return (-1);
	}

if (succ==0) { /* worked */
	/* unpack rest of details */

	upk_int32 (mb, &e, 1); /* acutally this is the entry number */
	upk_int32 (mb, &toreturn, 1);	/* how many entries in this message */

#ifdef VERBOSE
	printf("ns_info: group %d entries received %d\n", e, toreturn);
#endif

	*nmembers = ne;	/* return the # of members even if we do not pass the data back later */

	if (group) /* i.e. they want the group info */
		*group = e; /* return it to them (its the NS entry number) */
					/* don't confuse this with entries which is the NS slots! */


	if ((toreturn<0)||(toreturn > rsize)) {	/* if we get an invalid # of returns! */
	        *nmembers = -1;
		if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
		return (-1);
		}

	for (i=0;i<toreturn;i++) {	/* unpack all  */
								/* only if we have memory to unpack them to !!! */

		if (entries) upk_int32 (mb, &(entries[i]), 1);
		else 
			upk_int32 (mb, &tmp, 1);

		if (info1s)
			upk_raw32 (mb, &(info1s[i]), 1);	/* binary not an int */
		else 
			upk_raw32 (mb, &tmp, 1);

		if (info2s)
			upk_int32 (mb, &(info2s[i]), 1);
		else 
			upk_int32 (mb, &tmp, 1);
	
		}	/* for all entries */	
	
	free_msg_buf (mb);	/* free the message buffer at the end */

	return (toreturn);
	
	} /* if success */

else {	/* failed */
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
	*nmembers = -1;
	return (-1);
	}

}

/* dumps the name servers table contents AT the name server only */
/* returns number of entry types that exist with unique names */
int	ns_dump ()
{
int mb;

int i0;		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

#ifdef VERBOSE
	printf("Going to attempt to dump group info at the name server\n");
#endif


/* Make message/request header */
i0 = NS_PROTO_DUMP;

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, EMPTYMSGBUF, 0);	
/* end 0 = as no buffer to free after the send */

/* get the reply back */
ntag = 2;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d for entries %d\n", rc[0], rc[1]);
#endif

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);

if (ntag!=2) { /* bad message reply ? */
	return (-1);
	}

if (rc[0]!=NS_PROTO_DUMP) {	/* wrong reply? */
    return (-9);
	}

if (rc[1]>=0) { /* worked */
	return (rc[1]);
	}
else {	/* failed */
	return (-1);
	}

}

/* do a NS echo and timing test :) */
int ns_echo (double *timeresult)
{
int i0;
int ntag;
int from;
int rc[3];
int mb;
double ot, st, et;
int flip;

if (!ns_socket) return (-1);	/* not connected */

if (timeresult) ot = sec_time();	/* just to warm up the routine :) */
else ot=3.14159;	/* as we use ot as a echo checker value */

/* initialize */
st = 0.0;
et = 0.0;

/* Make message/request header */
i0 = NS_PROTO_ECHO;

mb = get_msg_buf (0);

if (mb<0) {
	fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
        return (-1);
}

/* Now form message */
flip = (int) ot;
pk_int32 (mb, &flip, 1);   /* some number ? */

if (timeresult) st = sec_time();	/* the real start time */

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);

/* get the reply back */
ntag = 2;   /* we expect */
i0 = recv_pkmesg (ns_socket, &from, &ntag, rc, NULL); /* do the recv */

if (mb>=0) free_msg_buf (mb);

if (timeresult) et = sec_time();	/* the real end time */

if (rc[0]!=NS_PROTO_ECHO) return (-1);	/* bad echo */

if (rc[1]!=(flip)) return (-2);	/* bad echo check value */

if (timeresult) {
	ot = et - st;	/* actual echo time with all the message overheads well */
	*timeresult = ot;	/* give it back */
	}

return (ns_ok);
}

/* puts a record into the NS Record DB (NS RDB) */
/* the record MUST be in a message buf already! */
int	ns_record_put (int me,char * rname,int rmb,int flags,int reqslot,int * record,int * slot,int delaftersend)
{
int mb;
int valid;	/* is record valid */
int rlen;	/* record len */
char* buf_ptr;	/* data the buffer contains */
int i0;		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

if (me<=0) me = NS_ID_CLIENT_ANON;

/* check args */
/* the buffer */

if (rmb != EMPTYMSGBUF) valid = check_buf (rmb);
else valid = 1;
if (!valid) {
	printf("ns_lib:record_put bad buffer specified (rmb=%d)\n", rmb);
	return (ns_err_bad_arg);
	}

if (
	(!flags&RECORD_DB_DEFAULT)&&(!flags&RECORD_DB_SINGLEONLY)&&
    (!flags&RECORD_DB_READONLY)&&(!flags&RECORD_DB_PRIVATE)&&
	(!flags&RECORD_DB_INDEXED)&&(!flags&RECORD_DB_IFEMPTY)
	) {
    printf("ns_lib:record_put request was with a bad flag arg of %d\n", flags);
    return (ns_err_bad_arg);
  }

#ifdef VERBOSE
printf("Going to attempt to add record [%s] flags 0x%x mb %d\n",
		rname, flags, rmb);
#endif


/* Make message/request header */
i0 = NS_PROTO_RDB_PUT;

mb = get_msg_buf (1);

if (mb<0) {
	        fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
		*record = -1;
    	*slot = -1;
    	return (-1);
}

pk_string (mb, rname);
pk_int32 (mb, &flags, 1);
pk_int32 (mb, &reqslot, 1);

/* OK now to pack the record. NOTE empty messages are allowed */
/* only if rmb is set to EMPTYMSGBUF */

if (rmb != EMPTYMSGBUF) {
	get_msg_buf_info(rmb,&buf_ptr,&rlen); /* get buffer info */
}
else {
	buf_ptr = NULL;                         /* if empty message */
	rlen = 0;
}

pk_int32 (mb, &rlen, 1);

if (rlen) { 
  if(pk_byte(mb, buf_ptr, rlen)==0) {
      fprintf(stderr,"[%s:%d] pk_byte failed !!\n", __FILE__,__LINE__);
      return -1;
  }
}

/* request */
send_pkmesg (ns_socket, me, 1, &i0, mb, 1);	
/* end 1 = release buf after send */
/* that way we can reuse mb */

if ((rmb!=EMPTYMSGBUF) &&(delaftersend)) { /* we are asked to delete the message after a put */
								/* NOTE we are not sure it worked. TODO XXX maybe */
	free_msg_buf (rmb);
	}


/* get the reply back */
ntag = 3;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	 printf("Operation returned code %d at entry %d slot %d\n", rc[0], rc[1], rc[2]);
#endif

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);

if (ntag!=3) { /* bad message reply ? */
	*record = -1;
	*slot = -1;
	return (-1);
	}

if (rc[0]!=NS_PROTO_RDB_PUT) {	/* wrong reply? */
    return (-9);
	}


if (rc[1]>=0) { /* worked */
	*record = rc[1];
	*slot = rc[2];
	return (0);
	}
else {	/* failed */
	*record = -1;
	*slot = -1;
	return (-1);
	}

}

/* swap a record in the NS Record DB (NS RDB) */
/* the record MUST be in a message buf already! */
/* the target of the SWAP MUST already be in the DB */
/* The swap happen if DB record matched the previous_owner arg */
/* This call is used to implement an atomic election algorithm */
int	ns_record_swap (int me,char * rname,int rmb,int flags,int reqslot,int previous_owner,int * record,int * slot,int delaftersend)
{
int mb;
int valid;	/* is record valid */
int rlen;	/* record len */
char* buf_ptr;	/* data the buffer contains */
int i0;		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

if (me<=0) me = NS_ID_CLIENT_ANON;
if (previous_owner<=0) previous_owner = NS_ID_CLIENT_ANON;	/* so u can use 0 */

/* check args */
/* the buffer */

if (rmb != EMPTYMSGBUF) valid = check_buf (rmb);
else valid = 1;
if (!valid) {
	printf("ns_lib:record_swap bad buffer specified (rmb=%d)\n", rmb);
	return (ns_err_bad_arg);
	}

if (flags!=RECORD_DB_INDEXED) {
    printf("ns_lib:record_swap request without RECORD_DB_INDEXED flag%d\n", flags);
    return (ns_err_bad_arg);
  }

#ifdef VERBOSE
printf("Going to attempt to swap record [%s] flags 0x%x mb %d previous 0x%x\n",
		rname, flags, rmb, previous_owner);
#endif


/* Make message/request header */
i0 = NS_PROTO_RDB_SWAP;

mb = get_msg_buf (0);

if (mb<0) {
	        fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
		*record = -1;
    	*slot = -1;
    	return (-1);
}

pk_string (mb, rname);
pk_int32 (mb, &flags, 1);
pk_int32 (mb, &reqslot, 1);
pk_int32 (mb, &previous_owner, 1);

/* OK now to pack the record. NOTE empty messages are allowed */
/* only if rmb is set to EMPTYMSGBUF */

if (rmb != EMPTYMSGBUF)
	get_msg_buf_info(rmb,&buf_ptr,&rlen); /* get buffer info */
else {
	buf_ptr = NULL;                         /* if empty message */
	rlen = 0;
}

pk_int32 (mb, &rlen, 1);

if (rlen) pk_byte (mb, buf_ptr, rlen);		/* pack record if its NOT EMPTY */

/* request */
send_pkmesg (ns_socket, me, 1, &i0, mb, 1);	
/* end 1 = release buf after send */
/* that way we can reuse mb */

if ((rmb!=EMPTYMSGBUF) &&(delaftersend)) { /* we are asked to delete the message after a put */
								/* NOTE we are not sure it worked. TODO XXX maybe */
	free_msg_buf (rmb);
	}


/* get the reply back */
ntag = 3;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	 printf("Operation returned code %d at entry %d slot %d\n", rc[0], rc[1], rc[2]);
#endif

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);

if (ntag!=3) { /* bad message reply ? */
	*record = -1;
	*slot = -1;
	return (-1);
	}

if (rc[0]!=NS_PROTO_RDB_SWAP) {	/* wrong reply? */
    return (-9);
	}


if (rc[1]>=0) { /* worked */
	*record = rc[1];
	*slot = rc[2];
	return (0);
	}
else {	/* failed */
	*record = -1;
	*slot = -1;
	return (-1);
	}

}

/* get detailed info for the record named by rname */
/* return me upto rsize element details */
/* return code is the number of entries returned or -1 for error */
/* if you allocated no space, then 0 is a valid return */
/* entries and rlens can be NULL, the others cannot */

int	ns_record_info (int me,char * rname,int rsize,int * record,int * nmembers,int * entries,int * rlens)
{
int i;
int e;
int ne;
int toreturn;
int succ;
int tmp;
int i0;
int mb;
int ntag;
int rc[3];
int from;


if (!ns_socket) return (-1);	/* not connected */

if (me<=0) me = NS_ID_CLIENT_ANON;

#ifdef VERBOSE
	printf("Going to attempt to get record info [%s] upto %d entries\n",
			rname, rsize);
#endif


/* Make message/request header */
i0 = NS_PROTO_RDB_INFO;

mb = get_msg_buf (0);

if (mb<0) {
	fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
        *nmembers = -1;
        return (-1);
}

/* Now form message */
pk_string (mb, rname);		/* they just need the group name and */
pk_int32 (mb, &rsize, 1);	/* how many entries we want back */

/* request */
send_pkmesg (ns_socket, me, 1, &i0, mb, 1);
/* end 1 = release buf after send */

/* get the reply back */
ntag = 3;   /* we expect */
i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d proto %d with %d entries\n", rc[0], rc[1], rc[2]);
#endif


if (ntag!=3) { /* bad message reply ? */
    *nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (-1);
    }

/* other paranoid checks */
/* rc[0] = proto tag, rc[1] = ok/failed and rc[2] = n entries sent to us */

if (rc[0]!=NS_PROTO_RDB_INFO) { /* wrong message tag ? */
    *nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (-1);
    }


succ = rc[1]; 
ne   = rc[2];

if ((succ<0)||(ne<0)) { /* check for fail, neg returns */
	*nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
	return (-1);
	}

if (succ==0) { /* worked */
	/* unpack rest of details */

	upk_int32 (mb, &e, 1); /* acutally this is the entry number */
	upk_int32 (mb, &toreturn, 1);	/* how many entries in this message */

#ifdef VERBOSE
	printf("ns_record_info: record %d entries received %d\n", e, toreturn);
#endif

	*nmembers = ne;	/* return the # of members even if we do not pass the data back later */

	if (record) /* i.e. they want the record info */
		*record = e; /* return it to them (its the NS entry number) */
					/* don't confuse this with entries which is the NS slots! */


	if ((toreturn<0)||(toreturn > rsize)) {	/* if we get an invalid # of returns! */
	        *nmembers = -1;
		if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
		return (-1);
		}

	for (i=0;i<toreturn;i++) {	/* unpack all  */
								/* only if we have memory to unpack them to !!! */

		if (entries) upk_int32 (mb, &(entries[i]), 1);
		else 
			upk_int32 (mb, &tmp, 1);
		if (rlens)
			upk_int32 (mb, &(rlens[i]), 1);
		else 
			upk_int32 (mb, &tmp, 1);
	
		}	/* for all entries */	
	
	free_msg_buf (mb);	/* free the message buffer at the end */

	return (toreturn);
	
	} /* if success */

else {	/* failed */
	*nmembers = -1;
	if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
	return (-1);
	}

}


/* gets a record from the NS Record DB (NS RDB) */
/* the record is returned in a message buf */
/* the user is required to free it as needed */
/* the return code is the length of the packed message (if any, so 0 is valid) <0 = err */

int	ns_record_get (int me,char * rname,int flags,int reqslot,int * record,int * slot,int * rmb, int *gencount)
{
int mb;
int rlen;	/* record len */
int i0;		/* outgoing message header */
int rc[4];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

if (me<=0) me = NS_ID_CLIENT_ANON;

/* check args */

if (
	(!flags&RECORD_DB_DEFAULT)&&(!flags&RECORD_DB_SINGLEONLY)&&
    (!flags&RECORD_DB_READONLY)&&(!flags&RECORD_DB_PRIVATE)&&
	(!flags&RECORD_DB_INDEXED)&&(!flags&RECORD_DB_IFEMPTY)&&
	(!flags&RECORD_DB_REMOVE)
	) {
    printf("ns_lib:record_get request was with a bad flag arg of %d\n", flags);
    return (ns_err_bad_arg);
  }

#ifdef VERBOSE
printf("Going to attempt to get record [%s] flags 0x%x index %d\n",
		rname, flags, reqslot);
#endif


/* Make message/request header */
i0 = NS_PROTO_RDB_GET;

mb = get_msg_buf (0);

if (mb<0) {
	        fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
	dump_msg_bufs();
		if (record) *record = -1;
    	if (slot) *slot = -1;
		if (rmb) *rmb = -1;
		if (gencount) *gencount = -1;
    	return (-1);
}

pk_string (mb, rname);
pk_int32 (mb, &flags, 1);
pk_int32 (mb, &reqslot, 1);

/* OK now to pack the record. */

/* request */
send_pkmesg (ns_socket, me, 1, &i0, mb, 1);	
/* we free mb, so can use it to receive etc etc */

/* get the reply back */
ntag = 4;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	 printf("Operation returned code %d at entry %d slot %d gencount %d\n", rc[0], rc[1], rc[2], rc[3]);
#endif


if (ntag!=4) { /* bad message reply ? */
#ifdef VERBOSE
	puts ("ns_record_get: bad reply from NS");
#endif
	if (record) *record = -1;
    if (slot) *slot = -1;
	if (rmb) *rmb = -1;
	if (gencount) *gencount = -1;
	if (mb>=0) free_msg_buf (mb);	/* as not needed anymore */
	return (-1);
	}

if (rc[0]!=NS_PROTO_RDB_GET) {	/* wrong reply? */
#ifdef VERBOSE
	puts ("ns_record_get: wrong reply from NS");
#endif
	if (record) *record = -1;
    if (slot) *slot = -1;
	if (rmb) *rmb = -1;
	if (gencount) *gencount = -1;
	if (mb>=0) free_msg_buf (mb);	/* as not needed anymore */
    return (-9);
	}

if (rc[1]>=0) { /* worked */
	if (record) *record = rc[1];
	if (slot) *slot = rc[2];
	if (gencount) *gencount = rc[3];
	upk_int32 (mb, &rlen, 1);	/* get the records message length */
	/* ok, we return the mb to the user now (if they want it) */
	if (rmb) *rmb = mb;
	else 
		if (mb>=0) free_msg_buf (mb); /* they don't want it, free it */
	return (rlen);	/* they get the returned record data length */
	}
else {	/* failed */
#ifdef VERBOSE
	puts ("ns_record_get: bad data from NS");
#endif
	if (record) *record = -1;
        if (slot) *slot = -1;
	if (rmb) *rmb = -1;
	if (gencount) *gencount = -1;
	if (mb>=0) free_msg_buf (mb);	/* as not needed anymore */
	return (-1);
	}

} /* end of record_get */

int	ns_gid (int index, int range, int *gids)
{
int i;
int mb;
int k;

int out[3];		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */
int obtained;
int start;
int end;
int basegid;
int endgid; 


if (!ns_socket) return (-1);	/* not connected */

#ifdef VERBOSE
	printf("Going to attempt to get GID with index %d\n", index);
#endif

if ((index<0)||(index>=_NS_ID_END) || range < 1 || gids==NULL ) return (ns_err_bad_arg);

/* Make message/request header */
out[0] = NS_PROTO_GID;
out[1] = index;
out[2] = range;

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 3, out, EMPTYMSGBUF, 0);	
/* end 0 = as no buffer to free after the send */

/* get the reply back */
ntag = 2;	/* we expect */
rc[0] = 0; rc[1] = 0;
i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

if (ntag!=2) { /* bad message reply ? */
    if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (ns_err_other);
}

if (rc[0]!=NS_PROTO_GID) {	/* wrong reply? */
    if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (ns_err_other);
}

obtained=rc[1];

if (obtained<0 || obtained > range) {
    if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (ns_err_other);
}

upk_int32 (mb, &start, 1);
upk_int32 (mb, &end, 1);


if(end<start) { /* roll over */
   upk_int32 (mb, &basegid, 1);
   upk_int32 (mb, &endgid, 1);
   k=start;
   for(i=0;i<obtained;i++) {
      gids[i]=k;
      if(k==endgid) {
          k=basegid;
      } else {
          k++; 
      }
   }
} else {
   k=start;
   for(i=0;i<obtained;i++) {
       gids[i]=k;
       k++;
   }
}

if (mb>=0) free_msg_buf (mb);

return obtained;

}


/* dumps the name servers table contents AT the name server only (dump everything) */
/* returns number of entry types that exist with unique names */
int	ns_dumpall ()
{
int mb;

int i0;		/* outgoing message header */
int rc[3];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

#ifdef VERBOSE
	printf("Going to attempt to dump group info at the name server\n");
#endif


/* Make message/request header */
i0 = NS_PROTO_DUMPALL;

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, EMPTYMSGBUF, 0);	
/* end 0 = as no buffer to free after the send */

/* get the reply back */
ntag = 2;	/* we expect */
(void)recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

#ifdef VERBOSE
	printf("Operation returned code %d for entries %d\n", rc[0], rc[1]);
#endif

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);

if (ntag!=2) { /* bad message reply ? */
	return (-1);
	}

if (rc[0]!=NS_PROTO_DUMPALL) {	/* wrong reply? */
    return (-9);
	}

if (rc[1]>=0) { /* worked */
	return (rc[1]);
	}
else {	/* failed */
	return (-1);
	}

}

/* ns_record_callback_register - register callback
@param flag flag (DEFAULT, AUTOUNREG)
@param addr address
@param port port number
@param recordname record name
@param slot slot  (use ANYSLOT for any slot)
@param tag tag
@param genCount generation counter
@retval >0 message buf
@retval <0 error
@retval 0 message buf (if EXIST flag set)
@retval 0 ok (if no EXIST flag set)
*/
int ns_record_callback_register (int flag, int addr, int port, char *recordName,int slot, int tag,int genCount )
{
int i;
int mb;
int i0;          /* header */
int rc[5];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (-1);	/* not connected */

/* Make message/request header */
i0 = NS_PROTO_RDB_CALLBACK_REG;
mb = get_msg_buf (0);

if (mb<0) {
    	return (-1);
}

pk_string (mb, recordName);
pk_int32 (mb, &flag, 1);
pk_raw32 (mb, &addr, 1);
pk_int32 (mb, &port, 1);
pk_int32 (mb, &tag, 1);
pk_int32 (mb, &genCount, 1);
pk_int32 (mb, &slot, 1);

send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);	

/* get the reply back */
ntag = 5;	/* we expect */
for(i=0;i<ntag;i++) rc[i]=0;

i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

if (ntag!=5) { /* bad message reply ? */
    if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (ns_err_other);
}

if (rc[0]==NS_PROTO_RDB_CALLBACK_REG) {
    if (mb>=0) free_msg_buf (mb); 
    return rc[1];
} else if(rc[0]==NS_PROTO_RDB_UPDATE_INFO) {
    if( (rc[1]!=tag) || ((flag & EXIST) != EXIST) ) {
        return (ns_err_other);
    }
    /*
    printf("[1;36mWe are returning mb %d\n",mb); fflush(stdout);
    */
    return mb;
} else {
    if (mb>=0) free_msg_buf (mb); /* on error don't keep memory locked in buffers */
    return (ns_err_other);
}

/* should not reach here */
return 0;
}

/* ns_record_callback_unregister - unregister callback
@param addr address
@param port port number
@param recordname record name
@param tag tag
*/
int ns_record_callback_unregister (int addr, int port, char *recordName,int tag)
{
int i;
int mb;
int i0;          /* header */
int rc[2];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */

if (!ns_socket) return (-1);	/* not connected */

/* Make message/request header */
i0 = NS_PROTO_RDB_CALLBACK_UNREG;
mb = get_msg_buf (0);

if (mb<0) {
    	return (-1);
}

pk_string (mb, recordName);
pk_raw32 (mb, &addr, 1);
pk_int32 (mb, &port, 1);
pk_int32 (mb, &tag, 1);

send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);	

/* get the reply back */
ntag = 2;	/* we expect */

for(i=0;i<ntag;i++) rc[i]=0;

i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

if (mb>=0) free_msg_buf (mb); 

if ((ntag!=2) || (rc[0]!=NS_PROTO_RDB_CALLBACK_UNREG)) {	
    return (ns_err_other);
}

return rc[1];
}

/** ns_record_del - Delete the record (delete everything in the record. Don't care any flag)
@param rname record name
@return error number
*/
int ns_record_del (char * rname)
{

    int mb;
    int i0;
    int rc[2];
    int i;
    int ntag;
    int from;

    i0 = NS_PROTO_RDB_DELETE;
    mb = get_msg_buf (0);

    if (mb<0) {
    	return (-1);
    }

    pk_string (mb, rname);
    send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);	

    /* get the reply back */
    ntag = 2;	/* we expect */
    for(i=0;i<ntag;i++) rc[i]=0;

    i = recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */
    if (mb>=0) free_msg_buf (mb); 
    if ((ntag!=2) || (rc[0]!=NS_PROTO_RDB_DELETE)) {	
       return (ns_err_other);
    }
    return rc[1];
}

/** ns_ismember - check an existance of entry in a group 
@param gname group name
@param ip ip
@param port port
@return 1 yes (i.e. ip&port is a member of the group)
@return 0 no (i.e. ip&port is not a member of the group)
*/
int ns_ismember(char *gname,int ip,int port)
{
int mb;
int i0;		/* outgoing message header */
int rc[2];	/* return codes */
int ntag;	/* # of tags we send/recv */
int from;	/* who we are talking too */


if (!ns_socket) return (0);   /* not connected */

#ifdef VERBOSE
printf("Checking IP  0x%x port %d in group [%s]\n",ip ,port, gname) ;
#endif

/* Make message/request header */
i0 = NS_PROTO_ISMEMBER;
mb = get_msg_buf (0);

if (mb<0) {
	fprintf(stderr,"[%s:%d] get_msg_buf bufs failed with %d\n", __FILE__,__LINE__,mb);
        dump_msg_bufs();
    	return (0);
}

pk_string (mb, gname);
pk_raw32 (mb, &ip, 1);	
pk_int32 (mb, &port, 1);

/* request */
send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, mb, 1);	

/* get the reply back */
ntag = 2;	/* we expect */
recv_pkmesg (ns_socket, &from, &ntag, rc, &mb); /* do the recv */

/* we don't need the message buffer, so free it if it is valid (just in case it has data!) */
if (mb>=0) free_msg_buf (mb);

if (ntag!=2) { /* bad message reply ? */
#ifdef VERBOSE
	fprintf(stderr,"[%s:%d] WRONG protocol!!. We got ntag %d instead of 2\n", __FILE__,__LINE__,ntag);
#endif
	return (0);
}

if (rc[0]!=NS_PROTO_ISMEMBER) {	/* wrong reply? */
#ifdef VERBOSE
	fprintf(stderr,"[%s:%d] WRONG protocol!!. We got protocol number %d instead of %d\n", __FILE__,__LINE__,rc[0],NS_PROTO_ISMEMBER);
#endif
	return (0);
}
#ifdef VERBOSE
	printf("Operation returned code %d \n", rc[1]);
#endif
	return rc[1];
}

/* 
ns_halt - Halt the name service
*/
int ns_halt (void)
{
   int i0;		/* outgoing message header */
   if (!ns_socket) return (-1);	/* not connected */
   /* Make message/request header */
   i0 = NS_PROTO_HALT;
   /* request */
   send_pkmesg (ns_socket, NS_ID_CLIENT_ANON, 1, &i0, EMPTYMSGBUF, 0);	
   return 0;
}
