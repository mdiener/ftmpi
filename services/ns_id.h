
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS NAME SERVICE IDs

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* GID base values. */
/* this is used by name_service AND ft-mpi-sys.c */
/* hense its in here */

#define NS_ID_CORE_BASE 	0x0100
#define NS_ID_FTMPI_BASE 	0x1000
#define NS_ID_OTHER_BASE 	0x5000
#define NS_ID_FTMPI_COMS_BASE 100 	/* non hex easy human readable */
#define _NS_ID_END_BASE 	0x0FFF


#define NS_ID_CORE_END 		0xEFFF
#define NS_ID_FTMPI_END 	0x4FFF
#define NS_ID_OTHER_END 	0x9FFF
#define NS_ID_FTMPI_COMS_END 29999 	/* non hex easy human readable */
#define _NS_ID_END_END 		0x0FFF


