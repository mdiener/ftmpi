dnl Get the format of Fortran names.  Uses F77, FFLAGS, and sets WDEF.
dnl If the test fails, sets NOF77 to 1, HAS_FORTRAN to 0
dnl
define(PAC_GET_FORTNAMES,[
   # Check for strange behavior of Fortran.  For example, some FreeBSD
   # systems use f2c to implement f77, and the version of f2c that they 
   # use generates TWO (!!!) trailing underscores
   # Currently, WDEF is not used but could be...
   #
   # Eventually, we want to be able to override the choices here and
   # force a particular form.  This is particularly useful in systems
   # where a Fortran compiler option is used to force a particular
   # external name format (rs6000 xlf, for example).
   cat > confftest.f <<EOF
       subroutine mpir_init_fop( a )
       integer a
       a = 1
       return
       end
EOF
   $F77 $FFLAGS -c confftest.f > /dev/null 2>&1
   if test ! -s confftest.o ; then
        print_error "Unable to test Fortran compiler"
        print_error "(compiling a test program failed to produce an "
        print_error "object file)."
	NOF77=1
        HAS_FORTRAN=0
   elif test -z "$FORTRANNAMES" ; then
    # We have to be careful here, since the name may occur in several
    # forms.  We try to handle this by testing for several forms
    # directly.
    if test $arch_CRAY ; then
     # Cray doesn't accept -a ...
     nameform1=`strings confftest.o | grep mpir_init_fop_  | sed -n -e '1p'`
     nameform2=`strings confftest.o | grep MPIR_INIT_FOP   | sed -n -e '1p'`
     nameform3=`strings confftest.o | grep mpir_init_fop   | sed -n -e '1p'`
     nameform4=`strings confftest.o | grep mpir_init_fop__ | sed -n -e '1p'`
    else
     nameform1=`strings -a confftest.o | grep mpir_init_fop_  | sed -n -e '1p'`
     nameform2=`strings -a confftest.o | grep MPIR_INIT_FOP   | sed -n -e '1p'`
     nameform3=`strings -a confftest.o | grep mpir_init_fop   | sed -n -e '1p'`
     nameform4=`strings -a confftest.o | grep mpir_init_fop__ | sed -n -e '1p'`
    fi
    /bin/rm -f confftest.f confftest.o
    if test -n "$nameform4" ; then
	echo "Fortran externals are lower case and have 1 or 2 trailing underscores"
	FORTRANNAMES="FORTRANDOUBLEUNDERSCORE"
	AC_DEFINE(FORTRANDOUBLEUNDERSCORE)
    elif test -n "$nameform1" ; then
        # We don't set this in CFLAGS; it is a default case
        echo "Fortran externals have a trailing underscore and are lowercase"
	FORTRANNAMES="FORTRANUNDERSCORE"
	AC_DEFINE(FORTRANUNDERSCORE)
    elif test -n "$nameform2" ; then
	echo "Fortran externals are uppercase"     
	FORTRANNAMES="FORTRANCAPS" 
	AC_DEFINE(FORTRANCAPS)
    elif test -n "$nameform3" ; then
	echo "Fortran externals are lower case"
	FORTRANNAMES="FORTRANNOUNDERSCORE"
	AC_DEFINE(FORTRANNOUNDERSCORE)
    else
	print_error "Unable to determine the form of Fortran external names"
	print_error "Make sure that the compiler $F77 can be run on this system"
	NOF77=1
        HAS_FORTRAN=0
    fi
    fi

dnl    if test -n "$FORTRANNAMES" ; then
dnl        WDEF="-D$FORTRANNAMES"
dnl    fi
    ])dnl
dnl



dnl# Like AC_PROG_CC, but for Fortran (FC)


dnl PAC_TEST_PROGRAM is like AC_TEST_PROGRAM, except that it makes it easier
dnl to find out what failed.
dnl
define(PAC_TEST_PROGRAM,
[AC_PROVIDE([$0])
dnl AC_REQUIRE([AC_CROSS_CHECK])
if test "$cross_compiling" = 1 -a -z "$TESTCC" ; then
    ifelse([$4], , ,$4)
    Pac_CV_NAME=0
    print_error " Sh.. something went wrong"
else
    if test -n "$TESTCC" ; then
      CCsav="$CC"
      CC="$TESTCC"
    fi
    cat > conftest.c <<EOF
#include "confdefs.h"
[$1]
EOF
    eval $compile
    if test ! -s conftest ; then
      echo "Could not build executable program:"
      echo "${CC-cc} $CFLAGS conftest.c -o conftest $LIBS"
      ${CC-cc} $CFLAGS conftest.c -o conftest $LIBS 
    ifelse([$3], , , [$3
])
    else
      /bin/rm -f conftestout
      if test -s conftest && (./conftest; exit) 2>conftestout; then
          ifelse([$2], , :, [$2
])
      else
	if test [$2] != "MMAP=1"; then
            echo "Execution of test program failed"
	fi
        ifelse([$3], , , [$3
])
        if test -s conftestout ; then
            cat conftestout
        fi
      fi
    fi
  if test -n "$TESTCC" ; then
        CC="$CCsav"
  fi
  dnl rm -fr conftest*
fi
])dnl
dnl



dnl
dnl PAC_FORTRAN_GET_SIZE(var_for_size)
dnl Second version adapted to caching
dnl
dnl sets var_for_size to the size.  Ignores if the size cannot be determined
dnl
define(PAC_FORTRAN_GET_SIZE,
[
/bin/rm -f conftestval
/bin/rm -f conftestf.f conftestf.o
cat <<EOF > conftestf.f
      subroutine isize( )
      $2 i(2)
      call cisize( i(1), i(2) )
      end
EOF
if $F77 $FFLAGS -c conftestf.f >/dev/null 2>&1 ; then 
    SaveLIBS="$LIBS"
    LIBS="conftestf.o $LIBS"
    PAC_TEST_PROGRAM([#include <stdio.h>
#ifdef FORTRANCAPS
#define cisize_ CISIZE
#define isize_  ISIZE
#elif defined(FORTRANNOUNDERSCORE)
#define cisize_ cisize
#define isize_  isize
#endif
static int isize_val;
void cisize_( i1p, i2p )
char *i1p, *i2p;
{
	isize_val = (i2p - i1p) * sizeof(char);
}
main() { 
  FILE *f=fopen("conftestval","w");
  
  if (!f) exit(1);
  isize_();
  fprintf( f, "%d\n", isize_val);
  exit(0);
}],Pac_CV_NAME=`cat conftestval`,Pac_CV_NAME="")
LIBS="$SaveLIBS"
else
   :
fi
if test -z "$Pac_CV_NAME" ; then
    # Try to compile/link with the Fortran compiler instead.  This
    # worked for the NEC SX-4
    compile_f='${CC-cc} $CFLAGS -c conftest.c; ${F77-f77} $FFLAGS -o conftest conftest.o conftestf.o $LIBS >/dev/null 2>&1'
    echo " try to run $compile_f"
    eval $compile_f
    if test ! -s conftest ; then 
	echo "Could not build executable program:"
	echo "${F77-f77} $FFLAGS -o conftest conftest.o $LIBS"
    else
	/bin/rm -f conftestout
	if test -s conftest && (./conftest;exit) 2>conftestout ; then
	    Pac_CV_NAME=`cat conftestval`
        fi
    fi

fi
/bin/rm -f conftest*
$1=$Pac_CV_NAME
])dnl
dnl


dnl
dnl
dnl PAC_STRUCT_GET_SIZE(var_for_size)
dnl Second version adapted to caching
dnl
dnl sets var_for_size to the size.  Ignores if the size cannot be determined
dnl
define(PAC_STRUCT_GET_SIZE,
[ /bin/rm -f conftestval
/bin/rm -f conftest.c conftest.o conftest2.c conftest2.o
cat <<EOF > conftest2.c
#include <stdlib.h>
int calc ()
{
  int i;
  char *p1;
  char *p2;
  struct mytype {
   $2 a;
   $3 b;
  };
  struct mytype *s1;
  struct mytype *s2;
  s1=( struct mytype*)malloc(2*sizeof(struct mytype));
  s2= s1 + 1;
  p1= (char*) s1;
  p2= (char*) s2;
  i = p2-p1;
  return i;
}
EOF
if $CC $CFLAGS -c conftest2.c >/dev/null 2>&1 ; then 
    SaveLIBS="$LIBS"
    LIBS="conftest2.o $LIBS"
    PAC_TEST_PROGRAM([#include <stdio.h>
main() { 
  int isize_val;
  FILE *f=fopen("conftestval","w");
  
  if (!f) exit(1);
  isize_val = calc();
  fprintf( f, "%d\n", isize_val);
  exit(0);
}],Pac_CV_NAME=`cat conftestval`,Pac_CV_NAME="")
LIBS="$SaveLIBS"
else
  :
fi
if test -z "$Pac_CV_NAME" ; then
    # Try to compile/link with the Fortran compiler instead.  This
    # worked for the NEC SX-4
    compile_f='${CC-cc} $CFLAGS -c conftest.c; ${F77-f77} $FFLAGS -o conftest conftest.o conftestf.o $LIBS >/dev/null 2>&1'
    echo " try to run $compile_f"
    eval $compile_f
    if test ! -s conftest ; then 
	echo "Could not build executable program:"
	echo "${F77-f77} $FFLAGS -o conftest conftest.o $LIBS"
    else
dnl	/bin/rm -f conftestout
	if test -s conftest && (./conftest;exit) 2>conftestout ; then
	    Pac_CV_NAME=`cat conftestval`
        fi
    fi

fi
/bin/rm -f conftest*
dnl if test -n "$Pac_CV_NAME" -a "$Pac_CV_NAME" != 0 ; then
dnl     AC_MSG_RESULT($Pac_CV_NAME)
dnl else
dnl    AC_MSG_RESULT(unavailable)
dnl fi
$1=$Pac_CV_NAME
])dnl
dnl

