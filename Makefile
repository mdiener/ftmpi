# Makes everything we hope

all:
	cd share ; make -f Makefile
	cd services ; make -f Makefile
	cd src ; make -f Makefile 
	cd plugins/ftmpi ; make -f Makefile all
	cd utils/console ; make -f Makefile all
	cd ftmpi ; make -f Makefile all


c++: all
	cd c++ && ./configure --with-ftmpi --with-profiling && make install
	cd lib/$(HARNESS_ARCH) ; if [ -f libpmpi++.a ] ; then \
		mv libpmpi++.a libftmpi++.a ; fi 

clean:
	cd share ; make -f Makefile clean
	cd services ; make -f Makefile clean
	cd src ; make -f Makefile clean
	cd plugins/ftmpi ; make -f Makefile clean
	cd utils/console ; make -f Makefile clean
	cd ftmpi ; make -f Makefile clean
	cd examples ; make -f Makefile clean

cleaner:	#same as clean except it clears lib and bin directories as well
			#+ tmp, svc etc etc. AKA beware of its use!
			# this effects $(HARNESS_ARCH) directories only
	cd share ; make -f Makefile cleaner
	cd services ; make -f Makefile cleaner
	cd src ; make -f Makefile cleaner
	cd plugins/ftmpi ; make -f Makefile clean
	cd ftmpi ; make -f Makefile cleaner
	cd utils/console ; make -f Makefile cleaner
	cd bin; make -f Makefile empty
	cd lib; make -f Makefile empty
	cd tmp; make -f Makefile empty
	cd c++; make clean

distclean:	#same as clean except it clears lib and bin directories as well
			#+ tmp, svc etc etc. AKA beware of its use!
			# THIS EFFECTS all ARCH directories (use to make distributions)
	cd share ; make -f Makefile realclean
	cd services ; make -f Makefile realclean
	cd src ; make -f Makefile realclean
	cd plugins/ftmpi ; make -f Makefile realclean
	cd utils/console ; make -f Makefile cleaner
	cd bin; make -f Makefile emptyall
	cd lib; make -f Makefile emptyall
	cd tmp; make -f Makefile emptyall
	cd c++; make distclean

