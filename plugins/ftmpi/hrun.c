
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	HARNESS hrun : this program is used to start ftmpi programs

	This routine relies on the following:
	
	message routines (msg), simple name server (name_service),
	meta data ring service (MDSR), TCP communications library (SNIPE_LITE)
	libstartup that calls the startup_d (as either a standalone)
	(or as a g_hcore_d loaded service)

	ORNL/Christian use the standalone startup_d only

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham E Fagg <fagg@cs.utk.edu>
 			Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mpi.h"
#include "ft-mpi-sys.h"
#include "ft-mpi-modes.h"

#include	<sys/time.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>
#include	<netdb.h>
#include <sys/uio.h>
	/* now the non system stuff */
#include	"msg.h"
#include	"snipe_lite.h"
#include	"ns_lib.h"
#include	"envs.h"
#include	"internal_state_sys.h"

#include	"notifier.h"	/* get protos for notifier comms */
				/* and port info */
#include	"libstartup.h"

#define TIMEOUT 500

#ifdef USEMDRS
#include	"mdrs_clib.h"
#endif

/* these should be in common.c (they are from g_hcore_d) */
void	init_lsocket (int *, int *);
int		my_addr ();

int main (int argc, char **argv)
{

/* pass input and get the options out */
/* set up the DB and then spawn the job */

/* min args are */
/* hrun -np <n> <exename> */

/* or else np can be picked up by get env? */


int np, exe, exeargs;
/* np = num of procs requested */
/* exeargs = # of exe args */

/* ft_mode_t cm, mm; */

int next;	/* used to step through the argv data */

char vmname[256];     /* Virtual machine name */
char hn_cwdir[256];     /* current working directory */
char *vm;

int cwdir;
int needoutput;


/* Start up options for the new tasks */
int flag;

needoutput=0;

if (argc<4) {
badusage:
	fprintf(stderr,
		"usage %s -np <nprocs>  [-<opt name> <opts>...] <exename> <exe args>\n", argv[0]);
        fprintf(stderr,"Options include :\n");
        fprintf(stderr,"        -o\n");
	fprintf(stderr,"\t\tRedirect output\n");
        fprintf(stderr,"        -vm\tvmname\n");
	fprintf(stderr,"\t\tSpecific virtual machine name\n");
        fprintf(stderr,"        -cwd\t<DIR>\n");
	fprintf(stderr,"\t\tSet current working directory to DIR\n");
        fprintf(stderr,"        -s\n");
	fprintf(stderr,"\t\tSilent startup\n");
	fprintf(stderr," \n");
	exit (-1);
	}

	/* TA: Find VM name */
	vm=getenv("HARNESS_VM");
	if(vm!=NULL) {
	      strcpy(vmname,vm);
	} else {
	      strcpy (vmname, GHCORE_DEFAULT_VM_NAME);
	}


	/* Now to get the mode operation values */
	/* Just in case they are no set, set defaults */

	next = 1;	/* were we look next */
	exe = 1;	/* were we think the exe name is */
	cwdir=0;
	np = 0;
	silent = 0;

	while (next < argc) {
            if (!strcmp(argv[next],"-vm")) { /* Override VMNAME */
	        if(argv[next+1]!=NULL) {
		     strcpy(vmname,argv[next+1]);
		     exe = next+2;
		     next+=2;
	        } else {
	             goto badusage; /*TA:FIXME: I don't like goto */
		}
            } else if (!strcmp(argv[next],"-o")) {
	        needoutput=1;
	        exe = next+1;
	        next+=1;
            } else if (!strcmp(argv[next],"-s")) {
	        silent=1;
	        exe = next+1;
	        next+=1;
            } else if (!strcmp(argv[next],"-cwd")) {
	        if(argv[next+1]!=NULL) {
	            cwdir = next+1;
	            exe = next+2;
	            next+=2;
		} else {
                    goto badusage;
		}
	    } else if (!strcmp(argv[next],"-np")) { 
	        if(argv[next+1]!=NULL) {
		     np = atoi (argv[next+1]);
		     if ((np<=0)||(np>2048)) {
			fprintf(stderr,"Number of procs requested is %d??\n", np);
			exit (-2);
		     }
		     exe = next+2;
		     next+=2;
	        } else {
	             goto badusage;
		}
	    } else {
                next++;
	    }
	} 

	/* The exe is at argv[exe] */
	/* calculate the number of exe args */ 
	exeargs = argc - exe ;	

        if(argv[exe]==NULL) {
	    goto badusage;
	}

	/*
	if (!silent) printf("Starting %d procs for exe %s with %d exe args.\n", np, argv[exe], exeargs);
	*/

        flag=RUN_HARNESS;
	if(needoutput==1) {
            flag=flag | RUN_NEEDOUTPUT;
	}

        if(getcwd(hn_cwdir,256)==NULL) { 
        }

	if(cwdir!=0) { 
            startup_run(flag,vmname,np,HOSTAUTO,NULL,0,0,exeargs,&argv[exe],argv[cwdir]);
	} else {
            startup_run(flag,vmname,np,HOSTAUTO,NULL,0,0,exeargs,&argv[exe],hn_cwdir);
	}

	return 0;
}
