
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu> 
			Reed Wade <wade@cs.utk.edu>
			Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <poll.h>
#include <time.h>

#include "names.h"
#include "finit.h"

#include "startup.h"
#include "libstartup.h"
#include "libsio.h"
#include "daemon.h"
#include "msgbuf.h"
#include "msg.h"
#include "envs.h"
#include "plist.h"
#include "ns_lib.h"

#ifdef FTMPI_NOTIFY
    #include "notifier.h"
#endif /* FTMPI_NOTIFY */

/* me thinks these are soon to go bye byes */
#define MAX_VAR 50
#define MAX_NUM_VAR 50
#define MAX_COMMENT 256
#define MAX_COMMAND_LINE 512
#define TIMEOUT 500
#define INTERVAL_CHECK_ALIVE 3600 /* seconds */

#ifdef USE_OPENSSL
    #include <netinet/in.h>
    #include <openssl/ssl.h>
    #include "libsecure.h"
    #define KEYFILE "harness_ssl"
    #define DHFILE ".harness_ssl_dh1024"
    #define PASSWORD "password"
#endif


/* Timeout between HUP and TERM signal in kill process (second) */
#define KILL_TIMEOUT 1          



#define NOW (time((time_t)0))

extern int errno;
extern int daemon_mode;
int port;
char virtual_machine_name[MAXNAMELENGTH];

#ifdef STANDALONE

void check_NS(int k)
{
   int alive;
   alive=am_i_alive(virtual_machine_name,port);
   if(!alive) {
#ifdef STANDALONE
      exit(1);
#endif
   }
#ifdef STANDALONE	  
    alarm(INTERVAL_CHECK_ALIVE);
#endif
   return;
}

int	main (int argc, char **argv) 
#else

/* #ifdef TBTBAAA */
int     _hinit ()
{
puts ("I am a _hinit function\n");
return (0);
}
 
int     _finfo (int max,int * nfuncs,char ** names,int * types,int * rvals,int * inits)
{
  int c=0;

  if (nfuncs) {
     *nfuncs = 1;        /* real # of functions */
  }

  if (c==max) {
    return (c);
  }

  strcpy (names[c], "startup_notify_service");
  /* ftmpi:services:startup_notify_service */
  types[c] = FTYPE_SOCKET;
  rvals[c] = FINIT_VOID;
  inits[c] = 0;
  c++;
  if (c==max) return (c);
       
  return (c); /* return the number of functions in package */
}

/* #endif */




void startup_notify_service_s()
#endif /* STANDALONE */

{
  int socket,fd,ret,pid;
  int i,j;
  int rc;

  int caller_id;
  int new_gid;		/* what the new process is to be called */
  int command;
  int arg_num;

  int tmp_var[TMP_VAR_MAX];
  char comment[MAX_COMMENT];
  char command_line[MAX_COMMAND_LINE];
  char cwdir[MAX_COMMENT];

  char *args[MAX_NUM_VAR]; 
  /*char **args; */
  char *copy_arg_p;
  char *envp[MAX_NUM_VAR]; 
  /*char **envp; */
  char *copy_env_p;
  char *ns_host = NULL;
  char *ns_port = NULL;

  int from;
  int mb;
  int ntag;
  int tags[10];
  int env_num;
  int myid;	/* we get this when we call addme to ns call */
  int group;
  int id;
  int age;
  int mysignal;
  int status;
  int err;
  int found;
  int nItem;
  int out_ip;
  int out_port;
  int flag;
  int ngid;
  int *gidlist;
  struct startupItem *item;
#ifdef STANDALONE
  int next, vm;
#endif  /* STANDALONE */
  time_t longtime;
  int difftime;  

  struct pollfd *poll_fd;
  int verbose_mode;

#ifdef USE_OPENSSL
  char keyfilename[256];
  char dhfilename[256];
  SSL_CTX *ctx;
  BIO *sbio;
  SSL *ssl;
  struct in_addr tmpaddr;
#endif

  daemon_mode=0;
  verbose_mode=0;
  port=STARTUP_D_PORT_START;


#ifdef STANDALONE
  vm = 1;
  next = 1;

  signal(SIGALRM,check_NS);

  while(next < argc) {
     if(!strcmp(argv[next],"-v")) {
        vm=next+1;
        next++;
	verbose_mode=1;
     } else if(!strcmp(argv[next],"-d")) {
        vm=next+1;
        next++;
	daemon_mode=1;
     } else if(!strcmp(argv[next],"-hn")) {
	ns_host = argv[next+1];
        vm=next+2;
        next+=2;
     } else if(!strcmp(argv[next],"-hp")) {
	ns_port = argv[next+1];
        vm=next+2;
        next+=2;
     } else {
        next++;
     }
  }

  if(daemon_mode==1) {
     verbose_mode=0;
     daemon_init();
  }

  get_envs_local(); 
  if(argv[vm]!=NULL) {
     if(argv[vm+1]!=NULL) {
        printf("%s [<-option>] [vmname]\n",argv[0]);
        printf("Option include\n");
        printf("-d daemon mode\n");
        printf("-v verbose mode\n");
	printf("-hn name_service host name\n");
	printf("-hp name_service port number\n");
	exit(1);
     }
     strcpy(virtual_machine_name,argv[vm]);
  } else {
     if(HARNESS_LOCAL_VMNAME!=NULL) {
         strcpy(virtual_machine_name,HARNESS_LOCAL_VMNAME);
     } else {
         strcpy(virtual_machine_name,GHCORE_DEFAULT_VM_NAME);
     }
  }

#else
  get_envs_local(); 
  if(HARNESS_LOCAL_VMNAME!=NULL) {
     strcpy(virtual_machine_name,HARNESS_LOCAL_VMNAME);
  } else {
     strcpy(virtual_machine_name,GHCORE_DEFAULT_VM_NAME);
  }
#endif

  for(i=0;i<MAX_NUM_VAR;i++){
    args[i] = (char*) malloc(MAX_VAR);
    envp[i] = (char*) malloc(MAX_VAR);
  }
  copy_arg_p = NULL;
  copy_env_p = NULL;


  if(daemon_mode==0) {
      printf("Startup_notify service daemon\n");
  }

  startupInit();

  ret = setportconn(&socket, port, STARTUP_D_PORT_RANGE);
  if(socket<0) {
    if(daemon_mode==0) {
        printf("SERVER >> Initial socket init failed.\n");
    }
    exit (socket);
  }
  else {
    if(verbose_mode==1) {
       printf("SERVER >> End-point established on socket [%d] for port [%d]\n", socket, ret);
    }
  }


/* before we add ourself to the NS if we are talking to a an FTMPI_NOTIFIER */
/* we need to make sure we can find it first */

#ifdef FTMPI_NOTIFY
  	if( ns_host == NULL ) {
  	  /* first get the NS service */
          ns_host=getenv("HARNESS_NS_HOST");
	  if(ns_host==NULL) {
            printf("Cannot get HARNESS_NS_HOST environment\n");
            exit(1); 
	  }
	}
	if( ns_port == NULL ) {
          ns_port=getenv("HARNESS_NS_HOST_PORT");
	  if(ns_host==NULL) {
            printf("Cannot get HARNESS_NS_HOST_PORT environment\n");
            exit(1); 
	  }
	}
	
	rc = ns_init(ns_host,atoi(ns_port)); 

	if (rc<0) {
            if(daemon_mode==0) {
		printf("Startup_d: Cannot contact NS? is it running or the envs not set right ?\n");
	    }
	    exit (rc);
	}

	ns_close ();   

        if(verbose_mode==1) {
	    printf("Attempting to find the notifier service\n"); fflush(stdout);
	}
	rc = notifier_find ();

	if (rc<0) {
            if(daemon_mode==0) {
		printf("Ops cannot find the ftmpi_notifier service. Please start it first if you are using this startup service for FTMPI jobs\n");
	    }
	    exit (-1);
	}

/* ok we have found one, the library stores info about who to talk to it */
/* so that now we can just use notifier_ function calls */

#endif /* FTMPI_NOTIFY */


/*   id in pf_bin format = add_self_to_ns(vmname, port); */
	/* sorry tone, port is the one you wanted, but you get ret as above! GEF */

	/* ret is from the setportconn above */
	/* this routine returns the gid of this service  in pfbin format */
	/* this id, is only used in lowlevel message protocols and nothing else */

  port=ret; /* keep it for checking am I alive? */
  myid = add_self_to_ns(virtual_machine_name,ret);

  poll_fd=(struct pollfd *)malloc(sizeof(struct pollfd));

#ifdef USE_OPENSSL

  if(daemon_mode==0) {
     puts("WE ARE IN SECURE CONNECTION"); 
  }

  tmpaddr.s_addr=get_my_addr();
  sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr));
  ctx=ctx_init(keyfilename,PASSWORD);
  /*
  sprintf(dhfilename,"%s/%s",getenv("HOME"),DHFILE);
  load_dh_params(ctx,dhfilename);
  */

#endif

#ifdef STANDALONE	  
    alarm(INTERVAL_CHECK_ALIVE);
#endif

  while(1){


    unblockChildHandler();
    if(verbose_mode==1) {
        printf("Accepting Connections ...\n");
        fflush(stdout);
    }

    while(1) {


        rios_poll(TIMEOUT);
        poll_fd->fd=socket;
        poll_fd->events=POLLIN;
        poll_fd->revents=0;
	ret=poll(poll_fd,1,TIMEOUT);
        if(ret>0) {
           fd = allowconn (socket, 0, &pid);

#ifdef USE_OPENSSL
	   sbio=BIO_new_socket(fd,BIO_NOCLOSE);
           ssl=SSL_new(ctx);
	   SSL_set_bio(ssl,sbio,sbio);
           if((SSL_accept(ssl))<=0) {
               berr_exit("SSL accept error");
	   }
	   if(deamon_mode==0) {
	       puts("SSL ACCEPT OK ;-)");
	   }
#endif

	   break;
        }
        startupLoopOnce();
    }

    blockChildHandler();

    startupLoopOnce();

    if(fd > 0){
        startupLoopOnce();

		ntag = 5;
		for(i=0;i<ntag;i++) tags[i]=0;
#ifdef USE_OPENSSL
	       ret = recv_pkmesg_s (ssl, &from, &ntag, tags, &mb); /* do the recv */
#else
	       ret = recv_pkmesg (fd, &from, &ntag, tags, &mb); /* do the recv */
#endif
               if(verbose_mode==1) {
		    printf("Ret %d from %x ntag %d 0 %d 1 %d 2 %d 3 %d 4 %d\n", ret, from, ntag, tags[0], tags[1], tags[2], tags[3], tags[4]);
		    fflush(stdout);
	       }
		if (ntag>=1)
			command = tags[0];
		else 
			command = -1;

  
        switch(command){
          case PROTO_STARTUP_SPAWN:
            if(verbose_mode==1) {
                printf("STARTUP_ADD\n");
	    }
			if (mb<0) break;
            bzero(tmp_var,sizeof(int)*TMP_VAR_MAX);
			upk_int32 (mb, &caller_id, 1);
			upk_int32 (mb,  &flag, 1);
			upk_int32 (mb,  &ngid, 1);
			gidlist=(int *)malloc(sizeof(int)*ngid);

			for(i=0;i<ngid;i++) {
			   upk_int32 (mb,  &gidlist[i], 1);
			}
			upk_string (mb, comment, MAX_COMMENT);
			upk_string (mb, command_line, MAX_COMMAND_LINE);
			upk_string (mb, cwdir, MAX_COMMAND_LINE);
			upk_raw32 (mb,  &out_ip, 1); 
			upk_int32 (mb,  &out_port, 1);
			upk_int32 (mb, &arg_num, 1);
            /* for(i=0;i<arg_num;i++) upk_string(mb, args[i], MAX_COMMENT); */
                   /* args=(char **)malloc(sizeof(char)*(arg_num+1));  */
                    for(i=0;i<arg_num;i++) {
                       /* args[i]=(char *)malloc(sizeof(char)*MAX_VAR);  */
		        upk_string(mb, args[i], MAX_VAR);
		    }

			/* temp terminate arg list */
			copy_arg_p = args[arg_num]; args[arg_num] = NULL; 

			upk_int32 (mb, &env_num, 1);
            /*for(i=0;i<env_num;i++) upk_string(mb, envp[i], MAX_COMMENT); */
                      /*  envp=(char **)malloc(sizeof(char)*(env_num+1));  */
                        for(i=0;i<env_num;i++) { 
                           /* envp[i]=(char *)malloc(sizeof(char)*MAX_VAR);  */
			    upk_string(mb, envp[i], MAX_VAR);
			}


			/* temp terminate ENV list */
			copy_env_p = envp[env_num]; envp[env_num] = NULL; 

			free_msg_buf (mb);
                        /* printf("NGID is %d\n",ngid);  */


			/* alloc GID here if needed */
                        if(gidlist[0]==SPAWN_STARTUP_ALLOCATE_GIDS) {
                             ns_open();
                             ns_gid (NS_ID_FTMPI,ngid,gidlist);                            
			     /* assume that we want the whole GID allocated here */
                             ns_close();
			     new_gid=ngid;
                        } else {
                             new_gid=0;
			}

			for(i=0;i<ngid;i++) {

                            if(verbose_mode==1) {
			      printf("caller %x gid %x [%s] exe [%s], argc %d envc %d out_port %d flag %d \n",
					caller_id, gidlist[i], comment, command_line, arg_num, env_num, out_port, flag);


			      fflush(stdout);
	                    }


			    /* Very bad.. a lot of hack, but save bandwidth 
			     (i.e. don't send whole environment for every parallel process) */

			    if(flag>0) { /* change environment manually */
			        if((flag&RUN_FTMPI)==RUN_FTMPI) {
			            sprintf(envp[1],"FTMPI-GID=%d", gidlist[i]);
				}
			        if((flag&RUN_HARNESS)==RUN_HARNESS) {
			            sprintf(envp[1],"HCORE-GID=%d", gidlist[i]);
				}
			    }

                           ret = startupAdd(caller_id, gidlist[i], comment, out_ip, out_port, command_line, cwdir, args, envp,0);
			}
			/* send result back and close connection */

			ntag = 3;
			tags[0] = PROTO_STARTUP_SPAWN;
			tags[1] = ret;
			tags[2] = new_gid;
			if(new_gid==0) {
#ifdef USE_OPENSSL
			   rc = send_pkmesg_s (ssl, myid, ntag, tags, EMPTYMSGBUF, 0);
			   closeconn_s (ssl,fd);
#else
			   rc = send_pkmesg (fd, myid, ntag, tags, EMPTYMSGBUF, 0);
			   closeconn (fd);
#endif
			} else {
#ifdef USE_OPENSSL
                           mb=get_msg_buf(1);
                           pk_int(mb,gidlist,new_gid);
			   rc = send_pkmesg_s (ssl, myid, ntag, tags, mb, 1);
			   closeconn_s (ssl,fd);
#else
			   rc = send_pkmesg (fd, myid, ntag, tags, mb, 1);
			   closeconn (fd);
#endif
			}

			free(gidlist);

			/* Now copy back pointers we used to termnate lists */
			args[arg_num] = copy_arg_p;
			envp[env_num] = copy_env_p;

			/* clear lists before next operation (optional really) */
      		for(i=0;i<MAX_NUM_VAR;i++){
        		memset(args[i],0,MAX_VAR);
        		memset(envp[i],0,MAX_VAR);
      		}

			/*
			puts("ZZZZ");
			fflush(stdout);
			for(i=0;i<arg_num;i++) {
                            free(args[i]); 
			}
			free(args);
			puts("KKKK");
			fflush(stdout);
			for(i=0;i<env_num;i++) {
                            free(envp[i]); 
			}
			free(envp);
			*/
                break;

	case PROTO_STARTUP_PS_ALL:

/* TA:FIXME: If someday we use multi-thread. Please don't forget to lock mutex. */
/* The startupGetNumberOfItem and startupGetNextItem must be called as atomic function. */

		upk_int32 (mb,  &flag, 1);
		free_msg_buf (mb);

                nItem = startupGetNumberOfItem();
                mb = get_msg_buf(1);
                pk_int32(mb, &nItem, 1);
                group = myid & 0xffff;
                pk_int32(mb, &group, 1);
                item = NULL;
                for (i = 0; i < nItem; i++) {
                    item = (struct startupItem *) startupGetNextItem(item);
                    pk_int32(mb, &(item->gid), 1);
                    pk_int32(mb, &(item->caller), 1);
                    pk_string(mb, item->execName);
                    pk_int32(mb, &(item->status), 1);
                    pk_int32(mb, &(item->exitValue), 1);

		    if(item->stopTime==0) { /* the process is still running */
                        longtime=time(0) - (item->startTime);
		        difftime=(int) longtime;
		    } else {
                        longtime=(item->stopTime) - (item->startTime);
		        difftime=(int) longtime;
		    }

		    /* Don't send long across multiple architecture unless we use DDT */ 
                    pk_int32(mb, &difftime, 1);

		    /*
                    pk_int32(mb, &(item->startTime), 1);
		    if(item->stopTime==0) { 
		        time_t tmp_stoptime = time(0);
                        pk_int32(mb, &tmp_stoptime, 1);
                    } else {
                        pk_int32(mb, &(item->stopTime), 1);
		    }
		    */

                    pk_int32(mb, &(item->callbackListLength), 1);
                    for (j = 0; j < item->callbackListLength; j++) {
                       /* pk_int32(mb, &(item->callbackList[j]), 1);*/
                        pk_int32(mb, &(item->callbackList), j);
                    }
                }
                ntag = 1;
                tags[0] = PROTO_STARTUP_PS_ALL_ACK;
#ifdef USE_OPENSSL
                rc = send_pkmesg_s(ssl, myid, ntag, tags, mb, 1);
		closeconn_s (ssl,fd);
#else
                rc = send_pkmesg(fd, myid, ntag, tags, mb, 1);
                closeconn(fd);
#endif
		if(flag&1==1) { /* expire item */
                    startupExpireItems(0); 
		}
                break;

            case PROTO_STARTUP_DELETE:
                rm_from_ns(virtual_machine_name);
				ntag = 2;
				tags[0] = PROTO_STARTUP_KILL_ACK;
#ifdef STANDALONE
				tags[1] = RESULT_EXITING;
#ifdef USE_OPENSSL
				rc = send_pkmesg_s(ssl, myid, ntag, tags, EMPTYMSGBUF, 0);
				closeconn_s(ssl,fd);
#else
				rc = send_pkmesg(fd, myid, ntag, tags, EMPTYMSGBUF, 0);
				closeconn(fd);
#endif
                exit(0);
#else
				tags[1] = RESULT_EXIT_DELAYED;
				rc = send_pkmesg(fd, myid, ntag, tags, EMPTYMSGBUF, 0);
				closeconn (fd);
				return;
#endif /* STANDALONE */
                break;

            case PROTO_STARTUP_KILL:
                upk_int32(mb, &id, 1);
                upk_int32(mb, &mysignal, 1);
                upk_int32(mb, &flag, 1);
		free_msg_buf (mb);
                mb = get_msg_buf(1);
                item = NULL;
                status = 0;
                found = 0;
                err = 0;
                nItem = startupGetNumberOfItem();
		/* printf( "\n\n\nKILL THEM ALLLLLLL (items %d) !!!!!!!!!\n\n\n", nItem ); */
#ifdef OLD_CODE
                for (i = 0; i < nItem; i++) {
                    item = (struct startupItem *) startupGetNextItem(item);
                    if ( (item->gid == id && flag==1) || (item->caller == id && flag==2) ) {
                        found++;
                        if (item->status != STATUS_RUNNING) {
                            status = -3;
                            break;
                        }
                        if (mysignal == -1) {     /*kill */
                            rc = kill(item->pid, 1);    /* try kill with HUP first */
                            if (rc < 0) {
                                status = -2;
                                err = errno;
                            } else {
                                sleep(KILL_TIMEOUT);
                                rc = kill(item->pid, 0);        /* check process exist */
                                if (rc != ESRCH) {      /* process still alive */
                                    rc = kill(item->pid, 15);   /* terminate with TERM signal */
                                    if (rc < 0) {
                                        status = -2;
                                        err = errno;
                                    }
                                }
                            }
                            item->stopTime = NOW-1;
                        } else {        /*signal */
                            rc = kill(item->pid, mysignal);
                            if (rc < 0) {
                                status = -2;
                                err = errno;
                            }
                            item->stopTime = NOW-1;
                        }
			if(flag==1) {  /* kill by gid */
                            break;
			}
                    }
                }
#endif  /* OLD_CODE */
		{
		  int pos = 0;

		  /* if mysignal is the SIGTERM then do the standard kill */
		  if( mysignal == SIGTERM ) mysignal = -1;

		  for (i = 0; i < nItem; i++) {
		    item = (struct startupItem *) startupGetNextItem(item);
		    if( ((item->caller == id) && (flag == 2)) ||
			((item->gid == id) && (flag == 1)) ) {
		      if (item->status != STATUS_RUNNING) {
			status = -3;
			continue;
		      }
		      if (mysignal == -1) {     /*kill */
			rc = kill(item->pid, SIGTERM);    /* dont be shy KILLLL it */
			if (rc < 0) {
			 /* printf( "Cant kill the pid %d with SIGTERM error %s\n", item->pid, strerror(errno) ); */
			} else pos++;
		      } else {        /*signal */
			rc = kill(item->pid, mysignal);
			if (rc < 0) {
			  status = -2;
			  err = errno;
			} else pos++;
			item->stopTime = NOW-1;
		      }
		    }
		  }
		 /* printf( "\n\nSEND TERM SIGNAL TO %d processes\n\n", pos ); */
		  found = pos;
		}
		/*  printf( "\n\n\nKILL THEM ALL (killed %d)\n\n\n", found );*/
                if (found == 0) {
                    status = -1;
                }
                pk_int32(mb, &status, 1);
                pk_int32(mb, &err, 1);

                ntag = 1;
                tags[0] = PROTO_STARTUP_KILL_ACK;
#ifdef USE_OPENSSL
                rc = send_pkmesg_s(ssl, myid, ntag, tags, mb, 1);
                closeconn_s(ssl,fd);
#else
                rc = send_pkmesg(fd, myid, ntag, tags, mb, 1);
                closeconn(fd);
#endif
                break;

            case PROTO_STARTUP_EXPIRE:
                upk_int32(mb, &age, 1);
		free_msg_buf (mb);
                mb = get_msg_buf(1);
		found = startupExpireItems(age);
                pk_int32(mb, &found, 1);

                ntag = 1;
                tags[0] = PROTO_STARTUP_EXPIRE_ACK;
#ifdef USE_OPENSSL
                rc = send_pkmesg_s(ssl, myid, ntag, tags, mb, 1);
                closeconn_s(ssl,fd);
#else
                rc = send_pkmesg(fd, myid, ntag, tags, mb, 1);
                closeconn(fd);
#endif
                break;

            case PROTO_STARTUP_HALT:
                rm_from_ns(virtual_machine_name);
		item=NULL;
                nItem = startupGetNumberOfItem();
                for (i = 0; i < nItem; i++) {
                    item = (struct startupItem *) startupGetNextItem(item);
		    if(item!=NULL) {
		        kill(item->pid,15); /* Kill with SIGTERM */
		    }
		}
		kill(0,15); /* Just in case  */
		exit(0); /* suicide (both startup_d, g_hcore_d) */
                break;

            case PROTO_STARTUP_RESET:
		item=NULL;
                nItem = startupGetNumberOfItem();
                for (i = 0; i < nItem; i++) {
                    item = (struct startupItem *) startupGetNextItem(item);
		    if(item!=NULL) {
		        kill(item->pid,15); /* Kill with SIGTERM */
		    }
		}
                break;

            case PROTO_STARTUP_INFO:
                ntag = 1;
                mb = get_msg_buf(1);
                pk_string(mb, HARNESS_ARCH);
                tags[0] = PROTO_STARTUP_INFO_ACK;
#ifdef USE_OPENSSL
                rc = send_pkmesg_s(ssl, myid, ntag, tags, mb, 1);
                closeconn(ssl,fd);
#else
                rc = send_pkmesg(fd, myid, ntag, tags, mb, 1);
                closeconn(fd);
#endif
                break;

            case PROTO_STARTUP_PING:
                ntag = 1;
                tags[0] = PROTO_STARTUP_PING_ACK;
#ifdef USE_OPENSSL
                rc = send_pkmesg_s(ssl, myid, ntag, tags, EMPTYMSGBUF, 0);
                closeconn_s(ssl,fd);
#else
                rc = send_pkmesg(fd, myid, ntag, tags, EMPTYMSGBUF, 0);
                closeconn(fd);
#endif
                break;

          case 2:
            if(daemon_mode==0) {
               printf("STARTUP_MONITOR\n");
	    }
            break;
          case 3:
            if(daemon_mode==0) {
               printf("STARTUP_UN_MONITOR\n");
	    }
            break;
          case 4:
            if(daemon_mode==0) {
               printf("STARTUP_GET_STATUS\n");
	    }
            break;
          case 5:
            if(daemon_mode==0) {
               printf("STARTUP_EXPIRE_ITEMS\n");
	    }
            startupExpireItems(pid);
            break;
          case 6:
            if(daemon_mode==0) {
               printf("PRINTING STATUS %d\n",pid);
	    }
            break;
          case -99:
		  case -1:
            if(daemon_mode==0) {
                printf("Closing Connection %d ...\n",fd);
    		fflush(stdout);
	    }
            closeconn(fd);
            break;
          default:
	    if(mb >= 0) {
                 free_msg_buf(mb); 
	    }
            if(daemon_mode==0) {
               printf("Badly formed message, ignoring\n");
	    }
            sleep(1);
            bzero(&command,sizeof(int));
			ntag = 2;
			tags[0] = PROTO_STARTUP_CLOSED;
			tags[1] = command;
			rc = send_pkmesg (fd, myid, ntag, tags, EMPTYMSGBUF, 0);
            closeconn(fd);
            break;
        }
/*         printf("******************************************\n"); */
/*         printf("------------------------------------------------\n"); */
    }
    else {
      startupLoopOnce();
    }
  }
}

