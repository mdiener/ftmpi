
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg	 <fagg@cs.utk.edu>	<project lead>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* this program loops slowly checking the state of the startup daemons */

/* if a startup daemon no longer responds it can update the NS */

/* todo: more checking on how this code works */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "notifier.h"
#include "ns_lib.h"
#include "hlib.h"
#include "../src/envs.h"
#include "libstartup.h"

#define	giveupat	2
#define ns_check_loops	giveupat+1 /* must be bigger!! */

static	int daemons=0;
static	int failed=0;
static	int failing=0;
static	int	mislabelled=0;

static	long	ftutime=0;	/* sleep inm usecs */
static	int		sleeptime=0;	/* sleep in seconds */

static	int* state=NULL;	/* state of each daemon */
static	int* ids=NULL;		/* actual ID of each daemon */

static 	int	tout=30;		/* total time out */

int get_daemons ();				/* finds/updates the daemon lists */
int check_daemons (int fast);	/* does one loop on all daemons */
int remove_failed ();			/* removes the failed nodes from the NS */
int display_stats ();			/* basic statistics on statuses */

static char dvm[256];

extern int *startup_entries;
extern int startup_group;


int main (int argc, char *argv[])
{
	int rc;
	int done;
	int loops;
	int tmp;

	if (argc>3) { 
	   	fprintf(stderr,"usage %s <DVM> <TTOUT>\nIf no DVM given then defaults to the name \'dvm\'\nTTOUT = Total time out. The time taken to test all the nodes. \nDefault value is %d seconds\n", 
			  argv[0], tout);
		exit (-1);
	}

	if (argc==1) {
	   sprintf(dvm,"dvm");
	}
	else
	   strcpy (dvm, argv[1]);

	if (argc==3) {
	   tmp = atoi (argv[2]);
	   if (tmp>0) tout = tmp;
	}

	printf("Startup daemon watchdog monitor for DVM [%s] started ttout=%d\n", dvm, tout);

	init_envs  ();  /* get environment variables */
	get_envs   ();

	if (!HARNESS_NS_HOST) {
	   fprintf(stderr,"No HARNESS_NS_HOST specified, bailing\n");
	   exit (-1);
	}

	if (!HARNESS_NS_HOST_PORT) {
	   fprintf(stderr,"No HARNESS_NS_HOST_PORT specified, bailing\n");
	   exit (-1);
	}

	done = 0;
	while (!done) {
		rc = ns_init ( HARNESS_NS_HOST, HARNESS_NS_HOST_PORT ); /* talk to NS */
		ns_close ();
		if (rc<0) {
	   		fprintf(stderr, "OPS, no name service running.. sleeping\n");
			sleep (10);
		}
		else done = 1;
	}
	/* ok have NS now contactable */

	get_daemons (); /* prime the loop by getting a list of daemons! */

	/* big loop now */
	done = 0;
	loops = 0;
	while (!done) {
	   if (!daemons) {
		  get_daemons ();	/* get list of daemons */
		  loops = 0;
	   }
	   if (daemons) {
		  check_daemons (0);	/* check the daemons */
		  display_stats (); /* let the world know what is happening */
		  loops++;
	   }
	   if (failing) { /* if someone starts to fail.. recheck them only ! */
		  check_daemons (1);	/* check only failing daemons */
		  display_stats (); /* let the world know what is happening */
	   }
	   if (failed||mislabelled) {
		  remove_failed ();	/* remove failed daemons from NS */
		  get_daemons (); 	/* update list */
		  loops = 0;
	   }
	   if (loops == ns_check_loops) {
		  get_daemons ();	/* update daemon list periodically */
		  loops = 0;
	   }

	  /* now the sleep part */
	  if (sleeptime) sleep (sleeptime);
	  else
		 usleep (ftutime);
	}
}



int get_daemons ()
{
   int done;
   int rc;
   int i;
   double dd, dt, ds;

	done = 0;
	while (!done) {
		rc = startup_finder_vm (dvm);
		printf ("rc %d \n", rc);
		if (!rc) {
	   		fprintf(stderr, "No startup daemons running.. sleeping\n");
			sleep (10);
		}
		if (rc>0) done = 1;
		if (rc<0) {
	   		fprintf(stderr, "Problem getting startup daemons listing.. bailing\n");
			exit (rc);
		}
	}

	daemons = rc;
	if (state) free (state); /* get rid of old list */
	if (ids) free (ids); 	/* get rid of old ids */

	state = (int*) malloc (daemons*(sizeof(int)));	/* space for new list */
	ids = (int*) malloc (daemons*(sizeof(int)));	/* space for new ids */

	for (i=0;i<daemons;i++) {
	   state[i]=0; /* reset all states to 0 failures */
	   ids[i]=-1;  /* we don't know yet, we get these from the ping call */
	}
	failed = 0;

	for (i=0;i<daemons;i++) printf("%d ", startup_entries[i]);
	printf("\n");

	if (daemons<tout) {
	   sleeptime = tout/daemons;
	   if (sleeptime==0) sleeptime =1;
	   ftutime = 0;
	}
	else {
	   dd = (double) daemons;
	   dt = (double) tout;
	   ds = dt/dd;
	   ds *= 1000000.0;
	   ftutime = (long) ds;
	   sleeptime = 0;
	}
	printf("sleeptime %d ftutime %d\n", sleeptime, ftutime);

	return (daemons);
}


int check_daemons (int fast)		/* does one loop on all daemons */
{
   int i;
   int rc;
   double rt;	/* round trip time */
   int loc;

   failed = 0; failing = 0; /* reseting all counters */
   mislabelled = 0;

   for (i=0;i<daemons;i++) { /* for each daemon */
	  if ( (state[i]<giveupat) &&	/* not dead already */
			( (fast&&(state[i]>0)) || (!fast)
			 )
		 )
	  	{ 
   		 rt = 0.0;
		 loc = 0;
		 rc = startup_ping (0x900, i, &loc, &rt);
		 ids[i]=loc;
		 loc = loc & 0xFFFF;
		 printf("index %d ping %d self reported index %d ns reported index %d rt %lf\n", i, rc, loc, startup_entries[i], rt);
		 if (rc<0) { /* trouble */
			state[i]++;
			failing++;
		 }
		 if ((rc==0) && (loc==startup_entries[i])) {	
									/* daemon ok and in the right place */
			state[i]=0;	/* its ok, reset its touts */
		 }
		 if ((rc==0) && (loc!=startup_entries[i])) {	
									/* daemon ok but in the wrong place */
			state[i]=-1;	/* its not ok and is a duplicate */
			mislabelled++;
		 }
	  }
	  if (state[i]==giveupat) { /* its dead */
		 failed++;
	  }

	  /* now the sleep part */
	  if ((!fast)&&(!failing)) {
	  	if (sleeptime) sleep (sleeptime);
		 else
			usleep (ftutime);
	  }
   }

   return (0);
}

int remove_failed ()		/* removes the failed nodes from the NS */
{
   int i;
   int rc;
   int group, slot; /* ns position values */
   char gname[256];

   /* make the name of the startup service */
   sprintf(gname,"%s:ftmpi:services:startup_notify_service", dvm);

   rc = ns_open ();
   for (i=0;i<daemons;i++) { /* for each daemon */
	  if ((state[i]==giveupat)||(state[i]==-1)) { /* if dead or mislabelled */

		 group = startup_group;

		 /* note for slot we use the startup_entries slot value */
		 /* this is because 'i' is a relative compacted address */
		 /* not an absolute one */
		 slot  = startup_entries[i];

		 /* connect to ns, delete entry and then close connection */
		 rc = ns_del (gname, group, slot);
		 printf("ns_del on [%s] grp [%d] slot [%d] -> %d\n", 
			   gname, group, slot, rc);

	  }
   }
   ns_close ();
   return (0);
}

int display_stats ()		/* basic statistics on statuses */
{
   if (!daemons) {
	  printf("watchdog: no startup daemons listed in NS\n");
	  return (0);
   }
	if ((!failing)||(!mislabelled))
	  	printf("watchdog: startup daemons %d\n", daemons);
		

	if ((failing)||(mislabelled))
		 printf("watchdog: startup daemons %d failing %d failed %d mislabelled %d\n", daemons, failing, failed, mislabelled);
	return (0);
}
	

