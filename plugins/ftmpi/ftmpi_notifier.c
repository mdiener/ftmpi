
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* 
	changes proto to include <mid><gid><pid><rc>
	GEF STR 2001
*/

/*
	Added correct proto defs to notifier.h
	now handle things as a packed message 
*/


/* 
	This program received failure and exit messages from the startup_ds
	and then redirects them to the interested parties (ie ftmpi procs)
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/uio.h>

#include "envs.h"       /* this is from Harness g_hcore_d */

#include "snipe_lite.h"       
#include "msg.h"       
#include "msgbuf.h"       
#include "ns_lib.h"       

#include "notifier.h"
#include "daemon.h"


/* HARNESS NAME SERVICES */
/* char*   HARNESS_NS_HOST; */
/* int     HARNESS_NS_HOST_PORT; */
/* char*   HARNESS_RS_HOST; */
/* int     HARNESS_RS_HOST_PORT; */

int PORT = FTMPI_NOTIFIER_PORT_START;

#define MAXRUNIDS	20
#define MAXENTRIES	514

int notify_list_gids [MAXRUNIDS][MAXENTRIES];	/* the lists (should be a list) */
int notify_list_addr [MAXRUNIDS][MAXENTRIES];	/* the lists (should be a list) */
int notify_list_port [MAXRUNIDS][MAXENTRIES];	/* the lists (should be a list) */
int notify_runids [MAXRUNIDS];
int notify_counts [MAXRUNIDS];
int daemon_mode;

/* prototypes */
void init_lists ();
int addme (int *tags, int mb);
int removeme (int *tags);
int removeall (int *tags);
int exit_event (int *tags);


int main (int argc, char **argv)
{

int socket,pid,port,ret;
int i1, i2;	/* ns args */
int r, addr;
int tags[6];
int ntag;
int s;
int i;
int rtag[3];
int from;
int mb;


  char myname[256]="ftmpi:services:notifier\0";
  daemon_mode=0;

  if(argc==2) {
      if(!strcmp(argv[1],"-d")) {
           daemon_mode=1;
           daemon_init();
      }
  }
  /* SIGPIPE may occur when we are in process of notifying a process
   * while the process got killed or suicide simultaneously */
  signal(SIGPIPE,SIG_IGN); /* just in case */

  init_lists ();

/* OK, now get the rest of the stuff we need */

get_envs_ns  ();  /* get environment variables needed for the NS */

if (!HARNESS_NS_HOST) {
    if(!daemon_mode) fprintf(stderr,"No HARNESS_NS_HOST set\n");
    return (-1);
}

if (!HARNESS_NS_HOST_PORT) {
    if(!daemon_mode) fprintf(stderr,"No HARNESS_NS_HOST PORT set\n");
    return (-1);
}
r = ns_init (HARNESS_NS_HOST, HARNESS_NS_HOST_PORT);


if (r) {
    if(!daemon_mode) fprintf(stderr,"Harness NS host not contactable\n");
    return (-1);
}

  ret = setportconn(&socket, PORT, FTMPI_NOTIFIER_PORT_RANGE);

  if(socket<0) {
    if(!daemon_mode) printf("SERVER >> Initial socket init failed.\n");
    exit (socket);
  }
  else {
    if(!daemon_mode) printf("SERVER >> End-point established on socket [%d] for port [%d]\n", socket, ret);
  } 

	
addr = get_my_addr ();	/* from SNIPE LITE */
port = ret;

r = ns_add (myname, addr, port, &i1, &i2);

ns_close ();

if (r<0) {
	if(!daemon_mode) printf("Could not register myself in the name service\n");
	exit (-1);
}

if (i2) {
	if(!daemon_mode) printf("I am not the only one running?\n");
	ns_open();
	r = ns_del (myname, i1, i2);			/* remove me */
	ns_close();
	exit (-1);
}

  while(1){
    s = allowconn (socket, 0, &pid);

	/* get the request */

	ntag = 6;	/* max we have */
	for(i=0;i<ntag;i++) tags[i]=0;

	(void)recv_pkmesg (s, &from, &ntag, tags, &mb); /* do the recv */

	switch (tags[0]) {
		case NOTIFIER_PROTO_ADDME:
			puts("NOTIFIER_PROTO_ADDME");
			rtag[0] = NOTIFIER_PROTO_ADDME;
			rtag[1] = from;
			rtag[2] = addme (tags, mb);
            send_pkmesg (s, NOTIFIER_ID_SERVER, 2, rtag, EMPTYMSGBUF, 0);
			closeconn(s);
			break;
		case NOTIFIER_PROTO_REMOVE:
			puts("NOTIFIER_PROTO_REMOVE");
			rtag[0] = NOTIFIER_PROTO_REMOVE;
			rtag[1] = from;
			rtag[2] = removeme (tags);
            send_pkmesg (s, NOTIFIER_ID_SERVER, 2, rtag, EMPTYMSGBUF, 0);
			closeconn(s);
			break;
		case NOTIFIER_PROTO_REMOVEALL:
			puts("NOTIFIER_PROTO_REMOVEALL");
			rtag[0] = NOTIFIER_PROTO_REMOVEALL;
			rtag[1] = from;
			rtag[2] = removeall (tags);
            send_pkmesg (s, NOTIFIER_ID_SERVER, 2, rtag, EMPTYMSGBUF, 0);
			closeconn(s);
			break;
		case NOTIFIER_PROTO_EXIT:
			puts("NOTIFIER_PROTO_EXIT");
			closeconn(s);
			exit_event (tags);
			break;
		case NOTIFIER_PROTO_ACK:
			puts("NOTIFIER_PROTO_ACK");
			rtag[0] = NOTIFIER_PROTO_ACK;
			rtag[1] = from;	/* reply with your ID */
            send_pkmesg (s, NOTIFIER_ID_SERVER, 2, rtag, EMPTYMSGBUF, 0);
			closeconn(s);
			break;
                case NOTIFIER_PROTO_SHUTDOWN:
			puts("NOTIFIER_PROTO_SHUTDOWN");
                        ns_open ();
                        r = ns_del (myname, i1, i2);			/* remove me */
                        ns_close ();
			exit(1);
			break;
		default:
			if(!daemon_mode) printf("Unknown message/request of length %d\n", ntag);
			closeconn(s);
			break;
	} /* end of switch */

  } /* end of while, which we do for ever and ever */

}	/* end of main */

void init_lists ()
{
int i, j;

for (i=0;i<MAXRUNIDS;i++) {
	notify_runids[i] = 0;
	notify_counts[i] = 0;
	for (j=0;j<MAXENTRIES;j++) {
		notify_list_gids[i][j] = 0;
		notify_list_addr[i][j] = 0;
		notify_list_port[i][j] = 0;
		}
	}
}


int addme (int *tags, int mb)
{
int i,j;
int list, slot;

if (tags[1] < 0) return (-1); /* Bad run ID */
if (tags[2] < 0) return (-1); /* Bad GID */

/* find list */
list =-1;
for (i=0;i<MAXRUNIDS;i++) 
	if (notify_runids[i]==tags[1]) { list = i; break; }

if (list==-1) { /* search for free instead */
	for (i=0;i<MAXRUNIDS;i++) 
		if (!notify_runids[i]) { 
			notify_runids[i] = tags[1];	/* my list now */
			list = i; 
			break; 
			}
	if (list==-1) { /* no space for new runid! */
		if(!daemon_mode) printf("ftmpi_notifier:addme:no room for new runid of %d\n", tags[1]);
		return (-1);
		}
}	

/* ok have list now find slot */
/* first check for me being here already */
/* find list */
slot =-1;
for (j=0;j<MAXENTRIES;j++) 
	if (notify_list_gids[list][j]==tags[2]) { 
		/* if we are in the list, one of two things */
		/* (A) we have a blank record (addr,port=0) or (B) we are in the list */
		/* either case we just update and continue */
		if (notify_list_addr[list][j])	{ /* B */
		   if (tags[3]) /* overwriting with good info */
			if(!daemon_mode) printf("ftmpi_notifier:addme:%d updating nonblank record in list %d for runid %d\n",
				tags[2], list, tags[1]);
		   else /* overwriting valid with blank record */
		   {
			if(!daemon_mode)  {
                           printf("ftmpi_notifier:addme:%d WARNING overwriting nonblank record with blank record in list at %d for runid %d\n",
				tags[2], list, tags[1]);
		   	   printf("Overwrite attempt being ignored\n");
                         }
			free_msg_buf ( mb );
			return (0);
			}
		}
		else 
			if(!daemon_mode) printf("ftmpi_notifier:addme:%d updating blank record in list %d for runid %d\n",
				tags[2], list, tags[1]);
		notify_list_gids[list][j] = tags[2];
		/* notify_list_addr[list][j] = tags[3]; */
		notify_list_port[list][j] = tags[3];
		upk_raw32 ( mb, &(notify_list_addr[list][j]), 1);
		
		free_msg_buf ( mb );
		return (0);
		}

/* now find empty slot */
for (j=0;j<MAXENTRIES;j++) 
	if (!notify_list_gids[list][j]) { slot=j; break; }

if (slot==-1) { /* no space for new gid! */
		if(!daemon_mode) printf("ftmpi_notifier:addme:no free slot for new gid of %d\n", tags[2]);
		return (-1);
		}

/* else we have a slot and list */

notify_list_gids[list][slot] = tags[2];
/*notify_list_addr[list][slot] = tags[3]; */
notify_list_port[list][slot] = tags[3];
upk_raw32 ( mb, &(notify_list_addr[list][slot]), 1);
free_msg_buf ( mb );

notify_counts[list]++;

return (0);
}

int removeme (int *tags)
{
int i,j;
int list, slot;

if (tags[1] < 0) return (-1); /* Bad run ID */
if (tags[2] < 0) return (-1); /* Bad GID */

/* find list */
list =-1;
for (i=0;i<MAXRUNIDS;i++) 
	if (notify_runids[i]==tags[1]) { list = i; break; }

if (list==-1) {
	if(!daemon_mode) printf("ftmpi_notifier:runid %d not in list\n", tags[1]);
	return (-1);
	}

/* find me */
slot =-1;
for (j=0;j<MAXENTRIES;j++) 
	if (notify_list_gids[list][j]==tags[2]) { slot = j; break; }

if (slot==-1) {
	if(!daemon_mode) printf("ftmpi_notifier:gid %d not in runid %d list\n",
			tags[2], tags[1]);
	return (-1);
	}
			
notify_list_gids[list][slot] = 0;
notify_list_addr[list][slot] = 0;
notify_list_port[list][slot] = 0;
notify_counts[list]--;

if (!notify_counts[list]) notify_runids[list] = 0;	/* if last clean up list */

return (0);	/* did ok */
}

int removeall (int *tags)
{
int i,j;
int list, slot;

if (tags[1] < 0) return (-1); /* Bad run ID */
if (tags[2] < 0) return (-1); /* Bad GID */

/* find list */
list =-1;
for (i=0;i<MAXRUNIDS;i++) 
	if (notify_runids[i]==tags[1]) { list = i; break; }

if (list==-1) {
	if(!daemon_mode) printf("ftmpi_notifier:runid %d not in list\n", tags[1]);
	return (-1);
	}

/* find me */
slot =-1;
for (j=0;j<MAXENTRIES;j++) 
	if (notify_list_gids[list][j]==tags[2]) { slot = j; break; }

if (slot==-1) {
	if(!daemon_mode) printf("ftmpi_notifier:gid %d not in runid %d list\n",
			tags[2], tags[1]);
	return (-1);
	}
			
/* ok, we can remove this list */
for (j=0;j<MAXENTRIES;j++) {
	notify_list_gids[list][j] = 0;
	notify_list_addr[list][j] = 0;
	notify_list_port[list][j] = 0;
	}
notify_counts[list]=0;
notify_runids[list]=0;

return (0);	/* all removed */
}

/* 
	here we so the signalling of other tasks when a process 
	bites the dust
*/
int exit_event (int *tags)
{
  int i,j;
  int list, slot;
  int nc = 0;	/* notify count attempted */
  int nok = 0;	/* notified ok */
  int fok = 0;	/* failed to notify */
  int s, p, rc;	/* connection sockets, ports and rcs */
  
  if(!daemon_mode) printf("EXIT: %d 0x%x %d\n", tags[1], tags[2], tags[3]); fflush(stdout);
  
  
  if (tags[1] < 0) return (-1); /* Bad run ID */
  if (tags[2] < 0) return (-1); /* Bad GID */
  
  /* find list */
  list =-1;
  for (i=0;i<MAXRUNIDS;i++) 
    if (notify_runids[i]==tags[1]) { list = i; break; }
  
  if (list==-1) {
    if(!daemon_mode) {
      printf("ftmpi_notifier:exit_event runid %d not in list???\n", tags[1]);
      printf("ftmpi_notifier:exit_event thus cannot notify someone.\n");
      return (-1);
    }
  }
  
  /* find exited */
  /* and remove it from the list as it doesn't need to be notified */
  
  slot =-1;
  for (j=0;j<MAXENTRIES;j++) 
    if (notify_list_gids[list][j]==tags[2]) { slot = j; break; }
  
  if (slot==-1) {	/* wasn't found, maybe a race so log it */
    if(!daemon_mode) printf("ftmpi_notifier:exit_event gid %d not in runid %d list [race maybe?]\n",
			    tags[2], tags[1]);
    return (-1);
  }
  else {	/* it was in the list */
    notify_list_gids[list][slot] = 0;
    notify_list_addr[list][slot] = 0;
    notify_list_port[list][slot] = 0;
    notify_counts[list]--;
    if (!notify_counts[list]) notify_runids[list] = 0;
    /* if last clean up list */
  }
  
  /* OK, if notifiy_counts >0 we have someone left to notify */
  if (notify_counts[list]) {
    for (j=0;j<MAXENTRIES;j++) {
      if (notify_list_addr[list][j]) {
	/* they have some info, try to tell em */
	nc++ ;
	p = notify_list_port[list][j];	
	s = getconn_addr (notify_list_addr[list][j], &p, 0);	/* note 0 */
	if (s<0) {/* connection attempt and thus notify atttemp failed */
	  closeconn(s);
	  fok++;
	}
	else { /* we have a socket, sent it to em. */
	  rc = send_pkmesg (s, NOTIFIER_ID_SERVER, 4, tags, 
			    EMPTYMSGBUF, 0);
	  if (rc>=0) nok++;	/* we think */
	  closeconn (s);
	}
      } /* if this entry has an address registered to notify */
    } /* loop for all entries in notify list of runid */
    if(!daemon_mode) printf("ftmpi_notifier:exit_event gid %d notifed %d outof %d\n",
			    tags[2], nok, nc);
    if (nok!=nc) 
      if(!daemon_mode) printf("ftmpi_notifier:exit_event gid %d: blank %d failed %d\n",
			      tags[2], nc-(nok+fok), fok);
  } /* if someone to notify */
  
  else /* no one to notify left, log it (last one switched the lights out) */
    if(!daemon_mode) printf("ftmpi_notifier:exit_event gid %d no one left to switch the lights out.\n", tags[2]);
  return 0; 
}



