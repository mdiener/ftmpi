
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:
 			Reed Wade <wade@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu> 
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	changed notify message to contain:
	<mid><gid><pid><exit code>
	or <who started me, the dead global ID, the process ID and its exit code 

	GEF STR 2001.
*/

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#include <regex.h>

#include <string.h>

#include <signal.h>

#include <stdarg.h>

#include <netdb.h>
#include <time.h>
#include "startup.h"
#include "libstartup.h"

#ifdef BRAINDEADONLY
#include "../services/ns_lib.c"
#endif /* TONE */
/* Tone get it right, its not brain dead its a needed service and I am still */
/* waiting on yours to be finished. So stop insulting others people code */
/* and get your right first buster. */
/* Graham STR 2001 */

#ifdef FTMPI_NOTIFY
	#include "notifier.h"
#endif /* FTMPI_NOTIFY */

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "libsio.h"

/** for abort message when seg fault **/
#include "ns_lib.h"
#include "../ftmpi/ft-mpi-modes.h"
/** for abort message when seg fault **/


#define NOW (time((time_t)0))

#define MAX_PROC 256

int GROUP,MEMBER;
int myid=0;

int daemon_mode = 0;

extern char **environ;

extern char virtual_machine_name[512];

static char *stateDir = "/tmp/startup_%d";

static struct startupItem *newItem();

static int pipes[2];

struct rio {
   int pid;
   int pp[2]; /* stdout */
   int pp2[2]; /* stderr */
} rios[MAX_PROC];
static int lowest_slot=-1;
static int nprocs;


/* the freak list is a set of chidren that exit before we've recorded them in the process list */
static int *freakList;
static int *freakListStat;
static int freakListLength = 0;
static int freakListCapacity = 20;

struct startupItem *itemList = (struct startupItem*)0;


/* static void unblockChildHandler(); */
/* static void blockChildHandler(); */

static void freeItem(struct startupItem *item);

static void addItemCallback(struct startupItem *item, int id);
static void removeItemCallback(struct startupItem *item, int id);
char *statusToString(int status);
int add_self_to_ns(char *vmname,int port);

static void setChildHandler();
static void childHandler();

static int regmatch(const char *string, char *pattern);

static int getFreak(int pid);
static void addFreak(int pid, int returnStatus);

static void handleProcess(struct startupItem *item);


/* GEF from startup.h */
int _ATB_BLOCK = 0;
int _ATB_ACPTNG = 0;

int startupAdd( int callerId, int gid, char* comment, int out_ip, int out_port, ... )
{
  va_list ap;
  char *cwdir, *path;
  int i;
  pid_t pid;
  struct startupItem *item;
  char **argv;
  char **envp;
  char seps[]="/";
  char temp[512];
  char bin_dir[256];
  char arch[256];
  int count = 0;
  char *token = NULL;
  int startup_silent=0;

#ifdef OLD_LOOKUPCODE  
  int t0,t1,e
  char outputname[256];
#endif
  int entry,ret,needoutput;
  char buf[256];
  char *rstring;

  va_start(ap, out_port);

  path = va_arg(ap, char *);
  cwdir = va_arg(ap, char *);
  argv = va_arg(ap, char **);
  envp = va_arg(ap, char **);

/*   printf("%s\n\n",path); */
  strcpy(temp,path);

  token = strtok( path, seps );
  while(token){
/*     printf("%s\n",token); */
    if(strcmp(token,"..") == 0){
      count--;
      if(count < 0){
        if(!daemon_mode) {
            printf("PANIC >>> GOING BELOW BIN DIR : %s <<< PANIC\n",temp);
            printf("REFUSING TO EXECUTE BINARY !!!!!!!!!\n");
	}
        return(-11);
      } 
    }  
    else if(strcmp(token,".") != 0){
      count++;
    }
    else {
    }
    token = strtok( NULL, seps );
  }

  /* get silent flag */ 
  rstring = strstr( envp[12],"=");
  startup_silent = atoi(rstring+1);
   

  strcpy(path,temp);
  item = newItem();
  item->execName = (char*)malloc(strlen(path)+1);
  strcpy(item->execName, path);

  item->comment = (char*)malloc(strlen(comment)+1);
  strcpy(item->comment, comment);

  item->cwdir = (char*)malloc(strlen(cwdir)+1);
  strcpy(item->cwdir, cwdir);

  item->startTime = NOW;
  item->caller = callerId;
  item->gid = gid;
  item->postabort = 0;
  item->status = STATUS_RUNNING;

  while (0 != (i = va_arg(ap, int))) {
    addItemCallback(item, i);
  }
  va_end(ap);

/*   blockChildHandler(); */


  /* request for redirect IO address/port */

#ifdef OLD_LOOKUPCODE
   sprintf(outputname, "HARNESS:output:0x%x", callerId);
   ns_open ();
   needoutput = ns_info (outputname, 1, &t0, &t1, &e, &out_ip, &out_port);
   ns_close ();
#else
   needoutput=0;
   if(out_ip!=0 && out_port!=0) { /* require redirect output */ 
      needoutput=1;
   }
   /*
   printf("Needoutput is %d, out_ip is %d outport is %d\n",needoutput,out_ip,out_port);
   fflush(stdout);
   */
#endif
   if(needoutput==1) { /* require redirect output */ 
       if (lowest_slot!=-1) {
	  entry=lowest_slot; 
	  /* find next free slot */
	  lowest_slot=-1; /* in case we don't find one */
	  if (nprocs!=(MAX_PROC-1)) { /* we can search */
              if ((entry+1)<MAX_PROC) /* if we can search */
	          for(i=entry+1;i<MAX_PROC;i++)
	             if (rios[i].pid==-1) {
	                  lowest_slot = i;
	                  break;
	             }
          }
       } else {
          for (i=0;i<MAX_PROC;i++) {
             if (rios[i].pid==-1) { 
		entry=i;
	       	break; 
	     } else {
                if(i==MAX_PROC-1) {
                    if(!daemon_mode) {
                       printf("[%s:%d] All %d parking spaces are full. Are you come to UT @10am?\n",__FILE__,__LINE__,MAX_PROC);
		    }
		    entry=-1;
	     	}
	     }
          }
       }

       if(entry!=-1) {
          nprocs++;
          if (pipe(rios[entry].pp) == -1) {
             perror("pipe");
          }
          if (pipe(rios[entry].pp2) == -1) {
             perror("pipe");
          }

          sprintf(buf,"%d:stdout",gid);
          ret=rios_register_pipe2_addr_port(buf,rios[entry].pp[0],out_ip,out_port,startup_silent);
          if(ret!=RIO_SUCCESS) {
             if(!daemon_mode) {
                 printf("Cannot connect to %d:%d\n",out_ip,out_port);
	     }
          }

          sprintf(buf,"%d:stderr",gid);
          ret=rios_register_dup(buf,rios[entry].pp[0],rios[entry].pp2[0],startup_silent);
          if(ret!=RIO_SUCCESS) {
             if(!daemon_mode) {
                printf("Cannot duplicate destination for stderr\n");
	     }
          }

       }
   }

   pid = fork();

/*   unblockChildHandler(); */

#ifdef VERBOSE 
  if(!daemon_mode) {
     printf(">>>>> %d\n",pid); fflush(stdout);
  }
#endif

  if (pid == 0) {
    /* in child */


    if(needoutput==1) {
        dup2(rios[entry].pp[1],1);
        dup2(rios[entry].pp2[1],2);
    }  else {
	/* Prevent child process print something to interfere with childHandler system */
        if(daemon_mode) {  
           close(0);
           close(1);
           close(2);
	}
    }

    strcpy(bin_dir,getenv("HARNESS_BIN_DIR"));
    strcpy(arch,getenv("HARNESS_ARCH"));

/*     printf("I AM A CHILD ... LONG LIVE THE MIDGETS ... the little midgets\n"); */
#ifdef VERBOSE
  if(!daemon_mode) {
    printf("Child process execing the target binary from the HARNESS_BIN_DIR [%s]\n", bin_dir);
    fflush(stdout);
  }
#endif

/*     printf("bin_dir: %s\n",bin_dir); */
/*     printf("arch   : %s\n",arch); */
/*     printf("path   : %s\n",path); */

    strcpy(temp,bin_dir);
    strcat(temp,"/");
    strcat(temp,arch);
    strcat(temp,"/");
    strcat(temp,path);

    /*
    printf( "starting from |%s|\n", temp );
    fflush(stdout);
    */

    if(strcmp(cwdir,"NONE")) { /* i.e. specific -cwd */
       if(chdir(cwdir)==-1) { 
       /* We don't exit here because cwdir may not the same on different machine */
/*
          FILE *fp;
          char f[1024];

          sprintf(f, "%s/failed_%09d", stateDir, getpid());

          fp = fopen(f, "w");
          fprintf(fp, "%d\n%s\n", errno, strerror(errno));
          fclose(fp);
          _exit(127);
*/
       }
    }
  
/*     printf("NEW PATH : %s\n",temp); */

    /* complete the actual environment with all variables supplied to the current function */
    while( *envp != NULL ) {
      putenv( *envp );
      envp++;
    }

    i = 0;

    if (-1 == execv(temp, argv)) {
      FILE *fp;
      char f[1024];

      sprintf(f, "%s/failed_%09d", stateDir, getpid());

      fp = fopen(f, "w");
      fprintf(fp, "%d\n%s\n", errno, strerror(errno));
      fclose(fp);
      _exit(127);
    }
    
    /* not reached */
    _exit(0);

  } else if (pid == -1) {
    freeItem(item);
    if(!daemon_mode) {
       printf("fork failed (%s)\n", strerror(errno));
       fflush(stdout);
    }
    return(-1 * errno);
  }


   if(needoutput==1) { /* require redirect output */ 
      rios[entry].pid=pid;
      close(rios[entry].pp[1]); 
      close(rios[entry].pp2[1]); 
      rios[entry].pp[1] = -1;
      rios[entry].pp2[1] = -1;
   }


/*   printf(" >>>>> I AM THE PARENT AND THIS IS MY MIDGET CHILD %d\n",pid); fflush(stdout); */

  /* add this item to the list */

/*   printf("TRYING TO BLOCK THE CHILD ..\n"); */
/*   blockChildHandler(); */
/*   printf("CHILD IS BLOCKED ..\n"); */
  item->next = itemList;
  itemList = item;
  item->pid = pid;
/*   unblockChildHandler(); */

  /* check race condition: it's possible that the sigchld handler ran 
	before we recorded our pid */

  i = getFreak(pid);

  if (i != -1) {
	item->exitValue = freakListStat[i];
    handleProcess(item);
    freakList[i] = -1;
  }


  return (int)pid;
}




/*
void startupMonitor(pid, [id, ...] , 0)
*/
int startupMonitor( pid_t pid, ... )
{
  va_list ap;
  struct startupItem *item;
  int i;

  if( !(item = startupGetItemByPid(pid)) ) return 0;

  va_start(ap, pid);

  while (0 != (i = va_arg(ap, int))) {
    addItemCallback(item,  i);
  }
  va_end(ap);
  return 1;
}

/*
void startupUnMonitor(pid, [id, ...] , 0)
*/
int startupUnMonitor(pid_t pid, ... )
{
  va_list ap;
  struct startupItem *item;
  int i;

  if( !(item = startupGetItemByPid(pid)) ) return 0;

  va_start(ap, pid);

  while (0 != (i = va_arg(ap, int))) {
    removeItemCallback(item,  i);
  }
  va_end(ap);
  return 1;
}

/*
 * startupInit() should be called once before any other startup function
 */
void startupInit( void ) {
  static char newStateDir[255];
  char s[255];
  FILE *fp;
  struct stat statbuf;
  int i;

  signal(SIGPIPE,SIG_IGN); /* just in case */

  /* create the state dir if needed*/
  sprintf(newStateDir, stateDir, geteuid());
  stateDir = newStateDir;
  if (-1 == stat(stateDir, &statbuf)) {
    /* can't stat, try to create it and assume the parent dirs do exist */
    if (-1 == mkdir(stateDir, 0755)) {
      perror("startup (trying to create state directory)");
      exit(1);
    }
  }

  /* write my own pid in the state directory */
  strcpy(s, stateDir);
  strcat(s, "/mypid");
  fp = fopen(s, "w");
  if (!fp) {
    perror("startup (trying to write my pid file)");
    exit(1);
  }
  fprintf(fp, "%d\n", getpid());
  fclose(fp);

  freakListCapacity = 10;
  freakList = (int*)malloc(10 * sizeof(int));
  freakListStat = (int*)malloc(10 * sizeof(int));

  /* open pipe used by sig handler to record pids */
  pipe(pipes);
  i = fcntl(pipes[0], F_GETFL, 0);
  i |= O_NONBLOCK;
  fcntl(pipes[0], F_SETFL, i);

  setChildHandler();

  for(i=0;i<MAX_PROC;i++) {
     rios[i].pid=-1;
     rios[i].pp[0]=-1;
     rios[i].pp[1]=-1;
     rios[i].pp2[0]=-1;
     rios[i].pp2[1]=-1;
  }
  nprocs=0;
  lowest_slot=-1;
  rios_init();
}


int startupGetStatus(struct startupItem *item) {
  return (item->status);
}

int startupGetStatusByPid(int pid) {
  struct startupItem *item;

  if (item = startupGetItemByPid(pid)) {
    return (item->status);
  } else {
    return (STATUS_INVALID);
  }
}

void startupPrintItem(FILE *fp, struct startupItem *item) {
  int i;

  fprintf(fp, "Item:");
  fprintf(fp, " pid: %d", item->pid);
  fprintf(fp, " caller: %d", item->caller);
  fprintf(fp, " path: %s", item->execName);
  fprintf(fp, " comment: %s\n", item->comment);

  fprintf(fp, "  status:%d (%s)", item->status, statusToString(item->status));
  if (item->status == STATUS_FAILED_ON_EXEC) {
    /* show errno */
    fprintf(fp, " (%s)", strerror(WEXITSTATUS(item->exitValue)));
  } else {
    if (WIFEXITED(item->exitValue)) {
      fprintf(fp, " value: %d", WEXITSTATUS(item->exitValue));
    } else if (WIFSIGNALED(item->exitValue)) {
      fprintf(fp, " signal: %d", WTERMSIG(item->exitValue));
    }
  }

  fprintf(fp, " start,stop: %d,%d (%ds)\n", 
		item->startTime, item->stopTime,
		(item->stopTime == 0) ?
		(NOW - item->startTime)  :
		(item->stopTime - item->startTime) 
		);

  fprintf(fp, "  callbacks: ");
  if (item->callbackListLength)  {
    for (i=0; i<item->callbackListLength; i++) {
      if (item->callbackList[i] != -1) 
        fprintf(fp, "%d ", item->callbackList[i]);
    }
    fprintf(fp, "\n");
  } else {
    fprintf(fp, "none\n");
  }
}

char *statusToString(int status) {
  switch (status) {
    case STATUS_INVALID:
      return "invalid";
    case STATUS_RUNNING:
      return "running";
    case STATUS_EXITED:
      return "exited";
    case STATUS_FAILED_ON_EXEC:
      return "failed on exec";
    default:
      return "unknown status code";
  }
}

void startupPrintAll(FILE *fp, int status) {
  struct startupItem *item;

/*   blockChildHandler(); */
  item = itemList;
/*   unblockChildHandler(); */
  for (; item; item = item->next) {
    if ((status == STATUS_ALL) || (status == item->status)) {
      startupPrintItem(fp, item);
    }
  }
}



static void setChildHandler() {
  struct sigaction act;

#ifdef VERBOSE
  if(!daemon_mode) printf(" -----  Setting CHILD HANDLER ...\n");
#endif
  act.sa_handler = childHandler;
  sigemptyset(&act.sa_mask);
  act.sa_flags = 0;

  sigaction(SIGCHLD, &act, (struct sigaction*)0);
}

static void childHandler() {
  int v[2];
  int i;
  struct startupItem *item;

#ifdef VERBOSE 
  if(!daemon_mode) printf(" ---- in a CHILD HANDLER ----\n");
#endif
  if(_ATB_ACPTNG == 0 && _ATB_BLOCK == 1){
    if(!daemon_mode) {
       printf("BUMMER BUMMER ---- playing with BLOCKED data %d %d\n",_ATB_ACPTNG,_ATB_BLOCK);
       fflush(stdout);
    }
    exit(-9);
  }
  while ((v[0] = waitpid(-1, &v[1], WNOHANG)) > 0) {
#ifdef VERBOSE 
    if(!daemon_mode) {
       printf(" ---- %d %d ----\n",v[0],v[1]);
       fflush(stdout);
    }
#endif
    v[1] = v[1]%128;

    for (i=0;i<MAX_PROC;i++) {
       if (rios[i].pid==v[0]) { 
	  rios[i].pid=-1; 
	  rios[i].pp[0]=-1; 
	  rios[i].pp[1]=-1; 
	  rios[i].pp2[0]=-1; 
	  rios[i].pp2[1]=-1; 
	  nprocs--;
	  break; 
       }
    }
    if(lowest_slot > i) {
       lowest_slot=i; 
    }
    if(nprocs==0) {
       lowest_slot=0;
    }

#ifdef FTMPI_NOTIFY
	/* GEF changed message proto XXX */

	item = startupGetItemByPid(v[0]); /* get the complete record */
	if (!item) { /* ops cannot tell anyone */	
            if(!daemon_mode) {
		printf("PID %d died silently in the snow\n", v[0]);
	    }
	}
	else {
		notifier_send_exit_event (myid, item->caller, item->gid, v[1]);
		}

#endif	/* FTMPI NOTIFY */

#ifdef VERBOSE
    if(!daemon_mode) {
        printf("WRITING TO PIPES .. \n");
    }
#endif


     write(pipes[1], v, 2*sizeof(int)); 
  }
}

void startupLoopOnce() {
  int v[2];
  struct startupItem *item;

  while ((2*sizeof(int)) == read(pipes[0], v, 2*sizeof(int))) { 
      if (item = startupGetItemByPid(v[0])) {
        item->exitValue = v[1];
	    /* notify everyone on the callback list */
		handleProcess(item);
      } else {
        /* we don't have a record for this pid yet */
        addFreak(v[0], v[1]);
      }
  }
}

/* static void blockChildHandler() { */
void blockChildHandler() {
  sigset_t sigset;
  sigemptyset(&sigset);
  sigaddset(&sigset, SIGCHLD);
  sigprocmask(SIG_BLOCK, &sigset, (sigset_t*)0);
  _ATB_BLOCK = 1;
  _ATB_ACPTNG = 0;
}

/* static void unblockChildHandler() { */
void unblockChildHandler() {
  sigset_t sigset;
  _ATB_BLOCK = 0;
  _ATB_ACPTNG = 1;
  sigemptyset(&sigset);
  sigaddset(&sigset, SIGCHLD);
  sigprocmask(SIG_UNBLOCK, &sigset, (sigset_t*)0);
}




struct startupItem *newItem() {
  struct startupItem *item;
  item = (struct startupItem *)malloc(sizeof(struct startupItem));

  item->execName = (char*)0;
  item->comment = (char*)0;
  item->caller = 0;
  item->gid = 0;
  item->pid = 0;
  item->exitValue = 0;
  item->status = STATUS_INVALID;
  item->startTime = 0;
  item->stopTime = 0;
  item->exitValue = 0;
  item->postabort = 0;

  item->callbackListCapacity = 5; 
  item->callbackList = (int*)malloc(item->callbackListCapacity * sizeof(int));
  item->callbackListLength = 0;

  item->next = (struct startupItem*)0;

  return item;
}

static void freeItem(struct startupItem *item) {
  if (item) {
      if (item->execName) free(item->execName);
      if (item->comment) free(item->comment);
      if (item->callbackList) free(item->callbackList);
      free(item);
  }
}

int getItemStatus(struct startupItem *item) {
  return (item->status);
}

/*
 * we don't actually try to reshuffle the list and reclaim space, 
 * just mark the entry as nonvalid
 */
static void removeItemCallback(struct startupItem *item, int id) {
  int i;

  for (i=0; i < item->callbackListLength; i++) {
    if (item->callbackList[i] == id) {
      item->callbackList[i] = -1;
    }
  }
}

static void addItemCallback(struct startupItem *item, int id) {

  /* grow the list, if needed */
  if (item->callbackListCapacity == item->callbackListLength) {
    item->callbackListCapacity *= 2;
    item->callbackList = (int*)realloc(item->callbackList, item->callbackListCapacity * sizeof(int));
  }

  item->callbackList[ item->callbackListLength++ ] = id;

}


struct startupItem *startupGetItemByPid(int pid) {
  struct startupItem *item;

/*   blockChildHandler(); */
  item = itemList;
/*   unblockChildHandler(); */
  for (; item && (item->pid != pid); item = item->next) {
    /* spin */
  }
  return (item);
}

struct startupItem *startupGetNextItemByCaller(struct startupItem *item, 
	int caller) 
{
  if (!item) {
/*     blockChildHandler(); */
    item = itemList;
/*     unblockChildHandler(); */
  }
  for (; item && (item->caller != caller); item = item->next) {
    /* spin */
  }
  return (item);
}

struct startupItem *startupGetNextItemByName(struct startupItem *item, 
	char*s, int isRegex, int which) 
{
  if (!item) {
/*     blockChildHandler(); */
    item = itemList;
/*     unblockChildHandler(); */
  } else
    item = item->next;

  if (!item)
      return ((struct startupItem *)0);

  if (isRegex) {

    do {
      if (which == SEARCH_EXECNAME || which == SEARCH_BOTH) {  
        if (regmatch(item->execName, s)) {
		  return (item);
        }
      }
      if (which == SEARCH_COMMENT || which == SEARCH_BOTH) {  
        if (regmatch(item->comment, s)) {
		  return (item);
        }
      }
    } while (item = item->next);

  } else {
    do {
      if (which == SEARCH_EXECNAME || which == SEARCH_BOTH) {  
        if (strcmp(s, item->execName) == 0)
		  return (item);
      }
      if (which == SEARCH_COMMENT || which == SEARCH_BOTH) {  
        if (strcmp(s, item->comment) == 0)
		  return (item);
      }
    } while (item = item->next);
  }

  return (item);
}

/* new stuff from TA */
/**
 *  startupGetNextItem - get next item of item list
 *   @param item current item
 *    @return vnext item
 *     */
struct startupItem *startupGetNextItem(struct startupItem *item)
{
if (!item) {
	item = itemList;
} else {
    item = item->next;
}

if (!item) {
    return ((struct startupItem *)0);
}
    return (item);
}

/**
 *  startupGetNumberOfItem - get number of Item
 *   @return number of item
 *    */
int startupGetNumberOfItem(void) 
{
struct startupItem *item;
int nItem =0;

item = itemList;
for(; item ; item = item->next) {
   nItem++;
}
return nItem;
}

/**
 * is_proc_running - Is processes running? 
 @retval 1 yes
 @retval 0 no
*/
int is_proc_running(void) {
struct startupItem *item;

item = itemList;
for(; item ; item = item->next) {
   if(item->status==STATUS_RUNNING) {
      return 1;
   }
}
return 0;
}




int regmatch(const char *string, char *pattern) {
  int status;
  regex_t re;
  if (regcomp(&re, pattern, REG_EXTENDED|REG_NOSUB) != 0) {
    return(0);      /* report error */
  }
  status = regexec(&re, string, (size_t) 0, NULL, 0);
  regfree(&re);
  if (status != 0) {
    return(0);      /* report error */
  }
  return(1);
}

int startupExpireItems(int age) {
  struct startupItem *item;
  struct startupItem *lastGoodItem;
  struct startupItem *nextItem;
  time_t t;
  int found;

  found = 0;
  t = NOW - age;



  lastGoodItem = (struct startupItem*)0;

/*   blockChildHandler(); */

  for (item=itemList; item; item = nextItem) {
    nextItem = item->next;
    if (item->stopTime && (item->stopTime < t)) {
	    found++;
      /* reset the pointer that got us here */
      if (lastGoodItem) 
        lastGoodItem->next = item->next;
      else
        itemList = item->next;

      /* now free the item and it's contents */
      freeItem(item);
       
    } else {
      lastGoodItem = item;
    }
  }
	return (found);
/*   unblockChildHandler(); */
}



static void handleProcess(struct startupItem *item) {
  char f[1024];
  int i;
  struct stat statbuf;
  int err;

  item->stopTime = NOW;

  item->status = STATUS_EXITED;

  /* if the exit code is 127 it might be a failed on exec */

  if (WEXITSTATUS(item->exitValue) == 127) {
    /* check for a failure-on-exec file */
    sprintf(f, "%s/failed_%09d", stateDir, item->pid);

    if (-1 != stat(f, &statbuf)) {
      FILE *fp = fopen(f, "r");
      char line[255];

      if (fp) {
        fgets(line, 255, fp);
        item->exitValue -= (127 << 8);
        item->exitValue += (atoi(line) << 8);
      }


      unlink(f);
      item->status = STATUS_FAILED_ON_EXEC;
    }
  }

  for (i=0; i<item->callbackListLength; i++) {
    if (item->callbackList[i] != -1)  {
	  if(daemon_mode==0) {
	       printf("CALLING %d: %d has status %d, exit code %d\n", 
		  item->callbackList[i], item->pid, item->status, item->exitValue);
	  }
    }
  }

   if (WIFSIGNALED(item->exitValue)) {
      if(WTERMSIG(item->exitValue)==11) {
          /** Segmentation fault!!. We will terminate whole job **/
	  /** to prevent forever recovery disaster **/
          
	  /** post abort to whole caller ID  **/
	 /** (We don't need any phone number here...so we won't need the callerID ) **/
       
         if(item->postabort==0) {
             {
                 /* we need a fork here. unless we have startup_kill_but_me() and then kill_my_child()  */
                 int pid;
                 pid=fork();
		 if(pid==0) {
                    startup_finder_vm(virtual_machine_name);
	            startup_kill(item->caller,-1,0,2,&err);
		    exit(1);
		 } else {
	            item->postabort=1;
		 }
	     }
	 }
      }
   }



}


static int getFreak(int pid) {
  int i;
  for (i=0; i<freakListLength; i++) { 
    if (pid == freakList[i])
      return (i);
  }
  return (-1);
}

static void addFreak(int pid, int returnStatus) {
  int i;

  /* first, try to find an unused entry */

  i = getFreak(-1);
  if (i != -1) {
    freakList[i] = pid;
    freakListStat[i] = returnStatus;
    return;
  }

  /* grow list if needed */
  if (freakListLength == freakListCapacity) {
    freakListCapacity *= 2;
    freakList = (int*)realloc(freakList, freakListCapacity * sizeof(int));
  }

  freakListStat[freakListLength] = returnStatus;
  freakList[freakListLength++] = pid;
}

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/
/* this routine now returns the GID of this service in pf bin format */

int add_self_to_ns(char *vmname,int port)
{
  char virtual_machine_name[512];
  u_long host_addr;
  int p, f;     /* used to make an id (so we don't have to include id.c) */



/*   ns_init(getenv("HARNESS_NS_HOST"),atoi(getenv("HARNESS_NS_HOST_PORT")));
*/
	/* we are already connected ? */
 
  host_addr = get_my_addr (); /* Its in the snipe lib  GEF */

  sprintf(virtual_machine_name,"%s:ftmpi:services:startup_notify_service",vmname);
  ns_open ();
  ns_add(virtual_machine_name,host_addr,port,&GROUP,&MEMBER);
  ns_close();

  /* make an id for us (same as pf_bin func) */
  /* this id is used by FTMPI_NOTIFY as a rouge GID */
  p = GROUP; 
  f = MEMBER;
  p = p & (0xFFFF);
  p = p<<16;
  f = f & (0xFFFF);
  myid = p | f;
  return (myid);	/* as myid is a global only in this file */
}

void rm_from_ns (char *vm)
{
  char grpnm[256];
  sprintf(grpnm,"%s:ftmpi:services:startup_notify_service",vm);
  ns_open ();
  (void)ns_del(grpnm,GROUP,MEMBER);
  ns_close ();
}

/* am_i_alive - ask the name service that am I really alive? 
@param vmname virtual machine name
@param port port
@retval 1 yes
@retval 0 no
*/
int am_i_alive(char *vmname,int port)
{
    u_long host_addr;
    int rc;
    char virtual_machine_name[512];

    if(!is_proc_running()) { /* no one is running */
         host_addr = get_my_addr();
         sprintf(virtual_machine_name,"%s:ftmpi:services:startup_notify_service",vmname);
         ns_open ();
         rc=ns_ismember(virtual_machine_name,host_addr,port);
         ns_close();
	 return rc;
    }
    return 1;
}

/* add_id_to_ns -  Add ID information to NS
@param vmname virtual machine name
@param port port of startup daemon
@param d ID (global process ID or runID)
*/
/*
void add_id_to_ns(char *vmname,int port,int id)
{
  char virtual_machine_name[512];
  u_long host_addr;
  int group, member;


  host_addr = get_my_addr ();

  sprintf(virtual_machine_name,"%s:%d",vmname,id);
  ns_open ();
  ns_add(virtual_machine_name,host_addr,port,&group,&member);
  ns_close();
}

*/







