
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS FTMPI_NOTIFIER 

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	This library contain notifiy registration functions and routines
	to handle events
	GEF STR 2002
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#ifndef WIN32
#include "snipe_lite.h"	/* get all we need */
#else
#include "../../wincomm/wincomm.h"	/* get all we need */
#endif
#include "ns_lib.h"	/* get all we need */
#include "msgbuf.h"	/* get all we need */

#include "notifier.h"	/* get protos */

/* notifier contact stuff */
int notifier=0;
int naddr;
int nport;

/* event list stuff */
int event_count=0;
int events_free=MAXEVENTLOOP;
int event_next=0;
int event_last=0;

int event_list_gids[MAXEVENTLOOP];
int event_list_rc[MAXEVENTLOOP];


/* returns 0 if ok, -1 if not */
/* will oneday find all of them rather than just the first */
int	notifier_find ()
{
char notifyname[256];
int t0, t1;
int e, a, p;
int rc;

    if (notifier) return (0); 
    sprintf(notifyname, "ftmpi:services:notifier\0");
    ns_open ();
    rc = ns_info (notifyname, 1, &t0, &t1, &e, &a, &p);
    ns_close ();

	if (rc!=1) return (-1);

	/* save results */
	naddr = a;
	nport = p;

	notifier=1;

	return (0);
}


/* 
 This routine adds me to the notifier list of those needing to be
notified. We send the contact info to save it having to look it up
in the NS. Yes teh startup_ds can do this but I prefer ordering my
event notification through the ftmpi_notifier instead.
The startup_ds notify lists will be used to so fanouts to 
multiple ftmpi-notifiers.

The notifier will notify me if anyone else in the same runid
exits.
*/

int	notifier_addme (int runid, int gid, int addr, int port) 
{
int mb;	/* my message buffer */
int s;
int p;
int tags[5];
int ntag;
int rc;
int from;

if (!notifier) return (-1);	/* no notifier yet */

/* get connection to notifier */
p = nport;	/* make tmp copy */
s = getconn_addr(naddr, &p, 0);	/* note 0 */

if (s<0) return (-1);	/* cannot connect */

tags[0] = NOTIFIER_PROTO_ADDME;
tags[1] = runid;
tags[2] = gid;
/*tags[3] = addr; */
tags[3] = port;
tags[4] = NOTIFIER_PROTO_END;

mb = get_msg_buf_of_size ( sizeof(int), 1, 0 ); /* resizable */
pk_raw32 ( mb, &addr, 1 );

send_pkmesg (s, gid, 4, tags, mb, 0);	/* send req */
free_msg_buf ( mb ); 

ntag = 3;
rc = recv_pkmesg (s, &from, &ntag, tags, NULL);	/* recv ack */

closeconn (s);	/* clean up sockets */

if (rc<0) return (-1);
if (ntag!=2) return (-1);
if ((tags[0]!=NOTIFIER_PROTO_ADDME)||(tags[1]!=gid)) return (-1);
return (tags[2]);	/* else added maybe */
}



int	notifier_removeme (int runid, int gid) 
{
int s;
int p;
int tags[6];
int ntag;
int rc;
int from;

if (!notifier) return (-1);	/* no notifier yet */

/* get connection to notifier */
p = nport;	/* make tmp copy */
s = getconn_addr(naddr, &p, 0);	/* note 0 */

if (s<0) return (-1);	/* cannot connect */

tags[0] = NOTIFIER_PROTO_REMOVE;
tags[1] = runid;
tags[2] = gid;
tags[3] = NOTIFIER_PROTO_END;

send_pkmesg (s, gid, 3, tags, EMPTYMSGBUF, 0);	/* send req */

ntag = 3;
rc = recv_pkmesg (s, &from, &ntag, tags, NULL);	/* recv ack */

closeconn (s);	/* clean up sockets */

if (rc<0) return (-1);
if (ntag!=2) return (-1);
if ((tags[0]!=NOTIFIER_PROTO_REMOVE)||(tags[1]!=gid)) return (-1);
return (tags[2]);	/* else removed ok */
}

/**
notifier_check - check alive of the notifier
@param from who am I?
*/

int notifier_check(int from)
{
int s;
int tags[6];
int ntag;

char notifyname[256];
int t0, t1;
int e, a, p;
int rc;

/* Don't trust data (port etc) from notifier_find. Because
 * It always report cache result */

    sprintf(notifyname, "ftmpi:services:notifier\0");
    rc=ns_open ();
    if (rc!=0) return (-1);
    rc = ns_info (notifyname, 1, &t0, &t1, &e, &a, &p);
    if (rc!=1) return (-1);
    ns_close ();

    /* save results */
    naddr = a;
    nport = p;
    notifier=1;

    /* get connection to notifier */
    p = nport;	/* make tmp copy */
    s = getconn_addr(naddr, &p, 0);	/* note 0 */

    if (s<0) return (-1);	/* cannot connect */
    tags[0] = NOTIFIER_PROTO_ACK;
    send_pkmesg (s, from, 1, tags, EMPTYMSGBUF, 0);	/* send req */
    ntag = 2;
    rc = recv_pkmesg (s, &from, &ntag, tags, NULL);	/* recv ack */
    closeconn (s);	/* clean up sockets */
    if (rc<0) return (-1);
    return 0;
}

/*
 This routine is used to remove all listeners and notify targets 
 from the notify service.
 This is called by ft_mpi_sys for MPI_Abort, MPI_Finalize etc 
*/

int	notifier_removeall (int runid, int gid) 
{
int s;
int p;
int tags[6];
int ntag;
int rc;
int from;

if (!notifier) return (-1);	/* no notifier yet */

/* get connection to notifier */
p = nport;	/* make tmp copy */
s = getconn_addr(naddr, &p, 0);	/* note 0 */

if (s<0) return (-1);	/* cannot connect */

tags[0] = NOTIFIER_PROTO_REMOVEALL;
tags[1] = runid;
tags[2] = gid;
tags[3] = NOTIFIER_PROTO_END;

send_pkmesg (s, gid, 3, tags, EMPTYMSGBUF, 0);	/* send req */

ntag = 3;
rc = recv_pkmesg (s, &from, &ntag, tags, NULL);	/* recv ack */

closeconn (s);	/* clean up sockets */

if (rc<0) return (-1);
if (ntag!=2) return (-1);
if ((tags[0]!=NOTIFIER_PROTO_REMOVEALL)||(tags[1]!=gid)) return (-1);
return (tags[2]);	/* else removed ok */
}

int notifier_clear_all_events ()
{
int i;
int howmanyclrd;

howmanyclrd = event_count;

for (i=0;i<MAXEVENTLOOP;i++) {
	event_list_gids[i] = 0;
	event_list_rc[i] = 0;
	}

event_count=0;
events_free=MAXEVENTLOOP;
event_next=0;				/* next to read off */
event_last=0;				/* last to put on */

return (howmanyclrd);	/* knowing how many can prevent races */
}

int dump_event_loop ()
{
int i;
printf("Event loop contents: # of events %d fee %d\n", event_count, events_free);
if (!event_count) return 0;
for(i=0;i<MAXEVENTLOOP;i++)
	if (event_list_gids[i]) 
		printf("GID [0x%x] exitcode [%d]\t", event_list_gids[i], event_list_rc[i] );

printf("\n\n");
return 0;
}

/*
 Search for event by its GID 
 If found remove from event loop
*/

int remove_event_by_gid (int id) 
{
int i;
for (i=0;i<MAXEVENTLOOP;i++) 
	if(event_list_gids[i]==id) {
		event_list_gids[i]=0;
		event_list_rc[i] = 0;
		event_count--;
		events_free++;
		return(1);
		}
return (0);
}

/*
 Search for event by its GID 
*/

int find_event_by_gid (int id) 
{
int i;
for (i=0;i<MAXEVENTLOOP;i++) if(event_list_gids[i]==id) return(1);
return (0);
}

int add_event_to_loop (int id, int rc)
{

if (!events_free) return (-1);	/* no space */
event_count++;
events_free--;
event_last++;
if (event_last==MAXEVENTLOOP) event_last = 0;	/* loop */
event_list_gids[event_last] = id;
event_list_rc[event_last] = rc;
return (0);
}

/* this gets a set of events if they are avaliable */
/* NOTE, if max=0, it just checks once for an event (probe) */
/* if max>0 then it returns only when this number is met */
/* if max=-1, it does not return any events, it just loops until */
/* there are none left! */

int	notifier_get_queued_events (int es, int myrunid, int *ids, int max)
{
int ntag;
int tags[6];
int k;
int rc;
int done=0;
int found=0;
int s;
int i;
int from;
int tout;

#ifdef VERBOSE
printf("notifier_get_queued_events EAS %d runid %x max %d\n",
		es, myrunid, max);
fflush(stdout);
#endif

if (max>0) tout = 10000;	/* we take our time more */
/* if you only want to probe, we are quicker, i.e. as fast as possible */
else	tout = 0;	

while (!done) {
	k = pollconn (es, 0, tout);	
						/* should be there or else why did we call it ? */

if (k>0) { /* yep we have one or more */
	s = allowconn (es, 0, 0);
	if (s>0) {
		ntag = 4; /* proto_exit, runid, gid, exit code */
		for(i=0;i<ntag;i++) tags[i]=0;

 		rc= recv_pkmesg (s, &from, &ntag, tags, NULL); 
		if ((ntag==4)&&(tags[0]==NOTIFIER_PROTO_EXIT)&&(tags[1]==myrunid)) { 
			rc =  add_event_to_loop (tags[2], tags[3]);
			if (max!=-1) { /* i.e. we are allowed to return events */
				if (!rc) { /* i.e. not a dup */
					if (ids) ids[found] = tags[2];	/* return it to user */ 
					found++; /* update local count */
					}
				} /* max!=-1 */
			} /* if valid exit message that we care about */
		} /* valid socket */
	closeconn(s);
	}

if (max!=-1)
	if (found>=max) break;	/* no more so we stop */
	/* we break as soon as we are finished */

if ((max==-1)&&(k<=0)) break; /* if we just loop until no more events */
} /* while looking for events */

return (found);
}


/* this is used to send exit codes */
/* *** usually only called by startup_d and maybe ftmpirun *** */

int	notifier_send_exit_event (int from, int runid, int gid, int exitcode) 
{
int s;
int p;
int tags[6];
int rc;

if (!notifier) return (-1);	/* no notifier yet */

/* get connection to notifier */
p = nport;	/* make tmp copy */
s = getconn_addr(naddr, &p, 0);	/* note 0 */

if (s<0) return (-1);	/* cannot connect */

tags[0] = NOTIFIER_PROTO_EXIT;
tags[1] = runid;
tags[2] = gid;
tags[3] = exitcode;
tags[4] = NOTIFIER_PROTO_END;

rc = send_pkmesg (s, from, 4, tags, EMPTYMSGBUF, 0);	/* send req */

if (rc<0) { /* problem sending ? */
			closeconn (s);
			return (-1);
			}

/* this is no ACK ! */
#ifndef WIN32
usleep (1000);	/* hope and pray it gets there before closing the socket */
#else
Sleep(1);
#endif
closeconn (s);	/* clean up sockets */
return (0);
}

/**
notifier_shutdown - shutdown the notifier
@param from who am I?
*/

int notifier_shutdown(int from)
{
int s;
int tags[6];

char notifyname[256];
int t0, t1;
int e, a, p;
int rc;

/* Don't trust data (port etc) from notifier_find. Because
 * It always report cache result */

    sprintf(notifyname, "ftmpi:services:notifier\0");
    rc= ns_open ();
    if (rc!=0) return (-1);
    rc = ns_info (notifyname, 1, &t0, &t1, &e, &a, &p);
    if (rc!=1) return (-1);
    ns_close ();

    /* save results */
    naddr = a;
    nport = p;

    notifier=1;

    /* get connection to notifier */
    p = nport;	/* make tmp copy */
    s = getconn_addr(naddr, &p, 0);	/* note 0 */

    if (s<0) return (-1);	/* cannot connect */
    tags[0] = NOTIFIER_PROTO_SHUTDOWN;
    send_pkmesg (s, from, 1, tags, EMPTYMSGBUF, 0);	/* send req */
    return 0;
}
