/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS FTMPI_NOTIFIER 

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			THara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifdef USE_OPENSSL

#include "libsecure.h"
#include <openssl/err.h>
#include <signal.h>

#define CA_LIST "harness_ssl_root"


BIO *bio_err=0;
static char *pass;
static int password_cb(char *buf,int num,
  int rwflag,void *userdata);

/* A simple error and exit routine*/
int err_exit(char *string)
{
  fprintf(stderr,"%s\n",string);
  return -1;
}

/* Print SSL errors and exit*/
int berr_exit(char *string)
{
   BIO_printf(bio_err,"%s\n",string);
   ERR_print_errors(bio_err);
   return -1;
}

/*The password code is not thread safe*/
static int password_cb(char *buf,int num,
  int rwflag,void *userdata)
  {
    if(num<strlen(pass)+1)
      return(0);

    strcpy(buf,pass);
    return(strlen(pass));
  }



/** 
ctx_init Initialize context object 
@keyfile Key file
@password password 
@return context object
*/

SSL_CTX *ctx_init(char *keyfile,char *password)
{
    SSL_METHOD *meth;
    SSL_CTX *ctx;
    char root[256];
    
    if(!bio_err){
      /* Global system initialization*/
      SSL_library_init();
      SSL_load_error_strings();
      
      /* An error write context */
      bio_err=BIO_new_fp(stderr,BIO_NOCLOSE);
    }

    /* Ignore SIGPIPE */
    signal(SIGPIPE,SIG_IGN);
    
    /* Create our context*/
    meth=SSLv23_method();
    ctx=SSL_CTX_new(meth);

    /* Load our keys and certificates*/
    if(!(SSL_CTX_use_certificate_chain_file(ctx, keyfile))) {
      berr_exit("Can't read certificate file");
      exit(1);
    }

    pass=password;
    SSL_CTX_set_default_passwd_cb(ctx, password_cb);
    if(!(SSL_CTX_use_PrivateKey_file(ctx, keyfile,SSL_FILETYPE_PEM))) {
      berr_exit("Can't read key file");
      exit(1);
    }

    /* Load the CAs we trust*/

    sprintf(root,"%s/.harness/%s_%s",getenv("HOME"),CA_LIST,inet_ntoa(get_my_addr()));
    if(!(SSL_CTX_load_verify_locations(ctx, root, 0))) {
      berr_exit("Can't read CA list");
      exit(1);
    }

#if (OPENSSL_VERSION_NUMBER < 0x00905100L)
    SSL_CTX_set_verify_depth(ctx,1);
#endif
    
    return ctx;
}
     

/** 
ctx_finalize - Finalize context object 
@param ctx context object
*/
void ctx_finalize(SSL_CTX *ctx)
{
   SSL_CTX_free(ctx);
}


/**
check_cert - Check that the common name matches the host name
@param ssl SSL
@param host host
*/
void check_cert(SSL *ssl,char *host)
{
    X509 *peer;
    char peer_CN[256];

    if(SSL_get_verify_result(ssl)!=X509_V_OK) {
        berr_exit("Certificate doesn't verify");
        exit(1);
    }

    /*Check the cert chain. The chain length
     *is automatically checked by OpenSSL when
     *we set the verify depth in the ctx */

    /*Check the common name*/
    peer=SSL_get_peer_certificate(ssl);
    X509_NAME_get_text_by_NID (X509_get_subject_name(peer), NID_commonName, peer_CN, 256);
    if(strcasecmp(peer_CN,host)) {
         printf("peer_CN is #%s# host is #%s#\n",peer_CN,host);
          err_exit ("Common name doesn't match host name");
    }
}

/**
load_dh_params - 
@param ctx context object
@param file file
*/
void load_dh_params(SSL_CTX *ctx,char *file)
{
    DH *ret=0;
    BIO *bio;

    if ((bio=BIO_new_file(file,"r")) == NULL) {
       berr_exit("Couldn't open DH file");
       exit(1);
    }

    ret=PEM_read_bio_DHparams(bio,NULL,NULL, NULL);
    BIO_free(bio);
    if(SSL_CTX_set_tmp_dh(ctx,ret)<0) {
       berr_exit("Couldn't set DH parameters");
       exit(1);
    }
}

#endif

/* The foo function is declare to prevent segmentation fault
 * while compiling this file of some compiler */
void secure_foo(void)
{

}

