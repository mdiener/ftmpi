
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Antonin Bukovsky <tone@cs.utk.edu> 
			Thara Angskun <angskun@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* 
	Brief History
	Reed wrote the startup inital code to grahams over complex design
	(it was all needed honest)
	Tone made it more RPCable by using lots of writevs
	Graham started to take them out after having to add the gid stuff!
	Graham then took out tones stuff so that everything uses msgbuffers.
	(i.e. safer message passing)
	Thara then added functionality to allow delete/kill/ps procs
	as well as expire 
	Thara has also added openssl calls
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef WIN32
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/uio.h>
#endif

#include "libcio.h"
#ifdef WIN32
#include "wincomm.h"
#else
#include "mpi.h"
#include "snipe_lite.h"
#endif
#include "msg.h"
#include "msgbuf.h"
#include "../../include/ns_lib.h"
#include	"notifier.h"
#ifndef WIN32
#include "ft-mpi-sys.h"
#include "ft-mpi-modes.h"
#else
#include "../../ftmpi/ft-mpi-modes.h"
#include "libcio.h"
#endif

#ifdef USEMDRS
#include	"mdrs_clib.h"
#endif


#ifdef USE_OPENSSL
#include <openssl/ssl.h>
#include "libsecure.h"
#endif

#include "libstartup.h"
#include "internal_state_sys.h"
#include "envs.h"

#define MAX_COMMENT 128
#define MAX_COMMAND_LINE 256
#define TIMEOUT 500


#define KEYFILE "harness_ssl"
#define PASSWORD "password"

int  make_conn(char*);

/* WriteV stuff. */
/* soon (as in very) to be removed */
/* actually some of it should be in the msgbuf file (todo) */

struct iovec * IOV_STRUCT = NULL;
int IOV_STRUCT_SIZE = 0;
int PORT = 22345;

#define STATUS_INVALID  0
#define STATUS_RUNNING  1
#define STATUS_EXITED   2
#define STATUS_FAILED_ON_EXEC   3
#define STATUS_ALL      4

/* startup_finder stuff */
int startup_found=0;
int *startup_addrs=NULL;
int *startup_ports=NULL;
int *startup_entries = NULL;
int startup_group=-1;

/* silent startup flag */
int silent=0;


#ifdef WIN32
struct iovec{
  void * iov_base;
  int iov_len;
};

int writev(int fd,struct iovec * iov,int cnt)
{
  int i;
  int ret = 0;
  for(i=0;i<cnt;i++){
    ret+=writeconn((SOCKET)fd,(char *)&iov[i].iov_len,sizeof(int));
    ret+=writeconn((SOCKET)fd,(char *)iov[i].iov_base,iov[i].iov_len);
  }
  return ret;
}
#endif


/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/

int make_conn(char *server)
{
  int fd;
  fd = getconn(server, &PORT, 20);

  if (fd<0) {
    printf("connect failed.\n");
    return(-1);
  }
  printf("Connection Established\n");
  return(fd);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
void startupRPCclose(int fd)
{
  int command = -99;


/*   printf("CLOSING CONNECTION TO %d\n",fd); */
  writeconn(fd,(char *)&command,sizeof(int));
  closeconn(fd);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
int  startupAdd_rpc(int fd,int caller_id,int gid,char *comment,char * path,int argc,char **argv,int envc,char **envp,int intc,int *intv)
{
  struct iovec * iov;
  int total,i,j=0,ret;
  int command = 1;
  int tmp_var_var[TMP_VAR_MAX];
  int tmp_var_var_size;

  total = argc+envc+11;
  tmp_var_var_size = 2+argc+envc;

  if(IOV_STRUCT == NULL){
    IOV_STRUCT = (struct iovec *) malloc(sizeof(struct iovec)*total);
    iov = IOV_STRUCT;
  }
  else if(IOV_STRUCT_SIZE < total){
    free(IOV_STRUCT);
    IOV_STRUCT = (struct iovec *) malloc(sizeof(struct iovec)*total);
    iov = IOV_STRUCT;
  }
  else {
    iov = IOV_STRUCT;
  }


 

  if(iov == NULL || TMP_VAR_MAX < tmp_var_var_size){
    return(-1);
  }

  iov[0].iov_base = (char*) &command;
  iov[0].iov_len = sizeof(int);
  iov[1].iov_base = (char*) &caller_id;
  iov[1].iov_len = sizeof(int);
  iov[2].iov_base = (char*) &gid;
  iov[2].iov_len = sizeof(int);
  iov[3].iov_base = (char*) &tmp_var_var_size;
  iov[3].iov_len = sizeof(int);
  iov[4].iov_base = (char*) tmp_var_var;
  iov[4].iov_len = sizeof(int) * tmp_var_var_size;

  iov[5].iov_base = (char*) comment;
  tmp_var_var[j++] = iov[5].iov_len = strlen(comment);

  iov[6].iov_base = (char*) path;
  tmp_var_var[j++] = iov[6].iov_len = strlen(path);

  iov[7].iov_base = (char*) &argc;
  iov[7].iov_len = sizeof(int);
  for(i=0;i<argc;i++){
    iov[8+i].iov_base = argv[i];
    tmp_var_var[j++] = iov[8+i].iov_len = strlen(argv[i]);
  }

  iov[8+argc].iov_base = (char*) &envc;
  iov[8+argc].iov_len = sizeof(int);
  for(i=0;i<envc;i++){
    iov[9+argc+i].iov_base = envp[i];
    tmp_var_var[j++] = iov[9+argc+i].iov_len = strlen(envp[i]);
  }

  iov[9+argc+envc].iov_base = (char*) &intc;
  iov[9+argc+envc].iov_len = sizeof(int);
  iov[10+argc+envc].iov_base = (char*) intv;
  iov[10+argc+envc].iov_len = sizeof(int)*intc;
  
  ret = writev(fd,iov,total);

  readconn(fd,(char*)&ret,sizeof(int));
  return(ret);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
int  startupMonitor_rpc(int fd,int pid,int size,int * array_of_who_knows_what)
{

  struct iovec iov[4];
  int command = 2;
  int ret;


  iov[0].iov_base = (char*) &command;
  iov[0].iov_len = sizeof(int);
  iov[1].iov_base = (char*) &pid;
  iov[1].iov_len = sizeof(int);
  iov[2].iov_base = (char*) &size;
  iov[2].iov_len = sizeof(int);
  iov[3].iov_base = (char*) array_of_who_knows_what;
  iov[3].iov_len = sizeof(int)*size;

  ret = writev(fd,iov,4);

  readconn(fd,(char*)&ret,sizeof(int));
  return(ret);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
int startupUnMonitor_rpc(int fd,int pid,int size,int * array_of_who_knows_what)
{
 
  struct iovec iov[3];
  int command = 3;
  int ret;

  iov[0].iov_base = (char*) &command;
  iov[0].iov_len = sizeof(int);
  iov[1].iov_base = (char*) &pid;
  iov[1].iov_len = sizeof(int);
  iov[2].iov_base = (char*) array_of_who_knows_what;
  iov[2].iov_len = sizeof(int)*size;

  ret = writev(fd,iov,3);

  readconn(fd,(char*)&ret,sizeof(int));
  return(ret);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/
int  startupGetStatusByPid_rpc(int fd,int pid)
{
  struct iovec iov[2];
  int command = 4;
  int ret;

  iov[0].iov_base = (char*) &command;
  iov[0].iov_len = sizeof(int);
  iov[1].iov_base = (char*) &pid;
  iov[1].iov_len = sizeof(int);

  ret = writev(fd,iov,2);

  readconn(fd,(char*)&ret,sizeof(int));
  return(ret);
}

/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/

/* not currently used.. by anyone! GEF */
int  startupExpireItems_rpc(int fd,int age)
{
  struct iovec iov[2];
  int command = 5;
  int ret;

  iov[0].iov_base = (char*) &command;
  iov[0].iov_len = sizeof(int);
  iov[1].iov_base = (char*) &age;
  iov[1].iov_len = sizeof(int);

  ret = writev(fd,iov,2);
  if(ret > 0){
    return(1);
  }
  return(0);
}
/**************************************************************************************/
/**************************************************************************************/
/**************************************************************************************/

/**
startup_finder_vm - find startup services and automatic load startup services as Harness plug-in
@param vmname virtual machine name [INPUT]
@return number of startup services listed in the NS
*/
int startup_finder_vm (char * vm)
{
int rc, t0, t1;
char startupname[256];

if (startup_found) {	/* being called again, so reset all */
	startup_found = 0;
	free (startup_addrs);
	startup_addrs=NULL;
	free (startup_ports);
	startup_ports=NULL;
	free (startup_entries);
	startup_entries=NULL;
	}

/* now we do the two step look it up twice routine */
sprintf(startupname, "%s:ftmpi:services:startup_notify_service\0",vm);

ns_open ();
rc = ns_info (startupname, 0, &t0, &t1, NULL, NULL, NULL);

/* t1 is the actual number of entries in the NS */
if (t1<1)  { /* i.e. there isn't any */
/* 	ns_close (); */
/* note if we lock the ns up here on a fast start it hangs ! */
	return (0);	/* none */
	}


/* t0 = startup group in the NS */
startup_group = t0;


/* ok malloc memory and then get the entries stored */
startup_addrs = (int*) malloc ((sizeof(int) * t1));
startup_ports = (int*) malloc ((sizeof(int) * t1));
startup_entries = (int*) malloc ((sizeof(int) * t1));

rc = ns_info (startupname, t1, &t0, &t1, startup_entries, startup_addrs, startup_ports);
ns_close ();

if (rc<1) { /* if a problem ??? clean up lass */
	startup_found = 0;
	free (startup_addrs);
	startup_addrs=NULL;
	free (startup_ports);
	startup_ports=NULL;
	free (startup_entries);
	startup_entries=NULL;
	}

startup_found = rc;
return (startup_found);
}


/**
startup_ps - get all process status
@param index index of startup deamon [INPUT]
@param me who am I? [INPUT]
@param flag flag [INPUT] (1=expire, 0 not expire)
@param itemHead pointer to the first process information [OUTPUT]
@param hostID host indentifier [OUTPUT]
@return number of ITEM(process).
*/
int startup_ps(int index, int me, int flag, struct startup_procstat **itemHead, int *hostID)
{
    int ntag;
    int i, j;
    int tags[2];
    int s, p;
    int rc;
    int nItem;
    int mb;
    int myID;
    int from;
    struct startup_procstat *item;
    char command[MAX_COMMAND_LINE];
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
    SSL *ssl;
#endif

    if ((index < 0) || (index >= startup_found))
        return (-1);            /* bad index */
    ntag = 1;
    tags[0] = PROTO_STARTUP_PS_ALL;
    p = startup_ports[index];
    s = getconn_addr(startup_addrs[index], &p, 0);

    mb = get_msg_buf(1);        /* resizable as this might be large */

    if (mb < 0) {
        return (-1);            /* ops bad luck */
    }

    pk_int32(mb, &flag, 1);
#ifdef USE_OPENSSL

    tmpaddr.s_addr=get_my_addr();
    sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
    ctx=ctx_init(keyfilename,PASSWORD);
    /* Connect the SSL socket */
    ssl=SSL_new(ctx);
    sbio=BIO_new_socket(s,BIO_NOCLOSE);
    SSL_set_bio(ssl,sbio,sbio);
    if(SSL_connect(ssl)<=0) {
       berr_exit("SSL connect error");
    }
    tmpaddr.s_addr=startup_addrs[index];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
    /*     check_cert(ssl,inet_ntoa(tmpaddr)); */
    rc = send_pkmesg_s(ssl, me, ntag, tags, mb, 1);
#else
    rc = send_pkmesg(s, me, ntag, tags, mb, 1);
#endif
    if (rc <= 0) {
#ifdef USE_OPENSSL
	SSL_shutdown(ssl);
	ctx_finalize(ctx);
#endif
        closeconn(s);
        return (-1);
    }
#ifdef USE_OPENSSL
    rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);       /* do the recv for the ack */
    SSL_shutdown(ssl);
    ctx_finalize(ctx);
#else
    rc = recv_pkmesg(s, &from, &ntag, tags, &mb);       /* do the recv for the ack */
#endif
    closeconn(s); /* close connection afterwards */

    if (rc <= 0) {
        return (-1);
    }

    upk_int32(mb, &nItem, 1);
    upk_int32(mb, &myID, 1);
    (*hostID) = myID;
    for (i = 0; i < nItem; i++) {
        if (i == 0) {
            item = (struct startup_procstat *) malloc(sizeof(struct startup_procstat));
            (*itemHead) = item;
        } else {
            item->next =
                (struct startup_procstat *) malloc(sizeof(struct startup_procstat));
            item = item->next;
        }
        upk_int32(mb, &(item->gid), 1);
        upk_int32(mb, &(item->caller), 1);
        upk_string(mb, command, MAX_COMMAND_LINE);
        item->execName = (char *) malloc(strlen(command) + 1);
        strcpy(item->execName, command);
        upk_int32(mb, &(item->status), 1);
        upk_int32(mb, &(item->exitValue), 1);
        upk_int32(mb, &(item->elapsedTime), 1);
        upk_int32(mb, &(item->callbackListLength), 1);
        if (item->callbackListLength != 0) {
            item->callbackList =
                (int *) malloc(((item->callbackListLength) + 1) *
                               sizeof(int));
            for (j = 0; j < item->callbackListLength; j++) {
                upk_int32(mb, &(item->callbackList), j);
            }
        }
    }
    if (nItem != 0) {
        item->next = NULL;
    }
    free_msg_buf(mb);


    return nItem;
}

#ifdef USE_OPENSSL
int  startup_spawn_async (int index,int me,int runid,int flag,int ngid, int *theirgid,char *path, char *cwdir, int argc,char **argv,int envc,char **envp,int out_ip, int out_port, SSL **ssl)
#else
int  startup_spawn_async (int index,int me,int runid,int flag,int ngid, int *theirgid,char *path, char *cwdir, int argc,char **argv,int envc,char **envp,int out_ip, int out_port, int *s)
#endif
{
    int mb;
    int ntag;
    int tags[6];
    int i;
    int rc;
    char comment[256];
    int p;
    int gid;

#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
    int s;
#endif

  /*  printf("STARTUP_SPAWN_ASYNC index %d me %d runid %d flag %d ngid %d gid 0x%p path %s argc %d envc %d out_ip %d out_port %d\n",index,me,runid,flag,ngid,theirgid,path,argc,envc,out_ip,out_port);  */
  /*  fflush(stdout); */
    
    if ((index < 0) || (index >= startup_found))
        return (-1);            /* bad index */

    if (theirgid != NULL) {
        sprintf(comment, "FTMPI:process:0x%x:0x%x\0", runid, *theirgid);
    } else {

    }

    mb = get_msg_buf(1);        /* resizable as this might be large */

    if (mb < 0)
        return (-1);            /* ops bad luck */

/* else pack and send :) */

    pk_int32(mb, &runid, 1);
    pk_int32(mb, &flag, 1);
    if (theirgid != NULL) {
        pk_int32(mb, &ngid, 1);
        for(i=0;i<ngid;i++) {
            gid = theirgid[i];
            pk_int32(mb, &gid, 1);
        }
    } else {
        ngid=1;
	gid=SPAWN_STARTUP_ALLOCATE_GIDS;
        pk_int32(mb, &ngid, 1);
        pk_int32(mb, &gid, 1);
    }
    pk_string(mb, comment);
    pk_string(mb, path);
    if(cwdir!=NULL) {
       pk_string(mb, cwdir);
    } else {
       pk_string(mb, "NONE");
    }
    pk_raw32(mb, &out_ip, 1);
    pk_int32(mb, &out_port, 1);
    pk_int32(mb, &argc, 1);
    for (i = 0; i < argc; i++)
        pk_string(mb, argv[i]);
    pk_int32(mb, &envc, 1);
    for (i = 0; i < envc; i++)
        pk_string(mb, envp[i]);

    ntag = 1;
    tags[0] = PROTO_STARTUP_SPAWN;

/* ok connect and do the stuff */
    p = startup_ports[index];

#ifdef USE_OPENSSL    
    s = getconn_addr(startup_addrs[index], &p, 0);
    if (s < 0) {                /* connection failed */
      if ((mb != EMPTYMSGBUF)) free_msg_buf(mb);	/* free msg buf it */
        return s;
    }
#else
    (*s) = getconn_addr(startup_addrs[index], &p, 0);
    if ((*s) < 0) {                /* connection failed */
      if ((mb != EMPTYMSGBUF)) free_msg_buf(mb);	/* free msg buf it */
      return (*s);
    }
#endif

#ifdef USE_OPENSSL
    puts("WE ARE IN SECURE CONNECTION");

    tmpaddr.s_addr=get_my_addr();
    sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
    ctx=ctx_init(keyfilename,PASSWORD);
    /* Connect the SSL socket */
    (*ssl)=SSL_new(ctx);
    sbio=BIO_new_socket(s,BIO_NOCLOSE);
    SSL_set_bio((*ssl),sbio,sbio);
    if(SSL_connect((*ssl))<=0) {
       berr_exit("SSL connect error");
    }
    tmpaddr.s_addr=startup_addrs[index];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
/*     check_cert(ssl,inet_ntoa(tmpaddr)); */

    rc = send_pkmesg_s((*ssl), me, ntag, tags, mb, 1); /* msg buf freed always */
#else
    rc = send_pkmesg((*s), me, ntag, tags, mb, 1); /* msg buf freed always */
#endif

    return rc; 
}

int  startup_spawn (int index,int me,int runid,int *theirgid,char *path, char *cwdir, int argc,char **argv,int envc,char **envp,int out_ip, int out_port)
{
    int mb;
    int ntag;
    int tags[6];
    int i;
    int rc;
    char comment[256];
    int s;
    int p;
    int from;
    int gid;
    int ngid;
    int flag;

#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif

    /*
    printf("AAAA IN Startup_spawn index %d me %d runid %d path %s cwd %s argc %d envc %d out_ip %d out_port %d\n",index,me,runid,path,cwdir,argc,envc,out_ip,out_port);
    fflush(stdout);
    */

    /* this 3 lines below make protocol compatable with startup_async with parallel spawn */
    gid=SPAWN_STARTUP_ALLOCATE_GIDS;
    ngid=1; /* don't support parallel startup_sync at this time */
    flag=-1;
    
    if ((index < 0) || (index >= startup_found))
        return (-1);            /* bad index */

    if (theirgid != NULL) {
        sprintf(comment, "FTMPI:process:0x%x:0x%x\0", runid, *theirgid);
    } else {
        return (-1);
    }

    mb = get_msg_buf(1);        /* resizable as this might be large */

    if (mb < 0)
        return (-1);            /* ops bad luck */

/* else pack and send :) */
    pk_int32(mb, &runid, 1);
    pk_int32(mb, &flag, 1);
    pk_int32(mb, &ngid, 1);
    if (theirgid != NULL) {
        gid = (*theirgid);
    }
    pk_int32(mb, &gid, 1);
    pk_string(mb, comment);
    pk_string(mb, path);
    if(cwdir!=NULL) {
       pk_string(mb, cwdir);
    } else {
       pk_string(mb, "NONE");
    }
    pk_raw32(mb, &out_ip, 1);
    pk_int32(mb, &out_port, 1);
    pk_int32(mb, &argc, 1);
    for (i = 0; i < argc; i++)
        pk_string(mb, argv[i]);
    pk_int32(mb, &envc, 1);
    for (i = 0; i < envc; i++)
        pk_string(mb, envp[i]);

    ntag = 1;
    tags[0] = PROTO_STARTUP_SPAWN;

/* ok connect and do the stuff */
    p = startup_ports[index];
    s = getconn_addr(startup_addrs[index], &p, 0);
    if (s < 0) {                /* connection failed */
		if ((mb != EMPTYMSGBUF)) free_msg_buf(mb);	/* free msg buf it */
        return (s);
    }

#ifdef USE_OPENSSL
    puts("WE ARE IN SECURE CONNECTION SPAWN");

    tmpaddr.s_addr=get_my_addr();
    sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
    ctx=ctx_init(keyfilename,PASSWORD);
    /* Connect the SSL socket */
    ssl=SSL_new(ctx);
    sbio=BIO_new_socket(s,BIO_NOCLOSE);
    SSL_set_bio(ssl,sbio,sbio);
    if(SSL_connect(ssl)<=0) {
       berr_exit("SSL connect error");
    }
    tmpaddr.s_addr=startup_addrs[index];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
    /* check_cert(ssl,inet_ntoa(tmpaddr));  */

    rc = send_pkmesg_s(ssl, me, ntag, tags, mb, 1); /* msg buf freed always */
#else
    rc = send_pkmesg(s, me, ntag, tags, mb, 1); /* msg buf freed always */
#endif

    if (rc <= 0) {
#ifdef USE_OPENSSL
        ctx_finalize(ctx);
        SSL_shutdown(ssl);
#endif
        closeconn(s);
        return (-1);
    }

    ntag = 3;
    tags[0] = 0;
    tags[1] = 0;
    tags[2] = 0;

#ifdef USE_OPENSSL
    rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);      /* do the recv for the ack */
#else
    rc = recv_pkmesg(s, &from, &ntag, tags, &mb);      /* do the recv for the ack */
#endif

    if ((mb != EMPTYMSGBUF)) {
        upk_int32(mb,theirgid,tags[2]);
	free_msg_buf(mb);	/* free msg buf it */
    }

#ifdef USE_OPENSSL
    ctx_finalize(ctx);
    SSL_shutdown(ssl);
#endif

    if (rc < 0) {
        closeconn(s);
        return (-1);
    }

    if ((ntag != 3) || (tags[0] != PROTO_STARTUP_SPAWN)) {      /* failed */
        closeconn(s);
        return (-1);
    }

/* else it might have worked? */

    closeconn(s);
    return (tags[1]);
}


/**
startup_new_services - startup new services with specific number
@param n number of services [INPUT]
@param vmname virtual machine name [INPUT]
@return error number
*/
int startup_new_services(int n,char *vmname)
{
    coreinfo_t *cores;
    char startupname[256];
    int totalcores;
    int ncores;
    int try1;
    int i;
    int s;
    int rc, t0, t1;
    int package;
    int ncalls;
    int funct_id;

    cores = (coreinfo_t *)malloc(sizeof(coreinfo_t)*n);
    ncores = hlib_find_cores(vmname, 1, n, &totalcores, cores);
    if (ncores < 1) {
#ifdef VERBOSE 
        puts("Not enough cores to do a test on!");
#endif
        return ncores;
    }

    for(i=0;i<ncores;i++) {  /* load all plugin */
        rc = hlib_load_package(&cores[i], "ftmpi", "services", &package, &ncalls);
        if (rc < 0) {
#ifdef VERBOSE 
            printf("Load package on %s fails with rc %d\n", cores[i], rc);
#endif
            return -1;
        }

        rc = hlib_get_function(&cores[i], "startup_notify_service", package, &funct_id);
        if (rc < 0) {
#ifdef VERBOSE 
            printf("get function %s failed with rc %d\n", "startup_notify_service", rc);
#endif
            return -1;
        }
        rc = hlib_call_function(&cores[i], funct_id, &s); 

    }


    sprintf(startupname, "%s:ftmpi:services:startup_notify_service",vmname);
    ns_open();
    rc = ns_info(startupname, 0, &t0, &t1, NULL, NULL, NULL);
    ns_close();

    try1=0;
    while(try1 < 7 && t1!=ncores) {  /* We try to wait for plugin register 7 times before we surrender */
#ifdef WIN32
        Sleep(1);
#else
        sleep(1);
#endif
        sprintf(startupname, "%s:ftmpi:services:startup_notify_service",vmname);
        ns_open();
        rc = ns_info(startupname, 0, &t0, &t1, NULL, NULL, NULL);
        ns_close();
	try1++;
    }

    if (t1 < 1) {           /* i.e. there isn't any */
        return 0;         /* none */
    }

/* ok malloc memory and then get the entries stored */
    startup_addrs = (int *) malloc((sizeof(int) * t1));
    startup_ports = (int *) malloc((sizeof(int) * t1));

/* Used by startup_delete */
    startup_entries = (int *) malloc((sizeof(int) * t1));

    sprintf(startupname, "%s:ftmpi:services:startup_notify_service",vmname);
    ns_open();
    rc = ns_info(startupname, t1, &t0, &t1, startup_entries, startup_addrs,
                 startup_ports);
    ns_close();

    if (rc < 1) {
        startup_found = 0;
        free(startup_addrs);
        startup_addrs = NULL;
        free(startup_ports);
        startup_ports = NULL;
	free (startup_entries);
	startup_entries=NULL;
    }

    startup_found = rc;
    return (startup_found);
}

/**
startup_totalnew_services - startup total services with specific number
@param max maximum number of services [INPUT]
@param vmname virtual machine name [INPUT]
@return error number
*/
int startup_total_services(int max,char *vmname)
{
  return startup_new_services(max-startup_finder_vm(vmname),vmname);
}

/**
startup_delete - delete startup daemon
@param sud startup daemon identifier [INPUT]
@param me who am I? [INPUT]
@retval 0 success
@retval -1 problem with the startup daemon indentifier
@retval -2 can not connect to the startup daemon

GEF 
@retval RESULT_ERROR_PROTO PROTOCOL ERROR (i.e. try again)
@reval = RESULT_EXITING/RESULT_CANNOT_UNLOAD/RESULT_CANNOT_EXIT/RESULT_INUSE
@reval = RESULT_NO_AUTH/RESULT_EXIT_DELAYED
As defined in libstartup.h
*/
int startup_delete(int sud, int me)
{
	int from;
    int ntag;
    int tags[5];	/* only need 2 but we may get a bigger reply */
    int s, p;
    int rc;
    int i;

#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif


    if (sud < 0)
        return (-1);            /* bad sud */
    ntag = 1;
    tags[0] = PROTO_STARTUP_DELETE;

    for (i = 0; i < startup_found; i++) {
        if (startup_entries[i] == sud) {
            break;
        }
    }
    if (i == startup_found) 
        return (-1);            /* Can not find hostID */
    p = startup_ports[i];
    s = getconn_addr(startup_addrs[i], &p, 0);

#ifdef USE_OPENSSL
    tmpaddr.s_addr=get_my_addr();
    sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
    ctx=ctx_init(keyfilename,PASSWORD);
    /* Connect the SSL socket */
    ssl=SSL_new(ctx);
    sbio=BIO_new_socket(s,BIO_NOCLOSE);
    SSL_set_bio(ssl,sbio,sbio);
    if(SSL_connect(ssl)<=0) {
       berr_exit("SSL connect error");
    }
    tmpaddr.s_addr=startup_addrs[sud];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
/*     check_cert(ssl,inet_ntoa(tmpaddr)); */

    rc = send_pkmesg_s(ssl, me, ntag, tags, EMPTYMSGBUF, 0);
#else
    rc = send_pkmesg(s, me, ntag, tags, EMPTYMSGBUF, 0);
#endif

    if (rc < 0) {
#ifdef USE_OPENSSL
        SSL_shutdown(ssl);
	ctx_finalize(ctx);
#endif
        closeconn(s);
        return (-2);
    }

	/* if not failed to connect we wait for a reply */
#ifdef USE_OPENSSL
	rc = recv_pkmesg_s(ssl, &from, &ntag, tags, NULL);
        SSL_shutdown(ssl);
	ctx_finalize(ctx);
#else
	rc = recv_pkmesg(s, &from, &ntag, tags, NULL);
#endif
	closeconn(s);

	if (tags[0]!=PROTO_STARTUP_DELETE_ACK) return (RESULT_ERROR_PROTO);
	else
		return (tags[1]);
}

/**
post_abort - post abort message to the runID
@param runID runID
*/
int post_abort(int runID)
{
   int mb;
   int state;
   int leader_id;
   int gepoch;
   int gnproc;
   int gextent;
   int t2,t3;
   char statename[256];

   state=FT_STATE_ABORT;  /* defined in ft-mpi-modes.h */
   leader_id=0;
   gepoch=0;
   gnproc=0;
   gextent=0;

   mb = get_msg_buf_of_size (5*sizeof(int), 0, 0);   /* get a small buffer */


   pk_int32(mb, &state, 1);
   pk_int32(mb, &leader_id, 1);    /* should be me */
   pk_int32(mb, &gepoch, 1);
   pk_int32(mb, &gnproc, 1);
   pk_int32(mb, &gextent, 1);

   sprintf(statename,"FTMPI:state:%x\0", runID);
   ns_open ();
   (void)ns_record_put (0, statename, mb, RECORD_DB_INDEXED | RECORD_DB_READONLY, 0, &t2, &t3, 1);
   ns_close ();
   return 1;
}

/**
clean_ns - clean name service
@param runID runID
*/
int clean_ns(int runID)
{
   char recordname[256];
   ns_open();
   sprintf(recordname,"FTMPI:leader:%x\0", runID);
   ns_record_del(recordname);
   sprintf(recordname,"FTMPI:startup:%x\0", runID);
   ns_record_del(recordname);
   sprintf(recordname,"FTMPI:state:%x\0", runID);
   ns_record_del(recordname);
   ns_close();
   return 1;
}



/**
startup_kill - kill process on specific gid
@param id global process ID/run ID [INPUT]
@param signal signal [INPUT]
@param me who am I [INPUT]
@param flag (1=gid,2=runID) [INPUT]
@retval 0 success
@retval -1 can not find that gid
@retval -2 kill() return error (the error will specific in errnum).
@retval -3 call this function while the process is not running.
@retval -4 connection problem (the error hostID will specific in errnum).
*/
int startup_kill(int id, int signal, int me, int flag, int *errnum)
{
    int ntag;
    int tags[2];
    int s, p;
    int rc;
    int mb;
    int i;
    int status;
    int from;
    int err;
    int retval;
    int found = 0;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif

    if((flag==2) & (signal==-1)) { /* killall */
         /* TA: FIXME: WARNING: This is highly security risk. */
         /* i.e. we don't check that we have permission to abort this runID or not!! */
         /* I will change it after we implement security model */
         post_abort(id);
    }

    retval=0;
    mb = get_msg_buf(1);        /* resizable as this might be large */

    for (i = 0; i < startup_found; i++) {
        ntag = 1;
        tags[0] = PROTO_STARTUP_KILL;

        pk_int32(mb, &id, 1);
        pk_int32(mb, &signal, 1);
        pk_int32(mb, &flag, 1);

        p = startup_ports[i];
        s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, mb, 0);
#else
        rc = send_pkmesg(s, me, ntag, tags, mb, 1);
#endif
        if (rc <= 0) {
#ifdef USE_OPENSSL
            SSL_shutdown(ssl);
#endif
            closeconn(s);
            (*errnum) = startup_entries[i];     /* return Host ID */
            retval = -4;
        }
#ifdef USE_OPENSSL
        rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);   /* do the recv for the ack */
	SSL_shutdown(ssl);
#else
        rc = recv_pkmesg(s, &from, &ntag, tags, &mb);   /* do the recv for the ack */
#endif
        closeconn(s); /* alway close it */

        if (rc < 0) {
            free_msg_buf(mb);
            (*errnum) = startup_entries[i];     /* return Host ID */
            retval = -4;
        }
        upk_int32(mb, &status, 1);
        upk_int32(mb, &err, 1);
	if(err!=0) {
            (*errnum) = err;
	}
        if (status != -1 ) {     /* found pid */
            found = 1;
	    if(flag==1) {
               free_msg_buf(mb);
               return status;
	    }
        }
    }
    free_msg_buf(mb);

    if((flag==2) & (signal==-1) ) { /* killall  */
         /* TA: FIXME: WARNING: This is highly security risk. */
         /* i.e. we don't check that we have permission to clean this runID or not!! */
         /* I will change it after we implement security model */
         clean_ns(id);
    }

    if (retval!=0) {
        return retval;
    }
    if (found == 0) {
        return -1;
    }
    return 0;
}

/**
startup_expire - expire process information if the process is dead before specific age
@param age age of process(seconds) [INPUT]
@param me who am I ?[INPUT]
@param errnum error number [OUTPUT]
@retval >= 0  number of expired process
@retval < 0 connection problem (error hostID specific in errnum)
*/
int startup_expire(int age, int me, int *errnum)
{
    int ntag;
    int tags[2];
    int s, p;
    int rc;
    int mb;
    int i;
    int status;
    int from;
    int found = 0;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif


    mb = get_msg_buf(1);        /* resizable as this might be large */

    for (i = 0; i < startup_found; i++) {
        ntag = 1;
        tags[0] = PROTO_STARTUP_EXPIRE;

        pk_int32(mb, &age, 1);

        p = startup_ports[i];
        s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, mb, 0);
#else
        rc = send_pkmesg(s, me, ntag, tags, mb, 1);
#endif
        if (rc <= 0) {
#ifdef USE_OPENSSL
            SSL_shutdown(ssl);
	    ctx_finalize(ctx);
#endif
            closeconn(s);
            (*errnum) = startup_entries[i];     /* return Host ID */
            return (-4);
        }
#ifdef USE_OPENSSL
        rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);   /* do the recv for the ack */
	SSL_shutdown(ssl);
	ctx_finalize(ctx);
#else
        rc = recv_pkmesg(s, &from, &ntag, tags, &mb);   /* do the recv for the ack */
#endif
        closeconn(s); /* always close it */

        if (rc < 0) {
            free_msg_buf(mb);
            (*errnum) = startup_entries[i];     /* return Host ID */
            return (-4);
        }
        upk_int32(mb, &status, 1);
        found = found + status;
    }
    free_msg_buf(mb);
    return found;
}







/**** ADDED BY TONE TO ALLOW TO START A SERVICE ON A SPECIFIC CORE -- USED WITH JAVA GUI ****/
/**** MUST HAVE ACQUIRED THE ACTUAL HCORE STRUCTURE BEFORE HAND --- THIS WILL LOAD A STARTUP_D ON THE HCORE NOTHING MORE ****/
/************************************************************************************/
/************************************************************************************/
/************************************************************************************/
int startup_new_service_on_core(coreinfo_t * hcore)
{
  int rc;
  int package;
  int ncalls;
  int s;
  int funct_id;

  if(hcore != NULL){
    rc = hlib_load_package(hcore, "ftmpi", "services", &package, &ncalls);
    if (rc < 0) {
        printf("Load package on %s fails with rc %d\n", hcore->vmname, rc);
        return -1;
    }
    rc = hlib_get_function(hcore, "startup_notify_service", package, &funct_id);
    if (rc < 0) {
        printf("get function %s failed with rc %d\n", "startup_notify_service", rc);
        return -1;
    }
    rc = hlib_call_function(hcore, funct_id, &s); 
    if(rc < 0){
      return -1;
    }
  }
  return 0;
}
/**** ADDED THIS FUNCTION TO KILL A PROCESS ON A SPECIFIC STARTUP_D ****/
/**** GID - proces global id ****/
/**** i - index of a startup_d in the array alocated *****/
/************************************************************************************/
/************************************************************************************/
/************************************************************************************/
int specific_startup_kill(int i,int gid, int signal, int me, int flag)
{
  int ntag;
  int tags[2];
  int s, p;
  int rc;
  int mb;
  int from;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif


  mb = get_msg_buf(1);

  ntag = 1;
  tags[0] = PROTO_STARTUP_KILL;

  pk_int32(mb, &gid, 1);
  pk_int32(mb, &signal, 1);
  pk_int32(mb, &flag, 1);

  p = startup_ports[i];
  s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, mb, 0);
#else
  rc = send_pkmesg(s, me, ntag, tags, mb, 1);
#endif

  if (rc <= 0) {
#ifdef USE_OPENSSL
    SSL_shutdown(ssl);
    ctx_finalize(ctx);
#endif
    closeconn(s);
    return -1;
  }

  rc = recv_pkmesg(s, &from, &ntag, tags, &mb);
  free_msg_buf(mb);
  closeconn(s);
  return rc;
}

/**
startup_run - execute process
@param flag flags (RUN_HARNESS - for general job /  RUN_FTMPI - for mpi job / RUN_NEEDOUTPUT - if you want output)
@param vmname virtual machine name
@param np number of processes
@param nhosts number of hosts (HOSTAUTO - automatic select host)
@param hostlist (list of hosts)
@param cm communicator mode ( 0 - for general job /  FT_MODE_BLANK, FT_MODE_REBUILD, FT_MODE_SHRINK, FT_MODE_ABORT - for mpi job)
@param mm message mode ( 0 - for general job, FT_MODE_NOP, FT_MODE_CONT - for mpi job)
@param exeargc execute argument count
@param exeargv execute argument value
@param cwdir current working directory
*/
int startup_run(int flag, char *vmname, int np, int nhosts, int *hostlist, int cm, int mm, int exeargc, char **exeargv, char *cwdir)
{


/* pass input and get the options out */
/* set up the DB and then spawn the job */

/* min args are */
/* ftmpirun -np <n> <exename> */

/* or else np can be picked up by get env? */

/* cma = com_mode arg */
/* mma = mesg mode arg */
/* np = num of procs requested */
/* exeargs = # of exe args */
/* exea = where exe args start */

int mid; 	/* My ID */
int i, j, k, l; /* general counters */
int rc; /* return counter */

#ifdef OLDNSLOOKUPOFNOTIFYSERVICE 
char startupname[256]; /* name of NS entries used by the startup daemons */
#endif  /* OLDNSLOOKUPOFNOTIFYSERVICE */
char notifyname[256]; /* name of NS entries used by the notify daemons */

char statename[256];	/* NS entry new tasks look for */
int leader;			/* leader of the pack (if they need one) */
int mb;		


char outputname[256]; 
int out_addr, out_port;
int out_sockfd;
int out_group, out_mem;
int cur_conn, ret;

#ifndef SYNC_SPAWN /* i.e. async */ 

#ifdef USE_OPENSSL
SSL **sockfd;	
#else
int *sockfd;	
#endif  /* USE_OPENSSL */

int *rclist;	
int *done;	
int *eachngid;
int ntag;
int tags[3];
int from;
int counter, begin, p, d;
int ncp, npp, ngid;
int first;
int **gidlist;
#endif  /* SYNC_SPAWN */



   if( ((flag&RUN_FTMPI)==RUN_FTMPI) && ((flag&RUN_HARNESS)==RUN_HARNESS) ) {
      return -1;
   }
   if( ((flag&RUN_FTMPI)==0) && ((flag&RUN_HARNESS)==0) ) {
      return -1;
   }
   if( (((flag&RUN_HARNESS)==RUN_HARNESS) && (cm!=0)) || (((flag&RUN_HARNESS)==RUN_HARNESS) && (mm!=0))) {
      return -1;
   }
   if(np <1) {
      return -1;
   }

	/* build mbox info using either the NS / MDRS */

	/* first find the service via env vars */

	init_envs  ();  /* get environment variables */
	get_envs   ();

	if (!HARNESS_NS_HOST) {
		fprintf(stderr,"No HARNESS_NS_HOST specified, bailing\n");
		exit (-1);
	}

	if (!HARNESS_NS_HOST_PORT) {
		fprintf(stderr,"No HARNESS_NS_HOST_PORT specified, bailing\n");
		exit (-1);
	}

	rc = ns_init ( HARNESS_NS_HOST, HARNESS_NS_HOST_PORT );	/* talk to NS */

	if (rc<0) {
		fprintf(stderr, "OPS, wot no name service running.. bailing\n");
		exit (-2);
	}

	/* ok have NS at least contactable */


#ifdef USEGHCORECALLS
	{
	int package, ncalls, funct_id, s, tmp;

	rc = load_package ("ftmpi", "services", &package, &ncalls);
	rc = get_function ("make_id", package, &funct_id);

	rc = call_function (funct_id, &s);
	rc = readconn (s, &tmp, sizeof(int));
	closeconn (s);

	/* should unload the package here */

	mid = tmp;	/* copy across id value */
	}

#else /* if unable to use G_HCORE calls */

	/* GEF XXX this should use ns_gid for now */
	mid = 0; /* fixed see below */

#endif /* creating an id via the core plug-in or not */

	i = FT_OK;

#ifdef USEMDRS
	{
	int len, flags, index, ack_server, del_mbox;
	char initdataname[256]; /* name of mailbox entries */
	char *pbuf;
	int sbuf;   /* send buffer used by mail boxes */

	/* create the mbox data name */
	/* 	sprintf(initdataname, "FT-MPI:startup:0x%x", mid); */
	/* we do this after we get out gid (called mid here) */

	init_msg_bufs ();	
	sbuf = get_msg_buf (0);

	ack_server = 0; /* we need to ack the server that we started up correctly ? */
	del_mbox = 1;	/* we need to delete the mbox entry for the server? */

	rc = 0;
	rc += pk_int32 (sbuf, &mid, 1);
	rc += pk_int32 (sbuf, &ack_server, 1);
	rc += pk_int32 (sbuf, &del_mbox, 1);
	rc += pk_int32 (sbuf, &cm, 1);
	rc += pk_int32 (sbuf, &mm, 1);
	rc += pk_int32 (sbuf, &i, 1);

	/* ok, rc should equal length from query function... maybe */
	get_msg_buf_info (sbuf, &pbuf, &len);

	flags = MboxDefault;

	rc = mdrs_mbox_put(initdataname, mid, pbuf, len, flags, &index);

	if (rc<0) {
	   printf( "can't register mbox startup info\n"  );
	  exit( -5 );
	 }

	free_msg_buf (sbuf);

	 printf("Wrote startup info into MBOX [%s]\n", initdataname);

	}


#else /* USE the NS only */
	{
	int t0, t1, t2, t3;	/* NS interface vars */
#ifdef OLDNSLOOKUPOFNOTIFYSERVICE 
	int *sockets;		/* actual array of sockets used to talk to started processes */
	int *addrs, *ports;	/* the startup daemons addr and ports */
#endif  /* OLDNSLOOKUPOFNOTIFYSERVICE */
	int sud;			/* startup daemon we are currently spawning on */
	int indexsud;
	int nsud;			/* number of startup daemons we find */
	int started;		/* just how many did we start up */
	int lastloopstarted;	/* how many we started in the last loop.. */
				/* have we looped without starting ?? */
	char **envp;		/* initial ENV values */
	int	*gids;			/* initial tmp gid for each process */ 


	/* for this we just set up our notify socket and put that in the NS */

	/* we now need to publish this info in the name service */

	/* mid = ns_gid (NS_ID_OTHER); */
	ns_gid (NS_ID_OTHER,1,&mid);

	ns_close ();

	out_addr=0;
	out_port=0;
	if((flag&RUN_NEEDOUTPUT)==RUN_NEEDOUTPUT) {
	    out_addr = get_my_addr ();				
	    init_lsocket (&out_sockfd, &out_port);		
	    sprintf(outputname, "HARNESS:output:0x%x", mid);
	    ns_open ();
	    rc = ns_add (outputname, out_addr, out_port, &out_group, &out_mem);	
	    ns_close ();
	    rioc_init(out_sockfd,0);
	}

if((flag&RUN_FTMPI)==RUN_FTMPI) {
	rc = notifier_find ();

	if (rc<0) {
		fprintf(stderr,"There is no FTMPI notifier service listed under [%s]\n",
						notifyname);
		fprintf(stderr,"You need atleast one. more = better scaling faster error notification.\n");
		exit (-1);
	}
}






	/* ok now we need to access the startup daemons */
	/* first we get their addresses from the NS */

	/* their name is stored using the form vm:pack:comp:servicename:inst */
	/* in our case we just use pack:comp:service as the identifier */
	/* as we have no direct way of getting the VM name out of the non UT core */

#ifdef OLDNSLOOKUPOFNOTIFYSERVICE 
	
	sprintf(startupname, "ftmpi:services:startup_notify_service\0");


	/* find out details of the startup service */
	/* as it does NOT alloc memory for us find out # members first */

	ns_open ();
	rc = ns_info (startupname, 0, &t0, &t1, NULL, NULL, NULL);
	ns_close ();

	/* rc is 0 (as we gave 0 space), t0 is the group ID and t1 # members */

	if (rc<0) {
/*		fprintf(stderr,"There are no processes listed under [%s]\n", startupname);
		return (-t0);
		*/

		/* Please keep returned error number match CONS_NOHOST in Harness/hcore/utils/console/utils.h */
		return -6;
	}


	addrs = (int*) malloc ((sizeof(int) * t1));
	ports = (int*) malloc ((sizeof(int) * t1));

	sockets = (int*) malloc ((sizeof(int) * t1)); /* where we connect via */

	ns_open ();
	rc = ns_info (startupname, t1, &t0, &t1, NULL, addrs, ports);
	ns_close ();

	/* yes check again... */

	if (rc<0) {
		/*
		fprintf(stderr,"There are no processes listed under [%s]\n", startupname);
		return (-t0);
		*/

		/* Please keep returned error number match CONS_NOHOST in Harness/hcore/utils/console/utils.h */
		return -6;
	}

#else /* OLDNSLOOKUPOFNOTIFYSERVICE */

	/* ok rather than do it the old way, we just lookit up */
	/* using the new neat short routine */

	rc = startup_finder_vm (vmname);		/* easier :) */

	if (rc<1) {
            rc=startup_new_services(np,vmname);     /* load services if posiible */
        }

        if (rc<1) {
		/*
            sprintf(startupname, "%s:ftmpi:services:startup_notify_service\0",vmname);
            fprintf(stderr,"There are no processes listed under [%s]\n", startupname);
	    return (-1);
	    */
	/* Please keep returned error number match CONS_NOHOST in Harness/hcore/utils/console/utils.h */
	 return -6;
	}
	
#endif /* OLDNSLOOKUPOFNOTIFYSERVICE */

        if(nhosts!=HOSTAUTO) { 
	    nsud = nhosts;	
	} else {
	    nsud = rc;	
	}

/* 	for(i=0;i<nsud;i++) { */
/* 		printf("Startup daemon [%d] at addr [0x%x] port [%d]\n", i,  */
/* 					addrs[i], ports[i]); */

/* getting the socket once and sending multiple requests doesn't work! */
					/* Why? ask Tone.. */
/* 		sockets[i] = getconn_addr (addrs[i], &ports[i], 10); */
/* 		printf("Connect on socket [%d]\n", i); */
/*  */
/* 		} */

/* almost ready to start them up. */
/* first make space to store them gids */

gids = (int*) malloc (np*sizeof(int));
for (i=0;i<np;i++) gids[i] =0;	/* or use bzero later */

/*
npp=((np-1)/nsud);
if(((np-1)%nsud)!=0) {
   npp++;
}
if((np-1) > nsud) {
    ncp=nsud; 
} else {      
    ncp=(np-1);
}
*/
npp=(np/nsud);
if((np%nsud)!=0) {
   npp++;
}

if(np > nsud) {
    ncp=nsud; /* number of core parallel */
} else {      
    ncp=np;
}

gidlist = (int **)malloc(ncp * sizeof(int *));

for(j=0;j<ncp;j++)
{
    gidlist[j]=(int *)malloc(sizeof(int)*npp); 
}
   

for(i=0;i<ncp;i++) {
    for(j=0;j<npp;j++) {
        gidlist[i][j]=-1;
    }
}


/* Ok, everything should work from this line, so we print some information to users */
if(!silent) {
     printf("Starting %d process(es) of ",np); 
     for(l=0;l<exeargc;l++) printf("%s ",exeargv[l]);
     puts("");
     if(cwdir!=NULL) printf("Current working directory is %s\n\n",cwdir);
}


#ifdef USE_OPENSSL
sockfd = (SSL **) malloc (ncp*sizeof(SSL *));
#else
sockfd = (int*) malloc (ncp*sizeof(int));
for (i=0;i<ncp;i++) sockfd[i] =-1;	/* or use bzero later */
#endif

rclist = (int*) malloc (ncp*sizeof(int));
for (i=0;i<ncp;i++) rclist[i] =-1;	/* or use bzero later */

done = (int*) malloc (ncp*sizeof(int));
for (i=0;i<ncp;i++) done[i] =-1;	/* or use bzero later */

eachngid = (int*) malloc (ncp*sizeof(int));
for (i=0;i<ncp;i++) eachngid[i] =-1;	/* or use bzero later */

counter=0;

/* At the moment we loop and do the connect shuffle on the round robin but... */
/* we should compute this in advance and so that we do multi startup per loop */
/* this also allow us to do cluster attributes as per MPI JOD */

	/* We have to pass each process some envs */
	/* so we make some space here for that */
	envp = (char**) malloc (14*sizeof(char*));	/* ops.. must be n+1 */
	envp[0] = (char *) malloc (256);  /* runID */
	envp[1] = (char *) malloc (256);  /* GID */
	envp[2] = (char *) malloc (256);  /* NS_HOST */
	envp[3] = (char *) malloc (256);  /* NS_PORT */
	envp[4] = (char *) malloc (256);  /* pgid */
	envp[5] = (char *) malloc (256);  /* exename*/
	envp[6] = (char *) malloc (256);  /* jobsize */
	envp[7] = (char *) malloc (256);  /* leader ID */
	envp[8] = (char *) malloc (256);  /* output host */
	envp[9] = (char *) malloc (256);  /* output port */
	envp[10] = (char *) malloc (256); /* vmname */ 
	envp[11] = (char *) malloc (256); /* cwdir */ 
	envp[12] = (char *) malloc (256); /* silent flag */ 
	envp[13] = NULL;

	/* we can set the first ENV value */
	sprintf(envp[0],"FTMPI-RUNID=%d", mid);

	/* the second is the GID */

	/* the third is the ns host and forth the ns port */
	sprintf(envp[2],"HARNESS_NS_HOST=%s", getenv("HARNESS_NS_HOST"));
	sprintf(envp[3],"HARNESS_NS_HOST_PORT=%s", getenv("HARNESS_NS_HOST_PORT"));

	/* we can set the parent ENV value */
	if((flag&RUN_FTMPI)==RUN_FTMPI) {
	       sprintf(envp[4],"FTMPI-PGID=%d", mid);	/* their parent is FTMPIRUN */
	       sprintf(envp[5],"FTMPI-EXENAME=%s", exeargv[0]);	/* the exe name! */
	       sprintf(envp[6],"FTMPI-REQJOBSIZE=%d", np);	/* the requested/expected job size */
	       sprintf(envp[8],"FTMPI-OUTPUT-HOST=%d",out_addr); /* output host */
	       sprintf(envp[9],"FTMPI-OUTPUT-PORT=%d",out_port); /* output port */
	       sprintf(envp[10],"FTMPI-VMNAME=%s",vmname);       /* vmname */
	       if(cwdir!=NULL) {
	           sprintf(envp[11],"FTMPI-CWDIR=%s",cwdir);     /* cwdir */
	       } else {
	           sprintf(envp[11],"FTMPI-CWDIR=NONE");         /* cwdir */
	       }
	       sprintf(envp[12],"FTMPI-SILENT=%d",silent);         /* silent */
	} else {
	       sprintf(envp[4],"HCORE-PGID=%d", mid);	/* their parent is HRUN */
	       sprintf(envp[5],"HCORE-EXENAME=%s", exeargv[0]);	/* the exe name! */
	       sprintf(envp[6],"HCORE-REQJOBSIZE=%d", np);	/* the requested/expected job size */
	       sprintf(envp[8],"HCORE-OUTPUT-HOST=%d",out_addr); /* output host */
	       sprintf(envp[9],"HCORE-OUTPUT-PORT=%d",out_port);   /* output port */
	       sprintf(envp[10],"HCORE-VMNAME=%s",vmname);         /* vmname */
	       if(cwdir!=NULL) {
	           sprintf(envp[11],"HCORE-CWDIR=%s",cwdir);         /* cwdir */
	       } else {
	           sprintf(envp[11],"HCORE-CWDIR=NONE");         /* cwdir */
	       }
	       sprintf(envp[12],"FTMPI-SILENT=%d",silent);         /* silent */
	}

	/* as startup_d doesn't give the args unless we pass them all */

	/* This ENV value is set if you are expected to be the leader/root. */
	/* This allows the leader to get to the state record and post a leader record earlier */
	/* the real setting of the leader is done internally within ft-mpi-sys */

	/* expected leader value set within the startup loop */

	/* note as soon as we 'think' we have spawned a leader successfully then we reset this! */
	leader = -1;	

	/* now we round robin on the startup daemons */
        if(nhosts==HOSTAUTO) { 
	   sud = 0;	
	} else {
	   indexsud=0;
           sud = hostlist[indexsud];
	}
	started = 0;	/* our process count */
	lastloopstarted=0;	/* how many we started last loop */
	/* why two vars.. just in case we lose all startup_d in the middle! */


	ns_open ();
	ns_gid (NS_ID_FTMPI,np,gids);	
	ns_close ();

	/* fill gids to gidlist */
	/* sorry for this stupid loop */


        for(i=0;i<npp;i++) {
            for(j=0;j<ncp;j++) {
	       if( ((i*ncp)+j) >= np ) {
                   break;
	       }
               gidlist[j][i]=1;
	    }
	}

        k=0;
	for(i=0;i<ncp;i++) {
	    for(j=0;j<npp;j++) {
                if(gidlist[i][j]!=-1) {
                    gidlist[i][j]=gids[k];
		    k++;
		}
	    }
	}

        for(i=0;i<npp-1;i++) {
           gidlist[0][i]=gidlist[0][i+1];
	}
        gidlist[0][npp-1]=-1;

	if((flag&RUN_FTMPI)==RUN_FTMPI) {
	     sprintf(envp[7],"FTMPI-LEADER=%d", gids[0]);	/* expected leader */
             sprintf(envp[1],"FTMPI-GID=%d", gids[0]);	
	} else {
	     sprintf(envp[7],"HCORE-LEADER=%d", gids[0]);	/* expected leader */
	     sprintf(envp[1],"HCORE-GID=%d", gids[0]);	
        }

        /* start leader first, we have to make sure that leader can start */

	while(sud<nsud) {
	     if (exeargc == 0){
		rclist[0] = startup_spawn(sud, mid, mid, gids, exeargv[0], cwdir, 0, NULL, 13, envp, out_addr, out_port);
	     } else {	 /* if args send them! (bug spotted by Tone) */
		rclist[0] = startup_spawn(sud, mid, mid, gids, exeargv[0], cwdir, exeargc, exeargv, 13, envp, out_addr, out_port);
	     }
	     if(rclist[0]>0) {
#ifdef VERBOSE
	        if (!silent) printf("Leader: Envs are [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]\n", 
			envp[0], envp[2], envp[3], envp[4], envp[5], envp[6], envp[7], envp[8], envp[9], envp[10], envp[11], envp[12], envp[1]);
#endif
		started++;
		/* tell the notifier about it */
	        if((flag&RUN_FTMPI)==RUN_FTMPI) {
		        rc = notifier_addme (mid, gids[0], 0, 0);
		}
		/* note the 0, 0, this allows the process itself to update this */
		/* my mid = the runid for this ftmpi applications lifetime */

		/* ok it worked, if we had no leader before, then now this must be it! */
		/* so we keep it set */
		leader=gids[0];
		/* done[0]=1; *//* just in case */
		break;
	     }
             if(nhosts==HOSTAUTO) { 
	         sud++;
	     } else {
	         indexsud++;
                 sud = hostlist[indexsud];
	     }
	}

        first=1;

        if(leader==-1) {
	    fprintf(stderr,"Everyone failed to start? No pain, no gain, no leader to save you.\nMaybe the exe is not in the $HARNESS_ROOT/bin/$HARNESS_ARCH directory?\n");
	    return -1;
	} 

        /* We have our leader. Let's start followers (peon) */

        i=0;
        counter=0;
	begin=0;

	while(i<ncp) {

          if(done[i]==-1) {
                if(first==1) {
                    first=0;
		} else {
                    if(nhosts==HOSTAUTO) { 
	                sud++;
	            } else {
	                indexsud++;
                        sud = hostlist[indexsud];
	            }
		    if(nhosts==HOSTAUTO) {
	                if (sud==nsud) sud = 0;
		    } else {
                        if (indexsud==nsud) {
	                   indexsud=0;
                           sud = hostlist[indexsud];
		        }
		    }
	        }

		/* build them at startup_d */
	        if((flag&RUN_FTMPI)==RUN_FTMPI) {
		        sprintf(envp[1],"FTMPI-GID=%d", gids[i]);	
	        } else {
		        sprintf(envp[1],"HCORE-GID=%d", gids[i]);	
		}

#ifdef OLDNSLOOKUPOFNOTIFYSERVICE 
		{
		  int surc;
		  sockets[sud] = getconn_addr (addrs[sud], &ports[sud], 0); 
		  /* yes we have to do this each time */
		  surc = startupAdd_rpc (sockets[sud], mid, gids[i], "FTMPIRUN started",
					 argv[exe], 0, NULL, 10, envp, 0, NULL);
		  printf("RETURNED %d\n",surc);
		  /* close connections to startup daemons */
		  startupRPCclose(sockets[sud]);
		}
#else /* i.e. the new calls */
	/* which hide a lot of details */

		for(ngid=0;ngid<npp;ngid++) {
        /*            printf("AAAAA i is %d ngid is %d gidlist[i][ngid] is %d\n",i,ngid,gidlist[i][ngid]); */
                    if(gidlist[i][ngid]==-1) {
                          break;
		    }
		}
		eachngid[i]=ngid;

		/* if no one need to spawn in this core. Let's  continue to the next core */
		if(ngid==0) {
                   done[i]=0;
		   i++;
		   continue; 
		}

	        if (exeargc == 0){
		     rclist[i] = startup_spawn_async(sud, mid, mid, flag, ngid, gidlist[i], exeargv[0], cwdir, 0, NULL, 13, envp, out_addr, out_port, &sockfd[i]);
	        }   else {	 /* if args send them! (bug spotted by Tone) */
		     rclist[i] = startup_spawn_async(sud, mid, mid, flag, ngid, gidlist[i], exeargv[0], cwdir, exeargc, exeargv, 13, envp, out_addr, out_port, &sockfd[i]);
	        }

#endif /* OLDNSLOOKUPOFNOTIFYSERVICE */

                 counter++;
            } /* start someone */
            i++;
            if(counter==nsud || i==ncp) {
               ntag = 3;
               tags[0] = 0;
               tags[1] = 0;
               tags[2] = 0;
	       p=begin;

	       for(d=0; d < counter; ) {
                   if(done[p] == -1) {
                        if(rclist[p] >= 0) {
#ifdef USE_OPENSSL
                            rclist[p] = recv_pkmesg_s(sockfd[p], &from, &ntag, tags, NULL);
#else
                            rclist[p] = recv_pkmesg(sockfd[p], &from, &ntag, tags, NULL);
#endif
                            if ((ntag != 3) || (tags[0] != PROTO_STARTUP_SPAWN)) { 
                                done[p]=-1;
                            } else {
			        if(tags[1] > 0 && rclist[p] >= 0) { /* we can start this guy */
                                   if(eachngid[p]!=0) {
#ifdef VERBOSE
	                               if (!silent ) printf("Follower: Envs are [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s] [%s]", 
			               envp[0], envp[2], envp[3], envp[4], envp[5], envp[6], envp[7], envp[8], envp[9], envp[10], envp[11], envp[12]);
#endif
				       for(j=0;j<npp;j++) {
                                            if(gidlist[p][j]==-1) break; 
#ifdef VERBOSE
	                                    if((flag&RUN_FTMPI)==RUN_FTMPI) {
                                                if (!silent ) printf(" [FTMPI-GID=%d]",gidlist[p][j]); 
	                                    } else {
                                                if (!silent ) printf(" [HCORE-GID=%d]",gidlist[p][j]); 
					    }
#endif
				       }
#ifdef VERBOSE
				       if (!silent ) printf("\n");
#endif
	                            } 

	                            started=started+eachngid[p];
		/* previously we always added a blank record for the notfier */
		/* but it could race with the real task? */
		/* so, I (GEF) am commenting it out for now */
		/* we leave the leader blank in, but also update notifier */
		/* to ignore a blank overwrite of a valid notify record */
/*                                     if((flag&RUN_FTMPI)==RUN_FTMPI) { */
/* 	                                rc = notifier_addme (mid, gids[p], 0, 0);
 * 	                                */
/* 	                            }  */

			            done[p]=1;
			            rclist[p]=tags[1];
			         } else {
			            done[p]=-1;
			         }
			    }
			    if(done[p]==-1) {
                                 if(p<i) i=p;
			    }
			} else {
                            if(p<i) i=p;
			}

#ifdef USE_OPENSSL
                        SSL_shutdown(sockfd[p]);
#else
                        closeconn(sockfd[p]);
#endif

		        d++;
		   }
		   p++;
               } 
               if(counter==nsud) {
		   if (started==lastloopstarted) { 
			break;
		   } else { 
			lastloopstarted = started;
		   } 
	       } 
               counter=0;
	       begin=i;
	   }	
      } /* loop starting processes.. */


	/* ok have started up the job */
	/* I USED to nominate the leader and write out the DB */
	/* then I head off into the sunset */

/* *** this is now done in the startup loop itself ***
	leader = -1;
	for (i=0;i<np;i++) if (gids[i]) { leader = gids[i]; break; }
	*** LEAVE commented OUT ***
	GEF
*/



	if (leader==-1) { 
		fprintf(stderr,"Everyone failed to start? No pain, no gain, no leader to save you.\nMaybe the exe is not in the $HARNESS_ROOT/bin/$HARNESS_ARCH directory?\n");
		}
	else {	/* we have a leader (or survivers) so publish basic info for them */

/* JUNK for debugging a slow slow startup */
/* puts("sleeping");	fflush(stdout); */
/* sleep (10); */
/* puts("sleeping");	fflush(stdout); */

	if((flag&RUN_FTMPI)==RUN_FTMPI) {
		/* FIRST THE LEADER RECORD */
		sprintf(statename, "FTMPI:leader:%x\0", mid);
		mb = get_msg_buf_of_size (3*sizeof(int), 0, 0); 

		t0 = 0; 
		pk_int32(mb, &leader, 1);	/* leaders ID */
		pk_int32(mb, &t0, 1);
		pk_int32(mb, &t0, 1);

		ns_open();
		/* put the record up there, in first place, delete buffer afer */
		ns_record_put (leader, statename, mb, RECORD_DB_SINGLEONLY, 0, &t2, &t3, 1);

		/* Note FTMPIRUN puts the leader record OWNED by the LEADER */
		/* Not by the runid etc etc */
		/* This is so the new leader can control it correctly and that */
		/* the ns_record_swap will work on an election algorithm */

		/* Now the state record */

		mb = get_msg_buf_of_size ((2+5+started)*sizeof(int), 1, 0); 

	/* record is only 5 now, but we add gids in the next section */
	/* the +2 is for the comm and conn mode info in the startup rec */

		t0 = FT_STATE_START;
		t1 = 0;	/* epoch = 0 */
		pk_int32(mb, &t0, 1);
		pk_int32(mb, &leader, 1);
		pk_int32(mb, &t1, 1);
		pk_int32(mb, &started, 1);
		pk_int32(mb, &np, 1);

	/* could also pack com/con/msg mode info here as well */
	/* todo later GEF */

	sprintf(statename, "FTMPI:state:%x\0", mid);
		/* put the record up there, in first place, don't delete mb afer */
	ns_record_put (mid, statename, mb, RECORD_DB_SINGLEONLY, 0, &t2, &t3, 0);

	/* OK now for the Startup record */
	/* we add to the state record, and GIDS of the new MPI_COMM_WORLD */

	for(i=0;i<np;i++) if (gids[i]) pk_int32(mb, &gids[i], 1);

	sprintf(statename, "FTMPI:startup:%x\0", mid);

	/* this record also holds the commicator and message modes */

	pk_int32(mb, &cm, 1);
	pk_int32(mb, &mm, 1);

	ns_record_put (mid, statename, mb, RECORD_DB_SINGLEONLY, 0, &t2, &t3, 1);

	/* this time we delete the mb after record putting */

	ns_close();	/* ns_open was before the leader record */

	/* that should do it.. they will be looping awaiting that info */

	} /* RUN_FTMPI */
	}
	

	/* below uses 'i' for proc count so set it to the correct value */
	i = started;

        /* clean up */ 
        if(gids!=NULL) free(gids); 

	}
#endif	/* using the NS only */

	 /* so now we are free to spawn a new set of nodes up */

	if (exeargc == 0)  /* no user code arguments */
		if (!silent ) printf("No user exe arguments\n"); 

	else  /* we do have user code arguments */

	/* if i != np what do we do? */

	if (!silent ) printf("Spawned %d %s procs\n", i, exeargv[0]);

	if((flag&RUN_NEEDOUTPUT)==RUN_NEEDOUTPUT) {
	   ret=rioc_poll(TIMEOUT,&cur_conn);
	   while(ret!=0 || cur_conn < i) {  
	      /* NOTE:  cur_conn may more than i in case of recovery */
              ret=rioc_poll(TIMEOUT, &cur_conn);
	   }
	   closeconn(out_sockfd);
	   ns_open ();
	   ns_del (outputname, out_group, out_mem);	/* clean up */
	   ns_close ();
	}

        /* clean up */

#ifdef OLDNSLOOKUPOFNOTIFYSERVICE 
        if(addrs!=NULL) free(addrs); 
        if(ports!=NULL) free(ports); 
        if(sockets!=NULL) free(sockets); 
#endif
        if(sockfd!=NULL) free(sockfd); 
        if(rclist!=NULL) free(rclist); 
        if(done!=NULL) free(done); 
        if(eachngid!=NULL) free(eachngid); 

        return 0;
}

/* common code borrowed from the g_hcore_d */
/* this should be in a common.c file grrr */

int my_addr (void)
{
  char hname[256];
  struct hostent *hp;
  char **p;
  struct in_addr in;

  gethostname (hname, 255);
  hp = gethostbyname (hname);
  p = hp->h_addr_list;
  (void) memcpy(&in.s_addr, *p, sizeof (in.s_addr));

  return ((int) in.s_addr);
}

void    init_lsocket (int *os, int *op)
{
int sl;
int p0;
int p;

        p0 = FTMPI_NOTIFIER_PORT_START;  /* obvious really */
        p = setportconn (&sl, p0, FTMPI_NOTIFIER_PORT_RANGE);
        if (p>0) {
#ifdef VERBOSE 
        printf("notify on port [%d] listening on socket [%d]\n", p, sl);
#endif
            *op = p;
            *os = sl;
        }

        else {
            fprintf(stderr,"Cannot create a listen socket at port %d in rangle % d\n", p0, 10);
            exit (p);
        }
}

/**
startup_halt - halt system
@param me who am I ?[INPUT]
@param errnum error number [OUTPUT]
@return error number, connection problem (error hostID specific in errnum)
*/
int startup_halt(int me, int *errnum)
{
    int ntag;
    int tags[1];
    int s, p;
    int rc;
    int i;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif

    ntag = 1;
    tags[0] = PROTO_STARTUP_HALT;
    for (i = 0; i < startup_found; i++) {
        p = startup_ports[i];
        s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, EMPTYMSGBUF, 0);
	SSL_shutdown(ssl);
	ctx_finalize(ctx);
#else
        rc = send_pkmesg(s, me, ntag, tags, EMPTYMSGBUF, 0);
#endif
        closeconn(s);
        if (rc <= 0) {
	    /*
            closeconn(s);
            (*errnum) = startup_entries[i]; 
            return -4;
	    */
        }
    }
    return 0; 
}

/**
startup_reset - reset system
@param me who am I ?[INPUT]
@param errnum error number [OUTPUT]
@return error number, connection problem (error hostID specific in errnum)
*/
int startup_reset(int me, int *errnum)
{
    int ntag;
    int tags[1];
    int s, p;
    int rc;
    int i;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif

    ntag = 1;
    tags[0] = PROTO_STARTUP_RESET;
    for (i = 0; i < startup_found; i++) {
        p = startup_ports[i];
        s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, EMPTYMSGBUF, 0);
	SSL_shutdown(ssl);
	ctx_finalize(ctx);
#else
        rc = send_pkmesg(s, me, ntag, tags, EMPTYMSGBUF, 0);
#endif
        closeconn(s);
        if (rc <= 0) {
	    /*
            closeconn(s);
            (*errnum) = startup_entries[i]; 
            return -4;
	    */
        }
    }
    return 0; 
}

/* startup_free - free allocated memory
*/
void startup_free(void)
{

   if (startup_found) {	
       free (startup_addrs);
       free (startup_ports);
       free (startup_entries);
   }
}

/**
specific_startup_info - get startup information
@param me who am I ?[INPUT]
@param i index of startup_d array [INPUT]
*/


int specific_startup_info(int me,int i,struct startup_info *sinfo)
{
  char arch[TMP_VAR_MAX];
  int ntag;
  int tags[1];
  int s, p;
  int rc;
  int mb;
  int from;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif

  if(i<0 || me<0 || sinfo == NULL) {
      return -1;
  } 

  mb = get_msg_buf(1);

  ntag = 1;
  tags[0] = PROTO_STARTUP_INFO;

  p = startup_ports[i];
  s = getconn_addr(startup_addrs[i], &p, 0);
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, EMPTYMSGBUF, 0);
#else
  rc = send_pkmesg(s, me, ntag, tags, mb, 1);
#endif

  if (rc <= 0) {
#ifdef USE_OPENSSL
    SSL_shutdown(ssl);
    ctx_finalize(ctx);
#endif
    closeconn(s);
    return -1;
  }

#ifdef USE_OPENSSL
  rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);
  SSL_shutdown(ssl);
  ctx_finalize(ctx);
#else
  rc = recv_pkmesg(s, &from, &ntag, tags, &mb);
#endif
  upk_string(mb, arch, TMP_VAR_MAX);
  strcpy(sinfo->arch,arch);
  free_msg_buf(mb);
  closeconn(s);
  return 0;
}


/**
startup_ping - startup ping. (lighter than getinfo etc)
@param me who am I ?[INPUT]
@param i index of startup_d array [INPUT]
@param hostrank of startup_d [OUTPUT]
@param mrtt measured round trip time in seconds [OUTPUT]
*/


int startup_ping(int me,int i, int *hostrank, double *mrtt)
{
  int ntag;
  int tags[1];
  int s, p;
  int rc;
  int from;
  double st, et;
#ifdef USE_OPENSSL
    SSL_CTX *ctx;
    SSL *ssl;
    BIO *sbio;
    char keyfilename[256];
    struct in_addr tmpaddr;
#endif


  if(i<0 || me<0) {
      return -1;
  } 

/*   mb = get_msg_buf(1); */

  ntag = 1;
  tags[0] = PROTO_STARTUP_PING;

  p = startup_ports[i];
  st = sec_time ();
  s = getconn_addr(startup_addrs[i], &p, 0);

  if (s<0) {
	 return (-1);
  }
#ifdef USE_OPENSSL
        tmpaddr.s_addr=get_my_addr();
        sprintf(keyfilename,"%s/.harness/%s_%s",getenv("HOME"),KEYFILE,inet_ntoa(tmpaddr)); 
        ctx=ctx_init(keyfilename,PASSWORD);
        /* Connect the SSL socket */
        ssl=SSL_new(ctx);
        sbio=BIO_new_socket(s,BIO_NOCLOSE);
        SSL_set_bio(ssl,sbio,sbio);
        if(SSL_connect(ssl)<=0) {
            berr_exit("SSL connect error");
        }
        tmpaddr.s_addr=startup_addrs[i];
    /* If we enable server authentication (uncomment check_cert(...) line) */
    /* The common name in certificate must be an IP address of it */
        /* check_cert(ssl,inet_ntoa(tmpaddr)); */
        rc = send_pkmesg_s(ssl, me, ntag, tags, EMPTYMSGBUF, 0);
#else
  rc = send_pkmesg(s, me, ntag, tags, EMPTYMSGBUF, 0);
#endif

  if (rc <= 0) {
#ifdef USE_OPENSSL
    SSL_shutdown(ssl);
    ctx_finalize(ctx);
#endif
    closeconn(s);
    return -1;
  }

#ifdef USE_OPENSSL
  rc = recv_pkmesg_s(ssl, &from, &ntag, tags, &mb);
  SSL_shutdown(ssl);
  ctx_finalize(ctx);
#else
  rc = recv_pkmesg(s, &from, &ntag, tags, NULL);
#endif
  et = sec_time ();
/*   free_msg_buf(mb); */
  closeconn(s);


if (rc<0) { /* bad recv of data! */
  if (hostrank) *hostrank = -1;
  if (mrtt) *mrtt = 0.0;
  return (-2);
}

  if (hostrank) *hostrank = from;
  if (mrtt) *mrtt = (et-st);

  return 0;
}

