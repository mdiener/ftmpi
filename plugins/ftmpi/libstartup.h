
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS STARTUP_D

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>
			Thata Angskun <angskun@cs.utk.edu>


 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
	These are the original ones I have modified to handle gids etc etc
	But they still use writeV							GEF STR Dec 2001
	Original routines based on Reeds Drone stuff winter 2000.
	Tone Modified spring 2001.
	Thara modified more in Spring 2003.
*/ 
#ifndef LIBSTARTUP_H__
#define LIBSTARTUP_H__ 1

#include <sys/types.h>
#ifndef WIN32
#include <unistd.h>
#endif  /* WIN32 */

#include "hlib.h"

/* protocol tag values */

#define PROTO_STARTUP_SPAWN 	8001
#define PROTO_STARTUP_ACK 		8111

#define PROTO_STARTUP_PS_ALL 		9001
#define PROTO_STARTUP_PS_ALL_ACK 	9111

#define PROTO_STARTUP_DELETE        9002
#define PROTO_STARTUP_DELETE_ACK    9112

#define PROTO_STARTUP_KILL      9003
#define PROTO_STARTUP_KILL_ACK          9113

#define PROTO_STARTUP_EXPIRE        9004
#define PROTO_STARTUP_EXPIRE_ACK        9114

#define PROTO_STARTUP_HALT        9005

#define PROTO_STARTUP_RESET       9006

#define PROTO_STARTUP_INFO        9007
#define PROTO_STARTUP_INFO_ACK        9117

#define PROTO_STARTUP_PING        9008
#define PROTO_STARTUP_PING_ACK        9118

/* global reply codes */
/* used by both transport and user lib RCs */
#define RESULT_ERROR_PROTO		-999	/* a bad message back */
#define RESULT_SUCCESS			0		/* general ok */


/* return flags if special values used */
#define RESULT_EXITING				100
#define RESULT_CANNOT_UNLOAD		200
#define RESULT_CANNOT_EXIT			300
#define RESULT_INUSE				400 /* same as busy */
#define RESULT_NO_AUTH				500
#define RESULT_EXIT_DELAYED			600 /* will as soon as poss */

#define STARTMAXPATHLEN			1024	/* maximum library expects path name */

/* startup spawn needs these constants */
#define SPAWN_NO_GID_NEEDED			-100
#define SPAWN_STARTUP_ALLOCATE_GIDS	-200

#define PROTO_STARTUP_CLOSED 	8999	/* what happens when we shut up shop */

#define GHCORE_DEFAULT_VM_NAME	"dvm"	/* should match console version! */

/* startup_run flag  */
#define RUN_FTMPI      1
#define RUN_HARNESS    2
#define RUN_NEEDOUTPUT 4

#define HOSTAUTO -1

#define TMP_VAR_MAX 40

struct startup_info {
    char arch[TMP_VAR_MAX];
};


/* The following structure should be the same as startupItem in startup.h */
/* However, we cannot send both startTiem & stopTime to here because of "long" is different */
/* in each architecture (unless we use DDT).   */

struct startup_procstat {
  char *	execName; /* path passed to startupAdd() */
  char *	comment;  /* comment passed to startupAdd() */
  char *	cwdir;  /* current working directory passed to startupAdd() */

#ifndef WIN32
  pid_t		pid;
#else
  int pid;
#endif

  int		caller;
  int		gid;

  int		exitValue;
  int 	        status;

  /*
  time_t 	startTime;
  time_t 	stopTime; 
  */

  /* We use elapsedTime instead of startTime & stopTime because of send time_t(long) across platform problem */
  int      elapsedTime;

  int       postabort; /* Do we post abort? In case of segmentation fault */
	              /* to prevent forever post abort message */

  int *		callbackList;
  int	    callbackListLength;   /* number of items in list */
  int	    callbackListCapacity; /* number of items mallocated for list */

  struct startup_procstat *next;
};



int  startupAdd_rpc(int fd,int caller_id,int gid,char *comment,char * path,int argc,char **argv,int envc,char **envp,int intc,int *intv);
int  startupMonitor_rpc(int fd,int pid,int size,int * array_of_who_knows_what);
int startupUnMonitor_rpc(int fd,int pid,int size,int * array_of_who_knows_what);
int  startupGetStatusByPid_rpc(int fd,int pid);
int  startupExpireItems_rpc(int fd,int age);
void startupPrintAll_rpc(int fd,int status);
void startupRPCclose(int fd);

/*
	These are are modified to use packed mesages
	I.e. msgbuf routines.
	A server that understands these cannot understand the above ones.
*/ 

/*
Finds startup and notify service daemons
returns number found.
(It stores con
*/
int	startup_finder (void);
int	startup_finder_vm (char *vmname);
int startup_new_services(int n, char *vmname);
int startup_total_services(int max, char *vmname);

/*
This takes the index from the above call service
and starts an exec as required
Args = index,me,runid,theirgid,path,argc,argv,envc,envp
*/
int startup_kill(int id, int signal, int me, int flag, int *errnum);
int startup_ps(int index, int me, int flag, struct startup_procstat **itemHead, int *hostID);
int startup_run(int flag, char *vmname, int np, int nhosts, int *hostlist, int cm, int mm, int argc, char **argv, char *cwdir); 
int  startup_spawn (int index,int me,int runid,int *theirgid,char *path, char *cwdir, int argc,char **argv,int envc,char **envp,int out_ip,int out_port);
int startup_delete(int,int);
int startup_new_service_on_core(coreinfo_t * hcore);
int specific_startup_kill(int i,int gid, int signal, int me, int flag);
int specific_startup_info(int me, int i, struct startup_info *);
int startup_ping(int me,int i, int *hostrank, double *mrtt);
int startup_reset(int me, int *errnum);
int startup_halt(int me, int *errnum);
int startup_expire(int age, int me, int *errnum);




void init_lsocket(int *os, int *op);
int my_addr(void);


extern int startup_found;
extern int *startup_addrs;
extern int *startup_ports;
extern int *startup_entries;
extern int silent;

#endif
