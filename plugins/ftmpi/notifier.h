
/*
	HARNESS G_HCORE
	HARNESS FT_MPI
	HARNESS NOTIFIER

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* message proto exchanges */
#define	NOTIFIER_PROTO_ADDME		7001
#define	NOTIFIER_PROTO_REMOVE		7002
#define	NOTIFIER_PROTO_REMOVEALL	7003
#define	NOTIFIER_PROTO_EXIT			7004
#define	NOTIFIER_PROTO_SHUTDOWN                 7005
#define	NOTIFIER_PROTO_ACK			7111
#define	NOTIFIER_PROTO_END			7999


/* length of the event journal */
#define MAXEVENTLOOP 300	/* good enough for 299 procs */

/* IDs */
#define NOTIFIER_ID_SERVER			1776


/* PORTS */
#define FTMPI_NOTIFIER_PORT_START		1948
#define FTMPI_NOTIFIER_PORT_RANGE		100


int	notifier_find (void);
int	notifier_addme (int runid, int gid, int addr, int port);
int     notifier_check(int from);
int	notifier_removeme (int runid, int gid) ;
int	notifier_removeall (int runid, int gid) ;
int notifier_clear_all_events (void);
int remove_event_by_gid (int id) ;
int find_event_by_gid (int id) ;
int add_event_to_loop (int id, int rc);
int	notifier_get_queued_events (int es, int myrunid, int *ids, int max);
int	notifier_send_exit_event (int from, int runid, int gid, int exitcode);
int notifier_shutdown(int from);
