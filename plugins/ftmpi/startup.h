
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Antonin Bukovsky <tone@cs.utk.edu> 
			Graham E Fagg <fagg@cs.utk.edu>
			Jeremy Millar <millar@cs.utk.edu>
			Reed Wade <wade@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* 
	History

	Graham designed something complex.
		It both starts things up and has a ** changable ** list of notify parties.
		It also has a simple req/reply interface that makes it a bit like a console.
	Reed wrote the inital version during Jan 2000.
		Most time spent catching processes that die in strange ways.
	Graham hacked on it.
	Tone added some _atb_ calls and unusual/colourfull debugging messages
		Also checked execution path to make sure you weren't running bad code.
		And wrote the listener that makes up the equivalent of a notify service tester.
	Graham hacked on it to get it working with ftmpi (Aug-Sep / Dec onwards)
		Also debugging why it doesnot like being loaded as a shared object.
	Jeremy ran away and got married, came back and is now looking at it.
	Graham added gid stuff as hcore/ftmpi doesn't care about pids but gids 
*/
#ifndef HCORE_STARTUP_H
#define HCORE_STARTUP_H 1

#include <sys/types.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/wait.h>
#endif

/* int _ATB_BLOCK = 0; */
/* int _ATB_ACPTNG = 0; */
/* now defined in startup.c where they should be in the first place */

/* port at which we hope to bind */
#define STARTUP_D_PORT_START	22500

/* range of ports before we give up */
/* note some UNIX implementations cannot handle Above 32768 or 65000 */
/* i.e. negative or they think it is 'top reserved' range... grr GEF */

#define STARTUP_D_PORT_RANGE (32767-STARTUP_D_PORT_START)

struct startupItem {
  char *	execName; /* path passed to startupAdd() */
  char *	comment;  /* comment passed to startupAdd() */
  char *	cwdir;  /* current working directory passed to startupAdd() */

#ifndef WIN32
  pid_t		pid;
#else
  int pid;
#endif

  int		caller;
  int		gid;

  int		exitValue;
  int 	        status;

  time_t 	startTime;
  time_t 	stopTime; /* 0 until the process is reaped */

  int       postabort; /* Do we post abort? In case of segmentation fault */
	              /* to prevent forever post abort message */

  int *		callbackList;
  int	    callbackListLength;   /* number of items in list */
  int	    callbackListCapacity; /* number of items mallocated for list */

  struct startupItem *next;
};


#define STATUS_INVALID  0
#define STATUS_RUNNING  1
#define STATUS_EXITED	2
#define STATUS_FAILED_ON_EXEC	3
#define STATUS_ALL	4


void startupInit( void );
int  startupAdd( int callerID, int gid, char* comment, int out_ip, int out_port, ... );
int  startupMonitor( pid_t pid, ... );
int  startupUnMonitor(pid_t pid, ...);

int startupGetStatus(struct startupItem *item);
int startupGetStatusByPid(int pid);


int startupGetNumberOfItem(void);

struct startupItem *startupGetItemByPid(int pid);
struct startupItem *startupGetNextItemByCaller(struct startupItem *item,int caller);
struct startupItem *startupGetNextItemByName(struct startupItem *item,char*s, int isRegex, int which);
struct startupItem *startupGetNextItem(struct startupItem *item);

#define SEARCH_EXECNAME 1
#define SEARCH_COMMENT  2
#define SEARCH_BOTH     3


void startupLoopOnce(void);

int  startupExpireItems(int age);
int am_i_alive(char *vmname,int port);
void rm_from_ns (char *vm);
int add_self_to_ns(char *vmname,int port);




void startupPrintItem(FILE *fp, struct startupItem *item);
void startupPrintAll(FILE *fp, int status);

void blockChildHandler(void);
void unblockChildHandler(void);

char *statusToString(int);
#endif
