/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 File:	plist	(plug-in list code)

 --------------------------------------------------------------------------

 Purpose:	handles internal list of plug-ins

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/* simple non linked list get it working code :) */

/* includes */

/* needed for timevals */
/* #include <sys/time.h> */
/* now just using long for secs */

#ifndef _GHCORE_H_PLIST
#define _GHCORE_H_PLIST 1

/* for the pointer to function notations */
#include "dlo3.h"

/* for ftypes */
#include "names.h"


#define MAXNAMELENGTH	512
#define MAXPATHNAMELEN	2048
#define MAXFUNCTIONSPERPACKAGE 10
#define MAXINSTANCES 1	/* this will solve that problem */

/* sizes of tables ? */
#define MAXCOMPONENTS   25
#define MAXFUNCTIONS    100


/* multiple instances */

typedef enum {
	MULTIALLOWED,
	SINGLEONLY
} instance_t;

/* invocation types */

typedef enum {
	DIRECTONLY,
	DIRECTANDREMOTE,
	REMOTEONLY,
	SERVICEONLY
}	invocation_t;

/* for now multiple instances will have multiple plist entries */
/* something for tone/sathish to fix later */


typedef struct {

	/* internal ID */

	int		plist_id;

	/* canonical stuff */
	char	fullname[MAXNAMELENGTH];
	char	author[MAXNAMELENGTH];
	char	comments[MAXNAMELENGTH];
	
	/* actual naming stuff */
	char	package[MAXNAMELENGTH];
	char	component[MAXNAMELENGTH];

	int		nfunctions;
	char	functions[MAXFUNCTIONSPERPACKAGE][MAXNAMELENGTH];

	/* actual library file */
	char	library[MAXPATHNAMELEN];
	void	*libhandle;
	int		finfo;

	/* Now for the function stuff */
	/* not all is per instance */

	/* is there a singleinit */
	int		singleinit;
	ifp		singleinit_fp;
	int		initcalled;

	int		fids[MAXFUNCTIONSPERPACKAGE];

	/* other functions are stored in flist entries */

	/* times etc */
	long	published;
	long	loaded;
	long	ttl;

}	plist_t;


/* each function has its own entry */

typedef struct {

	/* what is my function number ? */
	int	flist_id;

	/* from which plugin? */
	int	plist_id;

	/* where in the plist */
	int plist_findex;

	/* actual naming */
	char	function[MAXNAMELENGTH];		/* real symbol name of function */

	/* types of functions etc */
	int		singleinit;
	int		owninit;
	int		initcalled;
	int		ftype;
	int		rtype;

	/* now for the actual function pointers */
	vfp		vfunc_ft;
	ifp		ifunc_ft;
	ffp		ffunc_ft;
	dfp		dfunc_ft;

	/* for my own init function */
	ifp		init_fp;

}	flist_t;



/* prototypes */

int     get_symbol_into_new_flist (void *h,char * fname,int ftype,int rval);



/* list low level manipulation */
void init_plist ();
void init_flist ();
int     zero_plist (int i);
int zero_flist (int i);
int  find_free_plist ();
int  find_free_flist ();
int  release_plist (int id);
int  release_flist (int id);

/* inserting/editing data */
int     mk_plist (char *fullname,char * author,char * comments,char * package,char * component,int nf,char ** fnames,char * library,void * libhdl,int finfo,int sinit,ifp fp,long  published,long loaded,long  ttl);
int mk_flist ();
int     edit_plist (int id,char * fullname,char * author,char * comments,char * package,char * component,int nf,char ** fnames,char * library,void * libhdl,int finfo,int sinit,ifp fp,long published,long loaded,long ttl);
int     edit_flist ();

/* searches etc */
int     find_plists_by_fullname (int maxfound,char * sname,int * results);
int     find_flists_by_functionname (int maxfound,char * fname,int * results);
int     find_flists_by_plistid (int maxfound,int sid,int * results);

/* list high level manipulation */
int     display_plist_entry (int id,FILE * s);
int     display_plist_full_entry (int id,FILE * s);
int     display_flist_entry (int id,FILE * s);
int     display_flist_full_entry (int id,FILE * s);
int     display_full_package (int id,FILE * s);


/* tests */
void test_plist (int id);
int test_flist (int id);





#endif /*  _GHCORE_H_PLIST */
