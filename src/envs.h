
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _GHCORE_H_ENVS
#define _GHCORE_H_ENVS 1

/* these are global names usually set up in rc or profile */
#define envname_root "HARNESS_ROOT"
#define envname_lib "HARNESS_LIB_DIR"
#define envname_cache "HARNESS_CACHE_DIR"
#define envname_arch "HARNESS_ARCH"
#define envname_log "HARNESS_LOG"
#define envname_ghcored_port "HARNESS_GHCORED_PORT"
#define envname_ns_host "HARNESS_NS_HOST"
#define envname_ns_host_port "HARNESS_NS_HOST_PORT"
#define envname_rs_host "HARNESS_RS_HOST"
#define envname_rs_host_port "HARNESS_RS_HOST_PORT"

/* local names used to pass info between cores and services */
/* i.e. have different values on different hosts */
#define envname_vm_name "HARNESS_LOCAL_VMNAME"

void init_envs (void);
void get_envs_ns (void);
void get_envs (void);
void get_envs_local (void);
int set_local_vmname (char* name);


extern char*   HARNESS_NS_HOST;
extern int     HARNESS_NS_HOST_PORT;
extern char*   HARNESS_RS_HOST;
extern int     HARNESS_RS_HOST_PORT;

extern char*	HARNESS_LOCAL_VMNAME;
extern char*	HARNESS_ARCH;

#endif /* _GHCORE_H_ENVS */
