
/*
	HARNESS G_HCORE
	HARNESS FT_MPI

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Keith Moore <moore@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#ifndef _GHCORE_H_HTTP
#define _GHCORE_H_HTTP 1

typedef struct {
  int port;
  char *proto;
  char *host;	/* just the host name */
  char *_host;  /* the host name with :port if specified in original url */
  char *path;	/* everything aftyer host:port */
  char *full;	/* original url */
} URL;

int try_loading_http(int s,URL * url,char* headers,char** result);


#endif /*  _GHCORE_H_HTTP */
