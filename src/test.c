
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*

Example function by Graham (Nov 2000)

*/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <string.h>

#include "names.h"
#include "envs.h"
#include "finit.h"

#include "snipe_lite.h"

static int* count_ptr;

int	_hinit ()
{
puts ("I am a _hinit function\n");
return (1234);
}

int	_finfo (max, nfuncs, names, types, rvals, inits) 
int max;
int *nfuncs;
char *names[];
int	*types;
int	*rvals;
int	*inits;

	{
	int c=0;

	if (nfuncs) *nfuncs = 2;	/* real # of functions */
	if (c==max) return (c);		/* just in case we have 0 memory to store info */

	strcpy (names[c], "count");
	types[c] = FTYPE_SOCKET;
	rvals[c] = FINIT_SOCKET;
	inits[c] = 1;
	c++;
	if (c==max) return (c);

	strcpy (names[c], "backwards");
	types[c] = FTYPE_SOCKET;
	rvals[c] = FINIT_SOCKET;
	inits[c] = 0;
	c++;
	if (c==max) return (c);

	strcpy (names[c], "whoami");
	types[c] = FTYPE_SOCKET;
	rvals[c] = FINIT_SOCKET;
	inits[c] = 0;
	c++;
	if (c==max) return (c);

	/* also check my vmname here by getting it */
	get_envs_local (); /* not really needed as I am usually a thread.. */
        if(HARNESS_LOCAL_VMNAME!=NULL) {
	   printf("My local VMNAME is [%s]\n", HARNESS_LOCAL_VMNAME); 
        } else {
           printf("Cannot get my local VMNAME!!!!\n");
        }

	return (c); /* return the number of functions in package */
	}








/* REAL FUNCTIONS */

int count_init ()
/* int count_s_init () */
	{
	puts("funct count_init");
	count_ptr = (int *) malloc (sizeof(int));

	*count_ptr = 0;
	return (0);	/* int return type */

	}



int count_s (s)
	int s;
	{
	int res;
	int dummy=0;
	int sz;

	puts("funct count_s");
	printf("count_s replies by %d\n", s);
	res = ++(*count_ptr);

	printf("count_s returns %d\n", res);

	writeconn (s, &res, sizeof(int));
	writeconn (s, &res, sizeof(int));

	closeconn (s);

	return (*count_ptr);
	}

/* count backwards */
int backwards_s (s)
	int s;
	{
	int res;
	int dummy=0;
	int sz;

	puts("funct backwards_s");
	printf("backwards_s replies by %d\n", s);
	res = --(*count_ptr);

	printf("backwards_s returns %d\n", res);

	writeconn (s, &res, sizeof(int));
	writeconn (s, &res, sizeof(int));

	closeconn (s);
	return (*count_ptr);
	}

/* who am I ? */
int whoami_s (s)
	int s;
	{
	char hname[256];
	char porkpie[1024];
	int pid;
	int res;
	int sz;

	puts("funct whoami_s");
	printf("whoami replies by %d\n", s);

	gethostname (&hname, sizeof(hname));
	pid = getpid ();

	/* build my reply */
	sprintf(porkpie, "%s:%d\0", hname, pid);

	printf("whoami returns %s\n", porkpie);

	sz = strlen (porkpie);

	writeconn (s, &sz, sizeof(int));
	writeconn (s, porkpie, sz);

	closeconn (s);
	return (0);
	}

