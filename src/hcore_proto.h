
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/*
 This lists the new hcore v2 client-hcore-hcore protocol and message tags
 Graham Stuttgart Dec 2001
 */

/*
	This superceeds the original +, T, etc string based protocol
	with a message packed protocol based on msg/msgbuf routines
*/

/* 
	Calls are based on different classes such as info, load, unload,
	invocation, managemnet calls and service requests
*/

#ifndef _GHCORE_H_PROTO
#define _GHCORE_H_PROTO 1


/* INFO */
#define	HCORE_PROTO_PING	1001
#define	HCORE_PROTO_ECHO	1002
#define	HCORE_PROTO_INFO	1003
#define	HCORE_PROTO_PINFO	1010

/* LOAD */
#define	HCORE_PROTO_LOAD	2001

/* UNLOAD */
#define	HCORE_PROTO_UNLOAD	3001

/* INVOCATION */
#define	HCORE_PROTO_CALLFNC	4001
#define	HCORE_PROTO_CALLFNC_PROC	4002
#define	HCORE_PROTO_CALLFNC_THREAD	4003
#define	HCORE_PROTO_CALLFNC_DIRECT	4004
#define	HCORE_PROTO_CALLCMD	4020

/* MANAGEMENT */

/* SERVICE OPS */

/* Other details */ 

/* when using a packed receive you are required to receive upto n args */
#define HCORE_PROTO_MAXARGS	5


#endif /*  _GHCORE_H_PROTO */
