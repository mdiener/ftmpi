
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

#ifndef _GHCORE_H_INT
#define _GHCORE_H_INT 1

/* data structures needed by the G_HCORE_D internally */

/* fixed pool of workers ? */
#define MAX_THREADS	20	
/* note, have locked up when above this?? must find out why */

/* fixed maximum on forked processes */
#define MAXPROCS	10

/* threads / mutexes etc */
pthread_mutex_t	startlock [MAX_THREADS];

typedef enum {
	TS_IDLE,
	TS_RUNNING,
	TS_DONE,
	TS_EXITING,
	TS_EXIT
}	state_t;

typedef enum {
	NEXTOP_NOTHING,
	NEXTOP_INFO,
	NEXTOP_CALLFUNC,
	NEXTOP_CALLME,
	NEXTOP_EXIT
}	next_t;

typedef	struct	{

	/* about me */
	int	id;
	pthread_t	ptid;

	/* my state */
	state_t	state;

	/* who am I talking to */
	int	socket;

	/* what the core wants me to do next */
	next_t	next;

	/* what am I actually doing? */
	char fullname[MAXNAMELENGTH];
	char pname[MAXNAMELENGTH];
	char cname[MAXNAMELENGTH];
	char fname[MAXNAMELENGTH];

	/* ok, the real pointers to it */
	int	plist;
	int	flist;

	/* how am I controlled */
	pthread_mutex_t	lock;

	pthread_mutex_t	cvlock;
	pthread_cond_t  cv;

}	thread_state_t;

/* individual thread states */
thread_state_t             thread_state[MAX_THREADS];


#endif /*  _GHCORE_H_INT */
