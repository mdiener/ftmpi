/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




#include "names.h"
#include "internal_state.h"
#include<string.h>

void mk_fname (char *fname,char * pname,char * mname)
{

char tname [4096];
char libending[10];

#ifdef WIN32
	sprintf(libending, ".dll");
#else
	sprintf(libending, ".so");
#endif
	

if (!fname) return ;	/* no space, no name */

if (!mname) return ;	/* MUST have a module name */

if (!pname) 
	sprintf(tname,"%s%s\0", mname, libending);
else 
	sprintf(tname,"%s/%s%s\0", pname, mname, libending);

strcpy (fname, tname);

}

void mk_piname (char *piname,char * pname,char * mname,char * cname,int FTYPE)
{
char tname [4096];
char cending[10];

if (!piname) return ;	/* no space, no name */

if (!mname) return ;	/* MUST have a module name */

cending[0] ='\0';
if (FTYPE==FTYPE_DEFAULT) cending[0] ='\0';
if (FTYPE==FTYPE_HINIT)	sprintf(cending, "_hinit");
if (FTYPE==FTYPE_INIT)	sprintf(cending, "_init");
if (FTYPE==FTYPE_START)	sprintf(cending, "_start");
if (FTYPE==FTYPE_CONT)	sprintf(cending, "_cont");
if (FTYPE==FTYPE_STOP)	sprintf(cending, "_stop");
if (FTYPE==FTYPE_SUSPEND)	sprintf(cending, "_suspend");
if (FTYPE==FTYPE_SOCKET)	sprintf(cending, "_s");
if (FTYPE==FTYPE_FINFO)	sprintf(cending, "_finfo");

if (!pname) 
	sprintf(tname,"%s:%s%s\0", mname, cname, cending);
else 
	sprintf(tname,"%s:%s:%s%s\0", pname, mname, cname, cending);

strcpy (piname, tname);

}

/* 
	make a symbol name used by the actual dynamic loader 
*/

void mk_sname (char *sname,char * cname,int FTYPE)
{
char tname [4096];
char cending[10];

if (!sname) return ;	/* no space, no name */

if (!cname) return ;	/* MUST have a call name */

cending[0] ='\0';
if (FTYPE==FTYPE_DEFAULT) cending[0] ='\0';
if (FTYPE==FTYPE_HINIT)	sprintf(cending, "_hinit");
if (FTYPE==FTYPE_INIT)	sprintf(cending, "_init");
if (FTYPE==FTYPE_START)	sprintf(cending, "_start");
if (FTYPE==FTYPE_CONT)	sprintf(cending, "_cont");
if (FTYPE==FTYPE_STOP)	sprintf(cending, "_stop");
if (FTYPE==FTYPE_SUSPEND)	sprintf(cending, "_suspend");
if (FTYPE==FTYPE_SOCKET)	sprintf(cending, "_s");
if (FTYPE==FTYPE_FINFO)	sprintf(cending, "_finfo");

	sprintf(tname,"%s%s\0", cname, cending);

strcpy (sname, tname);

}



/* this makes the full library name for the modules library */

void	mk_lib_name (char *libname,char * pname,char * mname)
{
char tname [4096];
char tname2[4096];

if (!libname) return; /* no space, no name */

tname[0]='\0';

mk_fname (tname, pname, mname);
if (tname[0]=='\0') return;

/* now to build the full name with environment vars and arch type */
if (!HARNESS_LIB_DIR) return;
if (!HARNESS_ARCH)	return;

sprintf(tname2, "%s/%s/%s\0", HARNESS_LIB_DIR, HARNESS_ARCH, tname);

strcpy (libname, tname2);

}

void	mk_cache_name (char *cachename,char * pname,char * mname)
{
char tname [4096];
char tname2[4096];

if (!cachename) return; /* no space, no name */

tname[0]='\0';

mk_fname (tname, pname, mname);
if (tname[0]=='\0') return;

/* now to build the full name with environment vars and arch type */
if (!HARNESS_CACHE_DIR) return;
if (!HARNESS_ARCH)	return;

sprintf(tname2, "%s/%s/%s\0", HARNESS_CACHE_DIR, HARNESS_ARCH, tname);

strcpy (cachename, tname2);

}

