/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




/* this is where we set up all of our initial conditions */
/* from info in teh environment */

/* incept G Fagg Dec 2000 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "internal_state_sys.h"
#include "envs.h"

char*	HARNESS_ROOT;
char*	HARNESS_LIB_DIR;
char*	HARNESS_CACHE_DIR;
char*	HARNESS_ARCH;

int		HARNESS_GHCORED_PORT;

FILE*	HARNESS_LOG_FPTR;


static int init_envs_yet=0;
static int init_ns_envs_yet=0;

char*   HARNESS_NS_HOST;
int     HARNESS_NS_HOST_PORT;
char*   HARNESS_RS_HOST;
int     HARNESS_RS_HOST_PORT;

char*	HARNESS_LOCAL_VMNAME;


static int envs_silent;

void init_envs () {

HARNESS_ROOT = NULL;
HARNESS_LIB_DIR = NULL;
HARNESS_CACHE_DIR = NULL;
HARNESS_ARCH = NULL;
HARNESS_GHCORED_PORT = 0;
HARNESS_LOG_FPTR = NULL;
HARNESS_NS_HOST = NULL;
HARNESS_NS_HOST_PORT = 0;
HARNESS_RS_HOST = NULL;
HARNESS_RS_HOST_PORT = 0;

HARNESS_LOCAL_VMNAME = NULL;
 envs_silent      = 0; /*default: talk to me */

init_envs_yet = 1;

/* from now on use get_envs, which both gets and releases memory */
}

void init_ns_envs () {

HARNESS_NS_HOST = NULL;
HARNESS_NS_HOST_PORT = 0;
HARNESS_RS_HOST = NULL;
HARNESS_RS_HOST_PORT = 0;

init_ns_envs_yet = 1;
 envs_silent      = 0; /*default: talk to me */
/* from now on use get_ns_envs, which both gets and releases memory */
}

void get_envs ()
{

int cc;
char *p;
int i, j;

/*
	check for init 
*/

if (!init_envs_yet) {
	fprintf(stderr,"Internal error, get_envs called before init!\n");
	exit (-1);
}

/* Are we allowed to talk or shall we shut up ? */
p =(char *) malloc (4096);
if ( getenv("FTMPI-SILENT") ) {
  strcpy (p, getenv ("FTMPI-SILENT"));
  envs_silent = atoi(p);
}
else
  free ( p );

/* First set all to null and then set them one by one */
p =(char *) malloc (4096);
if (getenv(envname_root)) {
	strcpy (p, getenv(envname_root));
	HARNESS_ROOT = p;
	}
else {
	if (!envs_silent) puts ("No root path specified\n");
	free (p);
	}


p = (char *) malloc (4096);
if (getenv(envname_lib)) {
	strcpy (p, getenv(envname_lib));
	HARNESS_LIB_DIR = p;
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_cache)) {
	strcpy (p, getenv(envname_cache));
	HARNESS_CACHE_DIR = p;
	}
else {
	if (!envs_silent) puts ("No cache path specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_arch)) {
	strcpy (p, getenv(envname_arch));
	HARNESS_ARCH = p;
	}
else {
	if (!envs_silent) puts ("No architecture type specified\n");
	free (p);
	}

/* 
	LOGGING SHOULD GO HERE 
*/

/*
p = malloc (4096);
if (getenv(envname_lib)) {
	strcpy (p, getenv(envname_lib));
	HARNESS_LIB_DIR = p;
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}
*/


/*
	NS SERVICES 
*/

p = (char *) malloc (4096);
if (getenv(envname_ns_host)) {
	strcpy (p, getenv(envname_ns_host));
	HARNESS_NS_HOST = p;
	}
else {
	if (!envs_silent) puts ("No Name Service Host specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_ns_host_port)) {
	strcpy (p, getenv(envname_ns_host_port));
	HARNESS_NS_HOST_PORT = atoi (p);
	}
else {
	if (!envs_silent) puts ("No Name Service Port specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_rs_host)) {
	strcpy (p, getenv(envname_rs_host));
	HARNESS_RS_HOST = p;
	}
else {
	if (!envs_silent) puts ("No Ring Service Host specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_rs_host_port)) {
	strcpy (p, getenv(envname_rs_host_port));
	HARNESS_RS_HOST_PORT = atoi (p);
	}
else {
	if (!envs_silent) puts ("No Ring Service Port specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_ghcored_port)) {
	strcpy (p, getenv(envname_ghcored_port));
	HARNESS_GHCORED_PORT = atoi (p);
	}
else {
	if (!envs_silent) puts ("No default GHCORE contact port specified\n");
	free (p);
	}
}

void get_envs_ns ()	/* get the envs JUST for the NS. (like in the hlib) */
{

int cc;
char *p;
int i, j;
char *rstring; 

/*
	check for init 
*/

if (!init_envs_yet) {
/* 	fprintf(stderr,"Internal error, get_envs called before init!\n"); */
/* 	exit (-1); */
/* be nice and do it for them :) */
	init_ns_envs ();
	}

/* Are we allowed to talk or shall we shut up ? */
p =(char *) malloc (4096);
if ( getenv("FTMPI-SILENT") ) {
  strcpy (p, getenv ("FTMPI-SILENT"));
  envs_silent = atoi(p);
}
else
  free ( p );

/* First set all to null and then set them one by one */

/*
	NS SERVICES 
*/

p = (char *) malloc (4096);
if (getenv(envname_ns_host)) {
	strcpy (p, getenv(envname_ns_host));
	HARNESS_NS_HOST = p;
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_ns_host_port)) {
	strcpy (p, getenv(envname_ns_host_port));
	HARNESS_NS_HOST_PORT = atoi (p);
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_rs_host)) {
	strcpy (p, getenv(envname_rs_host));
	HARNESS_RS_HOST = p;
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_rs_host_port)) {
	strcpy (p, getenv(envname_rs_host_port));
	HARNESS_RS_HOST_PORT = atoi (p);
	}
else {
	if (!envs_silent) puts ("No library path specified\n");
	free (p);
	}

}

/* get local info via the environment */
void get_envs_local ()	
{

int cc;
char *p;
int i, j;
char *rstring; 

/*
	check for init 
*/

/* Are we allowed to talk or shall we shut up ? */
p =(char *) malloc (4096);
if ( getenv("FTMPI-SILENT") ) {
  strcpy (p, getenv ("FTMPI-SILENT"));
  envs_silent = atoi(p);
}
else
  free ( p );


/* First set all to null and then set them one by one */

/*
	LOCAL SERVICES 
*/

p = (char *) malloc (4096);
#ifdef VERBOSE
if (!envs_silent) printf("[%s:%d] envname_vm_name was set [%s]\n",__FILE__,__LINE__, envname_vm_name);
#endif

if (getenv(envname_vm_name)) {
	strcpy (p, getenv(envname_vm_name));
	HARNESS_LOCAL_VMNAME = p;
	} else {
#ifdef VERBOSE
	if (!envs_silent) printf ("[%s:%d] No local VM name specified\n",__FILE__,__LINE__);
#endif
	free (p);
	}

p = (char *) malloc (4096);
if (getenv(envname_arch)) {
	strcpy (p, getenv(envname_arch));
	HARNESS_ARCH = p;
	} else {
	free (p);
	}



}

/* other misc local functions */
/* like setting envs */
/* normally done by profile/rc but sometimes needed to be done manually */

int set_local_vmname (char* name)
{
char *tmpvmname; 
char *hahaha;
int len;
int rc;

	if (!name) {
		if (!envs_silent) puts("set_local_vmname: no name given for local vmname??\n");
		return (-1);
		}

len = strlen (envname_vm_name) + strlen (name)+ 2; /* 1 for = and 1 for null */
tmpvmname =(char *) malloc (len);
if (!tmpvmname) {
		puts ("set_local_vmname: internal error cannot allocate space for vmname???Blame Thara.\n");
		exit (-999);
}

rc = sprintf (tmpvmname, "%s=%s", envname_vm_name, name);

/* printf("set_local_vmname: setting local vmname [%s]\n", tmpvmname); */
putenv (tmpvmname);

/* some systems do not malloc memory for the env when set by */
/* user processes. solaris does, linux does not */

#ifndef DONOTFREEPUTENVSTR
free (tmpvmname); 
#endif 

/* sprintf(tmpvmname,"%s=stupid", envname_vm_name); */

/* hahaha = getenv (envname_vm_name); */
/* if (hahaha) printf("its %s\n", hahaha); */
/* else */
/* puts("stupid"); */

  return 0;
}
