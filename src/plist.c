/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 File:	plist	(plug-in list code)

 --------------------------------------------------------------------------

 Purpose:	handlis internal list of plug-ins

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* includes */
#include <stdio.h>
#include <stdlib.h>

/* local includes */
#include "plist.h"
#include "finit.h"	/* for the FINIT_ return types */
#include <strings.h>


/* data yes :) */


plist_t	plist[MAXCOMPONENTS];
flist_t	flist[MAXFUNCTIONS];

int		plistcnt;
int		flistcnt;

/* tests */
void test_plist (int id);
int test_flist (int id);

void	init_plist ()
{
int i, j;

for (i=0;i<MAXCOMPONENTS;i++)	zero_plist (i); 
plistcnt = 0;
}

void	init_flist ()
{
int i, j;

for (i=0;i<MAXFUNCTIONS;i++)	zero_flist (i); 
flistcnt = 0;
}


int init_plist_blank_entry ()
{
int id;
char t[256];

id = 0;
strcpy(t, "Reserved");
strcpy(plist[id].fullname,	t);
strcpy(plist[id].author,	t);
strcpy(plist[id].comments,	"Reserved by the system for internal use");
strcpy(plist[id].package,	t);
strcpy(plist[id].component,	t);
strcpy(plist[id].functions[0],	t);
strcpy(plist[id].library,	t);

plist[id].nfunctions = 0;
plist[id].singleinit = 0;

plist[id].published = 0;
plist[id].loaded = 0;
plist[id].ttl = 0;

plist[id].plist_id = id;

return (0);
}





int	zero_plist (int i)
{
int j;

	if (plist[i].plist_id == -1) {
		fprintf(stderr,"plist: zeroing [%d] record that is already empty\n", i);
		fflush(stderr);
		return (-1);
	}

	plist[i].plist_id = -1;	/* so we know its free */

	plist[i].fullname[0] = '\0';
	plist[i].author[0] = '\0';
	plist[i].comments[0] = '\0';
	plist[i].package[0] = '\0';
	plist[i].component[0] = '\0';
	for (j=0;j<MAXFUNCTIONSPERPACKAGE;j++) plist[i].functions[j][0] = '\0';
	plist[i].library[0] = '\0';
	plist[i].libhandle  = NULL;
	plist[i].finfo = 0;
	plist[i].nfunctions = 0;

	plist[i].singleinit = 0;	
	plist[i].singleinit_fp = NULL;
	plist[i].initcalled = 0;	

	for (j=0;j<MAXFUNCTIONSPERPACKAGE;j++) plist[i].fids[j] = -1;

	plist[i].published = 0;
	plist[i].loaded = 0;
	plist[i].ttl = 0;


	init_plist_blank_entry (); /* special package for homeless f()s */


	return (0);
}



int	zero_flist (int i)
{
int j;

	if (flist[i].flist_id == -1) {
		fprintf(stderr,"flist: zeroing [%d] record that is already empty\n", i);
		fflush(stderr);
		return (-1);
	}

	flist[i].flist_id = -1;	/* free */
	flist[i].plist_id = -1; /* from no one yet */
	flist[i].plist_findex = -1; /* from no where in a land of no where */

	flist[i].function[0] = '\0';

	flist[i].singleinit = 0;
	flist[i].owninit = 0;
	flist[i].initcalled = 0;

	flist[i].ftype = FTYPE_DEFAULT;
	flist[i].rtype = FINIT_VOID;	

		flist[i].vfunc_ft = NULL;
		flist[i].ifunc_ft = NULL;
		flist[i].ffunc_ft = NULL;
		flist[i].dfunc_ft = NULL;


	flist[i].init_fp = NULL;

	return (0);

}



int		find_free_plist ()
{
int i;

for (i=0;i<MAXCOMPONENTS;i++) {
	if (plist[i].plist_id==-1) {
		plist[i].plist_id=i;		/* MARK AS OWNED */
		plistcnt++;
		return (i);
		}
	}
return (-1);
}

int		find_free_flist ()
{
int i;

for (i=0;i<MAXFUNCTIONS;i++) {
	if (flist[i].flist_id==-1) {
		flist[i].flist_id=i;		/* MARK AS OWNED */
		flistcnt++;
		return (i);
		}
	}
return (-1);
}


int  release_plist (int id)
{
	if (plist[id].plist_id == -1) {
		fprintf(stderr,"plist: releasing [%d] record that is already empty\n",
						id);
		fflush(stderr);
		return (-1);
	}

	plistcnt--;
	return (zero_plist (id));
}


int  release_flist (int id)
{

	if (flist[id].flist_id == -1) {
		fprintf(stderr,"flist: releasing [%d] record that is already empty\n", 
						id);
		fflush(stderr);
		return (-1);
	}

	flistcnt--;
	return (zero_flist (id));
}



int	mk_plist (char *fullname,char * author,char * comments,char * package,char * component,int nf,char ** fnames,char * library,void * libhdl,int finfo,int sinit,ifp fp,long  published,long loaded,long  ttl)
{
int id;
int j;

	id = find_free_plist ();
	if (id==-1) {
		fprintf(stderr,"plist: no room at the inn to add another package library\n" );
		fflush(stderr);
		return (-1);
	}


	/* silly test */
	if (plist[id].plist_id != -1) {
		fprintf(stderr,"plist: re-using [%d] record that is not already empty in a mk_plist?\n", id);
		fflush(stderr);
		return (-1);
	}


	plist[id].plist_id = id;	/* so we know its in use */

	if (fullname) strcpy( plist[id].fullname, fullname);
	if (author) strcpy ( plist[id].author, author);
	if (comments) strcpy ( plist[id].comments, comments);
	if (package) strcpy ( plist[id].package, package);
	if (component) strcpy ( plist[id].component, component);

	if (nf>MAXFUNCTIONSPERPACKAGE) {
		nf = MAXFUNCTIONSPERPACKAGE;
		fprintf(stderr,"Package [%s] ID [%d] could only store %d outof %d functions requested\n", 
						package, id, MAXFUNCTIONS, nf);
		fflush(stderr);
	}

	plist[id].nfunctions = nf;
	for (j=0;j<nf;j++) strcpy (&(plist[id].functions[j][0]), fnames[j]);

	if (library) strcpy ( plist[id].library, library);
	if (libhdl) plist[id].libhandle = libhdl;
	plist[id].finfo = finfo;

	plist[id].singleinit = sinit;	

	if (fp) plist[id].singleinit_fp = fp;

	plist[id].published = published;
	plist[id].loaded = loaded;
	plist[id].ttl = ttl;

	return (id);
}

int	edit_plist (int id,char * fullname,char * author,char * comments,char * package,char * component,int nf,char ** fnames,char * library,void * libhdl,int finfo,int sinit,ifp fp,long published,long loaded,long ttl)
{
int j;

	if ((id<0)||(id>=MAXCOMPONENTS)) {
		fprintf(stderr,"edit_plist: invalid [%d] plist ID\n", id);
		fflush(stderr);
		return (-1);
	}


	if (plist[id].plist_id == -1) {
		fprintf(stderr,"edit_plist: [%d] record is empty in a mk_plist?\n", id);
		fflush(stderr);
		return (-1);
	}


	plist[id].plist_id = id;	/* so we know its in use */

	if (fullname) strcpy( plist[id].fullname, fullname);
	if (author) strcpy ( plist[id].author, author);
	if (comments) strcpy ( plist[id].comments, comments);
	if (package) strcpy ( plist[id].package, package);
	if (component) strcpy ( plist[id].component, component);

	if (nf>MAXFUNCTIONSPERPACKAGE) {
		nf = MAXFUNCTIONSPERPACKAGE;
		fprintf(stderr,"Package [%s] ID [%d] could only store %d outof %d functions requested\n", 
						package, id, MAXFUNCTIONS, nf);
		fflush(stderr);
	}

	plist[id].nfunctions = nf;
	for (j=0;j<nf;j++) strcpy (&(plist[id].functions[j][0]), fnames[j]);

	if (library) strcpy ( plist[id].library, library);
	if (libhdl) plist[id].libhandle = libhdl;
	plist[id].finfo = finfo;

	plist[id].singleinit = sinit;	

	if (fp) plist[id].singleinit_fp = fp;

	plist[id].published = published;
	plist[id].loaded = loaded;
	plist[id].ttl = ttl;

	return (id);
}


int	find_plists_by_fullname (int maxfound,char * sname,int * results)
/* int	maxfound;	size of result array */
/* char *sname;	  search name */
/* int	*results;	search results in terms of plist_ids */
{
int i;
int found;

found = 0;
for (i=0;i<MAXCOMPONENTS;i++) {

	if (plist[i].plist_id !=-1) { /* valid entry so search it */
		if (strcmp(plist[i].fullname, sname)==0) { /* we have a match */
			results[found] = i;
			found++;
			if (maxfound==found) return (maxfound);
			}	
		}
	}
	return (found);
}

int	find_flists_by_functionname (int maxfound,char * fname,int * results)
/* int	maxfound;	size of result array */
/* char *fname;	  search name */
/* int	*results;	search results in terms of flist_ids */
{
int i;
int found;

found = 0;
for (i=0;i<MAXFUNCTIONS;i++) {

	if (flist[i].flist_id !=-1) { /* valid entry so search it */
		if (strcmp(flist[i].function, fname)==0) { /* we have a match */
			results[found] = i;
			found++;
			if (maxfound==found) return (maxfound);
			}	
		}
	}
	return (found);
}


int	find_flists_by_plistid (int maxfound,int sid,int * results)
/* int	maxfound;	 size of result array */
/* int sid;		     search id */
/* int	*results;	 search results in terms of flist_ids */
{
int i;
int found;

found = 0;
for (i=0;i<MAXFUNCTIONS;i++) {

	if (flist[i].flist_id !=-1) { /* valid entry so search it */
		if (flist[i].plist_id == sid) { /* we have a match */
			results[found] = i;
			found++;
			if (maxfound==found) return (maxfound);
			}	
		}
	}
	return (found);
}

int	display_plist_entry (int id,FILE * s)
{
if (plist[id].plist_id == -1) {
	fprintf(s,"plist:  [%d] record does not exist\n",
					id);
	fflush(s);
	return (-1);
	}

fprintf(s,"[%d] %s (%s:%s) from (%s)\n", 
	id, plist[id].fullname, plist[id].package, 
	plist[id].component, plist[id].library);
fflush(s);

return (0);

}

int	display_plist_full_entry (int id,FILE * s)
{
int i;

if (plist[id].plist_id == -1) {
	fprintf(s,"plist:  [%d] record does not exist\n",
					id);
	fflush(s);
	return (-1);
	}

fprintf(s, "[%d] %s (%s:%s) from (%s)\n", 
	id, plist[id].fullname, plist[id].package, 
	plist[id].component, plist[id].library);

fprintf(s, "Package fullname :\t %s\n", plist[id].fullname);
fprintf(s, "Package name     :\t %s\n", plist[id].package);
fprintf(s, "Package component:\t %s\n", plist[id].component);
fprintf(s, "Package library  :\t %s\n", plist[id].library);
if (plist[id].libhandle) fprintf(s, "Package library  :\t currently open\n");
else fprintf(s, "Package library  :\t not open\n");
if (plist[id].finfo) fprintf(s, "Package f() info :\t yes\n");
else fprintf(s, "Package f() info :\t none found\n");
fprintf(s, "Package f() count:\t %d\n", plist[id].nfunctions);

if (plist[id].nfunctions>0) {
	for (i=0;i<plist[id].nfunctions;i++)
	if (plist[id].functions[i][0]!='\0')
		fprintf(s, "Package f() name:\t [%d] %s\n", i, &(plist[id].functions[i][0]) );
	} /* nfunctions */


fprintf(s, "Package author   :\t %s\n", plist[id].author);
fprintf(s, "Package comments :\t %s\n", plist[id].comments);


fprintf(s, "Package published:\t %lu\n", plist[id].published);
fprintf(s, "Package loaded   :\t %lu \n", plist[id].loaded);
fprintf(s, "Package ttl      :\t %lu\n", plist[id].ttl);
fflush(s);

return (0);

}


int	display_flist_entry (int id,FILE * s)
{
if (flist[id].flist_id == -1) {
	fprintf(s,"flist:  [%d] record does not exist\n",
					id);
	fflush(s);
	return (-1);
	}

fprintf(s,"[%d] %s from package (%d)\n", 
	id, flist[id].function, flist[id].plist_id );
fflush(s);

return (0);

}

int	display_flist_full_entry (int id,FILE * s)
{
if (flist[id].flist_id == -1) {
	fprintf(s,"flist:  [%d] record does not exist\n",
					id);
	fflush(s);
	return (-1);
	}

fprintf(s,"[%d] %s from package (%d) call (%d)\n", 
	id, flist[id].function, flist[id].plist_id, flist[id].plist_findex );

fprintf(s,"Function name:\t %s\n", flist[id].function);
fprintf(s,"From package (%d)\n", flist[id].plist_id);
fprintf(s,"Function ID (%d)\n", flist[id].flist_id);

if (flist[id].singleinit) fprintf(s,"single init function.\n");
else fprintf(s,"no single init function.\n");
if (flist[id].owninit) fprintf(s,"has init function.\n");
else fprintf(s,"does not have an individual init functions.\n");
if (flist[id].ftype == FTYPE_DEFAULT) 
	fprintf(s,"Function is a direct function call\n"); 
if (flist[id].ftype == FTYPE_HINIT) 
	fprintf(s,"Function is a HARNESS init function call for a complete module/component\n"); 
if (flist[id].ftype == FTYPE_INIT) 
	fprintf(s,"Function is an init function call\n"); 
if (flist[id].ftype == FTYPE_START) 
	fprintf(s,"Function is a service start function call\n"); 
if (flist[id].ftype == FTYPE_CONT) 
	fprintf(s,"Function is a service continue function call\n"); 
if (flist[id].ftype == FTYPE_SUSPEND) 
	fprintf(s,"Function is a service suspend request function call\n"); 
if (flist[id].ftype == FTYPE_STOP) 
	fprintf(s,"Function is a service stop function call\n"); 
if (flist[id].ftype == FTYPE_SOCKET) 
	fprintf(s,"Function is a direct function call using socket arg passing\n");

switch (flist[id].rtype) {
	case FINIT_VOID :
		fprintf(s,"Functions return type is VOID\n"); break;
	case FINIT_INT :
		fprintf(s,"Functions return type is INTEGER\n"); break;
	case FINIT_FLOAT :
		fprintf(s,"Functions return type is FLOAT\n"); break;
	case FINIT_DOUBLE :
		fprintf(s,"Functions return type is DOUBLE\n"); break;
	case FINIT_SOCKET :
		fprintf(s,"Functions return type is SOCKET\n"); break;
	case FINIT_SVC :
		fprintf(s,"Functions return type is SERVICE\n"); break;
	default:
		fprintf(s,"Functions return type is UNKNOWN\n"); break;
	}	 /* switch */


fflush(s);

return (0);

}

/* bigger meaner display routines */

int	display_full_package (int id,FILE * s)
{
int i;
int nf;
int fid;

/* check id is valid first */
if (id<0) return (0);
if (plist[id].plist_id==-1) return (0);

display_plist_full_entry (id, s);

/* ok go through it and do each function as well if possible */

nf = plist[id].nfunctions;

for (i=0;i<nf;i++) {
	fid = plist[id].fids[i];
	if (fid!=-1) 
		if (flist[fid].flist_id!=-1) /* should need this check really */
			display_flist_full_entry (fid, s);
	} /* for each function */

return (1);	/* did it */
}

int	display_all_packages (FILE *s)
{
int id;
int i;

for(i=0;i<MAXCOMPONENTS;i++) 
	if (plist[i].plist_id != -1) {
		display_full_package (i, s);
		}

return (0);
}




int	plist_size ()
{
return (plistcnt);
}

int	flist_size ()
{
return (flistcnt);
}

int	plist_space ()
{
return (MAXCOMPONENTS - plistcnt);
}

int	flist_space ()
{
return (MAXFUNCTIONS - flistcnt);
}



/* ----- */

void test_plist (int id)
{
char t[256];
sprintf(t, "test %d", id);
printf("t = %s\n", t);
strcpy(plist[id].fullname,	t);
strcpy(plist[id].author,	t);
strcpy(plist[id].comments,	t);
strcpy(plist[id].package,	t);
strcpy(plist[id].component,	t);
strcpy(plist[id].functions[0],	t);
strcpy(plist[id].library,	t);

plist[id].singleinit = 0;

plist[id].published = 1000;
plist[id].loaded = 2000;
plist[id].ttl = 500;

plist[id].plist_id = id;


}

int test_flist (int id)
{
char t[256];
sprintf(t, "funct %d", id);
strcpy(flist[id].function,	t);
printf("t = %s\n", t);

flist[id].singleinit = 0;

flist[id].plist_id = id/2;
flist[id].flist_id = id;
return 0;
}

