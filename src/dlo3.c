
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/





#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

/* #include "dlo3.h"	 */
/* get protos */
#include "finit.h"	/* get return types */
#include "plist.h"	/* get plist/flist types and defines */


/* I need access to the plist/flist stuff directly.. bad I know */
extern plist_t plist[MAXCOMPONENTS];
extern flist_t flist[MAXFUNCTIONS];


int load_only (char *libname,char * fname,vfp * pfptr)
{
	void * h;
	int i;
	int ftype;	/* type of function, int, double, float etc etc */
	ifp init;
	char iname[100];	/* name of the init function */
	char fsname[100];	/* full symbol name of the function */

	sprintf (iname, "%s_init", fname);

	printf("module name %s funct name %s init funct name %s\n\n",
			libname, fname, iname);

	fflush(stdout);
	h = dlopen( libname, RTLD_LAZY );
	puts("done dlopen");
	fflush(stdout);

	if ( h == NULL )
		{
			printf( "Error Loading Module %s\n", libname );
			puts(dlerror());
			return (-1);
		}

		else
		{
			printf( "Module %s Successfully Loaded.\n", libname );

			/* Execute Initialization Function */

			if ( (init = (ifp) dlsym( h, iname))
					!= NULL )
			{
				printf( "Executing %s for Module %s:\n",
					iname, libname );
				ftype = -1;	/* err code default */
				ftype = (*init)();
				printf( "%s returned type %d: ", iname, ftype);
				if (ftype==FINIT_VOID) puts("void");
				if (ftype==FINIT_INT) puts("int");
				if (ftype==FINIT_FLOAT) puts("float");
				if (ftype==FINIT_DOUBLE) puts("double");
				if (ftype==FINIT_SOCKET) puts("socket serialized");
				if (ftype==FINIT_SVC) puts("init runs its own socket service");
			}
			else
			{
				printf( "No %s found for Module %s.\n",
					iname, libname);
				return (-2);
			}

			if (ftype==FINIT_SOCKET) 
				sprintf(fsname, "%s_s\0", fname);
			else
				strcpy (fsname, fname);


			/* Load the Function */
			printf( "Attempting to load %s for Module %s:\n", fsname, libname );

				*pfptr = (vfp) dlsym (h, fsname);
				printf("dlo: %s @ 0x%lx\n", fsname, *pfptr);
			return (0);
		}
		return (-9);
	}

/* loads a library and stores it in a plist */
/* returns the plist entry that it found */

int load_component (char *libname)
{
	void *h=NULL;	/* handle to a library */
	int id;

	int sinit=0;
	ifp sfp=NULL;

	int finfo;
	ifp ffp=NULL;

	int nf;	/* number of functions available */
	int n;

	char fn[MAXNAMELENGTH];


	printf("loading %s\n", libname);
	h = dlopen( libname, RTLD_LAZY );

	if ( h == NULL )
		{
			printf( "Error Loading Module %s\n", libname );
			puts(dlerror());
			return (-1);
		}

	/* now we have it loaded, we store its info in the plist structires */

	id = find_free_plist ();
	printf("Free list location %d\n", id);
printf("plist[%d] id is? %d\n", id, plist[id].plist_id);

	if (id==-1) 
		{
			fprintf(stderr,"Cannot find space in the inn for another component library.\n");
			fflush (stderr);
			return (-1);
		}

	/* ok we have a slot */	


	/* is there an Hinit function for the whole thing? */

	mk_sname (fn, "\0", FTYPE_HINIT);
	printf("looking for [%s]\n", fn);

			if ( (sfp = (ifp) dlsym( h, fn))
					!= NULL )
			{
			sinit = 1;
			printf("[%s] found.\n", fn);
			} else {
                           printf("Error: %s\n",dlerror());
                        }

	/* is there a finfo function for the whole thing? */

	mk_sname (fn, "\0", FTYPE_FINFO);
	printf("looking for [%s]\n", fn);

			if ( (ffp = (ifp) dlsym( h, fn))
					!= NULL )
			{
			finfo = 1;
			printf("[%s] found.\n", fn);

			n = (*ffp)(0, &nf, NULL, NULL, NULL, NULL );	/* get number of functions available only */
			printf("[%s] reports a total of %d functions in library component [%s]\n", fn, nf, libname);
			}


	/* ok update the record with all we know.... */	

	/* int edit_plist (id, fullname, author, comments, package, component, nf, fnames, library, libhdl, finfo, sinit, fp,
	published,
	loaded, ttl) */

	/* access the record directly */
	strcpy (plist[id].library, libname);
	plist[id].libhandle = h;
	plist[id].finfo = finfo;
	plist[id].singleinit = sinit;
	plist[id].singleinit_fp = sfp;
	plist[id].nfunctions = 0; /* as we don't store the name.. */


printf("plist[%d] id is? %d\n", id, plist[id].plist_id);

return (id);


}





int load_function ();       /* loads a function from an ALREADY loaded plist library */



int load_all_components (char *libname)
{
	void *h=NULL;	/* handle to a library */
	int id;
	int fid;

	int sinit=0;
	ifp sfp=NULL;

	int finfo;
	ifp ffp=NULL;

	int nf;	/* number of functions available */
	int n;
	char *fnames[100];
	char anames[100][256];
	int ftypes[100];
	int rvals[100];
	int  inits[100];

	void *fp;

	int i, j, k;


	char fn[MAXNAMELENGTH];


	printf("loading %s\n", libname);
	h = dlopen( libname, RTLD_LAZY );

	if ( h == NULL )
		{
			printf( "Error Loading Module %s\n", libname );
			puts(dlerror());
			return (-1);
		}

	/* now we have it loaded, we store its info in the plist structires */

	id = find_free_plist ();
	printf("Free list location %d\n", id);
printf("plist[%d] id is? %d\n", id, plist[id].plist_id);

	if (id==-1) 
		{
			fprintf(stderr,"Cannot find space in the inn for another component library.\n");
			fflush (stderr);
			return (-1);
		}

	/* ok we have a slot */	


	/* is there an Hinit function for the whole thing? */

	mk_sname (fn, "\0", FTYPE_HINIT);
	printf("looking for [%s]\n", fn);

			if ( (sfp = (ifp) dlsym( h, fn))
					!= NULL )
			{
			sinit = 1;
			printf("[%s] found.\n", fn);
			} else {
                           printf("Error: %s\n",dlerror());
			}

	/* is there a finfo function for the whole thing? */

	mk_sname (fn, "\0", FTYPE_FINFO);
	printf("looking for [%s]\n", fn);


			if ( (ffp = (ifp) dlsym( h, fn))	/* if finfo exists */
					!= NULL )
			{
			finfo = 1;
			printf("[%s] found.\n", fn);


			for (i=0;i<100;i++) fnames[i] = (char*) anames[i]; /* build return array for function names */

			n = (*ffp)(100, &nf, fnames, ftypes, rvals, inits ); /* get all info as much as possible */

			printf("[%s] reports a total of %d functions in library component [%s]\n", fn, nf, libname);

			/* store function name info even if it may not be loadable / available ? */
			for (j=0;j<n;j++)  strcpy (&(plist[id].functions[j][0]), (char*)fnames[j]); 
/* 			for (j=0;j<n;j++) printf("%d 0x%x\n", j,  */
/* 			(char *)(fnames[j]) ); */

/* 			for (j=0;j<n;j++) printf("%d 0x%x\n", j,  */
/* 			&(plist[id].functions[j][0]) ); */

			/* store the function name info */ 

			/* now for each function maybe load its symbol ? */

			for (j=0;j<n;j++) { /* load each function */

				/* now attempt to load it */
				mk_sname (fn, fnames[j], ftypes[j]);
				printf("Would attempt to load function by name [%s] of type [%d] and return type [%d]\n", 
					fn, ftypes[j], rvals[j]);

				fid = get_symbol_into_new_flist (h, fnames[j], ftypes[j], rvals[j]);

				if (fid!=-1) {
					printf("Fid = %d\n", fid);

					/* fill in the stuff that needs to be stored in flist */
					flist[fid].plist_id = id;
					flist[fid].plist_findex = j;

					/* now the plist update */
					plist[id].fids[j] = fid;

					display_flist_full_entry (fid, stdout);
				}
				else /* clear the plist entry so we don't call it */
					plist[id].fids[j] = -1;


			} /* for each function */

	} /* if finfo exists */


	/* ok update the record with all we know.... */	
	/* access the record directly */
	strcpy (plist[id].library, libname);
	plist[id].libhandle = h;
	plist[id].finfo = finfo;
	plist[id].singleinit = sinit;
	plist[id].singleinit_fp = sfp;
	plist[id].nfunctions = n; /* as we store the names so list the number.. */

		



return (id);


}

int	get_symbol (void *h,char * name,void ** fp)
{
int ok=0;
void *sym;

if (!h) return (0);	/* failed */
if (!name) return (0); /* failed */

if ( (sym = dlsym( h, name)) != NULL ) {
	printf ("symbol = 0x%x\n", sym);
	*fp = sym;
	return (1);	/* note they will have to cast the result */
	}
return (0);
}





int load_function ();       /* loads a function from an ALREADY loaded plist library */



int	get_symbol_into_new_flist (void *h,char * fname,int ftype,int rval)
{
int i;
char fn[MAXNAMELENGTH];
char fninit[MAXNAMELENGTH];
void *fp;
int fid;
int init;

fid = find_free_flist ();	/* get an flist entry slot */
if (fid==-1) return (-1);

/* first get the function name */
/* which is just the fname modified by its type */

mk_sname (fn, fname, ftype);

printf("function %s of type %d is now %d flist entry\n", fn, ftype, fid);

if  ( !get_symbol (h, fn, &fp) ) /* if we can NOT get this f() */
	return (-1);

printf("fp = 0x%x for %s\n", fp, fn);

switch (rval) {
	case FINIT_VOID :
		flist[fid].vfunc_ft = (vfp) fp; break;
	case FINIT_INT :
		flist[fid].ifunc_ft = (ifp) fp; break;
	case FINIT_FLOAT :
		flist[fid].ffunc_ft = (ffp) fp; break;
	case FINIT_DOUBLE :
		flist[fid].dfunc_ft = (dfp) fp; break;
	case FINIT_SOCKET :
		flist[fid].vfunc_ft = (vfp) fp; break;
	case FINIT_SVC :
		flist[fid].vfunc_ft = (vfp) fp; break;
	default :
		printf("Invalid return type for function [%s] of type %d?\n",
				fname, rval);
		zero_flist (fid);
		return (-1);
	} /* switch */

/* ok update {some of the} contents of the record */

strcpy ((char*)flist[fid].function, fn);
flist[fid].ftype = ftype;
flist[fid].rtype = rval;

/* now find the init if it has one */
init = 0; /* as far as we know */

mk_sname (fn, fname, FTYPE_INIT);

if  ( get_symbol (h, fn, &fp) ) { /* if we can get the init function */

	flist[fid].owninit = 1;	/* has own init() */
	flist[fid].init_fp = (ifp) fp;	/* remember init function */
									/* NOTE we haven't called it yet */
	}

else { /* there is no init or we cannot get the symbol from the library */

	flist[fid].owninit = 0;
	flist[fid].init_fp = NULL;	/* just to make sure its not used */

	}

/* Note we haven't updated plist indexes  ids etc */

return (fid);	/* if we got here, be happy */
}
