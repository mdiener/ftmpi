
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



/* dynamic loading stuff header file */
/* G Fagg Jan-March 2000 */

/* changed in Nov 2000 to include the library name correctly! */

#ifndef _GHCORE_H_DL2
#define _GHCORE_H_DL2 1

typedef void (*vfp)();
typedef int (*ifp)();
typedef float (*ffp)();
typedef double (*dfp)();

int load_run_unload ();
int load_only ();

#endif /*  _GHCORE_H_DL2 */
