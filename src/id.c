
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/

/* call names in binary format */

unsigned int pf_bin (unsigned p,unsigned f)
{
unsigned int r;

    p = p & (0xFFFF);
    p = p<<16;

    f = f & (0xFFFF);

    r = p | f;

    return (r);
}




void bin_pf (unsigned bin,unsigned *  p,unsigned * f)
{
unsigned int r, s;


    r = (bin>>16);
    r = r & (0xFFFF);

    s = bin & (0xFFFF);

    *p = r;
    *f = s;
}

