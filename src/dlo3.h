
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


#ifndef _GHCORE_H_DL3
#define _GHCORE_H_DL3 1

/* dynamic loading stuff header file */
/* G Fagg Jan-March 2000 */

/* changed in Nov 2000 to include the library name correctly! */

/* changed again from the dlo2 library so that it handles load library, load all functions etc etc */

typedef void (*vfp)();
typedef int (*ifp)();
typedef float (*ffp)();
typedef double (*dfp)();

/* this version is now no longer used */
/* int load_run_unload (); */

/* this still opens a dl, and runs init and passes back the function */
int load_only (char *libname,char * fname,vfp * pfptr);


/* -------- */
/* New functions */


int load_component (char *libname);		/* loads a library and stores it in a plist */
int load_all_components (char *libname);	/* loads a library and stores it in a plist and ALL its functions in flists */
int load_function ();		/* loads a function from an ALREADY loaded plist library */


#endif /* _GHCORE_H_DL3 */
