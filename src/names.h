
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



/* 
 Names and other true lies

 G. Fagg Sep-Nov 2000

 Harness 

 */

#ifndef _GHCORE_H_NAMES
#define _GHCORE_H_NAMES 1


typedef enum {
		FTYPE_DEFAULT,
		FTYPE_HINIT,
		FTYPE_INIT,
		FTYPE_START,
		FTYPE_CONT,
		FTYPE_SUSPEND,
		FTYPE_STOP,
		FTYPE_SOCKET,
		FTYPE_FINFO
		} ftype_t;

#define MAXFTYPES	9


/* part names */
void mk_fname (char *fname,char * pname,char * mname);
void mk_piname (char *piname,char * pname,char * mname,char * cname,int FTYPE);
void mk_sname (char *sname,char * cname,int FTYPE);

/* full path names */
void	mk_lib_name (char *libname,char * pname,char * mname);
void	mk_cache_name (char *cachename,char * pname,char * mname);
void	mk_url_name (void);

#endif /*  _GHCORE_H_NAMES */
