/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/




#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
int errno;

#ifdef IMA_SUN4SOL2
#include <sys/loadavg.h>
#endif /* solaris for getloadavg call */
#ifdef IMA_SGI64
    #include <sys/sysmp.h>
#endif /* SGI for getloadavg call */
#ifdef IMA_ALPHA
    #include <sys/table.h>
#endif /* ALPHA for getloadavg call */



#include "snipe_lite.h"		/* TCP coms */
#include "msg.h"			/* send/recv packed messages */
#include "msgbuf.h"			/* buffer management */

#ifdef BM
	#include "sec_time.h"		/* for benchmarking */
#endif

#include "internal_state.h"	/* for env information */


#include "plist.h"	/* for defs and package/compontent/function lists */

#include "g_hcore_d_int.h"	/* for internal data structures and types */

#include "ns_lib.h" /* for name service facilities */

#include "mkpaths.h"	/* for file path name creation */

#include "envs.h"		/* for environment vars */

#include "finit.h"		/* for return function types */

#include "http.h"		/* used for http downloading */

#include "hcore_proto.h"	/* used to define how we talk to the world */

#include "hlib.h"

#include "id.h"


void* child_func (void *id);
void child_svc  ();

void init_lsocket ();
void term_threads ();
int init_locks_threads ();
int wait_for_threads (int its);
int get_core_id (char *vmname);
int get_free ();
int my_addr ();

void call_function (int s,int * rtags);
void call_command (int s);
void call_load (int s,int mb);
void call_unload (int s);
void call_pinfo (int s,int mb);

void  call_func (int s,int  p,int  f);

void call_ping (int s, int flag);
void call_hcore_info (int s);

void  go_fork_call (int s,int p,int f);
void  go_thread_call (int s,int  p,int  f);


/* control data */
int	ctrl_free;
pthread_mutex_t	ctrl_lock;

/* other globals */
int	core_id;
int nsaccess;
int	exiting;
int our_socket;
int our_port;
int our_addr;
int debug;
char hname[256];	/* my hostname */
int npackages;
int ntotalfunctions;

/* globals borrowed from other libraries.... */
/* I need access to the plist/flist stuff directly.. bad I know */
extern plist_t plist[MAXCOMPONENTS];
extern flist_t flist[MAXFUNCTIONS];

/* char*   HARNESS_NS_HOST; */
/* int     HARNESS_NS_HOST_PORT; */
/* char*   HARNESS_RS_HOST; */
/* int     HARNESS_RS_HOST_PORT; */


int main (int argc,char ** argv) 
{

		double st, et;
		int i;

		int sa; /* sockets */
		int p;		/* TCP port address */

		vfp	funct;
		long	addr;
		char data[100];

		int reqmb;	/* request message buffer */
		int reqtags[HCORE_PROTO_MAXARGS];	/* tags received */
		int nreqtag;	/* number of request tags */
		int rc;		/* packed msg return codes */
		int from;	/* who made a request */

		int pid;
		char *tmp;

		npackages = 0;
		ntotalfunctions = 0;

		init_envs  ();	/* get environment variables */
		init_plist ();	/* package list */
		init_flist ();	/* function list */

		i = init_msg_bufs ();
		if (i>0) printf("init bufs ok with %d\n", i);
		else { puts ("init msg_bufs failed, halting\n"); exit (-1); }


		init_locks_threads ();	/* create all the threads and all required locks */

		get_envs (); /* the the environment vars before we do anything else */
		init_lsocket ();	/* get our listen up and running */

		if ((argc==2)||(argc==3)) { 
			/* contact NS and get a unique ID */
			core_id = get_core_id (argv[1]);
			set_local_vmname (argv[1]);			/* put vm name into env */
			if (argc==3) debug = atoi (argv[2]);
			}
		else {
#ifdef SINGLETONALLOWED
			fprintf(stderr,"G_HCORED_D: started with no VM name!.\n");
			fprintf(stderr,"G_HCORED_D singleton ID of 0 assumed.\n");
			core_id = 0;
			nsaccess = 0;	/* no name services */
#else
			fprintf(stderr,"G_HCORE_D: no named VM not allowed!\n");
			fprintf(stderr,"usage: %s <VMNAME> <debug_opt>\n", argv[0]);
			exit (-1);
#endif
			}

		if (core_id<0) { /* there was a problem starting the core? */
			fprintf(stderr,"G_HCORE_D: problem with naming. No name, no gain, no use. Exiting.\n(Check the name service is running correctly).\n");
			exit (core_id);
			}

		printf("Main loop running\n"); fflush(stdout);
		printf("G_HCORE_D: my id [%d] my host addr 0x%x my contact port %d\n",
				core_id, our_addr, our_port);



		/* check timers */
		st = sec_time();
		et = sec_time();

		exiting = 0;	/* let main loop run until told to exit */

	for (;;) {

		/* get request ds ready */
		nreqtag = HCORE_PROTO_MAXARGS;
		for(i=0;i<nreqtag;i++) reqtags[i]=-1;

		sa = allowconn (our_socket, 0, 0);	/* get a connection */

		rc = recv_pkmesg (sa, &from, &nreqtag, reqtags, &reqmb); /* the recv */

		/* reqtags[0] tells us the type of request */

		switch (reqtags[0]) {
			case HCORE_PROTO_PING :
				call_ping (sa, reqtags[1]);
				break;
			case HCORE_PROTO_CALLFNC :
				call_function (sa, reqtags);
				break;
			case HCORE_PROTO_CALLFNC_PROC :
				call_function (sa, reqtags);
				break;
			case HCORE_PROTO_CALLFNC_THREAD :
				call_function (sa, reqtags);
				break;
			case HCORE_PROTO_CALLFNC_DIRECT :
				call_function (sa, reqtags);
				break;
			case HCORE_PROTO_LOAD :
				call_load (sa, reqmb);
				break;
			case HCORE_PROTO_PINFO :
				call_pinfo (sa, reqmb);
				break;
			case HCORE_PROTO_UNLOAD :
				call_unload (sa);
				break;
			case HCORE_PROTO_CALLCMD :
				call_command (sa);
				break;
			default :
				fprintf(stderr,"Unknown message request header. Dropping\n");
				close (sa);
		} /* switch */

		free_msg_buf (reqmb);	/* clear if up if not already cleared */

	if (exiting) break;	/* check for exit command */
	
	} /* for ever loop */

	fprintf(stderr,"Waiting for threads to finish their work\n");
	wait_for_threads (0);	/* wait before killing off :) */

	fprintf(stderr,"Unthreading\n");
	term_threads ();

	exit(0);
}



/* the handler function as a thread */
void *  child_func (void * vid)
{
int i, j;
int me;
int *id;
int	ok;
thread_state_t copy;

id = (int *)vid;

me = *id;
ok = 1;

printf("thread handler [%d] started.\n", me); fflush(stdout);
/* printf ("copy %d\n", sizeof(copy)); */

/* let start thread know that I have started */

pthread_mutex_lock(&ctrl_lock);
/* printf("++"); fflush(stdout); */
ctrl_free++; 
pthread_mutex_unlock(&ctrl_lock);
/* printf("@@"); fflush(stdout); */

/* first check me start lock */
pthread_mutex_lock(&startlock[me]);	/* hope I block right here, right now */

printf("thread handler [%d] past start lock\n", me); fflush(stdout);

/* ok main loop ok */

while (ok) {
	/* I always start by trying to get my own go lock */
	/* and the waiting on my condition variable */

	pthread_mutex_lock(&thread_state[me].cvlock);	

	printf("thread handler [%d] locked cv .\n", me); fflush(stdout);
	pthread_cond_wait(&thread_state[me].cv, &thread_state[me].cvlock);	
	printf("thread handler [%d] past cv .\n", me); fflush(stdout);
	pthread_mutex_unlock(&thread_state[me].cvlock);	
	printf("thread handler [%d] unlocked cv .\n", me); fflush(stdout);

	/* ok have the next lock so there is an instruction for me somewhere */
	/* so lock my data structure to get it */
	pthread_mutex_lock(&thread_state[me].lock);

	/* now I can read my instructions and start it */
	copy = thread_state[me];

	/* so mark myself as working on it */
	thread_state[me].state = TS_RUNNING;

	/* update the global ccontrol data structures */
	pthread_mutex_lock(&ctrl_lock);
	ctrl_free--;
	pthread_mutex_unlock(&ctrl_lock);

	/* unlock my structure so the master can update it */
	pthread_mutex_unlock(&thread_state[me].lock);
	
	/* do the work */

	printf("Thread %d asked to do op %d sock [%d] package %d funct %d (0x%x)->%s.\n",
			me, copy.next, copy.socket, copy.plist, copy.flist, 
			pf_bin (copy.plist, copy.flist), 
			flist[copy.flist].function);


	call_func (copy.socket, copy.plist, copy.flist);

	printf("Thread %d has done %d.\n",	me, copy.next );


	fflush (stdout);

	/* ok done the work */

	/* get my state lock */
	pthread_mutex_lock(&thread_state[me].lock);

	thread_state[me].state = TS_IDLE;
	thread_state[me].next = 0;
	thread_state[me].plist = -1;
	thread_state[me].flist = -1;

	/* unlock my state */	
	pthread_mutex_unlock(&thread_state[me].lock);

	/* update the global ccontrol data structures */
	pthread_mutex_lock(&ctrl_lock);
	ctrl_free++;
	pthread_mutex_unlock(&ctrl_lock);


	}	/* while */

	return NULL;
}


int	init_locks_threads () {

int i;
int j;
int idarray[MAX_THREADS];
int allup;
pthread_attr_t attr;
pthread_mutexattr_t mattr;


printf ("idarray %d attr %d mattr %d\n", sizeof(idarray), sizeof(attr), sizeof (mattr));
pthread_attr_init(&attr);
pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

pthread_mutexattr_init(&mattr);
#ifndef IMA_LINUXALPHA
   pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_PRIVATE);
#endif

/* make ids */
for(i=0;i<MAX_THREADS;i++) idarray[i] = i;	/* don't ask, I'm being lazy here */

/* init the locks */

/* the control data block stuff */
pthread_mutex_init(&ctrl_lock, NULL);
ctrl_free = 0;

/* the lock for all the threads */
for(i=0;i<MAX_THREADS;i++) {
	pthread_mutex_init(&startlock[i], &mattr);
}

/* lock the startup locks for ALL the threads */
for(i=0;i<MAX_THREADS;i++) 
	pthread_mutex_lock(&startlock[i]);

/* now create and fill in the thread data structures */
for(i=0;i<MAX_THREADS;i++) {

	/* fill in the easy ones */
	thread_state[i].id	= i;
	thread_state[i].state = TS_IDLE;
	thread_state[i].socket = 0;
	thread_state[i].next = NEXTOP_NOTHING;
	thread_state[i].plist = -1;
	thread_state[i].flist = -1;
	
	strcpy(thread_state[i].fullname, "none");
	strcpy(thread_state[i].pname, "none");
	strcpy(thread_state[i].cname, "none");
	strcpy(thread_state[i].fname, "none");

	pthread_mutex_init(&thread_state[i].lock, &mattr);
	pthread_mutex_init(&thread_state[i].cvlock, &mattr);

	pthread_cond_init(&thread_state[i].cv, NULL);

	/* now we can start the threads, as they will block on the startlock */
	j = pthread_create (&(thread_state[i].ptid), &attr, child_func, &(idarray[i]));
	printf("Started thread [%d]\n", idarray[i]); fflush(stdout);

}

allup = 0;
while (!allup) {	/* wait for all threads to start so their IDs don't walk off... */
	printf("**"); fflush(stdout);
	sleep (1);
	if (get_free()==MAX_THREADS) allup = 1;
}


/* unlock the startup locks for ALL the threads */
for(i=0;i<MAX_THREADS;i++) 
	pthread_mutex_unlock(&startlock[i]);

return (0);

}


/* this routine waits for all the threads to be free */
/* NOTE this does not block forever */
int wait_for_threads (int its)
{
int	i;
int f;

if (its==0) its = 50000;	/* if lazy give it */

for(i=0;i<its;i++) {

	f = get_free ();

	if (f==MAX_THREADS) break; 
	else usleep (10000);
}

if (f==MAX_THREADS) return (0);
else return (-1);

}


int	get_free () {
int f;

pthread_mutex_lock(&ctrl_lock);
f = ctrl_free;
pthread_mutex_unlock(&ctrl_lock);

return (f);
}


int	find_idle_thread () {
int f;
int i;

if (get_free()==0) return (-1);	/* none free so don't search */

f = -1;
for (i=0;i<MAX_THREADS;i++) {
	pthread_mutex_lock(&thread_state[i].lock);	
		if (thread_state[i].state==TS_IDLE) 
			f = i;
	pthread_mutex_unlock(&thread_state[i].lock);	
	if (f!=-1) break;
	}

return (f);
}




int	get_core_id (char *vmname)
{
int r;
int i;
int i1, i2;
unsigned int rc;

if (!HARNESS_NS_HOST) {
	fprintf(stderr,"No HARNESS_NS_HOST set\n");
	return (-1);
}

if (!HARNESS_NS_HOST_PORT) {
	fprintf(stderr,"No HARNESS_NS_HOST PORT set\n");
	return (-1);
}

r = ns_init (HARNESS_NS_HOST, HARNESS_NS_HOST_PORT);

if (r) {
	fprintf(stderr,"Harness NS host not contactable\n");
	return (-1);
}	

nsaccess = 1;

our_addr = my_addr ();	/* gets my ip and MY hostname */

/* ok we have a NS now lets get an ID */

r = ns_add (vmname, our_addr, our_port, &i1, &i2);

ns_close (); /* finished with the NS for now. */

/* ok make the core_id to be the NS location value :) */
rc = pf_bin (i1, i2);
return (rc);

}


int	my_addr ()
{
struct hostent *hp;
#ifdef ulong_t	/* or maybe IMA_LINUX */
ulong_t addr;
#else
unsigned int addr;
#endif
char **p;
struct in_addr in;

	gethostname (hname, 255);	/* one less than size for NULL */
	hp = gethostbyname (hname);

/* 	printf("hp 0x%x\n", (void*)hp); */

/* 	for (p = hp->h_addr_list; *p != 0; p++) { */
/*               struct in_addr in; */
/*               char **q; */
/*               (void) memcpy(&in.s_addr, *p, sizeof (in.s_addr)); */
/* 			  printf("0x%x\n", in.s_addr); */
/*                  (void) printf("%s\t%s", inet_ntoa(in), hp->h_name); */
/*               for (q = hp->h_aliases; *q != 0; q++) */
/*                   (void) printf(" %s", *q); */
/*               (void) putchar('\n'); */
/*           } */

	p = hp->h_addr_list;
	(void) memcpy(&in.s_addr, *p, sizeof (in.s_addr));

return ((int) in.s_addr);
}

void	init_lsocket ()
{
int sl;
int p0;
int p;

		/* take the default port from envs or else take 1948 */
		if (HARNESS_GHCORED_PORT==0) p0 = 1948;	/* obvious really */
		else p0 = HARNESS_GHCORED_PORT;

		p = setportconn (&sl, p0, 10);	
		if (p>0) {
		printf("Server alive on port [%d] listening on socket [%d]\n", p, sl);
			our_port = p;
			our_socket = sl;
		}

		else {
			fprintf(stderr,"Cannot create a listen socket at port %d in rangle %d\n", p0, 10);
			exit (p);
		}
}

/* not a nice function */
void term_threads ()
{
int i;

for (i=0;i<MAX_THREADS;i++)
	if (thread_state[i].ptid > (pthread_t) 0) pthread_kill (thread_state[i].ptid, SIGKILL);
}




/* actual functions that do other things :) */


void call_function (int s,int * rtags)
/* int s;	 reply socket */
/* int *rtags;	 request tags */
{
int reply[2];
int binfid;	
int p, c, f;
int ctype;	/* call type */


/* get the request info from the request message tags */

ctype = rtags[0];	/* call type */
binfid = rtags[1];	/* binary combo package:function ID */

bin_pf (binfid, (unsigned int *)&p, (unsigned int *)&f);

printf("call_function: p %d f %d bin 0x%x process type %c\n", 
						p, f, binfid, ctype);

if (ctype==HCORE_PROTO_CALLFNC_DIRECT) {
	fprintf(stderr,"Overriding the direct call method\nSorry calling via a thread\n");
	ctype=HCORE_PROTO_CALLFNC_THREAD;
	}

/* check p and f values */

if ( ((p<0) || (p>MAXCOMPONENTS)) || ((f<0) || (f>MAXFUNCTIONSPERPACKAGE)) ) {
	fprintf(stderr,"Badly formed message.\n");
	fprintf(stderr,"Package [%d] and function [%d] ID out of range.\n", p, f);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	}

/* check actual package and function values */
if (plist[p].plist_id==-1) {
	fprintf(stderr,"Badly formed message.\n");
	fprintf(stderr,"Package [%d] ID is invalid.\n", p);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	}

if (flist[f].flist_id==-1) {
	fprintf(stderr,"Badly formed message.\n");
	fprintf(stderr,"Function [%d] ID is invalid.\n", f);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	}


if (ctype==HCORE_PROTO_CALLFNC_PROC) go_fork_call (s, p, f);
if (ctype==HCORE_PROTO_CALLFNC_THREAD) go_thread_call (s, p, f);

	return;

}

void call_command (int s)
{
/* int i0, i1; */
int reply[2];

	fprintf(stderr,"Command unsupported currently\n");
/* 	i0 = -1; */
/* 	i1 = -1; */
/* 	writeconn (s, &i0, sizeof(int)); */
/* 	writeconn (s, &i1, sizeof(int)); */

	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
}

void call_load (int s,int mb)
{
int i, j;
int t;
int reply[2];
int tmp0, tmp1;
char pname[MAXNAMELENGTH];
char cname[MAXNAMELENGTH];
char fname[MAXNAMELENGTH];
char libpath[4096];
char cachepath[4096];
char fullnamelib[4096];
char fullnamecache[4096];
char *where;
char urn[4096];

int cc;
int success;
int r;
int ri;
int nf;



/* get the args first */
/* readconn (s, &t, sizeof(int)); */

t = 0;
cc = upk_int32 (mb, &t, 1);

if ((t<2)||(t>3)||(cc<=0)) {
	fprintf(stderr,"call_load: badly formed message [%d]\n", t);
	closeconn(s);
	return ;
	}

printf("type %d\n", t); fflush(stdout);

/* readconn (s, &i0, sizeof(int)); */
/* readconn (s, pname, i0); */
/* pname[i0] = '\0'; */
cc = upk_string (mb, pname, MAXNAMELENGTH);

if (cc<=0) {
	fprintf(stderr,"call_load: badly formed message [pname]\n");
	closeconn(s);
	return ;
	}

/* readconn (s, &i1, sizeof(int)); */
/* readconn (s, cname, i1); */
/* cname[i1] = '\0'; */
cc = upk_string (mb, cname, MAXNAMELENGTH);

if (cc<=0) {
	fprintf(stderr,"call_load: badly formed message [cname]\n");
	closeconn(s);
	return ;
	}

printf("pname [%s] cname [%s]\n", pname, cname); fflush(stdout);

if (t==3) {	/* if 3rd argument */
/* 	readconn (s, &i2, sizeof(int)); */
/* 	readconn (s, fname, i2); */
	cc = upk_string (mb, fname, MAXNAMELENGTH);
	if (cc<=0) {
		fprintf(stderr,"call_load: badly formed message [fname]\n");
		closeconn(s);
		return ;
		}
	}


/* make the other possible names we will serach for */

mk_lib_name (fullnamelib, pname, cname);
mk_cache_name (fullnamecache, pname, cname);

/* ok have the name so do the loading */
success = 0;
where = NULL;

/* first the local lib directory */

if (!success) {
    printf("Checking for [%s]\n", fullnamelib);
    cc = check_path (fullnamelib);
    if (cc==0) { /* found it */
        printf("Found library file.\n");
        success = 1;
        where = fullnamelib;
        }
}

/* now the cache lib directory */

if (!success) {
    printf("Checking for [%s]\n", fullnamecache);
    cc = check_path (fullnamecache);
    if (cc==0) { /* found it */
        printf("Found library file.\n");
        success = 1;
        where = fullnamecache;
        }
}

/* http stuff here */
#ifdef DONOTENABLE	/* until the redirection service is up */
    if (!success) {
        int liblen=0;
		URL req;
		FILE *fptr;
		char hdr[]="\n";
		char *libdata;
		char httphost[]="www.cs.utk.edu";
		char httppath[1024];



        sprintf(httppath, "/~fagg/HARNESS/%s/%s/%s\0", getenv(envname_arch), pname, cname);
		req.host = httphost;
        req.port = 80;
        req.path = httppath;
        printf("host %s port %d path %s hrd %s\n",
                    req.host, req.port, req.path, hdr);

        printf("Checking via HTTP for [%s]\n", urn);
        cc = try_loading_http (-1, &req, hdr, &libdata);

        if (cc<=0) { /* not downloadable */
            printf("Cannot download...\n");
			/* nothing we can do so bop out, that was our last hope */
			success = 0;
			reply[0] = -1;
			reply[1] = -1;
			send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
			closeconn (s);
			return;
            }

        /* ok we downloaded it */
        liblen = cc;
        printf("Downloaded library of size %d\n", liblen);

        /* ok put it in the cache */
        /* first make the cache directory if needed */
        cc = mk_path (fullnamecache,0); /* 0 = last part is a file name */

        if (cc<0) { /* cannot create cache directory */
            printf("Cannot create cache directory\n");
			success = 0;
			reply[0] = -1;
			reply[1] = -1;
			send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
			closeconn (s);
			return;
            }

        /* ok now we can copy the file to disk (maybe) */
        fptr=fopen (fullnamecache, "w");
        fwrite (libdata, liblen, 1, fptr);
        fclose (fptr);

        /* the crunch if whether we can access it */
        printf("Checking for [%s]\n", fullnamecache);
        cc = check_path (fullnamecache);
        if (cc==0) { /* found it */
            printf("Found library file.\n");
            success = 1;
            where = fullnamecache;
            }

        }   /* if via HTTP */

#endif /* do not enable (yet) */
/* http stuff here */


if (!success) {
	fprintf(stderr,"Cannot get access to a copy of the library\n");
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	}

/* ok so we can now load the library? */

if (t==2) {
	r = load_all_components (where);
	if (r<0) { /* we failed */
		fprintf(stderr,"Cannot load the library %s\n", where);
		reply[0] = -1;
		reply[1] = -1;
		send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
		closeconn (s);
		return;
		}

	/* it worked */

	/* check for single init function */

	if ((plist[r].singleinit)&&(!plist[r].initcalled)) 
		if (plist[r].singleinit_fp) { /* ok there is a __init () */
			fprintf(stderr,"Calling single init on loaded library\n");

			ri = (*plist[r].singleinit_fp)();
			plist[r].initcalled = 1;

			fprintf(stderr,"_hinit returned [%d]\n", ri);
			fflush(stderr);

			}

	/* update the plist stuff as much as possible */
	strcpy (plist[r].package, pname);
	strcpy (plist[r].component, cname);

	/* verbose stuff */

	display_full_package (r, stdout);

	/* should update published, loaded and ttl here */


	/* as we have now loaded a package */
	/* update the counters of how many loaded etc */

	npackages++;
	ntotalfunctions+=plist[r].nfunctions;



	/* update external services */

	/* now for each function store the details in the name service */

	nf = plist[r].nfunctions;


	for (i=0;i<nf;i++) {
		char realname[1024];

		if (plist[r].fids[i]>=0)  {
			mk_piname (realname, pname, cname, &(plist[r].functions[i][0]),
				flist[plist[r].fids[i]].ftype);
	ns_open ();	/* make sure its open buster */
			ns_add (realname, core_id, (int) pf_bin (r, i), &tmp0, &tmp1);
	ns_close (); /* after we have finished with the name service */
			} /* if we can attempt to update the data base */


	}




	reply[0] = r;
	reply[1] = plist[r].nfunctions;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);

	return;
	}

else { /* explicit load of a single function.... grrr */
	puts ("loading a single function");
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;

	}


}

void call_unload (int s)
{
int reply[2];

	fprintf(stderr,"Cannot do unload library yet\n");
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
}

void call_pinfo (int s,int mb)
{
int i, j;
int reply[2];
int t;
char fname[MAXNAMELENGTH];
int cc;
int success;
int r;
int ri;
int nf;
int p, fid;
unsigned int id;



/* get the args first */
/* readconn (s, &p, sizeof(int)); */
p = -1;
upk_int32 (mb, &p, 1);

if ((p<0)||(p>MAXCOMPONENTS)) {
	fprintf(stderr,"Cannot get info on package [%d]\n",p);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	}

printf("Package info on %d\n", p); fflush(stdout);


/* readconn (s, &i0, sizeof(int)); */
/* readconn (s, fname, i0); */
/* fname[i0] = '\0';	 */
upk_string (mb, fname, MAXNAMELENGTH);

if (plist[p].plist_id == -1) {
	fprintf(stderr,"Cannot get info on unknown package %d \n", p);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	}

/* else its a valid package */
/* go find the function */

fid = -1;
nf = plist[p].nfunctions;

for (i=0;i<nf;i++) {
	if (plist[p].fids[i]>=0) {
		if (strcmp (&(plist[p].functions[i][0]), fname) == 0) {
			/* fid = i; */
			fid = plist[p].fids[i];
			break;
			}
         }
}
			

if (fid == -1) {
	fprintf(stderr,"Cannot get info on function %s in package %d \n", 
					fname, p);
	reply[0] = -1;
	reply[1] = -1;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	}

/* else found it, so use it */

	id = pf_bin (p, fid);

	fprintf(stderr,"Found function %s in package %d with ID of 0x%x\n",
					fname, p, id);
/* 	i0 = id; */
/* 	i1 = flist[fid].ftype; */
/* 	writeconn (s, &i0, sizeof(int)); */
/* 	writeconn (s, &i1, sizeof(int)); */
	reply[0] = id;
	reply[1] = flist[fid].ftype;
	send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
	closeconn (s);
	return;
	
}



void	go_fork_call (int s,int p,int f)
{
int r, rc;
int i, j;
int reply[2];
int pid;


	printf("Forking\n");

	/* first we have have to wait for all threads to be free */
	i = wait_for_threads (0);

	if (i==-1)	{	/* horror, timeout ! */
		fprintf(stderr,"Timed out waiting for threads to finish!\n");
		reply[0] = -2;
		reply[1] = -1;
		send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
		closeconn (s);
		return;
		}	

	printf("Forking: all threads are idle now.\n");

	pid = fork ();

	if (pid) { /* me the original master */
		/* I just return and hope :) */
		printf("Forked and returning back to main loop\n");
		return;
		}

	else {	/* I am the slave and we have lots to do.... */
			/* and its going to get real messy */

		printf("Forked process: removing all threads.\n");

		term_threads ();	/* first order, murder most foul */

		/* now call the function */

		call_func (s, p, f);

		/* now exit I hope.... */
		exit (0);

		}
}

void	go_thread_call (int s,int  p,int  f)
{
int i, j, r, rc;
int fid;
int i0, i1;
int reply[2];


puts ("go_thread_call () entered"); fflush(stdout);
/* get a idle thread */
fid = find_idle_thread ();
printf ("go_thread_call () found thread %d\n", fid); fflush(stdout);

if (fid<0) { /* none idle, wait for one */

	for(i=0;i<5000;i++) {
		f = get_free ();
		if (f) break; /* i.e. one is free somewhere */
		else usleep (10000);
		}
	fid = find_idle_thread ();	/* hunt it down again */

	}	/* the search */

/* if no thread idle still.. time out :( */

if (fid<0) { /* none idle return the bad news */
		fprintf(stderr,"Timed out waiting for any thread to be idle!\n");
/* 		i0 = -2; */
/* 		i1 = -1; */
/* 		writeconn (s, &i0, sizeof(int)); */
/* 		writeconn (s, &i1, sizeof(int)); */
		reply[0] = -2;
		reply[1] = -1;
		send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
		closeconn (s);
		return;
		}	

/* ok we have a free thread so set it up and kick it in butt as jim would say */

/* first the setting up */
puts ("go_thread_call () setting thread up"); fflush(stdout);
pthread_mutex_lock(&thread_state[fid].lock);	

	thread_state[fid].socket = s;
	thread_state[fid].plist  = p;
	thread_state[fid].flist  = f;
	thread_state[fid].next   = 1; /* ? ok... */

pthread_mutex_unlock(&thread_state[fid].lock);	
puts ("go_thread_call () set it up"); fflush(stdout);


/* now for the fun bit, kicking it (off) */

/* we activate via a CV, so get lock and then signal, unlock */
/* thread updates its own status etc etc */

pthread_mutex_lock(&thread_state[fid].cvlock);		/* lock */
puts ("go_thread_call () cv lock "); fflush(stdout);
pthread_cond_signal (&thread_state[fid].cv);		/* signal */
puts ("go_thread_call () signalled"); fflush(stdout);
pthread_mutex_unlock(&thread_state[fid].cvlock);	/* unlock */
puts ("go_thread_call () unlocked"); fflush(stdout);


}


/* the real deal */

void	call_func (int s,int  p,int  f)
{
int i, j, r, rc;
int ri;
double rd;
float rf;
int rval;
void *fp;
int reply[2];


/* check for single init function for the package  */

if (p>0) /* package 0 is a reserved package number for lost sheep */

	if ((plist[p].singleinit)&&(!plist[p].initcalled)) 
		if (plist[p].singleinit_fp) { /* ok there is a _hinit () */
			fprintf(stderr,"Calling single init on loaded library\n");

			ri = (*plist[p].singleinit_fp)();
			plist[p].initcalled = 1;

			fprintf(stderr,"_hinit returned [%d]\n", ri);
			fflush(stderr);

			}


/* ok first check the state of any init functions for this function */

	if ((flist[f].owninit)&&(!flist[f].initcalled))	 
		if (flist[f].init_fp) { /* ok there is a _hinit () for THIS function  */

			fprintf(stderr,"Calling init for individual function\n");

			ri = (*flist[f].init_fp)();
			flist[f].initcalled = 1;

			fprintf(stderr,"f()_hinit returned [%d]\n", ri);
			fflush(stderr);

			}
		
/* ok, we are now free to call the actual function..... grr.... */


	if (flist[f].ftype!=FTYPE_SOCKET) {	/* await the arg passing from ORNL */
		fprintf(stderr,"No socket type call, dropped\n");
/* 		i0 = -3; */
/*         i1 = -1; */
/*         writeconn (s, &i0, sizeof(int)); */
/*         writeconn (s, &i1, sizeof(int)); */
		reply[0] = -3;
		reply[1] = -1;
		send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
        closeconn (s);
        return;
		} /* to be changed next */

	/* else call it */

	switch (flist[f].rtype) {
   	 case FINIT_VOID :
  	      (*flist[f].vfunc_ft) (s); 
			break;
  	  case FINIT_INT :
  	      (*flist[f].ifunc_ft) (s); 
			break;
  	  case FINIT_FLOAT :
  	      (*flist[f].ffunc_ft) (s); 
			break;
  	  case FINIT_DOUBLE :
  	      (*flist[f].dfunc_ft) (s); 
			break;
  	  case FINIT_SOCKET :
  	      (*flist[f].vfunc_ft) (s); 
			break;
  	  case FINIT_SVC :
  	      (*flist[f].vfunc_ft) (s); 
			break;
  	  default :
  	      printf("Invalid return type for function [%s] of type %d?\n",
  	              flist[f].function, flist[f].rtype);
/* 		i0 = -3; */
/*         i1 = -1; */
/*         writeconn (s, &i0, sizeof(int)); */
/*         writeconn (s, &i1, sizeof(int)); */
		reply[0] = -3;
		reply[1] = -1;
		send_pkmesg (s, core_id, 2, reply, EMPTYMSGBUF, 0);
        closeconn (s);
        return;
    } /* switch */

	


}




int hcore_getloadavg(double avg[],int nelem)
{

#if defined(IMA_LINUX) || defined(IMA_SUN4SOL2)
    return getloadavg(avg,nelem);
#endif

#ifdef IMA_ALPHA
    struct tbl_loadavg load_ave;
    int elem;
    table (TBL_LOADAVG, 0, &load_ave, 1, sizeof (load_ave));
    for (elem = 0; elem < nelem; elem++) {
	  if(load_ave.tl_lscale == 0) {
	     avg[elem] = load_ave.tl_avenrun.d[elem] ;
	  } else { 
             avg[elem] = (load_ave.tl_avenrun.l[elem] / (double) load_ave.tl_lscale);
	  }
    }
    return nelem;
#endif

#ifdef IMA_SGI64
    int ldav_off, fd;
    long offset;
    double loadavg[3];
    offset = sysmp (MP_KERNADDR, MPKA_AVENRUN) & 0x7fffffff;

    if((fd = open ("/dev/kmem", 0)) >=0 ) {
        lseek (fd, offset, 0);
        read (fd, (char *) loadavg, sizeof (loadavg));
        close (fd);
        avg[0]=loadavg[0];
        avg[1]=loadavg[1];
        avg[2]=loadavg[2];
        return nelem;
   }
#endif

    avg[0]=0.0;
    avg[1]=0.0;
    avg[2]=0.0;
    return 0;
}

/* return a ping request */
/* if COREINFO_PING_ONLY */
/* we just pack our ID, send and then close the connect */
/* if COREINFO_PING_GET_STATS send back stats */
/* if COREINFO_PING_GET_NAMES send back names */
/* note, flag can be a mix of these via the logical OR */

void call_ping (int s, int flag)
/* int s;	socket */
{
int rc;
int mb;

/* tmp */
double loads[3];
int nthreads;
int nsvcs;
int status;
/* tmp */

status = 0; /* we are running ? */

if (flag==COREINFO_PING_ONLY) {
/* puts("COREINFO_PING_ONLY"); */
	rc = send_pkmesg (s, core_id, 1, &status, EMPTYMSGBUF, 0);
	closeconn (s);
	}

/* else its a more complex operation involving a message buffer etc */
mb = get_msg_buf (0);

if (flag&COREINFO_PING_GET_STATS) {
/* puts("COREINFO_PING_GET_STATS"); */
	/* as I don't know these yet */
	nthreads=0; nsvcs=0;

	rc = hcore_getloadavg (loads,3);

	if (rc<0) {
		loads[0]=0.0; loads[1]=0.0; loads[2]=0.0;
	}

	pk_int32 (mb, &npackages, 1);
	pk_int32 (mb, &nthreads, 1);
	pk_int32 (mb, &nsvcs, 1);
	pk_real64 (mb, loads, 3);
}
if (flag&COREINFO_PING_GET_NAMES) {
/* puts("COREINFO_PING_GET_NAMES"); */
	pk_string (mb, hname);
}

rc = send_pkmesg (s, core_id, 1, &status, mb, 1);
/* printf("ping sent %d bytes\n", rc); */
closeconn (s);
}

/* return a ping request */
/* we just pack our ID, send and then close the connect */

void call_hcore_info (int s)
/* int s;	socket */
{
int rc;

rc = send_pkmesg (s, core_id, 0, NULL, EMPTYMSGBUF, 0);
closeconn (s);
}


/* the handler function as a service (i.e. separate process)  */
/* currently needs to be updated / filled in */
void	child_svc (int * id)
{
}
