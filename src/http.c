
/*
	HARNESS G_HCORE
	HARNESS FT_MPI


	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	
			Graham E Fagg <fagg@hlrs.de>
			Keith Moore <moore@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/





#include <errno.h>       
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>

#include <ctype.h>
#include <sys/uio.h>

#include <strings.h>
#include <string.h>
#include <stdlib.h>

#include "http.h"

char USER_AGENT[]="HARNESS G_HCORE";

/*
 * connect_http()
 *
 * Establish a connection to a certain
 * host on a certain port.
 */

int connect_http(char * hostname,int to_port)
{
    struct sockaddr_in sa;
    struct hostent     *hp;
    int a, s;

    /*
     * XXX: Why the %#@! are we doing a DNS lookup here?!?!
     *      the rc server is caching this stuff now!!!
     */
    
    if ((hp= gethostbyname(hostname)) == NULL) {
	errno= ECONNREFUSED;
	return(-1);
    }

printf("hostname %s port %d\n", hostname, to_port);

    bzero(&sa,sizeof(sa));
    bcopy(hp->h_addr,(char *)&sa.sin_addr,hp->h_length);
    sa.sin_family= hp->h_addrtype;
    sa.sin_port= htons((u_short)to_port);
    
    if ((s= socket(hp->h_addrtype,SOCK_STREAM,0)) < 0)
	return(-2);
    if (connect(s,(struct sockaddr *) &sa,sizeof sa) < 0) {
	close(s);
	return(-3);
    }
    return(s);
}

/*
 * try_loading_http()
 */

int try_loading_http(int s,URL * url,char* headers,char** result)
/* int s;           socket to browser */
/* URL *url; */
/* char *headers;   headers from browser request */
/* char **result;   returned data */
{
    int to_s;	/* remote http server connection */
    int cc,code;
    char head[4096],*ptr;
	int i, r;
	int data_length;
	char *data;	/* actual result data */
	int hstart;	/* actual offset of data in header if any */
	int offset;	/* how far into data are we */
	int toget;	/* data left to download */
   

   printf("try\n"); fflush(stdout);
    to_s = connect_http(url->host, url->port);

	printf("socket %d\n", to_s);
    
    /* send request */
    
    if (to_s<0) {
	return to_s;
    } else {
	char *newheader=(char*)malloc(strlen(headers)+128);
	char *ptr=headers;
	struct iovec iov[4];

	/*
	 * replace the browser's User-Agent field with our own
	 * maybe this will make people notice us...
	 */
	while (1) {
	    if ((ptr=(char*)strchr(ptr,'\n'))==NULL)
		break;
	    ptr++;
	}

	if (ptr==NULL) {
	    sprintf(newheader,"User-Agent: %s\r\n",USER_AGENT);
	    strcat(newheader,"Accept-Ranges: bytes\r\n");
	    strcat(newheader,headers);
	} 
/* 	else { */
/* 	    bcopy(headers,newheader,(int)(ptr-headers)); */
/* 	    sprintf(newheader+(int)(ptr-headers),"User-Agent: %s\r\n", */
/* 		    USER_AGENT); */
/* 	    if ((ptr=(char*)strchr(ptr,'\n'))!=NULL) { */
/* 		ptr++; */
/* 		strcat(newheader,ptr); */
/* 	    } */
/* 	} */

/* puts(newheader); */
printf("hdr [%s]\n", newheader);
	
	iov[0].iov_base = "GET ";
	iov[1].iov_base = url->path;
	iov[2].iov_base = " HTTP/1.0\r\n";
	iov[3].iov_base = newheader;
	iov[0].iov_len = strlen(iov[0].iov_base);
	iov[1].iov_len = strlen(iov[1].iov_base);
	iov[2].iov_len = strlen(iov[2].iov_base);
	iov[3].iov_len = strlen(iov[3].iov_base);

puts (iov[0].iov_base);
puts (iov[1].iov_base);
puts (iov[2].iov_base);
puts (iov[3].iov_base);
	
	writev(to_s, iov, 4);

	printf("wrote data\n");

	free(newheader);
	
	/*
	 * check the header to see if an error occured
	 * if one is found, close the connection so that we can try the
	 * next url in our list
	 */
	cc=read(to_s,head,sizeof(head));
	printf("read %d [[%s]]\n", cc, head);

	ptr=(char*)strchr(head,' ')+1;
	code=atoi(ptr);
	if (code>=400) {
			printf("returned code was %d\n", code);
	    close(to_s);
	    return code;
	}

	printf("returned code was %d\n", code); fflush(stdout);

	/* extract header info if possible */
	ptr = (char*)head;
	data_length = 0;
	for (i=0;i<cc-14;i++) {
		if (i==10) { printf("Looking at %s\n", ptr+i); fflush(stdout); }
		r = strncasecmp((ptr+i), "Content-Length", 14);
		if (r==0) {
			printf("Found it @%d [%s]\n", i, (ptr+i));
			ptr=(char*)strchr(ptr+i,' ')+1;
			data_length=atoi(ptr);
			printf("returned code was %d\n", data_length); fflush(stdout);
			break;
		}
	}
	
	/* if there is no content length return a 0! */

	if (!data_length) {
		*result = NULL;
		return (0);
	}

	/* OK.. we have some data to get. */
	/* some of it is already in the head (maybe) */
	/* we need to collect it all into the data buffer called result */
	/* first allocate memory for it. (and hope that they free it... ) */

	data = (char*) malloc (data_length);

	if (data==NULL) {
		*result = NULL;
		return (-1);		/* other error */
	}

	printf("[%d] bytes allocated\n", data_length);

	/* else we now have the memory */
	/* so now we need to find the start of the actual data within the 'head' header */


	/* hopefully the 'ptr' is pointing to the start of the content-length string */
	/* i.e. just search for an empty line... */

	/* in this case its a \n followed by \r \n ! */

	printf("Offset %d to end of search %d length to search %d\n",
			(ptr-head), cc-3, ((cc-3) - (ptr-head)) );

	hstart = -1;
	for (i=(ptr-head);i<(cc-3);i++) {

	printf("[%c] " , *(head+i));

		if ( (*(head+i)=='\n')&&(*(head+i+1)=='\r')&&(*(head+i+2)=='\n') ) {
			printf("[%s]\n", head+i+3); 
			hstart = i+3;	/* i.e. where the actual data starts */
			break;
		}
	}

	/* if hstart is set then we have some actual result data in the header we have already read */
	/* so we need to copy this to the data buffer and decrement the amount left to download */

	if (hstart>0) {
		memcpy (data, (char*) (head+hstart), cc - hstart);

		/* toget = total - how much header - how much of header is not data */
		toget = data_length - (cc - hstart);
		offset = cc - hstart;
	}
	else {
		toget = data_length;	/* ops.. maybe the header isn't all read ??? ek */
		return (-2);			/* exit out as we need to deal with this correctly */
	}

	printf("hstart %d left %d\n", hstart, toget);

	/* loop round until we are finished */
/* #ifdef HA */
	while (toget) {
	    char buf[8192];
		int	 toget2;

		if (toget>8192) toget2 = 8192;	else toget2=toget;

		printf("toget [%d] toget2 [%d]\n", toget, toget2);

	    cc = read(to_s, data+offset, toget2);

	    if (cc>0) {
			printf ("Got %d\n", cc);
			toget -= cc;
			offset += cc;
			}
		else {
			printf("?? rc %d but we need %d \n", cc, toget2);
			}
/* 		sleep (1); */
	} 
/* #endif */
	/* ok we got it ? */
	if (offset==data_length) {
		printf("got all the data\n");
		*result=data;
		return (offset);
	}
	else {
		printf("didn't get all the data ?\noffset %d length %d\n", offset, data_length);

	}

	close(to_s);
	
	return 0;
    }
}

