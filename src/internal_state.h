
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/


/*
	contains internal state variables 

	OK.. globals that are really really usefull like env vars etc 


	G Fagg Nov 2000
*/

#ifndef _GHCORE_H_INTSTATE
#define _GHCORE_H_INTSTATE 1

#include <stdio.h>


/*	HARNESS dynamic stuff */

extern	char*	HARNESS_ROOT;
extern	char*	HARNESS_LIB_DIR;
extern	char*	HARNESS_CACHE_DIR;
extern	char*	HARNESS_ARCH;
extern	int	HARNESS_GHCORED_PORT;

/* HARNESS LOGGING AND IO */


extern	FILE*	HARNESS_LOG_FPTR;

/* HARNESS REPOSITORIES */

/* HARNESS NAME SERVICES */

extern	char*	HARNESS_NS_HOST;
extern	int		HARNESS_NS_HOST_PORT;


/* everything else is in hcore_sys.h */

#endif /*  _GHCORE_H_INTSTATE */
