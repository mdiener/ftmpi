
/*
	HARNESS G_HCORE

	Innovative Computer Laboratory,
	University of Tennessee,
	Knoxville, TN, USA.

	harness@cs.utk.edu

 --------------------------------------------------------------------------

 Authors:	Graham Fagg <fagg@cs.utk.edu>

 --------------------------------------------------------------------------

                              NOTICE

 Permission to use, copy, modify, and distribute this software and
 its documentation for any purpose and without fee is hereby granted
 provided that the above copyright notice appear in all copies and
 that both the copyright notice and this permission notice appear in
 supporting documentation.

 Neither the University of Tennessee nor the Authors make any
 representations about the suitability of this software for any
 purpose.  This software is provided ``as is'' without express or
 implied warranty.

 HARNESS, HARNESS G_HCORE and  FT_MPI was funded in part by the 
 U.S. Department of Energy.

*/



/* 

This is the second major version of this code.

This version requires that the VM name is used while loading a package and that
a different number of options are used when invoking functions.

Loading packages can also return a 'key'. This can be used when to 
remove sensitive functions. The key is transferable. 

*/


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#ifdef WIN32
#include"wincomm.h"
#else
#include"snipe_lite.h"
#endif

#include "names.h"
#include "envs.h"
#include "id.h"

#include "msg.h"
#include "msgbuf.h"

#include "hcore_proto.h"


#include "ns_lib.h"

/* harness lib */
#include "hlib.h"
#include "envs.h"

/* globals */
int init_called=0;
/* end globals */

/* lib */

/* ---------------------------------------------------------------------- */

/* this libraries init function */
/* it gets things like the envs and checks the name service is still there */
/* returns -1 if there is a problem */

int hlib_init () 
{
int rc;

if (init_called) return (0);	/* already called */

/* get the envs */
init_envs ();
get_envs_ns ();

/* Now check for a name service */
/* if there is no name service.. well panic? */

rc = ns_init (HARNESS_NS_HOST, HARNESS_NS_HOST_PORT);

if (rc<0) {
	fprintf(stderr,"hlib_init: problem contacting name service.\n");	
	return (-1);
}

init_called = 1;

ns_close ();	/* we reopen each time */
return (0);
}

/* returns # of cores you requested details about */
/* how many cores are 'listed' in the NAME SERVICE is returned via a varptr */
/* if the VERIFY flag is set, it attempts to contact them as well */
/* you must allocate upto MAX space for the results yourself */

int	hlib_find_cores (char *vmname,int verify,int maxresults,int * totalcores,coreinfo_t * results)
{
int i;
int j;
int group;
int ncores;
int returned; /* how many results we return */
int *ent;	
int *addr;
int *port;
int rc;
int req;
coreinfo_t *hi_p, *tmp_p;
double et;	/* echo time */

if (!init_called) 						/* if they didn't do it for them */
	if (hlib_init()<0) return (-1);		/* if init fails return failure */

if (!vmname) {
	fprintf(stderr,"find_cores: no VM name secified.\n");
	return (-1);
	}

if (maxresults<0) {
	fprintf(stderr,"find_cores: max results negative? call with 0 instead\n");
	return (-1);
	}

if ((maxresults>0) && (!results)) {
	fprintf(stderr,"find_cores: no return/result memory for %d results requested?\n",
					(maxresults));
	return (-1);
	}

/* hlib_init called means we have a NS so use it to find the core info */

/* first make some space for some result info :) */
ent = (int *) malloc (maxresults*sizeof(int));
addr = (int *) malloc (maxresults*sizeof(int));
port = (int *) malloc (maxresults*sizeof(int));

if ((ent==NULL)||(addr==NULL)||(port==NULL)) {
	fprintf(stderr,"find_cores: cannot get memory for results!! not good.\n");
	free (ent);
	free (addr);
	free (port);
	return (-1);
	}

ns_open();
rc = ns_info (vmname, maxresults, &group, &ncores, ent, addr, port);
ns_close();

if (rc<0) {
	free (ent);
	free (addr);
	free (port);
	return (-1);
	}

returned = rc;

/* ok copy the stuff over */
hi_p = results;	/* get a copy of the from pointer */
tmp_p = hi_p;   /* my tmp ptr */

for (i=0;i<rc;i++) {
printf("grp %d i %d ent[i] %d bin %d\n", group, i, ent[i], pf_bin (group, ent[i]));
	tmp_p -> rank = ent[i];
	tmp_p -> core_id = pf_bin (group, ent[i]);	/* make core_id */
	strcpy (tmp_p -> vmname, vmname );	/* copy name */
	sprintf (tmp_p -> hostname, "Not listed");	/* ! */
	tmp_p -> addr = addr[i];
	tmp_p -> port = port[i];
	tmp_p -> verified = COREINFO_UNKNOWN;	/* not yet */
	tmp_p -> rtt = COREINFO_RTT_UNKNOWN; /* seconds */
	tmp_p -> status = -1;		/* not known yet */
	tmp_p -> npackages = -1;	/* not known yet */
	tmp_p -> cpuloads[0] = 0.0;
	tmp_p -> cpuloads[1] = 0.0;
	tmp_p -> cpuloads[2] = 0.0;
	tmp_p++;	/* next record */
	}

tmp_p = hi_p;	/* again we check */
if (verify) { /* then ping em, no answer means not verified */
	for (i=0;i<rc;i++) {
		hlib_core_ping (tmp_p, 1,
		(COREINFO_PING_GET_STATS|COREINFO_PING_GET_NAMES));		
									/* ping/echo and time */
		tmp_p++;	/* ok, lets step through them */
		}
	} /* if we verify hcores */

/* finished with tmp results mem so free it */
free (ent);
free (addr);
free (port);

/* return the total number of cores that exist */
/* some people only call this library to verify such things */

if (totalcores) *totalcores = ncores;


return (returned);
}




/*
 ping has changed
 now not only can it act as a ping
 but if timeit set it can update the rtt value
 but if the getinfo flag is also set, get gets an info update :)
 GEF UTK May03
 */
int	hlib_core_ping (coreinfo_t *core_ptr, int timeit, int getinfo) 
{
int addr;
int port;
int rc;
int result;
int cs;
int ntag;
int from;
double st, et, ot;
int i0;
int tags[3];
int mb;
int npackages;
int nsvcs;
int nthreads;
double loads[3];
char hname[256];
int status;

/* default paranoid checks */
if (!core_ptr) return (-1);

if (timeit) ot=sec_time ();	/* warm up timing routine */

/* connect to hcore */
addr = core_ptr->addr;
port = core_ptr->port;

#ifdef VERBOSE
	printf("hlib:core_ping: attempting to connect to [0x%x][%d]\n",
		addr, port);
#endif

/* we use addr version as hostname may not be registered */

if (timeit) st=sec_time ();
cs = getconn_addr(addr, &port, 0);	/* not range is zero ! */		
									/* i.e. we connect only to THAT port */
if (cs<0) { 
#ifdef VERBOSE
	printf("hlib:core_ping:could not connect\n");
#endif
	core_ptr -> verified = COREINFO_FAILED;
	return (-1);	/* could not connect to given hcore */
					/* you might try again later ? */
	}

/* send request to core */
tags[0] = HCORE_PROTO_PING; 
tags[1] = getinfo;
rc = send_pkmesg (cs, HLIB_ID_CLIENT_ANON, 2, tags, EMPTYMSGBUF, 0);
if (rc<0) {
#ifdef VERBOSE
	printf("hlib:core_ping:connected but bad\n");
#endif
	closeconn (cs);	/* be safe not sorry */
	return (-1);
	}

/* receive reply */
ntag = 1;   /* we expect the cores status as the only return tag */
i0 = recv_pkmesg (cs, &from, &ntag, &status, &mb); /* do the recv */

if (timeit) et=sec_time ();

closeconn (cs);	/* be safe not sorry */

/* check it */
if (i0<0) {
#ifdef VERBOSE
	printf("hlib:core_ping:connected but bad reply\n");
#endif
	core_ptr -> verified = COREINFO_FAILED;
	free_msg_buf (mb);
	return (-1);	/* bad reply */
	}

if (from != core_ptr->core_id) {	/* wrong core replied! */
#ifdef VERBOSE
	printf("hlib:core_ping:connected but wrong core replied (expected 0x%x but got 0x%x \n", core_ptr->core_id, from);

#endif
	core_ptr -> verified = COREINFO_STALEINFO;
	free_msg_buf (mb);
	return (-1);
		/* maybe do something else here later */
	}

/* else all is ok */
#ifdef VERBOSE
	printf("hlib:core_ping:0x%x pinged ok\n",  core_ptr->core_id);
#endif

core_ptr -> status = status;
core_ptr -> verified = COREINFO_OK;	/* yeha how ever that is spelt */

if (timeit) core_ptr -> rtt = (double)((et - st)*(double)1000.0);

/* else its a more complex operation involving a message buffer etc */

if (getinfo&COREINFO_PING_GET_STATS) {
    upk_int32 (mb, &npackages, 1);
    upk_int32 (mb, &nthreads, 1);
    upk_int32 (mb, &nsvcs, 1);
    upk_real64 (mb, loads, 3);

#ifdef VERBOSE
	printf("npackages %d nthreads %d nsvcs %d loads %lf %lf %lf\n",
		npackages, nthreads, nsvcs, loads[0], loads[1], loads[2]);
#endif

	/* copy it back to the coreinfo */
	core_ptr -> npackages = npackages;
	core_ptr -> nthreads = nthreads;
	core_ptr -> nservices = nsvcs;
	core_ptr -> cpuloads[0] = loads[0];
	core_ptr -> cpuloads[1] = loads[1];
	core_ptr -> cpuloads[2] = loads[2];
}
if (getinfo&COREINFO_PING_GET_NAMES) {
    upk_string (mb, hname, 255);

#ifdef VERBOSE
	printf("hname [%s]\n", hname);
#endif

	strcpy (core_ptr->hostname, hname);
}

free_msg_buf (mb);

return (0);
}

void	hlib_print_core_info (coreinfo_t *core_ptr,int count)
{
int i;
coreinfo_t *cp;

/* default paranoid checks */
if (!core_ptr) return;

cp = core_ptr;

for (i=0;i<count;i++) {

puts("-----------------------------------------------------------------------");
printf("Core info display\n");
puts("-----------------------------------------------------------------------");
printf("core rank\t\t:%d\n", cp->rank);
printf("core ID\t\t\t:%d\n", cp->core_id);
printf("core VM name\t\t:%s\n", cp->vmname);
printf("core hostname\t\t:%s\n", cp->hostname);
printf("core IPv4 addr\t\t:0x%x\n", cp->addr);
printf("core contact port\t:%d\n", cp->port);
if (cp->verified ==COREINFO_UNKNOWN)
	printf("core contact verified\t: UNKNOWN\n");
if (cp->verified ==COREINFO_OK)
	printf("core contact verified\t: OK\n");
if (cp->verified ==COREINFO_FAILED)
	printf("core contact verified\t: core has FAILED\n");
if (cp->verified ==COREINFO_STALEINFO)
	printf("core contact verified\t: WRONG hcore replied. User library has stale contact info!\n");
if (cp->status >=0)
	printf("core status code\t:%d\n", cp->status);
else
	printf("core status code\t: UNKNOWN\n");

if (cp->rtt == COREINFO_RTT_UNKNOWN)
	printf("core ping rtt \t\t: not measured (unknown)\n");
else
	printf("core ping rtt \t\t: %lf mSecs\n", cp->rtt);

if (cp->npackages >=0)
	printf("core packages loaded\t:%d\n", cp->npackages);
else
	printf("core packages loaded\t: UNKNOWN\n");

printf("core last loadavgs %lf : %lf : %lf\n",
		cp->cpuloads[0], cp->cpuloads[1], cp->cpuloads[2]);

/* template for more info as added */
/* printf("core rank\t\t:%d\n", cp->rank); */

	cp++;	/* look at the next core */

} /* for each core */

puts("-----------------------------------------------------------------------");
puts("\n");
return; 
}

int hlib_call_function (coreinfo_t * core_idp,int fid,int * s)
{
int p;
int i;
int i0, i1, i2, i3, i4, i5;
int op, type=-123;
int cs;
int mb;
int req[2];
int rc;

if(!core_idp) return (-1);


if (!init_called) 						/* if they didn't do it for them */
	if (hlib_init()<0) return (-1);		/* if init fails return failure */


p = core_idp->port;
cs = getconn_addr ( core_idp->addr, &p, 0);
if (cs<0) { 
	printf("hlib:call_function: connect failed. Marking network status UNKNOWN\n"); 
	core_idp->verified = COREINFO_UNKNOWN;
	return (-1);
	}

op = 0;	/* for now use a default op type for sync non parallel calls */

printf("hlib:call_function: Calling with %d 0x%x %d\n", type, fid, op);

req[0] = HCORE_PROTO_CALLFNC_THREAD;
req[1] = fid;

/* we don't have to pack data as all is in the header tags for speed */

/* writeconn (*s, &T, sizeof(char)); */
/* writeconn (*s, &id, sizeof(int)); */
/* writeconn (*s, &O, sizeof(char)); */

rc = send_pkmesg (cs, HLIB_ID_CLIENT_ANON, 2, req, EMPTYMSGBUF, 0);
if (rc<=0) {
	printf("hlib:call_function: problem sending request to hcore 0x%x\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

/* request sent */
/* now if they don't want the return data (i.e. they gave a NULL for */
/* the socket, we close it for them, else we return the socket and then */
/* its their problem */

if (s) *s = cs;
else closeconn (cs);

return (0);
}

int hlib_load_package (coreinfo_t *core_idp,char * pname,char * cname,int * id,int * nf)
{
int p;
int i;
int s=-123;
unsigned int call;
int req[2];
int result[2];
int type;
int ntag;
int rc;
int cs;
int mb;
int from;

if(!core_idp) return (-1);
if (!init_called) 						/* if they didn't do it for them */
	if (hlib_init()<0) return (-1);		/* if init fails return failure */

req[0] = HCORE_PROTO_LOAD;	/* load */

type = 2;	/* type of load using only two names */

if ((!pname)||(!cname)) {
	printf("hlib:load_package: incomplete package/component name specified\n");
	return (-1);
	}

p = core_idp->port;
cs = getconn_addr ( core_idp->addr, &p, 0);
if (cs<0) { 
	printf("hlib:load_package: connect failed. Marking network status UNKNOWN\n"); 
	core_idp->verified = COREINFO_UNKNOWN;
	return (-1);
	}


printf("Calling with %d %d %s %s \n", req[0], type, pname, cname);
mb = get_msg_buf (0);
if (mb<0) {
	printf("hlib:load_package: create message buffer failed?\n");
	closeconn(cs);
	return (-1);
	}

/* writeconn (s, &T, sizeof(char)); */
/* writeconn (s, &i1, sizeof(int)); */
/* writeconn (s, &i2, sizeof(int)); */
/* writeconn (s, pname, i2); */
/* writeconn (s, &i4, sizeof(int)); */
/* writeconn (s, cname, i4); */

/* we just build a request now rather than write each bit individually */

rc = pk_int32 (mb, &type, 1);
if (rc<=0) {
	printf("hlib:load_package: problem making request message\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

rc = pk_string (mb, pname);
if (rc<=0) {
	printf("hlib:load_package: problem making request message\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

rc = pk_string (mb, cname);
if (rc<=0) {
	printf("hlib:load_package: problem making request message\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

/* send actual request */
rc = send_pkmesg (cs, HLIB_ID_CLIENT_ANON, 1, req, mb, 1);

if (rc<=0) {
	printf("hlib:load_package: problem sending request to hcore 0x%x\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

/* get reply */
ntag = 2;   /* we expect */
rc = recv_pkmesg (cs, &from, &ntag, result, NULL); /* do the recv */

if (rc<0) {
	printf("hlib:load_package: badly formed reply from hcore 0x%x\n", core_idp->core_id);
	closeconn (cs);
	return (-1);
	}
	
/* readconn (s, &i0, sizeof(int)); */
/* readconn (s, &i1, sizeof(int)); */

/* *id = i0; */
/* *nf = i1; */

/* usleep (1000); */

/* return results only if we have valid pointers */

	if (id) *id = result[0];
	if (nf) *nf = result[1];

	if (result[1]>=0) { /* i.e. we got a valid result back, we update DS */
		core_idp -> npackages = result[1];
		}

closeconn (s);

return (result[0]);
}


int hlib_get_function (coreinfo_t *core_idp,char * fname,int package,int * fid)
{
int p;
int i;
int s=-123;
int i0, i1, i2, i3, i4, i5;
unsigned int call;
int result[2];
int req[2];
int ntag;
int rc;
int type;
int id;
int from;
int cs;
int mb;

if(!core_idp) return (-1);
if (!init_called) 						/* if they didn't do it for them */
	if (hlib_init()<0) return (-1);		/* if init fails return failure */

if (!fid) {
	printf("hlib:get_function: function id return ptr is a NULL ?\n");
	return (-1);
	}

req[0] = HCORE_PROTO_PINFO;	/* load */

p = core_idp->port;
cs = getconn_addr ( core_idp->addr, &p, 0);
if (cs<0) { 
	printf("hlib:get_function: connect failed. Marking network status UNKNOWN\n"); 
	core_idp->verified = COREINFO_UNKNOWN;
	return (-1);
	}


printf("Calling with reqID %d funcname %s package ID %d \n", req[0], fname, package);
mb = get_msg_buf (0);
if (mb<0) {
	printf("hlib:get_function: create message buffer failed?\n");
	closeconn(cs);
	return (-1);
	}

/* writeconn (s, &T, sizeof(char)); */
/* writeconn (s, &package, sizeof(int)); */

/* writeconn (s, &i2, sizeof(int)); */
/* writeconn (s, fname, i2); */

rc = pk_int32 (mb, &package, 1);
if (rc<=0) {
	printf("hlib:load_package: problem making request message\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}
	
rc = pk_string (mb, fname);
if (rc<=0) {
	printf("hlib:load_package: problem making request message\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}

/* send actual request */
rc = send_pkmesg (cs, HLIB_ID_CLIENT_ANON, 1, req, mb, 1);

if (rc<=0) {
	printf("hlib:get_function: problem sending request to hcore 0x%x\n",
			core_idp->core_id);
	closeconn (cs);
	return (-1);
	}


/* get reply */

/* readconn (s, &i0, sizeof(int)); */
/* readconn (s, &i1, sizeof(int)); */

/* get reply */
ntag = 2;   /* we expect */
rc = recv_pkmesg (cs, &from, &ntag, result, NULL); /* do the recv */

if (rc<0) {
	printf("hlib:get_function: badly formed reply from hcore 0x%x\n", core_idp->core_id);
	closeconn (cs);
	return (-1);
	}
	
/* readconn (s, &i0, sizeof(int)); */

/* get results from tags */
id = result[0];
type = result[1];

printf("Fid 0x%x\n", id);

printf("found function %s in package %d with type %d of ID 0x%x\n",
		fname, package, type, id);

/* return var  */
*fid = id;

#ifndef WIN32
usleep (1000);
#endif
closeconn (s);

return (id);
}

int hlib_end ()
{
if (!init_called) return(-1);

/* we would free msg buffers but the NS client lib does this for us instead */

ns_close ();	/* we reopen each time */
return (0);

}

